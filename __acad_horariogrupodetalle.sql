-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-01-2018 a las 01:10:19
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_horariogrupodetalle`
--

CREATE TABLE `acad_horariogrupodetalle` (
  `idhorario` int(11) NOT NULL,
  `idgrupoauladetalle` int(11) NOT NULL,
  `fecha_finicio` timestamp NOT NULL,
  `fecha_final` timestamp NOT NULL,
  `descripcion` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `color` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idhorariopadre` tinyint(4) NOT NULL,
  `diasemana` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_horariogrupodetalle`
--

INSERT INTO `acad_horariogrupodetalle` (`idhorario`, `idgrupoauladetalle`, `fecha_finicio`, `fecha_final`, `descripcion`, `color`, `idhorariopadre`, `diasemana`) VALUES
(1, 1, '2018-01-09 05:00:00', '2018-01-09 05:00:00', '2017', '#cccccc', 0, 0),
(2, 2, '2018-01-15 11:00:00', '2018-01-15 13:00:00', 'A1', '#cccccc', 0, 0),
(3, 2, '2018-01-22 11:00:00', '2018-01-22 13:00:00', 'A1', '#cccccc', 0, 0),
(4, 2, '2018-01-29 11:00:00', '2018-01-29 13:00:00', 'A1', '#cccccc', 0, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acad_horariogrupodetalle`
--
ALTER TABLE `acad_horariogrupodetalle`
  ADD PRIMARY KEY (`idhorario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
