-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-08-2018 a las 23:19:18
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas_quiz`
--

CREATE TABLE `notas_quiz` (
  `idnota` int(11) NOT NULL,
  `idcursodetalle` int(11) DEFAULT NULL,
  `idrecurso` int(11) DEFAULT NULL,
  `idalumno` int(11) NOT NULL,
  `tipo` char(1) NOT NULL,
  `nota` int(11) NOT NULL,
  `regfecha` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `notas_quiz`
--

INSERT INTO `notas_quiz` (`idnota`, `idcursodetalle`, `idrecurso`, `idalumno`, `tipo`, `nota`, `regfecha`) VALUES
(1, NULL, 1, 55555555, 'E', 85, '2018-08-21'),
(2, NULL, 2, 55555555, 'E', 65, '2018-08-21'),
(3, NULL, 1, 55555555, 'S', 85, '2018-08-21'),
(4, NULL, 2, 55555555, 'S', 99, '2018-08-21');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `notas_quiz`
--
ALTER TABLE `notas_quiz`
  ADD PRIMARY KEY (`idnota`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
