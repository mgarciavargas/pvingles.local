<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-11-2016 
 * @copyright	Copyright (C) 19-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebBiblioteca extends JrWeb
{
	private $oNegBiblioteca;
	public function __construct()
	{
		parent::__construct();
	}

	public function defecto(){
		return $this->listado();
	}

	private function dirToArray($dir,$type,$path='') { 
   
	   $result = array();
	   $cdir = scandir($dir);
	   $tmptype["image"]=array('png','gif','jpg','jpeg','bmp');
       $tmptype["audio"]=array('mp3','wav','mid');
       $tmptype["video"]=array('mp4','mpeg');
       $tmptype["file"]=array('pdf','doc','docx','xls','xlsx','ppt','pptx','txt');
       $tmptype["pdf"]=array('pdf');
	   foreach ($cdir as $key => $value) 
	   { 	      
	      
	      if (!in_array($value,array(".",".."))){ 
	        if (is_dir($dir . DIRECTORY_SEPARATOR . $value)){
	         	$path1=$path.DIRECTORY_SEPARATOR . $value;
	         	$res=$this->dirToArray($dir . DIRECTORY_SEPARATOR . $value,$type,$path1);
	            $result=array_merge($result,$res);
	        }else{
	         	$ext=strtolower(pathinfo($value, PATHINFO_EXTENSION));
				if(!empty($tmptype[$type])){	         		
	         		if(in_array($ext,$tmptype[$type])){
	         			$result[] = $path.DIRECTORY_SEPARATOR.$value; 
	         		}
	         	}else
	         	$result[] = $path.DIRECTORY_SEPARATOR.$value; 
	        }	         	
	      } 
	   }
   	   return $result; 
	} 

	public function listado()
	{
		try{
			global $aplicacion;			
			$this->type=!empty($_REQUEST["type"])?$_REQUEST["type"]:'all';
			$plt=!empty($_REQUEST["plt"])?$_REQUEST["plt"]:'general';
			$directoriotmp=!empty($_REQUEST["dir"])?($_REQUEST["dir"]):('aulasvirtuales');
			$dire='';
			$this->directorio='';
			if(!empty($directoriotmp)){
				$dire=$directoriotmp.SD;
				$this->directorio='/'.$directoriotmp.'/';
			}
			$dir =  RUTA_BASE.'static'.SD.'media'.SD.$dire;
			$this->datos  = $this->dirToArray($dir,$this->type);
			$this->documento->plantilla = $plt;
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
			$this->documento->setTitulo(JrTexto::_('Media'), true);
			$this->esquema = 'biblioteca/listado';
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	public function listadojson()
	{
		$this->documento->plantilla = 'returnjson';
		$type=!empty($_POST["type"])?$_POST["type"]:'';
        $nombre=!empty($_POST["texto"])?$_POST["texto"]:'';
		$filtros=array();
		if(!empty($type) && $type!='all' ) $filtros['tipo']=$type;
        if(!empty($nombre) ){$filtros['nombre']=$nombre;}
		$this->datos=$this->oNegBiblioteca->buscar($filtros);
		if(!empty($this->datos)){
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
			exit(0);
		}else{
			echo json_encode(array('code'=>'Error','msj'=>$filtros));//JrTexto::_('File type incorrect')));
			exit(0);
		}
	}

	private function nombrefile($file,$cont=0,$nameorgi=''){		
		$nameinfo=pathinfo($file);
		$name=$nameinfo['filename'].".".$nameinfo['extension'];
		if(file_exists($file)){
			$cont++;
			$nameorgi=!empty($nameorgi)?$nameorgi:$name;	
			$newname=$cont."_".$nameorgi;
			$file=str_replace($name, $newname, $file);
			$name = $this->nombrefile($file,$cont,$nameorgi);
			return $name;
		}else 
		return $name;
	}

	public function cargarmedia(){
		try {			
			$this->documento->plantilla = 'returnjson';

			if(empty($_POST["tipo"])||empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
			$intipo='';
			$tipo=@$_POST["tipo"];
			if($tipo=='video'){
				$intipo=array('mp4','mov');
			}elseif($tipo=='image'){
				$intipo=array('jpg', 'jpeg','gif','png');
			}elseif($tipo=='audio'){
				$intipo=array('mp3','wav');
			}elseif($tipo=='pdf'){
				$intipo=array('pdf', 'PDF');
			}elseif($tipo=='ppt'){
				$intipo=array('ppt', 'PPT', 'pptx', 'PPTX');
			}elseif($tipo=='xls'){
				$intipo=array('xls', 'XLX', 'xls', 'XLSX');
			}
			
			if(!in_array($ext, $intipo)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
				exit(0);
			}
			try{				
				$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD .'aulasvirtuales' .SD .$tipo. SD ;
				@mkdir($dir_media,0775,true);
				@chmod($dir_media,0775);				
				$nombrefile=$this->nombrefile($dir_media.$file["name"]);
				if(move_uploaded_file($file["tmp_name"],$dir_media.$nombrefile)) 
			  	{
			   		global $aplicacion;
			   		echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File Upload success'),'namelink'=>'/media/aulasvirtuales/'.$tipo.'/'.$nombrefile,'nombre'=>$nombrefile));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error upload File')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}
	}


	public function cargarblob(){
		try {			
			$this->documento->plantilla = 'returnjson';
			if(empty($_POST["type"])||empty($_POST["name"])||empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$type=$_POST["type"];
			$intipo='';
			$tipe_=@explode('/',@$_POST["type"]);
            $tipo=$tipe_[0];
            $ext=$tipe_[1];
			if($tipo=='image'){
				$intipo=array('jpg','gif','png');
			}elseif($tipo=='audio'){
				$intipo=array('mp3','wav');
			}
			$name=@$_POST["name"];			
			if(!in_array($ext, $intipo)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
				exit(0);
			}
			try{

				$newname="N".@$_POST["nivel"].'-'."U".@$_POST["unidad"].'-'."A".@$_POST["leccion"].'-'.date("Ymdhis").".".$ext;
				
				$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo ;
				@mkdir($dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo,'0777');
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newname)) 
			  	{
			   		global $aplicacion;			
					$usuarioAct = NegSesion::getUsuario();
			   		$this->oNegBiblioteca->__set('nombre',$name);
					$this->oNegBiblioteca->__set('link',$newname);
					$this->oNegBiblioteca->__set('tipo',$tipo);
					$this->oNegBiblioteca->__set('idpersonal',@$usuarioAct["dni"]);
					$this->oNegBiblioteca->__set('idnivel',@$_POST["nivel"]);
					$this->oNegBiblioteca->__set('idunidad',@$_POST["unidad"]);
					$this->oNegBiblioteca->__set('idleccion',@$_POST["leccion"]);
					$this->oNegBiblioteca->__set('fechareg',date('Y/m/d'));
					$res=$this->oNegBiblioteca->agregar();
			   		echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File Upload success'),'namelink'=>$newname,'nombre'=>$name,'id'=>$res));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error upload File')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}

	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}				
				$link = $args[0];
				$dir_media = RUTA_BASE . 'static'.$link ;
				@unlink($dir_media);
                $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Delete file success')), 'success');
				$oRespAjax->setReturnValue(true);
				
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
}