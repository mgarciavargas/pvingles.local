<?php $idgui = uniqid(); 
//var_dump($this->datos);
$personal=$this->usuarioAct["nombre_full"]." <".$this->usuarioAct["email"].">";
$enviamail=$this->personal[0]["nombre"]." ";
$aula=@$this->datos;
$portada=$aula["portada"];
if(!empty($portada)){
	$pos = strpos($portada, 'nofoto');
	if($pos===false)
		$portada='<div style="text-align: center;"><img  src="'.@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$portada).'" width="60%" ></div>';
	else
		$portada='';
}else
$portada='';
$enlace=$this->documento->getUrlBase()."/aulavirtual/requisitos/?id=".$aula["aulaid"]."&".$idgui.$aula["aulaid"].date("Ymd")."&idioma=ES";
?>
<style type="text/css">
	.input-group-addon{
		width:120px; !important;
	}
	.input-group{
		width: 100%;
	}
</style>
<form method="post" id="frm-<?php echo $idgui;?>" onsubmit="return false;">
<input type="hidden" name="idaula" id="idaula" value="<?php echo @$aula["aulaid"]; ?>">
<div class="row">
	<div class="panel panel-primary" style="margin-top: 1ex;">
		<div class="panel-heading" style="overflow: hidden;">
		<ol class="breadcrumb pull-left" style="margin: 0px; padding: 0px; background:rgba(187, 197, 184, 0);">
		  <li><a href="<?php echo $this->documento->getUrlBase() ?>/" style="color:#fff">Smartclass</a></li>
		  <li><a href="<?php echo $this->documento->getUrlBase() ?>/aula/editar/?id=<?php echo @$aula["aulaid"]; ?>" style="color:#fff">
		  <?php echo ucfirst(JrTexto::_('Edit')); ?></a></li> 
		  <li class="active"  style="color:#ccc"><?php echo JrTexto::_('Invite participants'); ?></li>
		</ol>
		<!--div class="pull-right"><a href="#" class="btn btn-xs btn-default showdemo" data-videodemo="settings.mp4"><i class="fa fa-question-circle"></i> <?php //echo JrTexto::_("Help"); ?></a></div-->
    </div>
    <div class="panel-body">
      <div class="panel pnl100">
        <div class="panel-body">
           <div class="col-xs-12 col-sm-12 col-md-12">
           	<div class="form-group">
              <div class="input-group">
                <div class="input-group-addon"><b><?php echo ucfirst(JrTexto::_("Subject"))?> :</b></div>                
                <input type="text" name="asunto" id="asunto" value="<?php echo ucfirst(JrTexto::_("Invitation to participate in Smartclass"))?>" class="form-control">
              </div>
            </div>          
            <div class="form-group" style="display: none;">
              <div class="input-group">
                <div class="input-group-addon"><b><?php echo ucfirst(JrTexto::_("From"))?> :</b></div>                
                <input type="email" readonly="readonly" name="deemail" id="deemail" value="<?php echo $this->usuarioAct["email"]; ?>" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon"><b><?php echo ucfirst(JrTexto::_("To"))?> :</b></div>
                <ul id="myTags" style="margin: 0px;">                	
                </ul>
                <div class="input-group-addon"><b data-ventana="vparticipantes" data-titulo="<?php echo JrTexto::_("List Participants"); ?>" href="<?php echo $this->documento->getUrlBase().'/aulavirtualinvitados/mostrar/?id='.$aula["aulaid"]; ?>" data-modal="si" class="btn btn-primary btn-xs btn-showparticipantes"><i class="fa fa-users"></i> <?php echo ucfirst(JrTexto::_("Add participants"))?> </b></div>
              </div>
            </div>   
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
	          <div class="form-group">
	              <label><b><?php echo ucfirst(JrTexto::_("Message"))?> :</b></label>
	              <textarea class="form-control" name="mensaje" id="mensaje">Ud. <b><span style="color:#b90e0e"> !!usuario!! </span></b> 
	              <br>Esta cordialmente invitado a la siguiente clase virtual denominada:<br>
				  <h3 style="text-center:center; color:#031b47"><?php echo $aula["titulo"]; ?></h3><br><?php echo $portada; ?><br>
	              <p style="text-align: justify;" data-mce-style="text-align: justify;"><?php echo $aula["descripcion"]; ?></p>
	              <p style="text-align: center;" data-mce-style="text-align: center;">
	              <h4 style="text-align: center;" data-mce-style="text-align: center;">Para conectarse </h4>
	              Verifique la fecha y hora de la clase : <b><?php echo $aula["fecha_inicio"]; ?></b><br> luego
	              click  en el siguiente enlace <b><a href="<?php echo $enlace ?>">Ingresar aqui</b></a><br>
	              o copie la siguiente direccion url en su navegador<br>
	              <a href="<?php echo $enlace ?>"><b><?php echo $enlace ?></b></a>
	              </p></textarea>
	            </div>
            </div>
        </div>

      </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">          
          <button class="btn btn-primary btnsendemail"><i class="fa fa-paper-plane"></i>&nbsp; <?php echo ucfirst(JrTexto::_('Send')).' '.JrTexto::_('Email'); ?> </button>          
        </div>      
    </div>  
  </div>
</div>
</form>
<script type="text/javascript">
    function recargarparticipantes<?php echo $idgui; ?>(data){
    	$("#myTags li.tagit-choice").remove();
    	$.each(data,function(i,v){
    		var _email=v.name!=''?(v.name+'<'+v.email+'>'):v.email;
    		$("#myTags").tagit("createTag", _email);
    	});    	
    }

    function obteneremails<?php echo $idgui; ?>(){
    	$paraemails=[];
		$('#myTags').find('li span.tagit-label').each(function(){
			var texto=$(this).text();
			var regex = /<([^>]*)>/;            
			var email='';
			var name='';
			if(regex.exec(texto)!=null){email=regex.exec(texto)[1]; name=texto.split("<")[0];}else email=texto;
			if(!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email))
			  return false;
			var item={email:email,name:name}
			$paraemails.push(item);
		});
		return $paraemails;
    }
	$(document).ready(function(){
		$('.btn-showparticipantes').click(function(e){
			e.preventDefault();
		    e.stopPropagation();
		    var emails=obteneremails<?php echo $idgui; ?>();
		    var enmodal=$(this).attr('data-modal')||'no';
		    var fcall=$(this).attr('data-fcall')||'recargarparticipantes<?php echo $idgui; ?>';
		    var url=$(this).attr('href')
		    if(url.indexOf('?')!=-1) url+='&fcall='+fcall+'&usuarios='+JSON.stringify(emails);
		    else url+='?fcall='+fcall;
		    var ventana=$(this).attr('data-ventana')||'Alumno';
		    var claseid=ventana+'_<?php echo $idgui; ?>';
		    $('.modal.'+claseid).remove();
		    var titulo=$(this).attr('data-titulo')||'';
		    titulo=titulo.toString().replace('<br>',' ');     
		    if(enmodal=='no'){
		      return redir(url);
		    }
		    url+='&plt=modal';
		    openModal('lg',titulo,url,ventana,claseid);
		});
		$('.btnsendemail').click(function(){
			tinyMCE.triggerSave(); 
			var mensaje=$('#mensaje').val();
			var Arradjuntos=[];
			var imgs=[];
			$(mensaje).find('img').each(function(){
				imgs.push($(this).attr('src'));
			});
			var emails=obteneremails<?php echo $idgui; ?>();
			var txtemails=JSON.stringify(emails);
			var formData = new FormData();
			if(imgs!=='[]')formData.append("images", JSON.stringify(imgs));
			if(txtemails==='[]'){ 
				mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', '<?php echo JrTexto::_('Mail message without recipients'); ?>', 'error');
				return;
			}
			formData.append("paraemail", txtemails);
			formData.append("idaula", '<?php echo $aula["aulaid"]; ?>');
			 $.ajax({
	            url: _sysUrlBase_+'/Aulavirtualinvitados/json_guardar/',
	            type: "POST",
	            data:  formData,
	            contentType: false,
	            dataType :'json',
	            cache: false,
	            processData:false,	          
	            success: function(data)
	            {	
	                if(data.code==='ok'){
	                	mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'success');	                   
	                }else{
	                    mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'warning');
	                }
	                $('#procesando').hide('fast');
	                return false;
	            },
	            error: function(xhr,status,error){
	            	console.log(xhr.responseText);
	            	console.log(status);
	            	console.log(error);
	                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', status, 'warning');
	                $('#procesando').hide('fast');
	                return false;
	            }
	        }).always(function(){
	            //$('.btnsendemail').removeAttr('disabled');
	        });


            formData.append("deemail", $('#deemail').val());
            formData.append("mensaje", mensaje);
            formData.append("asunto", $('#asunto').val());            
            
            $.ajax({
	            url: _sysUrlBase_+'/Sendemail/enviarcorreoall',
	            type: "POST",
	            data:  formData,
	            contentType: false,
	            dataType :'json',
	            cache: false,
	            processData:false,
	            beforeSend: function(XMLHttpRequest){
	                $('#procesando').show('fast');
	                $('.btnsendemail').attr('disabled','disabled');
	            },
	            success: function(data)
	            {	               	
	              
	                if(data.code==='ok'){
	                	mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'success');	                   
	                }else{
	                    mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'warning');
	                }
	                $('#procesando').hide('fast');
	                $('.btnsendemail').removeAttr('disabled');
	                return false;
	            },
	            error: function(xhr,status,error){
	                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', status, 'warning');
	                $('#procesando').hide('fast');
	                return false;
	            },
	        }).always(function() {
	            $('.btnsendemail').removeAttr('disabled');
	        });
		})
		var mostrarEditorMCE  = function(obj,showtoolstiny){
		  var showtools=showtoolstiny||'';
		  tinymce.init({
		    relative_urls : false,
		    remove_script_host: false,
		    convert_newlines_to_brs : true,
		    menubar: false,
		    statusbar: false,
		    verify_html : false,
		    content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
		    selector: obj,
		    height: 200,
		    paste_auto_cleanup_on_paste : true,
		    paste_preprocess : function(pl, o) {
		        var html='<div>'+o.content+'</div>';
		        var txt =$(html).text(); 
		        o.content = txt;
		    },paste_postprocess : function(pl, o) {       
		        o.node.innerHTML = o.node.innerHTML;
		    },
		    plugins:[showtools+"  link image textcolor paste" ],  //chingosave chingoinput chingoimage chingoaudio chingovideo styleselect
		    toolbar: ' undo redo |  bold italic underline | alignleft aligncenter alignright alignjustify | numlist |  forecolor backcolor |  '+showtools // chingosave chingoinput chingoimage chingoaudio chingovideo 
		  });
		};
		mostrarEditorMCE('#mensaje');
		$('#myTags').tagit({ 
			beforeTagAdded: function(event, ui){
			var texto=ui.tag[0].innerText;
			var email=texto;
			var regex = /<([^>]*)>/;
			if(regex.exec(texto)!=null) email=regex.exec(texto)[1];
			else email=email.substr(0,email.length-1);
			if(!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email))
			  return false;
		    },fieldName: "toemails",placeholderText:"example@tuempresa.com"});
		<?php 
		if(!empty($this->alumnos))
			foreach ($this->alumnos as $alumno){
			echo '$("#myTags").tagit("createTag", "'.$alumno["ape_paterno"].' '.$alumno["ape_materno"].' '.$alumno["nombre"]."<".$alumno["email"].'>");';
			}
		if(!empty($this->alumnosinvitados))
			foreach ($this->alumnosinvitados as $alumno){
			echo '$("#myTags").tagit("createTag", "'.$alumno["usuario"]."<".$alumno["email"].'>");';
			}
		?>
});
</script>