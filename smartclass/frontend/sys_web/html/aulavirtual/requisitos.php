<style type="text/css">
.input-group{
	width: 100%;
}
.icon1{font-size: 4em;}
.iconerror{
	/*text-decoration:underline;*/
}
.iconerror i:after{
    font-size: 1.3em;
    position: relative;
    right: 14px;
    /* width: 100%; */
    content: "X";
    color: rgba(245, 16, 16, 0.51);

}
h4.title{
	width: 100%;
    margin: 0;
    padding: 0;
    text-align: center;
}
.btnprobarmicro.grabando .fa{
	color: red;
}
</style>
<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$aula=@$this->aula[0];
$usuarioAct=$this->usuarioAct;
$portada=@$aula["portada"];
if(!empty($portada)){
	$pos = strpos($portada, 'nofoto');
	if($pos===false)
		$portada='<img class="img-responsive"  src="'.@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$portada).'" width="60%" >';
	else
		$portada='';
}else
$portada='';
$enlace=$this->documento->getUrlBase()."/smartclass/?uri=".$aula["aulaid"]."_".$idgui.$aula["aulaid"].date("Ymd")."&idioma=ES";
$fini=new DateTime($aula["fecha_inicio"]);
$fini->modify('-20 minutes');
$ffin=new DateTime($aula["fecha_final"]);
$fnow=new DateTime("now");
$msj='';
$errorlogin=false;
if($fini>=$fnow&&$fnow<$ffin){
	$msj='Smartclass '.JrTexto::_('not yet active for user connection');
	$errorlogin=true;
}else if($fnow>$ffin){
	$msj='Smartclass '.JrTexto::_('expired');
	$errorlogin=true;
}

?>
<script type="text/javascript" src="<?php echo $this->documento->getUrlBase(); ?>/static/libs/pizarra/getScreenId.js"></script>
<script type="text/javascript" src="<?php echo $this->documento->getUrlBase(); ?>/static/libs/pizarra/DetectRTC.js"></script>
<div class="row" style="padding: 0px 1ex;">
	<div class="panel panel-primary" id="ventanainfosmartclass" >
	    <div class="panel-headding bg-blue text-center" style="overflow: hidden;">
	    	<h4><?php echo ucfirst(JrTexto::_('Smartclass information')) ?></h4>
	    </div>
	    <div class="panel-body" style="overflow: hidden;">
	    	<div class="col-xs-12 col-sm-12 col-md-12 ">
	    		<h4 class="title"><?php echo ucfirst($aula["titulo"]); ?></h4>
		    	<div class="col-xs-12 col-sm-6 col-md-8">
			    	<p><?php echo $aula["descripcion"]; ?></p>
			    	<p><b><?php echo JrTexto::_("Start Date")?>: </b><?php echo $aula["fecha_inicio"]; ?></p>
			    	<p><b><?php echo JrTexto::_("Finish Date")?>: </b><?php echo $aula["fecha_final"]; ?></p>
		    	</div>
		    	<div class="col-xs-12 col-sm-6 col-md-4 text-center">
		    		<p><?php echo $portada; ?></p>
		    	</div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<?php if(!empty($msj)){?>
	    	<br>
	    	<hr>
	    	<br>
	    	<div class="alert alert-info" role="alert">
	    		<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-info-circle" aria-hidden="true"></i></div>
				<div class="col-xs-8 col-sm-8 col-md-9">
				<h4 class="alert-heading"> <?php echo JrTexto::_("Information") ?> !  </h4>
				    <p class="mb-0"><?php echo $msj; ?>   </p>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php } ?>

	    </div>
   	</div>
	<div class="panel panel-primary" id="vrequisitos" >
	    <div class="panel-headding bg-red text-center" style="overflow: hidden;">
	    	<h4><?php echo ucfirst(JrTexto::_('Smartclass requirements')) ?></h4>
	    </div>
	    <div class="panel-body" style="overflow: hidden; position: relative;">
	        <a href="javascript:window.location.reload(true);" class="btnverificar btn btn-sm btn-danger pull-right" style="margin: 2em; position: absolute; z-index:8; right: 1em; "><i class="fa fa-refresh"></i> <?php echo JrTexto::_("Re-check requirements") ?></a>
	    	<div class="col-xs-12 col-sm-12 col-md-12 ">
	    		<div class="alert alert-danger" id="validarnavegador" role="alert">
		    		<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-times-circle" aria-hidden="true"></i></div>
					<div class="col-xs-8 col-sm-8 col-md-9">
					<h4 class="alert-heading"> <?php echo JrTexto::_("Error") ?> ! <span class="iconerror"> (<?php echo JrTexto::_("Browser"); ?>)</span> </h4>
					    <p class="mb-0"><?php echo JrTexto::_("we feel your web browser does not support smartclass, you must use chrome or mozilla"); ?><br>
					    <a href="https://www.google.com/chrome/browser/desktop/index.html"><?php echo JrTexto::_("Install google chrome here") ?></a>  o <br>
					    <a href="https://www.mozilla.org/es-ES/firefox/new/"><?php echo JrTexto::_("Install mozilla firefox here") ?></a><br>
					    </p>
					</div>
					<div class="clearfix"></div>				   
				</div>
		    	<div class="alert alert-danger" id="validarmicrofono" role="alert">
		    		<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-times-circle" aria-hidden="true"></i></div>
					<div class="col-xs-8 col-sm-8 col-md-9">
					<h4 class="alert-heading"> <?php echo JrTexto::_("Error") ?> ! <span class="iconerror"><i class="fa fa-microphone"></i> (<?php echo JrTexto::_("Microphone"); ?>)</span> </h4>
					    <p class="mb-0"><?php echo JrTexto::_("No connected microphone has been detected, this being necessary for communication"); ?></p>
					</div>
					<div class="clearfix"></div>				   
				</div>
				<div class="alert alert-danger" id="validaraudio" role="alert">
		    		<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-times-circle" aria-hidden="true"></i>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-10">
					<h4 class="alert-heading"> <?php echo JrTexto::_("Error") ?> ! <span class="iconerror"><i class="fa fa-volume-up"></i> (<?php echo JrTexto::_("Audio output"); ?>)</span> </h4>
					    <p class="mb-0"><?php echo JrTexto::_("No connected speakers or headphones detected, this being necessary for communication"); ?></p>
					</div>
					<div class="clearfix"></div>			    
				</div>
				<div class="alert alert-warning" id="validarcamara" role="alert">
					<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-exclamation-triangle" aria-hidden="true"></i></div>
					<div class="col-xs-8 col-sm-8 col-md-9">
					<h4 class="alert-heading"> <?php echo JrTexto::_("Warning") ?> ! <span class="iconerror"><i class="fa fa-camera"></i> (<?php echo JrTexto::_("Webcam"); ?>)</span> </h4>
					    <p class="mb-0"><?php echo JrTexto::_("No connected speakers or headphones detected, this being necessary for communication"); ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="alert alert-warning" id="validarcompartirpantalla"  role="alert">
					<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-exclamation-triangle" aria-hidden="true"></i></div>
					<div class="col-xs-8 col-sm-8 col-md-9">
					<h4 class="alert-heading"> <?php echo JrTexto::_("Warning") ?> ! <span class="iconerror"><i class="fa fa-desktop"></i> (<?php echo JrTexto::_("Share desktop"); ?>)</span></h4>
					    <p class="mb-0"><?php echo JrTexto::_("no plugin needed to share your desktop, click the following link to install it"); ?> <br>
					   <a id="ischrome" href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk" target="_blank"><?php echo JrTexto::_('Google chrome extension'); ?></a> <a id="isFirefox" href="https://www.webrtc-experiment.com/store/firefox-extension/enable-screen-capturing.xpi" target="_blank"><?php echo JrTexto::_('Mozilla firefox extension'); ?></a>
					    </p>
					</div>
					<div class="clearfix"></div>
				</div>	    	
	    	</div>
	    </div>
   	</div>
   	<div class="panel panel-primary" id="vinfoconexion" style="display: none;" >
	    <div class="panel-headding bg-green text-center" style="overflow: hidden;">
	    	<h4><?php echo ucfirst(JrTexto::_('User information')) ?></h4>
	    </div>
	    <div class="panel-body" style="overflow: hidden;">
	    	<div class="col-xs-12 col-sm-6 col-md-6 text-center">	
	    	<div class="alert alert-info">
	    		Sr(a). se recomienda probar  su microfono, sus parlantes o auriculares para no tener problemas al conectarse 
	    	</div>  
	    	<div class="row">
		        	<div class='col-xs-12 col-sm-12 col-md-6'>
		        	<button class="btn btnprobarmicro btn-danger" style="width: 100%"><?php echo JrTexto::_('Probar mi microfono'); ?> <span class="fa fa-microphone"></span></button><br>
		        	<select id="selmicro<?php echo $idgui;?>" class="form-control"></select>
		        	</div>
		        	<div class='col-xs-12 col-sm-12 col-md-6'>
		        	<button class="btn btnprobaraudio btn-danger" style="width: 100%"><?php echo JrTexto::_('Probar mi Audio'); ?> <span class="fa fa-volume-up"></span></button><br>
		        	<select id="selaudio<?php echo $idgui;?>" class="form-control"></select>
		        	<audio id="audio<?php echo $idgui;?>" style="display: none" src="<?php echo $this->documento->getUrlStatic()."/media/aulasvirtuales/audios/alert.mp3"; ?>"></audio>
		        	</div>
		        	<div class='col-xs-12 col-sm-12 col-md-12 text-center'><br><br>
		        		<button class="btn btndispositivoslista btn-primary"><?php echo JrTexto::_('Actualizar dispositivos'); ?> <span class="fa fa-refresh"></span></button>
		        	</div>
		        </div>  		
	    	</div>
	    	<div class="col-xs-12 col-sm-6 col-md-6">
	    		<div class="alert alert-info">
	    			Sr(a). registre su usuario y el correo con el que fue invitado  para poder conectarse. 
	    		</div>
	    		<div class="form-group">		
			        <div class='input-group' >
			        	<span class="input-group-addon btn" style="width:70px"><span class="fa fa-user"></span></span>
			            <input type='text' class="form-control " placeholder="<?php echo JrTexto::_('Enter your username'); ?>" name="txtnombre" id="txtnombre" value="<?php echo ucfirst($usuarioAct["usuario"]); ?>"/>           
			        </div>
			        <div class='input-group' >
			        	<span class="input-group-addon btn" style="width:70px"><span class="fa fa-envelope-o"></span></span>
			            <input type='mail' class="form-control " placeholder="<?php echo JrTexto::_('Enter your Email');?>" name="txtemail" id="txtemail" value="<?php echo $usuarioAct["email"]; ?>" />           
			        </div>
			        <div class="clearfix"></div>	
			        <hr>
			        <div class="col-xs-12 col-sm-12 col-md-12" style=" text-align: center;">
			        <button class="btnunirsala btn btn-sm btn-primary disabled"><span><?php echo JrTexto::_("Smartclass open"); ?></span> <i class="fa fa-sign-in"></i> </button>
			        </div>	        
			    </div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<div class="alert alert-info" id="infosala"  role="alert" style="display: none;">
				<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-info-circle" aria-hidden="true"></i></div>
				<div class="col-xs-8 col-sm-8 col-md-9">
				<h4 class="alert-heading"> <?php echo JrTexto::_("Information") ?> ! </h4>
				    <p class="mb-0">Smartclass <?php echo JrTexto::_("Moderator is not connected to the room"); ?> <br>				   
				    </p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="alert alert-danger" id="infosalaerror"  role="alert" style="display: none;">
				<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-close" aria-hidden="true"></i></div>
				<div class="col-xs-8 col-sm-8 col-md-9">
				<h4 class="alert-heading"> <?php echo JrTexto::_("Error") ?> ! </h4>
				    <p class="mb-0">Smartclass <?php echo JrTexto::_("Moderator is not connected to the room"); ?> <br>				   
				    </p>
				</div>
				<div class="clearfix"></div>
			</div>  	
	    </div>
   	</div>
</div>
<form id="frmaula" method="post" action="<?php echo $this->documento->getUrlBase(); ?>/aulavirtual/ver/" style="display: none;">
	<input type="hidden" name="id" id="frmidaula">
	<input type="hidden" name="user" id="frmuser">
	<input type="hidden" name="email" id="frmemail">
	<input type="hidden" name="tipo" id="frmtipo">
</form>
<script type="text/javascript">
	var selmicro=document.getElementById('selmicro<?php echo $idgui;?>');
	var selaudio=document.getElementById('selaudio<?php echo $idgui;?>');
	var audio=document.getElementById('audio<?php echo $idgui;?>');
	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
	var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
function gotDevices(deviceInfos){
  $('option',selmicro).remove();
  $('option',selaudio).remove();
  for (var i = 0; i !== deviceInfos.length; ++i){
    var deviceInfo = deviceInfos[i];
    var option = document.createElement('option');
    option.value = deviceInfo.deviceId;
    if(deviceInfo.kind === 'audioinput') {
      option.text = deviceInfo.label || 'microphone ' + (selmicro.length + 1);
      selmicro.appendChild(option);
    }else if (deviceInfo.kind === 'audiooutput'){
      option.text = deviceInfo.label || 'speaker ' + (selaudio.length + 1);
      selaudio.appendChild(option);
    }
  }
  selaudio.addEventListener('change',cambiaraudio);
}
function handleError(error) {
  console.log('navigator.getUserMedia error: ', error);
}
navigator.mediaDevices.enumerateDevices().then(gotDevices).catch(handleError);

function attachSinkId(sinkId){	
  if (typeof audio.sinkId !== 'undefined') {
    audio.setSinkId(sinkId).then(function(){
    	audio.play(); 
    	sessionStorage.setItem('audioOutId', sinkId);
    }).catch(function(error){
      var errorMessage = error;
      if (error.name === 'SecurityError') {
        errorMessage = 'You need to use HTTPS for selecting audio output device: ' + error;
      }
      console.error(errorMessage);      
      //audioOutputSelect.selectedIndex = 0;
    });
  } else {
    console.warn('Browser does not support output device selection.');
  }
}
function cambiaraudio(ev){
	var deviceId = event.target.value;
  	var outputSelector = event.target;
	attachSinkId(deviceId);
}


$(document).ready(function(){
	function validarcontroles(){	  
       DetectRTC.load(function(){
       	var _navegadorok=_haymicro=_haywebcam=_hayaudio=_hayScreen=false;
       	if(DetectRTC.isWebRTCSupported === true){
       		$('#validarnavegador').hide();
		    _navegadorok=true;
		}
        if(DetectRTC.hasMicrophone === true) { // enable microphone 
            $('#validarmicrofono').hide();
            _haymicro=true;
        }
        if(DetectRTC.hasWebcam === true){// enable camera   
           $('#validarcamara').hide();          
            _haywebcam=true;
        }

        if (DetectRTC.hasSpeakers === true ||DetectRTC.browser.isFirefox) { // checking for "false"
             $('#validaraudio').hide();
         	_hayaudio=true;
        }


        if(DetectRTC.isScreenCapturingSupported){
            hayScreen=true;           
        }
        var comprobarrecursos=function(_hayScreen){
        	if(_navegadorok&&_haymicro&&_haywebcam&&_hayaudio&&_hayScreen){
        		$('#vrequisitos').hide();
        	}
        	if(_navegadorok&&_haymicro&&_hayaudio){
        		var errorlogin='<?php echo $errorlogin;?>';
        		if(!errorlogin)$('#vinfoconexion').show('fast');
        	}
        }

        getChromeExtensionStatus(function(status){
            var _hayScreen=false;
            if(status==='installed-enabled'){
                _hayScreen=true;
                $('#validarcompartirpantalla').hide();
            }else{ 
                if(DetectRTC.browser.isFirefox){
                    $('#validarcompartirpantalla #ischrome').hide();
                }else{
                    $('#validarcompartirpantalla #isFirefox').hide();
                } 
            } 
            comprobarrecursos(_hayScreen);         
        });     
    });    
}
validarcontroles();
var verificarusuario=function(){	
	var email=$('#txtemail').val();
	var users=$('#txtnombre').val();
	if(!$('.btnunirsala').hasClass('disabled')) $('.btnunirsala').addClass('disabled').attr('disabled','disabled');
	if(email==''||users=='') return;
	if(!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email))	return false;
	sessionStorage.setItem("emailsmartclass", email);
	sessionStorage.setItem("userssmartclass", users);
	var txtemails=[{email:email,usuario:users}];
	var formData = new FormData();
        formData.append("usuario", JSON.stringify(txtemails));
        formData.append("idaula", '<?php echo @$aula["aulaid"]; ?>');
	$.ajax({
        url: _sysUrlBase_+'/aulavirtual/json_verificarusuario/',
        type: "POST",
        data:  formData,
        contentType: false,
        dataType :'json',
        cache: false,
        processData:false,            
        success: function(data)
        {  
            if(data.code==='ok'){
	            var info=data.msj;
	            $('#infosalaerror').hide('fast');
	            if(info.como==="M"){
	            	$('#infosala').hide('fast');
	            	$('.btnunirsala').find('span').text('<?php echo JrTexto::_("Smartclass open"); ?>');	            	
	            }else if(info.estado!=="AB"){
	            	$('#infosala').show('fast');	            	
	            	if(info.estado==='0'){
	            		$('#infosala').removeClass('alert-danger').addClass('alert-warning');
	            		$('#infosala .alert-heading').text(' <?php echo JrTexto::_("Warning") ?> ! ');
	            		$('#infosala .icon1').removeClass('fa-close').addClass('fa-exclamation-triangle');
	            		$('#infosala .mb-0').text('Smartclass <?php echo JrTexto::_("Moderator is not connected to the room"); ?> ');
	            	}else if(info.estado==='CL'){
	            		$('#infosala').removeClass('alert-warning').addClass('alert-danger');
	            		$('#infosala .mb-0').text('Smartclass <?php echo JrTexto::_("close room"); ?> ');
	            		$('#infosala .alert-heading').text(' <?php echo JrTexto::_("Error") ?> ! ');
	            		$('#infosala .icon1').addClass('fa-close').removeClass('fa-exclamation-triangle');
	            	}else if(info.estado==='BL'){
	            		$('#infosala').removeClass('alert-warning').addClass('alert-danger');
	            		$('#infosala .mb-0').text('Smartclass <?php echo JrTexto::_("lock room"); ?> ');
	            		$('#infosala .alert-heading').text(' <?php echo JrTexto::_("Error") ?> ! ');
	            		$('#infosala .icon1').addClass('fa-close').removeClass('fa-exclamation-triangle');
	            	}
	            	$('.btnunirsala').find('span').text('<?php echo JrTexto::_("Smartclass Join"); ?>');
	            }else{
	            	$('#infosala').hide('fast');
	            	$('.btnunirsala').find('span').text('<?php echo JrTexto::_("Smartclass Join"); ?>');
	            }
	            $('.btnunirsala').removeClass('disabled').removeAttr('disabled').attr('data-tipoconex',info.como);
	            sessionStorage.setItem("tiposmartclass", info.como);
	            $('#frmidaula').val('<?php echo @$aula["aulaid"]; ?>')
	            $('#frmemail').val(email);
	            $('#frmuser').val(users);
	            $('#frmtipo').val(info.como);
            }else{
				$('#infosalaerror .mb-0').text(data.msj);            	
            	$('#infosalaerror').show('fast');
            	if(!$('.btnunirsala').hasClass('disabled')) $('.btnunirsala').addClass('disabled').attr('disabled','disabled');
            	setTimeout(function(){verificarusuario()},5000);
            }           
            return false;
        },error: function(xhr,status,error){
          	console.log(xhr.responseText,status,error);
          	setTimeout(function(){verificarusuario()},5000);
            return false;
        }
    }).always(function() {
        //$('.btnsendemail').removeAttr('disabled');
    });
}
$('.btnverificar').click(function(){
	validarcontroles();
});
$('#txtnombre').blur(function(){if($(this).val().length>3) verificarusuario();});
$('#txtemail').keyup(function(){ if($(this).val().length>3) verificarusuario();});
$('#txtemail').blur(function(){if($(this).val().length>3) verificarusuario();});
$('.btnunirsala').click(function(){
	var tipouser=$(this).attr('data-tipoconex')||'';
	//if(tipouser=='')return;
	$('#frmaula').submit();
});

var mediaRecorder;
var chunks=[];
var streamtest;
$('.btnprobarmicro').click(function(ev){
	var btn=$(this);
	if(!btn.hasClass('grabando')){
	DetectRTC.load(function(){		
	 	if (DetectRTC.hasMicrophone === true){
	 		chunks=[];
	 		var audioSource=selaudio.value;
	 		sessionStorage.setItem('audioInId', audioSource);
	 		var constraints = {audio: {deviceId: audioSource ? {exact: audioSource} : undefined}, video:false};
	 		navigator.mediaDevices.getUserMedia({audio:true,video:false}).then(function(stream){
				streamtest=stream;				
				mediaRecorder = new MediaRecorder(stream);
				mediaRecorder.onstop = function(e){
					blob = new Blob(chunks, { 'type' : 'audio/wav' });
					var tmpurl=URL.createObjectURL(blob);
					audio.src=tmpurl;
					audio.play();
					if(streamtest) streamtest.getTracks()[0].stop();
		  		}
		  		mediaRecorder.ondataavailable = function(e) { chunks.push(e.data);}
		  		mediaRecorder.start();
		  		btn.addClass('grabando');		  		
		  	});
	 		btn.removeClass('btn-danger').addClass('btn-success');
	 	}else{
	 		btn.removeClass('grabando');
	 		btn.removeClass('btn-danger').addClass('btn-warning');
	 		alert('microfono no detectado');
	 	}
	})
	}else{
		btn.removeClass('grabando');
		mediaRecorder.stop();
	}	
});

$('.btnprobaraudio').click(function(ev){
	var btn=$(this);
	var $audio=$('audio#audio<?php echo $idgui;?>');
	 DetectRTC.load(function(){
	 	console.log(DetectRTC.hasSpeakers);
	 	if (DetectRTC.hasSpeakers === true){
	 		$audio[0].play();
	 		btn.removeClass('btn-danger').addClass('btn-success');
	 	}else{
	 		btn.removeClass('btn-danger').addClass('btn-warning');
	 	}
	});
});

$('.btndispositivoslista').click(function(ev){
	navigator.mediaDevices.enumerateDevices().then(gotDevices).catch(handleError);
});
	$('#txtnombre').focus();
	verificarusuario();	
});

</script>