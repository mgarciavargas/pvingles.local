<?php 
	$idgui=uniqid(); 
	$curaula=$this->aula;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/aulavirtual/css/ver.css">
<style type="text/css">
	.jconfirm-box{background: #c1f9b0 !important}
	.jconfirm-box .title-c{ text-align: center; }
	.jconfirm-box .content-pane{ text-align: center; }
	.jconfirm-box .buttons{ float: none !important; text-align: center; }
</style>
<div id="pantalla1">
	 	<div class="panel ventanas" id="vparticipantes" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Participants')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">				 
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body" style="overflow: auto; ">
				<div id="userclone" style="display: none;">
					<li id="idtmp" class="dropdown liuser" >
		                <a href="#" class="dropdown-toggle miestatus" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cubes"></i><span class="caret"></span></a>
		                <ul class="liestatus dropdown-menu ">
		                  <li><a class="userstatus modeU" data-mode="U" href="#"><i class="fa fa-user"></i> <?php echo ucfirst(JrTexto::_('Participant')); ?></a></li>
		                  <li><a class="userstatus modeP" data-mode="P" href="#"><i class="fa fa-users"></i> <?php echo ucfirst(JrTexto::_('Presenter')); ?></a></li>
		                  <li><a class="userstatus modeM" data-mode="M" href="#"><i class="fa fa-cubes"></i> <?php echo ucfirst(JrTexto::_('Moderator')); ?></a></li>
		                </ul>
		     			<span class="nombre"></span>
		     			<span class="sysinfouser"></span>
		     			<span class="userchataccion">
			     		<div class="btn-group tipoM">			     				
			     			<button type="button" class="btn btn-default btn-xs infomano btnmano" title="<?php echo JrTexto::_('Requesting a microphone'); ?>" style="display: none;" ><i class="fa fa-thumbs-up animated infinite"></i></button>	
			     			<button type="button" class="btn btn-default btn-xs infollamada btnmicroprivado" title="<?php echo JrTexto::_('Enable and disable call private'); ?>"><i class="fa fa-phone animated infinite"></i></button>
			     			<i class="infocamera fa fa-camera colorgreen" style="display: none; padding: 0.25ex;"></i>
					 		<i class="infoscreen fa fa-desktop colorgreen" style="display: none; padding: 0.25ex;"></i>												
							<ul class="sysmenu btn btn-xs btn-micro oneuser">
								<li>
									<i class="vericon fa fa-microphone-slash animated infinite"></i> <span class="caret"></span>
									<ul>
										<li class="btnmicroactive"><i class="fa fa-microphone iconactive"></i> <?php echo ucfirst(JrTexto::_('On')); ?></li>
										<li class="btnmicroinactive"><i class="fa fa-microphone-slash iconinactive"></i> <?php echo ucfirst(JrTexto::_('Off')); ?></li>
										<!--li class="btnmicropermitir"><i class="fa fa-microphone"></i> <?php //echo ucfirst(JrTexto::_('Allow to use')); ?></li>
										<li class="btnmicronopermitir"><i class="fa fa-microphone-slash"></i> <?php //echo ucfirst(JrTexto::_('do not allow to use')); ?></li-->
									</ul>
								</li>
							</ul>
							<!--button type="button" class="btn btn-default btn-xs btnmicro" title="Activar/desactivar microfono"><i class="fa fa-microphone-slash animated infinite"></i></button-->
					 	</div>
					 	<div class="btn-group tipoU">
					 		<i class="infomano fa fa-thumbs-up colorgreen" style="display: none; padding: 0.25ex;"></i>
					 		<i class="infollamada fa fa-microphone colorgreen" style="display: none; padding: 0.25ex;"></i>
					 		<i class="infomicro fa fa-microphone colorgreen" style="display: none; padding: 0.25ex;"></i>
					 		<i class="infocamera fa fa-camera colorgreen" style="display: none; padding: 0.25ex;"></i>
					 		<i class="infoscreen fa fa-desktop colorgreen" style="display: none; padding: 0.25ex;"></i>
					 	</div>
					 	</span>
					 </li>
					<li class="lilinea"> <hr class="midivider"></li>
				</div>
				<ul id="usersparticipante" >	
					<li class="linea"> <hr class="midivider"></li>
					<li id="totaluser" ><?php echo ucfirst(JrTexto::_('Total users')); ?> (<b>0</b>)<span class="userchataccion">
					<!--ul class="sysmenu btn btn-xs btn-micro alluser" >
						<li>
							<i class="vericon fa fa-microphone-slash animated infinite"></i> <span class="caret"></span>
							<ul>
								<li class="btnmicroactive"><i class="fa fa-microphone iconactive"></i> <?php //echo ucfirst(JrTexto::_('On')); ?></li>
								<li class="btnmicroinactive"><i class="fa fa-microphone-slash iconinactive"></i> <?php //echo ucfirst(JrTexto::_('Off')); ?></li>
								<li class="btnmicropermitir"><i class="fa fa-microphone"></i> <?php //echo ucfirst(JrTexto::_('Allow to use')); ?></li>
								<li class="btnmicronopermitir"><i class="fa fa-microphone-slash"></i> <?php //echo ucfirst(JrTexto::_('do not allow to use')); ?></li>
							</ul>
						</li>
					</ul-->					
					</li>
				</ul>
			</div>
		</div>

		<div class="panel ventanas" id="vchat"  >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Chat')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">				 
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
				<div class="clearfix"></div>
				<audio id="audioenviadochat" src='' style="display: none;"></audio>
			</div>
			<div class="panel-body">
			    <div class="chatAll" style="position: absolute;width: 100%;height: 100%; padding: 1ex;">                                                
                    <div class="mensajes text-center" style="overflow: auto;" >
                      <div class="text-center"><?php echo ucfirst(JrTexto::_('Welcome to chat')); ?><br><span class="userid"></span></div>
                    </div>
                    <div class="newmensaje text-center" style="position: absolute;bottom: 0px;height: 100px; width: 98%;  overflow: auto;">
                      <textarea id="txtnewmensaje" placeholder="<?php echo ucfirst(JrTexto::_('Write your message')); ?>" style="width: 98%; height:60px;"></textarea>
		            </div> 		
				</div>
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px 1px;">
				<?php require("iconos.php"); ?>
				<span class="btn btn-sm btnchatmicro"><i class="fa fa-microphone-slash"></i></span>
				<span class="btn btn-sm btnchatfile"><i class="fa fa-file"></i></span>
				<div class="grupo-opciones pull-right">
					<div class="pull-right" style="padding-right: 1em;"><?php echo ucfirst(JrTexto::_('To')); ?> :
	                      <select id="userchatsms" >
	                      	<option value="alluser"><span class="fa fa-users"></span> <?php echo ucfirst(JrTexto::_('All users')); ?></a></option>
	                      </select>
                      </div>
				</div>
			</div>
		</div>
		
		<div class="panel ventanas" id="vinformation">
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Information')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">				 
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
				<div class="clearfix"></div>
			</div>

			<div class="panel-body">
				<div class="infosmartclass">
					<div class="row">
						<h1 class="col-xs-12 text-center"> <?php echo ucfirst(JrTexto::_('Welcome to')); ?> Smartclass</h1>
						<h3 class="text-center"><?php echo $curaula["titulo"]; ?></h3>
						<p class="text-center"><?php echo $curaula["descripcion"]; ?></p>

						<div class="col-xs-12 botones-compartir">
							<h4 class="text-center"><?php echo ucfirst(JrTexto::_('What do you want to share')); ?>?</h4>
							<div class="text-center">
								<button class="btn btn-default btncompartir btnsharepdfinboard istooltip" title="<?php echo ucfirst(JrTexto::_('share pdf')); ?>"><i class="color-danger fa fa-file-pdf-o fa-3x animated infinite"></i></button>
								<button class="btn btn-default btncompartir btnsharedesktop istooltip" title="<?php echo ucfirst(JrTexto::_('share desktop')); ?>"><i class="color-info fa fa-desktop fa-3x animated infinite"></i></button>
								<button class="btn btn-default btncompartir btnsharecamera istooltip" title="<?php echo ucfirst(JrTexto::_('Share webcam')); ?>"><i class="color-primary fa fa-camera fa-3x animated infinite"></i></button>
							</div>
							<div class="text-center">
								<button class="btn btn-default btncompartir btnsharevideo istooltip" title="<?php echo ucfirst(JrTexto::_('Share Video')); ?>"><i class="color-danger fa fa-video-camera fa-3x animated infinite"></i></button>
								<button class="btn btn-default btncompartir btnsharefile istooltip" title="<?php echo ucfirst(JrTexto::_('Upload and share files')); ?>"><i class="color-info fa fa-files-o fa-3x animated infinite"></i></button>
								
							</div>					
						</div>
					</div>
				</div>
				<div id="medias-container" style="display: none;">
			   		
			   	</div>

			</div>

			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px;">
				<?php require("iconos.php"); ?>
				<div class="grupo-opciones">
					<button class="btn btn-xs btn-default btnherramienta btnshareventana istooltip" title="<?php echo ucfirst(JrTexto::_('Share')); ?>"><i class="fa fa-share"></i><span class="sr-only"><?php echo ucfirst(JrTexto::_('Share')); ?></span></button>
					<button class="btn btn-xs btn-default btnherramienta btnfullscreen istooltip" title="<?php echo ucfirst(JrTexto::_('Full Screen')); ?>"><i class="fa fa-window-maximize"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_('Full Screen')); ?></span></button>
				</div>
			</div>
		</div>
		
		<div class="panel ventanas" id="vnotas" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Notes')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">				 
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body">			
			   	<textarea id="vtxtnotes"></textarea>			   	
			</div>
		</div>

		<div id="configaudio" class="modal fade" role="dialog">
		  	<div class="modal-dialog">
		    <!-- Modal content-->
		    	<div class="modal-content">
		    	<!--div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Modal Header</h4></div-->
		    		<div class="modal-body">
		    			<!--div class="alert alert-info">
				    		Sr(a). se recomienda probar  su microfono, sus parlantes o auriculares para no tener problemas al conectarse 
				    	</div-->
			        	<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6 text-right" style="padding: 1.3ex 1ex;">
								<?php echo JrTexto::_('Microphone'); ?> <span class="fa fa-microphone"></span> : <button title="<?php echo JrTexto::_('Record'); ?>" id="btnconfigmicro" class=" btn btn-warning" style="width: 100px;"><?php echo JrTexto::_('Record'); ?></button>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<select id="selconfigmicro" class="form-control"></select>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6 text-right" style="padding: 1.3ex 1ex;">
								<?php echo JrTexto::_('Audio Out'); ?> <span class="fa fa-volume-up"></span> : <button id="btnconfigaudio" class="btn btn-warning" style="width: 100px;"><?php echo JrTexto::_('Listen'); ?></button>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<select id="selconfigaudio" class="form-control"></select>
							</div>
							
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6 text-right" style="padding: 1.3ex 1ex;">
								<?php echo JrTexto::_('Web Camera'); ?>	<span class="fa fa-camera"></span> :  <button id="btnconfigcamera" class="btn btn-warning"  style="width: 100px;"><?php echo JrTexto::_('See'); ?></button>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<select id="selconfigwebcam" class="form-control"></select>
							</div>
							<div id="paneltest271086" class="text-center">
								<audio id="testaudioconfig" style="display:none;" controls></audio>
								<video id="testvideoconfig" style="display:none" width="200px" height="200px;" controls></video>
							</div>							
						</div>
					</div>
					<div class="modal-footer" style="text-align: center;">
						<button type="button" class="btn btnupdatelistdevice btn-primary"> <i class="fa fa-refresh"></i> <?php echo JrTexto::_('Update list Device') ?> </button>
						<button type="button" class="btn btnaceptarconfigaudio btn-success"> <i class="fa fa-check"></i> <?php echo JrTexto::_('Accept') ?> </button>
				    	<button type="button" class="btn btnacerrarconfigaudio btn-default btn-danger" data-dismiss="modal"> <i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?> </button>
				    </div>
				</div>
			</div>
		</div>

		<div class="panel ventanas" id="vpizarra" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Board')); ?> :
				<span style="position: relative;">
				    <span class="emitpizarra" style="display: none;">Yo</span>
					<select id="userspizarra" style="display: none; background: transparent; border:0px;"></select>
				</span>
				</h3>	
					<span class="pull-right btn-group btn-group-sm">
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize"></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body" style="border: 1.2ex solid #af7010; min-height: 80%;">			
			   	<canvas id="micanvas"></canvas>
			   	<canvas id="micanvaspdf" style="display: none"></canvas>
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px; border-top: 1.2ex solid #af7010;">
				<div class="grupo-opciones">
					<div class="btn-group btn-group-sm" role="group">					  	
					  	<a href="#" class="btn btn-default istooltip btnherramienta btnshareventana" title="<?php echo ucfirst(JrTexto::_('Share')); ?>" ><i class="fa fa-share"></i></a> 
						<a href="#" class="btn btn-default istooltip btnfullscreen istooltip" title="<?php echo ucfirst(JrTexto::_('Full Screen'));?>"><i class="fa fa-desktop"></i></a>	
					</div>
				</div>				
				<div class="grupo-opciones herramienta-dibujo">
					<div class="btn-group btn-group-sm" role="group">
						<a href="#" class="btn btn-default istooltip toolcolorfondocanvas" data-color="" style="overflow: hidden; position: relative; color:#fff" title="<?php echo ucfirst(JrTexto::_('color fondo')); ?>" >
				  			<i id="idsyscolorback" class="fa fa-square jscolor {valueElement:null,value:'#ffffff',closable:true,closeText:'Cerrar',onFineChange:'updatecolor(this)'}"  style="background-image: none; background-color: rgb(255, 255, 255); color: rgb(255, 255, 255); font-size: 1.5em;"></i>
				  			
				  		</a>
						<a href="#" class="btn btn-default istooltip toolcolorline" data-color="" style="overflow: hidden; position: relative;" title="<?php echo ucfirst(JrTexto::_('color Line / border color')); ?>" >
				  			<i id="idsyscolorline" class="fa fa-circle jscolor {valueElement:null,value:'#000000',closable:true,closeText:'Cerrar',onFineChange:'updatecolor(this)'}" style=" background-image: none; color: rgb(0, 0, 0);  background-color: rgb(0, 0, 0); font-size: 1.5em;"></i>				  			
				  		</a> 
						<a href="#" class="btn btn-default istooltip toolcolorfill" data-color="" title="<?php echo ucfirst(JrTexto::_('Fill color'));?>"><i id="idsyscolorfill" class="fa fa-circle-o jscolor {valueElement:null,value:'#000000',closable:true,closeText:'Cerrar',onFineChange:'updatecolor(this)'}" style="background-image: none; color: rgb(0, 0, 0); font-size: 1.5em;  background-color: rgb(0, 0, 0); "></i>
						</a>				  		
				  	</div>
				  	<div class="btn-group btn-group-sm" role="group">
				  		<a href="#" class="btn btn-default istooltip toolpuntero" title="<?php echo ucfirst(JrTexto::_('Puntero')); ?>" ><i class="fa fa-arrow-up"></i></a>
				  		<a href="#" class="btn btn-default istooltip toollapiz" title="<?php echo ucfirst(JrTexto::_('Pencil')); ?>" ><i class="fa fa-pencil"></i></a> 
						<a href="#" class="btn btn-default istooltip toolresaltador" title="<?php echo ucfirst(JrTexto::_('Marker pen'));?>"><i class="fa fa-paint-brush"></i></a>
						<a href="#" class="btn btn-default istooltip toollinea" title="<?php echo ucfirst(JrTexto::_('Line')); ?>" ><i class="fa fa-minus"></i></a>
						<a href="#" class="btn btn-default istooltip toolsquare" title="<?php echo ucfirst(JrTexto::_('Square')); ?>" ><i class="fa fa-square-o"></i></a>
						<a href="#" class="btn btn-default istooltip toolcircle" title="<?php echo ucfirst(JrTexto::_('Circle')); ?>" ><i class="fa fa-circle-o"></i></a>
						<a href="#" class="btn btn-default istooltip toolborrador" title="<?php echo ucfirst(JrTexto::_('Eraser')); ?>" ><i class="fa fa-eraser"></i></a>
						<a href="#" class="btn btn-default istooltip toolborrador2" title="<?php echo ucfirst(JrTexto::_('Eraser All')); ?>" ><i class="fa fa-eraser"></i><i class="fa fa-eraser"></i></a>
				  	</div> 
				  	<div class="btn-group btn-group-sm" role="group">
					  <a class="btn btn-default istooltip tooltext" title="<?php echo ucfirst(JrTexto::_('Text')); ?>" ><i class="fa fa-text-width"></i><i class=""></i></a>
					  <div class="btn-group btn-group-sm dropup tooltextfont">
					    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					    <i class="fa fa-font"></i> <span class="caret"></span></button>
					    <ul class="dropdown-menu" role="menu" style="height: 150px; overflow: auto;">
					      <li><a href="#">Arial</a></li>
					      <li><a href="#">Arial Black</a></li>
					      <li><a href="#">Bodoni</a></li>
					      <li><a href="#">Book Antiqua</a></li>
					      <li><a href="#">Courier New</a></li>
					      <li><a href="#">Comic</a></li>
					      <li><a href="#">Garamond</a></li>
					      <li><a href="#">Georgia</a></li>
					      <li><a href="#">Helvetica</a></li>
					      <li><a href="#">Impact</a></li>
					      <li><a href="#">Lucida Console</a></li>
					      <li><a href="#">Lucida Sans Unicode</a></li>
					      <li><a href="#">Palatino Linotype</a></li>
					      <li><a href="#">Tahoma</a></li>
					      <li><a href="#">Times new Roman</a></li>
					      <li><a href="#">Trajan</a></li>
					      <li><a href="#">Verdana</a></li>				      
					    </ul>
					  </div>
					  <a class="btn btn-default istooltip toolimage" title="<?php echo ucfirst(JrTexto::_('Image')); ?>" ><i class="fa fa-image"></i></a>
					  <a class="btn btn-default istooltip toolpdf btnsharepdfinboard" title="<?php echo ucfirst(JrTexto::_('File PDF')); ?>" ><i class="fa fa-file-pdf-o"></i></a>
					  <a class="btn btn-default istooltip tooldownload" title="<?php echo ucfirst(JrTexto::_('download image')); ?>" ><i class="fa fa-download"></i></a>
				  	</div>
				</div>
				<div class="grupo-opciones pull-right pdffileok" style="display: none;" >
					<div class="btninpdf btn-group btn-group-sm" role="group">					  	
					  	<a href="#" class="btn btn-default istooltip" id="pdf-prev" title="<?php echo ucfirst(JrTexto::_('Previous Page'));?>" ><i class="fa fa-chevron-circle-left"></i> <?php echo ucfirst(JrTexto::_('Previous Page')); ?></a> 
						<a href="#" class="btn btn-default istooltip istooltip" id="pdf-next" title="<?php echo ucfirst(JrTexto::_('Next Page')); ?>"><?php echo ucfirst(JrTexto::_('Next Page')); ?> <i class="fa fa-chevron-circle-right"></i></a>
						<a href="#" class="btn btn-default"><span id="page-count-container">Page <span id="pdf-current-page"></span> of <span id="pdf-total-pages"></span></span></a>
					</div>
				</div>
			</div>
		</div>
		<div class="panel ventanas" id="vvideos" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Shared Videos')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body">
			<div class="addvideoaqui"></div>		
			   
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px;">
				<div class="grupo-opciones">
					<button class="btn btn-xs btn-default btnsharevideo istooltip" title="<?php echo ucfirst(JrTexto::_('Upload Video')); ?>"><i class="fa fa-upload"></i><span class="sr-only"><?php echo ucfirst(JrTexto::_('Upload Video')); ?></span></button>
				</div>
			</div>
		</div>
		<div class="panel ventanas" id="vaudios" style="display: none" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Audios de Participantes')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body" id="container-audios" style="padding: 1ex;">
			</div>			
		</div>

		<div class="panel ventanas" id="vemiting" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Meeting')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  		 
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body">			
			   	<!--div id="medias-container">
			   		
			   	</div-->
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px;">
				<?php require("iconos.php"); ?>
				<div class="grupo-opciones">
					<button class="btn btn-xs btn-default btnherramienta btnshareventana istooltip" title="<?php echo ucfirst(JrTexto::_('Share')); ?>"><i class="fa fa-share"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_('Share')); ?></span></button>
					<button class="btn btn-xs btn-default btnherramienta btnfullscreen istooltip" title="<?php echo ucfirst(JrTexto::_('Full Screen')); ?>"><i class="fa fa-window-maximize"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_('Full Screen')); ?></span></button>
				</div>
			</div>
		</div>

		<div class="panel ventanas" id="vfiles" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Shared Files')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body">			
			   	<table class="table table-striped">			   		
			   			<tr id="addfileshareclone" >
			   				<td class="nombreuser"><?php echo JrTexto::_("User") ?></td>
			   				<td class="nombrefile"><?php echo JrTexto::_("Name File") ?></td>
			   				<td class="extension"><?php echo JrTexto::_("File Extension") ?></td>
			   				<td class="link"><a href="#"><?php echo ucfirst(JrTexto::_('Download')); ?></a></td>
			   			</tr>			   						   		
			   	</table>
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px;">
				<div class="grupo-opciones">
					<button class="btn btn-xs btn-default btnsharefile istooltip" title="<?php echo ucfirst(JrTexto::_('Upload file')); ?>"><i class="fa fa-upload"></i><span class="sr-only"><?php echo ucfirst(JrTexto::_('Upload file')); ?></span></button>
				</div>
			</div>
		</div>

		<div class="panel ventanas" id="vwebcam" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Webcam')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body" id="container-webcams" style="padding: 1ex;">				
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px;">
				<div class="grupo-opciones">
					<button class="btn btn-xs btn-default btnherramienta btnshareventana istooltip" title="<?php echo ucfirst(JrTexto::_('Share')); ?>"><i class="fa fa-share"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_('Share')); ?></span></button>
					<button class="btn btn-xs btn-default btnherramienta btnfullscreen istooltip" title="<?php echo ucfirst(JrTexto::_('Full Screen')); ?>"><i class="fa fa-window-maximize"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_('Full Screen')); ?></span></button>
				</div>
			</div>
		</div>

		<div class="panel ventanas" id="vencuesta" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Encuesta')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body" style="padding: 1ex" >
				<div class="form-group">    
	                <label><?php echo ucfirst(JrTexto::_("Question"))?> :</label>
	                <input type="text" name="pregunta" id="pregunta" value="Pregunta" class="form-control" placeholder="¿Tu Pregunta ?">
	            </div> 
	            <div class="form-group">
	            <label><?php echo ucfirst(JrTexto::_("Alternatives"))?> :</label>
	            	<span class="btn btn-xs fa fa-plus-circle colorgreen" id="encuesta_addalternativa" style=" font-size: 1.5em !important;"> </span>
		            <table style="width: 100%">
		            	<tr class="no" id="encuesta_clonealternativa" style="display: none">
		            	<td style="padding: 1ex"><input type="text" name="alternative" id="alternative" value="<?php echo JrTexto::_('No') ?>" class="form-control" placeholder="alternativa"></td>
		            	<td style="text-align: center;"><span class="btn btn-xs fa fa-trash colored encuesta_removealternativa" style=" font-size: 1.2em !important;"> </span> </td>
		            	</tr>
		            	<tr class="si">
		            	<td style="padding: 1ex"><input type="text" name="alternative" id="alternative" value="<?php echo JrTexto::_('Yes') ?>" class="form-control" placeholder="alternativa"></td>
		            	<td style="text-align: center;"><span class="btn btn-xs fa fa-trash colored encuesta_removealternativa" style=" font-size: 1.2em !important;"> </span> </td>
		            	</tr>
		            	<tr class="si">
		            	<td style="padding: 1ex"><input type="text" name="alternative" id="alternative" value="<?php echo JrTexto::_('No') ?>" class="form-control" placeholder="alternativa"></td>
		            	<td style="text-align: center;"><span class="btn btn-xs fa fa-trash colored encuesta_removealternativa" style=" font-size: 1.2em !important;"> </span> </td>
		            	</tr>
		            </table>	                
	            </div>
	            <hr>
	            <div class="text-center">
	            	<button class="btn btn-primary btn_encuesta_enviar"><?php echo JrTexto::_('Send Question') ?></button>	            	
	            </div>
            	<div class="resultadoencuesta"><hr></div>
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="modalquestion" >
			  <div class="modal-dialog modal-sm" role="document">
			    <div class="modal-content">
			      <div class="modal-header">        
			        <h4 class="modal-title" ><?php echo ucfirst(JrTexto::_('Window Question')); ?></h4>
			      </div>
			      <div class="modal-body text-center">        
			        <strong class="aquipregunta text-center">¿ pregunta?</strong>
			        <div class="graficodemela"></div>
			      </div>
			      <div class="modal-footer aquialternativas" style="text-align:center !important;">        
			        <a href="">Alternativa</a>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		<!--div class="panel ventanas" id="vlink" style="display: none;">
			<div class="panel-heading bg-blue">	
			<h3></h3>				
					<span class="pull-right btn-group btn-group-sm">
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body" style="padding: 1ex" >
				
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="modalquestion" >
			  <div class="modal-dialog modal-sm" role="document">
			    <div class="modal-content">
			      <div class="modal-header">        
			        <h4 class="modal-title" ><?php //echo ucfirst(JrTexto::_('Window Question')); ?></h4>
			      </div>
			      <div class="modal-body text-center">        
			        <strong class="aquipregunta text-center">¿ pregunta?</strong>
			        <div class="graficodemela"></div>
			      </div>
			      <div class="modal-footer aquialternativas" style="text-align:center !important;">        
			        <a href="">Alternativa</a>
			      </div>
			    </div>
			  </div>
			</div>
		</div-->
</div>
<audio src="" style="display: none" id="audiosonidos"></audio>
<div id="divprecargafile" style="display: none; position: absolute; bottom: 2ex; right: 20%; width: 300px; height: 150px; background: #fff;">
  <div style="position: absolute; bottom:0px; width:100%">
    <div class="text-center"><img width="30px" src="<?php echo $this->documento->getUrlStatic() ?>/sysplantillas/default.gif"></div>
    <div class="progress" style="margin-bottom: 0px;">  
      <div class="progress-bar progress-bar-striped progress-bar-animated focus active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:70%">
        <?php echo JrTexto::_('loading').'...'; ?><span>70%</span>
      </div>
    </div>
  </div>
</div>
<div id="infousersala" style="display: none;">
	<span class="_salaid"><?php echo $curaula["aulaid"];?></span>
	<span class="_sala">room<?php echo date("Ymd").$curaula["dni"].$curaula["aulaid"];?>c</span>
	<span class="_user"><?php echo @str_replace(" ","_",$this->userssmart);?></span>
	<span class="_dnimoderador"><?php echo $curaula["dni"];?></span>
	<span class="_typeuser"><?php echo $this->tiposmart;?></span>
	<span class="_username"><?php echo ucfirst($this->userssmart);?></span>
	<span class="_useremail"><?php echo $this->emailsmart;?></span>
	<span id="callprivatetitle"><?php echo JrTexto::_('Calling'); ?> : </span>
	<span id="callprivatecontent"><?php echo JrTexto::_('You want to accept the call'); ?></span>
	<span id="callprivatebtnok"><?php echo JrTexto::_('Accept'); ?></span>
	<span id="callprivatebtncancel"><?php echo JrTexto::_('Refuse'); ?></span>
</div>
<script type="text/javascript">
	var selmicro=document.getElementById('selconfigmicro');
	var selaudio=document.getElementById('selconfigaudio');
	var selwebcam=document.getElementById('selconfigwebcam');
	var testaudio=document.getElementById('testaudioconfig');
	var testvideo=document.getElementById('testvideoconfig');
	var _sysaudioout=false;
	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
	var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
	var mediaRecordertest='';
	function gotDevices(deviceInfos){
		var iselmicro= $('#selconfigmicro').val()||-1;
		var iselaudio= $('#selconfigaudio').val()||-1;
		var iselwebcam= $('#selconfigwebcam').val()||-1;
		var indexmicro=indexaudio=indexwebcam=0;
		var imicro=iaudio=iwebcam=0;
		$('option',selmicro).remove();
		$('option',selaudio).remove();
		$('option',selwebcam).remove();		
		for (var i = 0; i !== deviceInfos.length; ++i){
	    	var deviceInfo = deviceInfos[i];	    		    	
	    	var option = document.createElement('option');
	    	option.value = deviceInfo.deviceId;
	    	if(deviceInfo.kind === 'audioinput'){	    		
	    		if(deviceInfo.deviceId===iselmicro)indexmicro=imicro;
	      		option.text = deviceInfo.label || 'Microphone ' + (selmicro.length + 1);
	      		selmicro.appendChild(option);
	      		imicro++;
	    	}else if (deviceInfo.kind === 'audiooutput'){
	    		if(deviceInfo.deviceId===iselaudio)indexaudio=iaudio;
	      		option.text = deviceInfo.label || 'Speaker ' + (selaudio.length + 1);
	      		selaudio.appendChild(option);
	      		iaudio++;
	    	}else if (deviceInfo.kind === 'videoinput'){
	    		if(deviceInfo.deviceId===iselwebcam)indexwebcam=iwebcam;
	      		option.text = deviceInfo.label || 'Camera ' + (selwebcam.length + 1);
	      		selwebcam.appendChild(option);
	      		iwebcam++;
	    	}
		}
		selmicro.selectedIndex=indexmicro;
		selaudio.selectedIndex=indexaudio;
		selwebcam.selectedIndex=indexwebcam;

		//selmicro.addEventListener('change',cambiarmicro);
		selmicro.addEventListener('change',cambiartestmicro);
		selaudio.addEventListener('change',cambiartestaudio);
		selwebcam.addEventListener('change',cambiartestwebcam);
	}

	function attachSinkId(medio,sinkId){
	  if (typeof medio.sinkId !== 'undefined'){
	    medio.setSinkId(sinkId).then(function(){	    
	    }).catch(function(error){
	      var errorMessage = error;
	      if (error.name === 'SecurityError') {
	        errorMessage = 'You need to use HTTPS for selecting audio output device: ' + error;
	      }
	      console.error(errorMessage);      
	    });
	  } else {
	    console.warn('Browser does not support output device selection.');
	  }
	}
	function handleError(error){console.log('navigator.getUserMedia error: ', error);}
	navigator.mediaDevices.enumerateDevices().then(gotDevices).catch(handleError);
	function cambiartestmicro(){

	}

	function cambiartestaudio(){
		sinkId=selaudio.value;
		if(testaudio)attachSinkId(testaudio,sinkId);
		if(testvideo)attachSinkId(testvideo,sinkId);
	}

	function cambiartestwebcam(){

	}

	$('.btnupdatelistdevice').click(function(){
		navigator.mediaDevices.enumerateDevices().then(gotDevices).catch(handleError);
	});

	$('.btnaceptarconfigaudio').click(function(){
		$(this).closest('.modal').modal('hide');
		try{
		if(streamtest) streamtest.getTracks()[0].stop();
		testvideo.pause();
		testaudio.pause();
		sessionStorage.setItem('audioInId', selmicro.value||true);
		sessionStorage.setItem('audioOutId', selaudio.value||false);
		sessionStorage.setItem('videoInId', selwebcam.value||true);
		$('#testaudioconfig').hide();
		$('#testvideoconfig').hide();
		
		sinkId=selaudio.value||false;
		_sysaudioout=sinkId;
		if(sinkId!=false){
		var audios=$('audio');	
		$.each(audios,function(i,v){
			var audioid=$(this).attr('id')||('audioidtmp'+i);
			if(audioid=='audioidtmp'+i) $(this).attr('id',audioid);
			var audio=document.getElementById(audioid);
			attachSinkId(testaudio,sinkId);
		});
		var videos=$('videos');
		$.each(videos,function(i,v){
			var videoid=$(this).attr('id')||('videoidtmp'+i);
			if(videoid=='videoidtmp'+i) $(this).attr('id',videoid);
			var video=document.getElementById(videoid);
			attachSinkId(videoid,sinkId);
		});
		}
		}catch(ex){console.log(ex);}
	});

	$('.btnacerrarconfigaudio').click(function(){
		if(streamtest) streamtest.getTracks()[0].stop();
		$('#testaudioconfig').hide();
		$('#testvideoconfig').hide();
	});
	var streamtest;
	var testxcont=10;
	function stopmicrotest(){
		testxcont--;
		if(testxcont>0){
			$('#btnconfigmicro').text(testxcont);			
			setTimeout(function(){stopmicrotest()},1000);
		}else{
			console.log(mediaRecordertest.state);
			if(mediaRecordertest.state!='inactive')
			mediaRecordertest.stop();
		}
	}
	$('#btnconfigmicro').click(function(ev){
		btn=$(this);
		btn.toggleClass('active');
		if(btn.hasClass('active')){
		console.log('aqui1');
		chunks=[];
 		var audioSource=selmicro.value;
 		var constraints = {audio: {deviceId: audioSource ? {exact: audioSource} : undefined}, video:false};
 		navigator.mediaDevices.getUserMedia(constraints).then(function(stream){
			streamtest=stream;				
			mediaRecordertest = new MediaRecorder(stream);
			mediaRecordertest.onstop = function(e){
				$('#testaudioconfig').show();
				$('#testvideoconfig').hide();
				blob = new Blob(chunks, { 'type' : 'audio/wav' });				
				var tmpurl=URL.createObjectURL(blob);
				testaudio.src=tmpurl;
				testaudio.controls=true;
				testaudio.play();
				if(streamtest) streamtest.getTracks()[0].stop();
				btn.text(btn.attr('title'));				
	  		}
	  		mediaRecordertest.ondataavailable = function(e) { chunks.push(e.data);}
	  		mediaRecordertest.start();
	  		testxcont=10;	  		
	  		stopmicrotest();
	  	});
 		}else{
 			testxcont=0;
 			stopmicrotest(0);
 		}
	});

	$('#btnconfigaudio').click(function(ev){
		$('#testaudioconfig').show();
		$('#testvideoconfig').hide();	
		sinkId=selaudio.value;
		if(testaudio.src=='') testaudio.src='<?php echo $this->documento->getUrlStatic()."/media/aulasvirtuales/audios/alert.mp3"; ?>';
		attachSinkId(testaudio,sinkId);
		attachSinkId(testvideo,sinkId);
		testaudio.play();
	});

	$('#btnconfigcamera').click(function(ev){
		var audioSource=selmicro.value;
		var videoSource=selwebcam.value;
 		var constraints = {audio: {deviceId: audioSource ? {exact: audioSource} : undefined}, video:{deviceId: videoSource ? {exact: videoSource} : undefined}};
 		if(streamtest){streamtest.getTracks()[0].stop(); }
		navigator.mediaDevices.getUserMedia(constraints).then(function(stream){
			streamtest=stream;
			$('#testaudioconfig').hide();
			$('#testvideoconfig').remove();
			sinkId=selaudio.value;
			testvideo=document.createElement('video');
			testvideo.id='testvideoconfig';
			testvideo.srcObject=stream;
			testvideo.controls=true;
			testvideo.style.width='200px';
			testvideo.style.height='200px';
			attachSinkId(testvideo,sinkId);
			testvideo.play();
			$('#paneltest271086').append(testvideo);
			//if(streamtest) streamtest.getTracks()[0].stop();			
	  	});
	});

	function modificarsala(campo,valor){
		var formData = new FormData();
		  formData.append('id', '<?php echo $curaula["aulaid"];?>');
		  formData.append('campo', campo);
		  formData.append('valor', valor);  
		  var url=_sysUrlBase_+'/aulavirtual/addcampo';
		  $.ajax({
		    url: url,
		    type: "POST",
		    data:  formData,
		    contentType: false,
		    processData: false,
		    //dataType:'json',
		    cache: false,
		    beforeSend: function(XMLHttpRequest){ },      
		    success: function(data)
		    {     
		       console.log(data);     
		    },
		    error: function(e){ },
		    complete: function(xhr){ }
		  });
	}
</script>
<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic() ?>/libs/aulavirtual/ventanas.js?date=<?php echo date("his"); ?>"></script>
<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic() ?>/libs/aulavirtual/generales.js?date=<?php echo date("his"); ?>"></script>