<?php $idgui = uniqid(); ?>
<style type="text/css">
	.border0{
		border: 0px;
	}
	.input-group-addon{
		border-radius: 0.2ex;
	}
	.border1{
		margin-top:0px; 
		border:2px solid #4683af;
		border-radius: 0.25ex;
	}
  .input-group{
    margin: 5px 0px;
    border:2px solid #4683af;
    border-radius: 5px;
  }
  .form-group{
    margin-bottom: 0px; 
  }
  select.conestilo{
    border: 0px;
  }
  hr{margin: 4px;}
</style>
<div class="row">
	<div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo RUTA_WEB ;?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <li class="active"><?php echo JrTexto::_('Smartclass'); ?></li>
        </ol>
	</div>
</div>

<div class="row">
  <div class="col-xs-12 col-sm-4 col-md-4">          
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon" style="width: 100px"><b><?php echo ucfirst(JrTexto::_("Directed to"))?> :</b></div>
        <div class="cajaselect "> 
          <select name="dirigidoa" id="dirigidoa" class="conestilo">
              <option value=""><?php echo ucfirst(JrTexto::_("All"))?></option>
              <!--option value="A"><?php echo ucfirst(JrTexto::_("My students"))?></option-->
              <option value="I"><?php echo ucfirst(JrTexto::_("My Guests"))?></option>
              <option value="P"><?php echo ucfirst(JrTexto::_("General public"))?></option>
          </select>
        </div>           
      </div>
    </div>          
  </div>
  <div class="col-xs-12 col-sm-4 col-md-4">          
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon"><b><?php echo ucfirst(JrTexto::_("State"))?> :</b></div>
        <div class="cajaselect "> 
          <select name="estado" id="estado" class="conestilo">
            <option value=""><?php echo ucfirst(JrTexto::_("All"))?></option>
            <option value="0"><?php echo ucfirst(JrTexto::_("Active"))?></option>
            <option value="AB"><?php echo ucfirst(JrTexto::_("Opened"))?></option>
            <option value="CL"><?php echo ucfirst(JrTexto::_("Closed"))?></option>
            <option value="BL"><?php echo ucfirst(JrTexto::_("Locked"))?></option>
          </select>
        </div>           
      </div>
    </div>          
  </div>
  <div class="col-xs-6 col-sm-4 col-md-4">
    <div class="form-group">    
          <div class='input-group date datetimepicker1 border1' >
            <span class="input-group-addon btn"><b><?php echo  JrTexto::_("Date first")?> :</b></span>
              <input type='text' class="form-control border0" name="fecha_inicio" id="fecha_inicio" />
              <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span> </span>        
          </div>
      </div>  
  </div>  
</div>
<div class="row pnlestudiantes">
  <div class="col-xs-6 col-sm-4 col-md-4">
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon" style="width: 100px;"><b><?php echo ucfirst(JrTexto::_("Level"))?> :</b></div>
        <div class="cajaselect">
        		<select name="nivel" id="level-item" class="conestilo">
             	<option value="" <?php echo ""==@$this->idnivel?'Selected':'';?> ><?php echo ucfirst(JrTexto::_("All Levels"))?></option>
             	<?php if(!empty($this->niveles))
                     foreach ($this->niveles as $nivel){?>
                     <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $nivel["idnivel"]==$this->idnivel?'Selected':'';?>><?php echo $nivel["nombre"]?></option>
              	<?php }?>
            	</select>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-6 col-sm-4 col-md-4">
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon"><b><?php echo ucfirst(JrTexto::_("Unit"))?> :</b></div>
      	<div class="cajaselect"> 
      	<select name="unidad" id="unit-item" class="conestilo">
        	<option value="" <?php echo ""==@$this->idunidad?'Selected':'';?> ><?php echo ucfirst(JrTexto::_("All Units"))?></option>
        	<?php if(!empty($this->unidades))
                   foreach ($this->unidades as $unidad){?>
                   <option value="<?php echo $unidad["idnivel"]; ?>" <?php echo $unidad["idnivel"]==$this->idunidad?'Selected':'';?>><?php echo $unidad["nombre"]?></option>
            <?php }?>
      	</select>
      	</div>
      </div>
    </div>
  </div>
  <div class="col-xs-6 col-sm-4 col-md-4">
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon"><b><?php echo ucfirst(JrTexto::_("Activity"))?> :</b></div>
    	  <div class="cajaselect"> 
      	<select name="actividad" id="activity-item" class="conestilo">
          	<option value="" <?php echo ""==@$this->idactividad?'Selected':'';?> ><?php echo ucfirst(JrTexto::_("All Activities"))?></option>
          	 <?php if(!empty($this->actividades))
                     foreach ($this->actividades as $act){?>
                     <option value="<?php echo $act["idnivel"]; ?>" <?php echo $act["idnivel"]==$this->idactividad?'Selected':'';?>><?php echo $act["nombre"]?></option>
              <?php }?>
        </select>
  	    </div>
      </div>
    </div>
  </div>
</div>
<hr>
<div class="row">
<!--div class="col-xs-6 col-sm-4 col-md-3">
	<div class="form-group">
        <div class='input-group date datetimepicker2 border1'>
        	<span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span> <?php //echo  ucfirst(JrTexto::_("date finish"))?> </span>
            <input type='text' class="form-control border0" name="fecha_final" id="fecha_final" />           
        </div>
    </div>
</div-->
<div class="col-xs-12 col-sm-4 col-md-6">
	<div class="form-group border1">
	  <input type="text" name="titulo" id="titulo" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Title or class name"))?>">
	</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-6 text-center">         
      <a class="btn btn-success btn-sm btnbuscar" href="#">
      <?php echo ucfirst(JrTexto::_('Search')) ?> <i class="fa fa-search"></i></a>  
      <a class="btn btn-warning btn-sm" href="<?php echo JrAplicacion::getJrUrl(array("aula", "agregar"));?>">
      <i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('add new')) ?></a> 
  </div>
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="x_panel">        
        <div class="div_linea"></div>
         <div class="x_content table-responsive">
            <table class="table table-striped table-hover" style="width:100%;">
              <thead>
                <tr class="headings">
                  <th>#</th>                    
                    <th><?php echo JrTexto::_("title") ;?></th>
                    <th><?php echo JrTexto::_("Date") ;?></th>
                    <th><?php echo JrTexto::_("Video") ;?></th>
                    <th><?php echo JrTexto::_("State") ;?></th>                 
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php /*$i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ /*$i++; ?>
                <tr>
                  <td><?php echo $i;?></td>
                    <td><?php echo substr($reg["titulo"],0,100)."..."; ?></td>
                    <td><?php echo $reg["fecha_inicio"]." - ".$reg["fecha_final"] ;?></td>
                    <td></td>                   
                    <td><a class="btn btn-xs lis_ver " href="<?php echo JrAplicacion::getJrUrl(array('aulasvirtuales'))?>ver/?id=<?php echo $reg["aulaid"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update" href="<?php echo JrAplicacion::getJrUrl(array("aulasvirtuales", "editar", "id=" . $reg["aulaid"]))?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["aulaid"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php }*/ ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
    var currentaula=0;
    var curaula<?php echo $idgui; ?>=0;
    var participoantesguardar<?php echo $idgui; ?>=function(data){
      var emails=[];
      $.each(data,function(i,v){
        var item={email:v.email,name:v.name}
        emails.push(item);
      });
      var txtemails=JSON.stringify(emails);
      if(txtemails=='[]'){
        mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', '<?php echo JrTexto::_("There are no registered emails"); ?>', 'warning');
        return false;
      }
      if(curaula<?php echo $idgui; ?>===0){
       mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', '<?php echo JrTexto::_("Unregistered Smartclass"); ?>', 'warning');
        return false; 
      }
      var formData = new FormData();
      formData.append("paraemail", txtemails);
      formData.append("idaula", curaula<?php echo $idgui; ?>);
      $.ajax({
        url: _sysUrlBase_+'/Aulavirtualinvitados/json_guardar/',
        type: "POST",
        data:  formData,
        contentType: false,
        dataType :'json',
        cache: false,
        processData:false,            
        success: function(data)
        {                
            if(data.code==='ok'){
              mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'success');                     
            }else{
              mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'warning');
            }
            $('#procesando').hide('fast');
            $('.btnsendemail').removeAttr('disabled');
            return false;
        },error: function(xhr,status,error){
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', status, 'warning');
            $('#procesando').hide('fast');
            return false;
        }
    }).always(function() {
        //$('.btnsendemail').removeAttr('disabled');
    });

    };
   
    $(document).ready(function(){
        $('.istooltip').tooltip();
        var leerniveles=function(data){
            try{
                var res = xajax__('', 'niveles', 'getxPadre', data);
                if(res){ return res; }
                return false;
            }catch(error){
                return false;
            }       
        }
        var addniveles=function(data,obj){
          var objini=obj.find('option:first').clone();
          obj.find('option').remove();
          obj.append(objini);
          if(data!==false){
            var html='';
            $.each(data,function(i,v){
              html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
            });
            obj.append(html);
          }
          id=obj.attr('id');
          //if(id==='activity-item')  cargaraulaes();
        }

        $('#level-item').change(function(){
          var idnivel=$(this).val();
              var data={tipo:'U','idpadre':idnivel}
              var donde=$('#unit-item');
              if(idnivel!=='') addniveles(leerniveles(data),donde);
              else addniveles(false,donde);
              donde.trigger('change');
        });
        $('#unit-item').change(function(){
          var idunidad=$(this).val();
              var data={tipo:'L','idpadre':idunidad}
              var donde=$('#activity-item');
              if(idunidad!=='') addniveles(leerniveles(data),donde);
              else addniveles(false,donde);
              donde.trigger('change');
        });
        $('#dirigidoa').change(function(){
          if($('#dirigidoa').val()=='A') $('.pnlestudiantes').show();
          else {
            $('.pnlestudiantes').hide();
          }
        });
        $('#dirigidoa').trigger('change');

        $('#activity-item').change(function(ev){
          tabledatos.ajax.reload();
        });
        $('#estado').change(function(ev){
          ev.preventDefault();
          tabledatos.ajax.reload();
        });
        $('#fecha_inicio').change(function(ev){
          ev.preventDefault();
          tabledatos.ajax.reload();
        });

        $('#fecha_final').change(function(ev){
          ev.preventDefault();
          tabledatos.ajax.reload();
        });

        $('.btnbuscar').click(function(ev){
          ev.preventDefault();
          tabledatos.ajax.reload();
        });
        $('#titulo').change(function(ev){
          ev.preventDefault();
          if($(this).val().length>4){
            tabledatos.ajax.reload();
          }
        }).keyup(function(ev){
          if(ev.keyCode == 13) {
              tabledatos.ajax.reload();
          }
        });
        $('.datetimepicker1').datetimepicker({
            defaultDate: "<?php echo date ('m/d/Y h:00' , strtotime('-1 hour',strtotime(date('Y-m-d h:00')))); ?>",
            format: 'YYYY/MM/DD HH:mm'
        });
        $('.datetimepicker2').datetimepicker({
            defaultDate: "<?php echo date ('m/d/Y h:00', strtotime('+1 days',strtotime(date('Y-m-d h:00')))); ?>",
            format: 'YYYY/MM/DD HH:mm'           
        });

        var estados={'BL':'<?php echo JrTexto::_("Locked") ?>','AB':'<?php echo JrTexto::_("Opened") ?>','CL':'<?php echo JrTexto::_("Closed") ?>','0':'-'}
        var txtingresar='<?php echo JrTexto::_('Sign in') ?>';
        var txtusuarios='<?php echo JrTexto::_('Show').' '.JrTexto::_('participants') ?>';
        var txtinvitar='<?php echo JrTexto::_('Invite').' '.JrTexto::_('participants') ?>';
        var txtedit='<?php echo ucfirst(JrTexto::_('Edit')) ?> Smartclass';
        var txtremove='<?php echo JrTexto::_('Delete') ?> Smartclass';
        var txtingresaraula='<?php echo JrTexto::_('Start') ?> Smartclass';

        var draw=0;
        var tabledatos=$('.table').DataTable(
            { "searching": false,
              "processing": false,
              "serverSide": true,
              "columns" : [
                {'data': '#'},
                {'data': '<?php echo JrTexto::_("Title") ;?>'},
                {'data': '<?php echo JrTexto::_("Date") ;?>'},
                {'data': '<?php echo JrTexto::_("Video") ;?>'},
                {'data': '<?php echo JrTexto::_("State") ;?>'},
                {'data': '<?php echo JrTexto::_("Actions") ;?>'},
              ],
              "ajax":{
                url:_sysUrlBase_+'/?json=true',
                type: "post",                
                data:function(d){
                    d.json=true;
                    if($('#dirigidoa').val()=='A'){
                      d.nivel=$('#level-item').val();
                      d.unidad=$('#unit-item').val();
                      d.actividad=$('#activity-item').val();
                    }
                    d.estado=$('#estado').val();
                    d.fecha_inicio=$('#fecha_inicio').val();
                    d.fecha_final='';
                    d.titulo=$('#titulo').val();
                    draw=d.draw;
                },
                "dataSrc":function(json){
                  var data=json.data;
                  json.draw = draw;
                  json.recordsTotal = json.data.length;
                  json.recordsFiltered = json.data.length;
                  var datainfo = new Array();
                  for(var i=0;i< data.length; i++){
                    var veraula=' <a title="'+txtingresaraula+'" data-ventana="vaula" data-titulo="'+txtingresaraula+'" data-modal="no"  class="btn-showparticipantes istooltip btn btn-xs" href="'+_sysUrlBase_+'/aulavirtual/requisitos/?id='+data[i].aulaid+'"><i class="fa fa-drivers-license"></i></a>';
                    var showparticipantes=' <a title="'+txtusuarios+'" class="btn-showparticipantes istooltip btn btn-xs" data-modal="si" data-ventana="vparticipantes" data-titulo="'+txtusuarios+'" data-fcall="participoantesguardar<?php echo $idgui; ?>"  data-id="'+data[i].aulaid+'"   href="'+_sysUrlBase_+'/aulavirtualinvitados/mostrar/?id='+data[i].aulaid+'" data-id="'+data[i].aulaid+'"><i class="fa fa-users"></i></a>';
                    var invitarparticipantes=' <a title="'+txtinvitar+'" data-id="'+data[i].aulaid+'" class="istooltip btn btn-xs" href="'+_sysUrlBase_+'/aula/invitar/?idaula='+data[i].aulaid+'"><i class="fa fa-envelope-o"></i></a>';
                    datainfo.push({
                      '#':(i+1),
                      '<?php echo JrTexto::_("Title") ;?>': '<a class="istooltip" title="'+txtingresar+'" href="'+_sysUrlBase_+'/aulavirtual/requisitos/?id='+data[i].aulaid+'">'+data[i].titulo+'</a>',
                      '<?php echo JrTexto::_("Date") ;?>'  : data[i].fecha_inicio+' <br> '+data[i].fecha_final,
                      '<?php echo JrTexto::_("Video") ;?>'  :  '<a href="'+_sysUrlStatic_ +'/media/aulasvirtuales/'+data[i].video+'" target="_blank">'+data[i].video+'</a>',
                      '<?php echo JrTexto::_("State") ;?>'  :  estados[data[i].estado],
                      '<?php echo JrTexto::_("Actions") ;?>':veraula+showparticipantes+invitarparticipantes+'  | <a title="'+txtedit+'" class="istooltip btn btn-xs lis_update" href="'+_sysUrlBase_+'/aula/editar/?id='+data[i].aulaid+'"><i class="fa fa-pencil"></i></a><a title="'+txtremove+'" class="istooltip btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="'+data[i].aulaid+'" ><i class="fa fa-trash-o"></i>'
                    })
                  }

                  return datainfo }, error: function(d){console.log(d)}
              }
              <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
            });

        $('.table').on('click','.btn-eliminar',function(){
             var id=$(this).attr('data-id');
             $.confirm({
              title: '<?php echo JrTexto::_('Confirm action');?>',
              content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
              confirmButton: '<?php echo JrTexto::_('Accept');?>',
              cancelButton: '<?php echo JrTexto::_('Cancel');?>',
              confirmButtonClass: 'btn-success',
              cancelButtonClass: 'btn-danger',
              closeIcon: true,
              confirm: function(){             
                var res = xajax__('', 'aula', 'eliminar', parseInt(id));
                tabledatos.ajax.reload();
              }
            }); 
        }).on('click','.btn-showparticipantes',function(e){
            e.preventDefault();
            e.stopPropagation();
            curaula<?php echo $idgui; ?>=$(this).attr('data-id')||0; 
            console.log(curaula<?php echo $idgui; ?>);
            //var emails=obteneremails<?php //echo $idgui; ?>();      
            var enmodal=$(this).attr('data-modal')||'no';
            var fcall=$(this).attr('data-fcall')||'';
            var url=$(this).attr('href')
            if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
            else url+='?fcall='+fcall;
            var ventana=$(this).attr('data-ventana')||'Alumno';
            var claseid=ventana+'_<?php echo $idgui; ?>';
            $('.'+claseid).remove();
            var titulo=$(this).attr('data-titulo')||'';
            titulo=titulo.toString().replace('<br>',' ');     
            if(enmodal=='no'){
              return redir(url);
            }
            url+='&plt=modal';
            openModal('lg',titulo,url,ventana,claseid);
        });
    });
</script>