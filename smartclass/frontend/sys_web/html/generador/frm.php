<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$datoslistadoheader='';
$datoslistado='';
$addjschk=false;
$jsdate=false;
$jsdatetime=false;
$jstime=false;
$jsverchkeditor=false; 
$jsverwysihtml5 =false;
$jsvertinymce=false;
$jsfile=false;
$jschkformulario=false;
$jschkformulariomultiple=false;
$addimgsave=null;
$jsdatetxt=null;
$jsdatetimetxt=null;
?>
<?php
echo '<?php 
defined(\'RUTA_BASE\') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:\'eeeexzx-1\';
?>';
?>
<div class="row" style="<?php echo '<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>';?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <?php echo '<?php if($this->documento->plantilla!=\'modal\'){?>';?><div class="panel-heading bg-blue">
        <h3><?php echo "<?php echo JrTexto::_('".$tb."'); ?>";?><small id="frmaction"> <?php echo '<?php echo JrTexto::_($this->frmaccion);?>';?></small></h3>
        <div class="clearfix"></div>
      </div><?php echo '<?php } ?>';?>      
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo '<?php echo $id_vent;?>'; ?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pk<?php echo ucfirst($campopk); ?>" id="pk<?php echo $campopk; ?>" value="<?php echo '<?php echo JrTexto::_($this->pk);?>';?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo '<?php echo JrTexto::_($this->frmaccion);?>';?>">
          <?php if(!empty($campos))
          foreach ($campos as $campo){
            $Campo=ucfirst($campo);
            $verenlistado='_chk'.$campo;
            $tipo=@$frm['tipo_'.$campo];
            if($campo!=$campopk){
              //if($tipo!='fk'){
                if(@$frm["tipo2_".$campo]!='system'){ 
            ?><div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txt<?php echo $Campo; ?>">
              <?php echo "<?php echo JrTexto::_('".str_replace('_', ' ',$Campo)."');?>"; ?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <?php }
//////////////////////////////////////////////------------text -------------------
              if($tipo=='text'||$tipo=='double'||$tipo=='decimal'||$tipo=='money'){
              ?>  <input type="text"  id="txt<?php echo $Campo ?>" name="txt<?php echo $Campo ?>" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">
              <?php 

//////////////////////////////////////////////------------email -------------------
              }elseif($tipo=='email'){
              ?>  <input type="email" id="txt<?php echo $Campo ?>" name="txt<?php echo $Campo ?>" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">
              <?php 

//////////////////////////////////////////////------------date -------------------
              }elseif($tipo=='date'){
                    if($frm["tipo2_".$campo]=='user'){$jsdate=true;
                      $jsdatetxt.="
  $('#txt".$campo."').datetimepicker({
    lang:'es',
    timepicker:false,
    format:'Y/m/d'
  });";
              ?>  <input name="txt<?php echo $Campo ?>" id="txt<?php echo $Campo ?>" class="verdate form-control col-md-7 col-xs-12" required="required" type="date" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">   
              <?php }elseif($frm["tipo2_".$campo]=='system'){
              ?>  <input type="hidden" id="txt<?php echo $Campo ?>" name="txt<?php echo $Campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:date("Y/m/d") ?>' ?>">

              <?php } 

//////////////////////////////////////////////------------datetime -------------------
              }elseif($tipo=='datetime'){
                    if($frm["tipo2_".$campo]=='user'){$jsdatetime=true;
                      $jsdatetimetxt.="
  $('#txt".$campo."').datetimepicker({
    datepicker:false,
    format:'H:i',
    value:'12:00'
  });
  ";  
              ?>  <input name="txt<?php echo $Campo ?>" id="txt<?php echo $Campo ?>" class="verdatetime form-control col-md-7 col-xs-12" required="required" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">   
              <?php }elseif($frm["tipo2_".$campo]=='system'){
              ?>  <input type="hidden" id="txt<?php echo $Campo ?>" name="txt<?php echo $Campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:date("Y/m/d H:i:s"); ?>' ?>">

              <?php }

//////////////////////////////////////////////------------time -------------------
              }elseif($tipo=='time'){
                      if($frm["tipo2_".$campo]=='user'){$jstime=true;
                ?>  <input name="txt<?php echo $Campo ?>" id="txt<?php echo $Campo ?>" class="vertime form-control col-md-7 col-xs-12" required="required" type="date" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>">   
                <?php }elseif($frm["tipo2_".$campo]=='system'){
                ?>  <input type="hidden"  id="txt<?php echo $Campo ?>" name="txt<?php echo $Campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:date("H:i") ?>' ?>">

                <?php }

//////////////////////////////////////////////------------combobox -------------------
              }elseif($tipo=='combobox'){
              ?> <select id="txt<?php echo $campo ?>" name="txt<?php echo $Campo ?>" class="form-control col-md-7 col-xs-12" required>
              <option value=""><?php echo '<?php JrTexto::_("Seleccione");?>';?></option>
                <?php if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); ?>

              <option value=<?php echo $valoresopt[0]; ?>><?php echo "<?php echo JrTexto::_(".$valoresopt[0].");?>"; ?></option>
              <option value=<?php echo $valoresopt[1]; ?>><?php echo "<?php echo JrTexto::_(".$valoresopt[1].");?>"; ?></option>
                  <?php }else{ ?>
              <option value="1"><?php echo "<?php echo JrTexto::_('Activo');?>"; ?></option>
              <option value="0"><?php echo "<?php echo JrTexto::_('Inactivo');?>"; ?></option>
                  <?php } ?>
                <?php }else{ 
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ ?> 
              <option value="<?php echo $i;?>"> <?php echo "<?php echo JrTexto::_('".$i."');?> ".$i; ?></option>
              <?php } }
              ?> 
              </select>
              <?php  

//////////////////////////////////////////////------------checkbox -------------------
              }elseif($tipo=='checkbox'){
                $jschkformulario=true;
                if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); 
                  $jschkformulario=true;
              ?> 
              <a style="cursor:pointer;" class="chkformulario fa  <?php echo '<?php echo @$frm["'.$campo.'"]=='.$valoresopt[0].'?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>"
                data-valueno=<?php echo $valoresopt[1];?> data-value2="1">
                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]=='.$valoresopt[0].'?'.$valoresopt[0].':'.$valoresopt[1].');?>'; ?></span>
                 <input type="hidden" name="txt<?php echo $Campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>" > 
                 </a>
                  <?php }else{ ?>
                  <a style="cursor:pointer;" class="chkformulario fa  <?php echo '<?php echo @$frm["'.$campo.'"]==1?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>"
                data-valueno="0" data-value2="<?php echo '<?php echo @$frm["'.$campo.'"]==1?1:0;?>';?>">
                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]==1?"Active":"Inactive");?>'; ?></span>
                 <input type="hidden" name="txt<?php echo $Campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>';?>" > 
                 </a>
                  <?php } ?>
                <?php }else{ 
                     $jschkformulariomultiple=true;
                    if($frm["dattype".$campo]=="enum"){ 
                      $txtchk=str_replace("'", "", substr($frm["coltype".$campo], 5,-1));
                      $valoresopt=@explode(",",$txtchk); 
                      for($i=0;$i<count($valoresopt);$i++){                       
                       ?>

                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo $valoresopt[$i]; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("'.$valoresopt[$i].'");?>'; ?></span>                 
                 </a><br>
                  <?php    }
                      
                  }else{
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ 
              ?> 
                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo $i; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("Opcion - '.$i.'");?>'; ?></span>                 
                 </a><br>
              <?php 
                  }  
                  }?>
                  <input type="hidden" name="txt<?php echo $Campo ?>" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>" >
                  <?php
                }                    

//////////////////////////////////////////////------------radiobutton -------------------
              }elseif($tipo=='radiobutton'){                
                if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){ 
                  $jschkformulario=true;
                 if($frm["dattype".$campo]=="enum"){ 
                  $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1)); 
                  $jschkformulario=true;

              ?>  
                <a style="cursor:pointer;" class="chkformulario fa  <?php echo '<?php echo @$frm["'.$campo.'"]=='.$valoresopt[0].'?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>"  data-valueno=<?php echo $valoresopt[1];?> data-value2="<?php echo '<?php echo @$frm["'.$campo.'"]=='.$valoresopt[0].'?'.$valoresopt[1].':'.$valoresopt[0].';?>';?>">
                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]=='.$valoresopt[0].'?'.$valoresopt[0].':'.$valoresopt[1].');?>'; ?></span>
                 <input type="hidden" name="txt<?php echo $Campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>" > 
                 </a>
                  <?php }else{ ?>
                  <a style="cursor:pointer;" class="chkformulario fa  <?php echo '<?php echo @$frm["'.$campo.'"]==1?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo '<?php echo @$frm["'.$campo.'"];?>'; ?>"  data-valueno="0" data-value2="<?php echo '<?php echo @$frm["'.$campo.'"]==1?1:0;?>';?>">
                 <span> <?php echo '<?php echo JrTexto::_(@$frm["'.$campo.'"]==1?"Activo":"Inactivo");?>'; ?></span>
                 <input type="hidden" name="txt<?php echo $Campo ?>" value="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>" > 
                 </a>
                  <?php } ?>
                <?php }else{ 
                 $jschkformulariomultiple=true;
                    if($frm["dattype".$campo]=="enum"){ 
                      $txtchk=str_replace("'", "", substr($frm["coltype".$campo], 5,-1));
                      $valoresopt=@explode(",",$txtchk); 
                      for($i=0;$i<count($valoresopt);$i++){                       
                       ?>
  <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo $valoresopt[$i]; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("'.$valoresopt[$i].'");?>'; ?></span>
                 
                 </a><br>
                  <?php    }
                      
                  }else{
                for($i=1;$i<=$frm["tipo2_".$campo];$i++){ 
              ?> 
                <a style="cursor:pointer;" class="chkformulariomultiple fa  <?php echo '<?php echo @$frm["'.$campo.'"]=="'.$valoresopt[$i].'"?"fa-check-circle":"fa-circle-o";?>'; ?>" 
                data-value="<?php echo $i; ?>" >
                 <span> <?php echo '<?php echo JrTexto::_("Opcion - '.$i.'");?>'; ?></span>
                 
                 </a><br>
              <?php 
                  }  
                  }?>
                  <input type="hidden" name="txt<?php echo $Campo ?>" value="<?php echo !empty($frm["'.$campo.'"])?$frm["'.$campo.'"]:0;?>'; ?>" >
                  <?php } 
                   

//////////////////////////////////////////////------------Number -------------------
              }elseif($tipo=='int'||$tipo=='tinyint'||$tipo=='bigint'){
              ?> <input type="number" id="txt<?php echo $Campo ?>" name="txt<?php echo $Campo ?>" step="1" value="<?php echo '<?php echo !empty($frm[$Campo])?$frm[$Campo]:1;?>'; ?>"  min="1" class="form-control col-md-7 col-xs-12" required />
              <?php 

//////////////////////////////////////////////------------password -------------------
              }elseif($tipo=='password'){
                if($frm["tipo2_".$campo]=='simple'){
              ?> <input type="password" id="txt<?php echo $Campo ?>" name="txt<?php echo $Campo ?>" class="form-control" required />    
              <?php  }else{ 
              ?> <input type="password" id="txt<?php echo $Campo ?>" name="txt<?php echo $Campo ?>" class="form-control" required /> 
              <?php  }

//////////////////////////////////////////////------------file -------------------
              }elseif($tipo=='file'){
                $jsfile=true;
                if($frm["tipo2_".$campo]=='all'){
                  $tipo='all'; $txtfile=''; 
                ?> <a href="" data-type="all" class="img-thumbnail" id="id<?php echo $Campo ?>"> <?php echo "<?php echo JrTexto::_('ver');?>"; ?> </a>
                <?php }elseif($frm["tipo2_".$campo]=='imagen'){
                  $tipo='img';$txtfile='Imagen';
                ?> 
                <?php }elseif($frm["tipo2_".$campo]=='video'){
                  $tipo='vid';$txtfile='Video';
                ?> <video src=""  data-type="video" id="id<?php echo $Campo ?>"><?php echo "<?php echo JrTexto::_('Descargar video');?>"; ?> </video>
                <?php }elseif($frm["tipo2_".$campo]=='documentos'){
                  $tipo='doc';$txtfile='Documento';
                ?> <a href=""  data-type="archivo" class="img-thumbnail" id="id<?php echo $campo ?>"> <?php echo "<?php echo JrTexto::_('ver Archivo');?>"; ?></a>
                <?php }elseif($frm["tipo2_".$campo]=='audio'){
                  $tipo='aud';$txtfile='Audio';
                ?> <audio src=""  controls  data-type="audio" class="img-thumbnail" id="id<?php echo $campo ?>"><?php echo "<?php echo JrTexto::_('Descargar Audio');?>"; ?></audio>
                <?php }
                if($txtfile!='Imagen'){
              ?> <span class="btn btn-info btn-file"> <i class="fa fa-upload"></i> <?php echo "<?php echo JrTexto::_('Seleccionar ".$txtfile."');?>"; ?> 
                 <input data-texto="<?php echo $txtfile; ?>" data-campo="<?php echo $campo; ?>" data-action="<?php echo "<?php echo JrAplicacion::getJrUrl(array('".$this->tabla."', 'subir_archivo'));?>?arc=".$campo."&tipo=".$tipo; ?>" type="file" name="file<?php echo $Campo ?>" id="file<?php echo $Campo ?>" class="cargarfile" data-type="<?php  echo $tipo; ?>"></span>
                 <input type="hidden" id="txt<?php echo $Campo ?>" name="txt<?php echo $campo ?>" class="form-control" required />
                 <iframe name="if_cargar_file" style="display:none"></iframe>
              <?php }else{ 
                $addimgsave.='
                $("#txt'.$Campo.'").val($("img[alt=\'img'.$campo.'\']").cropper("getDataURL", "image/png", 1.0));
                ';
                ?>
                <div class="row">
                  <input type="hidden" name="txt<?php echo $Campo ?>" id="txt<?php echo $Campo ?>">
                  <input type="hidden" name="txt<?php echo $Campo ?>_old" value="<?php echo '<?php echo @$frm["'.$campo.'"];?>';?>">
                  <div class="col-md-12">
                    <div class="img-container">
                      <img src="<?php echo '<?php echo !empty($frm["'.$campo.'"])?$this->documento->getUrlBase().$frm["'.$campo.'"]:($this->documento->getUrlStatic()."/media/'.strtolower($tb).'/default.png");?>';?>" alt="img<?php echo $campo ?>" data-ancho="100"  data-alto="100">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 docs-buttons text-center">
                    <div class="btn-group">
                      <label class="btn btn-info btn-upload" for="inputImage<?php echo $campo; ?>" title="Upload image file">
                        <input class="sr-only btncropper-upload" alt="img<?php echo $campo;?>" id="inputImage<?php echo $campo; ?>" name="file" type="file" accept="image/*">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo '<?php echo JrTexto::_("Seleccionar imagen");?>'; ?>">
                        <span class="fa fa-upload"></span></span>
                      </label>
                      <button class="btn btn-primary btncropper" data-image="img<?php echo $campo;?>"  data-method="setDragMode" data-option="move" type="button">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo '<?php echo JrTexto::_("Mover imagen");?>'; ?>"><span class="fa fa-arrows"></span></span>
                      </button>
                      <button class="btn btn-primary btncropper" data-image="img<?php echo $campo;?>" data-method="zoom" data-option="0.1" type="button" >
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo '<?php echo JrTexto::_("Aumentar Zoom");?>';?>"><span class="fa fa-search-plus"></span></span>
                      </button>
                      <button class="btn btn-primary btncropper" data-image="img<?php echo $campo;?>" data-method="zoom" data-option="-0.1" type="button">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo '<?php echo JrTexto::_("Disminuir Zoom");?>';?>"><span class="fa fa-search-minus"></span></span>
                      </button>
                      <button class="btn btn-primary btncropper" data-image="img<?php echo $campo;?>" data-method="rotate" data-option="-45" type="button">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo '<?php echo JrTexto::_("Rotar a la Izquierda");?>';?>" ><span class="fa fa-rotate-left"></span></span>
                      </button>
                      <button class="btn btn-primary btncropper" data-image="img<?php echo $campo;?>" data-method="rotate" data-option="45" type="button">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo '<?php echo JrTexto::_("Rotar a la Derecha");?>';?>" ><span class="fa fa-rotate-right"></span></span>
                      </button>
                      <button class="btn btn-primary btncropper" data-image="img<?php echo $campo;?>" data-method="reset" type="button">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo '<?php echo JrTexto::_("Resetear Imagen");?>';?>"><span class="fa fa-refresh"></span></span>
                      </button>
                    </div>
                  </div>
                </div>

             <?php }

//////////////////////////////////////////////------------textArea -------------------
              }elseif($tipo=='textArea'){
                if($frm["tipo2_".$campo]=='simple'){
              ?> <textarea id="txt<?php echo $Campo ?>" name="txt<?php echo $Campo ?>" class="form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='wysihtml5'){
              $jsverwysihtml5 =true;
              ?> <textarea id="txt<?php echo $Campo ?>"name="txt<?php echo $Campo ?>" class="verwysihtml5 form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='chkeditor'){
              $jsverchkeditor=true; 
              $js.='CKEDITOR.replace("txt'.$campo.'");'
              ?> <textarea id="txt<?php echo $Campo ?>"name="txt<?php echo $Campo ?>" class="verchkeditor form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }elseif($frm["tipo2_".$campo]=='tinymce'){
              $jsvertinymce=true; 
              ?> <textarea id="txt<?php echo $Campo ?>"name="txt<?php echo $Campo ?>" class="vertinymce form-control" ><?php echo '<?php echo @trim($frm["'.$campo.'"]); ?>'; ?></textarea>
              <?php }

//////////////////////////////////////////////------------foreign key -------------------
              }elseif($tipo=='fk'){
                if($frm["tipofkcomo_".$campo]=='combobox'){
              ?><select id="txt<?php echo $campo ?>" name="txt<?php echo $Campo ?>" class="form-control">
               <option value=""><?php echo "<?php echo JrTexto::_('Seleccione'); ?>"; ?></option>
              <?php
              echo '<?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') { ?>';
              echo '<option value="<?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]?>" <?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]==@$frm["'.$campo.'"]?"selected":""; ?> ><?php echo $fk'.$campo.'["'.$frm["tipofkver_".$campo].'"] ?></option>';
              echo '<?php } ?>';
              ?>
              </select>
              <?php }elseif($frm["tipofkcomo_".$campo]=='radiobutton'){
                echo '
              <?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                 ?>';
              ?><input type="radio" id="txt<?php echo $campo ?>" name="txt<?php echo $Campo ?>" >
              <?php 
              echo ' <?php } ?>';
              }elseif($frm["tipofkcomo_".$campo]=='checkbox'){
                echo '
              <?php 
              if(!empty($this->fk'.$campo.'))
                foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                 ?>';
              ?><input type="checkbox" id="txt<?php echo $campo ?>" name="txt<?php echo $Campo ?>" class="form-control">
              <?php echo ' <?php } ?>';
              }elseif($frm["tipofkcomo_".$campo]=='text'){
              ?><input type="text" id="txt<?php echo $campo ?>" name="txt<?php echo $Campo ?>" class="form-control">
              <?php }elseif($frm["tipofkcomo_".$campo]=='listado'){
              ?><input type="button" id="txt<?php echo $campo ?>" name="txt<?php echo $Campo ?>" class="form-control listarfk">
              <?php }elseif($frm["tipofkcomo_".$campo]=='system'){
              ?><?php echo '<?php echo $this->fk'.$campo.'["usuario"];?>';?>
              <?php }

//////////////////////////////////////////////------------otraopcion -------------------
              }elseif($frm["tipofkcomo_".$campo]=='money'){?>
              <input type="text"  id="txt<?php echo $campo ?>" name="txt<?php echo $Campo ?>" class="form-control col-md-7 col-xs-12"
              value="<?php echo '<?php echo @$frm["txt'.$Campo.'"] ?>'?>">
              <?php }else{
              ?>   <input type="text"  id="txt<?php echo $campo ?>" name="txt<?php echo $Campo ?>" class="form-control col-md-7 col-xs-12" value="<?php echo '<?php echo @$frm["txt'.$Campo.'"] ?>'?>">
              <?php } if(@$frm["tipo2_".$campo]!='system'){ 
              ?>
                    
              </div>
            </div>

            <?php }
            }
          }?>

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-save<?php echo $tb ?>" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo "<?php echo JrTexto::_('Save');?>"; ?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo "<?php echo JrAplicacion::getJrUrl(array('".strtolower($tb)."'))?>"; ?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo "<?php echo JrTexto::_('Cancel');?>"; ?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
  <?php echo $jsdatetxt; ?>
  <?php echo $jsdatetimetxt; ?>
  <?php if($jsvertinymce==true){?>
    tinymce.init({
    selector: ".vertinymce",
     plugins: [
        "link image"
    ],
    toolbar: " bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image",
    language_url:"../../static/libs/tinymce/tinymce_idioma/es.js"
  });
  <?php } ?>
  <?php if($jsverchkeditor ==true){?>

  <?php } ?>
  <?php if($jsverwysihtml5==true){?>
    $(".verwysihtml5").wysihtml5();
  <?php } ?>
  <?php echo @$js; ?>

$('#frm-<?php echo '<?php echo $id_vent;?>'; ?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', '<?php echo strtolower($tb);?>', 'save<?php echo $tb?>', xajax.getFormValues('frm-<?php echo '<?php echo $id_vent;?>'; ?>'));
      if(res){
        if(typeof <?php echo '<?php echo $ventanapadre?>';?> == 'function'){
          <?php echo '<?php echo $ventanapadre?>';?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo '<?php echo JrAplicacion::getJrUrl(array("'.$tb.'"))?>';?>');
      }
     }
  });

 
  <?php if($jsfile==true){ ?>

  $('.cargarfile').on('change',function(){
    var file=$(this);
     agregar_msj_interno('success', '<?php echo "<?php echo JrTexto::_(\"loading\");?>";?> '+file.attr('data-texto')+'...','msj-interno',true);
     $('#frm<?php echo $tb;?>').attr('action',file.attr('data-action'));
     $('#frm<?php echo $tb;?>').attr('target','if_cargar_'+file.attr('data-campo'));
     $('#frm<?php echo $tb;?>').attr('enctype','multipart/form-data');
     $('#frm<?php echo $tb;?>').submit();                
  });
    <?php } ?>

});

<?php if($jschkformulariomultiple){?>
      $('.chkformulariomultiple').click(function(){
        var v1=$(this).attr('data-value');
        if($(this).hasClass("fa-circle-o")){
          $(this).removeClass('fa-circle-o').addClass(' fa-check-circle ');
        }else{
          $(this).removeClass('fa-check-circle').addClass(' fa-circle-o ');
        }

      });
<?php }?>
<?php if($jschkformulario){?>
$('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo "<?php echo JrTexto::_(\"Active\");?>"; ?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo "<?php echo JrTexto::_(\"Inactive\");?>"; ?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }
  });
<?php }?>
<?php if($jsfile==true){ ?>

function result_archivo(result, mensaje,txt) {
  if(result) {
    $("#txt"+txt).val(mensaje);
    agregar_msj_interno2("success", '<?php echo "<?php echo JrTexto::_(\"File updated successfully\");?>";?>',"msj-interno2",true);
    var tipo=$("#id"+txt).attr("data-type");
    if(tipo=="imagen"||tipo=="video"||tipo=="audio")
      $("#id"+txt).removeAttr("style").attr("src", "<?php echo $this->documento->getUrlBase()?>" + mensaje );
    else
      $("#id"+txt).removeAttr("style").attr("href",  "<?php echo $this->documento->getUrlBase()?>" + mensaje );
    }else{
      agregar_msj_interno2("warning", mensaje,"msj-interno2",true);
    }
}
  <?php } ?>

</script>

