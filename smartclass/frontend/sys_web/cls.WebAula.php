<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAulasvirtuales', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE, 'sys_negocio');
class WebAula extends JrWeb
{
	private $oNegAulasvirtuales;
	private $oNegAlumno;
	private $oNegPersonal;
	protected $oNegGrupos;
    protected $oNegLocal;
    protected $oNegNiveles;
    protected $oNegGrupo_matricula;
    private $oNegAulavirtualinvitados;
	public function __construct()
	{
		parent::__construct();
		$this->oNegNiveles = new NegNiveles;			
		$this->oNegAulasvirtuales = new NegAulasvirtuales;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegAlumno=new NegAlumno;
		$this->oNegGrupos = new NegGrupos;
        $this->oNegLocal = new NegLocal;
        $this->oNegGrupo_matricula = new NegGrupo_matricula;
        $this->oNegAulavirtualinvitados = new NegAulavirtualinvitados;
	}

	public function defecto(){
		global $aplicacion;			
		return $this->ver();		
	}

	public function invitar(){
		global $aplicacion;
		$this->documento->script('tinymce.min', '/libs/tinymce/');
		$this->documento->script('jquery-ui.min', '/tema/js/');
        $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
        $this->documento->script('tag-it.min', '/libs/tagit/');
        $this->documento->stylesheet('jquery.tagit', '/libs/tagit/');

		$this->usuarioAct = NegSesion::getUsuario();
		$this->personal=$this->oNegPersonal->buscar(array('dni'=>$this->usuarioAct["dni"]));
		$this->oNegAulasvirtuales->aulaid = @$_GET['idaula'];		
		$this->datos = $this->oNegAulasvirtuales->dataAulasvirtuales;
		if(!empty($this->datos)){
			if($this->datos["dirigidoa"]==='A'){
				$filtroestudiantes=$this->datos["filtroestudiantes"];
				$objclase=json_decode($filtroestudiantes);
				$filtros=array();
				if(!empty($objclase->colegio))
					if($objclase->colegio<=0)
						$this->alumnos=null;
					else{
						$filtros["iddocente"]=$this->usuarioAct["dni"];
						$filtros["idlocal"]=$objclase->colegio;
						if($objclase->aula>0) $filtros["idambiente"]=$objclase->aula;
						if($objclase->grupo>0) $filtros["idgrupo"]=$objclase->grupo;				
						if($objclase->alumnos>0) $filtros["idalumno"]=$objclase->alumnos;
						$this->alumnos=$this->oNegGrupo_matricula->buscar($filtros);
					}
				else $this->alumnos=null;
			}
			$this->alumnosinvitados=$this->oNegAulavirtualinvitados->buscar(array('idaula'=>@$this->datos["aulaid"],'como'=>'U'));	
		}
		$this->documento->plantilla ='aulavirtual/inicio';
		$this->esquema = 'aulavirtual/invitar';
		$this->documento->setTitulo(JrTexto::_('Aulasvirtuales'), true);
		return parent::getEsquema();
	}
	
	public function agregar()
	{
		try {
			global $aplicacion;
			$this->frmaccion='Nuevo';
			$this->unidades=$this->actividades=array();	
			$this->idunidad=$this->idactividad='';	
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$this->idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:'';
			if(!empty($this->idnivel)){				
				$this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
				$this->idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:'';
				if(!empty($this->idunidad)){
					$this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
					$this->idactividad=!empty($this->actividades[0]["idnivel"])?$this->actividades[0]["idnivel"]:'';
				}
			}
			$this->documento->setTitulo(JrTexto::_('Smartclass').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			$this->frmaccion='Editar';
			$this->oNegAulasvirtuales->aulaid = @$_GET['id'];
			$this->datos = $this->oNegAulasvirtuales->dataAulasvirtuales;
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$this->unidades=$this->actividades=array();	
			$this->idunidad=$this->idactividad='';
			$idnivel_=!empty($this->datos["idnivel"])?$this->datos["idnivel"]:'';
			$idunidad_=!empty($this->datos["idunidad"])?$this->datos["idunidad"]:'';
			$idactividad_=!empty($this->datos["idactividad"])?$this->datos["idactividad"]:'';

			$this->idnivel=!empty($idnivel_)?$idnivel_:(!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:'');
			if(!empty($this->idnivel)){				
				$this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
				$this->idunidad= !empty($idunidad_)?$idunidad_:(!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:'');
				if(!empty($this->idunidad)){
					$this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
					$this->idactividad=!empty($idactividad_)?$idactividad_:(!empty($this->actividades[0]["idnivel"])?$this->actividades[0]["idnivel"]:'');      
				}
			}
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo('Smartclass /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegAulasvirtuales->aulaid = @$_GET['id'];
			$this->datos = $this->oNegAulasvirtuales->dataAulasvirtuales;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('see'), true);
			$this->esquema = 'aulavirtual/aulasvirtuales-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->usuarioAct = NegSesion::getUsuario();
			$this->personal=$this->oNegPersonal->buscar(array('dni'=>$this->usuarioAct["dni"]));
			$this->documento->setTitulo(JrTexto::_('Edit'),true);
			$usuarioAct = NegSesion::getUsuario();
            $filtros = array();
            $filtros['iddocente'] = $usuarioAct["dni"];
            $this->locales=array();
            $arrLocales = array();
	       	$this->grupos=$this->oNegGrupos->buscar($filtros);
            foreach ($this->grupos as $g) {
                if(!in_array($g['idlocal'], $arrLocales)){
                    $this->oNegLocal->idlocal = $g['idlocal'];
                    $this->locales[]=$this->oNegLocal->getXid();
                    $arrLocales[]=$g['idlocal'];
                }
            }			
			$this->esquema = 'aulavirtual/formulario';
			$this->documento->plantilla ='aulavirtual/inicio';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function guardar()
	{
		$this->documento->plantilla = 'returnjson';
        try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incorrect')));
                exit(0);            
            }

            @extract($_POST);
            global $aplicacion;         
            $usuarioAct = NegSesion::getUsuario(); 
			if(!empty($idaula)){
				$this->oNegAulasvirtuales->aulaid = $idaula;
				$accion="editar";
			}
			$moderador=!empty($moderador)?$moderador:$usuarioAct["email"];
			$txtportada=@str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',$portada);
			$this->oNegAulasvirtuales->__set('idnivel',!empty($nivel)?$nivel:-1);
			$this->oNegAulasvirtuales->__set('idunidad',!empty($unidad)?$unidad:-1);
			$this->oNegAulasvirtuales->__set('idactividad',!empty($actividad)?$actividad:-1);
			$this->oNegAulasvirtuales->__set('fecha_inicio',@$fecha_inicio);
			$this->oNegAulasvirtuales->__set('fecha_final',@$fecha_final);
			$this->oNegAulasvirtuales->__set('titulo',@$titulo);
			$this->oNegAulasvirtuales->__set('descripcion',@$descripcion);
			$this->oNegAulasvirtuales->__set('moderadores',$moderador);
			$this->oNegAulasvirtuales->__set('estado',@$estado);
			$this->oNegAulasvirtuales->__set('video','');
			$this->oNegAulasvirtuales->__set('chat','');
			$this->oNegAulasvirtuales->__set('notas','');
			$this->oNegAulasvirtuales->__set('dirigidoa',@$dirigidoa);
			$this->oNegAulasvirtuales->__set('estudiantes','');
			$this->oNegAulasvirtuales->__set('dni',@$usuarioAct["dni"]);
			$this->oNegAulasvirtuales->__set('fecha_creado',date("Y/m/d H:i:s"));
			$this->oNegAulasvirtuales->__set('portada',@$txtportada);
			$this->oNegAulasvirtuales->__set("filtroestudiantes",@$filtroestudiantes);
			$this->oNegAulasvirtuales->__set("nparticipantes",@$nparticipantes);
		    if(@$accion=="editar"){
				$res=$this->oNegAulasvirtuales->editar();
			}else{
				$res=$this->oNegAulasvirtuales->agregar();
		    }		
		    $this->oNegAulavirtualinvitados->__set('idaula',$res);
			$this->oNegAulavirtualinvitados->__set('dni',@$usuarioAct["dni"]);
			$this->oNegAulavirtualinvitados->__set('email',@$usuarioAct["email"]);
			$this->oNegAulavirtualinvitados->__set('asistio','0');
			$this->oNegAulavirtualinvitados->__set('como','M');				
			$this->oNegAulavirtualinvitados->__set('usuario',@$usuarioAct["usuario"]);
			$this->oNegAulavirtualinvitados->agregar();
			if($moderador!=$usuarioAct["email"]){
				$this->oNegAulavirtualinvitados->__set('idaula',$res);
				$this->oNegAulavirtualinvitados->__set('dni',@$usuarioAct["dni"]);
				$this->oNegAulavirtualinvitados->__set('email',$moderador);
				$this->oNegAulavirtualinvitados->__set('asistio','0');
				$this->oNegAulavirtualinvitados->__set('como','M');				
				$this->oNegAulavirtualinvitados->__set('usuario',@$usuarioAct["usuario"]);
				$this->oNegAulavirtualinvitados->agregar();
			}
			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Smartclass')).' '.JrTexto::_('saved successfully'),'new'=>$res)); //
            exit(0);				
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		} 		
	}

	
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAulasvirtuales->__set('aulaid', $pk);
				$res=$this->oNegAulasvirtuales->eliminar();
				if(!empty($res)){
					$this->oNegAulavirtualinvitados->eliminarxfiltro(array('idaula'=>$pk));
					$oRespAjax->setReturnValue($res);
				}else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAulasvirtuales->__set('aulaid', $pk);
				$propiedad=@$args[1];
				$valor=@$args[2];
				$res=$this->oNegAulasvirtuales->setCampo($pk, $propiedad, $valor);
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('update Record')), 'warning');
					$oRespAjax->setReturnValue($res);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}	     
}