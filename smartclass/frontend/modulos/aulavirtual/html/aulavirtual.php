<style type="text/css">

  ul#menupersonalizado li a {
    color:#fff;
  } 
  ul#menupersonalizado li ul .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover{
    background: #1641ad;
  } 
  ul#menupersonalizado li ul {
    background: #4466bb;
  }

  ul#menupersonalizado .dropdown li a.active::after{
    content: "\f00c";
    font-family: FontAwesome;
    padding-right: 1em;
    position: absolute;
    right: 0;
  }

  #menupersonalizado>li {
    background-color: rgb(68, 102, 187);
    float: left;
    z-index: 1040 !important;
  }
  
  #menupersonalizado > li.istooltip a{
    font-size: 1.2em;
  }

  .btnsharemicro i.fa-microphone-slash{
    color:#fff !important;
  }

  @media (max-width: 600px) {
    .navbar-right.menutop1 {
      display: none;
    }
  }

</style>
<div id="myModalvideohelp" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Video help me</h4>
      </div>
      <div class="modal-body">
        <p><video id="videohelp" src="<?php echo $this->documento->getUrlStatic(); ?>/media/videomanual/smartclass.mp4" width="100%" controls></video></p>
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div-->
    </div>

  </div>
</div>
<script type="text/javascript">
  $("#myModalvideohelp").on('hidden.bs.modal', function(){      
      $('video#videohelp')[0].pause();
  });
</script>
<header>
    <div class="container-fluid">
      <nav class="navbar navbar-static-top">
        <div class="container-fluid">
          <div class="navbar-header">            
          </div>
          <ul class="nav navbar-nav" id="menupersonalizado">
           <li class="active"><a href="<?php echo $this->documento->getUrlSitio() ?>" target="_blank"><?php echo JrTexto::_('Home'); ?></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo JrTexto::_('Windows'); ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li ><a class="resetwindow" href="#"><?php echo ucfirst(JrTexto::_('Reset windows')); ?></a></li>
                  <li ><a class="showwindow init active" data-ventana="vinformation" href="#"><?php echo ucfirst(JrTexto::_('Information')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vemiting" href="#"><?php echo ucfirst(JrTexto::_('Emitting class')); ?></a></li>
                  <li ><a class="showwindow init active" data-ventana="vchat" href="#"><?php echo ucfirst(JrTexto::_('Chat')); ?></a></li>
                  <li ><a class="showwindow init active" data-ventana="vparticipantes" href="#"><?php echo ucfirst(JrTexto::_('Participants')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vpizarra" href="#"><?php echo ucfirst(JrTexto::_('Board')); ?></a></li>
                  <li style="display: none"><a class="showwindow" data-ventana="vinvite" href="#"><?php echo ucfirst(JrTexto::_('Invite users')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vfiles" href="#"><?php echo ucfirst(JrTexto::_('Shared files')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vnotas" href="#"><?php echo ucfirst(JrTexto::_('Notes')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vwebcam" href="#"><?php echo ucfirst(JrTexto::_('Webcam')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vencuesta" href="#"><?php echo ucfirst(JrTexto::_('Questions')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vvideos" href="#"><?php echo ucfirst(JrTexto::_('Videos')); ?></a></li>
                  <!--li ><a class="showwindow " data-ventana="vaudios" href="#"><?php echo ucfirst(JrTexto::_('Audios')); ?></a></li-->
                </ul>
            </li>
            <!--li class="dropdown menucompartir">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo JrTexto::_('Share'); ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li ><a class="btnsharedesktop" href="#"><?php echo ucfirst(JrTexto::_('Full Screen')); ?></a></li>
                  <li ><a class="btnsharefile" href="#"><?php echo ucfirst(JrTexto::_('File')); ?></a></li>                  
                </ul>
            </li-->
            <!--li class="dropdown menutools">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo JrTexto::_('Tools'); ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li ><a class="btnalzarmano" href="#"><?php echo ucfirst(JrTexto::_('Hand Raised')); ?></a></li>
                  <li ><a class="btnsharemicro" href="#"><?php echo ucfirst(JrTexto::_('Microphone')); ?></a></li>
                  <li ><a class="btnsharecamera" href="#"><?php echo ucfirst(JrTexto::_('Webcam')); ?></a></li>                  
                  <li ><a class="btnsharegrabar" href="#"><?php echo ucfirst(JrTexto::_('record the class')); ?></a></li>                  
                </ul>
              </li-->
              <li class="istooltip hvr-pulse-grow" title="<?php echo JrTexto::_('Raise your hand'); ?>"><a class="btnalzarmano" href="#"><i class="fa fa-hand-stop-o animated infinite"></i></a></li>
              <li class="istooltip hvr-pulse-grow" title="<?php echo JrTexto::_('Enable and disable Microphone'); ?>"><a class="btnsharemicro" href="#"><i class="fa fa-microphone-slash animated infinite"></i></a></li>
              <li class="istooltip hvr-pulse-grow" title="<?php echo JrTexto::_('Enable and disable Webcam'); ?>"><a class="btnsharecamera" href="#"><i class="fa fa-camera animated infinite"></i></a></li>              
              <li class="istooltip hvr-pulse-grow" title="<?php echo JrTexto::_('Enable and disable record class'); ?>"><a class="btnsharegrabar" href="#"><i class="glyphicon glyphicon-record animated infinite"></i></a></li>
              <li class="istooltip hvr-pulse-grow" title="<?php echo JrTexto::_('Enable and disable screen sharing'); ?>"><a class="btnsharedesktop" href="#"><i class="fa fa-desktop animated infinite"></i></a></li>
              <!--li class="istooltip hvr-pulse-grow" title="Activar/Desactivar Compartir archivo"><a class="btnsharefile" href="#"><i class="fa fa-shared-file"></i></a></li-->
              <li class="istooltip hvr-pulse-grow" title="<?php echo JrTexto::_('Open/look'); ?> Smartclass"><a class="btnlokedroom" href="#"><i class="fa fa-unlock"></i></a></li>
              <li class="istooltip hvr-pulse-grow" title="<?php echo JrTexto::_('Exit'); ?> Smartclass"><a class="btnsaliraula" href="#"><i class="fa fa-power-off"></i></a></li>
              <li class="istooltip hvr-pulse-grow" title="<?php echo JrTexto::_('Setting devices'); ?> "><a  data-toggle="modal" data-target="#configaudio" href="#"><i class="fa fa-gear"></i></a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right menutop1">
              <li>
                <a href="#" data-toggle="modal" data-target="#myModalvideohelp" title="<?php echo JrTexto::_('Help Me'); ?>" class="hvr-buzz-out istooltip" target="_blank"><i class="btn-icon fa fa-question-circle-o"></i></a>
              </li>
              <li>
                <a href="<?php echo RUTA_WEB; ?>/examenes" title="<?php echo JrTexto::_('Go'); ?> Smartquiz" class="hvr-buzz-out istooltip" target="_blank"><i class="btn-icon fa fa-list"></i></a>
              </li>
              <!--li>
              <a href="<?php echo RUTA_WEB; ?>/examenes" title="<?php echo JrTexto::_('Go'); ?> Smartquiz" class="hvr-buzz-out istooltip" target="_blank">                    
                    <i class="btn-icon fa fa-list"></i>
               </a>
              </li-->            
              <li>
              <a href="<?php echo RUTA_WEB; ?>/recursos/ver" title="<?php echo JrTexto::_('Go'); ?> Smartbook" class="hvr-buzz-out istooltip" target="_blank">                    
                    <i class="btn-icon fa fa-address-book-o"></i>
               </a>
            </li>
            <li>
              <a href="<?php echo RUTA_WEB; ?>/modbiblioteca" title="<?php echo JrTexto::_('Go'); ?> Smartdata" class="hvr-buzz-out istooltip" target="_blank">                    
                    <i class="btn-icon fa fa-book"></i>
               </a>
            </li>
            <li>
              <a href="<?php echo RUTA_WEB; ?>/tarea" title="<?php echo JrTexto::_('Go'); ?> Smartask" class="hvr-buzz-out istooltip" target="_blank">                    
                    <i class="btn-icon fa fa-briefcase"></i>
               </a>
            </li>

            <li>
              <a href="<?php echo RUTA_WEB; ?>/docente/panelcontrol" title="<?php echo JrTexto::_('Go'); ?> Smartraking" class="hvr-buzz-out istooltip" target="_blank">                    
                    <i class="btn-icon fa fa-tachometer"></i>
               </a>
            </li>            
            <!--li> 
                    <a  class="hvr-buzz-out" href="#" id="getting-started">
                        <i class="fa fa-th-large"></i>
                    </a> 
                </li-->
          </ul>
        </div>
      </nav>
    </div>
</header>