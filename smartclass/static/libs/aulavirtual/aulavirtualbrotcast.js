var infokey='chingo01';
var connection = new RTCMultiConnection();
connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';
connection.socketMessageEvent = infokey;
connection.session = {data:true, oneway: true};

connection.sdpConstraints.mandatory = {
    OfferToReceiveAudio: false,
    OfferToReceiveVideo: false
};
connection.onstream = function(event) {
    console.log(event)
}
connection.onstreamended = function(event) {
    console.log(event)
}
var lastSharedScreenShot = '';
var _sysshareScreen=false;
connection.sharePartOfScreen = function(element) {
    html2canvas(element, {
        onrendered: function(canvas){                
            if(_sysshareScreen==false) return;              
            var screenshot = canvas.toDataURL('image/png', 1);
            if(screenshot === lastSharedScreenShot){                    
                connection.sharePartOfScreen(element);
                return;
            }
            lastSharedScreenShot=screenshot;               
            var data={accion:'addscreenshot', screen:screenshot }
            connection.send(data);
            connection.sharePartOfScreen(element);                
        },
        grabMouse: true
    });
};
connection.stopPartOfScreenSharing=function(){
   _sysshareScreen=false;
}
connection.onmessage = function(event) {
    console.log(event)
}
connection.onclose = function(event) {
    console.log(event)
}
connection.onopen = function(event) {
    console.log(event)
}
connection.onMediaError = function(error){console.log(JSON.stringify(error))};
connection.onPeerStateChanged = function(state){
    if (state.iceConnectionState.search(/closed|failed/gi) !== -1) {
        Object.keys(connection.streamEvents).forEach(function(streamid) {
            var streamEvent = connection.streamEvents[streamid];
            if (streamEvent.userid === state.userid) {
                connection.onstreamended(streamEvent);
            }
        });
    }
}
connection.getScreenConstraints = function(callback){
    getScreenConstraints(function(error, screen_constraints) {
        if (!error){
            screen_constraints = connection.modifyScreenConstraints(screen_constraints);
            callback(error, screen_constraints);
            return;
        }
        console.log(error);
    });
}

function reCheckRoomPresence(){
    connection.checkPresence(roomid, function(isRoomExist){
        connection.userid=connection.token();
        if(isRoomExist){
            connection.sdpConstraints.mandatory = {
                OfferToReceiveAudio: true,
                OfferToReceiveVideo: true
            };
            connection.join(roomid);
            return;
        }else{
            connection.open(roomid);
        }
    });
}
reCheckRoomPresence();
$('body').click(function(ev){
    console.log('aa');
    connection.send('hola como estan');
});