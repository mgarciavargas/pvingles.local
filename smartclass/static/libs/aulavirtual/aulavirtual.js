//window.enableAdapter = true;
$(document).ready(function(){
/************************************************************************************************/
var infokey='chingo01d';
var rtcconectado=false; 
var compartiraudio=true;
var compartiravideo=false;
var compartirascreen=false;
var connection = new RTCMultiConnection();
    var _detectRTC=connection.DetectRTC;
    connection.dontCaptureUserMedia = true;
    connection.enableScalableBroadcast=true;
    //connection.maxRelayLimitPerUser = 1;   
    connection.session = {audio:true, video: false, screen:false, data: true};
    connection.mediaConstraints = {video:false,audio:true};
    connection.sdpConstraints.mandatory = { OfferToReceiveAudio: true,OfferToReceiveVideo: true};
    //connection.setCustomSocketHandler(SSEConnection);
    connection.socketURL='https://abacoeducacion.org/';//'https://rtcmulticonnection.herokuapp.com:443/'; //'https://socket.skyrooms.io:443/';
    //connection.setCustomSocketHandler(SSEConnection);
    connection.socketMessageEvent = infokey;
    connection.socketCustomEvent = connection.channel;
    connection.direction = 'many-to-many';
    connection.autoCloseEntireSession = true;
    connection.maxParticipantsAllowed = 4000;
    connection.leaveOnPageUnload = false;
    //connection.closeBeforeUnload = false;
    connection.enableLogs = false;
    connection._mediasscreen = document.getElementById('medias-container');
    connection._mediaaudios= document.getElementById('container-audios');
    connection._mediawebcams= document.getElementById('container-webcams');
    var salirsala=false;
    var usersparticipante=$('#vparticipantes ul#usersparticipante');
    var userchatsms=$('#vchat select#userchatsms');
    var userspizarra=$('#vpizarra select#userspizarra');
    var mediosemit=new Object();
    var ultimostream='';
/******************************************** funciones ****************************************/
    function addparticipantes(jsonuser,soyyo){
        li=usersparticipante.find('li#'+jsonuser.userid);
        if(li.length) return;
        var userclone=$('#vparticipantes div#userclone').clone(true);
        userclone.find('#idtmp').attr('id',jsonuser.userid);
        userclone.find('a.userstatus.mode'+jsonuser.tipo).addClass('active');
        userclone.find('span.nombre').text(jsonuser.user);
        var iclass=userclone.find('a.userstatus.active i').attr('class');
        userclone.find('a.miestatus i').removeAttr('class').addClass(iclass); 
        if(soyyo==true){
            userclone.find('span.userchataccion').html('Me');
            usersparticipante.prepend(userclone.html());
            userspizarra.append('<option value="Yo">Me</option>');
            connection.extra=jsonuser;
            connection.updateExtraData();
        }else{
            userclone.find('.lilinea').remove();
            usersparticipante.find('li.linea').before(userclone.html());
            userchatsms.append('<option value="'+jsonuser.userid+'"><span class="'+iclass+'"></span> '+jsonuser.user+'</option>');
            userspizarra.append('<option value="'+jsonuser.userid+'">'+jsonuser.user+'</option>');
        }
        resetprivilegios();
        usersparticipante.find('#totaluser b').text(usersparticipante.find('li.liuser').length);        
    }

    function removeparticipante(userid){
        usersparticipante.find('li#'+userid).remove();
        userchatsms.find('option[value="'+userid+'"]').remove();
        userspizarra.find('option[value="'+userid+'"]').remove();
        usersparticipante.find('#totaluser b').text(usersparticipante.find('li.liuser').length);
    }

    function _addmensajechat(event,soyyo){       
        var currentTime = new Date();
        var time=currentTime.getHours()+':'+currentTime.getMinutes()+':'+currentTime.getSeconds();
        var txt='';
        if(soyyo==true){
            var data=event;
            txt='<div class="mensaje" id="'+connection.userid+'"><div class="pull-right text-left globoright"><b>Me :</b><br>'+data.texto+'<br><div class="chattime">'+time+'</div></div><div class="clearfix"></div></div>';
        }else{
            var data=event.data;
            var para=data.para;
            _user=event.extra;
            if(para!='alluser'&&para!=connection.userid) return;
            var isprivate=para==connection.userid?'mensaje isprivate':'mensaje';
            txt='<div class="'+isprivate+'"  id="'+event.userid+'"><div class="pull-left text-left globoleft"><b>'+_user.user+' :</b><br>'+data.texto+'<br><div class="chattime">'+time+'</div></div><div class="clearfix"></div></div>';
        }
        var scrollHeight=$('.chatAll .mensajes')[0].scrollHeight;
        $('.chatAll .mensajes').append(txt).animate({scrollTop:scrollHeight}, 500);
    }

    function resetprivilegios(){        
        if(tipouser=='M'){
            usersparticipante.find('a.miestatus').removeClass('disabled');
            usersparticipante.find('a.miestatus span').show(0);
            usersparticipante.find('.userchataccion .btn-group.tipoM').show(0);
            usersparticipante.find('.userchataccion .btn-group.tipoU').hide(0); 
            $('#menupersonalizado .btnlokedroom').show(0);
            $('#menupersonalizado .btnalzarmano').hide(0);
            $('#menupersonalizado .btnsharedesktop').show(0);
            $('#menupersonalizado .btnlokedroom').show(0);
            $('.infosmartclass .botones-compartir').show(0);
        }else{
            $('#menupersonalizado .btnlokedroom').hide(0);
            if(tipouser=='P'){
                $('#menupersonalizado .btnsharedesktop').show(0);
                $('#menupersonalizado .btnalzarmano').hide(0);
                $('.infosmartclass .botones-compartir').show(0);
            }else{
                $('#menupersonalizado .btnsharedesktop').hide(0);
                $('#menupersonalizado .btnalzarmano').show(0);
                $('.infosmartclass .botones-compartir').hide(0);
            }
            usersparticipante.find('a.miestatus').addClass('disabled');
            usersparticipante.find('a.miestatus span').hide(0);
            usersparticipante.find('.userchataccion .btn-group.tipoM').hide(0);
            usersparticipante.find('.userchataccion .btn-group.tipoU').show(0);
        }
    }
    
    function _cambiartypeuser(data){
        var para=data.para;
        var mode=data.mode;
        var iclass=data.cls;
        if(para==roomid&&mode!='M') return;
        if(para==connection.userid){
            tipouser=data.mode;
            userid=connection.userid;        
            savedatosuser();
            resetprivilegios();
        }
        var userachange=usersparticipante.find('li#'+para);
        userachange.find('.userstatus').removeClass('active');
        userachange.find('.miestatus i.fa').removeAttr('class').addClass(iclass);
        userachange.find('.userstatus.mode'+mode).addClass('active');       
    }

    function _alzarlamano(data,soyyo){
        if(soyyo==true) userremote=connection.userid;
        else userremote=data.userid;
        var alzarlamano=data.alzarlamano;
        var btnmano=$('#vparticipantes #usersparticipante li#'+userremote+' .btnmano');         
        if(alzarlamano==true){
            btnmano.show();
            $('#usersparticipante li#'+userremote+' .infomano').show();
            if(tipouser!='U'){                
                $('i',btnmano).addClass('bounceIn colorgreen');
            }
        }else if(alzarlamano==false){
            btnmano.hide();
            $('#usersparticipante li#'+userremote+' .infomano').hide();
            $('i',btnmano).removeClass('bounceIn colorgreen');
            if(userremote==connection.userid)$('#menupersonalizado a.btnalzarmano').removeClass('active').find('i').removeClass('colorgreen');
        }
    }

    var useremit={};
    function updateuseremit(){
        if(connection.userid!==roomid)return;
        var users=connection.getAllParticipants();    
        var userold=roomid;
        $.each(users,function(i,v){
            if(userold==roomid) useremit[userold]={id:roomid,_get:'',_put:''};
            useremit[v]={id:v,_get:userold,_put:''};           
            useremit[userold]['_put']=v;
            userold=v;
        });
        connection.send({accion:'actualizaruser',data:useremit});
    }

    function _actualizaruser(event,local){
        console.log(event,local);
        var _miuserid=connection.userid;
        var olddatos=useremit[_miuserid]||-1;       
        var newdatos=event.data[_miuserid];
        if(olddatos==-1){
            useremit=event.data;
            /*if(useremit[_miuserid]!=undefined&&useremit[_miuserid]!=''){
                if(useremit[_miuserid]._get){
                    var _get=useremit[_miuserid]._get;
                    connection.send({accion:'enviarmemedios'},_get);
                }                    
            }
            return;*/
        }else{
            var _miget=olddatos._get||'';
            var _miput=olddatos._put||'';
            var _newget=newdatos._get||'';
            var _newput=newdatos._put||'';
            var newevent={userid:'',data:{tipoenvio:'yo'}};
            if(_miget!=_newget){
                useremit[_miuserid]['_get']=_newget;
                if(_newget!=''||_newget!=undefined){
                    //console.log('user: '+_miuserid+'- oldget -'+_miget+' - cambio get: '+_newget);    
                    newevent.userid=_newget;
                    newevent.data.tipoenvio='_put';
                    _enviarmemedios(newevent);
                }
            }else if(_miput!=_newput){
                useremit[_miuserid]['_put']=_newput;                
                if(_newput!=''||_newput!=undefined){
                    //console.log('user: '+_miuserid+'- oldput -'+_miput+' - cambio put:'+_newput);
                    newevent.userid=_newput;
                    newevent.data.tipoenvio='_get';
                    _enviarmemedios(newevent);
                }               
            }
        }
    }

    function _reconectarBye(idpeer){
        try{
            var btnmicro=$('#menupersonalizado a.btnsharemicro');
            if(btnmicro.hasClass('active')) _sysactivarmicro(btnmicro);

            var btncamera=$('#menupersonalizado a.btnsharecamera');
            if(btncamera.hasClass('active')) _sysactivarcamara(btncamera)

        }catch(err){console.log(err)}
        console.log(event);
    }
    
    var camarasonline={}
    function shichmicroprivado(userremote,data){
        var userde=userremote;
        var userpara=data.userremote;
        var audiouserde=$('audio#audio_'+userde);
        var audiouserpara=$('audio#audio_'+userpara);
        $('#vparticipantes #usersparticipante li .btnmicro.active').each(function(){
            $('i',this).removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
            var id=$(this).closest('li').attr('id');
            var audiotmp=$('audio#audio_'+id);
            if(audiotmp.length){
               audiotmp.prop('muted',true);
               audiotmp.trigger('pause');
            }
            $('#vparticipantes #usersparticipante li#'+id+' .btnmicroprivado').removeClass('active');
            $('#vparticipantes #usersparticipante li#'+id+' .btnmicroprivado i').addClass('fa-phone').removeClass('fa-volume-control-phone bounceIn colorgreen');

        });      
        if(connection.userid==userpara){
            if(audiouserde.length && data.active==true){
                audiouserde.prop('muted',false); 
                audiouserde.trigger('play');                
                $('#vparticipantes #usersparticipante li#'+userde+' .btnmicroprivado').addClass('active');
                $('#vparticipantes #usersparticipante li#'+userde+' .btnmicroprivado i').removeClass('fa-phone').addClass('fa-volume-control-phone bounceIn colorgreen');
                $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
                $('#menupersonalizado .menutools a.btnsharemicro').removeClass('active');
            }else{
                audiouserde.prop('muted',true);
                audiouserde.trigger('pause');
                $('#vparticipantes #usersparticipante li#'+userde+' .btnmicroprivado').removeClass('active');
                $('#vparticipantes #usersparticipante li#'+userde+' .btnmicroprivado i').addClass('fa-phone').removeClass('fa-volume-control-phone bounceIn colorgreen');                
            }
        }else if(connection.userid==userde){
            if(audiouserpara.length && data.active==true){
                audiouserpara.prop('muted',false); 
                audiouserpara.trigger('play');
                $('#vparticipantes #usersparticipante li#'+userpara+' .btnmicroprivado').addClass('active');
                $('#vparticipantes #usersparticipante li#'+userpara+' .btnmicroprivado i').removeClass('fa-phone').addClass('fa-volume-control-phone bounceIn colorgreen');
                $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
                $('#menupersonalizado .menutools a.btnsharemicro').removeClass('active');
            }else{
                audiouserpara.prop('muted',true);
                audiouserpara.trigger('pause');                
                $('#vparticipantes #usersparticipante li#'+userpara+' .btnmicroprivado').removeClass('active');
                $('#vparticipantes #usersparticipante li#'+userpara+' .btnmicroprivado i').addClass('fa-phone').removeClass('fa-volume-control-phone bounceIn colorgreen');
            }
        }else{
            $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
            $('#menupersonalizado .menutools a.btnsharemicro').removeClass('active');                           
        }
    }

    /*******************************funciones de pdf ********************************************/    
    var sysencuestas={};
    function tomarencuesta(encuesta){
        var modal=$('#modalquestion').clone(true);
        modal.attr('id',encuesta.idencuesta);
        modal.find('.aquipregunta').text(encuesta.pregunta);
        var aquialternativas=modal.find('.aquialternativas');
        var alt_=encuesta.alternativas
        aquialternativas.html('');
        if(alt_!=undefined)
        $.each(alt_,function(i,v){
            var bt='<a href="#" data-value="'+i+'" class="btn btn-default btnresponder">'+i+'</a>';
            aquialternativas.append(bt);
        })
        modal.modal({backdrop: 'static', keyboard: false});
    }

    function responderencuesta(userremote,data){
        var id=data.id;
        var res=data.respuesta;
        if(sysencuestas[id]!=undefined){
            sysencuestas[id]['alternativas'][res]=sysencuestas[id]['alternativas'][res]+1;            
        }        
    }

    function mostraricono(userremote,data){
        var audiosound=document.getElementById('audiosonidos');
        //var audiosound=$('#audiosonidos');       
        var span='<span id="icon'+userremote+'"><i class="'+data.icon+'"></i></span>';
        var li=$('#vparticipantes ul#usersparticipante li#'+userremote);
        var spanicon=$('#icon'+userremote,li);
        if(spanicon.length<1){
            $('.userchataccion',li).prepend(span);    
        }else{
            if(!audiosound.paused) audiosound.pause();           
            $('i',spanicon).removeAttr('class').addClass(data.icon);
        }
        url=_sysUrlStatic_+'/media/aulasvirtuales/audios/'+data.sonido;
        if(_sysfileExists_(url)){
            audiosound.src=url;
            audiosound.currentTime=0;
            if(_sysaudioout)attachSinkId(audiosound,_sysaudioout);
            audiosound.play();           
        }else{ // si no tiene audio el icono
            setTimeout(function(){  $('#icon'+userremote,li).remove(); },8000);
        }
        audiosound.onended = function(){
            $('#icon'+userremote,li).remove();
        }           
    }

    function addsharefile(userremote,data){
        var newfile=$('#vfiles #addfileshareclone').clone(true);
        newfile.find('.nombreuser').text(userremote);
        newfile.find('.nombrefile').text(data.namefile);
        newfile.find('.extension').text(data.extension);
        newfile.find('.link a').attr('href',_sysUrlStatic_+'/media/aulasvirtuales/'+data.namefile).attr('target','_blank');
        newfile.show('fast').removeAttr('id');
        $('#vfiles table').append(newfile);
        $('#vfiles').show();
    }

    function addsharevideo(userremote,data){
        var video=document.createElement('video');
        video.autoplay=true;
        if(_sysaudioout)attachSinkId(video,_sysaudioout);
        video.src=_sysUrlStatic_+'/media/aulasvirtuales/'+data.namefile;
        video.controls=true;        
        $('#vvideos div.addvideoaqui').html('').append(video);
        video.play();
        $('#vvideos').show();
    }

    function addscreenshot(userremote,data){
        $('#medias-container audio').hide();
        $('#medias-container video').remove();
        $img=$("#mediascrenshot");
        if($img.length>0)
            $img.attr('src',data.screen);
        else{
            var img='<img src="'+data.screen+'" width="100%" id="mediascrenshot">';        
            $('#medias-container').append(img);
        } 
        //connection._medias.appendChild(media);
        ventana=$('.showwindow[data-ventana="vinformation"]');
        if(ventana.hasClass('active')){
            $('#vinformation').show();
        }else{
            ventana.trigger('click');
        }
    }

    function _cambiarmicrouser(estado,para){
        if(para===connection.userid){
            // console.log(para+'='+connection.userid+' ------ '+estado);        
            if(estado==='nopermitir'){               
                permiso.micro=false;
                permiso.webcam=false;
                resetmedios();
            }else if(estado==='permitir'){                
                permiso.micro=true;
                resetmedios();
            }else if(estado==='inactive'){                
                $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone-slash iconinactive animated infinite');
            }else if(estado==='active'){               
                $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone-slash').addClass('fa-microphone bounceIn colorgreen');
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone bounceIn colorgreen animated infinite');
            }            
        }else{
            var audiouser=$('audio#audio_'+para);
            if(estado==='nopermitir'){
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone-slash  animated infinite');
            }else if(estado==='permitir'){
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone animated infinite');
            }else if(estado==='inactive'){
                if(audiouser.length) {audiouser.prop('muted',true);  audiouser.trigger('pause');}
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone-slash iconinactive animated infinite');
            }else if(estado==='active'){
                if(audiouser.length) {audiouser.prop('muted',false);  audiouser.trigger('play');}
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone bounceIn colorgreen animated infinite');
            }
        }
    }

    function _cambiarmicro(userremote,data){
        var estado=data.estado;
        var para=userremote;
        if(para==='alluser'){
            var obj=$('#vparticipantes #usersparticipante li.liuser');
            $.each(obj,function(i,v){
                var id=$(v).attr('id');
                _cambiarmicrouser(estado,id);
            });
            var cls='';
            if(estado==='active') cls='fa fa-microphone colorgreen';
            else if(estado==='inactive') cls='fa fa-microphone-slash iconinactive';
            else if(estado==='permitir')  cls='fa fa-microphone';
            else if(estado==='nopermitir')  cls='fa fa-microphone-slash';
            $('#vparticipantes #usersparticipante .alluser .btn-micro i.vericon').removeAttr('class').addClass('vericon '+cls+' animated infinite');            
        }else{
            _cambiarmicrouser(estado,para);
        }        
    }

    function _activarmicro(data){
        var estado=data.estado;
        var para=data.userremote;
        if(estado==true){ //enceder micro remoto
            if(para=='alluser'||para==connection.userid){
               btnmicro=$('#menupersonalizado .btnsharemicro');
               if(!btnmicro.hasClass('active')){
                    btnmicro.show();
                    btnmicro.trigger('click');
                }
            }
        }else{ //apagar micro remoto
            if(para=='alluser'||para==connection.userid){
               btnmicro=$('#menupersonalizado .btnsharemicro');
               if(btnmicro.hasClass('active')) btnmicro.trigger('click');
            }
        }    
    }

    function _stopscreen(userid){
        var $hay=$('#scren_'+userid);
        if($hay.length){
            $hay[0].pause();
            $hay.remove();
            $('#medias-container').html('').hide(0); 
            $('.infosmartclass').show();
        }
       
        if(userid==connection.userid){
            if(_miaudioscreen) {
             _miaudioscreen.stop();
             _miaudioscreen='';
            }
            if(_miscreen){
                _miscreen.stop();
                _miscreen='';
            }
            var btnmicro=$('#menupersonalizado a.btnsharemicro');
            if(btnmicro.hasClass('active')) _sysactivarmicro(btnmicro);
            $('#medias-container').html('').hide(0); 
            $('.infosmartclass').show();
        }    
        $('#usersparticipante li#'+userid+' .infoscreen').hide();
    }

    function _addscreem(stream,userid){
        var $hay=$('#scren_'+userid);
        if($hay.length)$hay.remove();
        var $hay=$('#micro_'+userid);
        if($hay.length)$hay.remove();
        userscreen=userid;
        $('#medias-container').show().html(''); 
        $('.infosmartclass').hide(); 
        $('#usersparticipante li#'+userid+' .infoscreen').show();
        btnmicro=$('#usersparticipante li#'+userid+' .sysmenu i.vericon');
        btnmicro.removeClass('fa-microphone-slash').addClass('fa-microphone colorgreen');
        $('#usersparticipante li#'+userid+' .infomicro').show();
        var video=document.createElement('video');
        video.id='scren_'+userid;
        video.controls=false;
        video.autoplay=true;
        video.style.width='100%';
        video.volume=0;
        video.srcObject=stream;
        if(_sysaudioout)attachSinkId(video,_sysaudioout);
        connection._mediasscreen.appendChild(video);
        ventana=$('.showwindow[data-ventana="vinformation"]');
        if(ventana.hasClass('active')){
            $('#vinformation').show();
        }else{
            ventana.trigger('click');
        }
    }

    function _addcamera(stream,userid){
        var $hayvideos=$('#camera_'+userid);
        if($hayvideos.length)$hayvideos.remove();
        var videos=$('#container-webcams').find('video');
        var totalvideos=100/(videos.length+1);
        if(totalvideos<19)totalvideos=19;
        $.each(videos,function(i, o){             
            $(this).css('width',totalvideos+'%'); 
        });
        $('#usersparticipante li#'+userid+' .infocamera').show();
        var video=document.createElement('video');
        video.id='camera_'+userid;
        video.controls=false;
        video.autoplay=true;
        video.style.width=totalvideos+'%';
        video.srcObject=stream;
        video.volume=0;
        if(_sysaudioout)attachSinkId(video,_sysaudioout);
        connection._mediawebcams.appendChild(video);
        ventana=$('.showwindow[data-ventana="vwebcam"]');
        if(ventana.hasClass('active')){
            $('#vwebcam').show();
        }else{
            ventana.trigger('click');
        }
    }

    function _stopcamera(userid){
        var $hay=$('#camera_'+userid);
        if($hay.length){
            $hay.remove();
            $hay[0].pause();
        }
        var videos=$('#container-webcams').find('video'); 
        $('#usersparticipante li#'+userid+' .infocamera').hide();       
        if(videos.length==0) {
            $('#vwebcam').hide();
            $('.showwindow[data-ventana="vwebcam"]').removeClass('active');
        }else{
            var totalvideos=100/(videos.length);        
            if(totalvideos<19)totalvideos=19;
            $.each(videos,function(i, o){$(this).css('width',totalvideos+'%'); });
        }
    }

    function _addmicro(stream,userid){
        var $hay=$('#micro_'+userid);
        if($hay.length)$hay.remove();
        btnmicro=$('#usersparticipante li#'+userid+' .sysmenu i.vericon');
        btnmicro.removeClass('fa-microphone-slash').addClass('fa-microphone colorgreen');
        $('#usersparticipante li#'+userid+' .infomicro').show();
        var medio=document.createElement('audio');
        medio.id='micro_'+userid;
        medio.controls=true;
        medio.autoplay=true;        
        medio.srcObject=stream;
        medio.volume=0.75;
        if(_sysaudioout)attachSinkId(medio,_sysaudioout); 
        connection._mediaaudios.appendChild(medio);        
    }
    
    function _stopmicro(userid){
        var $hay=$('#micro_'+userid);
        if($hay.length){
            $hay[0].pause();
            $hay.remove();
        }
        $('#usersparticipante li#'+userid+' .sysmenu i.vericon').removeClass('fa-microphone colorgreen').addClass('fa-microphone-slash');
        $('#usersparticipante li#'+userid+' .infomicro').hide();
    }

    function _updatesala(data){
        var estado=data.estado;
        $btnsala=$('.btnlokedroom i');
        if(estado==true){
            $btnsala.removeClass('fa-unlock').addClass('fa-lock');
        }else{
            $btnsala.addClass('fa-unlock').removeClass('fa-lock');
        }
    }

    function callllamadaprivada(stream,datosuser){
        $.confirm({
            title: $('#callprivatetitle').text()+' '+datosuser.user,
            content: $('#callprivatecontent').text(),
            type: 'green',
            icon: 'fa fa-volume-control-phone',
            typeAnimated: true,
            confirmButton: $('#callprivatebtnok').text(),
            cancelButton: $('#callprivatebtncancel').text(),
            confirmButtonClass: 'btn-primary',
            confirmButtonIcon: 'fa fa-check',
            cancelButtonClass: 'btn-danger',
            cancelButtonIcon:'fa fa-close',
            confirm: function(){
                userremote=datosuser.userid;
                var audioSource=sessionStorage.getItem('audioInId')||true;
                var objaudio=audioSource==true?true:({deviceId: audioSource ? {exact: audioSource} : undefined});     
                var $btninfollamada=$('#vparticipantes li#'+userremote+ ' .btn-group.tipoM .infollamada');
                var $btninfollamada2=$('#vparticipantes li#'+userremote+ ' .btn-group.tipoU .infollamada');      
                navigator.mediaDevices.getUserMedia({audio:objaudio,video:false}).then(function(miaudio){                    
                    $btninfollamada.addClass('active').find('i').addClass('fa-volume-control-phone colorgreen').removeClass('fa-phone colored');
                    $btninfollamada2.addClass('active fa-volume-control-phone colorgreen').removeClass('fa-phone colored').show(0);
                    if(_millamadaprivada) _millamadaprivada.getTracks()[0].stop();
                    _millamadaprivada=miaudio;
                    var $hay=$('#micro_'+userremote);
                    if($hay.length)$hay.remove();
                    var medio=document.createElement('audio');
                    medio.id='micro_'+userremote;
                    medio.controls=true;
                    medio.autoplay=true;        
                    medio.srcObject=stream;
                    if(_sysaudioout)attachSinkId(medio,_sysaudioout);
                    connection._mediaaudios.appendChild(medio);
                    connection.extra.emit='llamadaprivadaaceptada';
                    connection.extra.emituser=connection.userid;
                    connection.updateExtraData();
                    if(connection.peers[userremote]==undefined) return;
                    var peer = connection.peers[userremote].peer;
                    peer.addStream(miaudio);
                    connection.dontAttachStream = true;
                    connection.renegotiate(userremote);
                    connection.dontAttachStream = false; 
                });
               data={accion:'llamadaprivada',estado:'activa'}; 
               connection.send(data,datosuser.userid);
            },
            cancel:function(){                
                data={accion:'llamadaprivada',estado:'colgar',userremote:connection.userid}; 
                connection.send(data,connection.userid);
            }                       
        });
            
    }    
    /******************************************** eventos sockets ********************************/
    connection.onopen = function(event){
        var extra=event.extra;
        addparticipantes(extra,false);
        updateuseremit();
    }

    connection.onclose = function(event){  
        var remoteuserid=event.userid;
        if(connection.userid==remoteuserid){ connection.close(); return; }
        delete useremit[remoteuserid];
        savedatosuser();        
        if(remoteuserid==roomid&&connection.userid!=roomid){
            setTimeout(function(){reconectarasala(1,0),3000}); //cuando se desconecta de la sala el que lo crea;
        }
        connection.disconnectWith(remoteuserid);
        removeparticipante(remoteuserid);
        _stopmicro(remoteuserid);
        _stopcamera(remoteuserid);
        _stopscreen(remoteuserid);        
    }

    connection.onMediaError = function(error){console.log(JSON.stringify(error))};

    connection.onmessage=function(event){
        //console.log(event);
        var data=event.data||'';
        var accion=data!=''?data.accion:'';
        var userremote=data.userremote||event.userid;       
        if(accion=='addmensajechat') _addmensajechat(event,false);
        else if(accion=='cambiartypeuser') _cambiartypeuser(data);
        else if(accion=='alzarlamano') _alzarlamano(data,false);
        else if(accion=='activarmicro') _activarmicro(data);
        else if(accion=='llamadaprivada') _llamadaprivada(data,userremote);
        else if(accion=='shichmicroprivado') shichmicroprivado(event.userid,data);
        else if(accion=='tomarencuesta') tomarencuesta(data);
        else if(accion=='responderencuesta') responderencuesta(event.userid,data); 
        else if(accion=='mostraricono') mostraricono(userremote,data);
        else if(accion=='addsharefile') addsharefile(userremote,data); 
        else if(accion=='addsharevideo') addsharevideo(userremote,data);
        else if(accion=='addscreenshot') addscreenshot(userremote,data);
        else if(accion=='cambiarmicro') _cambiarmicro(userremote,data);
        else if(accion=='stopscreen') _stopscreen(userremote);
        else if(accion=='stopcamera') _stopcamera(userremote);
        else if(accion=='stopmicro') _stopmicro(userremote);
        else if(accion=='addmicroicon') _addmicroicon(userremote);
        else if(accion=='actualizaruser') _actualizaruser(data,false);
        else if(accion=='updatesala') _updatesala(data);
        else if(accion=='reconectarBye') _reconectarBye(event);
        else if(accion=='enviarmemedios') _enviarmemedios(event);        
       // else if(accion=='shichcamara') _shichcamara(event.userid,data);
    }

    function _enviarmemedios(event){
        _userid=event.userid;
        _tipoenvio=event.data.tipoenvio||'ninguno';
        var i=0;
        var tmp={};
        for(k in mediosemit){
            tmp[k]=mediosemit[k];
        }
       // var tmp=Object.assign({},mediosemit);
       // console.log(tmp);
        async function __reenvia(tmp){
            for(var k in tmp){
                var obj=tmp[k];
                var objde=obj.de;
                if(objde=='yo'||_tipoenvio=='ninguno'||objde==_tipoenvio){
                    connection.extra.emit=obj.tipo;
                    connection.extra.emituser=obj.userid;
                    connection.updateExtraData();
                   // console.log(obj,_userid);
                    renviarmedio(obj.stream,_userid);
                    delete tmp[k];
                    setTimeout(function(){ __reenvia(tmp)},3000);
                    return;
                }else {
                    delete tmp[k];
                    __reenvia(tmp);
                    return;
                } 
            }
        }       
        __reenvia(tmp);
    }

    function renviarmedio(stream,_userid){
        try{

        if(connection.peers[_userid]==undefined) return;
        var peer = connection.peers[_userid].peer||false;
            if(peer==false) {
                console.log('error con '+_userid);
                return;
            }
            peer.addStream(stream);
            connection.dontAttachStream = true;
            connection.renegotiate(_userid);
            connection.dontAttachStream = false;
            return true;
        }catch(err){console.log(err)}
    }

    function _llamadaprivada(data,userid){
        var $btninfollamada=$('#vparticipantes li#'+userid+ ' .btn-group.tipoM .infollamada');
        var $btninfollamada2=$('#vparticipantes li#'+userid+ ' .btn-group.tipoU .infollamada');
        if(data.estado=='aceptar'){
            $btninfollamada.addClass('active').removeClass('enprogreso').find('i').addClass('fa-volume-control-phone colorgreen').removeClass('fa-phone colored');
            $btninfollamada2.addClass('active fa-volume-control-phone colorgreen').removeClass('fa-phone colored enprogreso').show(0);
        }else if(data.estado=='colgar'){
            $btninfollamada.removeClass('active enprogreso').find('i').removeClass('fa-volume-control-phone colorgreen colored').addClass('fa-phone');
            $btninfollamada2.removeClass('active enprogreso fa-volume-control-phone colorgreen colored').addClass('fa-phone').hide(0);
            try{if(_millamadaprivada) _millamadaprivada.getTracks()[0].stop();}catch(ex){console.log(ex)};
            var $hay=$('#micro_'+userid);
            if($hay.length)$hay.remove();
        }else if(data.estado=='enprogreso'){
            try{if(_millamadaprivada) _millamadaprivada.getTracks()[0].stop();}catch(ex){console.log(ex)};
            $('.jconfirm').remove();
        }
    }

    connection.onstream = function(event){
        if(event.stream.id==ultimostream) return;       
        ultimostream=event.stream.id;
        var userremote=event.userid;
        var _stream=event.stream;
        var _tipouser=event.type;

        event.mediaElement.pause();
        delete event.mediaElement;
        var emit=event.extra.emit||'';
        var deuser=event.extra.emituser||'';
        if(emit!=''&&deuser!=''){
            try{
                connection.extra.emit=emit;
                connection.extra.emituser=deuser;
                connection.updateExtraData();
                if(_tipouser=='remote'){
                    console.log(event);  
                    if(emit=='llamadaprivada'){
                        callllamadaprivada(_stream,event.extra);
                        return;
                    }else if(emit=='llamadaprivadaaceptada'){
                        var $hay=$('#micro_'+userremote);
                        if($hay.length)$hay.remove();
                        var medio=document.createElement('audio');
                        medio.id='micro_'+userremote;
                        medio.controls=true;
                        medio.autoplay=true;        
                        medio.srcObject=_stream;
                        medio.volume=0.75;
                        if(_sysaudioout)attachSinkId(medio,_sysaudioout);
                        connection._mediaaudios.appendChild(medio);
                        var data={estado:'aceptar',userremote:userremote};
                        _llamadaprivada(data,userremote);
                        return;
                    }else if(emit=='screen'){
                        $('#usersparticipante li .infoscreen').hide();
                        if($('#menupersonalizado a.btnsharedesktop').hasClass('active')){
                            _stopscreen(connection.userid);
                             $('.btnsharedesktop').removeClass('active').find('i').removeClass('colorgreen');
                             if(_miaudioscreen) _miaudioscreen.stop();
                             if(_miscreen)_miscreen.stop();
                        }
                        _addscreem(_stream,deuser);                        
                    }else if(emit=='camera') _addcamera(_stream,deuser); 
                    else if(emit=='micro') _addmicro(_stream,deuser);
                    var miuseremit=useremit[connection.userid];
                    if(miuseremit!=undefined){                
                        _put=miuseremit._put||'';
                        _get=miuseremit._get||'';                         
                        if(_put!=''&&_put!=deuser&&_put!=userremote){                            
                            renviarmedio(_stream,_put);
                        }
                        if(_get!=''&&_get!=deuser&&_get!=userremote){                           
                            renviarmedio(_stream,_get);
                        }
                        if(_put==userremote) mediosemit[emit+'_'+deuser]={userid:deuser,tipo:emit,stream:_stream,de:'_put'};
                        else if(_get==userremote) mediosemit[emit+'_'+deuser]={userid:deuser,tipo:emit,stream:_stream,de:'_get'};
                        return;
                    }
                }                
            }catch(err){console.log(err);}
        }
    }

    connection.onstreamended = function(event){
        var _userid=event.userid;
        _stopmicro(_userid);
        _stopcamera(_userid);
        _stopscreen(_userid);
    }

    connection.onPeerStateChanged = function(event){
        if (event.iceConnectionState.search(/closed|failed/gi) !== -1) {
            Object.keys(connection.streamEvents).forEach(function(streamid){
                var streamEvent = connection.streamEvents[streamid];
                if (streamEvent.userid === event.userid) {
                    connection.onstreamended(streamEvent);
                }
            });
        }else{
            if(event.iceConnectionState === 'connected' && event.signalingState === 'stable') {
                if(connection.peers[event.userid].gettingStats === true) { return;
                }

                connection.peers[event.userid].gettingStats = true; // do not duplicate
                var peer = connection.peers[event.userid].peer;
                var interval = 1000;

                if(DetectRTC.browser.name === 'Firefox') {
                  getStats(peer, peer.getLocalStreams()[0].getTracks()[0], function(stats) {
                    onGettingWebRTCStats(stats, event.userid);
                  }, interval);
                }
                else {
                  getStats(peer, function(stats) {
                    onGettingWebRTCStats(stats, event.userid);
                  }, interval);
                }               
              }
        }
    }

    function onGettingWebRTCStats(stats, remoteUserId){
      if(!connection.peers[remoteUserId]){
        stats.nomore();
      }
      var statsData = 'UserID: ' + remoteUserId + '\n';
      statsData += 'Bandwidth: ' + bytesToSize(stats.bandwidth.speed);
      statsData += '\n';
      statsData += 'Codecs: ' + stats.audio.recv.codecs.concat(stats.video.recv.codecs).join(', ');
      statsData += '\n';
      statsData += 'Data: ' + bytesToSize(stats.audio.bytesReceived + stats.video.bytesReceived);
      statsData += '\n';
      statsData += 'ICE: ' + stats.connectionType.remote.candidateType.join(', ');
      statsData += '\n';
      statsData += 'Port: ' + stats.connectionType.remote.transport.join(', ');
      //console.log(stats);
    }

    
    /*
    function getDivByUserId(_userid){
        divx =document.getElementById('infouserid_'+_userid);        
        if(divx==null){
            divx1=document.createElement('div');
            divx1.id='infouserid_'+_userid;
            $('#_infousers_').show().append(divx1);
            return $('#infouserid_'+_userid); 
        }
        return $('#infouserid_'+_userid); 
    }
    */

    function bytesToSize(bytes) {
        var k = 1000;
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes === 0) {
            return '0 Bytes';
        }
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(k)), 10);
        return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
    }

    connection.getScreenConstraints = function(callback){
        getScreenConstraints(function(error, screen_constraints) {
            if (!error){
                screen_constraints = connection.modifyScreenConstraints(screen_constraints);
                callback(error, screen_constraints);
                return;
            }
            console.log(error);
        });
    }

    var lastSharedScreenShot = '';
    var _sysshareScreen=false;
    connection.sharePartOfScreen = function(element) {
        html2canvas(element, {
            onrendered: function(canvas){                
                if(_sysshareScreen==false) return;              
                var screenshot = canvas.toDataURL('image/png', 1);
                if(screenshot === lastSharedScreenShot){                    
                    connection.sharePartOfScreen(element);
                    return;
                }
                lastSharedScreenShot=screenshot;               
                var data={accion:'addscreenshot', screen:screenshot }
                connection.send(data);
                connection.sharePartOfScreen(element);                
            },
            grabMouse: true
        });
    };

    connection.stopPartOfScreenSharing=function(){ _sysshareScreen=false; }

    /************************************** Eventos Botones ********************************************/
    var _detectRTC=connection.DetectRTC;
    $('.btnalzarmano').click(function(ev){
        var btnalzarmano=$('#menupersonalizado a.btnalzarmano');
        var obj=$(this);
        var i=obj.find('i');
        btnalzarmano.toggleClass('active');
        if(btnalzarmano.hasClass('active')){
            btnalzarmano.find('i').addClass('colorgreen');
            var data={userid:connection.userid,accion:'alzarlamano',alzarlamano:true}
            connection.send(data);
            _alzarlamano(data,true);
        }else{
            btnalzarmano.find('i').removeClass('colorgreen');
            var data={userid:connection.userid,accion:'alzarlamano',alzarlamano:false}
            connection.send(data);
            _alzarlamano(data,true);
        }
    });

    var _millamadaprivada='';
    $('#vparticipantes').on('click','#usersparticipante .btnmano',function(){
        $(this).hide();
        $(this).find('i').removeClass('bounceIn colorgreen');
        var userremote=$(this).closest('li').attr('id');
        var data={userid:userremote,accion:'alzarlamano',alzarlamano:false}
            connection.send(data);
    }).on('click','.btnmicroactive',function(ev){
        ev.preventDefault();
        var getuser=$(this).closest('.sysmenu');
        var data={accion:'activarmicro',estado:true,userremote:''};
        if(getuser.hasClass('alluser')){
            data.userremote='alluser';
            connection.send(data);
        }else if(getuser.hasClass('oneuser')){           
            data.userremote=$(this).closest('li.liuser').attr('id');
            connection.send(data,data.userremote);                  
        } 
    }).on('click','.btnmicroinactive',function(ev){
        ev.preventDefault();
        var getuser=$(this).closest('.sysmenu');
        var data={accion:'activarmicro',estado:false,userremote:''};  
        if(getuser.hasClass('alluser')){
            data.userremote='alluser';
            connection.send(data);
        }else if(getuser.hasClass('oneuser')){
           data.userremote=$(this).closest('li.liuser').attr('id');
            connection.send(data,data.userremote); 
        } 
    }).on('click','.btnmicropermitir',function(ev){
        ev.preventDefault();
        var tipouser=$(this).closest('.sysmenu');
        if(tipouser.hasClass('alluser')){
            var userremote='alluser';
        }else if(tipouser.hasClass('oneuser')){
            var userremote=$(this).closest('li.liuser').attr('id');
        }     
        var data={accion:'cambiarmicro',estado:'permitir',userremote:userremote};
        _cambiarmicro(userremote,data);
        connection.send(data);
    }).on('click','.btnmicronopermitir',function(ev){
        ev.preventDefault();
        var tipouser=$(this).closest('.sysmenu');
        if(tipouser.hasClass('alluser')){
            var userremote='alluser';
        }else if(tipouser.hasClass('oneuser')){
            var userremote=$(this).closest('li.liuser').attr('id');
        }       
        var data={accion:'cambiarmicro',estado:'nopermitir',userremote:userremote};
        _cambiarmicro(userremote,data);
        connection.send(data);
    }).on('click','.userstatus',function(){
        var li=$(this).closest('li.dropdown');
        var iduser=li.attr('id');
        var mode=$(this).attr('data-mode'); 
        if(iduser==roomid&&mode!='M') return;        
        var _clasi=$('i',this).attr('class');        
        var data={accion:'cambiartypeuser', mode:mode,para:iduser,cls:_clasi}
        _cambiartypeuser(data);
        connection.send(data);
    }).on('click','.btnmicroprivado',function(){
        var userremote=$(this).closest('li').attr('id');
        var $btninfollamada=$('#vparticipantes li#'+userremote+ ' .btn-group.tipoM .infollamada');
        var $btninfollamada2=$('#vparticipantes li#'+userremote+ ' .btn-group.tipoU .infollamada');
        btnmicro=$('#menupersonalizado a.btnsharemicro');        
        var data={accion:'llamadaprivada'}
        if($btninfollamada.hasClass('active')||$btninfollamada2.hasClass('active')){
            $btninfollamada.removeClass('active enprogreso').find('i').removeClass('colorgreen fa-volume-control-phone colored').addClass('fa-phone');
            $btninfollamada2.removeClass('active enprogreso colorgreen fa-volume-control-phone colored').addClass('fa-phone').hide(0);
            data.estado='colgar';
            connection.send(data,userremote);
            try{if(_millamadaprivada) _millamadaprivada.getTracks()[0].stop();}catch(ex){console.log(ex)};
            var $hay=$('#micro_'+userremote);
            if($hay.length)$hay.remove();
        }else if($btninfollamada.hasClass('enprogreso')||$btninfollamada2.hasClass('enprogreso')){
            $btninfollamada.removeClass('active enprogreso').find('i').removeClass('colorgreen fa-volume-control-phone colored').addClass('fa-phone');
            $btninfollamada2.removeClass('active enprogreso colorgreen fa-volume-control-phone colored').addClass('fa-phone').hide(0);
            data.estado='enprogreso';
            connection.send(data,userremote);
            try{if(_millamadaprivada) _millamadaprivada.getTracks()[0].stop();}catch(ex){console.log(ex)};
            var $hay=$('#micro_'+userremote);
            if($hay.length)$hay.remove();           
        }else{            
            var audioSource=sessionStorage.getItem('audioInId')||true;
            var objaudio=audioSource==true?true:({deviceId: audioSource ? {exact: audioSource} : undefined});
            if(_millamadaprivada) _millamadaprivada.getTracks()[0].stop();
            navigator.mediaDevices.getUserMedia({audio:objaudio,video:false}).then(function(miaudio){
                $btninfollamada.removeClass('active').addClass('enprogreso').find('i').addClass('fa-volume-control-phone colored').removeClass('fa-phone colorgreen');
                $btninfollamada2.removeClass('active colorgreen fa-phone colorgreen').addClass('enprogreso fa-volume-control-phone colored').show(0);
                if(btnmicro.hasClass('active')){
                    btnmicro.find('i').removeClass('fa-microphone colorgreen').addClass('fa-microphone-slash');
                    if(_miaudio) _miaudio.getTracks()[0].stop();
                    var data={accion:'stopmicro'};
                    connection.send(data);
                    _stopmicro(connection.userid); 
                }
                _millamadaprivada=miaudio;
                connection.extra.emit='llamadaprivada';
                connection.extra.emituser=connection.userid;
                connection.updateExtraData();
                if(connection.peers[_userid]==undefined) return;
                var peer = connection.peers[userremote].peer;
                peer.addStream(miaudio);
                connection.dontAttachStream = true;
                connection.renegotiate(userremote);
                connection.dontAttachStream = false;
            });
        }        
    });

    $('.btnshareventana').click(function(){
        var iobj=$('i',this);
        if(iobj.hasClass('fa-share')){
            iobj.removeClass('fa-share').addClass('fa-share-alt');
            $('#tmpscreenshot').removeAttr('id');          
            $(this).closest('.ventanas').find('.panel-body').attr('id','tmpscreenshot');
            if($('#tmpscreenshot').length>0){
                _sysshareScreen=true;
                connection.sharePartOfScreen(document.getElementById('tmpscreenshot'));
            }
        }else{
            iobj.removeClass('fa-share-alt').addClass('fa-share');
            connection.stopPartOfScreenSharing();
        }
    })

    function enviarstrean(tipo,stream){
        connection.extra.emit=tipo;
        connection.extra.emituser=connection.userid;
        connection.updateExtraData();
        if(useremit[connection.userid]!=undefined){
            _put=useremit[connection.userid]._put;
            _get=useremit[connection.userid]._get;
            if(_put!=''){
                if(connection.peers[_put]==undefined) return;
                var peer = connection.peers[_put].peer||false;
                if(peer==false) return;
                peer.addStream(stream);
                connection.dontAttachStream = true;
                connection.renegotiate(_put);
                connection.dontAttachStream = false;
            }
            if(_get!=''){
                if(connection.peers[_get]==undefined) return;
                var peer = connection.peers[_get].peer||false;
                if(peer==false) return;
                peer.addStream(stream);
                connection.dontAttachStream = true;
                connection.renegotiate(_get);
                connection.dontAttachStream = false;
            }
        }
    }

    function _addmicroicon(userid){
        $('#usersparticipante li#'+userid+' .sysmenu i.vericon').addClass('fa-microphone colorgreen').removeClass('fa-microphone-slash');
        $('#usersparticipante li#'+userid+' .infomicro').show();
    }

    function _sysactivarmicro(btnmicro){
        var audioSource=sessionStorage.getItem('audioInId')||true;
        var objaudio=audioSource==true?true:({deviceId: audioSource ? {exact: audioSource} : undefined});
        navigator.mediaDevices.getUserMedia({audio:objaudio,video:false}).then(function(miaudio){
            btnmicro.find('i').removeClass('fa-microphone-slash').addClass('fa-microphone colorgreen');
            if(_miaudio) _miaudio.getTracks()[0].stop();            
            _miaudio=miaudio;
            var audiotmp=document.createElement('audio');
            audiotmp.srcObject=miaudio;
            audiotmp.id=connection.userid;
            _userid=connection.userid;
            mediosemit['micro_'+userid]={userid:_userid, tipo:'micro',stream:_miaudio,de:'yo'};
            enviarstrean('micro',miaudio);
        });
    }

    function _sysactivarcamara(btn){
        var videoSource=sessionStorage.getItem('videoInId')||true;
        var objvideo=videoSource==true?true:({deviceId: videoSource ? {exact: videoSource} : undefined});
        $('#vwebcam').fadeIn(500);
        navigator.mediaDevices.getUserMedia({audio:false,video:objvideo}).then(function(mivideo){
            $('.btnsharecamera').find('i').addClass('colorgreen');
            mivideo.type='local';
            if(_miwebcam) _miwebcam.getTracks()[0].stop();
            _miwebcam=mivideo;
            _userid=connection.userid;
            mediosemit['camera_'+_userid]={userid:_userid, tipo:'camera',stream:_miwebcam,de:'yo'};
            _addcamera(mivideo,connection.userid);
            enviarstrean('camera',mivideo);
        });
    }

    $('.btnsharemicro').click(function(ev){
        ev.preventDefault(); 
        var btnmicro=$('#menupersonalizado a.btnsharemicro');
        btnmicro.toggleClass('active');
        if(btnmicro.hasClass('active')){
            /*if(_miscreen){
                //_miscreen.getAudioTracks()[0].enabled = true;
                btnmicro.find('i').removeClass('fa-microphone-slash').addClass('fa-microphone colorgreen');
                var data={accion:'addmicroicon'};
                connection.send(data);
            }else*/ _sysactivarmicro(btnmicro);
        }else{
             _userid=connection.userid;
            btnmicro.find('i').removeClass('fa-microphone colorgreen').addClass('fa-microphone-slash');
             // if(_miscreen)_miscreen.getAudioTracks()[0].enabled = false;
            if(_miaudio){ 
                _miaudio.getTracks()[0].stop(); _miaudio='';               
                delete mediosemit['micro_'+_userid];
            }
            var data={accion:'stopmicro'};
            connection.send(data);
            _stopmicro(_userid); 
        }
    });

    var _miaudio=_miwebcam=_miaudioscreen=_miscreen='';
    $('.btnsharecamera').click(function(ev){
        ev.preventDefault();
        var btn=$('#menupersonalizado a.btnsharecamera');
        btn.toggleClass('active');
        if(btn.hasClass('active')){
            _sysactivarcamara(btn)    
        }else{
            _userid=connection.userid;
            $('.btnsharecamera').find('i').removeClass('colorgreen');
            if(_miwebcam) {
                _miwebcam.getTracks()[0].stop();
                delete mediosemit['camera_'+_userid];
            }
            var data={accion:'stopcamera'};
            connection.send(data);
            _stopcamera(_userid);      
        }
    });

    var userscreen='';
    $('.btnsharedesktop').click(function(ev){
        ev.preventDefault(); 
        var btn=$('#menupersonalizado a.btnsharedesktop');
        var btnmicro=$('#menupersonalizado a.btnsharemicro');
        btn.toggleClass('active');
        if(btn.hasClass('active')){
            var audioSource=sessionStorage.getItem('audioInId')||true;
            var objaudio=audioSource==true?true:({deviceId: audioSource ? {exact: audioSource} : undefined});
            //if(btnmicro.hasClass('active')) btnmicro.trigger('click');
            if(_miaudioscreen) _miaudioscreen.stop();
            if(_miscreen)_miscreen.stop();
            if(userscreen!='')_stopscreen(userscreen);
            getScreenId(function (error, sourceId, screen_constraints){
              navigator.getUserMedia(screen_constraints, function (miscreen){
               // navigator.getUserMedia({audio: objaudio}, function (audioStream){
                    //_miaudioscreen=audioStream;
                    userscreen=connection.userid;
                   // miscreen.addTrack(audioStream.getAudioTracks()[0]);
                    _miscreen=miscreen;
                    _userid=connection.userid;
                    mediosemit['screen_'+_userid]={userid:_userid,tipo:'screen',stream:_miscreen,de:'yo'};
                   // var btnmicro=$('#menupersonalizado a.btnsharemicro');
                    //if(!btnmicro.hasClass('active')) _miscreen.getAudioTracks()[0].enabled = false;
                    $('.btnsharedesktop').find('i').addClass('colorgreen');                   
                   enviarstrean('screen',_miscreen);
                /*}, function(error) {
                  console.log(error);
                });*/
              }, function (error) {
                console.log(error);
              });
            });
        }else{
            $('.btnsharedesktop').find('i').removeClass('colorgreen');
            _userid=connection.userid;
            if(_miaudioscreen) {_miaudioscreen.stop(); _miaudioscreen='';}
            if(_miscreen){_miscreen.stop(); _miscreen=''; delete mediosemit['screen_'+_userid]; }
            var data={accion:'stopscreen'};
            connection.send(data);
            var btnmicro=$('#menupersonalizado a.btnsharemicro');
            if(btnmicro.hasClass('active')) _sysactivarmicro(btnmicro);
        }
    });

    var recorder = new MRecordRTC();
    recorder.mediaType = {
        audio: true, // or StereoAudioRecorder
        video: true, // or WhammyRecorder
    };

    var recorderstart=false;
    var recorderstream=null;
    $('.btnsharegrabar').click(function(ev){
        var btn=$(this);
        var icon=$('i',btn);
        icon.toggleClass('active');
        if(icon.hasClass('active')){                        
            recorderstart = true;
            getScreenId(function (error, sourceId, screen_constraints) {
              navigator.getUserMedia = navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
              navigator.getUserMedia(screen_constraints, function (stream) {
                navigator.getUserMedia({audio: true}, function (audioStream) {
                  stream.addTrack(audioStream.getAudioTracks()[0]);
                  icon.addClass('zoomIn colored');
                  recorder.addStream(stream);
                  recorder.startRecording();
                  recorderstream=stream;
                }, function (error) {
                  console.log(error);
                });
              }, function (error) {
                console.log(error);
              });
            });
        }else{
            recorderstart = false;
            icon.removeClass('zoomIn colored');        
            recorder.stopRecording(function(){
                var blob = recorder.getBlob();
                var name=$('#infousersala ._salaid').text()+"_video";
                var file = new File([blob.video], name+'.webm', { type: 'video/webm' });
                recorder.save(blob);
                recorder.writeToDisk(blob);              
                recorderstream.stop();
            });
        }
    });

    $('.btnsharepdfinboard').click(function(ev){
        var file=document.createElement('input');
        file.setAttribute("type", "file");
        file.setAttribute("accept", "application/pdf");
        file.click();
        $(file).change(function(){       
            if(['application/pdf'].indexOf($(this).get(0).files[0].type) == -1) {
                alert('Error : Not a PDF');
                return;
            }
            showPDF(window.URL.createObjectURL($(this).get(0).files[0]));
            $('#vpizarra').show();
            $('.showwindow[data-ventana="vpizarra"]').addClass('active');
            $('.btninpdf').show();
        });
    });

    $("#pdf-prev").on('click', function() {
        if(__PDFCURRENT_PAGE != 1)  showPage(--__PDFCURRENT_PAGE);
    });

    // Next page of the PDF
    $("#pdf-next").on('click', function() {
        if(__PDFCURRENT_PAGE != __PDFTOTAL_PAGES) showPage(++__PDFCURRENT_PAGE);
    });
   
    $('.btnsharevideo').click(function(){
        var file=document.createElement('input');
        file.setAttribute("type", "file");
        file.setAttribute("accept", "video/*");
        file.click();    
        $(file).change(function(){        
            _newfile=$(this).get(0).files[0];
            subirfile(_newfile,function(data){
                var midata={
                    accion:'addsharevideo',
                    namefile:data.namefile,
                    extension:data.extension,
                    userremote:connection.extra.user
                  }
                  var userremote=connection.extra.user;
                  addsharevideo(userremote,midata);
                  connection.send(midata);
            });
        });
    })

    $('.btnsharefile').click(function(){
        var file=document.createElement('input');
        file.setAttribute("type", "file");
        //file.setAttribute("accept", "image/*");
        file.click();    
        $(file).change(function(){        
            _newfile=$(this).get(0).files[0];
            subirfile(_newfile,function(data){

                var midata={
                    accion:'addsharefile',
                    namefile:data.namefile,
                    extension:data.extension,
                    userremote:connection.extra.user
                  }
                  var userremote=connection.extra.user;
                  addsharefile(userremote,midata);
                  connection.send(midata);
                  $('#vfiles').show('fast');
            });           
        });
    });

    $('#vchat #txtnewmensaje').keyup(function(e){
        var txt=$(this).val();
        if (e.which!=13) return;
        if (!txt.length) return;
        var data={
            accion:'addmensajechat',
            texto:txt,
            para:$('#vchat #userchatsms').val()
        }
        $(this).val('');
        _addmensajechat(data,true);
        if(data.para=='alluser') connection.send(data);
        else connection.send(data,data.para);
    });

    var recordersend=new MRecordRTC();
    recordersend.mediaType = { audio: true, video: true};
    recorder.mimeType = { audio: 'audio/wav'}    
    
    var _syssubirblob_=function(data,fn){
        var reader=new FileReader();
        reader.onload = function(event){
            fn(event.target.result);
            //console.log('data',data,event.target.result);
           /* var dt = new FormData();
            dt.append('name',data.name);
            dt.append('type',data.type);
            dt.append('filearchivo', event.target.result); */     
           
            /*$.ajax({
                url : _sysUrlBase_+"/aulavirtual/subirblob",
                type: 'POST',
                data: dt,
                contentType: false,
                processData: false,
                success: function(res) {
                   fn(res);
                },    
                error: function(e) {
                    console.log('Error inesperado intententelo mas tarde'+e);
                }
            });*/
        }
        reader.readAsDataURL(data.filearchivo);        
    }


    // var miblob=blobs['audio_'+oi];    
    
    $('#vchat .btnchatmicro').click(function(e){
        let btn=$(this).children('i');
        if(btn.hasClass('fa-microphone-slash')){
            navigator.mediaDevices.getUserMedia({audio:true,video:false}).then(function(miaudio){
                recordersend.addStream(miaudio);
                recordersend.startRecording();
                btn.removeClass('fa-microphone-slash').addClass('fa-microphone text-success');
            })            
        }else{
             recordersend.stopRecording(function(){
                var blob = recordersend.getBlob().audio;
                //console.log(recordersend.getDataURL());
                //console.log('urlBlob',blob);
                //var urlBlob=URL.createObjectURL(blob);
                //console.log('urlBlob',urlBlob);
                _syssubirblob_({filearchivo:blob,name:'audio',type:'mp3'},function(rs){

                    btn.removeClass('fa-microphone text-success').addClass('fa-microphone-slash');
                    
                     var data={
                        accion:'addmensajechat',
                        texto:'Nuevo Audio <span class="btn btn-sm btn-primary reproduciraudio" data-audio="'+rs+'"><i class="fa fa-play"></i></span>',
                        para:$('#vchat #userchatsms').val()
                    }
                    _addmensajechat(data,true);
                    if(data.para=='alluser') connection.send(data);
                    else connection.send(data,data.para);
                });
               /* */
               
             });           
        }
    });

    $('#vchat .btnchatfile').click(function(e){       
        var file=document.createElement('input');
        file.setAttribute("type", "file");
        //file.setAttribute("accept", "image/*");
        file.click();
        $(file).change(function(){        
            _newfile=$(this).get(0).files[0];
            subirfile(_newfile,function(data){
                 var data={
                    accion:'addmensajechat',
                    texto:'Envie un archivo<br><a style="color:#fff" href="'+data.namefile+'" download="'+connection.extra.user+'_'+data.namefile+'">Descargalo aqui</a>',
                    para:$('#vchat #userchatsms').val()
                }
                _addmensajechat(data,true);
                if(data.para=='alluser') connection.send(data);
                else connection.send(data,data.para);
            })
        });        
    })

    $('#vchat').on('click','.reproduciraudio',function(e){
        let i=$(this).children('i');
        let txt=$(this).attr('data-audio');
        let audio=$('#vchat #audioenviadochat');
        audio.attr('src',txt);
        audio.trigger('play');
        audio.off('playing');
        audio.on('playing', function() {
          i.removeClass('fa-play').addClass('fa-pause');
        });
        audio.on('ended', function() {
           playing = false;
           i.addClass('fa-play').removeClass('fa-pause');
           // enable button/link
        });
    })
  

    $('#vencuesta').on('click','.btn_encuesta_enviar',function(){      
        if($(this).hasClass('disabled'))return false;
        $(this).addClass('disabled');
        var preg=$('#vencuesta #pregunta').val();
        if(preg=='') return false;
        var alt={};
        $('#vencuesta table tr.si input').each(function(){
            var alt_=$(this).val();
            if(alt_!=''){
                alt[alt_]=0;
            }
        });
        var idencuesta=userid+'_'+Date.now();
        sysencuestas[idencuesta]={accion:'tomarencuesta',idencuesta:idencuesta,pregunta:preg,alternativas:alt,idsala:1};
        connection.send(sysencuestas[idencuesta]);
        var addencuesta='<div class="encuesta" data-id="'+idencuesta+'">'+preg+'<br><a href="#" class="btnverresultadoencuesta">ver respuestas <i class="fa fa-eye"></i></a>';
            addencuesta+=' <a href="#" class="btneliminarencuesta">Eliminar <i class="fa fa-trash"></i></a></div><br>';//  <a href="#" class="">Guardar <i class="fa fa-save"></i></a>'
        $('#vencuesta .resultadoencuesta').append(addencuesta);
        $(this).removeClass('disabled');
    }).on('click','.btnverresultadoencuesta',function(){
        var encuesta=$(this).closest('.encuesta').attr('data-id');
        if(sysencuestas[encuesta])
            var resencuesta=sysencuestas[encuesta];
            if(resencuesta){
                var alternativas=resencuesta.alternativas; 
                var modal='';
                if($('#modaltmp123').length) modal=$('#modaltmp123');
                else modal=$('#modalquestion').clone();
                modal.find('.modal-title').text('Graficos :');
                modal.find('.modal-dialog').removeClass('modal-sm');
                modal.attr('id','modaltmp123');
                modal.find('.modal-body').html('<div><strong>'+resencuesta.pregunta+'</strong></div><div class="graficoPie_'+encuesta+' col-md-6" ></div><div class="col-md-6 graficoBarra_'+encuesta+'" ></div>');
                modal.find('.modal-footer').html('<a href="#" class="btncerrarmodal100">Cerrar</a>');
                modal.modal('show'); 
                modal.on('shown.bs.modal',function(){
                    var series_=[];
                    var labels_=[];
                    var labels2_=[];
                    $.each(alternativas,function(i,v){
                        labels_.push(i+' '+v);
                        labels2_.push(i);
                        series_.push(v);
                    });
                    modal.find('.modal-body').removeAttr('style');
                    var data={ labels: labels_, series: series_};
                    var opciones={ fullWidth: true,chartPadding: 2, showLabel: true, labelOffset: 10, startAngle: 0, };                
                    var chartpie = new Chartist.Pie('.graficoPie_'+encuesta,data,opciones); 
                    chartpie.update();                     
                    var databar={ labels: labels2_, series: [series_]};
                    var chartbar = new Chartist.Bar('.graficoBarra_'+encuesta,databar,{seriesBarDistance: 10}); 
                    chartbar.update();  
                });
            }
    }).on('click','.btneliminarencuesta',function(){
        $(this).closest('.encuesta').remove();
    });

    $('#pantalla1').on('click','#showiconos i.em',function(){    
        var data={};
        data.icon=$(this).attr('class');
        data.accion='mostraricono';
        data.titulo=$(this).attr('title');
        data.sonido=$(this).attr('data-audio');
        mostraricono(connection.userid,data)
        connection.send(data);
    });

    $('.btnlokedroom').click(function(){
        var i = $('i',this);
        if(i.hasClass('fa-lock')){
            i.removeClass('fa-lock').addClass('fa-unlock');
            connection.send({accion:'updatesala',estado:false});
            modificarsala('estado','AB');
        }else {            
            i.removeClass('fa-unlock').addClass('fa-lock');
            connection.send({accion:'updatesala',estado:true});
            modificarsala('estado','BL');
        }
    })

    $('.btnsaliraula').click(function(){  
        salirsala=true;
        connection.leave();
        connection.close();
        sessionStorage.clear();
        window.location.href=(_sysUrlBase_);        
    }),

    $('body').on('click','a.btnresponder',function(ev){
        ev.preventDefault();
        var modal=$(this).closest('.modal');
        var encuesta={};
            encuesta.accion='responderencuesta';
            encuesta.id=modal.attr('id');
            encuesta.respuesta=$(this).attr('data-value');
            modal.modal('hide');
            connection.send(encuesta);
    }).on('click','a.btncerrarmodal100',function(ev){
        var modal=$(this).closest('.modal');
        modal.modal('hide');
    });

    function inisala(){
        connection.sessionid=roomid;
        connection.userid=connection.token();
        connection.checkPresence(roomid, function(isRoomExists){
             if(!isRoomExists){            
                connection.open(roomid,function(){
                    console.log('sala : '+roomid+': noexiste : open');
                    userid=connection.userid=roomid;
                    savedatosuser();
                    addparticipantes(datosuser,true);
                });
             }else{            
                connection.connectionDescription=connection.join(roomid,function(){
                    console.log('sala : '+roomid+': existe : join');
                    userid=connection.userid;
                    savedatosuser();
                    addparticipantes(datosuser,true);
                    connection.renegotiate();
                });
             }
        });
    }
    inisala(); 

    function reconectarasala(num,cont,contno){
        var cont=cont||0;
        var contno=contno||0;
        console.log('reconectando con sala');
        if(cont==2){
            connection.rejoin(connection.connectionDescription);  
            connection.renegotiate();
            return console.log('sala ok',num,cont);
        }else if(contno>10){
            setTimeout(function(){reconectarasala(1,0,9)},60000);
            return console.log('autor de sala ya no se reconecto');
        }
        connection.checkPresence(roomid, function(isRoomExists,roomid){        
             if(isRoomExists){
                num++;
                cont++;
                setTimeout(function(){reconectarasala(num,cont,0)},num*3000);
             }else{
                contno++;
                setTimeout(function(){reconectarasala(1,0,contno)},5000);
             }         
        });
    } 

    $(window).on("beforeunload",function(){
       try {
            connection.getAllParticipants().forEach(function(pid) {
                connection.disconnectWith(pid);
            });
            connection.attachStreams.forEach(function(localStream) {
                localStream.stop();
            });
            connection.closeEntireSession();
            connection.leave();
            connection.close();
        }catch(err){}   
    });
});