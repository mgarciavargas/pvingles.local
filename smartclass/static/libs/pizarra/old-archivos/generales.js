function medidas(id){
    return {w:document.getElementById(id).offsetWidth,h:document.getElementById(id).offsetHeight}
}

PNotify.desktop.permission();
function notificar(user,titulo,texto,tipo,link){
	var user=user||'';
	var texto=texto||'';
	var titulo=titulo||'';
	var tipo=tipo||'info';
	var link=link||'';
	titulo=''+user+' : '+titulo;

if(texto!='')
(new PNotify({
    title: titulo,
    text: texto,
    type: tipo,
    desktop: {
        desktop: true
    }
})).get().click(function(e) {
    if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
    //alert('Hey! You clicked the desktop notification!');
});
}

function askConfirmation (evt) {
  var msg = 'Si recarga la página perdera todos los datos ingresados.\n¿Deseas recargar la página?';
  evt.returnValue = msg;
  return msg;
}

(function() {
    var params = {},
        r = /([^&=]+)=?([^&]*)/g;
    function d(s) {
        return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1)))
        params[d(match[1])] = d(match[2]);
    window.params = params;
})();

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}
function geturl(){
    var loc = window.location;
    var pathname = window.location.pathname;
    return loc+pathname;
}


