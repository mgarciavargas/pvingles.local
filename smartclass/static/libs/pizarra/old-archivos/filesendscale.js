var connection = new RTCMultiConnection();
            // by default, socket.io server is assumed to be deployed on your own URL
            connection.socketURL = '/';

            // comment-out below line if you do not have your own socket.io server
            // connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

            connection.socketMessageEvent = 'files-scalable-broadcast-demo';

            connection.enableScalableBroadcast = true;
            connection.maxRelayLimitPerUser = 1;

            connection.fileReceived = {};
            connection.enableFileSharing = true;
            connection.chunkSize = 60 * 1000;

            document.getElementById('broadcast-id').value = connection.token();
            connection.filesContainer = document.getElementById('files-container');
            connection.sdpConstraints.mandatory = {
                OfferToReceiveAudio: false,
                OfferToReceiveVideo: false
            };

            connection.onopen = function(event) {
                if (connection.isInitiator) {
                    if (connection.selectedFile) {
                        connection.send(connection.selectedFile, event.userid);
                    }
                    return;
                }

                document.querySelector('input[type=file]').disabled = true;

                if (connection.isInitiatorConnected && connection.lastFile) {
                    connection.send(connection.lastFile, event.userid);
                }

                if (!connection.isInitiatorConnected) {
                    connection.initiatorId = event.userid;
                    connection.isInitiatorConnected = true;
                }
            };

            // ask node.js server to look for a broadcast
            // if broadcast is available, simply join it. i.e. "join-broadcaster" event should be emitted.
            // if broadcast is absent, simply create it. i.e. "start-broadcasting" event should be fired.
            document.getElementById('open-or-join').onclick = function() {
                var broadcastId = document.getElementById('broadcast-id').value;
                if (broadcastId.replace(/^\s+|\s+$/g, '').length <= 0) {
                    alert('Please enter broadcast-id');
                    document.getElementById('broadcast-id').focus();
                    return;
                }

                document.getElementById('open-or-join').disabled = true;

                connection.session = {
                    data: true,
                    oneway: true
                };

                connection.socket.emit('join-broadcast', {
                    broadcastId: broadcastId,
                    userid: connection.userid,
                    typeOfStreams: connection.session
                });
            };

            connection.connectSocket(function(socket) {
                connection.socket = socket;
                document.getElementById('open-or-join').disabled = false;

                // this event is emitted when a broadcast is already created.
                connection.socket.on('join-broadcaster', function(hintsToJoinBroadcast) {
                    connection.session = hintsToJoinBroadcast.typeOfStreams;
                    connection.sdpConstraints.mandatory = {
                        OfferToReceiveVideo: !!connection.session.video,
                        OfferToReceiveAudio: !!connection.session.audio
                    };

                    connection.join(hintsToJoinBroadcast.userid);
                });

                // this event is emitted when a broadcast is absent.
                connection.socket.on('start-broadcasting', function(typeOfStreams) {
                    // host i.e. sender should always use this!
                    connection.sdpConstraints.mandatory = {
                        OfferToReceiveVideo: false,
                        OfferToReceiveAudio: false
                    };
                    connection.session = typeOfStreams;
                    connection.open(connection.userid, function() {
                        showRoomURL(connection.sessionid);
                    });
                });

                socket.on('logs', function(log) {
                    document.querySelector('h1').innerHTML = log.replace(/</g, '----').replace(/>/g, '___').replace(/----/g, '(<span style="color:red;">').replace(/___/g, '</span>)');
                });
            });

            window.onbeforeunload = function() {
                // Firefox is weird!
                document.getElementById('open-or-join').disabled = false;
            };

            var FileProgressBarHandler = (function() {
                function handle(connection) {
                    var progressHelper = {};

                    // www.RTCMultiConnection.org/docs/onFileStart/
                    connection.onFileStart = function(file) {
                        if (connection.fileReceived[file.name]) return;

                        var div = document.createElement('div');
                        div.id = file.uuid;
                        div.title = file.name;
                        div.innerHTML = '<label>0%</label> <progress></progress>';

                        if (file.remoteUserId) {
                            div.innerHTML += ' (Sharing with:' + file.remoteUserId + ')';
                        }

                        connection.filesContainer.insertBefore(div, connection.filesContainer.firstChild);

                        if (!file.remoteUserId) {
                            progressHelper[file.uuid] = {
                                div: div,
                                progress: div.querySelector('progress'),
                                label: div.querySelector('label')
                            };
                            progressHelper[file.uuid].progress.max = file.maxChunks;
                            return;
                        }

                        if (!progressHelper[file.uuid]) {
                            progressHelper[file.uuid] = {};
                        }

                        progressHelper[file.uuid][file.remoteUserId] = {
                            div: div,
                            progress: div.querySelector('progress'),
                            label: div.querySelector('label')
                        };
                        progressHelper[file.uuid][file.remoteUserId].progress.max = file.maxChunks;
                    };

                    // www.RTCMultiConnection.org/docs/onFileProgress/
                    connection.onFileProgress = function(chunk) {
                        if (connection.fileReceived[chunk.name]) return;

                        var helper = progressHelper[chunk.uuid];
                        if (!helper) {
                            return;
                        }
                        if (chunk.remoteUserId) {
                            helper = progressHelper[chunk.uuid][chunk.remoteUserId];
                            if (!helper) {
                                return;
                            }
                        }

                        helper.progress.value = chunk.currentPosition || chunk.maxChunks || helper.progress.max;
                        updateLabel(helper.progress, helper.label);
                    };

                    // www.RTCMultiConnection.org/docs/onFileEnd/
                    connection.onFileEnd = function(file) {
                        if (connection.fileReceived[file.name]) return;

                        if (file.userid == connection.userid) {
                            connection.fileReceived[file.name] = file;
                        }

                        var helper = progressHelper[file.uuid];
                        if (!helper) {
                            return;
                        }

                        if (file.remoteUserId) {
                            helper = progressHelper[file.uuid][file.remoteUserId];
                            if (!helper) {
                                return;
                            }
                        }

                        var div = helper.div;
                        if (file.type.indexOf('image') != -1) {
                            div.innerHTML = '<a href="' + file.url + '" download="' + file.name + '">Download <strong style="color:red;">' + file.name + '</strong> </a><br /><img src="' + file.url + '" title="' + file.name + '" style="max-width: 80%;">';
                        } else if (file.type.indexOf('video/') != -1) {
                            div.innerHTML = '<a href="' + file.url + '" download="' + file.name + '">Download <strong style="color:red;">' + file.name + '</strong> </a><br /><video src="' + file.url + '" title="' + file.name + '" style="max-width: 80%;" controls></video>';
                        } else if (file.type.indexOf('audio/') != -1) {
                            div.innerHTML = '<a href="' + file.url + '" download="' + file.name + '">Download <strong style="color:red;">' + file.name + '</strong> </a><br /><audio src="' + file.url + '" title="' + file.name + '" style="max-width: 80%;" controls></audio>';
                        } else {
                            div.innerHTML = '<a href="' + file.url + '" download="' + file.name + '">Download <strong style="color:red;">' + file.name + '</strong> </a><br /><iframe src="' + file.url + '" title="' + file.name + '" style="width: 80%;border: 0;height: inherit;margin-top:1em;"></iframe>';
                        }

                        if (!file.slice) {
                            return;
                        }

                        if (file.slice && file.userid !== connection.userid) {
                            connection.getAllParticipants().forEach(function(paricipantId) {
                                if (paricipantId != file.userid) {
                                    connection.send(file, paricipantId);
                                }
                            });
                            connection.lastFile = file;
                        }
                    };

                    function updateLabel(progress, label) {
                        if (progress.position === -1) {
                            return;
                        }

                        var position = +progress.position.toFixed(2).split('.')[1] || 100;
                        label.innerHTML = position + '%';
                    }
                }

                return {
                    handle: handle
                };
            })();

            FileProgressBarHandler.handle(connection);

            document.querySelector('input[type=file]').onchange = function() {
                var file = this.files[0];
                if (!file) return;

                file.uuid = connection.userid;
                connection.selectedFile = file;
                if (connection.isInitiator) {
                    if (connection.getAllParticipants().length > 0) {
                        connection.send(file);
                    }
                }
            };

            function disableInputButtons() {
                document.getElementById('open-or-join').disabled = true;
                document.getElementById('broadcast-id').disabled = true;
            }

            // ......................................................
            // ......................Handling broadcast-id................
            // ......................................................

            function showRoomURL(broadcastId) {
                var roomHashURL = '#' + broadcastId;
                var roomQueryStringURL = '?broadcastId=' + broadcastId;

                var html = '<h2>Unique URL for your room:</h2><br>';

                html += 'Hash URL: <a href="' + roomHashURL + '" target="_blank">' + roomHashURL + '</a>';
                html += '<br>';
                html += 'QueryString URL: <a href="' + roomQueryStringURL + '" target="_blank">' + roomQueryStringURL + '</a>';

                var roomURLsDiv = document.getElementById('room-urls');
                roomURLsDiv.innerHTML = html;

                roomURLsDiv.style.display = 'block';
            }

            (function() {
                var params = {},
                    r = /([^&=]+)=?([^&]*)/g;

                function d(s) {
                    return decodeURIComponent(s.replace(/\+/g, ' '));
                }
                var match, search = window.location.search;
                while (match = r.exec(search.substring(1)))
                    params[d(match[1])] = d(match[2]);
                window.params = params;
            })();

            var broadcastId = '';
            if (localStorage.getItem(connection.socketMessageEvent)) {
                broadcastId = localStorage.getItem(connection.socketMessageEvent);
            } else {
                broadcastId = connection.token();
            }
            document.getElementById('broadcast-id').value = broadcastId;
            document.getElementById('broadcast-id').onkeyup = function() {
                localStorage.setItem(connection.socketMessageEvent, this.value);
            };

            var hashString = location.hash.replace('#', '');
            if(hashString.length && hashString.indexOf('comment-') == 0) {
              hashString = '';
            }

            var broadcastId = params.broadcastId;
            if(!broadcastId && hashString.length) {
                broadcastId = hashString;
            }

            if(broadcastId && broadcastId.length) {
                document.getElementById('broadcast-id').value = broadcastId;
                localStorage.setItem(connection.socketMessageEvent, broadcastId);

                // auto-join-room
                (function reCheckRoomPresence() {
                    connection.checkPresence(broadcastId, function(isRoomExists) {
                        if(isRoomExists) {
                            document.getElementById('open-or-join').onclick();
                            return;
                        }

                        setTimeout(reCheckRoomPresence, 5000);
                    });
                })();

                disableInputButtons();
            }