var connection = new RTCMultiConnection();
connection.socketURL = 'http://sirvoz.com';
connection.socketMessageEvent = 'midemotest';
connection.autoCloseEntireSession = true;
connection.enableScalableBroadcast = true;
connection.maxRelayLimitPerUser = 1;
connection.fileReceived = {};
connection.enableFileSharing = true;
connection.chunkSize = 60 * 1000;
roomid=roomid||$('#infousersala ._sala').text();
userid=userid||$('#infousersala ._user').text();

connection.filesContainer = document.getElementById('files-container');

connection.sdpConstraints.mandatory = {
    OfferToReceiveAudio: false,
    OfferToReceiveVideo: false
};

connection.onopen = function(event) {
    console.log(event);
    if (connection.isInitiator) {
        if (connection.selectedFile) {
            connection.send(connection.selectedFile, event.userid);
        }
        return;
    }

    if (connection.isInitiatorConnected && connection.lastFile) {
        connection.send(connection.lastFile, event.userid);
    }

    if (!connection.isInitiatorConnected) {
        connection.initiatorId = event.userid;
        connection.isInitiatorConnected = true;
    }
};

connection.connectSocket(function(socket){
    connection.socket = socket;
   
    // this event is emitted when a broadcast is already created.
    connection.socket.on('join-broadcaster', function(hintsToJoinBroadcast) {
        connection.session = hintsToJoinBroadcast.typeOfStreams;
        connection.sdpConstraints.mandatory = {
            OfferToReceiveVideo: !!connection.session.video,
            OfferToReceiveAudio: !!connection.session.audio
        };

        connection.join(hintsToJoinBroadcast.userid);
    });

    // this event is emitted when a broadcast is absent.
    connection.socket.on('start-broadcasting', function(typeOfStreams) {
        // host i.e. sender should always use this!
        connection.sdpConstraints.mandatory = {
            OfferToReceiveVideo: false,
            OfferToReceiveAudio: false
        };
        connection.session = typeOfStreams;
        connection.open(connection.userid, function() {
            console.log(connection.sessionid);
        });
    });

    socket.on('logs', function(log) {
        cosole.log(log);
    });
});

var FileProgressBarHandler = (function() {
    function handle(connection) {
        var progressHelper = {};

        // www.RTCMultiConnection.org/docs/onFileStart/
        connection.onFileStart = function(file) {
            if (connection.fileReceived[file.name]) return;

            var div = document.createElement('div');
            div.id = file.uuid;
            div.title = file.name;
            div.innerHTML = '<label>0%</label> <progress></progress>';

            if (file.remoteUserId) {
                div.innerHTML += ' (Sharing with:' + file.remoteUserId + ')';
            }

            connection.filesContainer.insertBefore(div, connection.filesContainer.firstChild);

            if (!file.remoteUserId) {
                progressHelper[file.uuid] = {
                    div: div,
                    progress: div.querySelector('progress'),
                    label: div.querySelector('label')
                };
                progressHelper[file.uuid].progress.max = file.maxChunks;
                return;
            }

            if (!progressHelper[file.uuid]) {
                progressHelper[file.uuid] = {};
            }

            progressHelper[file.uuid][file.remoteUserId] = {
                div: div,
                progress: div.querySelector('progress'),
                label: div.querySelector('label')
            };
            progressHelper[file.uuid][file.remoteUserId].progress.max = file.maxChunks;
        };

        // www.RTCMultiConnection.org/docs/onFileProgress/
        connection.onFileProgress = function(chunk) {
            if (connection.fileReceived[chunk.name]) return;

            var helper = progressHelper[chunk.uuid];
            if (!helper) {
                return;
            }
            if (chunk.remoteUserId) {
                helper = progressHelper[chunk.uuid][chunk.remoteUserId];
                if (!helper) {
                    return;
                }
            }

            helper.progress.value = chunk.currentPosition || chunk.maxChunks || helper.progress.max;
            updateLabel(helper.progress, helper.label);
        };

        // www.RTCMultiConnection.org/docs/onFileEnd/
        connection.onFileEnd = function(file) {
            if (connection.fileReceived[file.name]) return;

            if (file.userid == connection.userid) {
                connection.fileReceived[file.name] = file;
            }

            var helper = progressHelper[file.uuid];
            if (!helper) {
                return;
            }

            if (file.remoteUserId) {
                helper = progressHelper[file.uuid][file.remoteUserId];
                if (!helper) {
                    return;
                }
            }

            var div = helper.div;
            if (file.type.indexOf('image') != -1) {
                div.innerHTML = '<a href="' + file.url + '" download="' + file.name + '">Download <strong style="color:red;">' + file.name + '</strong> </a><br /><img src="' + file.url + '" title="' + file.name + '" style="max-width: 80%;">';
            } else if (file.type.indexOf('video/') != -1) {
                div.innerHTML = '<a href="' + file.url + '" download="' + file.name + '">Download <strong style="color:red;">' + file.name + '</strong> </a><br /><video src="' + file.url + '" title="' + file.name + '" style="max-width: 80%;" controls></video>';
            } else if (file.type.indexOf('audio/') != -1) {
                div.innerHTML = '<a href="' + file.url + '" download="' + file.name + '">Download <strong style="color:red;">' + file.name + '</strong> </a><br /><audio src="' + file.url + '" title="' + file.name + '" style="max-width: 80%;" controls></audio>';
            } else {
                div.innerHTML = '<a href="' + file.url + '" download="' + file.name + '">Download <strong style="color:red;">' + file.name + '</strong> </a><br /><iframe src="' + file.url + '" title="' + file.name + '" style="width: 80%;border: 0;height: inherit;margin-top:1em;"></iframe>';
            }

            if (!file.slice) {
                return;
            }

            if (file.slice && file.userid !== connection.userid) {
                connection.getAllParticipants().forEach(function(paricipantId) {
                    if (paricipantId != file.userid) {
                        connection.send(file, paricipantId);
                    }
                });
                connection.lastFile = file;
            }
        };

        function updateLabel(progress, label) {
            if (progress.position === -1) {
                return;
            }

            var position = +progress.position.toFixed(2).split('.')[1] || 100;
            label.innerHTML = position + '%';
        }
    }

    return {
        handle: handle
    };
})();

FileProgressBarHandler.handle(connection);

document.querySelector('input[type=file]').onchange = function() {
    var file = this.files[0];
    if (!file) return;

    file.uuid = connection.userid;
    connection.selectedFile = file;
    if (connection.isInitiator) {
        if (connection.getAllParticipants().length > 0) {
            connection.send(file);
        }
    }
};


function opensala(){
    var broadcastId = roomid;
    var jsonuser={           
        username:userid,
        userfullname:$('#infousersala ._username').text(),
        useremail:$('#infousersala ._useremail').text(),
        typeuser:tipouser
    }
    //connection.userid=userid;
    connection.session = { audio: false, video: false, screen:false, data:true , oneway: true };
    var socket = connection.getSocket();
    socket.emit('check-broadcast-presence', broadcastId, function(isBroadcastExists){
        console.log(broadcastId,isBroadcastExists,1);
        if (!isBroadcastExists) {// the first person (i.e. real-broadcaster) MUST set his user-id    
        console.log('aaa');        
            connection.userid = broadcastId;
            connection.extra=jsonuser;
        }           
        socket.emit('join-broadcast', {
            broadcastId: broadcastId,
            userid: connection.userid,
            extra:jsonuser,
            typeOfStreams: connection.session
        });
    });
}
opensala();