-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 19-09-2017 a las 10:52:27
-- Versión del servidor: 5.6.35
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sirvoz_data`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agenda`
--

CREATE TABLE IF NOT EXISTS `agenda` (
  `idagenda` int(11) NOT NULL,
  PRIMARY KEY (`idagenda`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE IF NOT EXISTS `alumno` (
  `dni` int(11) NOT NULL,
  `ape_paterno` varchar(50) NOT NULL,
  `ape_materno` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fechanac` date DEFAULT NULL,
  `sexo` char(1) NOT NULL,
  `estado_civil` char(1) NOT NULL,
  `ubigeo` char(8) NOT NULL,
  `urbanizacion` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `email` varchar(75) NOT NULL,
  `idugel` char(4) NOT NULL,
  `regusuario` int(11) NOT NULL,
  `regfecha` date NOT NULL,
  `usuario` varchar(35) NOT NULL,
  `clave` varchar(35) NOT NULL,
  `token` varchar(35) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `situacion` tinyint(1) NOT NULL,
  PRIMARY KEY (`dni`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`dni`, `ape_paterno`, `ape_materno`, `nombre`, `fechanac`, `sexo`, `estado_civil`, `ubigeo`, `urbanizacion`, `direccion`, `telefono`, `celular`, `email`, `idugel`, `regusuario`, `regfecha`, `usuario`, `clave`, `token`, `foto`, `estado`, `situacion`) VALUES
(12345678, 'Chingo', 'Tello', 'Abel', '1986-10-27', 'M', 'V', '', '', '', '', '', 'abel_chingo@hotmail.com', '', 0, '2016-11-11', 'alumno', '81dc9bdb52d04dc20036dbd8313ed055', '', '', 1, 1),
(72042592, 'Figueroa', 'Piscoya', 'Eder', '1992-12-21', '', '', '', '', '', '', '', 'edermax1992@gmail.com', '', 0, '2016-11-12', 'efigueroap', '81dc9bdb52d04dc20036dbd8313ed055', '', '', 1, 1),
(87654321, 'muñoz', 'MEZA', 'evelio', '2016-11-01', 'M', 'S', '00000001', 'BELLIDO', 'MEXICO', '251478', '979222660', 'CORREO', '0001', 1, '2016-11-01', 'emunoz', 'c4ca4238a0b923820dcc509a6f75849b', '43371672', '', 1, 1),
(11111111, 'System', 'master', 'user-alumno', NULL, '', '', '', '', '', '', '', 'email', '', 0, '2016-11-16', 'abel_alumno', '0192023a7bbd73250516f069df18b500', '', '', 0, 1),
(55555555, 'System', 'pvingles', 'docente3', '2017-02-01', 'M', 'S', '', '', '', '', '', 'docente3@hotmail.com', '', 0, '2017-02-09', 'docente3', '8be2efedf58bc5ca5ef33aec86a7a217', '8be2efedf58bc5ca5ef33aec86a7a217', '', 0, 1),
(44444444, 'System', 'pvingles', 'docente2', '2017-02-01', 'M', 'S', '', '', '', '', '', 'docente2@hotmail.com', '', 0, '2017-02-09', 'docente2', '8be2efedf58bc5ca5ef33aec86a7a217', '8be2efedf58bc5ca5ef33aec86a7a217', '', 0, 1),
(33333333, 'System', 'pvingles', 'a1', '2017-02-01', 'M', 'S', '', '', '', '', '', 'docente1@hotmail.com', '', 0, '2017-02-09', 'docente1', '8be2efedf58bc5ca5ef33aec86a7a217', '8be2efedf58bc5ca5ef33aec86a7a217', '', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ambiente`
--

CREATE TABLE IF NOT EXISTS `ambiente` (
  `idambiente` int(11) NOT NULL,
  `idlocal` int(11) NOT NULL,
  `numero` varchar(3) COLLATE utf8_spanish_ci DEFAULT NULL,
  `capacidad` int(11) NOT NULL DEFAULT '0',
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `estado` tinyint(4) DEFAULT NULL,
  `turno` char(1) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idambiente`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ambiente`
--

INSERT INTO `ambiente` (`idambiente`, `idlocal`, `numero`, `capacidad`, `tipo`, `estado`, `turno`) VALUES
(1, 1, '101', 10, 'L', 1, 'T'),
(2, 1, '401', 30, 'L', 1, 'M'),
(3, 1, '404', 20, 'L', 1, 'M');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aulasvirtuales`
--

CREATE TABLE IF NOT EXISTS `aulasvirtuales` (
  `aulaid` int(11) NOT NULL,
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_final` datetime NOT NULL,
  `titulo` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci,
  `moderadores` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(2) COLLATE utf8_spanish_ci NOT NULL COMMENT '0- creado, AB=abierto,CL=cerrado,BL=bloqueado',
  `video` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `chat` text COLLATE utf8_spanish_ci,
  `notas` text COLLATE utf8_spanish_ci,
  `dirigidoa` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `estudiantes` text COLLATE utf8_spanish_ci NOT NULL,
  `dni` int(8) NOT NULL,
  `fecha_creado` datetime NOT NULL,
  `portada` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `filtroestudiantes` text COLLATE utf8_spanish_ci,
  `nparticipantes` int(2) NOT NULL,
  PRIMARY KEY (`aulaid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `aulasvirtuales`
--

INSERT INTO `aulasvirtuales` (`aulaid`, `idnivel`, `idunidad`, `idactividad`, `fecha_inicio`, `fecha_final`, `titulo`, `descripcion`, `moderadores`, `estado`, `video`, `chat`, `notas`, `dirigidoa`, `estudiantes`, `dni`, `fecha_creado`, `portada`, `filtroestudiantes`, `nparticipantes`) VALUES
(1, 0, 0, 0, '2017-09-12 18:30:00', '2017-09-30 23:00:00', 'Herramientas del smartClass', 'Describiremos las herramientas del smartclass   para brindar clases virtuales a los estudiantes---', '{43831104}', 'AB', '', '', '', 'P', '', 43831104, '2017-09-12 18:17:52', '__xRUTABASEx__/static/media/image/pantalla.jpg', '', 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aulavirtualinvitados`
--

CREATE TABLE IF NOT EXISTS `aulavirtualinvitados` (
  `idinvitado` int(11) NOT NULL,
  `idaula` int(11) NOT NULL,
  `dni` int(11) DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `asistio` int(11) DEFAULT NULL,
  `como` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `usuario` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idinvitado`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `aulavirtualinvitados`
--

INSERT INTO `aulavirtualinvitados` (`idinvitado`, `idaula`, `dni`, `email`, `asistio`, `como`, `usuario`) VALUES
(5, 1, 0, 'eder.figueroa@outlook.com', 0, 'U', 'Eder Figueroa'),
(4, 1, 0, 'enrupf@gmail.com', 0, 'U', 'Enny Rup'),
(3, 1, 0, 'abelchingo@gmail.com', 0, 'U', 'Abel chingo 2'),
(2, 1, 0, 'abel_chingo@hotmail.com', 0, 'M', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE IF NOT EXISTS `configuracion` (
  `nivel` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `ventana` varchar(20) COLLATE utf8_spanish_ci NOT NULL COMMENT 'L=look,P=practice,D=dobyyourself',
  `atributo` char(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '' COMMENT 'Peso, Intentos',
  `valor` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`nivel`,`ventana`,`atributo`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`nivel`, `ventana`, `atributo`, `valor`) VALUES
('A1', 'L', 'P', '10'),
('A1', 'P', 'P', '10'),
('A1', 'D', 'P', '10'),
('A1', 'D', 'I', '3'),
('A2', 'D', 'I', '3'),
('B1', 'D', 'I', '3'),
('B2', 'D', 'I', '3'),
('C1', 'D', 'I', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE IF NOT EXISTS `grupos` (
  `idgrupo` char(9) COLLATE utf8_spanish_ci NOT NULL,
  `codigo` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `idproducto` int(11) NOT NULL,
  `iddocente` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idlocal` char(4) COLLATE utf8_spanish_ci NOT NULL,
  `idambiente` char(5) COLLATE utf8_spanish_ci NOT NULL,
  `idasistente` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `fechainicio` date NOT NULL,
  `fechafin` date DEFAULT NULL,
  `dias` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `horas` int(11) NOT NULL,
  `horainicio` time NOT NULL,
  `horafin` time NOT NULL,
  `valor` double NOT NULL,
  `valor_asi` double NOT NULL,
  `regusuario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `regfecha` date NOT NULL,
  `matriculados` int(11) NOT NULL,
  PRIMARY KEY (`idgrupo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`idgrupo`, `codigo`, `idproducto`, `iddocente`, `idlocal`, `idambiente`, `idasistente`, `fechainicio`, `fechafin`, `dias`, `horas`, `horainicio`, `horafin`, `valor`, `valor_asi`, `regusuario`, `regfecha`, `matriculados`) VALUES
('1', '', 0, '22222222', '1', '1', '99999999', '2017-03-01', '2017-12-15', 'M,V', 4, '08:00:00', '12:00:00', 20, 11, '72042592', '2017-04-20', 10),
('2', 'ING2017201BA', 0, '99999999', '1', '2', '99999999', '2017-03-01', '2017-12-15', 'L,J', 2, '08:00:00', '10:00:00', 20, 11, '72042592', '2017-04-20', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_matricula`
--

CREATE TABLE IF NOT EXISTS `grupo_matricula` (
  `idgrupo_matricula` int(11) NOT NULL,
  `idalumno` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idgrupo` char(9) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechamatricula` date NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `regusuario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `regfecha` date NOT NULL,
  `subgrupos` text COLLATE utf8_spanish_ci,
  PRIMARY KEY (`idgrupo_matricula`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `grupo_matricula`
--

INSERT INTO `grupo_matricula` (`idgrupo_matricula`, `idalumno`, `idgrupo`, `fechamatricula`, `estado`, `regusuario`, `regfecha`, `subgrupos`) VALUES
(1, '72042592', '1', '2017-04-03', '1', '72042592', '2017-04-20', NULL),
(2, '12345678', '1', '2017-03-06', '1', '72042592', '2017-04-20', NULL),
(3, '87654321', '1', '2017-03-30', '1', '72042592', '2017-04-20', NULL),
(4, '33333333', '2', '2017-03-08', '1', '72042592', '2017-04-20', NULL),
(5, '44444444', '2', '2017-02-28', '1', '72042592', '2017-04-20', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manuales`
--

CREATE TABLE IF NOT EXISTS `manuales` (
  `idmanual` int(11) NOT NULL,
  `titulo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `abreviado` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`idmanual`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `idmenu` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`idmenu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`idmenu`, `nombre`) VALUES
(1, 'Actividad'),
(2, 'Vocabulario'),
(3, 'seralumno'),
(4, 'permisos'),
(5, 'menu'),
(6, 'sincronizar'),
(7, 'registros'),
(8, 'Cursos'),
(9, 'reportes'),
(10, 'comunidad'),
(11, 'recursos'),
(12, 'Evaluacion'),
(13, 'ugel'),
(14, 'Local'),
(15, 'Ambiente'),
(16, 'unidad'),
(17, 'niveles'),
(18, 'detalle_matricula_alumno'),
(19, 'configuracion_docente'),
(20, 'reporte'),
(21, 'docente'),
(22, 'tarea'),
(23, 'tarea_asignacion'),
(24, 'tarea_respuesta'),
(25, 'tarea_archivos'),
(26, 'tarea_asignacion_alumno'),
(27, 'notas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `niveles`
--

CREATE TABLE IF NOT EXISTS `niveles` (
  `idnivel` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `tipo` char(1) NOT NULL COMMENT 'N= nivel, U= unidad, L=lesson',
  `idpadre` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL DEFAULT '0',
  `estado` int(11) NOT NULL COMMENT '1 activo , 0 inactivo',
  `orden` tinyint(4) NOT NULL DEFAULT '0',
  `imagen` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idnivel`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `niveles`
--

INSERT INTO `niveles` (`idnivel`, `nombre`, `tipo`, `idpadre`, `idpersonal`, `estado`, `orden`, `imagen`) VALUES
(1, 'A1', 'N', 0, 0, 1, 0, NULL),
(2, 'A2', 'N', 0, 0, 1, 0, NULL),
(3, 'B1', 'N', 0, 0, 1, 0, NULL),
(4, 'B2', 'N', 0, 0, 1, 0, NULL),
(5, 'C1', 'N', 0, 0, 1, 0, NULL),
(56, 'What''s this?', 'L', 6, 33333333, 1, 38, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313051952.jpg'),
(18, 'Hello', 'L', 6, 77777777, 1, 1, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105623.jpg'),
(17, 'My year', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314122424.jpg'),
(16, 'I know what you did yesterday', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314121727.jpg'),
(15, 'Where do you live?', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314114745.jpg'),
(14, 'Right now!', 'U', 1, 33333333, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323063813.jpg'),
(13, 'It''s a fact!', 'U', 1, 33333333, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314113752.jpg'),
(12, 'Food and drink', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313110118.jpg'),
(11, 'Going shopping', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313110322.jpg'),
(10, 'People', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313110217.jpg'),
(9, 'Leisure activities', 'U', 1, 33333333, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323063742.jpg'),
(8, 'Where are you from?', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314114908.png'),
(7, 'Numbers & Figures', 'U', 1, 33333333, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313033457.jpg'),
(6, 'Introductions', 'U', 1, 99999999, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170221083851.jpg'),
(21, 'Numbers', 'L', 7, 77777777, 1, 2, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313053818.jpeg'),
(22, 'Figures', 'L', 7, 33333333, 1, 6, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313054124.jpg'),
(23, 'Names', 'L', 7, 33333333, 1, 10, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313054219.jpg'),
(24, 'Countries', 'L', 8, 77777777, 1, 11, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313055154.png'),
(25, 'Nationalities', 'L', 8, 77777777, 1, 12, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313055826.jpg'),
(26, 'Places', 'L', 8, 77777777, 1, 14, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313055956.jpg'),
(27, 'My week', 'L', 9, 77777777, 1, 3, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313060555.jpg'),
(29, 'Leisure activities', 'L', 9, 77777777, 1, 16, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313061943.jpg'),
(30, 'Describing people', 'L', 10, 77777777, 1, 8, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323052738.jpg'),
(31, 'The body', 'L', 10, 77777777, 1, 13, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313063050.jpg'),
(32, 'My family', 'L', 10, 77777777, 1, 15, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313063351.jpg'),
(33, 'At the store', 'L', 11, 77777777, 1, 17, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313065047.jpg'),
(34, 'Shopping', 'L', 11, 77777777, 1, 18, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313064055.jpg'),
(35, 'In a supermarket', 'L', 11, 77777777, 1, 19, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313065148.jpg'),
(36, 'Food items', 'L', 12, 77777777, 1, 20, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323052824.jpg'),
(37, 'In a restaurant', 'L', 12, 77777777, 1, 21, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313070130.jpg'),
(38, 'Cooking or dining out?', 'L', 12, 77777777, 1, 22, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313070403.jpg'),
(39, 'Clothing', 'L', 13, 33333333, 1, 23, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313070713.jpg'),
(40, 'Planets', 'L', 13, 77777777, 1, 25, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313070944.jpg'),
(41, 'Animals', 'L', 13, 77777777, 1, 26, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313071240.jpg'),
(42, 'Weather festivals', 'L', 14, 77777777, 1, 24, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313071500.jpg'),
(43, 'What am i doing?', 'L', 14, 77777777, 1, 9, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313072809.png'),
(44, 'Help!', 'L', 14, 77777777, 1, 27, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313072901.jpg'),
(45, 'My hometown', 'L', 15, 77777777, 1, 28, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313073410.jpg'),
(46, 'My house', 'L', 15, 77777777, 1, 29, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313073940.jpg'),
(47, 'My address', 'L', 15, 77777777, 1, 30, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314103719.png'),
(48, 'Where were you?', 'L', 16, 77777777, 1, 31, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061239.jpg'),
(49, 'My School Years', 'L', 16, 77777777, 1, 32, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061319.jpg'),
(50, 'My work', 'L', 16, 77777777, 1, 33, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061349.jpg'),
(51, 'Dates and seasons', 'L', 17, 77777777, 1, 34, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061752.jpg'),
(52, 'My holidays', 'L', 17, 77777777, 1, 35, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323062332.jpg'),
(53, 'Journeys', 'L', 17, 77777777, 1, 36, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323063656.jpg'),
(55, 'About you', 'L', 6, 33333333, 1, 37, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313054934.jpg'),
(58, 'Routines', 'L', 9, 33333333, 1, 7, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170704050750.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `idpermiso` int(11) NOT NULL,
  `rol` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `_list` tinyint(1) NOT NULL,
  `_add` tinyint(1) NOT NULL,
  `_edit` tinyint(1) NOT NULL,
  `_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`idpermiso`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`idpermiso`, `rol`, `menu`, `_list`, `_add`, `_edit`, `_delete`) VALUES
(1, 1, 2, 1, 1, 1, 1),
(2, 1, 1, 1, 1, 1, 1),
(3, 2, 2, 1, 1, 1, 1),
(4, 3, 1, 1, 1, 1, 0),
(5, 2, 1, 1, 0, 1, 1),
(6, 3, 2, 1, 1, 1, 0),
(7, 4, 2, 0, 0, 0, 0),
(8, 1, 3, 1, 0, 0, 0),
(9, 2, 3, 1, 0, 0, 0),
(10, 1, 4, 1, 1, 1, 1),
(11, 1, 5, 1, 1, 1, 1),
(12, 1, 6, 1, 1, 0, 0),
(13, 1, 7, 1, 0, 0, 0),
(14, 1, 8, 1, 0, 0, 0),
(15, 1, 9, 1, 0, 0, 0),
(16, 1, 10, 1, 0, 0, 0),
(17, 3, 6, 1, 0, 0, 0),
(18, 2, 11, 1, 0, 1, 0),
(19, 1, 11, 1, 1, 1, 1),
(20, 1, 12, 1, 0, 0, 0),
(21, 1, 13, 1, 1, 1, 1),
(22, 2, 13, 0, 0, 0, 0),
(23, 1, 14, 1, 1, 1, 1),
(24, 1, 15, 1, 1, 1, 1),
(25, 1, 16, 1, 1, 1, 1),
(26, 2, 16, 1, 1, 0, 0),
(27, 2, 17, 1, 1, 0, 0),
(28, 2, 18, 0, 0, 1, 0),
(29, 1, 18, 1, 1, 1, 1),
(30, 1, 19, 1, 1, 1, 1),
(31, 1, 20, 1, 1, 1, 1),
(32, 2, 19, 1, 1, 1, 0),
(33, 2, 20, 1, 1, 1, 0),
(35, 1, 21, 1, 1, 1, 1),
(36, 2, 21, 1, 1, 1, 1),
(37, 1, 22, 1, 1, 1, 1),
(38, 1, 23, 1, 1, 1, 1),
(39, 1, 24, 1, 1, 1, 1),
(40, 1, 25, 1, 1, 1, 1),
(41, 2, 22, 1, 1, 1, 0),
(42, 2, 23, 1, 1, 1, 1),
(43, 2, 24, 0, 0, 0, 0),
(44, 2, 25, 1, 1, 1, 1),
(45, 1, 26, 1, 1, 1, 1),
(46, 2, 26, 1, 1, 1, 1),
(47, 3, 27, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE IF NOT EXISTS `personal` (
  `dni` int(11) NOT NULL,
  `ape_paterno` varchar(50) NOT NULL,
  `ape_materno` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fechanac` date DEFAULT NULL,
  `sexo` char(1) NOT NULL,
  `estado_civil` char(1) NOT NULL,
  `ubigeo` char(8) NOT NULL,
  `urbanizacion` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `email` varchar(75) NOT NULL,
  `idugel` char(4) NOT NULL,
  `regusuario` int(11) NOT NULL DEFAULT '0',
  `regfecha` date NOT NULL,
  `usuario` varchar(35) NOT NULL,
  `clave` varchar(35) NOT NULL,
  `token` varchar(35) NOT NULL,
  `rol` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `situacion` char(1) NOT NULL,
  `idioma` varchar(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`dni`),
  KEY `nombre` (`nombre`,`ape_paterno`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`dni`, `ape_paterno`, `ape_materno`, `nombre`, `fechanac`, `sexo`, `estado_civil`, `ubigeo`, `urbanizacion`, `direccion`, `telefono`, `celular`, `email`, `idugel`, `regusuario`, `regfecha`, `usuario`, `clave`, `token`, `rol`, `foto`, `estado`, `situacion`, `idioma`) VALUES
(43831104, 'Chinguit', 'Tello', 'Abel', '1986-10-28', 'M', '', '', '', '', '', '', 'abel_chingo@hotmail.com', '', 0, '2017-11-11', 'admin', '0192023a7bbd73250516f069df18b500', '0', 1, '', 1, '1', 'EN'),
(43371672, 'Muñoz', 'Meza', 'Evelio', '2016-11-14', 'M', 'C', '14010101', 'MARIA PARA DE BELLIDO', 'MEXICO 790', '251924', '979222660', 'emunozmeza@gmail.com', '0001', 1, '2016-11-14', '43371672', 'c4ca4238a0b923820dcc509a6f75849b', '43371672', 1, '', 1, '1', 'EN'),
(72042592, 'Figueroa', 'Piscoya', 'Eder', NULL, 'M', 'S', '', '', '', '', '', 'eder.figueroa@outlook.com', '', 0, '2016-12-26', 'eder.figueroa', '81dc9bdb52d04dc20036dbd8313ed055', '1234', 1, '', 1, '1', 'EN'),
(22222222, 'Eder', 'F.P.', 'Docente', NULL, 'M', 'S', '', '', '', '', '', 'docente1@hotmail.com', '', 1, '2016-12-26', 'doc_eder', '81dc9bdb52d04dc20036dbd8313ed055', '1234', 2, '', 1, '1', 'EN'),
(99999999, 'System', '', 'Cv', '2017-02-01', 'M', 'S', '', '', '', '', '', 'info@pvingles.com', '', 0, '0000-00-00', 'pvingles', 'eeb696d150083337a5610b560c2526bb', '', 3, '', 1, '1', 'EN'),
(75935728, 'Garrido', 'Reque', 'Tattiana', '1992-06-21', 'F', 'S', '', '', '', '', '', '', '', 0, '0000-00-00', 'tattiana', '4f1b1013e66d7a6331d96375ff34d814', 'tattiana2017', 1, '', 1, '1', 'EN'),
(33333333, 'System', '', 'docente1', '2017-02-01', 'M', 'S', '', '', '', '', '', 'docente1@hotmail.com', '', 0, '2017-02-09', 'kzoeger', '0192023a7bbd73250516f069df18b500', '8be2efedf58bc5ca5ef33aec86a7a217', 1, '', 1, '1', 'EN'),
(46500285, 'Zoeger', '', 'Karla', '2017-02-01', 'F', 'S', '', '', '', '', '', 'karlazoeger.123@hotmail.com', '', 0, '2017-02-09', 'kzoeger', '0192023a7bbd73250516f069df18b500', 'admin123', 1, '', 0, '1', 'EN'),
(55555555, 'Castro', 'Alzamora', 'Manuel', '1966-10-31', 'M', 'S', '', '', '', '', '', 'mcmanolito@gmail.com', '', 0, '2017-02-09', 'mcastro', '0192023a7bbd73250516f069df18b500', 'admin123', 1, '', 1, '1', 'EN'),
(66666666, 'Enny ', 'Enny', 'Enny', '2017-02-01', 'M', 'S', '', '', '', '', '', 'docente5@hotmail.com', '', 0, '2017-02-09', 'enny21', '81dc9bdb52d04dc20036dbd8313ed055', '0192023a7bbd73250516f069df18b500', 1, '', 1, '1', 'EN'),
(77777777, 'Anais', 'pvingles', 'Anais', '2017-02-01', 'M', 'S', '', '', '', '', '', 'docente6@hotmail.com', '', 0, '2017-02-09', 'anais', '3710fd04a04df697f0af60d5e1345549', '3710fd04a04df697f0af60d5e1345549', 1, '', 0, '0', 'EN'),
(88888888, 'm', '', 'Sharon', '2017-02-01', 'F', 'S', '', '', '', '', '', 'smamud@hotmail.com', '', 0, '2017-02-09', 'smamud', '0577b7cf26c365b19ec51219a7867fb7', '0577b7cf26c365b19ec51219a7867fb7', 1, '', 1, '1', 'EN'),
(11111111, 'Garrido', 'Reque', 'Tatiana', '2017-05-26', 'F', 'S', '', '', '', '', '', '', '', 0, '0000-00-00', 'tatiana', '4e39d9452ab78db254167d7fcf5261e1', '4e39d9452ab78db254167d7fcf5261e1', 1, '', 1, '1', 'EN'),
(21212121, 'Morante', '', 'Monica', NULL, 'F', 'C', '', '', '', '', '', 'mmorante@hotmail.com', '', 0, '2017-06-22', 'mmorante', '0192023a7bbd73250516f069df18b500', 'admin123', 1, '', 1, '1', 'EN'),
(10101010, 'Demo', 'Demo', 'Demo', '1906-06-06', 'F', 'S', '', '', '', '', '', 'demo@hotmail.com', '', 0, '2017-06-23', 'demo', '62cc2d8b4bf2d8728120d052163a77df', 'demo123', 1, '', 1, '1', 'EN');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `idrol` int(11) NOT NULL,
  `rol` varchar(35) NOT NULL,
  PRIMARY KEY (`idrol`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`idrol`, `rol`) VALUES
(1, 'administrador'),
(2, 'docente'),
(3, 'Supervisor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_configuracion`
--

CREATE TABLE IF NOT EXISTS `sis_configuracion` (
  `config` varchar(30) NOT NULL,
  `valor` longtext,
  `autocargar` enum('si','no') NOT NULL,
  PRIMARY KEY (`config`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_configuracion`
--

INSERT INTO `sis_configuracion` (`config`, `valor`, `autocargar`) VALUES
('descripcion_sitio', 'Sistema integral para la administración de ventas y gastos', 'no'),
('direccion_negocio', 'Perú', 'no'),
('email_contacto', 'abel_chingo@hotmail.com', 'no'),
('facebook_id', '', 'no'),
('idioma_defecto', 'EN', 'si'),
('multiidioma', 'SI', 'si'),
('nombre_sitio', 'Portal ingles', 'no'),
('servidor_correo_clave', 'info2016@', 'no'),
('servidor_correo_cuenta', 'info@ingles.com', 'no'),
('servidor_correo_puerto', '25', 'no'),
('servidor_correo_url', 'ip-50-62-188-218.ip.secureserver.net', 'no'),
('telefono', '55555', 'no');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
