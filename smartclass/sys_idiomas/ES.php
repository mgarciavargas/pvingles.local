<?php
defined('RUTA_BASE') or die();

$texts = array();
////////////////////---------------------A---------------------------------
$texts['adjustment']='Configuración';
$texts['add']='agregar';
$texts['add new']='agregar nuevo';
$texts['active']='Activo';
$texts['actions']='Acciones';
$texts['accept']='Aceptar';
$texts['attention']='Atención';
$texts['activity']='actividad';
$texts['advance<br>sessions']='Avance de<br>Sesiones';
$texts['activities']='Actividades';
$texts['ability']='Habilidad';
$texts['alternatives']='Alternativas';
$texts['attempt']='Intento';
$texts['and']='y';
$texts['another option']='otra opción';
$texts['attention']='Atención';
$texts['admin panel']='Panel de Administración';
$texts['add my own vocabulary']='Agregar mi propio vocabulario';
$texts['add my own links']='Agregar mis propios enlances';
$texts['a few words more']='unas cuantas paralabra más';
$texts['a few links more']='algunos enlaces más';
$texts['are you sure you want to delete this']='¿Está seguro que desea borrar este';
$texts['add my own']='Agregar mi propio';
$texts['a letter']='una letra';
$texts['a word']='una palabra';
$texts['a sentence']='una oración';
$texts['a part']='una parte';
$texts['assisted exercise']='ejercicio asistido';
$texts['assessments']='Exámenes';
$texts['academic']='Académico';
$texts['add scene']='Agregar escena';
$texts['add participants']='Agregar participantes';

$texts['alfabetic']='Alfabético';
$texts['all rights reserved']='Todos los derechos reservados';
$texts['all levels']='todos los niveles';
$texts['all units']='todas las unidades';
$texts['all activities']='todas las actividades';
$texts['all types']='todos los tipos';
$texts['all users']='todos los usuarios';
$texts['allow to use']='Permitir usar';
$texts['add title']='agregar título';
$texts['add description']='agregar descripción';
$texts['alfabetic']='alfabético';
$texts['automatic improvement']='Mejora automática';
$texts['audio output']='Salida de Audio';
$texts['apply improvement']='Aplicar mejora';
$texts['average of obtained scores']='Promedio de las puntuaciones obtenidas';
$texts['average score']='Puntaje promedio';
$texts['assign to students']='Asignar a alumnos';
$texts['attachment']='Adjunto';
$texts['assigned']='asignado';
$texts['attach']='Adjuntar';
$texts['assignment']='Asignación';
$texts['are you sure to delete this record?']='¿Está seguro de eliminar este registro?';

////////////////////---------------------B---------------------------------
$texts['back to general results']='Volver a Resultados Generales';
$texts['back to list']='Volver al listado';
$texts['bad']='malo';
$texts['banners']='Baners';
$texts['branch office']='Sucursal';
$texts['browser']='Navegador Web';
$texts['bookshop']='Libros de Interés';
$texts['books']='Libros';
$texts['board']='Pizarra';
$texts['birthdate']='Fecha de Nacimiento';
$texts['back']='Volver';
$texts['blank']='espacio vacío';
$texts['blocked, no more participants']='bloqueado, no se aceptan más participantes';


////////////////////---------------------C---------------------------------
$texts['camera']='Cámara';
$texts['customers']='clientes';
$texts['customer']='cliente';
$texts['content']='Contenido';
$texts['currency']='Moneda';
$texts['change password']='Cambiar contraseña';
$texts['change language']='Cambiar Idioma';
$texts['cancel']='Cancelar';
$texts['combination of email and password incorrect']='Combinación de correo electrónico y contraseña incorrecta';
$texts['company']='Negocios';
$texts['cash register']='Caja';
$texts['confirm action']='Confirmar Acción';
$texts['continue']='Continuar';
$texts['community']='Comunidad';
$texts['country']='País';
$texts['close']='Cerrar';
$texts['close room']='cerrar sala';
$texts['closed']='Cerrado';
$texts['close without saving']='Cerrar sin guardar';
$texts['click here to']='Click aquí para';
$texts['click here to add description']='Click aquí para agregar descripción';
$texts['click here to add title']='Click aquí para agregar título';
$texts['click here to change te exam cover']='Click aquí para cambiar la portada del examen';
$texts['complete sentence']='Completar oración';
$texts['crossword puzzle']='Crucigrama';
$texts['crossword']='Crucigrama';
$texts['courses']='Cursos';
$texts['chat']='Charlar';
$texts['check these out']='Echale un vistazo';
$texts['choose difficulty']='Elegir dificultad';
$texts['choose template']='Elegir plantilla';
$texts['choose skills']='Elegir habilidades';
$texts['choose']='elegir';
$texts['create']='Crear';
$texts['column']='columna';
$texts['correct answer']='respuesta correcta';
$texts['changes']='cambios';
$texts['character']='personaje';
$texts['continue to the next exercise']='continúa con el siguiente ejercicio';
$texts['created by']='Creado por';
$texts['choose one to start']='Escoja uno para empezar';
$texts['cover']='portada';
$texts['classroom']='aula';
$texts['control time by']='controlar el tiempo por';
$texts['control panel']='Panel de Control';
$texts['contact']='Contactar';
$texts['chart of progress since last tracking']='Tabla de progreso desde el último seguimiento';

////////////////////---------------------D---------------------------------

$texts['danger']='Error';
$texts['date/time of presentation']='Fecha/Hora de presentación';
$texts['date']='Fecha';
$texts['date first']='Fecha inicio';
$texts['date finish']='Fecha final';
$texts['danger']='Error';
$texts['data updated']='Datos actualizados';
$texts['data incorrect']='Datos incorrectos';
$texts['delete']='Eliminar';
$texts['dictionary']='Diccionario';
$texts['directed to']='Dirigido a';


$texts['done']='Listo';
$texts['download']='Descargar';
$texts['dni']='D.N.I.';
$texts['dashboard']='Menú Principal';
$texts['do not show this again']='No volver a mostrar esto';
$texts['description']='descripción';
$texts['drag and drop']='Arrastrar y soltar';
$texts['drop']='soltar';
$texts['dialogue']='Conversación';
$texts['does not exist']='No existe';
$texts['did you forget your password?']='¿Olvidó su contraseña?';
$texts['developer']='Desarrollador';
$texts['discard']='Descartar';
$texts['divide into']='Dividir en';
$texts['delete tag']='Eliminar etiqueta';
$texts['distractor']='distracciones';
$texts['drag']='arrastrar';
$texts['delete actual scene']='Eliminar escena actual';
$texts['do it by yourself']='Hazlo por ti mismo';
$texts['do not allow to use']='No permitir usar';
$texts['do you want to continue?']='¿Desea continuar?';
$texts['do you want to submit the homework now?']='¿Quieres presentar la tarea ahora?';
////////////////////---------------------E---------------------------------

$texts['edit']='editar';
$texts['editing exercise']='Editando ejercicio';
$texts['edit puzzle']='Editar rompecabezas';
$texts['email']='Correo';
$texts['e-mail']='Correo';
$texts['emails must be separated by commas']='correos electrónicos deben estar separados por comas';
$texts['email message successfully sent']='Mensaje de correo electrónico enviado correctamente';
$texts['emitting class']='Emitir Clase';
$texts['enable and disable microphone']='Activar o desactivar micrófono';//or
$texts['enable and disable webcam']='Activar o desactivar cámara web';//or
$texts['enable and disable record class']='Activar o desactivar grabar clase';//or
$texts['enable and disable screen sharing']='Activar o desactivar compartir pantalla';//or
$texts['enable and disable call private']='Activar o desactivar llamada privada';//or
$texts['evaluation']='Evaluación';
$texts['empty']='vacío';
$texts['enter your username']='Ingrese su nombre de usuario';
$texts['enter your email']='Introduce tu correo electrónico';

$texts['e.g.']='ejemp.';
$texts['external link']='Enlace externo';
$texts['exit preview']='Salir de Vista Previa';
$texts['edit description']='Editar descripción';
$texts['error sending mail, please try again later']='error al enviar el correo. Vuelve a intentarlo más tarde.';
$texts['error in filtering']='Error al filtrar';
$texts['error when improve learning']='Error al mejorar aprendizaje';
$texts['error in filtering']='Error en filtrado';
$texts['error loading file']='Error al subir archivo';
$texts['error in function']='Error en función';
$texts['evaluate and save']='Evaluar y guardar';
$texts['exercise']='ejercicio';
$texts['expired']='expirado';
$texts['exam']='examen';
$texts['exams']='exámenes';
$texts['exam type']='tipo de examen';
$texts['exam evaluation']='evaluación del examen';
$texts['exam format']='formato de examen';
$texts['exit']='Salir';

////////////////////---------------------F---------------------------------
$texts['flag']='Estado';
$texts['from']='De';
$texts['function']='Funciones';
$texts['first name']='Nombres';
$texts['finish']='Terminar';
$texts['finish date']='Fecha Final';
$texts['fix']='Fijar';
$texts['file']='archivo';
$texts['file upload success']='Archivo subido correctamente';
$texts['fonts']='fuentes';
$texts['font size']='tamaño de letra';
$texts['full name']='nombre completo';
$texts['full screen']='Pantalla completa';
$texts['filter']='filtrar';
$texts['finished']='finalizado';
////////////////////---------------------G---------------------------------
$texts['generator']='Generador';
$texts['get']='Obtener';
$texts['go']='Ir';
$texts['game']='Juego';
$texts['generate']='Generar';
$texts['group']='grupo';
$texts['grammar / structure']='Estructura Gramatical';
$texts['general results']='Resultados Generales';
$texts['general public']='Público en General';
$texts['general summary of progress']='Resumen General del Progreso';
$texts['good']='bueno';
$texts['go back']='Retroceder';
$texts['google chrome extension']='Extensión de google Chrome';

////////////////////---------------------H---------------------------------
$texts['hand raised']='Alzar la mano';
$texts['home']='Inicio';
$texts['highest score']='máxima puntuación';
$texts['higher score']='Puntaje más alto';
$texts['homework not completed, yet']='Tarea aún no completada';
$texts['homework']='tarea';
$texts['homework was returned to student']='La tarea fue devuelta al alumno';
$texts['help']='Ayuda';
////////////////////---------------------I---------------------------------

$texts['it is sure to change the status of this record ?']='¿Está seguro de cambiar el estado de este registro?';
$texts['it is sure to delete this record ?']='¿Esta seguro de eliminar este registro?';
$texts['if the email is correct you will receive a message with a link to change your password']='Si el correo electrónico es correcto recibirá un mensaje con un enlace para cambiar su contraseña';
$texts['interest<br>links']='Enlaces de<br>Interés';
$texts['Install google chrome here']='Instalar google chrome aquí';
$texts['Install mozilla firefox here']='Instalar google chrome aquí';
$texts['image']='imagen';
$texts['icons']='íconos';
$texts['image tagging']='Etiquetado de imagen';
$texts['improve learning']='Mejorar el Aprendizaje';
$texts['improve']='mejorar';
$texts['inactive']='Inactivo';
$texts['induction']='inducción';
$texts['information']='Información';
$texts['insert']='Insertar';
$texts['intermediate']='intermedio';
$texts['invite users']='Invitar usuarios';
$texts['invite participants']='Invitar participantes';
$texts['invitation to participate in smartclass']='Invitación a participar en Smartclass';


////////////////////---------------------J---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------K---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------L---------------------------------
$texts['log in'] = 'Iniciar Sesión';
$texts['license']='Privilegios';
$texts['language']='Idioma';

$texts['level']='Nivel';
$texts['levels']='Niveles';
$texts['lesson plan']='Plan de estudios';
$texts['left dialogue']='Diálogo Izquierdo';
$texts['loading']='Cargando';
$texts['list']='Listado';
$texts['list options']='Opciones de lista';
$texts['look']='Mirar';
$texts['letter']='letra';
$texts['listen']='Escuchar';
$texts['library']='Biblioteca';
$texts['lock room']='Bloquear sala';
$texts['locked']='Bloqueado';
$texts['log out']='Cerrar Sesión';
$texts['lower score']='Puntaje más bajo';
$texts['listening']='Escuchar';
$texts['last name']='Apellidos';

$texts['last completed activity']='Última actividad completada';
$texts['legend']='Leyenda';

////////////////////---------------------M---------------------------------
$texts['mail message, without recipiente']='Mensaje de correo sin destinatarios';
$texts['max. score']='Máx. puntaje';

$texts['maximum score']='puntaje máximo';
$texts['manual improvement']='Mejora manual';
$texts['media library']='biblioteca Multimedia';
$texts['meeting']='reunión';
$texts['message']='mensaje';
$texts['message empty']='mensaje vació';
$texts['message to teacher']='mensaje al profesor';
$texts['methodology']='Metodología';
$texts['microphone']='Micrófono';
$texts['minimum score to pass']='mínimo puntaje para aprobar';
$texts['minimum score to pass the scale']='mínimo puntaje para superar la escala';
$texts['minimum score']='puntaje mínimo';
$texts['min. score to pass']='Mín. puntaje para aprobar';
$texts['missing time to reach optimum time']='Tiempo faltante para llegar al tiempo óptimo';
$texts['moderator']='Moderador';
$texts['moderator is not connected to the room']='Moderador no se ha conectado a la sala';
$texts['move up']='Subir';
$texts['move down']='Bajar';
$texts['mozilla firefox extension']='Extensión de Mozilla firefox';
$texts['my profile']='Mi perfil';
$texts['my notes']='Mis notas';
$texts['my progress']='Mi progeso'; 
$texts['my students progress']='Progreso de mis estudiantes';
$texts['multiple choice']='Opción Múltiple';
$texts['my profile']='Mi Perfil';
$texts['my resources']='Mis Recursos';
$texts['my students']='Mis estudiantes';
$texts['my guests']='Mis invitados';
$texts['my homework']='Mis Tareas';

////////////////////---------------------N---------------------------------
$texts['name']='Nombre';
$texts['new']='Nuevo';
$texts['new activity']='Nueva actividad';
$texts['notes']='Notas';
$texts['no audio was detected for playback']='No se ha detectado audio  para reproducir';
$texts['not yet active for user connection']='Aún no está activo para la conexión de usuario';
$texts['no audio was detected for playback in scene']='No se ha detectado audio  para reproducir en la escena';
$texts['no data to send']='No existen datos a enviar';
$texts['no activities completed']='No tiene actividades completadas';
$texts['no connected microphone has been detected, this being necessary for communication']='No se ha detectado ningún micrófono conectado, siendo este necesario para la comunicación';
$texts['no connected speakers or headphones detected, this being necessary for communication']='No se detectan altavoces o auriculares conectados, siendo esto necesario para la comunicación';
$texts['no plugin needed to share your desktop, click the following link to install it']='No se ha detectado plugin necesario para compartir su escritorio, click en el siguiente enlace para instalarlo';
$texts['numeric']='numérico';
$texts['number of attempts you take']='número de intentos que usted lleva';
$texts['maximum number of participants']='Número máximo de participantes';


////////////////////---------------------O---------------------------------
$texts['office']='Sucursal';
$texts['off']='Apagado';
$texts['on']='Encendido';
$texts['or']='o';
$texts['of']='de';
$texts['order']='ordenar';
$texts['orderly']='ordenado';
$texts['out of 100 points']='Sobre 100 puntos';
$texts['optimal time of stay on the platform']='Tiempo óptimo para permanecer ';

$texts['optimal time']='Tiempo óptimo';
$texts['open/look']='Abrir/bloquear';
$texts['opened']='Iniciado';
////////////////////---------------------P---------------------------------
$texts['password'] = 'Contraseña';
$texts['participant']='Participante';
$texts['participants']='Participantes';
$texts['presenter']='Presentador';
$texts['pages']='Páginas';
$texts['page']='Página';
$texts['photo of profile']='Imagen de perfil';
$texts['preview']='Vista Previa';
$texts['photo and audio']='Imagen y audio';
$texts['practice']='Practicar';
$texts['puzzle']='Rompecabezas';
$texts['paragraph']='párrafo';
$texts['premise']='premisa';
$texts['person']='persona';
$texts['personal email']='Correo electrónico personal';
$texts['play first']='reproducir primero';
$texts['play second']='reproducir segundo';
$texts['play order']='orden de reproducción';
$texts['progress of my students']='Progreso de mis estudiantes';
$texts['progress in exams']='progreso en exámenes';
$texts['progress in activities']='progreso en actividades';
$texts['progress in homework']='progreso en tareas';
$texts['paste the video link here']='Pega el enlace del video aquí';
$texts['post']='publicar';
$texts['print view']='Vista de Impresión';
$texts['print']='Imprimir';
$texts['percentage']='porcentaje';
$texts['press the "filter" button to start']='Presione el botón "Filtrar" para comenzar';
$texts['date of presentation']='dia de presentación';
$texts['pending']='pendiente';
////////////////////---------------------Q---------------------------------
$texts['questions']='Preguntas';
$texts['question']='pregunta';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------R---------------------------------
$texts['raise your hand']='Alzar la mano';
$texts['read']='Leer';
$texts['reading']='Leer';
$texts['re-check requirements']='Volver a verificar requerimientos';
$texts['record the class']='Grabar la clase';
$texts['recover password']='Recuperar clave';
$texts['reports']='Reportes';
$texts['reserves']='Reservas';
$texts['reserve list']='Listado de Reservas';
$texts['reset']='Restaurar';
$texts['reset windows']='Reiniciar ventanas';
$texts['return to login']='Volver a iniciar sesión';


$texts['recording']='Grabar';
$texts['remove']='Quitar';
$texts['remove tab']='Quitar Tab';
$texts['right dialogue']='Diálogo Derecho';
$texts['Requesting a microphone']='solicitando micrófono';

$texts['resources']='Recursos';
$texts['register']='Registros';

$texts['remove just my own vocabulary']='Quitar solo mi propio Vocabulario';
$texts['remove just my own links']='Quitar solo mis propios Links';
$texts['role']='Rol';
$texts['row']='fila';

$texts['ramdon']='aleatorio';
$texts['progress report']='Informe de progreso';
$texts['return']='Devolver';
$texts['rubrics']='rúbricas';
$texts['room']='Aula';
////////////////////---------------------S---------------------------------
$texts['sales']='Ventas';


$texts['save']='Guardar';
$texts['save all']='Guardar Todo';
$texts['save and add exercise']='Guardar y agregar ejercicio';
$texts['save and finish']='Guardar y terminar';
$texts['saved successfully']='guardado correctamente';
$texts['save and add to the question']='guardar y agregar a la pregunta';
$texts['save and invite participants']='Guardar e invitar participantes';
$texts['search']='buscar';
$texts['see more']='Ver más';
$texts['select']='seleccionar';
$texts['select template']='Seleccionar Plantilla';
$texts['select or upload']='Seleccionar o cargar';
$texts['select student']='Seleccionar Alumno';
$texts['select group']='Seleccionar Grupo';
$texts['select classroom']='Seleccionar Aula';
$texts['select school']='Seleccionar Colegio';
$texts['select educational institution']='Seleccionar Institución Educativa';
$texts['select level']='Seleccionar Nivel';
$texts['select unit']='Seleccionar Unidad';
$texts['select activity']='Seleccionar Actividad';
$texts['select all']='seleccionar todo';
$texts['select an image from our multimedia library or upload your own image']='selecciona una imagen desde nuestra biblioteca multimedia o carga tu propia imagen';
$texts['select a video from our multimedia library or upload your own video']='selecciona un video desde nuestra biblioteca multimedia o carga tu propio video';
$texts['select an audio from our multimedia library or upload your own audio']='selecciona un audio desde nuestra biblioteca multimedia o carga tu propio audio';
$texts['select file from your computer']='selecciona un archivo desde su computadora';
$texts['send this report to']='enviar este informe al';
$texts['Upload and share files']='Enviar y compartir archivos';
$texts['send']='enviar';
$texts['services']='Servicios';
$texts['session']='Sesión';
$texts['settings']='Configuraciones';
$texts['setting']='Configuración';
$texts['start date']='Fecha de inicio';
$texts['sync']='Sincronización';
$texts['share']='Compartir';
$texts['shared']='Compartir';
$texts['share desktop']='compartir Escritorio';
$texts['share files']='compartir Archivos';
$texts['share pdf']='compartir PDF';
$texts['share video']='compartir Video';
$texts['share webcam']='compartir cámara web';
$texts['shared files']='Archivos compartidos';
$texts['show']='Mostrar';
$texts['show help']='Mostrar ayuda';
$texts['skills']='Habilidades';
$texts['skills building']='Desarrollo de habilidades';
$texts['sheet']='ficha';
$texts['something']='algo';
$texts['something went wrong']='algo salió mal';
$texts['sorting of questions']='Ordenamiento de las preguntas';
$texts['sorry, there have been the following errors']='Lo sentimos, se han producido los siguientes errores';
$texts['sorry, we could not find the requested address']='Lo sentimos, no hemos encontrado la página solicitada';
$texts['sorry, something went wrong']='Disculpe, algo salió mal';
$texts['score']='puntaje';
$texts['start tagging']='Iniciar etiquetado';
$texts['stop tagging']='Detener etiquetado';
$texts['student']='alumno';
$texts['student record']='Historial del Alumno';
$texts['student reports']='Informes de Alumnos';
$texts['student progress tracking']='Seguimiento de Progreso del Alumno';
$texts['students']='alumnos';
$texts['students assigned']='alumnos asignados';
$texts['student comments']='Comentarios del alumno';
$texts['student session']='Sesión del alumno';
$texts['state']='estado';
$texts['subject']='Asunto';
$texts['stop deleting tag']='Detener eliminación de etiqueta';
$texts['speak']='Hablar';
$texts['speaking']='Hablar';
$texts['school']='colegio';
$texts['scored by']='calificado por';
$texts['scale name']='nombre de la escala';
$texts['submitted']='presentados';
$texts['smartclass information']='Información de Smartclass';
$texts['smartclass requirements']='Requerimientos Smartclass';
$texts['smartclass open']='Abrir sala Smartclass';
$texts['smartclass join']='Unirme a sala Smartclass';


////////////////////---------------------T---------------------------------
$texts['this option is only active in online mode security']='Esta opción  solo está activa  en modo online por seguridad';
$texts['timetable']='Calendario';
$texts['time']='Tiempo';
$texts['time<br>sessions']='Tiempo de<br>Sesiones';
$texts['test day']='Dia de<br>Examen';
$texts['teaching<br>resoruces']='Recursos de<br>Enseñanza';
$texts['teacher resources']='Recursos del docente';
$texts['tab']='pestaña';
$texts['title']='título';
$texts['title or class name']='título o nombre de la clase';
$texts['text']='texto';
$texts['through']='mediante';
$texts['try again']='Intentar otra vez';
$texts['true or false']='Verdadero o Falso';
$texts['there is no file to display']='No hay archivo para mostrar';
$texts['there are no words to display']='No hay palabras para mostrar';
$texts['template']='plantilla';
$texts['today']='Hoy';
$texts['tomorrow']='Mañana';
$texts['total users']='Total de usuarios';
$texts['tool']='Herramienta';
$texts['tools']='Herramientas';
$texts['to']='para';
$texts['to edit']='para editar';
$texts['the template']='la plantilla';
$texts['this paragraph']='este párrafo';
$texts['tag']='etiqueta';
$texts['time in seconds when this text will start']='tiempo en segundos cuando empiece el texto';
$texts['time is over']='el tiempo se acabó';
$texts['the result of your search will be shown here']='El resultado de su búsqueda se mostrará aquí.';
$texts['the file does not exist']='El archivo no existe';
$texts['the file has been already loaded']='El archivo ya se ha cargado';
$texts['time control']='control del tiempo';
$texts['type of score']='tipo de puntuación';
$texts['the student has to improve the following skills']='El alumno tiene que mejorar las siguientes habilidades';
$texts['these are the most outstanding skills of the student']='Estas son las habilidades más destacadas del alumno';
$texts['time on the virtual platform']='Tiempo en la Plataforma Virtual';
$texts['exercises are now emphasized in the skills selected for improvement']='Los ejercicios ahora se enfatizan en la habilidades seleccionadas para mejorar';
$texts['there is no previous report found to compare']='No se encontró ningún informe anterior para comparar';
$texts['tracking']='Seguimiento';
$texts['there are no results to show']='No hay resultados para mostrar';
$texts['there are no students in this group']='No hay alumnos en este grupo';
$texts['the average score will be saved and can not longer modify']='El puntaje promedio será guardado y ya no podrá modificarse';
$texts['teacher returning message']='Mensaje de devolución del profesor';
$texts['this type of exercise can be solved only once']='Este tipo de ejercicio sólo puede ser resuelto una vez';
$texts['this email was generated automatically. please do not reply to this email']='Este email se ha generado automáticamente. Por favor, no contestes a este email';


////////////////////---------------------U---------------------------------
$texts['user'] = 'Usuario';
$texts['users and privileges']='Usuarios y Privilegios';
$texts['users']='Usuarios';
$texts['user or email']='Usuario o correo';
$texts['user information']='Información de usuario';
$texts['update']='Actualizar';



$texts['unit']='Unidad';
$texts['units']='Unidades';
$texts['user Guide']='Guía de Usuario';
$texts['User email has not been invited']='El email del usuario no ha sido invitado';
$texts['user Guide']='Guía de Usuario';
$texts['upload']='Cargar ';
$texts['updated data']='Datos actualizados';
$texts['upload and share files']='Subir y compartir archivos';
$texts['upload file error']='Error al cargar archivo';
$texts['upload file']='cargar archivo';
////////////////////---------------------V---------------------------------
$texts['validate']='Validar';
$texts['vocabulary']='Vocabulario';
$texts['video']='Video';
$texts['video demo']='video demostrativo';
$texts['video and text']='Video y Texto';
$texts['view']='ver';
$texts['virtual classroom']='aula virtual';
////////////////////---------------------W---------------------------------
$texts['warning']='Peligro';
$texts['watch me as a']='Verme como un';
$texts['what do you want to share']='Qué es lo que deseas compartir';
$texts['webcam']='Cámara Web';
$texts['well done!']='¡Bien hecho!';
$texts['welcome to chat']='Bienvenido a la charla';
$texts['welcome to']='Bienvenido a';
$texts['we feel your web browser does not support smartclass, you must use chrome or mozilla']='Lo Sentimos tu navegador Web no soporta Smartclass, debes usar Chrome o Mozilla';
$texts['with images']='con Imágenes';
$texts['windows']='Ventanas';
$texts['words you need to know']='Palabras que necesitas saber';
$texts['word<br>search']='Sopa de<br>letras';
$texts['word']='palabra';
$texts['word search']='Sopa de letras';
$texts['words must have more than one letter']='Las palabras debe tener más de una letra';
$texts['workforce']='Empleados';
$texts['workbook']='Libro de<br>Trabajo';
$texts['what is in the image?']='¿Qué hay en la imagen?';
$texts['write']='escribir';
$texts['write your message']='Escribe tu mensaje';
$texts['writing']='Escribiendo';
$texts['write a word']='Escribe una palabra';
$texts['write tag']='escribir etiqueta';
$texts['we could not find your word']='No pudimos encontrar tu palabra';
$texts['why are you returning this homework?']='¿Por qué está devolviendo esta tarea?';
////////////////////---------------------X---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------Y---------------------------------
$texts['yes']='Si';
$texts['you can not']='No puedes';
$texts['you must click on']='Debes hacer click en';
$texts['you must do the exercise before trying again']='Debes hacer el ejercicio antes de volverlo a intentar';
$texts['you must complete sheets correctly']='Debes completar las fichas correctamente.';
$texts['you have complete the exercise succesfully']='Has completado el ejercicio correctamente';
$texts['you have finished the activity']='Has terminado la actividad';
$texts['you have not set an order in the dialogues in scene']='No ha establecido un orden en los diálogos en escena';
$texts['you have not applied any filter']='No ha aplicado ningún filtro';
$texts['you can continue next exercise']='puedes continuar al siguiente ejercicio';
$texts['you can try again']='puedes intentarlo de nuevo';
$texts['you can not drag another option']='No puedes arrastrar otra opción';
$texts['you can not select another option']='No puedes seleccionar otra opción';
$texts['you must click on "try again"']='Debes hacer click en "Intentar otra vez"';
$texts['you must set an order for the dialog bubbles']='Debe establecer un orden a las burbujas de diálogo';
$texts['you have not finished editing this exercise']='No ha terminado de editar ese ejercicio';
$texts['you must completely solve the following exercise']='Debes resolver por completo el siguiente de ejercicio';

////////////////////---------------------Z---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';



///plantilla
$texts['click and drag']='Click y Arrastra';
$texts['options']='Opciones';
$texts['select box']='Selecciona';
$texts['gap fill']='Llenar el vacío';

$texts['true or false']='Verdadero o Falso';
$texts['join-audio-text-image']='Conectar-Audio-Texto-Imagen';
$texts['put in order']='Poner en orden palabras';
$texts['put in order-paragraph']='Poner en orden oraciones';
$texts['tag image']='Etiquetar Imagen';
$texts['other activities']='Otras Actividades';