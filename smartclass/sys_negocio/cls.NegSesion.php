<?php
/**
 * @autor		Abel Chingo Tello , ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersonal', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatAlumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatRoles', RUTA_BASE, 'sys_datos');
JrCargador::clase('jrAdwen::JrSession');
class NegSesion
{
	protected $oDatAlumno;	
	protected $oDatPersonal;
	protected $oDatRoles;
	public function __construct()
	{
		$this->oDatPersonal = new DatPersonal;
		$this->oDatAlumno = new DatAlumno;		
	}	
	public static function existeSesion()
	{
		$sesion = JrSession::getInstancia();		
		$dni = $sesion->get('dni', false, '__admin_m3c');		
		if(false === $dni) {
			return false;
		} else {
			return true;
		}
	}	
	public static function getUsuario()
	{
		$sesion = JrSession::getInstancia();
		$dni = $sesion->get('dni', false, '__admin_m3c');
		if(empty($dni)) {
			throw new Exception(JrTexto::_('Please sign in.'));
		}		
		return array('dni' => $dni					
					, 'usuario' => $sesion->get('usuario', false, '__admin_m3c')
					, 'email' => $sesion->get('email', false, '__admin_m3c')
					, 'nombre_full' => $sesion->get('nombre_full', false, '__admin_m3c')
					, 'rol' => $sesion->get('rol', false, '__admin_m3c')
					, 'rol_original' => $sesion->get('rol_original', false, '__admin_m3c')	
					, 'idHistorialSesion' => $sesion->get('idHistorialSesion', false, '__admin_m3c')					
					);
	}	
	public function salir()
	{
		return JrSession::limpiarEspacio('__admin_m3c');
	}	
	
	public function ingresar($usu, $clave)
	{//04.04.16
		try {
			if(empty($usu) || empty($clave)) {
				return false;
			}

			$personal = $this->oDatPersonal->getxCredencial($usu, $clave);
			if(!empty($personal)) {
				$usuario=$personal;				
			}else{
				$usuario = $this->oDatAlumno->getxCredencial($usu, $clave);
				if(empty($usuario)) {
					return false;
				}
			}		
			$sesion = JrSession::getInstancia();
			$rol=!empty($usuario['rol'])?$usuario['rol']:'Alumno';
			$sesion->set('dni', $usuario['dni'], '__admin_m3c');
			$sesion->set('usuario', $usuario['usuario'], '__admin_m3c');
			$sesion->set('email', $usuario['email'], '__admin_m3c');
			$sesion->set('rol', $rol , '__admin_m3c');
			$sesion->set('rol_original', $rol , '__admin_m3c');		
			$sesion->set('nombre_full', $usuario['ape_paterno'].' '.$usuario['ape_materno'].", ".$usuario['nombre'], '__admin_m3c');
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
	
	public static function get($prop, $espacio = '__admin_m3c')
	{
		$sesion = JrSession::getInstancia();		
		return $sesion->get($prop, null, $espacio);
	}
	
	public static function set_($prop, $valor)
	{
		$sesion = JrSession::getInstancia();		
		$dni = $sesion->get('dni', false, '__admin_m3c');
		if(empty($dni)) {
			throw new Exception(JrTexto::_('Please sign in.'));
		}		
		return $sesion->set($prop, $valor, '__admin_m3c');
	}	
	
	public static function set($prop, $valor, $espacio = '__admin_m3c')
	{
        
		$sesion = JrSession::getInstancia();
		return $sesion->set($prop, $valor, $espacio);
	}	

	public static function tiene_acceso($menu, $accion)
	{
		
		if(!NegSesion::existeSesion()) {
			return false;
		}		
		if('superadmin' == NegSesion::get('rol')) {
			return true;
		}
		$oDatRoles=new DatRoles;
		$existe_privilegio = $oDatRoles->existe_permiso(NegSesion::get('rol'),$menu, $accion);
		if(!$existe_privilegio){
			return false;
		}		
		return true;
	}
}