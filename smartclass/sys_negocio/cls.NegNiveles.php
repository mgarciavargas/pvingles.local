<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		14-11-2016
 * @copyright	Copyright (C) 14-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNiveles', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegNiveles 
{
	protected $idnivel;
	protected $nombre;
	protected $tipo;
	protected $idpadre;
	protected $idpersonal;
	protected $estado;
	protected $orden;
	protected $imagen;
	
	protected $dataNiveles;
	protected $oDatNiveles;	

	public function __construct()
	{
		$this->oDatNiveles = new DatNiveles;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatNiveles->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatNiveles->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatNiveles->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarActxIdNivel($filtros = array())
	{
		try {
			$unidades = $this->oDatNiveles->buscar($filtros);
			$arrActividades = array();
			if(!empty($unidades)){
				foreach ($unidades as $u) {
					$acts = $this->oDatNiveles->buscar(array(
						'tipo'=>'L',
						'idpadre'=> $u['idnivel'],
					));
					$arrActividades = array_merge($arrActividades, $acts);
				}
			}
			return $arrActividades;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarNiveles($filtros = array())
	{
		try {			
			 $sysnivel=$this->oDatNiveles->buscar($filtros);
			 $niveles=array();
			 foreach ($sysnivel as $nivel){
			 	$niveles[$nivel["idnivel"]]=$nivel;
			 	$filtros["tipo"]='U';
			 	$filtros["idpadre"]=$nivel["idnivel"];
			 	$sysunidad=$this->oDatNiveles->buscar($filtros);
			 	$niveles[$nivel["idnivel"]]["nunidades"]=count($sysunidad);
			 	$nactividades=0;
  			    foreach ($sysunidad as $unidad){
  			    	$niveles[$nivel["idnivel"]]["unidades"][$unidad["idnivel"]]=$unidad;  			    		 	
				 	$filtros["tipo"]='L';
			 		$filtros["idpadre"]=$unidad["idnivel"];
				 	$sysactividad=$this->oDatNiveles->buscar($filtros);	
				 	$nactividades=$nactividades+count($sysactividad);
				 	$niveles[$nivel["idnivel"]]["unidades"][$unidad["idnivel"]]["nactividades"]=count($sysactividad);
	  			    foreach ($sysactividad as $actividad){
					 	$niveles[$nivel["idnivel"]]["unidades"][$unidad["idnivel"]]["actividades"][$actividad["idnivel"]]=$actividad;					 	
					}			 	
				}
				$niveles[$nivel["idnivel"]]["nactividades"]=$nactividades;
			 }
			 return $niveles;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatNiveles->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatNiveles->get($this->idnivel);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('niveles', 'add')&&$this->tipo=='N') {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			else if(!NegSesion::tiene_acceso('unidad', 'add')&&$this->tipo=='U') {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			else if(!NegSesion::tiene_acceso('actividad', 'add')&&$this->tipo=='L') {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatNiveles->iniciarTransaccion('neg_i_Niveles');
			$this->idnivel = $this->oDatNiveles->insertar($this->nombre,$this->tipo,$this->idpadre,$this->idpersonal,$this->estado,$this->orden,$this->imagen);
			//$this->oDatNiveles->terminarTransaccion('neg_i_Niveles');	
			return $this->idnivel;
		} catch(Exception $e) {	
		   //$this->oDatNiveles->cancelarTransaccion('neg_i_Niveles');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('niveles', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatNiveles->actualizar($this->idnivel,$this->nombre,$this->tipo,$this->idpadre,$this->idpersonal,$this->estado,$this->orden,$this->imagen);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('niveles', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNiveles->eliminar($this->idnivel);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdnivel($pk){
		try {
			$this->dataNiveles = $this->oDatNiveles->get($pk);
			if(empty($this->dataNiveles)) {
				throw new Exception(JrTexto::_("Niveles").' '.JrTexto::_("not registered"));
			}
			$this->idnivel = $this->dataNiveles["idnivel"];
			$this->nombre = $this->dataNiveles["nombre"];
			$this->tipo = $this->dataNiveles["tipo"];
			$this->idpadre = $this->dataNiveles["idpadre"];
			$this->idpersonal = $this->dataNiveles["idpersonal"];
			$this->estado = $this->dataNiveles["estado"];
			$this->orden = $this->dataNiveles["orden"];
			$this->imagen = $this->dataNiveles["imagen"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('niveles', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataNiveles = $this->oDatNiveles->get($pk);
			if(empty($this->dataNiveles)) {
				throw new Exception(JrTexto::_("Niveles").' '.JrTexto::_("not registered"));
			}

			return $this->oDatNiveles->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function ordenar($frm){
		try {
				if(empty($frm))	return false;
			    $accion=intval($frm["ordenaa"]);
				$orden=intval($frm["ordenactual"])+$accion;		
				$id=$frm["id"];
				if($orden==0) return false;
				$filtro['tipo']=$frm["tipo"];
				$datos=$this->buscar($filtro);
				$i=0;	
				foreach ($datos as $nivel){	
					if($nivel["idnivel"]==$id)
						$this->setCampo($id,'orden',$orden);
					else{
						$i++;
						if($i==$orden) $i++;
						$this->setCampo($nivel["idnivel"],'orden',$i);
					}
				}
				return true;

			} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}		
}