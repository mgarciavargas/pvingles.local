<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		22-11-2016
 * @copyright	Copyright (C) 22-11-2016. Todos los derechos reservados.
 */
 defined('RUTA_BASE') or die();
 JrCargador::clase('sys_datos::DatActividades', RUTA_BASE, 'sys_datos');
 JrCargador::clase('sys_datos::DatActividad_detalle', RUTA_BASE, 'sys_datos');
 JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
 JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
 JrCargador::clase('sys_datos::DatConfiguraciontablas', RUTA_BASE, 'sys_datos');
 JrCargador::clase('sys_datos::DatNiveles', RUTA_BASE, 'sys_datos');
 class NegActividad
 {
	//actividad
 	protected $idactividad;
 	protected $nivel;
 	protected $unidad;
 	protected $sesion;
 	protected $metodologias;
 	protected $habilidades;
 	protected $titulos;
 	protected $descripcion;
 	protected $estado;
 	protected $url;
 	protected $idioma;
 	protected $iduser;


	//detalle
 	protected $det_pregunta;
 	protected $det_tipo;
 	protected $det_orden;
 	protected $det_tipo_desarrollo;
 	protected $det_tipo_actividad;
 	protected $det_url;
 	protected $det_texto;
 	protected $det_texto_edit;
 	protected $det_usuarios;
 	protected $det_users;
 	protected $det_iddetalle;

    //configuracion
 	protected $rutabase;

 	protected $dataActividades;
 	protected $oDatActividades;
 	protected $oDatActividad_detalle;
 	protected $oDatconfiguraciontablas;
 	protected $oDatNiveles;

 	protected $oNegMetodologia;
 	protected $cont=1;

 	public function __construct()
 	{
 		$this->oDatActividades = new DatActividades;
 		$this->oDatActividad_detalle = new DatActividad_detalle;
 		$this->oNegMetodologia = new NegMetodologia_habilidad;
 		$this->oDatconfiguraciontablas = new Datconfiguraciontablas;
 		$this->oDatNiveles = new DatNiveles;
 	}

 	public function __get($prop)
 	{
 		$metodo = 'get' . ucfirst($prop);

 		if(method_exists($this, $metodo)) {
 			return $this->$metodo();
 		} else {
 			return $this->$prop;
 		}
 	}

 	public function __set($prop, $valor)
 	{
 		$this->set($prop, $valor);
 	}

 	private function prop__($prop, $valor)
 	{
 		if(is_array($prop)) {
 			foreach($prop as $prop_ => $valor) {
 				$this->set($prop_, $valor);
 			}
 		}

 		$this->set($prop, $valor);
 	}
 	public function get($prop)
 	{
 		$metodo = 'get' . ucfirst($prop);
 		if(method_exists($this, $metodo)) {
 			return $this->$metodo();
 		} else {
 			return $this->$prop;
 		}
 	}

 	public function set($prop, $valor)
 	{
 		$metodo = 'set' . ucfirst($prop);
 		if(method_exists($this, $metodo)) {
 			$this->$metodo($valor);
 		} else {
 			$this->$prop = $valor;
 		}
 	}

 	public function listar()
 	{
 		try {
 			return $this->oDatActividades->listarall();
 		} catch(Exception $e) {
 			throw new Exception($e->getMessage());
 		}
 	}

 	public function nintentos($idnivel){
 		$nivel=$this->oDatNiveles->get($idnivel);
 		if(!empty($nivel["idnivel"])){
 			$filtros=array('nivel'=>$nivel["nombre"],'ventana'=>'D','atributo'=>'I');
 			$reg=$this->oDatconfiguraciontablas->buscar($filtros);
 			if(!empty($reg)){
 				return $reg[0]["valor"];
 			}
 			return 0;
 		}
 	}

 	public function fullActividades($filtros=null){
 		$fullActividades=array();
 		$fullEjercicios=array();
 		$condact = array();
 		$filtromet=array();
 		$filtrodet=array();
 		if(!empty($filtros["idactividad"])) {
 			$condact["idactividad"]=$filtros["idactividad"];
 		}
 		if(!empty($filtros["nivel"])){
 			$condact["nivel"]=$filtros["nivel"];
 		}
 		if(!empty($filtros["unidad"])){
 			$condact["unidad"]=$filtros["unidad"];
 		}
 		if(!empty($filtros["sesion"])){
 			$condact["sesion"]=$filtros["sesion"];
 		}
 		if(!empty($filtros["idmetodologia"])){
 			$filtromet["idmetodologia"]=$filtros["idmetodologia"];
 		}
 		if(!empty($filtros["titulodetalle"])){
 			$filtrodet["titulo"]=$filtros["titulodetalle"];
 		}
 		$filtromet['tipo']='M';
 		$metodologias=$this->oNegMetodologia->buscar($filtromet);
 		$idet=0;
 		if(!empty($metodologias))
 			foreach ($metodologias as $vmet){
				$condact["metodologia"]=$vmet["idmetodologia"];   //parametro adicional para leer las actividades
				$actividad=$this->oDatActividades->buscar($condact);
				if(!empty($actividad)){
					$fullActividades[$vmet["idmetodologia"]]["met"]=$vmet;
					$fullActividades[$vmet["idmetodologia"]]["act"]=$actividad[0];
					foreach ($actividad as $value){
						$filtrodet['idactividad']=$value["idactividad"];
						$act_det=$this->oDatActividad_detalle->buscar($filtrodet);
						if(!empty($act_det)){
							$idet++;
							$fullActividades[$vmet["idmetodologia"]]["acts"][$value["idactividad"]]=$act_det;
							$fullActividades[$vmet["idmetodologia"]]["act"]["det"]=$act_det;
							$fullEjercicios[]=$act_det;
						}
					}
					unset($actividad);
				}
			}

		if(!empty($filtros["ejercicios"])){
			return $fullEjercicios;
		}else{
			return $fullActividades;
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatActividades->get($this->idactividad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$_metodologias=explode("|",trim($this->metodologias));
			$iddet=array();
			foreach ($_metodologias as $met){
				$deletedetalle=array();
			    $nodeletedetalle=array();
				$_titulo=@$this->titulos[$met];
				$_descripcion=@$this->descripcion[$met];
				$_habil=@$this->habilidades[$met];
				$actexiste=$this->oDatActividades->buscar(array('nivel'=>$this->nivel,'unidad'=>$this->unidad,'sesion'=>$this->sesion,'metodologia'=>$met));
				if(empty($actexiste[0])){
					$actcur=$this->oDatActividades->insertar($this->nivel,$this->unidad,$this->sesion,$met,@$_habil[1],@$_titulo[1],@$_descripcion[1],1,'','EN',$this->iduser);
				}else{
					$actcur=$actexiste[0]["idactividad"];
					$this->oDatActividades->actualizar($actcur,$this->nivel,$this->unidad,$this->sesion,$met,@$_habil[1],@$_titulo[1],@$_descripcion[1],1,'','EN',$this->iduser);
					$alldetalles=$this->oDatActividad_detalle->buscar(array('idactividad'=>$actcur));
					foreach ($alldetalles as $detalle){
						$deletedetalle[]=$detalle["iddetalle"];
					}
				}
				$orden=0;
				$pregunta=@$this->det_pregunta[$met];
				$url=@$this->det_url[$met];
				$tipo=@$this->det_tipo[$met];
				$texto_edit=@$this->det_texto_edit[$met];
				$tipo_desarrollo=@$this->det_tipo_desarrollo[$met];
				$tipo_actividad=@$this->det_tipo_actividad[$met];
				$texto__=@$this->det_texto[$met];
				$_users=@$this->det_users[$met];
				$_iddetalles=$this->det_iddetalle[$met];
				ksort($texto__);
				if(!empty($texto__)){
					foreach ($texto__ as $index => $value){
							$orden++;
							$texto=$value;
							$preg_=@$pregunta[$index];
							$url_=@$url[$index];
							$tipo_=@$tipo[$index];
							$tipo_des=@$tipo_desarrollo[$index];
							$tipo_act=@$tipo_actividad[$index];
							$texto_edit_=@$texto_edit[$index];
							$hab_=@$_habil[$index];
							$tit=@$_titulo[$index];
							$des=@$_descripcion[$index];
							$user=@$_users[$index];
							$iddetalle=intval(!empty($_iddetalles[$index])?$_iddetalles[$index]:0);
							if($iddetalle>0)$nodeletedetalle[]=intval($iddetalle);
							if(!empty($texto)){
								$det=$this->agregar_detalle($iddetalle,$actcur,$preg_,$index,$url_,$tipo_,$tipo_des,$tipo_act,$hab_,$texto,$texto_edit_,$tit,$des,$user);
								$iddet[$met][$index]=$det;
							}
						}
					}
					if(!empty($deletedetalle)){
						$deletedetalle=array_diff($deletedetalle,$nodeletedetalle);
						foreach ($deletedetalle as $k=>$vdet){
							$this->oDatActividad_detalle->eliminar(intval($vdet));
						}
					}
				}
				return $iddet;
			} catch(Exception $e) {
				throw new Exception($e->getMessage());
			}
		}

		public function agregar_detalle($iddetalle,$actcur,$pregunta,$orden,$url,$tipo,$tipo_desarrollo,$tipo_actividad,$idhabilidad=null,$texto=null,$texto_edit,$tit=null,$des=null,$user=null){
			if(!empty($texto_edit)){
				$texto_edit=@str_replace($this->rutabase,'__xRUTABASEx__',$texto_edit);
				$texto=@str_replace($this->rutabase,'__xRUTABASEx__',$texto);
			}
			$idet=intval($iddetalle);

			if($idet>0){
        $texto =@str_replace("michelle");

				$det=$this->oDatActividad_detalle->get($idet);
				if(!empty($det)){
					$idet=$this->oDatActividad_detalle->actualizar($iddetalle,$actcur,$pregunta,$orden,$url,$tipo,$tipo_desarrollo,$tipo_actividad,$idhabilidad,trim($texto),trim($texto_edit),trim($tit),trim($des),trim($user));
				}else
				$idet=$this->oDatActividad_detalle->insertar($actcur,$pregunta,$orden,$url,$tipo,$tipo_desarrollo,$tipo_actividad,$idhabilidad,trim($texto),trim($texto_edit),trim($tit),trim($des),trim($user));
			}else{
        // cambiar aqui la variable texto cuando se crea el reconocimiento de voz
				$idet=$this->oDatActividad_detalle->insertar($actcur,$pregunta,$orden,$url,$tipo,$tipo_desarrollo,$tipo_actividad,$idhabilidad,trim($texto),trim($texto_edit),trim($tit),trim($des),trim($user));
      }
      return $idet;
		}

		public function tplxMetodologia($met){
			$tpl=Array();
			if($met==1){
				$tpl["padre"]=false;
				$tpl["tpl"]=Array(
					'look_video'=>JrTexto::_('Video'),
					'look_video_texto'=>JrTexto::_('Video and text'),
					'look_imagen'=>JrTexto::_('Photo and audio'),
					'look_audio'=>JrTexto::_('Audio'),
					'look_dialogo'=>JrTexto::_('Dialogue'),
					);
			}else if ($met==2){
				$tpl["padre"]=true;
				$tpl["tpl"]=Array(
					'practice_arrastre'=>JrTexto::_('Drag and drop'),
					'practice_mult_choice'=>JrTexto::_('Multiple choice'),
					'practice_opc_lista'=>JrTexto::_('List options'),
					'practice_rellena_orac'=>JrTexto::_('Complete sentence'),
					'practice_mult_choice_fig'=>JrTexto::_('Multiple choice').' '.JrTexto::_('with images'),
					'practice_verdad_falso'=>JrTexto::_('True or false'),
					'practice_seriada'=>JrTexto::_('Serial with level of complexity'),
					'practice_fichas'=>JrTexto::_('Sheet').'s',
					'practice_ordenar_simple'=>ucfirst(JrTexto::_('order')).' '.JrTexto::_('letter').'s '.JrTexto::_('or').' '.JrTexto::_('word').'s',
					'practice_ordenar_parrafos'=>ucfirst(JrTexto::_('order')).' '.JrTexto::_('paragraph').'s ',
					'practice_imagen_puntos'=>ucfirst(JrTexto::_('image tagging')),
					);
			}else{
				$tpl["padre"]=true;
				$tpl["tpl"]=Array(
					'dby_completar_arrastre'=>JrTexto::_('Drag and drop'),
					'dby_completar_mult_choice'=>JrTexto::_('Multiple choice'),
					'dby_completar_opc_lista'=>JrTexto::_('List options'),
					'dby_completar_rellena_orac'=>JrTexto::_('Complete sentence'),
					'dby_completar_mult_choice_fig'=>JrTexto::_('Multiple choice').' '.JrTexto::_('with images'),
					'dby_verdad_falso'=>JrTexto::_('True or false'),
					//'dby_seriada'=>JrTexto::_('Serial with level of complexity'),
					);
			}
			return $tpl;
		}


		public function tplxMetodologia2($met){
			$tpl=Array();
			if($met==1){
				$tpl["padre"]=false;
				$tpl["tpl"]=Array(
					1=>Array('page'=>'look_video','name'=>JrTexto::_('Video'),'img'=>'video','color'=>''),
					2=>Array('page'=>'look_video_texto','name'=>JrTexto::_('Video and text'),'img'=>'video_texto','color'=>''),
					3=>Array('page'=>'look_imagen','name'=>JrTexto::_('Photo and audio'),'img'=>'imagen_audio','color'=>''),
					4=>Array('page'=>'look_audio','name'=>JrTexto::_('Audio'),'img'=>'audio','color'=>''),
					5=>Array('page'=>'look_dialogo','name'=>JrTexto::_('Dialogue'),'img'=>'conversacion','color'=>''),
					);
			}else if ($met==2){
				$tpl["padre"]=true;
				$tpl["showtime"]=0;
				$tpl["showpuntaje"]=0;
				$tpl["timedefualt"]='0';
				$tpl["tpl"]=Array(
					1=>Array('page'=>'pra_arrastre_texto',
						    'name'=>ucfirst(JrTexto::_('Click and drag')),
						    'color'=>'#fc5c5c',
						    'showasistido'=>'1',
						    'showalternativas'=>'1',
						    'addclass'=>'isdrop',
						    'items'=>Array(	1=>Array('name'=>ucfirst(JrTexto::_('Text')),'img'=>'1','video'=>'1','tools'=>'chingoinput'),
									    	2=>Array('name'=>ucfirst(JrTexto::_('Image')),'img'=>'2','video'=>'2','tools'=>'chingoinput chingoimage'),
									    	3=>Array('name'=>ucfirst(JrTexto::_('Audio')),'img'=>'3','video'=>'3','tools'=>'chingoinput chingoaudio'),
									    	4=>Array('name'=>ucfirst(JrTexto::_('Video')),'img'=>'4','video'=>'4','tools'=>'chingoinput chingovideo'),
									    	)),
					2=>Array('page'=>'pra_arrastre_texto',
						    'name'=>ucfirst(JrTexto::_('Options')),
						    'color'=>'#5a89f8','showasistido'=>'1','showalternativas'=>'1','addclass'=>'isclicked',
						    'items'=>Array(	1=>Array('name'=>ucfirst(JrTexto::_('Text')),'img'=>'5','video'=>'5','tools'=>'chingoinput'),
									    	2=>Array('name'=>ucfirst(JrTexto::_('Image')),'img'=>'6','video'=>'6','tools'=>'chingoinput chingoimage'),
									    	3=>Array('name'=>ucfirst(JrTexto::_('Audio')),'img'=>'7','video'=>'7','tools'=>'chingoinput chingoaudio'),
									    	4=>Array('name'=>ucfirst(JrTexto::_('Video')),'img'=>'8','video'=>'8','tools'=>'chingoinput chingovideo')
									    	)),
					3=>Array('page'=>'pra_arrastre_texto',
						    'name'=>ucfirst(JrTexto::_('Select box')),
						    'color'=>'#6ac182','showasistido'=>'1','showalternativas'=>'1','addclass'=>'ischoice',
						    'items'=>Array(	1=>Array('name'=>ucfirst(JrTexto::_('Text')),'img'=>'9','video'=>'9','tools'=>'chingoinput'),
									    	2=>Array('name'=>ucfirst(JrTexto::_('Image')),'img'=>'10','video'=>'10','tools'=>'chingoinput chingoimage'),
									    	3=>Array('name'=>ucfirst(JrTexto::_('Audio')),'img'=>'11','video'=>'11','tools'=>'chingoinput chingoaudio'),
									    	4=>Array('name'=>ucfirst(JrTexto::_('Video')),'img'=>'12','video'=>'12','tools'=>'chingoinput chingovideo')
									    	)),
					4=>Array('page'=>'pra_arrastre_texto',
						    'name'=>ucfirst(JrTexto::_('Gap fill')),
						    'color'=>'#f5ab40','showasistido'=>'1','addclass'=>'iswrite',
						    'items'=>Array(	1=>Array('name'=>ucfirst(JrTexto::_('Text')),'img'=>'13','video'=>'13','tools'=>'chingoinput'),
									    	2=>Array('name'=>ucfirst(JrTexto::_('Image')),'img'=>'14','video'=>'14','tools'=>'chingoinput chingoimage'),
									    	3=>Array('name'=>ucfirst(JrTexto::_('Audio')),'img'=>'15','video'=>'15','tools'=>'chingoinput chingoaudio'),
									    	4=>Array('name'=>ucfirst(JrTexto::_('Video')),'img'=>'16','video'=>'16','tools'=>'chingoinput chingovideo')
									    	)),
					5=>Array('page'=>'other',
						    'name'=>ucfirst(JrTexto::_('Other Exercises')),
						    'items'=>Array(	1=>Array('page'=>'practice_verdad_falso','name'=>JrTexto::_('True or false'),'img'=>'17','color'=>'','video'=>'17'),
											2=>Array('page'=>'practice_fichas','name'=>JrTexto::_('Join-Audio-Text-Image'),'img'=>'18','color'=>'','video'=>'18'),
											3=>Array('page'=>'practice_ordenar_simple','name'=>ucfirst(JrTexto::_('Put in order')),'img'=>'19','color'=>'','video'=>'19'),
											4=>Array('page'=>'practice_ordenar_parrafos','name'=>ucfirst(JrTexto::_('Put in order-Paragraph')),'img'=>'20','color'=>'','video'=>'20'),
											5=>Array('page'=>'practice_imagen_puntos','name'=>ucfirst(JrTexto::_('Tag image')),'img'=>'21','color'=>'','video'=>'21'),
                      6=>Array('page'=>'voice_recog', 'name'=>ucfirst(JrTexto::_('Pronunciation Practice')),'img'=>'22', 'color'=>'', 'video'=>''),
                      )),

					);
			}else{
				$tpl["padre"]=true;
				$tpl["showtime"]=1;
				$tpl["showpuntaje"]=0;
				$tpl["timedefualt"]='01:00';
				$tpl["tpl"]=Array(
					1=>Array('page'=>'dby_multiplantilla',
						    'name'=>ucfirst(JrTexto::_('Click and drag')),
						    'color'=>'#fc5c5c',
						    'showasistido'=>'0',
						    'showalternativas'=>'1',
						    'addclass'=>'isdrop',
						    'items'=>Array(	1=>Array('name'=>ucfirst(JrTexto::_('Text')),'img'=>'1','video'=>'1','tools'=>'chingoinput'),
									    	2=>Array('name'=>ucfirst(JrTexto::_('Image')),'img'=>'2','video'=>'2','tools'=>'chingoinput chingoimage'),
									    	3=>Array('name'=>ucfirst(JrTexto::_('Audio')),'img'=>'3','video'=>'3','tools'=>'chingoinput chingoaudio'),
									    	4=>Array('name'=>ucfirst(JrTexto::_('Video')),'img'=>'4','video'=>'4','tools'=>'chingoinput chingovideo'),
									    	)),
					2=>Array('page'=>'dby_multiplantilla',
						    'name'=>ucfirst(JrTexto::_('Options')),
						    'color'=>'#5a89f8','showasistido'=>'0','showalternativas'=>'1','addclass'=>'isclicked',
						    'items'=>Array(	1=>Array('name'=>ucfirst(JrTexto::_('Text')),'img'=>'5','video'=>'5','tools'=>'chingoinput'),
									    	2=>Array('name'=>ucfirst(JrTexto::_('Image')),'img'=>'6','video'=>'6','tools'=>'chingoinput chingoimage'),
									    	3=>Array('name'=>ucfirst(JrTexto::_('Audio')),'img'=>'7','video'=>'7','tools'=>'chingoinput chingoaudio'),
									    	4=>Array('name'=>ucfirst(JrTexto::_('Video')),'img'=>'8','video'=>'8','tools'=>'chingoinput chingovideo')
									    	)),
					3=>Array('page'=>'dby_multiplantilla',
						    'name'=>ucfirst(JrTexto::_('Select box')),
						    'color'=>'#6ac182','showasistido'=>'0','showalternativas'=>'1','addclass'=>'ischoice',
						    'items'=>Array(	1=>Array('name'=>ucfirst(JrTexto::_('Text')),'img'=>'9','video'=>'9','tools'=>'chingoinput'),
									    	2=>Array('name'=>ucfirst(JrTexto::_('Image')),'img'=>'10','video'=>'10','tools'=>'chingoinput chingoimage'),
									    	3=>Array('name'=>ucfirst(JrTexto::_('Audio')),'img'=>'11','video'=>'11','tools'=>'chingoinput chingoaudio'),
									    	4=>Array('name'=>ucfirst(JrTexto::_('Video')),'img'=>'12','video'=>'12','tools'=>'chingoinput chingovideo')
									    	)),
					4=>Array('page'=>'dby_multiplantilla',
						    'name'=>ucfirst(JrTexto::_('Gap fill')),
						    'color'=>'#f5ab40','showasistido'=>'0','addclass'=>'iswrite',
						    'items'=>Array(	1=>Array('name'=>ucfirst(JrTexto::_('Text')),'img'=>'13','video'=>'13','tools'=>'chingoinput'),
									    	2=>Array('name'=>ucfirst(JrTexto::_('Image')),'img'=>'14','video'=>'14','tools'=>'chingoinput chingoimage'),
									    	3=>Array('name'=>ucfirst(JrTexto::_('Audio')),'img'=>'15','video'=>'15','tools'=>'chingoinput chingoaudio'),
									    	4=>Array('name'=>ucfirst(JrTexto::_('Video')),'img'=>'16','video'=>'16','tools'=>'chingoinput chingovideo')
									    	)),
					5=>Array('page'=>'other',
						    'name'=>ucfirst(JrTexto::_('Other activities')),
						    'items'=>Array(	1=>Array('page'=>'dby_verdad_falso','name'=>JrTexto::_('True or false'),'img'=>'17','color'=>'','video'=>'17'),
											2=>Array('page'=>'dby_fichas','name'=>JrTexto::_('Join-Audio-Text-Image'),'img'=>'18','color'=>'','video'=>'18'),
											3=>Array('page'=>'dby_ordenar_simple','name'=>ucfirst(JrTexto::_('Put in order')),'img'=>'19','color'=>'','video'=>'19'),
											4=>Array('page'=>'dby_ordenar_parrafos','name'=>ucfirst(JrTexto::_('Put in order-Paragraph')),'img'=>'20','color'=>'','video'=>'20'),
											5=>Array('page'=>'dby_imagen_puntos','name'=>ucfirst(JrTexto::_('Tag image')),'img'=>'21','color'=>'','video'=>'21'),
                      6=>Array('page'=>'dby_voice_recog','name'=>ucfirst(JrTexto::_('Pronunciation Practice')),'img'=>'22','color'=>'','video'=>'')
                        )),

					);
			}
			return $tpl;
		}

	}
