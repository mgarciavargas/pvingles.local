<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		24-05-2017
 * @copyright	Copyright (C) 24-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAulavirtualinvitados', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAulavirtualinvitados 
{
	protected $idinvitado;
	protected $idaula;
	protected $dni;
	protected $email;
	protected $asistio;
	protected $como;
	protected $usuario;
	
	protected $dataAulavirtualinvitados;
	protected $oDatAulavirtualinvitados;	

	public function __construct()
	{
		$this->oDatAulavirtualinvitados = new DatAulavirtualinvitados;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAulavirtualinvitados->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAulavirtualinvitados->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAulavirtualinvitados->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAulavirtualinvitados->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAulavirtualinvitados->get($this->idinvitado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {

			$invitado=$this->buscar(array('idaula'=>$this->idaula,'email'=>$this->email));
			if(empty($invitado))
				$this->idinvitado = $this->oDatAulavirtualinvitados->insertar($this->idaula,$this->dni,$this->email,$this->asistio,$this->como,$this->usuario);
			else{
				$invitado=$invitado[0];
				$this->idinvitado = $this->oDatAulavirtualinvitados->actualizar(@$invitado["idinvitado"],$this->idaula,@$invitado["dni"],$this->email,$this->asistio,$this->como,$this->usuario);
			}
				
			return $this->idinvitado;
		} catch(Exception $e) {	
		   		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
						
			return $this->oDatAulavirtualinvitados->actualizar($this->idinvitado,$this->idaula,$this->dni,$this->email,$this->asistio,$this->como,$this->usuario);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminarxfiltro($filtros)
	{
		try {		
			return $this->oDatAulavirtualinvitados->eliminarxfiltro($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function eliminar()
	{
		try {			
			return $this->oDatAulavirtualinvitados->eliminar($this->idinvitado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdinvitado($pk){
		try {
			$this->dataAulavirtualinvitados = $this->oDatAulavirtualinvitados->get($pk);
			if(empty($this->dataAulavirtualinvitados)) {
				throw new Exception(JrTexto::_("Aulavirtualinvitados").' '.JrTexto::_("not registered"));
			}
			$this->idinvitado = $this->dataAulavirtualinvitados["idinvitado"];
			$this->idaula = $this->dataAulavirtualinvitados["idaula"];
			$this->dni = $this->dataAulavirtualinvitados["dni"];
			$this->email = $this->dataAulavirtualinvitados["email"];
			$this->asistio = $this->dataAulavirtualinvitados["asistio"];
			$this->como = $this->dataAulavirtualinvitados["como"];
			$this->usuario = $this->dataAulavirtualinvitados["usuario"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('aulavirtualinvitados', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataAulavirtualinvitados = $this->oDatAulavirtualinvitados->get($pk);
			if(empty($this->dataAulavirtualinvitados)) {
				throw new Exception(JrTexto::_("Aulavirtualinvitados").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAulavirtualinvitados->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}		
}