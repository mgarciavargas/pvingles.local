<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		24-07-2017
 * @copyright	Copyright (C) 24-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAlumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAlumno 
{
	protected $dni;
	protected $ape_paterno;
	protected $ape_materno;
	protected $nombre;
	protected $fechanac;
	protected $sexo;
	protected $estado_civil;
	protected $ubigeo;
	protected $urbanizacion;
	protected $direccion;
	protected $telefono;
	protected $celular;
	protected $email;
	protected $idugel;
	protected $regusuario;
	protected $regfecha;
	protected $usuario;
	protected $clave;
	protected $token;
	protected $foto;
	protected $estado;
	protected $situacion;
	
	protected $dataAlumno;
	protected $oDatAlumno;	

	public function __construct()
	{
		$this->oDatAlumno = new DatAlumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAlumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAlumno->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAlumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAlumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAlumno->get($this->dni);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try{			
			$this->oDatAlumno->iniciarTransaccion('neg_i_Alumno');
			$this->dni = $this->oDatAlumno->insertar($this->ape_paterno,$this->ape_materno,$this->nombre,$this->fechanac,$this->sexo,$this->estado_civil,$this->ubigeo,$this->urbanizacion,$this->direccion,$this->telefono,$this->celular,$this->email,$this->idugel,$this->regusuario,$this->regfecha,$this->usuario,$this->clave,$this->token,$this->foto,$this->estado,$this->situacion);
			$this->oDatAlumno->terminarTransaccion('neg_i_Alumno');	
			return $this->dni;
		} catch(Exception $e) {	
		    $this->oDatAlumno->cancelarTransaccion('neg_i_Alumno');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try{						
			return $this->oDatAlumno->actualizar($this->dni,$this->ape_paterno,$this->ape_materno,$this->nombre,$this->fechanac,$this->sexo,$this->estado_civil,$this->ubigeo,$this->urbanizacion,$this->direccion,$this->telefono,$this->celular,$this->email,$this->idugel,$this->regusuario,$this->regfecha,$this->usuario,$this->clave,$this->token,$this->foto,$this->estado,$this->situacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatAlumno->cambiarvalorcampo($this->dni,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatAlumno->eliminar($this->dni);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setDni($pk){
		try {
			$this->dataAlumno = $this->oDatAlumno->get($pk);
			if(empty($this->dataAlumno)) {
				throw new Exception(JrTexto::_("Alumno").' '.JrTexto::_("not registered"));
			}
			$this->dni = $this->dataAlumno["dni"];
			$this->ape_paterno = $this->dataAlumno["ape_paterno"];
			$this->ape_materno = $this->dataAlumno["ape_materno"];
			$this->nombre = $this->dataAlumno["nombre"];
			$this->fechanac = $this->dataAlumno["fechanac"];
			$this->sexo = $this->dataAlumno["sexo"];
			$this->estado_civil = $this->dataAlumno["estado_civil"];
			$this->ubigeo = $this->dataAlumno["ubigeo"];
			$this->urbanizacion = $this->dataAlumno["urbanizacion"];
			$this->direccion = $this->dataAlumno["direccion"];
			$this->telefono = $this->dataAlumno["telefono"];
			$this->celular = $this->dataAlumno["celular"];
			$this->email = $this->dataAlumno["email"];
			$this->idugel = $this->dataAlumno["idugel"];
			$this->regusuario = $this->dataAlumno["regusuario"];
			$this->regfecha = $this->dataAlumno["regfecha"];
			$this->usuario = $this->dataAlumno["usuario"];
			$this->clave = $this->dataAlumno["clave"];
			$this->token = $this->dataAlumno["token"];
			$this->foto = $this->dataAlumno["foto"];
			$this->estado = $this->dataAlumno["estado"];
			$this->situacion = $this->dataAlumno["situacion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataAlumno = $this->oDatAlumno->get($pk);
			if(empty($this->dataAlumno)) {
				throw new Exception(JrTexto::_("Alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAlumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function setEmail($email)
	{
		try {
			$this->email= NegTools::validar('todo', $email, false, JrTexto::_("Please enter a valid email"), array("longmax" => 180));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	
	private function setUsuario($usuario)
	{
		try {
			$this->usuario= NegTools::validar('todo', $usuario, false, JrTexto::_("Please enter a valid user"), array("longmax" => 80));
			if($this->dataAlumno['usuario'] != $usuario) {
				$existe = $this->oDatAlumno->getxusuario($usuario);
				if(!empty($existe)) {
					throw new Exception(JrTexto::_('The user entered is already in use'));
				}
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setClave($clave)
	{
		try {
			if(empty($clave)) {
				throw new Exception(JrTexto::_('Enter password'));
			}
			$this->clave= $clave;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}