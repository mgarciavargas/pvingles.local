<?php
/**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-08-2016  
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */

class DatRol extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Roles").": " . $e->getMessage());
		}
	}

	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM roles";
			
			$cond = array();		
			
			if(!empty($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(!empty($filtros["rol"])) {
					$cond[] = "rol = " . $this->oBD->escapar($filtros["rol"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Roles").": " . $e->getMessage());
		}
	}

public function buscar($filtros=null)
{
		try {
			$sql = "SELECT * FROM roles";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(!empty($filtros["rol"])) {
					$cond[] = "rol = " . $this->oBD->escapar($filtros["rol"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Roles").": " . $e->getMessage());
		}
}
	
	
	public function get($rol)
	{//02.01.13
		try {
			$sql = "SELECT * FROM sis_rol"
					. " WHERE rol = " . $this->oBD->escapar($rol);			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\nObtener rol: " . $e->getMessage());
		}
	}
	
	public function insertar($rol)
	{
		try {			
			$this->iniciarTransaccion('dat_roles_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrol) FROM roles");
			++$id;			
			$estados = array('idrol' => $id,'rol'=>$rol);			
			$this->oBD->insert('roles', $estados);			
			$this->terminarTransaccion('dat_roles_insert');			
			return $id;
		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_roles_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Roles").": " . $e->getMessage());
		}
	}

	public function actualizar($id, $rol)
	{
		try {
			$this->iniciarTransaccion('dat_roles_update');
			$estados = array('rol'=>$rol);			
			$this->oBD->update('roles ', $estados, array('idrol' => $id));
		    $this->terminarTransaccion('dat_roles_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Roles").": " . $e->getMessage());
		}
	}
	
	public function existe_privilegio($rol, $recurso, $proceso)
	{//02.01.13
		try {
            $aproceso=array('list','add','update','delete','print','see');
            if(in_array($proceso, $aproceso)){
				$sql = "SELECT COUNT(*) FROM sis_privilegio"
						. " WHERE lower(rol) = " . $this->oBD->escapar(strtolower($rol))
						. " AND lower(recurso) = " . $this->oBD->escapar(strtolower($recurso))
						. " AND lower(a".$proceso.")= " . $this->oBD->escapar(strtolower($proceso));
				$res = $this->oBD->consultarEscalarSQL($sql);			
				return empty($res) ? false : true;
			}else
			 return false;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Privilege exists")." " . $e->getMessage());
		}
	}
	
	/*public function listar_privilegios($rol, $recurso)
	{//02.01.13
		try {
			$sql = "SELECT * FROM sis_privilegio"
					. " WHERE rol = " . $this->oBD->escapar($rol);			
			$sql .= !empty($recurso) ? " AND recurso = " . $this->oBD->escapar($recurso) : '';			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n ".JrTexto::_("Privilege list")." " . $e->getMessage());
		}
	}


	public function insertar_privilegio($rol, $recurso, $proceso)
	{//23.10.15
		try {
			$aproceso=array('list','add','update','delete','print','see');			
			$sql = "INSERT IGNORE INTO sis_privilegio VALUES(" . $this->oBD->escapar($rol) . ", " . $this->oBD->escapar($recurso)
					. ", " . $this->oBD->escapar($proceso) . ")";
			return $this->oBD->ejecutarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\nInsertar privilegio: " . $e->getMessage());
		}
	}
	
	public function limpiar_privilegios($rol)
	{//23.10.15
		try {
			return $this->oBD->delete('sis_privilegio', array('rol' => $rol));
		} catch(Exception $e) {
			throw new Exception("ERROR\nInsertar rol: " . $e->getMessage());
		}
	}*/
	
	public function eliminar($rol)
	{//23.10.15
		try {
			$this->oBD->delete('permisos', array('rol' => $rol));
			return $this->oBD->delete('sis_rol', array('rol' => $rol));
		} catch(Exception $e) {
			throw new Exception("ERROR\nInsertar rol: " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('sis_rol', array($propiedad => $valor), array('rol' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Role").": " . $e->getMessage());
		}
	}
}