<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-05-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatAulavirtualinvitados extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Aulavirtualinvitados").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM aulavirtualinvitados";
			
			$cond = array();		
			
			if(!empty($filtros["idinvitado"])) {
					$cond[] = "idinvitado = " . $this->oBD->escapar($filtros["idinvitado"]);
			}
			if(!empty($filtros["idaula"])) {
					$cond[] = "idaula = " . $this->oBD->escapar($filtros["idaula"]);
			}
			if(!empty($filtros["dni"])) {
					$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(!empty($filtros["email"])) {
					$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(!empty($filtros["asistio"])) {
					$cond[] = "asistio = " . $this->oBD->escapar($filtros["asistio"]);
			}
			if(!empty($filtros["como"])) {
					$cond[] = "como = " . $this->oBD->escapar($filtros["como"]);
			}
			if(!empty($filtros["usuario"])) {
					$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Aulavirtualinvitados").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM aulavirtualinvitados";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idinvitado"])) {
					$cond[] = "idinvitado = " . $this->oBD->escapar($filtros["idinvitado"]);
			}
			if(!empty($filtros["idaula"])) {
					$cond[] = "idaula = " . $this->oBD->escapar($filtros["idaula"]);
			}
			if(!empty($filtros["dni"])) {
					$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(!empty($filtros["email"])) {
					$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(!empty($filtros["asistio"])) {
					$cond[] = "asistio = " . $this->oBD->escapar($filtros["asistio"]);
			}
			if(!empty($filtros["como"])) {
					$cond[] = "como = " . $this->oBD->escapar($filtros["como"]);
			}
			if(!empty($filtros["usuario"])) {
					$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Aulavirtualinvitados").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM aulavirtualinvitados  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Aulavirtualinvitados").": " . $e->getMessage());
		}
	}
	
	public function insertar($idaula,$dni,$email,$asistio,$como,$usuario)
	{
		try {
			
			$this->iniciarTransaccion('dat_aulavirtualinvitados_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idinvitado) FROM aulavirtualinvitados");
			++$id;
			
			$estados = array('idinvitado' => $id							
							,'idaula'=>$idaula
							,'dni'=>$dni
							,'email'=>$email
							,'asistio'=>$asistio
							,'como'=>$como
							,'usuario'=>$usuario							
							);
			
			$this->oBD->insert('aulavirtualinvitados', $estados);			
			$this->terminarTransaccion('dat_aulavirtualinvitados_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_aulavirtualinvitados_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Aulavirtualinvitados").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idaula,$dni,$email,$asistio,$como,$usuario)
	{
		try {
			$this->iniciarTransaccion('dat_aulavirtualinvitados_update');
			$estados = array('idaula'=>$idaula
							,'dni'=>$dni
							,'email'=>$email
							,'asistio'=>$asistio
							,'como'=>$como
							,'usuario'=>$usuario								
							);
			
			$this->oBD->update('aulavirtualinvitados ', $estados, array('idinvitado' => $id));
		    $this->terminarTransaccion('dat_aulavirtualinvitados_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Aulavirtualinvitados").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM aulavirtualinvitados  "
					. " WHERE idinvitado = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Aulavirtualinvitados").": " . $e->getMessage());
		}
	}

	public function eliminarxfiltro($filtros){
		try {
			$cond=array();
			if(!empty($filtros["idinvitado"])) {
					$cond["idinvitado"] =  $filtros["idinvitado"];
			}
			if(!empty($filtros["idaula"])) {
					$cond["idaula"]=$filtros["idaula"];
			}
			if(!empty($filtros["dni"])) {
					$cond["dni"]=$filtros["dni"];
			}
			if(!empty($filtros["email"])) {
					$cond["email"]=$filtros["email"];
			}
			
			if(empty($cond)) return false;
			return $this->oBD->delete('aulavirtualinvitados', $cond);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Aulavirtualinvitados").": " . $e->getMessage());
		}

	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('aulavirtualinvitados', array('idinvitado' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Aulavirtualinvitados").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('aulavirtualinvitados', array($propiedad => $valor), array('idinvitado' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Aulavirtualinvitados").": " . $e->getMessage());
		}
	}
   
		
}