<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		23-12-2016
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPermisos', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegPermisos 
{
	protected $idpermiso;
	protected $rol;
	protected $menu;
	protected $_list;
	protected $_add;
	protected $_edit;
	protected $_delete;
	
	protected $dataPermisos;
	protected $oDatPermisos;	

	public function __construct()
	{
		$this->oDatPermisos = new DatPermisos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPermisos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatPermisos->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPermisos->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPermisos->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPermisos->get($this->idpermiso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('permisos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatPermisos->iniciarTransaccion('neg_i_Permisos');
			$this->idpermiso = $this->oDatPermisos->insertar($this->rol,$this->menu,$this->_list,$this->_add,$this->_edit,$this->_delete);
			//$this->oDatPermisos->terminarTransaccion('neg_i_Permisos');	
			return $this->idpermiso;
		} catch(Exception $e) {	
		   //$this->oDatPermisos->cancelarTransaccion('neg_i_Permisos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('permisos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatPermisos->actualizar($this->idpermiso,$this->rol,$this->menu,$this->_list,$this->_add,$this->_edit,$this->_delete);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Permisos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatPermisos->eliminar($this->idpermiso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpermiso($pk){
		try {
			$this->dataPermisos = $this->oDatPermisos->get($pk);
			if(empty($this->dataPermisos)) {
				throw new Exception(JrTexto::_("Permisos").' '.JrTexto::_("not registered"));
			}
			$this->idpermiso = $this->dataPermisos["idpermiso"];
			$this->rol = $this->dataPermisos["rol"];
			$this->menu = $this->dataPermisos["menu"];
			$this->_list = $this->dataPermisos["_list"];
			$this->_add = $this->dataPermisos["_add"];
			$this->_edit = $this->dataPermisos["_edit"];
			$this->_delete = $this->dataPermisos["_delete"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($rol,$menu,$campo,$flag){
		try {
			if(!NegSesion::tiene_acceso('permisos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$filtros=Array("rol"=>$rol,"menu"=>$menu);
			$permisos = $this->oDatPermisos->buscar($filtros);
			if(!empty($permisos[0])){
				$idpermiso=$permisos[0]["idpermiso"];
				return $this->oDatPermisos->set($idpermiso, $campo, $flag);				
			}else{
				$_list=$campo=='_list'?$flag:0;
				$_add=$campo=='_add'?$flag:0;
				$_edit=$campo=='_edit'?$flag:0;
				$_delete=$campo=='_delete'?$flag:0;
			  return $this->oDatPermisos->insertar($rol,$menu,$_list,$_add,$_edit,$_delete);
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}		
}