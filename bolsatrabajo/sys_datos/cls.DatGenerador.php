<?php
/**
 * @autor		Abel Chingo Tello ACT
 * @fecha		03-09-2015
 * @copyright	Copyright (C) 2015. Sitec Perú.
 */

class DatGenerador extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function getTablas(){
		try {
			$basedatos=$this->oBD->getbasedatos();
			$sql = "SHOW TABLES FROM `" . $basedatos."`";	
			//var_dump($sql)		;
			$res = $this->oBD->allsql($sql);	
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\nObtener Tablas de base de datos: " . $e->getMessage());
		}
	}

	public function getCampos($tabla){
		try {
			$basedatos=$this->oBD->getbasedatos();
			//COLUMN_NAME=Nombre de columna,
			//DATA_TYPE= tipo de dato, y tamaño de dato
			//CHARACTER_MAXIMUM_LENGTH=Maximo de caracteres,
			//COLUMN_KEY= si es PRI columna primaria,
			//COLUMN_TYPE=solo Tipo de dato ,
			//EXTRA= un valor adicional,
			//NUMERIC_PRECISION= parte entera de un numero,
			//NUMERIC_SCALEParte de decimal de un numero,
			//COLUMN_COMMENT= cometario del campo,
			//COLUMN_DEFAULT=valor por defecto,
			//IS_NULLABLE si acepta camponullo o no ...
			$sql = "SELECT COLUMN_NAME,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,COLUMN_KEY,COLUMN_TYPE,EXTRA,NUMERIC_PRECISION,NUMERIC_SCALE,COLUMN_COMMENT,COLUMN_DEFAULT,IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '".$tabla."' AND table_schema = '".$basedatos."'";			
			
			$res = $this->oBD->consultarSQL($sql);
			//var_dump($res)		;
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\nObtener Campos de Table: ".$tabla.'   <br>'. $e->getMessage());
		}
	}
}