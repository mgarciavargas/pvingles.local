<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->datos)) $frm=$this->datos;
$file='<img src="'.$this->documento->getUrlStatic().'/media/nofoto.jpg" class="img-responsive center-block">';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Bolsa_publicaciones"); ?></li>
  </ol>
</nav>
<?php } ?><div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col">
    <div id="msj-interno"></div>
    <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
    <input type="hidden" name="Idpublicacion" id="idpublicacion<?php echo $idgui; ?>" value="<?php echo $this->pk;?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo JrTexto::_($this->frmaccion);?>">
    <div class="row">
          <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Idempresa');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="idempresa<?php echo $idgui; ?>" name="idempresa" required="required" class="form-control" value="<?php echo @$frm["idempresa"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Titulo');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="titulo<?php echo $idgui; ?>" name="titulo" required="required" class="form-control" value="<?php echo @$frm["titulo"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Descripcion');?> <span class="required"> (*) </span></label>              
               <textarea id="descripcion<?php echo $idgui; ?>" name="descripcion" class="form-control" ><?php echo @trim($frm["descripcion"]); ?></textarea>
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Sueldo');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="sueldo<?php echo $idgui; ?>" name="sueldo" required="required" class="form-control" value="<?php echo @$frm["sueldo"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Nvacantes');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="nvacantes<?php echo $idgui; ?>" name="nvacantes" required="required" class="form-control" value="<?php echo @$frm["nvacantes"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Disponibilidadeviaje');?> <span class="required"> (*) </span></label>              
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["disponibilidadeviaje"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["disponibilidadeviaje"];?>"  data-valueno="0" data-value2="<?php echo @$frm["disponibilidadeviaje"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["disponibilidadeviaje"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" id="disponibilidadeviaje<?php echo $idgui; ?>" name="disponibilidadeviaje" value="<?php echo !empty($frm["disponibilidadeviaje"])?$frm["disponibilidadeviaje"]:0;?>" > 
                 </a>
                                              </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Duracioncontrato');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="duracioncontrato<?php echo $idgui; ?>" name="duracioncontrato" required="required" class="form-control" value="<?php echo @$frm["duracioncontrato"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Xtiempo');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="xtiempo<?php echo $idgui; ?>" name="xtiempo" required="required" class="form-control" value="<?php echo @$frm["xtiempo"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Fecharegistro');?> <span class="required"> (*) </span></label>              
                <input name="fecharegistro<?php echo $idgui; ?>" class="verdate form-control" required="required" type="date" value="<?php echo @$frm["fecharegistro"];?>">   
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Fechapublicacion');?> <span class="required"> (*) </span></label>              
                <input name="fechapublicacion<?php echo $idgui; ?>" class="verdate form-control" required="required" type="date" value="<?php echo @$frm["fechapublicacion"];?>">   
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Cambioderesidencia');?> <span class="required"> (*) </span></label>              
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["cambioderesidencia"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["cambioderesidencia"];?>"  data-valueno="0" data-value2="<?php echo @$frm["cambioderesidencia"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["cambioderesidencia"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" id="cambioderesidencia<?php echo $idgui; ?>" name="cambioderesidencia" value="<?php echo !empty($frm["cambioderesidencia"])?$frm["cambioderesidencia"]:0;?>" > 
                 </a>
                                              </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Mostrar');?> <span class="required"> (*) </span></label>              
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["mostrar"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["mostrar"];?>"  data-valueno="0" data-value2="<?php echo @$frm["mostrar"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["mostrar"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" id="mostrar<?php echo $idgui; ?>" name="mostrar" value="<?php echo !empty($frm["mostrar"])?$frm["mostrar"]:0;?>" > 
                 </a>
                                              </div>
            
    </div>
        <div class="row"> 
            <div class="col-12 form-group text-center">
              <hr>
              <button id="btn-saveBolsa_publicaciones" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Save'));?> </button>
              <button type="button" class="btn btn-warning btn-close" data-dismiss="modal" href="<?php echo JrAplicacion::getJrUrl(array('bolsa_publicaciones'))?>"> <i class=" fa fa-repeat"></i> <?php echo ucfirst(JrTexto::_('Cancel'));?></button>              
            </div>
        </div>
        </form>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
  
  $('#fecharegistro<?php echo $idgui; ?>').datetimepicker({ /*lang:'es', timepicker:false,*/ format:'YYYY/MM/DD'}); 
  
  $('#fechapublicacion<?php echo $idgui; ?>').datetimepicker({ /*lang:'es', timepicker:false,*/ format:'YYYY/MM/DD'}); 
            
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(ev){
      ev.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'bolsa_publicaciones', 'saveBolsa_publicaciones', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
       <?php if(!empty($fcall)){ ?> if(typeof <?php echo $fcall?> == 'function'){
          <?php echo $fcall?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else <?php } ?>  return redir('<?php echo JrAplicacion::getJrUrl(array("Bolsa_publicaciones"))?>');
      }
     }
  }); 
  
});

$('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }
  });
</script>