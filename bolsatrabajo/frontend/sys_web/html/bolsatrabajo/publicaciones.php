<?php
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$publicacion=!empty($this->publicaciones)?$this->publicaciones:'';
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$isloginuser=!empty($this->isLoginuser)?$this->isLoginuser:false;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
	.titulo-aviso{
		    color: #007bff;
	}
	.pnlitem<?php echo $idgui; ?>{
		margin: 0.5em 0px; width: 100%;
	}
</style>
<div class="container">
	<?php if(!$ismodal){?><nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-primary">
	    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>/../"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
	    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
	    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Publicaciones"); ?></li>
	  </ol>
	</nav>
	<?php } ?>
	<div class="row">	
<?php 
$i=0;
if(!empty($publicacion))
foreach ($publicacion as $pub){ $i++; ?>
<div class="card pnlitem<?php echo $idgui; ?> " >
	<div class="card-body hvr-border-fade hvr-curl-top-right sombra1" data-idpublicacion="<?php echo $pub["idpublicacion"] ?>" data-idempresa="<?php echo $pub["idempresa"] ?>" style="width: 100%;">
		<div class="row">
			<div class="col-12 col-sm-8 col-md-9">
				<h5 class="card-title titulo-aviso"><?php echo $pub["titulo"] ?> 
					<?php if($i<5){?><span class="badge badge-warning " style="font-size: 14px;"><?php echo JrTexto::_('Nuevo') ?></span><?php }  $i++;?></h5>
				<p class="card-text"><?php echo substr(strip_tags($pub["descripcion"]),0,500)."..." ?></p>
			</div>
			<div class="col-12 col-sm-4 col-md-3 text-center">
				<div class="tiempocontrol" data-id="<?php echo $i; ?>" data-tactual="<?php echo date('Y-m-d H:i:s');?>" data-pub="<?php echo $pub["fechapublicacion"]; ?>"><?php echo $pub["fechapublicacion"]; ?></div>
				<div class="text-center">
					<?php 
					$ruta=$this->documento->getUrlStatic()."/media/nofoto.jpg";
					if(!empty($pub["logo"])) $ruta=$this->documento->getUrlBase().$pub["logo"];?>
					<img src="<?php echo $ruta; ?>" class="img-responsive img-tumhnails center-block" style="max-width: 120px; max-height: 100px;">					
				</div>
			</div>
			<div class="col-12"><hr></div>
			<div class="col-12 col-md-3 text-center">
				<a class="btn btn-sm btn-primary text-white hvr-grow" href="<?php echo $this->documento->getUrlBase(); ?>/defecto/verpublicacion/?idpublicacion=<?php echo $pub["idpublicacion"]; ?>">
				<i class="fa fa-eye"> Ver detalle</i></a>
				<?php if($isloginuser==true){ ?> 
				<a class="btn btn-sm btn-success btn-postularme text-white hvr-grow" href="<?php echo $this->documento->getUrlBase(); ?>/defecto/postularme/"><i class="fa fa-thumbs-up"> Postularme</i></a>
				<?php } ?>
			</div>
			<div class="col-12 col-md-<?php echo $this->login?6:9;?> text-center">				
			<b><?php echo ucfirst($pub["nombre"])." : ".$pub["nvacantes"] ?> Vacantes</b>
			</div>
			<?php 
			if($this->login && $pub["idempresa"]==@$this->empresa["idempresa"]) {?>
			<div class="col-3 text-right">
				<a  href="<?php echo $this->documento->getUrlBase(); ?>/defecto/publicar/?idpublicacion=<?php echo $pub["idpublicacion"]; ?>">
				<i class="fa fa-pencil text-primary"> Editar</i></a>
			</div>
			<?php } ?>
		</div>		
	</div>
</div>	
<?php }else{ ?>
<div class="col">
<div class="row" id="publicacion-list" >
	<div class="col text-center" style="padding: 2em;"><h4>Lo sentimos muchos no hemos encontrado publicaciones</h4></div>
</div>
</div>
<?php } ?>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.tiempocontrol').achttiempopasado();
		$('.btn-postularme').on('click',function(ev){
			ev.preventDefault();
			ev.preventDefault();
			var data={
				idpublicacion:$(this).closest('.card-body').attr('data-idpublicacion'),	
			}
			var info={url:$(this).attr('href'), ismodal:true, titulo:$(this).attr('data-title'),frmdata:data}      		
			_sysopenmodal_(info);
			return false;
		});
	});
</script>