<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->empresa)) $frm=$this->empresa;
$pub=!empty($this->publicaciones)?$this->publicaciones:'';
$ruta=$this->documento->getUrlStatic()."/media/nofoto.jpg";
if(!empty($frm["logo"])) $ruta=$this->documento->getUrlBase().$frm["logo"];
$file='<img src="'.$ruta.'" class="img-responsive center-block" style="max-width: 200px; max-height: 150px;">';
?>
<div class="container">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
	<?php if(!$ismodal){?><nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-primary">
	    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>/../"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
	    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
	    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Publicar"); ?></li>
	  </ol>
	</nav>
	<?php } ?>
	<div class="row" id="vent-<?php echo $idgui;?>">
		<div class="col-12">
			<div id="msj-interno"></div>
	    	<form method="post" id="frmpublicar-<?php echo $idgui;?>" class="form-horizontal form-label-left" >
	    	<input type="hidden" name="idempresa" id="idempresa<?php echo $idgui; ?>" value="<?php echo @$frm["idempresa"];?>">
	    	<input type="hidden" name="idpublicacion" id="idpublicacion<?php echo $idgui; ?>" value="<?php echo @$pub["idpublicacion"];?>">	    
	    	<div class="row">
	    		<div class="col-12 text-center"><br>
	    			<h3><?php echo ucfirst(@$frm["nombre"]);?></h3>
	    			<hr> 			
	    		</div>
	    		<div class="col-12 col-sm-12 col-md-6 form-group">
	    			<label>Razon social </label> <?php echo @$frm["rason_social"];?><br>
	    			<label>Direccion </label><?php echo @$frm["direccion"];?><br>	    			
	    		</div>
	    		<div class="col-12 col-sm-12 col-md-6 form-group">
	    			<label>Correo </label> <?php echo @$frm["correo"];?><br>
	    			<label>Direccion </label><?php echo @$frm["direccion"];?><br>	    			
	    		</div>
	    		<div class="col-12 col-sm-12 col-md-12 text-center">
	    			<hr><br>
	    			<h3><?php echo JrTexto::_("Publicar") ?></h3>
	    		</div>
	    		<div class="col-12 form-group">
              		<label><?php echo JrTexto::_('Titulo');?> <span class="required"> </span></label>              
                	<input type="text"  id="titulo<?php echo $idgui; ?>" name="titulo" class="form-control" placeholder="<?php echo JrTexto::_('Titulo de la publicación o tipode profesional') ?>" value="<?php echo @$pub["titulo"]; ?>">
            	</div>
            	<div class="col-12 form-group">
              		<label><?php echo JrTexto::_('Descripción');?> <span class="required"> </span></label>              
                	<textarea id="descripcion<?php echo $idgui; ?>" name="descripcion" class="form-control"  rows="6" placeholder="<?php echo JrTexto::_('Describa al tipo de profesional que necesita') ?>"><?php echo @$pub["descripcion"]; ?></textarea>
            	</div>
            	<div class="col-12 col-sm-6 col-md-3 form-group">
            		<label><?php echo JrTexto::_("Salario") ?></label>
            		<div class="input-group">
            			<div class="input-group-prepend"><div class="input-group-text">S/. </div></div>
            			<input type="text" class="form-control" id="sueldo<?php echo $idgui; ?>" name="sueldo" placeholder="0.00" value="<?php echo @$pub["sueldo"]; ?>">
            		</div>
            	</div>
            	<div class="col-12 col-sm-6 col-md-3 form-group">
            		<label><?php echo JrTexto::_("N° Vacantes") ?></label>
            		<input type="number" class="form-control" id="nvacantes<?php echo $idgui; ?>" name="nvacantes" placeholder="" value="<?php echo !empty($pub["nvacantes"])?$pub["nvacantes"]:1; ?>">
            	</div>
            	<div class="col-12 col-sm-6 col-md-3 form-group">
            		<label><?php echo JrTexto::_("Tiempo contrato") ?></label>
            		<div class="cajaselect">
	                <select name="duracioncontrato" id="duracioncontrato<?php echo $idgui; ?>" class="form-control">	                  
	                  <option value="de 1 a 3 Meses" <?php echo @$pub["duracioncontrato"]=='de 1 a 3 Meses'?'selected="selected"':''; ?> >de 1 a 3 Meses</option>
	                  <option value="de 3 a 6 Meses" <?php echo @$pub["duracioncontrato"]=='de 3 a 6 Meses'?'selected="selected"':''; ?>>de 3 a 6 Meses</option> 
	                  <option value="de 6 Meses a 1 año" <?php echo @$pub["duracioncontrato"]=='de 6 Meses a 1 año'?'selected="selected"':''; ?>>de 6 Meses a 1 año</option>
	                  <option value="Mas de un año" <?php echo @$pub["duracioncontrato"]=='Mas de un año'?'selected="selected"':''; ?>>Mas de un año</option>
	                  <option value="Indeterminado" <?php echo @$pub["duracioncontrato"]=='Indeterminado'?'selected="selected"':''; ?>>Indeterminado</option>
	                </select>
	              </div>
            	</div>
            	<div class="col-12 col-sm-6 col-md-3 form-group">
            		<label><?php echo JrTexto::_("Tipo contrato") ?></label>
            		<div class="cajaselect">
	                <select name="xtiempo" id="xtiempo<?php echo $idgui; ?>" class="form-control">	                 
	                  <option value="Por Horas" <?php echo @$pub["xtiempo"]=='Por Horas'?'selected="selected"':''; ?> >Por Horas</option>
	                  <option value="Medio Tiempo" <?php echo @$pub["xtiempo"]=='Medio Tiempo'?'selected="selected"':''; ?> >Medio Tiempo</option> 
	                  <option value="Tiempo completo" <?php echo @$pub["xtiempo"]=='Tiempo completo'||empty($pub["xtiempo"])?'selected="selected"':''; ?> >Tiempo completo</option>	                  
	                </select>
	              </div>
            	</div>
            	<div class="col-12 col-sm-6 col-md-4 form-group">
            		<label><?php echo JrTexto::_("Disponibilidad para viajar") ?></label>
            		<div class="custom-control custom-checkbox">
					  <input type="checkbox" name="disponibilidadeviaje" id="disponibilidadeviaje<?php echo $idgui; ?>" class="custom-control-input" <?php echo @$pub["disponibilidadeviaje"]=='1'?'checked="true"':''; ?> >
					  <label class="chk<?php echo $idgui;?> custom-control-label" for="disponibilidadeviaje<?php echo $idgui; ?>"><?php echo @$pub["disponibilidadeviaje"]=='1'?'Si':'No'; ?> </label>
					</div>            		
            	</div>
            	<div class="col-12 col-sm-6 col-md-4 form-group">
            		<label><?php echo JrTexto::_("Disponibilidad para cambiar de residencia") ?></label>
            		<div class="custom-control custom-checkbox">
					  <input type="checkbox" name="cambioderesidencia" id="cambioderesidencia<?php echo $idgui; ?>" class="custom-control-input" <?php echo @$pub["cambioderesidencia"]=='1'?'checked="true"':''; ?> >
					  <label class="chk<?php echo $idgui;?> custom-control-label" for="cambioderesidencia<?php echo $idgui; ?>"><?php echo @$pub["cambioderesidencia"]=='1'?'Si':'No'; ?> </label>
					</div>
            	</div>
            	<div class="col-12 col-sm-6 col-md-4 form-group">
            		<label><?php echo JrTexto::_("Publicar Ahora") ?></label>
            		<div class="custom-control custom-checkbox">
					  <input type="checkbox"  name="mostrar" id="mostrar<?php echo $idgui; ?>" class="custom-control-input" <?php echo @$pub["mostrar"]=='1'?'checked="true"':''; ?>>
					  <label class="chk<?php echo $idgui;?> custom-control-label" for="mostrar<?php echo $idgui; ?>"><?php echo @$pub["mostrar"]=='1'?'Si':'No'; ?> </label>
					</div>
            	</div>
            	<div class="col-12 text-center">
            		<br>
            		<hr>
            		<button type="button" class="btn btn-warning btncancelar"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_("Cancelar") ?></button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo JrTexto::_("Guardar") ?></button>
					<br><br>
            	</div>		   
		    </div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){

		tinymce.init({
		  relative_urls : false,
		  convert_newlines_to_brs : true,
		  menubar: false,
		  statusbar: false,
		  verify_html : false,
		  //content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
		  selector: '#descripcion<?php echo $idgui; ?>',
		  height: 400,
		  plugins:["textcolor" ], 
		  toolbar: ' undo redo | styleselect |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor '
		});

		$('.chk<?php echo $idgui;?>').click(function(){
			var lbl=$(this);
			var chk=lbl.siblings('input');
			if(!chk.is(':checked')){
				lbl.text('Si');
			}else{
				lbl.text('No');
			}
		})

		$('#frmpublicar-<?php echo $idgui;?>').bind({    
     		submit: function(ev){
     			ev.preventDefault();
     			tinyMCE.triggerSave();
     			var tmpfrm=document.getElementById('frmpublicar-<?php echo $idgui;?>');	
				var formData = new FormData(tmpfrm);			
				var _sysajax={
					fromdata:formData,
					showmsjok:true,
					url:_sysUrlBase_+'/bolsa_publicaciones/guardarPublicaciones',					
					callback:function(rs){
						var idpublicacion=rs.newid;
						$('#idpublicacion<?php echo $idgui; ?>').val(idpublicacion);				
						setTimeout(function(){ redir(_sysUrlBase_+'/defecto/mispublicaciones');},500);
					}
				}
				sysajax(_sysajax);
				return false;
     		}
		});

		$('.btncancelar').click(function(){
			redir(_sysUrlBase_);
		})

		<?php if(empty($this->login)){?>
			veravisologin(false);
		<?php } ?>
	});
</script>