<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
class WebSesion extends JrWeb
{
	private $oNegSesion;
	protected $oNegConfig;
	protected $NegEmpresas;
	//public $oNegHistorialSesion;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegSesion = new NegSesion;
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->NegEmpresas = new NegBolsa_empresas;
	}
	public function defecto()
	{
		return $this->login();
	}
	public function login()
	{
		try {
			global $aplicacion;	
			if(true === NegSesion::existeSesion()){
				return $aplicacion->redir();
			}			
			if(!empty($_GET['t'])) {
				if(true === $this->oNegSesion->ingresarxToken($_GET['t'])) {
					return $aplicacion->redir(JrAplicacion::getJrUrl(array('perfil', 'cambiar-clave')), false);
				}
			}	
			$this->idioma=NegSesion::get('idioma','m3c_gen__');	
			@extract($_POST, EXTR_OVERWRITE);
			if(!empty($usuario) && !empty($clave)) {
				if(true === $this->oNegSesion->ingresar(@$usuario, @$clave)) {
					//$this->iniciarHistorialSesion('P');
					return $aplicacion->redir();
				}				
				$this->msjErrorLogin = true;
			}			
			return $this->form(@$usuario);
		} catch(Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			return $this->form(@$usuario);
		}
	}
	public function loginempresa(){
		try {
			$this->documento->plantilla = 'blanco';
			global $aplicacion;
			@extract($_POST, EXTR_OVERWRITE);
			if(empty($usuario)||empty($clave)){
				echo json_encode(array('code'=>'Error','msj'=>ucfirst(JrTexto::_('Datos incompletos'))));
				exit(0);
			}
			
			$islogin=$this->NegEmpresas->getxCredencial($usuario,$clave);
			if($islogin) echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Inicio Sesion correctamente')))); 
			else echo json_encode(array('code'=>'Error','msj'=>ucfirst(JrTexto::_('Usuario Y/o contraseña no existen'))));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}
	public function salirempresa(){
		try {
			@session_start();
			if(!empty($_SESSION["loginempresa"])){
				unset($_SESSION["loginempresa"]);
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Sesion terminada')))); 			
			}else{
				echo json_encode(array('code'=>'Error','msj'=>ucfirst(JrTexto::_('Sesion no existen'))));
			}
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}

	public function formloginempresa(){
		try {
			$this->documento->plantilla='modal';
			$this->esquema = 'login-modal';
			return parent::getEsquema();
			} catch(Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			$aplicacion->redir();
		}

	}

	protected function form($usuario = null)
	{
		try {
			global $aplicacion;			
			if(true == NegSesion::existeSesion()) {
				$aplicacion->redir();
			}			
			$this->usuario = $usuario;				
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'login';
			$tpl=$this->documento->plantilla!='login'?'login-modal':'login';
			$this->esquema = $tpl;
			return parent::getEsquema();
		} catch(Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			$aplicacion->redir();
		}
	}	
	public function salir()
	{
		try {
			global $aplicacion;
			if(true === NegSesion::existeSesion()) {
				$this->terminarHistorialSesion('P');
				$this->oNegSesion->salir();
			}			
			$aplicacion->redir();
		} catch(Exception $e) {
			$aplicacion->redir();
		}
	}	
	public function cambiar_ambito()
	{
		try {
			global $aplicacion;			
			if(!empty($_GET['ambito'])) {
				$oNegSesion = new NegSesion;
				$oNegSesion->cambiar_ambito($_GET['ambito']);
			}			
			$aplicacion->redir();
		} catch(Exception $e) {
			$aplicacion->redir();
		}
	}
	protected function iniciarHistorialSesion($lugar)
	{
		$usuarioAct = NegSesion::getUsuario();
		/*$this->oNegHistorialSesion->tipousuario = ($usuarioAct['rol']=='Alumno')?'A':'P';
		$this->oNegHistorialSesion->idusuario = $usuarioAct['dni'];
		$this->oNegHistorialSesion->lugar = $lugar;
		$this->oNegHistorialSesion->fechaentrada = date('Y-m-d H:i:s');
		$this->oNegHistorialSesion->fechasalida = null;
		$idHistSesion = $this->oNegHistorialSesion->agregar();
		$sesion = JrSession::getInstancia();
		$sesion->set('idHistorialSesion', $idHistSesion, '__admin_m3c');*/
	}
	protected function terminarHistorialSesion($lugar)
	{
		$usuarioAct = NegSesion::getUsuario();
		/*$this->oNegHistorialSesion->idhistorialsesion = $usuarioAct['idHistorialSesion'];
		$this->oNegHistorialSesion->fechasalida = date('Y-m-d H:i:s');
		$resp = $this->oNegHistorialSesion->editar();*/
	}
	// ========================== Funciones xajax ========================== //
	public function xSolicitarCambioClave(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0]['usuario'])) { return;}
				JrCargador::clase('sys_negocio::NegUsuario', RUTA_BASE, 'sys_negocio');
				$oNegUsuario = new NegUsuario;			
				$this->admin = $oNegUsuario->procesarSolicitudCambioClave($args[0]['usuario']);
				if(!empty($this->admin)) {
					try {
						$isonline=NegTools::isonline();
						if(!empty($isonline)){						
							global $configSitio;
							JrCargador::clase('jrAdwen::JrCorreo');
							$oCorreo = new JrCorreo;
							$oCorreo->setRemitente('desarrollo@sistecapps.com', JrTexto::_('System management'));
							$oCorreo->setAsunto(JrTexto::_('Change password'));						
							$this->esquema = 'syscorreo-recuperar-clave';
							$oCorreo->setMensaje(parent::getEsquema());
							$oCorreo->addDestinarioPhpmailer($this->admin['email'], $this->admin['nombre']);						
							$oCorreo->sendPhpmailer();
							$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention'))
								, $this->pasarHtml(JrTexto::_('If the email is correct you will receive a message with a link to change your password'))
								, 'success');
						}else{
							$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention'))
								, $this->pasarHtml(JrTexto::_('This option is only active in online mode security'))
								, 'info');
						}
					} catch(Exception $e) {}
				}				
				
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'error');
			} 
		}
	}

	public function xCambiarRol(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try{
				global $aplicacion;
				$usu = NegSesion::getUsuario();
				if($usu["rol"]==$usu["rol_original"]){
					$x=NegSesion::set('rol','Alumno');
				}else{
					$y=NegSesion::set('rol',$usu["rol_original"]);
				}
				$oRespAjax->call('redir', $this->documento->getUrlBase());
			}catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'error');
			} 
		}
	}
}