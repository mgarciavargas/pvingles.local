<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/07/2012
 * @copyright	Copyright (C) 2012. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class WebExcepcion extends JrWeb
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function defecto()
	{
		global $aplicacion;
		//$aplicacion->redir();
	}
	
	public function error($msj, $plantilla = null)
	{
		try {
			global $aplicacion;
			
			$this->msj = $msj;
			
			$this->documento->setTitulo(JrTexto::_('Error').' ', true);
			$this->esquema = 'error/general';
			
			$plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'excepcion';
			$this->documento->plantilla = $plantilla;//!empty($plantilla) ? $plantilla : 'excepcion';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			exit(JrTexto::_($e->getMessage()));
		}
	}
	
	public function noencontrado()
	{
		try {
			global $aplicacion;
			
			$this->documento->setTitulo(JrTexto::_('Page not found'), true);
			$this->esquema = 'error/404';
			
			$plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'excepcion';
			$this->documento->plantilla = $plantilla;//!empty($plantilla) ? $plantilla : 'excepcion';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			exit(JrTexto::_($e->getMessage()));
		}
	}
}