<?php
/**
 * @autor       Abel Chingo Tello
 * @fecha       08/09/2016
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */

date_default_timezone_set('America/Lima');
error_reporting(E_ALL);
/*error_reporting(0);*/
define('_BOOT_', true);
define('_http_',!empty($_SERVER['HTTPS'])?'https://':'http://');
define('RUTA_WEB',_http_."abacoeducacion.org");
define('SD', DIRECTORY_SEPARATOR);
define('_mitema_','tema1');
define('_sitio_','frontend');
define('RUTA_BASE',			dirname(dirname(__FILE__)) . SD);
$ruta_ini=dirname(dirname(dirname(__FILE__)));
define('RUTA_LIBS', 		$ruta_ini .SD. 'sys_lib' . SD);
define('RUTA_INC',			RUTA_BASE . 'sys_inc' . SD);
define('RUTA_PLANTILLAS', 	RUTA_BASE . _sitio_. SD . 'plantillas' . SD);
define('RUTA_SITIO',		RUTA_BASE . _sitio_. SD);
define('IS_LOGIN', false);
$host= !empty($_SERVER["HTTP_HOST"])?$_SERVER["HTTP_HOST"]:'abacoeducacion.org';
define('URL_BASE',_http_.$host.'/pvingles.local/bolsatrabajo');
//define('URL_SMARTQUIZ',URL_BASE.'/smartquiz/');
define('IDPROYECTO','bolsatrabajo');
define('_version_','1.3');
require_once(RUTA_LIBS . 'cls.JrCargador.php');
require_once(RUTA_LIBS . 'jrAdwen' . SD . 'jrFram.php');
require_once(RUTA_LIBS . 'jrAdwen'.SD.'documento'.SD.'cls.JrDocumento.'.'php');
try {
	JrCargador::clase('sys_inc::ConfigSitio', RUTA_BASE, 'sys_inc::');
	JrCargador::clase('sys_inc::Sitio', RUTA_BASE, 'sys_inc::');
	$aplicacion = Sitio::getInstancia();
} catch(Exception $e){
	exit('<h1>Imposible iniciar la aplicacion: '.$e->getMessage().'</h1>');
}
