-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 16-01-2018 a las 20:19:21
-- Versión del servidor: 5.6.35
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `abacoedu_devsmartlearn`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_curso`
--
drop table acad_curso;
CREATE TABLE IF NOT EXISTS `acad_curso` (
  `idcurso` int(1) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(1) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idusuario` int(11) NOT NULL,
  `vinculosaprendizajes` text COLLATE utf8_spanish_ci,
  `materialesyrecursos` text COLLATE utf8_spanish_ci,
  PRIMARY KEY (`idcurso`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_curso`
--

INSERT INTO `acad_curso` (`idcurso`, `nombre`, `imagen`, `descripcion`, `estado`, `fecharegistro`, `idusuario`, `vinculosaprendizajes`, `materialesyrecursos`) VALUES
(1, 'A1', '/static/media/image/N1-U1-A1-20180115122647.png', '', 1, '2018-01-16 21:54:44', 43831104, NULL, NULL),
(2, 'A2', '/static/media/image/N1-U1-A1-20180115045307.png', '', 1, '2018-01-16 22:00:05', 43831104, NULL, NULL),
(3, 'B1', '/static/media/image/N1-U1-A1-20180115045529.png', '', 1, '2018-01-16 22:00:37', 43831104, NULL, NULL),
(4, 'B2', '/static/media/image/N1-U1-A1-20180115045718.png', '', 1, '2018-01-16 22:01:08', 43831104, NULL, NULL),
(5, 'C1', '/static/media/image/N1-U1-A1-20180115045958.png', '', 1, '2018-01-17 00:50:56', 43831104, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_cursoaprendizaje`
--
drop table acad_cursoaprendizaje;
CREATE TABLE IF NOT EXISTS `acad_cursoaprendizaje` (
  `idaprendizaje` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `idcursodetalle` int(11) NOT NULL,
  `idpadre` int(11) NOT NULL,
  `idrecurso` int(11) DEFAULT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`idaprendizaje`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_cursodetalle`
--
drop table acad_cursodetalle;
CREATE TABLE IF NOT EXISTS `acad_cursodetalle` (
  `idcursodetalle` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `orden` bigint(20) NOT NULL,
  `idrecurso` int(11) NOT NULL,
  `tiporecurso` varchar(2) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Examen, nivel , rubrica,etc',
  `idlogro` int(11) NOT NULL,
  `url` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `idpadre` smallint(6) NOT NULL,
  PRIMARY KEY (`idcursodetalle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_cursodetalle`
--

INSERT INTO `acad_cursodetalle` (`idcursodetalle`, `idcurso`, `orden`, `idrecurso`, `tiporecurso`, `idlogro`, `url`, `idpadre`) VALUES
(39, 1, 0, 49, 'E', 0, '', 16),
(2, 1, 1, 6, 'U', 3, '', 0),
(38, 1, 0, 47, 'E', 0, '', 15),
(37, 1, 0, 45, 'E', 0, '', 14),
(36, 1, 0, 42, 'E', 0, '', 13),
(3, 1, 0, 34, 'E', 2, '', 2),
(4, 1, 1, 18, 'L', 1, '', 2),
(5, 1, 2, 56, 'L', 0, '', 2),
(6, 1, 3, 55, 'L', 0, '', 2),
(7, 1, 4, 61, 'L', 0, '', 2),
(8, 1, 5, 31, 'E', 0, '', 2),
(9, 1, 2, 7, 'U', 0, '', 0),
(10, 1, 3, 8, 'U', 0, '', 0),
(11, 1, 4, 9, 'U', 0, '', 0),
(12, 1, 5, 10, 'U', 0, '', 0),
(13, 1, 6, 11, 'U', 0, '', 0),
(14, 1, 7, 12, 'U', 0, '', 0),
(15, 1, 8, 13, 'U', 0, '', 0),
(16, 1, 9, 14, 'U', 0, '', 0),
(17, 1, 10, 15, 'U', 0, '', 0),
(18, 1, 11, 16, 'U', 0, '', 0),
(19, 1, 12, 17, 'U', 0, '', 0),
(20, 1, 0, 32, 'E', 0, '', 9),
(21, 1, 1, 21, 'L', 0, '', 9),
(22, 1, 2, 22, 'L', 0, '', 9),
(23, 1, 3, 23, 'L', 0, '', 9),
(24, 1, 4, 35, 'E', 0, '', 9),
(25, 1, 0, 36, 'E', 0, '', 10),
(26, 1, 1, 24, 'L', 0, '', 10),
(27, 1, 2, 25, 'L', 0, '', 10),
(28, 1, 3, 26, 'L', 0, '', 10),
(29, 1, 4, 37, 'E', 0, '', 10),
(30, 1, 0, 39, 'E', 0, '', 11),
(31, 1, 1, 27, 'L', 0, '', 11),
(32, 1, 2, 58, 'L', 0, '', 11),
(33, 1, 3, 29, 'L', 0, '', 11),
(34, 1, 4, 38, 'E', 0, '', 11),
(35, 1, 0, 40, 'E', 0, '', 12),
(40, 1, 0, 51, 'E', 0, '', 17),
(41, 1, 0, 53, 'E', 0, '', 18),
(42, 1, 0, 55, 'E', 0, '', 19),
(43, 1, 1, 51, 'L', 0, '', 19),
(44, 1, 2, 52, 'L', 0, '', 19),
(45, 1, 3, 53, 'L', 0, '', 19),
(46, 1, 1, 30, 'L', 0, '', 12),
(47, 1, 2, 31, 'L', 0, '', 12),
(48, 1, 3, 32, 'L', 0, '', 12),
(49, 1, 4, 41, 'E', 0, '', 12),
(50, 1, 1, 33, 'L', 0, '', 13),
(51, 1, 2, 34, 'L', 0, '', 13),
(52, 1, 3, 35, 'L', 0, '', 13),
(53, 1, 1, 36, 'L', 0, '', 14),
(54, 1, 2, 37, 'L', 0, '', 14),
(55, 1, 3, 38, 'L', 0, '', 14),
(56, 1, 1, 39, 'L', 0, '', 15),
(57, 1, 2, 40, 'L', 0, '', 15),
(58, 1, 3, 41, 'L', 0, '', 15),
(59, 1, 1, 42, 'L', 0, '', 16),
(60, 1, 2, 43, 'L', 0, '', 16),
(61, 1, 3, 44, 'L', 0, '', 16),
(62, 1, 1, 45, 'L', 0, '', 17),
(63, 1, 2, 46, 'L', 0, '', 17),
(64, 1, 3, 47, 'L', 0, '', 17),
(65, 1, 1, 48, 'L', 0, '', 18),
(66, 1, 2, 49, 'L', 0, '', 18),
(67, 1, 3, 50, 'L', 0, '', 18),
(68, 1, 4, 56, 'E', 0, '', 19),
(69, 1, 4, 54, 'E', 0, '', 18),
(70, 1, 4, 52, 'E', 0, '', 17),
(71, 1, 4, 44, 'E', 0, '', 13),
(72, 1, 4, 46, 'E', 0, '', 14),
(73, 1, 4, 50, 'E', 0, '', 16),
(74, 1, 4, 48, 'E', 0, '', 15),
(75, 2, 1, 62, 'U', 0, '', 0),
(76, 2, 2, 63, 'U', 0, '', 0),
(77, 2, 3, 64, 'U', 0, '', 0),
(78, 2, 4, 65, 'U', 0, '', 0),
(79, 2, 5, 66, 'U', 0, '', 0),
(80, 2, 6, 67, 'U', 0, '', 0),
(81, 2, 7, 68, 'U', 0, '', 0),
(82, 2, 8, 69, 'U', 0, '', 0),
(83, 2, 9, 70, 'U', 0, '', 0),
(84, 2, 10, 71, 'U', 0, '', 0),
(85, 2, 11, 72, 'U', 0, '', 0),
(86, 2, 12, 73, 'U', 0, '', 0),
(87, 2, 0, 68, 'E', 0, '', 75),
(88, 2, 0, 74, 'E', 0, '', 76),
(89, 2, 0, 77, 'E', 0, '', 77),
(90, 2, 0, 79, 'E', 0, '', 78),
(91, 2, 0, 81, 'E', 0, '', 79),
(92, 2, 0, 86, 'E', 0, '', 80),
(93, 2, 0, 102, 'E', 0, '', 81),
(94, 2, 0, 104, 'E', 0, '', 82),
(95, 2, 0, 106, 'E', 0, '', 83),
(96, 2, 0, 109, 'E', 0, '', 84),
(97, 2, 0, 111, 'E', 0, '', 85),
(98, 2, 0, 113, 'E', 0, '', 86),
(99, 2, 1, 75, 'L', 0, '', 75),
(100, 2, 2, 76, 'L', 0, '', 75),
(101, 2, 3, 77, 'L', 0, '', 75),
(102, 2, 1, 78, 'L', 0, '', 76),
(103, 2, 2, 79, 'L', 0, '', 76),
(104, 2, 3, 80, 'L', 0, '', 76),
(105, 2, 1, 82, 'L', 0, '', 77),
(106, 2, 2, 83, 'L', 0, '', 77),
(107, 2, 3, 84, 'L', 0, '', 77),
(108, 2, 1, 85, 'L', 0, '', 78),
(109, 2, 2, 86, 'L', 0, '', 78),
(110, 2, 4, 69, 'E', 0, '', 75),
(111, 2, 4, 75, 'E', 0, '', 76),
(112, 2, 4, 78, 'E', 0, '', 77),
(113, 2, 3, 87, 'L', 0, '', 78),
(114, 2, 4, 80, 'E', 0, '', 78),
(115, 2, 1, 88, 'L', 0, '', 79),
(116, 2, 2, 89, 'L', 0, '', 79),
(117, 2, 3, 90, 'L', 0, '', 79),
(118, 2, 4, 82, 'E', 0, '', 79),
(119, 2, 1, 91, 'L', 0, '', 80),
(120, 2, 2, 92, 'L', 0, '', 80),
(121, 2, 3, 93, 'L', 0, '', 80),
(122, 2, 4, 93, 'E', 0, '', 80),
(123, 2, 1, 94, 'L', 0, '', 81),
(124, 2, 2, 95, 'L', 0, '', 81),
(125, 2, 3, 96, 'L', 0, '', 81),
(126, 2, 4, 103, 'E', 0, '', 81),
(127, 2, 1, 97, 'L', 0, '', 82),
(128, 2, 2, 98, 'L', 0, '', 82),
(129, 2, 3, 99, 'L', 0, '', 82),
(130, 2, 4, 144, 'E', 0, '', 82),
(131, 2, 1, 101, 'L', 0, '', 83),
(132, 2, 2, 102, 'L', 0, '', 83),
(133, 2, 3, 103, 'L', 0, '', 83),
(134, 2, 1, 104, 'L', 0, '', 84),
(135, 2, 2, 105, 'L', 0, '', 84),
(136, 2, 3, 106, 'L', 0, '', 84),
(137, 2, 1, 107, 'L', 0, '', 85),
(138, 2, 2, 108, 'L', 0, '', 85),
(139, 2, 3, 109, 'L', 0, '', 85),
(140, 2, 4, 107, 'E', 0, '', 83),
(141, 2, 4, 110, 'E', 0, '', 84),
(142, 2, 4, 112, 'E', 0, '', 85),
(143, 2, 1, 111, 'L', 0, '', 86),
(144, 2, 2, 112, 'L', 0, '', 86),
(145, 2, 3, 113, 'L', 0, '', 86),
(146, 2, 4, 114, 'E', 0, '', 86),
(147, 3, 1, 114, 'U', 0, '', 0),
(148, 3, 2, 115, 'U', 0, '', 0),
(149, 3, 3, 116, 'U', 0, '', 0),
(150, 3, 4, 117, 'U', 0, '', 0),
(151, 3, 5, 118, 'U', 0, '', 0),
(152, 3, 6, 119, 'U', 0, '', 0),
(153, 3, 7, 120, 'U', 0, '', 0),
(154, 3, 8, 121, 'U', 0, '', 0),
(155, 3, 9, 122, 'U', 0, '', 0),
(156, 3, 10, 123, 'U', 0, '', 0),
(157, 3, 11, 124, 'U', 0, '', 0),
(158, 3, 12, 125, 'U', 0, '', 0),
(159, 3, 13, 126, 'U', 0, '', 0),
(160, 3, 14, 127, 'U', 0, '', 0),
(161, 4, 1, 172, 'U', 0, '', 0),
(162, 4, 2, 173, 'U', 0, '', 0),
(163, 4, 3, 174, 'U', 0, '', 0),
(164, 4, 4, 175, 'U', 0, '', 0),
(165, 4, 5, 176, 'U', 0, '', 0),
(166, 4, 6, 177, 'U', 0, '', 0),
(167, 4, 7, 178, 'U', 0, '', 0),
(168, 4, 8, 179, 'U', 0, '', 0),
(169, 4, 9, 180, 'U', 0, '', 0),
(170, 4, 10, 181, 'U', 0, '', 0),
(171, 4, 11, 182, 'U', 0, '', 0),
(172, 4, 12, 183, 'U', 0, '', 0),
(173, 4, 13, 184, 'U', 0, '', 0),
(174, 4, 14, 185, 'U', 0, '', 0),
(175, 3, 0, 115, 'E', 0, '', 147),
(176, 3, 0, 118, 'E', 0, '', 148),
(177, 3, 0, 133, 'E', 0, '', 149),
(178, 3, 0, 135, 'E', 0, '', 150),
(179, 3, 0, 137, 'E', 0, '', 151),
(180, 3, 0, 139, 'E', 0, '', 152),
(181, 3, 0, 141, 'E', 0, '', 153),
(182, 3, 0, 143, 'E', 0, '', 154),
(183, 3, 0, 145, 'E', 0, '', 155),
(184, 3, 0, 147, 'E', 0, '', 156),
(185, 3, 0, 149, 'E', 0, '', 157),
(186, 3, 0, 151, 'E', 0, '', 158),
(187, 3, 0, 153, 'E', 0, '', 159),
(188, 3, 0, 155, 'E', 0, '', 160),
(189, 3, 1, 128, 'L', 0, '', 147),
(190, 3, 2, 129, 'L', 0, '', 147),
(191, 3, 3, 130, 'L', 0, '', 147),
(192, 3, 1, 131, 'L', 0, '', 148),
(193, 3, 2, 132, 'L', 0, '', 148),
(194, 3, 3, 133, 'L', 0, '', 148),
(195, 3, 1, 134, 'L', 0, '', 149),
(196, 3, 2, 135, 'L', 0, '', 149),
(197, 3, 3, 136, 'L', 0, '', 149),
(198, 3, 4, 116, 'E', 0, '', 147),
(199, 3, 4, 119, 'E', 0, '', 148),
(200, 3, 1, 137, 'L', 0, '', 150),
(201, 3, 2, 138, 'L', 0, '', 150),
(202, 3, 3, 139, 'L', 0, '', 150),
(203, 3, 1, 140, 'L', 0, '', 151),
(204, 3, 2, 141, 'L', 0, '', 151),
(205, 3, 3, 142, 'L', 0, '', 151),
(206, 3, 4, 134, 'E', 0, '', 149),
(207, 3, 4, 136, 'E', 0, '', 150),
(208, 3, 4, 138, 'E', 0, '', 151),
(209, 3, 1, 143, 'L', 0, '', 152),
(210, 3, 2, 144, 'L', 0, '', 152),
(211, 3, 3, 145, 'L', 0, '', 152),
(212, 3, 4, 140, 'E', 0, '', 152),
(213, 3, 1, 146, 'L', 0, '', 153),
(214, 3, 2, 147, 'L', 0, '', 153),
(215, 3, 3, 149, 'L', 0, '', 153),
(216, 3, 1, 150, 'L', 0, '', 154),
(217, 3, 2, 151, 'L', 0, '', 154),
(218, 3, 3, 152, 'L', 0, '', 154),
(219, 3, 1, 154, 'L', 0, '', 155),
(220, 3, 2, 155, 'L', 0, '', 155),
(221, 3, 3, 156, 'L', 0, '', 155),
(222, 3, 1, 157, 'L', 0, '', 156),
(223, 3, 2, 158, 'L', 0, '', 156),
(224, 3, 3, 159, 'L', 0, '', 156),
(225, 3, 4, 142, 'E', 0, '', 153),
(226, 3, 4, 144, 'E', 0, '', 154),
(227, 3, 4, 148, 'E', 0, '', 156),
(228, 3, 1, 160, 'L', 0, '', 157),
(229, 3, 2, 161, 'L', 0, '', 157),
(230, 3, 3, 162, 'L', 0, '', 157),
(240, 3, 1, 166, 'L', 0, '', 159),
(241, 3, 2, 167, 'L', 0, '', 159),
(234, 3, 1, 163, 'L', 0, '', 158),
(235, 3, 2, 164, 'L', 0, '', 158),
(236, 3, 3, 165, 'L', 0, '', 158),
(237, 3, 1, 169, 'L', 0, '', 160),
(238, 3, 2, 170, 'L', 0, '', 160),
(239, 3, 3, 171, 'L', 0, '', 160),
(242, 3, 3, 168, 'L', 0, '', 159),
(243, 3, 4, 150, 'E', 0, '', 157),
(244, 3, 4, 152, 'E', 0, '', 158),
(245, 3, 4, 154, 'E', 0, '', 159),
(246, 3, 4, 146, 'E', 0, '', 155),
(247, 4, 0, 159, 'E', 0, '', 161),
(248, 4, 0, 172, 'E', 0, '', 162),
(249, 4, 0, 174, 'E', 0, '', 163),
(250, 4, 0, 181, 'E', 0, '', 164),
(251, 4, 1, 186, 'L', 0, '', 161),
(252, 4, 2, 187, 'L', 0, '', 161),
(253, 4, 3, 188, 'L', 0, '', 161),
(254, 4, 4, 160, 'E', 0, '', 161),
(255, 4, 1, 203, 'L', 0, '', 162),
(256, 4, 2, 204, 'L', 0, '', 162),
(257, 4, 3, 205, 'L', 0, '', 162),
(258, 4, 4, 171, 'E', 0, '', 162),
(259, 4, 1, 200, 'L', 0, '', 163),
(260, 4, 2, 201, 'L', 0, '', 163),
(261, 4, 3, 202, 'L', 0, '', 163),
(262, 4, 4, 175, 'E', 0, '', 163),
(263, 4, 1, 197, 'L', 0, '', 164),
(264, 4, 2, 198, 'L', 0, '', 164),
(265, 4, 3, 199, 'L', 0, '', 164),
(266, 4, 4, 184, 'E', 0, '', 164);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_cursosesion`
--
drop table acad_cursosesion;
CREATE TABLE IF NOT EXISTS `acad_cursosesion` (
  `idsesion` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `duracion` time DEFAULT NULL,
  `informaciongeneral` longtext,
  `idcursodetalle` int(11) NOT NULL,
  PRIMARY KEY (`idsesion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_grupoaula`
--
drop table acad_grupoaula;
CREATE TABLE IF NOT EXISTS `acad_grupoaula` (
  `idgrupoaula` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'virtual,presencial,mixto',
  `comentario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `nvacantes` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`idgrupoaula`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_grupoaula`
--

INSERT INTO `acad_grupoaula` (`idgrupoaula`, `nombre`, `tipo`, `comentario`, `nvacantes`, `estado`) VALUES
(1, 'ING-A1', 'P', '', 100, 1),
(2, 'ING-A2', 'P', '', 59, 1),
(3, 'ING-B1', 'P', '', 100, 1),
(4, 'ING-B2', 'P', '', 100, 1),
(5, 'ING-C1', 'P', '', 100, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_grupoauladetalle`
--
drop table acad_grupoauladetalle;
CREATE TABLE IF NOT EXISTS `acad_grupoauladetalle` (
  `idgrupoauladetalle` bigint(20) NOT NULL,
  `idgrupoaula` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `iddocente` int(11) NOT NULL,
  `idlocal` int(11) DEFAULT '0',
  `idambiente` int(11) DEFAULT '0' COMMENT 'aula física o laboratorio donde se dará la clase',
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_final` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idgrupoauladetalle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_grupoauladetalle`
--

INSERT INTO `acad_grupoauladetalle` (`idgrupoauladetalle`, `idgrupoaula`, `idcurso`, `iddocente`, `idlocal`, `idambiente`, `nombre`, `fecha_inicio`, `fecha_final`) VALUES
(1, 1, 1, 88888888, 1, 1, '-', '2018-01-16 05:00:00', '2018-02-28 05:00:00'),
(2, 2, 2, 88888888, 1, 1, '-', '2018-02-28 05:00:00', '2018-04-16 05:00:00'),
(3, 3, 3, 88888888, 1, 1, '-', '2018-04-16 05:00:00', '2018-06-01 05:00:00'),
(4, 4, 4, 88888888, 1, 1, '-', '2018-04-01 05:00:00', '2018-05-01 05:00:00'),
(5, 5, 5, 88888888, 1, 1, '-', '2018-04-30 05:00:00', '2018-05-31 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_horariogrupodetalle`
--

drop table acad_horariogrupodetalle;

CREATE TABLE IF NOT EXISTS `acad_horariogrupodetalle` (
  `idhorario` int(11) NOT NULL,
  `idgrupoauladetalle` int(11) NOT NULL,
  `fecha_finicio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_final` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descripcion` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `color` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idhorariopadre` tinyint(4) NOT NULL,
  `diasemana` tinyint(4) NOT NULL,
  PRIMARY KEY (`idhorario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_horariogrupodetalle`
--

INSERT INTO `acad_horariogrupodetalle` (`idhorario`, `idgrupoauladetalle`, `fecha_finicio`, `fecha_final`, `descripcion`, `color`, `idhorariopadre`, `diasemana`) VALUES
(1, 1, '2018-01-22 14:00:00', '2018-01-22 16:00:00', 'A1', '#1adf4b', 1, 1),
(2, 1, '2018-01-29 14:00:00', '2018-01-29 16:00:00', 'A1', '#1adf4b', 1, 1),
(3, 1, '2018-02-05 14:00:00', '2018-02-05 16:00:00', 'A1', '#1adf4b', 1, 1),
(4, 1, '2018-02-12 14:00:00', '2018-02-12 16:00:00', 'A1', '#1adf4b', 1, 1),
(5, 1, '2018-02-19 14:00:00', '2018-02-19 16:00:00', 'A1', '#1adf4b', 1, 1),
(6, 1, '2018-02-26 14:00:00', '2018-02-26 16:00:00', 'A1', '#1adf4b', 1, 1),
(7, 2, '2018-03-06 14:00:00', '2018-03-06 16:30:00', 'A2', '#2b8ace', 2, 2),
(8, 2, '2018-03-13 14:00:00', '2018-03-13 16:30:00', 'A2', '#2b8ace', 2, 2),
(9, 2, '2018-03-20 14:00:00', '2018-03-20 16:30:00', 'A2', '#2b8ace', 2, 2),
(10, 2, '2018-03-27 14:00:00', '2018-03-27 16:30:00', 'A2', '#2b8ace', 2, 2),
(11, 2, '2018-04-03 14:00:00', '2018-04-03 16:30:00', 'A2', '#2b8ace', 2, 2),
(12, 2, '2018-04-10 14:00:00', '2018-04-10 16:30:00', 'A2', '#2b8ace', 2, 2),
(19, 3, '2018-05-31 14:00:00', '2018-05-31 16:30:00', 'B1', '#d3e315', 3, 4),
(18, 3, '2018-05-24 14:00:00', '2018-05-24 16:30:00', 'B1', '#d3e315', 3, 4),
(17, 3, '2018-05-17 14:00:00', '2018-05-17 16:30:00', 'B1', '#d3e315', 3, 4),
(16, 3, '2018-05-10 14:00:00', '2018-05-10 16:30:00', 'B1', '#d3e315', 3, 4),
(15, 3, '2018-05-03 14:00:00', '2018-05-03 16:30:00', 'B1', '#d3e315', 3, 4),
(14, 3, '2018-04-26 14:00:00', '2018-04-26 16:30:00', 'B1', '#d3e315', 3, 4),
(13, 3, '2018-04-19 14:00:00', '2018-04-19 16:30:00', 'B1', '#d3e315', 3, 4),
(20, 4, '2018-04-04 14:00:00', '2018-04-04 16:30:00', 'B2', '#92ec8c', 4, 3),
(21, 4, '2018-04-11 14:00:00', '2018-04-11 16:30:00', 'B2', '#92ec8c', 4, 3),
(22, 4, '2018-04-18 14:00:00', '2018-04-18 16:30:00', 'B2', '#92ec8c', 4, 3),
(23, 4, '2018-04-25 14:00:00', '2018-04-25 16:30:00', 'B2', '#92ec8c', 4, 3),
(24, 5, '2018-05-04 14:40:00', '2018-05-04 16:40:00', 'C1', '#2b8ace', 5, 5),
(25, 5, '2018-05-11 14:40:00', '2018-05-11 16:40:00', 'C1', '#2b8ace', 5, 5),
(26, 5, '2018-05-18 14:40:00', '2018-05-18 16:40:00', 'C1', '#2b8ace', 5, 5),
(27, 5, '2018-05-25 14:40:00', '2018-05-25 16:40:00', 'C1', '#2b8ace', 5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_matricula`
--
drop table acad_matricula;
CREATE TABLE IF NOT EXISTS `acad_matricula` (
  `idmatricula` int(11) NOT NULL,
  `idgrupoauladetalle` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fecha_matricula` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmatricula`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_matricula`
--

INSERT INTO `acad_matricula` (`idmatricula`, `idgrupoauladetalle`, `idalumno`, `fecha_registro`, `estado`, `idusuario`, `fecha_matricula`) VALUES
(1, 1, 72042592, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:17:31'),
(2, 1, 43831104, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:18:36'),
(3, 1, 55555555, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:21:33'),
(4, 2, 55555555, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:22:03'),
(5, 3, 55555555, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:22:16'),
(6, 4, 55555555, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:22:50'),
(7, 2, 72042592, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:23:54'),
(8, 3, 72042592, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:24:15'),
(9, 4, 72042592, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:24:26'),
(10, 4, 43831104, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:25:15'),
(11, 2, 43831104, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:26:38'),
(12, 3, 43831104, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 11:26:58'),
(13, 5, 43831104, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 12:58:27'),
(14, 5, 72042592, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 12:58:41'),
(15, 5, 55555555, '2018-01-16 05:00:00', 1, 43831104, '2018-01-16 12:59:11');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
