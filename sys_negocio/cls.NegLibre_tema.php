<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-04-2018
 * @copyright	Copyright (C) 17-04-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatLibre_tema', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegLibre_tema 
{
	protected $idtema;
	protected $nombre;
	protected $descripcion;
	protected $idtipo;
	
	protected $dataLibre_tema;
	protected $oDatLibre_tema;	

	public function __construct()
	{
		$this->oDatLibre_tema = new DatLibre_tema;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatLibre_tema->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatLibre_tema->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatLibre_tema->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatLibre_tema->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatLibre_tema->get($this->idtema);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('libre_tema', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatLibre_tema->iniciarTransaccion('neg_i_Libre_tema');
			$this->idtema = $this->oDatLibre_tema->insertar($this->nombre,$this->descripcion,$this->idtipo);
			$this->oDatLibre_tema->terminarTransaccion('neg_i_Libre_tema');	
			return $this->idtema;
		} catch(Exception $e) {	
		    $this->oDatLibre_tema->cancelarTransaccion('neg_i_Libre_tema');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('libre_tema', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatLibre_tema->actualizar($this->idtema,$this->nombre,$this->descripcion,$this->idtipo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Libre_tema', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatLibre_tema->eliminar($this->idtema);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtema($pk){
		try {
			$this->dataLibre_tema = $this->oDatLibre_tema->get($pk);
			if(empty($this->dataLibre_tema)) {
				throw new Exception(JrTexto::_("Libre_tema").' '.JrTexto::_("not registered"));
			}
			$this->idtema = $this->dataLibre_tema["idtema"];
			$this->nombre = $this->dataLibre_tema["nombre"];
			$this->descripcion = $this->dataLibre_tema["descripcion"];
			$this->idtipo = $this->dataLibre_tema["idtipo"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('libre_tema', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataLibre_tema = $this->oDatLibre_tema->get($pk);
			if(empty($this->dataLibre_tema)) {
				throw new Exception(JrTexto::_("Libre_tema").' '.JrTexto::_("not registered"));
			}

			return $this->oDatLibre_tema->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}