<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_tipo_registro', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBib_tipo_registro 
{
	protected $id_registro;
	protected $nombre;
	
	protected $dataBib_tipo_registro;
	protected $oDatBib_tipo_registro;	

	public function __construct()
	{
		$this->oDatBib_tipo_registro = new DatBib_tipo_registro;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_tipo_registro->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_tipo_registro->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_tipo_registro->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBib_tipo_registro->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_tipo_registro->get($this->id_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_tipo_registro', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_tipo_registro->iniciarTransaccion('neg_i_Bib_tipo_registro');
			$this->id_registro = $this->oDatBib_tipo_registro->insertar($this->nombre);
			//$this->oDatBib_tipo_registro->terminarTransaccion('neg_i_Bib_tipo_registro');	
			return $this->id_registro;
		} catch(Exception $e) {	
		   //$this->oDatBib_tipo_registro->cancelarTransaccion('neg_i_Bib_tipo_registro');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_tipo_registro', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatBib_tipo_registro->actualizar($this->id_registro,$this->nombre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_tipo_registro', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_tipo_registro->eliminar($this->id_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_registro($pk){
		try {
			$this->dataBib_tipo_registro = $this->oDatBib_tipo_registro->get($pk);
			if(empty($this->dataBib_tipo_registro)) {
				throw new Exception(JrTexto::_("Bib_tipo_registro").' '.JrTexto::_("not registered"));
			}
			$this->id_registro = $this->dataBib_tipo_registro["id_registro"];
			$this->nombre = $this->dataBib_tipo_registro["nombre"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_tipo_registro', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_tipo_registro = $this->oDatBib_tipo_registro->get($pk);
			if(empty($this->dataBib_tipo_registro)) {
				throw new Exception(JrTexto::_("Bib_tipo_registro").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_tipo_registro->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}