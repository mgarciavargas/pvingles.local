<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		02-01-2017
 * @copyright	Copyright (C) 02-01-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRecord', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegRecord 
{
	protected $idrecord;
	protected $nombre;
	protected $link;
	protected $idpersonal;
	protected $idnivel;
	protected $idunidad;
	protected $idleccion;
	protected $fechareg;
	protected $idherramienta;
	
	protected $dataRecord;
	protected $oDatRecord;	

	public function __construct()
	{
		$this->oDatRecord = new DatRecord;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;			
			$this->oDatRecord->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatRecord->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRecord->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRecord->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRecord->get($this->idrecord);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('record', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatRecord->iniciarTransaccion('neg_i_Record');
			$this->idrecord = $this->oDatRecord->insertar($this->nombre,$this->link,$this->idpersonal,$this->idnivel,$this->idunidad,$this->idleccion,$this->fechareg,$this->idherramienta);
			//$this->oDatRecord->terminarTransaccion('neg_i_Record');	
			return $this->idrecord;
		} catch(Exception $e) {	
		   //$this->oDatRecord->cancelarTransaccion('neg_i_Record');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('record', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatRecord->actualizar($this->idrecord,$this->nombre,$this->link,$this->idpersonal,$this->idnivel,$this->idunidad,$this->idleccion,$this->fechareg,$this->idherramienta);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Record', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatRecord->eliminar($this->idrecord);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrecord($pk){
		try {
			$this->dataRecord = $this->oDatRecord->get($pk);
			if(empty($this->dataRecord)) {
				throw new Exception(JrTexto::_("Record").' '.JrTexto::_("not registered"));
			}
			$this->idrecord = $this->dataRecord["idrecord"];
			$this->nombre = $this->dataRecord["nombre"];
			$this->link = $this->dataRecord["link"];
			$this->idpersonal = $this->dataRecord["idpersonal"];
			$this->idnivel = $this->dataRecord["idnivel"];
			$this->idunidad = $this->dataRecord["idunidad"];
			$this->idleccion = $this->dataRecord["idleccion"];
			$this->fechareg = $this->dataRecord["fechareg"];
			$this->idherramienta = $this->dataRecord["idherramienta"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('record', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataRecord = $this->oDatRecord->get($pk);
			if(empty($this->dataRecord)) {
				throw new Exception(JrTexto::_("Record").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRecord->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}		
}