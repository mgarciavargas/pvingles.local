<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		24-08-2018
 * @copyright	Copyright (C) 24-08-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNotas_quiz', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegNotas_quiz 
{
	protected $idnota;
	protected $idcursodetalle;
	protected $idrecurso;
	protected $idalumno;
	protected $tipo;
	protected $nota;
	protected $regfecha;
	
	protected $dataNotas_quiz;
	protected $oDatNotas_quiz;	

	public function __construct()
	{
		$this->oDatNotas_quiz = new DatNotas_quiz;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatNotas_quiz->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatNotas_quiz->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatNotas_quiz->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatNotas_quiz->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatNotas_quiz->get($this->idnota);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_quiz', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			#$this->oDatNotas_quiz->iniciarTransaccion('neg_i_Notas_quiz');
			$this->idnota = $this->oDatNotas_quiz->insertar($this->idcursodetalle,$this->idrecurso,$this->idalumno,$this->tipo,$this->nota,$this->regfecha);
			#$this->oDatNotas_quiz->terminarTransaccion('neg_i_Notas_quiz');	
			return $this->idnota;
		} catch(Exception $e) {	
		    $this->oDatNotas_quiz->cancelarTransaccion('neg_i_Notas_quiz');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_quiz', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatNotas_quiz->actualizar($this->idnota,$this->idcursodetalle,$this->idrecurso,$this->idalumno,$this->tipo,$this->nota,$this->regfecha);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Notas_quiz', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotas_quiz->eliminar($this->idnota);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdnota($pk){
		try {
			$this->dataNotas_quiz = $this->oDatNotas_quiz->get($pk);
			if(empty($this->dataNotas_quiz)) {
				throw new Exception(JrTexto::_("Notas_quiz").' '.JrTexto::_("not registered"));
			}
			$this->idnota = $this->dataNotas_quiz["idnota"];
			$this->idcursodetalle = $this->dataNotas_quiz["idcursodetalle"];
			$this->idrecurso = $this->dataNotas_quiz["idrecurso"];
			$this->idalumno = $this->dataNotas_quiz["idalumno"];
			$this->tipo = $this->dataNotas_quiz["tipo"];
			$this->nota = $this->dataNotas_quiz["nota"];
			$this->regfecha = $this->dataNotas_quiz["regfecha"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('notas_quiz', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataNotas_quiz = $this->oDatNotas_quiz->get($pk);
			if(empty($this->dataNotas_quiz)) {
				throw new Exception(JrTexto::_("Notas_quiz").' '.JrTexto::_("not registered"));
			}

			return $this->oDatNotas_quiz->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
		
}