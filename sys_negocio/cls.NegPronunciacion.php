<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		16-10-2017
 * @copyright	Copyright (C) 16-10-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPronunciacion', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegPronunciacion 
{
	protected $id;
	protected $palabra;
	protected $pron;
	
	protected $dataPronunciacion;
	protected $oDatPronunciacion;	

	public function __construct()
	{
		$this->oDatPronunciacion = new DatPronunciacion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPronunciacion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatPronunciacion->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPronunciacion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarpronunciacion($palabra){
		try {
			if(empty($palabra)) return ;
			return $this->oDatPronunciacion->buscarpronunciacion($palabra);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPronunciacion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPronunciacion->get($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('pronunciacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatPronunciacion->iniciarTransaccion('neg_i_Pronunciacion');
			$this->id = $this->oDatPronunciacion->insertar($this->palabra,$this->pron);
			$this->oDatPronunciacion->terminarTransaccion('neg_i_Pronunciacion');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatPronunciacion->cancelarTransaccion('neg_i_Pronunciacion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('pronunciacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatPronunciacion->actualizar($this->id,$this->palabra,$this->pron);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Pronunciacion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatPronunciacion->eliminar($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataPronunciacion = $this->oDatPronunciacion->get($pk);
			if(empty($this->dataPronunciacion)) {
				throw new Exception(JrTexto::_("Pronunciacion").' '.JrTexto::_("not registered"));
			}
			$this->id = $this->dataPronunciacion["id"];
			$this->palabra = $this->dataPronunciacion["palabra"];
			$this->pron = $this->dataPronunciacion["pron"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('pronunciacion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataPronunciacion = $this->oDatPronunciacion->get($pk);
			if(empty($this->dataPronunciacion)) {
				throw new Exception(JrTexto::_("Pronunciacion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPronunciacion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}