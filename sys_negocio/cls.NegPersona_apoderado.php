<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		02-01-2018
 * @copyright	Copyright (C) 02-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersona_apoderado', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegPersona_apoderado 
{
	protected $idapoderado;
	protected $idpersona;
	protected $nombre;
	protected $ape_paterno;
	protected $ape_materno;
	protected $correo;
	protected $sexo;
	protected $tipodoc;
	protected $ndoc;
	protected $parentesco;
	protected $telefono;
	protected $celular;
	protected $mostrar;
	
	protected $dataPersona_apoderado;
	protected $oDatPersona_apoderado;	

	public function __construct()
	{
		$this->oDatPersona_apoderado = new DatPersona_apoderado;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPersona_apoderado->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatPersona_apoderado->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPersona_apoderado->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPersona_apoderado->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPersona_apoderado->get($this->idapoderado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_apoderado', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPersona_apoderado->iniciarTransaccion('neg_i_Persona_apoderado');
			$this->idapoderado = $this->oDatPersona_apoderado->insertar($this->idpersona,$this->nombre,$this->ape_paterno,$this->ape_materno,$this->correo,$this->sexo,$this->tipodoc,$this->ndoc,$this->parentesco,$this->telefono,$this->celular,$this->mostrar);
			$this->oDatPersona_apoderado->terminarTransaccion('neg_i_Persona_apoderado');	
			return $this->idapoderado;
		} catch(Exception $e) {	
		    $this->oDatPersona_apoderado->cancelarTransaccion('neg_i_Persona_apoderado');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_apoderado', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPersona_apoderado->actualizar($this->idapoderado,$this->idpersona,$this->nombre,$this->ape_paterno,$this->ape_materno,$this->correo,$this->sexo,$this->tipodoc,$this->ndoc,$this->parentesco,$this->telefono,$this->celular,$this->mostrar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatPersona_apoderado->cambiarvalorcampo($this->idapoderado,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Persona_apoderado', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPersona_apoderado->eliminar($this->idapoderado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdapoderado($pk){
		try {
			$this->dataPersona_apoderado = $this->oDatPersona_apoderado->get($pk);
			if(empty($this->dataPersona_apoderado)){
				throw new Exception(JrTexto::_("Persona_apoderado").' '.JrTexto::_("not registered"));
			}
			$this->idapoderado = $this->dataPersona_apoderado["idapoderado"];
			$this->idpersona = $this->dataPersona_apoderado["idpersona"];
			$this->nombre = $this->dataPersona_apoderado["nombre"];
			$this->ape_paterno = $this->dataPersona_apoderado["ape_paterno"];
			$this->ape_materno = $this->dataPersona_apoderado["ape_materno"];
			$this->correo = $this->dataPersona_apoderado["correo"];
			$this->sexo = $this->dataPersona_apoderado["sexo"];
			$this->tipodoc = $this->dataPersona_apoderado["tipodoc"];
			$this->ndoc = $this->dataPersona_apoderado["ndoc"];
			$this->parentesco = $this->dataPersona_apoderado["parentesco"];
			$this->telefono = $this->dataPersona_apoderado["telefono"];
			$this->celular = $this->dataPersona_apoderado["celular"];
			$this->mostrar = $this->dataPersona_apoderado["mostrar"];
			//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('persona_apoderado', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPersona_apoderado = $this->oDatPersona_apoderado->get($pk);
			if(empty($this->dataPersona_apoderado)) {
				throw new Exception(JrTexto::_("Persona_apoderado").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPersona_apoderado->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}