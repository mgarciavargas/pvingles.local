<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_autor', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBib_autor 
{
	protected $id_autor;
	protected $nombre;
	protected $ap_paterno;
	protected $ap_materno;
	protected $id_pais;
	
	protected $dataBib_autor;
	protected $oDatBib_autor;	

	public function __construct()
	{
		$this->oDatBib_autor = new DatBib_autor;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_autor->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_autor->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_autor->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBib_autor->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_autor->get($this->id_autor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_autor', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_autor->iniciarTransaccion('neg_i_Bib_autor');
			$this->id_autor = $this->oDatBib_autor->insertar($this->nombre,$this->ap_paterno,$this->ap_materno,$this->id_pais);
			//$this->oDatBib_autor->terminarTransaccion('neg_i_Bib_autor');	
			return $this->id_autor;
		} catch(Exception $e) {	
		   //$this->oDatBib_autor->cancelarTransaccion('neg_i_Bib_autor');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_autor', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatBib_autor->actualizar($this->id_autor,$this->nombre,$this->ap_paterno,$this->ap_materno,$this->id_pais);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_autor', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_autor->eliminar($this->id_autor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_autor($pk){
		try {
			$this->dataBib_autor = $this->oDatBib_autor->get($pk);
			if(empty($this->dataBib_autor)) {
				throw new Exception(JrTexto::_("Bib_autor").' '.JrTexto::_("not registered"));
			}
			$this->id_autor = $this->dataBib_autor["id_autor"];
			$this->nombre = $this->dataBib_autor["nombre"];
			$this->ap_paterno = $this->dataBib_autor["ap_paterno"];
			$this->ap_materno = $this->dataBib_autor["ap_materno"];
			$this->id_pais = $this->dataBib_autor["id_pais"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_autor', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_autor = $this->oDatBib_autor->get($pk);
			if(empty($this->dataBib_autor)) {
				throw new Exception(JrTexto::_("Bib_autor").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_autor->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
		
}