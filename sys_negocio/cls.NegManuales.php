<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		15-01-2018
 * @copyright	Copyright (C) 15-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatManuales', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegManuales 
{
	protected $idmanual;
	protected $titulo;
	protected $abreviado;
	protected $stock;
	protected $total;
	
	protected $dataManuales;
	protected $oDatManuales;	

	public function __construct()
	{
		$this->oDatManuales = new DatManuales;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatManuales->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatManuales->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatManuales->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatManuales->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatManuales->get($this->idmanual);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('manuales', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatManuales->iniciarTransaccion('neg_i_Manuales');
			$this->idmanual = $this->oDatManuales->insertar($this->titulo,$this->abreviado,$this->stock,$this->total);
			$this->oDatManuales->terminarTransaccion('neg_i_Manuales');	
			return $this->idmanual;
		} catch(Exception $e) {	
		    $this->oDatManuales->cancelarTransaccion('neg_i_Manuales');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('manuales', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatManuales->actualizar($this->idmanual,$this->titulo,$this->abreviado,$this->stock,$this->total);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Manuales', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatManuales->eliminar($this->idmanual);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmanual($pk){
		try {
			$this->dataManuales = $this->oDatManuales->get($pk);
			if(empty($this->dataManuales)) {
				throw new Exception(JrTexto::_("Manuales").' '.JrTexto::_("not registered"));
			}
			$this->idmanual = $this->dataManuales["idmanual"];
			$this->titulo = $this->dataManuales["titulo"];
			$this->abreviado = $this->dataManuales["abreviado"];
			$this->stock = $this->dataManuales["stock"];
			$this->total = $this->dataManuales["total"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('manuales', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataManuales = $this->oDatManuales->get($pk);
			if(empty($this->dataManuales)) {
				throw new Exception(JrTexto::_("Manuales").' '.JrTexto::_("not registered"));
			}

			return $this->oDatManuales->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}