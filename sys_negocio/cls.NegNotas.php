<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		27-11-2017
 * @copyright	Copyright (C) 27-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegNotas
{
	
	public function __construct()
	{
		$this->arrContextOptions=array( "ssl"=>array( "verify_peer"=>false, "verify_peer_name"=>false, ), ); 
	}

	public function getArchivos($filtros = array())
	{
		try {
			if(empty($filtros["iddocente"])){ throw new Exception(JrTexto::_("Impossible to get files of professor")); }
			$response = null;
			$service_resp = file_get_contents(URL_BASE.'/notas/service/get_todos_archivos?iddocente='.$filtros["iddocente"].'&identificador='.@$filtros["identificador"], false, stream_context_create($this->arrContextOptions));
            $service_resp= json_decode($service_resp, true);
            if($service_resp['code']==200){
            	$response = $service_resp['data'];
            }
            return $response;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
		
}