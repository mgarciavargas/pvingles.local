<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-11-2017
 * @copyright	Copyright (C) 03-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAlumno_logro', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAlumno_logro 
{
	protected $id_alumno_logro;
	protected $id_alumno;
	protected $id_logro;
	protected $idrecurso;
	protected $tiporecurso;
	protected $fecha;
	protected $bandera;
	
	protected $dataAlumno_logro;
	protected $oDatAlumno_logro;	

	public function __construct()
	{
		$this->oDatAlumno_logro = new DatAlumno_logro;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAlumno_logro->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAlumno_logro->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAlumno_logro->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAlumno_logro->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAlumno_logro->get($this->id_alumno_logro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('alumno_logro', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAlumno_logro->iniciarTransaccion('neg_i_Alumno_logro');
			$this->id_alumno_logro = $this->oDatAlumno_logro->insertar($this->id_alumno,$this->id_logro,$this->idrecurso,$this->tiporecurso,$this->fecha,$this->bandera);
			$this->oDatAlumno_logro->terminarTransaccion('neg_i_Alumno_logro');	
			return $this->id_alumno_logro;
		} catch(Exception $e) {	
		    $this->oDatAlumno_logro->cancelarTransaccion('neg_i_Alumno_logro');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('alumno_logro', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAlumno_logro->actualizar($this->id_alumno_logro,$this->id_alumno,$this->id_logro,$this->idrecurso,$this->tiporecurso,$this->fecha,$this->bandera);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Alumno_logro', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAlumno_logro->eliminar($this->id_alumno_logro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_alumno_logro($pk){
		try {
			$this->dataAlumno_logro = $this->oDatAlumno_logro->get($pk);
			if(empty($this->dataAlumno_logro)) {
				throw new Exception(JrTexto::_("Alumno_logro").' '.JrTexto::_("not registered"));
			}
			$this->id_alumno_logro = $this->dataAlumno_logro["id_alumno_logro"];
			$this->id_alumno = $this->dataAlumno_logro["id_alumno"];
			$this->id_logro = $this->dataAlumno_logro["id_logro"];
			$this->idrecurso = $this->dataAlumno_logro["idrecurso"];
			$this->tiporecurso = $this->dataAlumno_logro["tiporecurso"];
			$this->fecha = $this->dataAlumno_logro["fecha"];
			$this->bandera = $this->dataAlumno_logro["bandera"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('alumno_logro', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAlumno_logro = $this->oDatAlumno_logro->get($pk);
			if(empty($this->dataAlumno_logro)) {
				throw new Exception(JrTexto::_("Alumno_logro").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAlumno_logro->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}