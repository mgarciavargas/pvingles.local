<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		22-11-2016
 * @copyright	Copyright (C) 22-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRecord', RUTA_BASE, 'sys_datos');
//JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegRecord
{
	
	protected $nivel;
	protected $unidad;
	protected $actividad;

	protected $txtAudio;
	protected $txtTemp;
	protected $txtTipo;
	protected $idpersonal;
	
	
	//protected $dataActividades;
	protected $oDatRecord;	

	public function __construct()
	{
		$this->oDatRecord = new DatRecord;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRecord->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatRecord->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRecord->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRecord->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRecord->get($this->idactividad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			
			$this->idrecord = $this->oDatRecord->insertar($this->txtAudio,$this->txtTemp,$this->idpersonal,$this->nivel,$this->unidad,$this->actividad);

			return $this->idrecord;
		} catch(Exception $e) {	
		   //$this->oDatRecord->cancelarTransaccion('neg_i_Actividades');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
					
			return $this->oDatRecord->actualizar($this->idactividad,$this->nivel,$this->unidad,$this->sesion,$this->metodologia,$this->habilidad,$this->titulo,$this->descripcion,$this->estado,$this->url,$this->idioma);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Actividades', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatRecord->eliminar($this->idactividad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdactividad($pk){
		try {
			$this->dataActividades = $this->oDatRecord->get($pk);
			if(empty($this->dataActividades)) {
				throw new Exception(JrTexto::_("Actividades").' '.JrTexto::_("not registered"));
			}
			$this->idactividad = $this->dataActividades["idactividad"];
			$this->nivel = $this->dataActividades["nivel"];
			$this->unidad = $this->dataActividades["unidad"];
			$this->sesion = $this->dataActividades["sesion"];
			$this->metodologia = $this->dataActividades["metodologia"];
			$this->habilidad = $this->dataActividades["habilidad"];
			$this->titulo = $this->dataActividades["titulo"];
			$this->descripcion = $this->dataActividades["descripcion"];
			$this->estado = $this->dataActividades["estado"];
			$this->url = $this->dataActividades["url"];
			$this->idioma = $this->dataActividades["idioma"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('actividades', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataActividades = $this->oDatRecord->get($pk);
			if(empty($this->dataActividades)) {
				throw new Exception(JrTexto::_("Actividades").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRecord->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
		
}