<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		09-11-2017
 * @copyright	Copyright (C) 09-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_cursosesion', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAcad_cursosesion 
{
	protected $idsesion;
	protected $idcurso;
	protected $duracion;
	protected $informaciongeneral;
	protected $idcursodetalle;
	
	protected $dataAcad_cursosesion;
	protected $oDatAcad_cursosesion;	

	public function __construct()
	{
		$this->oDatAcad_cursosesion = new DatAcad_cursosesion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_cursosesion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_cursosesion->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_cursosesion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_cursosesion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_cursosesion->get($this->idsesion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$this->oDatAcad_cursosesion->iniciarTransaccion('neg_i_Acad_cursosesion');
			$this->idsesion = $this->oDatAcad_cursosesion->insertar($this->idcurso,$this->duracion,$this->informaciongeneral,$this->idcursodetalle);
			$this->oDatAcad_cursosesion->terminarTransaccion('neg_i_Acad_cursosesion');	
			return $this->idsesion;
		} catch(Exception $e) {	
		    $this->oDatAcad_cursosesion->cancelarTransaccion('neg_i_Acad_cursosesion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
		
			return $this->oDatAcad_cursosesion->actualizar($this->idsesion,$this->idcurso,$this->duracion,$this->informaciongeneral,$this->idcursodetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {

			return $this->oDatAcad_cursosesion->eliminar($this->idsesion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdsesion($pk){
		try {
			$this->idsesion = $this->dataAcad_cursosesion["idsesion"];
			$this->idcurso = $this->dataAcad_cursosesion["idcurso"];
			$this->duracion = $this->dataAcad_cursosesion["duracion"];
			$this->informaciongeneral = $this->dataAcad_cursosesion["informaciongeneral"];
			$this->idcursodetalle = $this->dataAcad_cursosesion["idcursodetalle"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			$this->dataAcad_cursosesion = $this->oDatAcad_cursosesion->get($pk);
			if(empty($this->dataAcad_cursosesion)) {
				throw new Exception(JrTexto::_("Acad_cursosesion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_cursosesion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}