<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		10-01-2017
 * @copyright	Copyright (C) 10-01-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatUgel', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegUgel 
{
	protected $idugel;
	protected $descripcion;
	protected $abrev;
	protected $iddepartamento;
	protected $idprovincia;
	protected $iddep;
	protected $idpro;
	protected $idpadre;
	protected $idpadre1;

	protected $dataUgel;
	protected $oDatUgel;	

	public function __construct()
	{
		$this->oDatUgel = new DatUgel;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatUgel->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatUgel->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatUgel->buscar($filtros);			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function ubigeo($filtros = array())
	{
		try {
			return $this->oDatUgel->ubigeo($filtros);			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatUgel->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatUgel->get($this->idugel);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$this->oDatUgel->iniciarTransaccion('neg_i_Ugel');
			$this->idugel = $this->oDatUgel->insertar($this->descripcion,$this->abrev,$this->iddepartamento,$this->idprovincia);
			$this->oDatUgel->terminarTransaccion('neg_i_Ugel');	
			return $this->idugel;
		} catch(Exception $e) {	
		    $this->oDatUgel->cancelarTransaccion('neg_i_Ugel');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {						
			return $this->oDatUgel->actualizar($this->idugel,$this->descripcion,$this->abrev,$this->iddepartamento,$this->idprovincia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			return $this->oDatUgel->eliminar($this->idugel);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdugel($pk){
		try {
			$this->dataUgel = $this->oDatUgel->get($pk);
			if(empty($this->dataUgel)) {
				throw new Exception(JrTexto::_("Ugel").' '.JrTexto::_("not registered"));
			}
			$this->idugel = $this->dataUgel["idugel"];
			$this->descripcion = $this->dataUgel["descripcion"];
			$this->abrev = $this->dataUgel["abrev"];
			$this->iddepartamento = $this->dataUgel["iddepartamento"];
			$this->idprovincia = $this->dataUgel["idprovincia"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {			
			$this->dataUgel = $this->oDatUgel->get($pk);
			if(empty($this->dataUgel)) {
				throw new Exception(JrTexto::_("Ugel").' '.JrTexto::_("not registered"));
			}
			return $this->oDatUgel->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}