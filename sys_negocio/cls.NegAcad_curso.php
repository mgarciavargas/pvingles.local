<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		08-02-2018
 * @copyright	Copyright (C) 08-02-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_curso', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAcad_curso 
{
	protected $idcurso;
	protected $nombre;
	protected $imagen;
	protected $descripcion;
	protected $estado;
	protected $fecharegistro;
	protected $idusuario;
	protected $vinculosaprendizajes;
	protected $materialesyrecursos;
	protected $color;
	
	protected $dataAcad_curso;
	protected $oDatAcad_curso;	

	public function __construct()
	{
		$this->oDatAcad_curso = new DatAcad_curso;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_curso->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_curso->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_curso->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getCursos_Not_In_GrupoAulaDetalle($filtros = array(), $iddocente=0)
	{
		try {
			if($iddocente<=0){ throw new Exception(JrTexto::_('Iddocente is missing')); }
			return $this->oDatAcad_curso->getCursos_Not_In_GrupoAulaDetalle($filtros, $iddocente);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_curso->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_curso->get($this->idcurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_curso', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_curso->iniciarTransaccion('neg_i_Acad_curso');
			$this->idcurso = $this->oDatAcad_curso->insertar($this->nombre,$this->imagen,$this->descripcion,$this->estado,$this->fecharegistro,$this->idusuario,$this->vinculosaprendizajes,$this->materialesyrecursos,$this->color);
			$this->oDatAcad_curso->terminarTransaccion('neg_i_Acad_curso');	
			return $this->idcurso;
		} catch(Exception $e) {	
		    $this->oDatAcad_curso->cancelarTransaccion('neg_i_Acad_curso');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_curso', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_curso->actualizar($this->idcurso,$this->nombre,$this->imagen,$this->descripcion,$this->estado,$this->fecharegistro,$this->idusuario,$this->vinculosaprendizajes,$this->materialesyrecursos,$this->color);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatAcad_curso->cambiarvalorcampo($this->idcurso,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_curso', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_curso->eliminar($this->idcurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcurso($pk){
		try {
			$this->dataAcad_curso = $this->oDatAcad_curso->get($pk);
			if(empty($this->dataAcad_curso)) {
				throw new Exception(JrTexto::_("Acad_curso").' '.JrTexto::_("not registered"));
			}
			$this->idcurso = $this->dataAcad_curso["idcurso"];
			$this->nombre = $this->dataAcad_curso["nombre"];
			$this->imagen = $this->dataAcad_curso["imagen"];
			$this->descripcion = $this->dataAcad_curso["descripcion"];
			$this->estado = $this->dataAcad_curso["estado"];
			$this->fecharegistro = $this->dataAcad_curso["fecharegistro"];
			$this->idusuario = $this->dataAcad_curso["idusuario"];
			$this->vinculosaprendizajes = $this->dataAcad_curso["vinculosaprendizajes"];
			$this->materialesyrecursos = $this->dataAcad_curso["materialesyrecursos"];
			$this->color = $this->dataAcad_curso["color"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_curso', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_curso = $this->oDatAcad_curso->get($pk);
			if(empty($this->dataAcad_curso)) {
				throw new Exception(JrTexto::_("Acad_curso").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_curso->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}