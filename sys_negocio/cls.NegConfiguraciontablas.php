<?php
/**
 * @autor		Sistec Peru
 */

defined('RUTA_BASE') or die();

JrCargador::clase('sys_negocio::NegBase', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_datos::DatConfiguraciontablas', RUTA_BASE, 'sys_datos');

class NegConfiguraciontablas extends NegBase
{
	protected $nivel;
	protected $ventana;
	protected $atributo;
	protected $valor;
	
	protected $dataConfiguraciontablas;
	protected $oDatconfiguraciontablas;
	
	public function __construct()
	{
		$this->oDatconfiguraciontablas = new Datconfiguraciontablas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatconfiguraciontablas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatconfiguraciontablas->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatconfiguraciontablas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}