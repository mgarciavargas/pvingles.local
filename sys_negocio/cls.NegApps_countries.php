<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatApps_countries', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegApps_countries 
{
	protected $id_pais;
	protected $country_code;
	protected $country_name;
	
	protected $dataApps_countries;
	protected $oDatApps_countries;	

	public function __construct()
	{
		$this->oDatApps_countries = new DatApps_countries;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatApps_countries->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatApps_countries->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatApps_countries->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatApps_countries->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatApps_countries->get($this->id_pais);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('apps_countries', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatApps_countries->iniciarTransaccion('neg_i_Apps_countries');
			$this->id_pais = $this->oDatApps_countries->insertar($this->country_code,$this->country_name);
			//$this->oDatApps_countries->terminarTransaccion('neg_i_Apps_countries');	
			return $this->id_pais;
		} catch(Exception $e) {	
		   //$this->oDatApps_countries->cancelarTransaccion('neg_i_Apps_countries');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('apps_countries', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatApps_countries->actualizar($this->id_pais,$this->country_code,$this->country_name);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Apps_countries', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatApps_countries->eliminar($this->id_pais);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_pais($pk){
		try {
			$this->dataApps_countries = $this->oDatApps_countries->get($pk);
			if(empty($this->dataApps_countries)) {
				throw new Exception(JrTexto::_("Apps_countries").' '.JrTexto::_("not registered"));
			}
			$this->id_pais = $this->dataApps_countries["id_pais"];
			$this->country_code = $this->dataApps_countries["country_code"];
			$this->country_name = $this->dataApps_countries["country_name"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('apps_countries', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataApps_countries = $this->oDatApps_countries->get($pk);
			if(empty($this->dataApps_countries)) {
				throw new Exception(JrTexto::_("Apps_countries").' '.JrTexto::_("not registered"));
			}

			return $this->oDatApps_countries->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
		
}