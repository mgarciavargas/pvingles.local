<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		23-02-2018
 * @copyright	Copyright (C) 23-02-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBitacora_alumno_smartbook', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBitacora_alumno_smartbook 
{
	protected $idbitacora_smartbook;
	protected $idcurso;
	protected $idsesion;
	protected $idusuario;
	protected $estado;
	protected $regfecha;
	
	protected $dataBitacora_alumno_smartbook;
	protected $oDatBitacora_alumno_smartbook;	

	public function __construct()
	{
		$this->oDatBitacora_alumno_smartbook = new DatBitacora_alumno_smartbook;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBitacora_alumno_smartbook->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBitacora_alumno_smartbook->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBitacora_alumno_smartbook->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBitacora_alumno_smartbook->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBitacora_alumno_smartbook->get($this->idbitacora_smartbook);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bitacora_alumno_smartbook', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatBitacora_alumno_smartbook->iniciarTransaccion('neg_i_Bitacora_alumno_smartbook');
			$this->idbitacora_smartbook = $this->oDatBitacora_alumno_smartbook->insertar($this->idcurso,$this->idsesion,$this->idusuario,$this->estado,$this->regfecha);
			$this->oDatBitacora_alumno_smartbook->terminarTransaccion('neg_i_Bitacora_alumno_smartbook');	
			return $this->idbitacora_smartbook;
		} catch(Exception $e) {	
		    $this->oDatBitacora_alumno_smartbook->cancelarTransaccion('neg_i_Bitacora_alumno_smartbook');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bitacora_alumno_smartbook', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatBitacora_alumno_smartbook->actualizar($this->idbitacora_smartbook,$this->idcurso,$this->idsesion,$this->idusuario,$this->estado,$this->regfecha);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bitacora_alumno_smartbook', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBitacora_alumno_smartbook->eliminar($this->idbitacora_smartbook);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdbitacora_smartbook($pk){
		try {
			$this->dataBitacora_alumno_smartbook = $this->oDatBitacora_alumno_smartbook->get($pk);
			if(empty($this->dataBitacora_alumno_smartbook)) {
				throw new Exception(JrTexto::_("Bitacora_alumno_smartbook").' '.JrTexto::_("not registered"));
			}
			$this->idbitacora_smartbook = $this->dataBitacora_alumno_smartbook["idbitacora_smartbook"];
			$this->idcurso = $this->dataBitacora_alumno_smartbook["idcurso"];
			$this->idsesion = $this->dataBitacora_alumno_smartbook["idsesion"];
			$this->idusuario = $this->dataBitacora_alumno_smartbook["idusuario"];
			$this->estado = $this->dataBitacora_alumno_smartbook["estado"];
			$this->regfecha = $this->dataBitacora_alumno_smartbook["regfecha"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bitacora_alumno_smartbook', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBitacora_alumno_smartbook = $this->oDatBitacora_alumno_smartbook->get($pk);
			if(empty($this->dataBitacora_alumno_smartbook)) {
				throw new Exception(JrTexto::_("Bitacora_alumno_smartbook").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBitacora_alumno_smartbook->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}