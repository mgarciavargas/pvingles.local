<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		27-11-2017
 * @copyright	Copyright (C) 27-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNota_detalle', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegNota_detalle 
{
	protected $idnota_detalle;
	protected $idcurso;
	protected $idmatricula;
	protected $idalumno;
	protected $iddocente;
	protected $idcursodetalle;
	protected $nota;
	protected $tipo;
	protected $observacion;
	protected $fecharegistro;
	protected $estado;
	
	protected $dataNota_detalle;
	protected $oDatNota_detalle;	

	public function __construct()
	{
		$this->oDatNota_detalle = new DatNota_detalle;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatNota_detalle->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatNota_detalle->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatNota_detalle->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatNota_detalle->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatNota_detalle->get($this->idnota_detalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('nota_detalle', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatNota_detalle->iniciarTransaccion('neg_i_Nota_detalle');
			$this->idnota_detalle = $this->oDatNota_detalle->insertar($this->idcurso,$this->idmatricula,$this->idalumno,$this->iddocente,$this->idcursodetalle,$this->nota,$this->tipo,$this->observacion,$this->fecharegistro,$this->estado);
			$this->oDatNota_detalle->terminarTransaccion('neg_i_Nota_detalle');	
			return $this->idnota_detalle;
		} catch(Exception $e) {	
		    $this->oDatNota_detalle->cancelarTransaccion('neg_i_Nota_detalle');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('nota_detalle', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatNota_detalle->actualizar($this->idnota_detalle,$this->idcurso,$this->idmatricula,$this->idalumno,$this->iddocente,$this->idcursodetalle,$this->nota,$this->tipo,$this->observacion,$this->fecharegistro,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Nota_detalle', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatNota_detalle->eliminar($this->idnota_detalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdnota_detalle($pk){
		try {
			$this->dataNota_detalle = $this->oDatNota_detalle->get($pk);
			if(empty($this->dataNota_detalle)) {
				throw new Exception(JrTexto::_("Nota_detalle").' '.JrTexto::_("not registered"));
			}
			$this->idnota_detalle = $this->dataNota_detalle["idnota_detalle"];
			$this->idcurso = $this->dataNota_detalle["idcurso"];
			$this->idmatricula = $this->dataNota_detalle["idmatricula"];
			$this->idalumno = $this->dataNota_detalle["idalumno"];
			$this->iddocente = $this->dataNota_detalle["iddocente"];
			$this->idcursodetalle = $this->dataNota_detalle["idcursodetalle"];
			$this->nota = $this->dataNota_detalle["nota"];
			$this->tipo = $this->dataNota_detalle["tipo"];
			$this->observacion = $this->dataNota_detalle["observacion"];
			$this->fecharegistro = $this->dataNota_detalle["fecharegistro"];
			$this->estado = $this->dataNota_detalle["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('nota_detalle', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataNota_detalle = $this->oDatNota_detalle->get($pk);
			if(empty($this->dataNota_detalle)) {
				throw new Exception(JrTexto::_("Nota_detalle").' '.JrTexto::_("not registered"));
			}

			return $this->oDatNota_detalle->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}