<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_det_estudio_autor', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBib_det_estudio_autor 
{
	protected $id_detalle;
	protected $id_estudio;
	protected $id_autor;
	
	protected $dataBib_det_estudio_autor;
	protected $oDatBib_det_estudio_autor;	

	public function __construct()
	{
		$this->oDatBib_det_estudio_autor = new DatBib_det_estudio_autor;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_det_estudio_autor->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_det_estudio_autor->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_det_estudio_autor->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBib_det_estudio_autor->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_det_estudio_autor->get($this->id_detalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_det_estudio_autor', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_det_estudio_autor->iniciarTransaccion('neg_i_Bib_det_estudio_autor');
			$this->id_detalle = $this->oDatBib_det_estudio_autor->insertar($this->id_estudio,$this->id_autor);
			//$this->oDatBib_det_estudio_autor->terminarTransaccion('neg_i_Bib_det_estudio_autor');	
			return $this->id_detalle;
		} catch(Exception $e) {	
		   //$this->oDatBib_det_estudio_autor->cancelarTransaccion('neg_i_Bib_det_estudio_autor');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_det_estudio_autor', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatBib_det_estudio_autor->actualizar($this->id_detalle,$this->id_estudio,$this->id_autor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_det_estudio_autor', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_det_estudio_autor->eliminar($this->id_detalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_detalle($pk){
		try {
			$this->dataBib_det_estudio_autor = $this->oDatBib_det_estudio_autor->get($pk);
			if(empty($this->dataBib_det_estudio_autor)) {
				throw new Exception(JrTexto::_("Bib_det_estudio_autor").' '.JrTexto::_("not registered"));
			}
			$this->id_detalle = $this->dataBib_det_estudio_autor["id_detalle"];
			$this->id_estudio = $this->dataBib_det_estudio_autor["id_estudio"];
			$this->id_autor = $this->dataBib_det_estudio_autor["id_autor"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_det_estudio_autor', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_det_estudio_autor = $this->oDatBib_det_estudio_autor->get($pk);
			if(empty($this->dataBib_det_estudio_autor)) {
				throw new Exception(JrTexto::_("Bib_det_estudio_autor").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_det_estudio_autor->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}