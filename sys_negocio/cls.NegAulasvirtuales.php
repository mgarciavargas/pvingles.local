<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		22-05-2017
 * @copyright	Copyright (C) 22-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAulasvirtuales', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAulasvirtuales 
{
	protected $aulaid;
	protected $idnivel;
	protected $idunidad;
	protected $idactividad;
	protected $fecha_inicio;
	protected $fecha_final;
	protected $titulo;
	protected $descripcion;
	protected $moderadores;
	protected $estado;
	protected $video;
	protected $chat;
	protected $notas;
	protected $dirigidoa;
	protected $estudiantes;
	protected $dni;
	protected $fecha_creado;
	protected $portada;
	protected $filtroestudiantes;
	protected  $nparticipantes;
	
	protected $dataAulasvirtuales;
	protected $oDatAulasvirtuales;	

	public function __construct()
	{
		$this->oDatAulasvirtuales = new DatAulasvirtuales;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAulasvirtuales->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAulasvirtuales->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAulasvirtuales->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAulasvirtuales->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAulasvirtuales->get($this->aulaid);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$this->aulaid = $this->oDatAulasvirtuales->insertar($this->idnivel,$this->idunidad,$this->idactividad,$this->fecha_inicio,$this->fecha_final,$this->titulo,$this->descripcion,$this->moderadores,$this->estado,$this->video,$this->chat,$this->notas,$this->dirigidoa,$this->estudiantes,$this->dni,$this->fecha_creado,$this->portada, $this->filtroestudiantes,$this->nparticipantes);				
			return $this->aulaid;
		} catch(Exception $e) {	
		   //$this->oDatAulasvirtuales->cancelarTransaccion('neg_i_Aulasvirtuales');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('aulasvirtuales', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAulasvirtuales->actualizar($this->aulaid,$this->idnivel,$this->idunidad,$this->idactividad,$this->fecha_inicio,$this->fecha_final,$this->titulo,$this->descripcion,$this->moderadores,$this->estado,$this->video,$this->chat,$this->notas,$this->dirigidoa,$this->estudiantes,$this->dni,$this->fecha_creado,$this->portada, $this->filtroestudiantes,$this->nparticipantes);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Aulasvirtuales', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAulasvirtuales->eliminar($this->aulaid);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setAulaid($pk){
		try {
			$this->dataAulasvirtuales = $this->oDatAulasvirtuales->get($pk);
			if(empty($this->dataAulasvirtuales)) {
				throw new Exception(JrTexto::_("Aulasvirtuales").' '.JrTexto::_("not registered"));
			}
			$this->aulaid = $this->dataAulasvirtuales["aulaid"];
			$this->idnivel = $this->dataAulasvirtuales["idnivel"];
			$this->idunidad = $this->dataAulasvirtuales["idunidad"];
			$this->idactividad = $this->dataAulasvirtuales["idactividad"];
			$this->fecha_inicio = $this->dataAulasvirtuales["fecha_inicio"];
			$this->fecha_final = $this->dataAulasvirtuales["fecha_final"];
			$this->titulo = $this->dataAulasvirtuales["titulo"];
			$this->descripcion = $this->dataAulasvirtuales["descripcion"];
			$this->moderadores = $this->dataAulasvirtuales["moderadores"];
			$this->estado = $this->dataAulasvirtuales["estado"];
			$this->video = $this->dataAulasvirtuales["video"];
			$this->chat = $this->dataAulasvirtuales["chat"];
			$this->notas = $this->dataAulasvirtuales["notas"];
			$this->dirigidoa = $this->dataAulasvirtuales["dirigidoa"];
			$this->estudiantes = $this->dataAulasvirtuales["estudiantes"];
			$this->dni = $this->dataAulasvirtuales["dni"];
			$this->fecha_creado = $this->dataAulasvirtuales["fecha_creado"];
			$this->portada = $this->dataAulasvirtuales["portada"];
			$this->filtroestudiantes = $this->dataAulasvirtuales["filtroestudiantes"];
			$this->nparticipantes= $this->dataAulasvirtuales["nparticipantes"];
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('aulasvirtuales', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAulasvirtuales = $this->oDatAulasvirtuales->get($pk);
			if(empty($this->dataAulasvirtuales)) {
				throw new Exception(JrTexto::_("Aulasvirtuales").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAulasvirtuales->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	
}