<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		31-05-2017
 * @copyright	Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTarea_archivos', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegTarea_archivos 
{
	protected $idtarea_archivos;
	protected $tablapadre;
	protected $idpadre;
	protected $nombre;
	protected $ruta;
	protected $tipo;
	protected $puntaje;
	protected $habilidad;
	public $texto;
	protected $idtarea_archivos_padre;
	
	protected $dataTarea_archivos;
	protected $oDatTarea_archivos;	

	public function __construct()
	{
		$this->oDatTarea_archivos = new DatTarea_archivos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTarea_archivos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatTarea_archivos->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTarea_archivos->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTarea_archivos->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTarea_archivos->get($this->idtarea_archivos);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$usuarioAct = NegSesion::getUsuario();
			if(!NegSesion::tiene_acceso('tarea_archivos', 'add') && $usuarioAct['rol']!='Alumno') {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatTarea_archivos->iniciarTransaccion('neg_i_Tarea_archivos');
			$this->idtarea_archivos = $this->oDatTarea_archivos->insertar($this->tablapadre,$this->idpadre,$this->nombre,$this->ruta,$this->tipo,$this->puntaje,$this->habilidad,$this->texto,$this->idtarea_archivos_padre);
			//$this->oDatTarea_archivos->terminarTransaccion('neg_i_Tarea_archivos');	
			return $this->idtarea_archivos;
		} catch(Exception $e) {	
		   //$this->oDatTarea_archivos->cancelarTransaccion('neg_i_Tarea_archivos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			$usuarioAct = NegSesion::getUsuario();
			if(!NegSesion::tiene_acceso('tarea_archivos', 'edit') && $usuarioAct['rol']!='Alumno') {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatTarea_archivos->actualizar($this->idtarea_archivos,$this->tablapadre,$this->idpadre,$this->nombre,$this->ruta,$this->tipo,$this->puntaje,$this->habilidad,$this->texto,$this->idtarea_archivos_padre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			$usuarioAct = NegSesion::getUsuario();
			if(!NegSesion::tiene_acceso('Tarea_archivos', 'delete') && $usuarioAct['rol']!='Alumno') {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatTarea_archivos->eliminar($this->idtarea_archivos);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtarea_archivos($pk){
		try {
			$this->dataTarea_archivos = $this->oDatTarea_archivos->get($pk);
			if(empty($this->dataTarea_archivos)) {
				throw new Exception(JrTexto::_("Tarea_archivos").' '.JrTexto::_("not registered"));
			}
			$this->idtarea_archivos = $this->dataTarea_archivos["idtarea_archivos"];
			$this->tablapadre = $this->dataTarea_archivos["tablapadre"];
			$this->idpadre = $this->dataTarea_archivos["idpadre"];
			$this->nombre = $this->dataTarea_archivos["nombre"];
			$this->ruta = $this->dataTarea_archivos["ruta"];
			$this->tipo = $this->dataTarea_archivos["tipo"];
			$this->puntaje = $this->dataTarea_archivos["puntaje"];
			$this->habilidad = $this->dataTarea_archivos["habilidad"];
			$this->texto = $this->dataTarea_archivos["texto"];
			$this->idtarea_archivos_padre = $this->dataTarea_archivos["idtarea_archivos_padre"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('tarea_archivos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataTarea_archivos = $this->oDatTarea_archivos->get($pk);
			if(empty($this->dataTarea_archivos)) {
				throw new Exception(JrTexto::_("Tarea_archivos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTarea_archivos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setTablapadre($tablapadre)
	{
		try {
			$this->tablapadre= NegTools::validar('todo', $tablapadre, false, JrTexto::_("Please enter a valid value"), array("longmax" => 1));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdpadre($idpadre)
	{
		try {
			$this->idpadre= NegTools::validar('todo', $idpadre, false, JrTexto::_("Please enter a valid value"), array("longmax" => 11));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setNombre($nombre)
	{
		try {
			$this->nombre= NegTools::validar('todo', $nombre, false, JrTexto::_("Please enter a valid value"), array("longmax" => 256));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setRuta($ruta)
	{
		try {
			$this->ruta= NegTools::validar('todo', $ruta, false, JrTexto::_("Please enter a valid value"), array("longmax" => 999999999));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setTipo($tipo)
	{
		try {
			$this->tipo= NegTools::validar('todo', $tipo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 1));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setPuntaje($puntaje)
	{
		try {
			$this->puntaje= NegTools::validar('todo', $puntaje, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setHabilidad($habilidad)
	{
		try {
			$this->habilidad= NegTools::validar('todo', $habilidad, false, JrTexto::_("Please enter a valid value"), array("longmax" => 999999999));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	/*private function setTexto($texto)
	{
		try {
			$this->texto= NegTools::validar('todo', $texto, false, JrTexto::_("Please enter a valid value"), array("longmax" => 999999999));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}*/
	private function setIdtarea_archivos_padre($idtarea_archivos_padre)
	{
		try {
			$this->idtarea_archivos_padre= NegTools::validar('todo', $idtarea_archivos_padre, false, JrTexto::_("Please enter a valid value"), array("longmax" => 11));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}