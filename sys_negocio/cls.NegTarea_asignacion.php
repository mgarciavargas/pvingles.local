<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		15-06-2017
 * @copyright	Copyright (C) 15-06-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTarea_asignacion', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatTarea_asignacion_alumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegTarea_asignacion 
{
	protected $idtarea_asignacion;
	protected $idtarea;
	protected $idgrupo;
	protected $iddocente;
	protected $fechaentrega;
	protected $horaentrega;
	protected $eliminado=0;
	
	protected $dataTarea_asignacion;
	protected $oDatTarea_asignacion;	
	protected $oDatTarea_asignacion_alumno;	

	public function __construct()
	{
		$this->oDatTarea_asignacion = new DatTarea_asignacion;
		$this->oDatTarea_asignacion_alumno = new DatTarea_asignacion_alumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTarea_asignacion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatTarea_asignacion->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTarea_asignacion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarConDetalle($filtros = array(), $filtroDetalle = array())
	{
		try {
			$arrTarea_Asignacion = $this->oDatTarea_asignacion->buscar($filtros);
			$x=0;
			foreach ($arrTarea_Asignacion as $ta) {
				$filt = array('idtarea_asignacion'=>$ta['idtarea_asignacion']);
				$filtros = array_merge($filt, $filtroDetalle);
				$arrTarea_Asignacion[$x]['detalle']=$this->oDatTarea_asignacion_alumno->buscar($filtros);
				$x++;
			}
			return $arrTarea_Asignacion;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTarea_asignacion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTarea_asignacion->get($this->idtarea_asignacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tarea_asignacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatTarea_asignacion->iniciarTransaccion('neg_i_Tarea_asignacion');
			$this->idtarea_asignacion = $this->oDatTarea_asignacion->insertar($this->idtarea,$this->idgrupo,$this->iddocente,$this->fechaentrega,$this->horaentrega,$this->eliminado);
			//$this->oDatTarea_asignacion->terminarTransaccion('neg_i_Tarea_asignacion');	
			return $this->idtarea_asignacion;
		} catch(Exception $e) {	
		   //$this->oDatTarea_asignacion->cancelarTransaccion('neg_i_Tarea_asignacion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('tarea_asignacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatTarea_asignacion->actualizar($this->idtarea_asignacion,$this->idtarea,$this->idgrupo,$this->iddocente,$this->fechaentrega,$this->horaentrega,$this->eliminado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Tarea_asignacion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatTarea_asignacion->eliminar($this->idtarea_asignacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtarea_asignacion($pk){
		try {
			$this->dataTarea_asignacion = $this->oDatTarea_asignacion->get($pk);
			if(empty($this->dataTarea_asignacion)) {
				throw new Exception(JrTexto::_("Tarea_asignacion").' '.JrTexto::_("not registered"));
			}
			$this->idtarea_asignacion = $this->dataTarea_asignacion["idtarea_asignacion"];
			$this->idtarea = $this->dataTarea_asignacion["idtarea"];
			$this->idgrupo = $this->dataTarea_asignacion["idgrupo"];
			$this->iddocente = $this->dataTarea_asignacion["iddocente"];
			$this->fechaentrega = $this->dataTarea_asignacion["fechaentrega"];
			$this->horaentrega = $this->dataTarea_asignacion["horaentrega"];
			$this->eliminado = $this->dataTarea_asignacion["eliminado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('tarea_asignacion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataTarea_asignacion = $this->oDatTarea_asignacion->get($pk);
			if(empty($this->dataTarea_asignacion)) {
				throw new Exception(JrTexto::_("Tarea_asignacion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTarea_asignacion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
/*
	private function setIdtarea($idtarea)
	{
		try {
			$this->idtarea= NegTools::validar('todo', $idtarea, false, JrTexto::_("Please enter a valid value"), array("longmax" => 11));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdgrupo($idgrupo)
	{
		try {
			$this->idgrupo= NegTools::validar('todo', $idgrupo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 11));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setFechaentrega($fechaentrega)
	{
		try {
			$this->fechaentrega= NegTools::validar('todo', $fechaentrega, false, JrTexto::_("Please enter a valid value"), array("longmax" => 10));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setHoraentrega($horaentrega)
	{
		try {
			$this->horaentrega= NegTools::validar('todo', $horaentrega, false, JrTexto::_("Please enter a valid value"), array("longmax" => 8));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setEliminado($eliminado)
	{
		try {
			$this->eliminado= NegTools::validar('todo', $eliminado, false, JrTexto::_("Please enter a valid value"), array("longmax" => 1));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
*/	
}