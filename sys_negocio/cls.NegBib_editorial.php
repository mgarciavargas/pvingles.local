<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_editorial', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBib_editorial 
{
	protected $id_editorial;
	protected $nombre;
	
	protected $dataBib_editorial;
	protected $oDatBib_editorial;	

	public function __construct()
	{
		$this->oDatBib_editorial = new DatBib_editorial;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_editorial->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_editorial->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_editorial->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBib_editorial->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_editorial->get($this->id_editorial);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_editorial', 'add')) {
                throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_editorial->iniciarTransaccion('neg_i_Bib_editorial');
			$this->id_editorial = $this->oDatBib_editorial->insertar($this->nombre);
			//$this->oDatBib_editorial->terminarTransaccion('neg_i_Bib_editorial');	
			return $this->id_editorial;
		} catch(Exception $e) {	
		   //$this->oDatBib_editorial->cancelarTransaccion('neg_i_Bib_editorial');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_editorial', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatBib_editorial->actualizar($this->id_editorial,$this->nombre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_editorial', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_editorial->eliminar($this->id_editorial);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_editorial($pk){
		try {
			$this->dataBib_editorial = $this->oDatBib_editorial->get($pk);
			if(empty($this->dataBib_editorial)) {
				throw new Exception(JrTexto::_("Bib_editorial").' '.JrTexto::_("not registered"));
			}
			$this->id_editorial = $this->dataBib_editorial["id_editorial"];
			$this->nombre = $this->dataBib_editorial["nombre"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_editorial', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_editorial = $this->oDatBib_editorial->get($pk);
			if(empty($this->dataBib_editorial)) {
				throw new Exception(JrTexto::_("Bib_editorial").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_editorial->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}	
}