<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_tipo', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBib_tipo 
{
	protected $id_tipo;
	protected $nombre;
	protected $filtro;
	protected $extension;
	protected $id_registro;
	
	protected $dataBib_tipo;
	protected $oDatBib_tipo;	

	public function __construct()
	{
		$this->oDatBib_tipo = new DatBib_tipo;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_tipo->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_tipo->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_tipo->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBib_tipo->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_tipo->get($this->id_tipo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_tipo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_tipo->iniciarTransaccion('neg_i_Bib_tipo');
			$this->id_tipo = $this->oDatBib_tipo->insertar($this->nombre,$this->filtro,$this->extension,$this->id_registro);
			//$this->oDatBib_tipo->terminarTransaccion('neg_i_Bib_tipo');	
			return $this->id_tipo;
		} catch(Exception $e) {	
		   //$this->oDatBib_tipo->cancelarTransaccion('neg_i_Bib_tipo');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_tipo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatBib_tipo->actualizar($this->id_tipo,$this->nombre,$this->filtro,$this->extension,$this->id_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_tipo', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_tipo->eliminar($this->id_tipo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_tipo($pk){
		try {
			$this->dataBib_tipo = $this->oDatBib_tipo->get($pk);
			if(empty($this->dataBib_tipo)) {
				throw new Exception(JrTexto::_("Bib_tipo").' '.JrTexto::_("not registered"));
			}
			$this->id_tipo = $this->dataBib_tipo["id_tipo"];
			$this->nombre = $this->dataBib_tipo["nombre"];
			$this->filtro = $this->dataBib_tipo["filtro"];
			$this->extension = $this->dataBib_tipo["extension"];
			$this->id_registro = $this->dataBib_tipo["id_registro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_tipo', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_tipo = $this->oDatBib_tipo->get($pk);
			if(empty($this->dataBib_tipo)) {
				throw new Exception(JrTexto::_("Bib_tipo").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_tipo->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
		
}