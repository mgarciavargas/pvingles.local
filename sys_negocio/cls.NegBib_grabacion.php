<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		31-05-2017
 * @copyright	Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_grabacion', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBib_grabacion 
{
	protected $id_grabacion;
	protected $nombre;
	protected $ruta;
	
	protected $databib_grabacion;
	protected $oDatbib_grabacion;	

	public function __construct()
	{
		$this->oDatbib_grabacion = new Datbib_grabacion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatbib_grabacion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatbib_grabacion->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatbib_grabacion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatbib_grabacion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatbib_grabacion->get($this->id_grabacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_grabacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatbib_grabacion->iniciarTransaccion('neg_i_bib_grabacion');
			$this->id_grabacion = $this->oDatbib_grabacion->insertar($this->nombre,$this->ruta);
			//$this->oDatbib_grabacion->terminarTransaccion('neg_i_bib_grabacion');	
			return $this->id_grabacion;
		} catch(Exception $e) {	
		   //$this->oDatbib_grabacion->cancelarTransaccion('neg_i_bib_grabacion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_grabacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			*/
			return $this->oDatbib_grabacion->actualizar($this->id_grabacion,$this->nombre,$this->ruta);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatbib_grabacion->cambiarvalorcampo($this->id_grabacion,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar2($id)
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_grabacion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatbib_grabacion->eliminar2($id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}	

	/*public function eliminar_logica()
	{
		try {
			if(!NegSesion::tiene_acceso('bib_grabacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->eliminado = 1;
			return $this->oDatbib_grabacion->actualizar($this->id_grabacion,$this->idnivel,$this->idunidad,$this->idactividad,$this->iddocente,$this->nombre,$this->ruta,$this->foto,$this->habilidades,$this->puntajemaximo,$this->puntajeminimo,$this->estado,$this->eliminado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}*/

	public function setId_grabacion($pk){
		try {
			$this->databib_grabacion = $this->oDatbib_grabacion->get($pk);
			if(empty($this->databib_grabacion)) {
				throw new Exception(JrTexto::_("bib_grabacion").' '.JrTexto::_("not registered"));
			}
			$this->id_grabacion = $this->databib_grabacion["id_grabacion"];
			$this->nombre = $this->databib_grabacion["nombre"];
			$this->ruta = $this->databib_grabacion["ruta"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_grabacion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->databib_grabacion = $this->oDatbib_grabacion->get($pk);
			if(empty($this->databib_grabacion)) {
				throw new Exception(JrTexto::_("bib_grabacion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatbib_grabacion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}

	private function setNombre($nombre)
	{
		try {
			$this->nombre= NegTools::validar('todo', $nombre, false, JrTexto::_("Please enter a valid value"), array("longmax" => 100));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	/*private function setRuta($ruta)
	{
		try {
			$this->ruta= NegTools::validar('todo', $ruta, false, JrTexto::_("Please enter a valid value"), array("longmax" => 999999999));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}*/
		
}