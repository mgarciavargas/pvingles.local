<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		18-04-2017
 * @copyright	Copyright (C) 18-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatDetalle_matricula_alumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegDetalle_matricula_alumno 
{
	protected $iddetalle;
	protected $idmatricula;
	protected $idejercicio;
	protected $orden;
	protected $estado;
	
	protected $dataDetalle_matricula_alumno;
	protected $oDatDetalle_matricula_alumno;	

	public function __construct()
	{
		$this->oDatDetalle_matricula_alumno = new DatDetalle_matricula_alumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatDetalle_matricula_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatDetalle_matricula_alumno->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatDetalle_matricula_alumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatDetalle_matricula_alumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatDetalle_matricula_alumno->get($this->iddetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('detalle_matricula_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatDetalle_matricula_alumno->iniciarTransaccion('neg_i_Detalle_matricula_alumno');
			$this->iddetalle = $this->oDatDetalle_matricula_alumno->insertar($this->idmatricula,$this->idejercicio,$this->orden,$this->estado);
			//$this->oDatDetalle_matricula_alumno->terminarTransaccion('neg_i_Detalle_matricula_alumno');	
			return $this->iddetalle;
		} catch(Exception $e) {	
		   //$this->oDatDetalle_matricula_alumno->cancelarTransaccion('neg_i_Detalle_matricula_alumno');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('detalle_matricula_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatDetalle_matricula_alumno->actualizar($this->iddetalle,$this->idmatricula,$this->idejercicio,$this->orden,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Detalle_matricula_alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatDetalle_matricula_alumno->eliminar($this->iddetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIddetalle($pk){
		try {
			$this->dataDetalle_matricula_alumno = $this->oDatDetalle_matricula_alumno->get($pk);
			if(empty($this->dataDetalle_matricula_alumno)) {
				throw new Exception(JrTexto::_("Detalle_matricula_alumno").' '.JrTexto::_("not registered"));
			}
			$this->iddetalle = $this->dataDetalle_matricula_alumno["iddetalle"];
			$this->idmatricula = $this->dataDetalle_matricula_alumno["idmatricula"];
			$this->idejercicio = $this->dataDetalle_matricula_alumno["idejercicio"];
			$this->orden = $this->dataDetalle_matricula_alumno["orden"];
			$this->estado = $this->dataDetalle_matricula_alumno["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('detalle_matricula_alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataDetalle_matricula_alumno = $this->oDatDetalle_matricula_alumno->get($pk);
			if(empty($this->dataDetalle_matricula_alumno)) {
				throw new Exception(JrTexto::_("Detalle_matricula_alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatDetalle_matricula_alumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setIdmatricula($idmatricula)
	{
		try {
			$this->idmatricula= NegTools::validar('todo', $idmatricula, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdejercicio($idejercicio)
	{
		try {
			$this->idejercicio= NegTools::validar('todo', $idejercicio, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setOrden($orden)
	{
		try {
			$this->orden= NegTools::validar('todo', $orden, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setEstado($estado)
	{
		try {
			$this->estado= NegTools::validar('todo', $estado, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}