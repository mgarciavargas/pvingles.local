<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-01-2018
 * @copyright	Copyright (C) 17-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatMaterial_ayuda', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegMaterial_ayuda 
{
	protected $idmaterial;
	protected $nombre;
	protected $archivo;
	protected $tipo;
	protected $descripcion;
	protected $mostrar;
	
	protected $dataMaterial_ayuda;
	protected $oDatMaterial_ayuda;	

	public function __construct()
	{
		$this->oDatMaterial_ayuda = new DatMaterial_ayuda;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMaterial_ayuda->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatMaterial_ayuda->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatMaterial_ayuda->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatMaterial_ayuda->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatMaterial_ayuda->get($this->idmaterial);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('material_ayuda', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatMaterial_ayuda->iniciarTransaccion('neg_i_Material_ayuda');
			$this->idmaterial = $this->oDatMaterial_ayuda->insertar($this->nombre,$this->archivo,$this->tipo,$this->descripcion,$this->mostrar);
			$this->oDatMaterial_ayuda->terminarTransaccion('neg_i_Material_ayuda');	
			return $this->idmaterial;
		} catch(Exception $e) {	
		    $this->oDatMaterial_ayuda->cancelarTransaccion('neg_i_Material_ayuda');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('material_ayuda', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatMaterial_ayuda->actualizar($this->idmaterial,$this->nombre,$this->archivo,$this->tipo,$this->descripcion,$this->mostrar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatMaterial_ayuda->cambiarvalorcampo($this->idmaterial,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Material_ayuda', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatMaterial_ayuda->eliminar($this->idmaterial);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmaterial($pk){
		try {
			$this->dataMaterial_ayuda = $this->oDatMaterial_ayuda->get($pk);
			if(empty($this->dataMaterial_ayuda)) {
				throw new Exception(JrTexto::_("Material_ayuda").' '.JrTexto::_("not registered"));
			}
			$this->idmaterial = $this->dataMaterial_ayuda["idmaterial"];
			$this->nombre = $this->dataMaterial_ayuda["nombre"];
			$this->archivo = $this->dataMaterial_ayuda["archivo"];
			$this->tipo = $this->dataMaterial_ayuda["tipo"];
			$this->descripcion = $this->dataMaterial_ayuda["descripcion"];
			$this->mostrar = $this->dataMaterial_ayuda["mostrar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('material_ayuda', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataMaterial_ayuda = $this->oDatMaterial_ayuda->get($pk);
			if(empty($this->dataMaterial_ayuda)) {
				throw new Exception(JrTexto::_("Material_ayuda").' '.JrTexto::_("not registered"));
			}

			return $this->oDatMaterial_ayuda->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}