<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		29-12-2017
 * @copyright	Copyright (C) 29-12-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAmbiente', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAmbiente 
{
	protected $idambiente;
	protected $idlocal;
	protected $numero;
	protected $capacidad;
	protected $tipo;
	protected $estado;
	protected $turno;
	
	protected $dataAmbiente;
	protected $oDatAmbiente;	

	public function __construct()
	{
		$this->oDatAmbiente = new DatAmbiente;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAmbiente->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAmbiente->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAmbiente->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getMisLocales($filtros = array())
	{
		try {
			return $this->oDatAmbiente->getMisLocales($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAmbiente->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAmbiente->get($this->idambiente);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$this->oDatAmbiente->iniciarTransaccion('neg_i_Ambiente');
			$this->idambiente = $this->oDatAmbiente->insertar($this->idlocal,$this->numero,$this->capacidad,$this->tipo,$this->estado,$this->turno);
			$this->oDatAmbiente->terminarTransaccion('neg_i_Ambiente');	
			return $this->idambiente;
		} catch(Exception $e) {	
		    $this->oDatAmbiente->cancelarTransaccion('neg_i_Ambiente');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {	
			return $this->oDatAmbiente->actualizar($this->idambiente,$this->idlocal,$this->numero,$this->capacidad,$this->tipo,$this->estado,$this->turno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatAmbiente->cambiarvalorcampo($this->idambiente,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			return $this->oDatAmbiente->eliminar($this->idambiente);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdambiente($pk){
		try {
			$this->dataAmbiente = $this->oDatAmbiente->get($pk);
			if(empty($this->dataAmbiente)) {
				throw new Exception(JrTexto::_("Ambiente").' '.JrTexto::_("not registered"));
			}
			$this->idambiente = $this->dataAmbiente["idambiente"];
			$this->idlocal = $this->dataAmbiente["idlocal"];
			$this->numero = $this->dataAmbiente["numero"];
			$this->capacidad = $this->dataAmbiente["capacidad"];
			$this->tipo = $this->dataAmbiente["tipo"];
			$this->estado = $this->dataAmbiente["estado"];
			$this->turno = $this->dataAmbiente["turno"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('ambiente', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAmbiente = $this->oDatAmbiente->get($pk);
			if(empty($this->dataAmbiente)) {
				throw new Exception(JrTexto::_("Ambiente").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAmbiente->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}