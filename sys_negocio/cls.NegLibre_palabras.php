<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-04-2018
 * @copyright	Copyright (C) 17-04-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatLibre_palabras', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegLibre_palabras 
{
	protected $idpalabra;
	protected $idtema;
	protected $palabra;
	protected $audio;
	protected $idagrupacion;
	protected $url_iframe;
	
	protected $dataLibre_palabras;
	protected $oDatLibre_palabras;	

	public function __construct()
	{
		$this->oDatLibre_palabras = new DatLibre_palabras;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatLibre_palabras->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatLibre_palabras->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatLibre_palabras->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarTable($filtros = array())
	{
		try {
			$data = array();
			$palabras = $this->oDatLibre_palabras->buscar($filtros);
			if(!empty($palabras)) {
				foreach ($palabras as $p) {
					if(!isset($data[ $p['idagrupacion'] ])) {
						$data[ $p['idagrupacion'] ] = array();
					}
					$data[ $p['idagrupacion'] ][] = $p;
				}
			}
			return $data;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatLibre_palabras->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatLibre_palabras->get($this->idpalabra);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('libre_palabras', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatLibre_palabras->iniciarTransaccion('neg_i_Libre_palabras');
			$this->idpalabra = $this->oDatLibre_palabras->insertar($this->idtema,$this->palabra,$this->audio,$this->idagrupacion,$this->url_iframe);
			$this->oDatLibre_palabras->terminarTransaccion('neg_i_Libre_palabras');	
			return $this->idpalabra;
		} catch(Exception $e) {	
		    $this->oDatLibre_palabras->cancelarTransaccion('neg_i_Libre_palabras');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('libre_palabras', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatLibre_palabras->actualizar($this->idpalabra,$this->idtema,$this->palabra,$this->audio,$this->idagrupacion,$this->url_iframe);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Libre_palabras', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatLibre_palabras->eliminar($this->idpalabra);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpalabra($pk){
		try {
			$this->dataLibre_palabras = $this->oDatLibre_palabras->get($pk);
			if(empty($this->dataLibre_palabras)) {
				throw new Exception(JrTexto::_("Libre_palabras").' '.JrTexto::_("not registered"));
			}
			$this->idpalabra = $this->dataLibre_palabras["idpalabra"];
			$this->idtema = $this->dataLibre_palabras["idtema"];
			$this->palabra = $this->dataLibre_palabras["palabra"];
			$this->audio = $this->dataLibre_palabras["audio"];
			$this->idagrupacion = $this->dataLibre_palabras["idagrupacion"];
			$this->url_iframe = $this->dataLibre_palabras["url_iframe"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('libre_palabras', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataLibre_palabras = $this->oDatLibre_palabras->get($pk);
			if(empty($this->dataLibre_palabras)) {
				throw new Exception(JrTexto::_("Libre_palabras").' '.JrTexto::_("not registered"));
			}

			return $this->oDatLibre_palabras->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}