<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		18-07-2017
 * @copyright	Copyright (C) 18-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatGeneral', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegGeneral 
{
	protected $idgeneral;
	protected $codigo;
	protected $nombre;
	protected $tipo_tabla;
	protected $mostrar;
	
	protected $dataGeneral;
	protected $oDatGeneral;	

	public function __construct()
	{
		$this->oDatGeneral = new DatGeneral;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatGeneral->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatGeneral->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatGeneral->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatGeneral->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatGeneral->get($this->idgeneral);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('general', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatGeneral->iniciarTransaccion('neg_i_General');
			$this->idgeneral = $this->oDatGeneral->insertar($this->codigo,$this->nombre,$this->tipo_tabla,$this->mostrar);
			$this->oDatGeneral->terminarTransaccion('neg_i_General');	
			return $this->idgeneral;
		} catch(Exception $e) {	
		    $this->oDatGeneral->cancelarTransaccion('neg_i_General');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('general', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatGeneral->actualizar($this->idgeneral,$this->codigo,$this->nombre,$this->tipo_tabla,$this->mostrar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatGeneral->cambiarvalorcampo($this->idgeneral,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('General', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatGeneral->eliminar($this->idgeneral);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdgeneral($pk){
		try {
			$this->dataGeneral = $this->oDatGeneral->get($pk);
			if(empty($this->dataGeneral)) {
				throw new Exception(JrTexto::_("General").' '.JrTexto::_("not registered"));
			}
			$this->idgeneral = $this->dataGeneral["idgeneral"];
			$this->codigo = $this->dataGeneral["codigo"];
			$this->nombre = $this->dataGeneral["nombre"];
			$this->tipo_tabla = $this->dataGeneral["tipo_tabla"];
			$this->mostrar = $this->dataGeneral["mostrar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('general', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataGeneral = $this->oDatGeneral->get($pk);
			if(empty($this->dataGeneral)) {
				throw new Exception(JrTexto::_("General").' '.JrTexto::_("not registered"));
			}

			return $this->oDatGeneral->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}