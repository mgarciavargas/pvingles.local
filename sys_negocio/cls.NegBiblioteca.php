<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-11-2016
 * @copyright	Copyright (C) 19-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBiblioteca', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBiblioteca 
{
	protected $idbiblioteca;
	protected $nombre;
	protected $link;
	protected $tipo;
	protected $idpersonal;
	protected $idnivel;
	protected $idunidad;
	protected $idleccion;
	protected $fechareg;
	
	protected $dataBiblioteca;
	protected $oDatBiblioteca;	

	public function __construct()
	{
		$this->oDatBiblioteca = new DatBiblioteca;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBiblioteca->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBiblioteca->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBiblioteca->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBiblioteca->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBiblioteca->get($this->idbiblioteca);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('biblioteca', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->idbiblioteca= $this->oDatBiblioteca->insertar($this->nombre,$this->link,$this->tipo,$this->idpersonal,$this->idnivel,$this->idunidad,$this->idleccion);
			return $this->idbiblioteca;
		} catch(Exception $e) {	
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('biblioteca', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatBiblioteca->actualizar($this->idbiblioteca,$this->nombre,$this->link,$this->tipo,$this->idpersonal,$this->idnivel,$this->idunidad,$this->idleccion,$this->fechareg);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Biblioteca', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBiblioteca->eliminar($this->idbiblioteca);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdbiblioteca($pk){
		try {
			$this->dataBiblioteca = $this->oDatBiblioteca->get($pk);
			if(empty($this->dataBiblioteca)) {
				throw new Exception(JrTexto::_("Biblioteca").' '.JrTexto::_("not registered"));
			}
			$this->idbiblioteca = $this->dataBiblioteca["idbiblioteca"];
			$this->nombre = $this->dataBiblioteca["nombre"];
			$this->link = $this->dataBiblioteca["link"];
			$this->tipo = $this->dataBiblioteca["tipo"];
			$this->idpersonal = $this->dataBiblioteca["idpersonal"];
			$this->idnivel = $this->dataBiblioteca["idnivel"];
			$this->idunidad = $this->dataBiblioteca["idunidad"];
			$this->idleccion = $this->dataBiblioteca["idleccion"];
			$this->fechareg = $this->dataBiblioteca["fechareg"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('biblioteca', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataBiblioteca = $this->oDatBiblioteca->get($pk);
			if(empty($this->dataBiblioteca)) {
				throw new Exception(JrTexto::_("Biblioteca").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBiblioteca->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}