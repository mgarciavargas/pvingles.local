<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		27-12-2016
 * @copyright	Copyright (C) 27-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatHerramientas', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegHerramientas 
{
	protected $idtool;
	protected $idnivel;
	protected $idunidad;
	protected $idactividad;
	protected $idpersonal;
	protected $titulo;
	protected $descripcion;
	protected $texto;
	protected $orden;
	protected $tool;
	
	protected $dataHerramientas;
	protected $oDatHerramientas;	

	public function __construct()
	{
		$this->oDatHerramientas = new DatHerramientas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatHerramientas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatHerramientas->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatHerramientas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatHerramientas->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatHerramientas->get($this->idtool);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar($tool)
	{
		try {

			$res=null;
			if($tool!='G')
				if($this->orden==0){
					$res=$this->buscar(array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>$tool));
				}else
				   $res=$this->buscar(array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'idpersonal'=>$this->idpersonal,'tool'=>$tool));

			if(empty($res[0]))   
				$this->idtool = $this->oDatHerramientas->insertar($this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->titulo,$this->descripcion,$this->texto,$this->orden,$tool);
			else{
				$this->idtool=$res[0]["idtool"];
				$this->idtool =$this->oDatHerramientas->actualizar($this->idtool,$this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->titulo,$this->descripcion,$this->texto,$this->orden,$tool);
			}
			return $this->idtool;
		} catch(Exception $e) {	
			throw new Exception($e->getMessage());
		}
	}

	public function editar($tool)
	{
		try {	
			return $this->oDatHerramientas->actualizar($this->idtool,$this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->titulo,$this->descripcion,$this->texto,$this->orden,$tool);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Herramientas', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatHerramientas->eliminar($this->idtool);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtool($pk){
		try {
			$this->dataHerramientas = $this->oDatHerramientas->get($pk);
			if(empty($this->dataHerramientas)) {
				throw new Exception(JrTexto::_("Herramientas").' u '.$pk.' -'.JrTexto::_("not registered"));
			}
			$this->idtool = $this->dataHerramientas["idtool"];
			$this->idnivel = $this->dataHerramientas["idnivel"];
			$this->idunidad = $this->dataHerramientas["idunidad"];
			$this->idactividad = $this->dataHerramientas["idactividad"];
			$this->idpersonal = $this->dataHerramientas["idpersonal"];
			$this->titulo = $this->dataHerramientas["titulo"];
			$this->descripcion = $this->dataHerramientas["descripcion"];
			$this->texto = $this->dataHerramientas["texto"];
			$this->orden = $this->dataHerramientas["orden"];
			$this->tool = $this->dataHerramientas["tool"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('herramientas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataHerramientas = $this->oDatHerramientas->get($pk);
			if(empty($this->dataHerramientas)) {
				throw new Exception(JrTexto::_("Herramientas").' '.JrTexto::_("not registered"));
			}

			return $this->oDatHerramientas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}

	public function buscarJuegos($filtros=null){
		$filtros['tool'] = 'G';
		//$this->oDatHerramientas->setLimit(0,99999);
		$this->oDatHerramientas->setLimite(0,99999);
		$juegos = $this->oDatHerramientas->buscar($filtros);		
		return $juegos;
	}
		
}