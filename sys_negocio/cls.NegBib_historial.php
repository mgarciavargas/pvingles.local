<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_historial', RUTA_BASE, 'sys_datos');
class NegBib_historial 
{
	protected $id_historial;
	protected $id_personal;
	protected $id_estudio;
	protected $descripcion;
	protected $fec_registro;
	protected $tipo_usuario;
	protected $cantidad;
	
	protected $dataBib_historial;
	protected $oDatBib_historial;	

	public function __construct()
	{
		$this->oDatBib_historial = new DatBib_historial;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_historial->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_historial->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_historial->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBib_historial->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_historial->get($this->id_historial);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			#if(!NegSesion::tiene_acceso('bib_historial', 'add')) {
            #    throw new Exception(JrTexto::_('Restricted access').'!!');
			#}
			$this->id_historial = $this->oDatBib_historial->insertar($this->id_personal,$this->id_estudio,$this->descripcion,$this->tipo_usuario,$this->cantidad);
			return $this->id_historial;
		} catch(Exception $e) {		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			#if(!NegSesion::tiene_acceso('bib_historial', 'edit')) {
			#	throw new Exception(JrTexto::_('Restricted access').'!!');
			#}	
			return $this->oDatBib_historial->actualizar($this->id_historial,$this->id_personal,$this->id_estudio,$this->descripcion,$this->tipo_usuario,$this->cantidad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			#if(!NegSesion::tiene_acceso('Bib_historial', 'delete')) {
			#	throw new Exception(JrTexto::_('Restricted access').'!!');
			#}
			return $this->oDatBib_historial->eliminar($this->id_historial);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_historial($pk){
		try {
			$this->dataBib_historial = $this->oDatBib_historial->get($pk);
			if(empty($this->dataBib_historial)) {
				throw new Exception(JrTexto::_("Bib_historial").' '.JrTexto::_("not registered"));
			}
			$this->id_historial = $this->dataBib_historial["id_historial"];
			$this->id_personal = $this->dataBib_historial["id_personal"];
			$this->id_estudio = $this->dataBib_historial["id_estudio"];
			$this->descripcion = $this->dataBib_historial["descripcion"];
			$this->fec_registro = $this->dataBib_historial["fec_registro"];
			$this->tipo_usuario = $this->dataBib_historial["tipo_usuario"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			#if(!NegSesion::tiene_acceso('bib_historial', 'editar')) {
			#	throw new Exception(JrTexto::_('Restricted access').'!!');
			#}
			$this->dataBib_historial = $this->oDatBib_historial->get($pk);
			if(empty($this->dataBib_historial)) {
				throw new Exception(JrTexto::_("Bib_historial").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_historial->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}	
}