<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		18-11-2016
 * @copyright	Copyright (C) 18-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatActividad_frase', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegActividad_frase 
{
	protected $idfrase;
	protected $idactividad_detalle;
	protected $descripcion;
	protected $frase;
	
	protected $dataActividad_frase;
	protected $oDatActividad_frase;	

	public function __construct()
	{
		$this->oDatActividad_frase = new DatActividad_frase;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatActividad_frase->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatActividad_frase->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatActividad_frase->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatActividad_frase->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatActividad_frase->get($this->idfrase);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			
			$this->idfrase = $this->oDatActividad_frase->insertar($this->idactividad_detalle,$this->descripcion,$this->frase);
			return $this->idfrase;
		} catch(Exception $e) {	
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			return $this->oDatActividad_frase->actualizar($this->idfrase,$this->idactividad_detalle,$this->descripcion,$this->frase);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Actividad_frase', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatActividad_frase->eliminar($this->idfrase);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdfrase($pk){
		try {
			$this->dataActividad_frase = $this->oDatActividad_frase->get($pk);
			if(empty($this->dataActividad_frase)) {
				throw new Exception(JrTexto::_("Actividad_frase").' '.JrTexto::_("not registered"));
			}
			$this->idfrase = $this->dataActividad_frase["idfrase"];
			$this->idactividad_detalle = $this->dataActividad_frase["idactividad_detalle"];
			$this->descripcion = $this->dataActividad_frase["descripcion"];
			$this->frase = $this->dataActividad_frase["frase"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('actividad_frase', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataActividad_frase = $this->oDatActividad_frase->get($pk);
			if(empty($this->dataActividad_frase)) {
				throw new Exception(JrTexto::_("Actividad_frase").' '.JrTexto::_("not registered"));
			}

			return $this->oDatActividad_frase->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}	
}