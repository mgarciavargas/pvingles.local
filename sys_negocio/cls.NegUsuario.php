<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		09-06-2016
 * @copyright	Copyright (C) 09-06-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatUsuario', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegUsuario 
{
	protected $id_usuario;
	protected $nombre;
	protected $usuario;
	protected $email;
	protected $clave;
	protected $rol;
	protected $token;
	protected $flag_estado;
	
	public $dataUsuario;
	protected $oDatUsuario;	

	public function __construct()
	{
		$this->oDatUsuario = new DatUsuario;
		
	}
	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}

	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatUsuario->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatUsuario->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatUsuario->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatUsuario->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	
	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('usuario', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatUsuario->iniciarTransaccion('neg_i_Usuario');
			$this->id_usuario = $this->oDatUsuario->insertar($this->nombre,$this->usuario,$this->email,md5($this->clave),$this->rol,$this->token,$this->flag_estado);
			$this->oDatUsuario->terminarTransaccion('neg_i_Usuario');	
			return $this->id_usuario;
		} catch(Exception $e) {	
		   $this->oDatUsuario->cancelarTransaccion('neg_i_Usuario');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('usuario', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatUsuario->iniciarTransaccion('neg_e_Usuario');
			$this->oDatUsuario->actualizar($this->id_usuario,$this->nombre,$this->usuario,$this->email,$this->rol,$this->flag_estado);

			if(!empty($this->clave)) {
				$this->oDatUsuario->set($this->id_usuario,'clave', md5($this->clave));
			}
			$this->oDatUsuario->terminarTransaccion('neg_e_Usuario');			
		} catch(Exception $e) {
		    $this->oDatUsuario->cancelarTransaccion('neg_e_Usuario');
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($pk)
	{
		try {
			if(!NegSesion::tiene_acceso('Usuario', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatUsuario->iniciarTransaccion('neg_d_Usuario');
			$data=$this->oDatUsuario->eliminar($pk);
			$this->oDatUsuario->terminarTransaccion('neg_d_Usuario');	
			return $data;
		} catch(Exception $e) {
			 $this->oDatUsuario->cancelarTransaccion('neg_d_Usuario');
			throw new Exception($e->getMessage());
		}
	}

	public function cambiarClave($clave, $re_clave)
	{
		try {
			if(empty($clave)) {
				throw new Exception(JrTexto::_('Enter password'));
			}
			
			if($clave != $re_clave) {
				throw new Exception(JrTexto::_('Passwords do not match'));
			}
			
			$this->oDatUsuario->setClave($this->id_usuario, $clave);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function procesarSolicitudCambioClave($usuario)
	{
		try {
			
			$usuario = $this->oDatUsuario->getxusuarioemail($usuario);			
			if(!empty($usuario)) {
				$this->token = JrSession::crearRandom();				
				$this->oDatUsuario->set($usuario['id_usuario'],'token',$this->token);				
				return $usuario;
			}			
			return null;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_usuario($pk){
		try {
			$this->dataUsuario = $this->oDatUsuario->get($pk);
			if(empty($this->dataUsuario)) {
				throw new Exception(JrTexto::_("User").' '.JrTexto::_("not registered"));
			}
			$this->id_usuario = $this->dataUsuario["id_usuario"];
			$this->nombre = $this->dataUsuario["nombre"];
			$this->usuario = $this->dataUsuario["usuario"];
			$this->email = $this->dataUsuario["email"];
			$this->clave = $this->dataUsuario["clave"];
			$this->rol = $this->dataUsuario["rol"];
			$this->token = $this->dataUsuario["token"];
			$this->flag_estado = $this->dataUsuario["flag_estado"];
			
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}
	public function setNombre($nombre)
	{
		try {
			$this->nombre = NegTools::validar('todo', $nombre, true, JrTexto::_('Enter Name'), array('longmax' => 80));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	public function setUsuario($usuario)
	{
		try {
			$this->usuario = NegTools::validar('todo', $usuario, true,JrTexto::_('Enter a valid user'), array('longmin' => 5, 'longmax' => 80));
			
			if(empty($this->id_usuario) || @$this->dataUsuario['usuario'] != $usuario) {
				$existe = $this->oDatUsuario->getxusuario($usuario);
				if(!empty($existe)) {
					throw new Exception(JrTexto::_('The user entered is already in use'));
				}
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	public function setEmail($email)
	{
		try {
			$this->email = NegTools::validar('email', $email, false, JrTexto::_('Enter an email'), array('longmax' => 80));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	public function setClave($clave)
	{
		try {
			if(empty($clave)) {
				throw new Exception(JrTexto::_('Enter password'));
			}
			$this->clave = $clave;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	public function setRol($rol)
	{
		try {
			if(empty($rol)) {
				throw new Exception(JrTexto::_('Select a role'));
			}
			$this->rol = $rol;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	
	public function setFlag_activo($flag_estado)
	{
		try {
			if(!in_array($flag_estado, array(0, 1))) {
				throw new Exception(JrTexto::_('Invalid state'));
			}
			
			$this->flag_estado = $flag_estado;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('usuario', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataUsuario = $this->oDatUsuario->get($pk);
			if(empty($this->dataUsuario)) {
				throw new Exception(JrTexto::_("User").' '.JrTexto::_("not registered"));
			}

			$this->oDatUsuario->iniciarTransaccion('neg_uc_Usuario');
			$data=$this->oDatUsuario->set($pk, $propiedad, $valor);
			$this->oDatUsuario->terminarTransaccion('neg_uc_Usuario');	
			return $data;
		} catch(Exception $e) {
			 $this->oDatUsuario->cancelarTransaccion('neg_uc_Usuario');
			throw new Exception($e->getMessage());
		}

	}
}