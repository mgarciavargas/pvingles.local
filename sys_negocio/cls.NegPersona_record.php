<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		01-02-2018
 * @copyright	Copyright (C) 01-02-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersona_record', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegPersona_record 
{
	protected $idrecord;
	protected $idpersona;
	protected $tiporecord;
	protected $titulo;
	protected $descripcion;
	protected $archivo;
	protected $fecharegistro;
	protected $mostrar;
	protected $idusuario;
	protected $dataPersona_record;
	protected $oDatPersona_record;	

	public function __construct()
	{
		$this->oDatPersona_record = new DatPersona_record;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPersona_record->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatPersona_record->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPersona_record->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPersona_record->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPersona_record->get($this->idrecord);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_record', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPersona_record->iniciarTransaccion('neg_i_Persona_record');
			$this->idrecord = $this->oDatPersona_record->insertar($this->idpersona,$this->tiporecord,$this->titulo,$this->descripcion,$this->archivo,$this->fecharegistro,$this->mostrar,$this->idusuario);
			$this->oDatPersona_record->terminarTransaccion('neg_i_Persona_record');	
			return $this->idrecord;
		} catch(Exception $e) {	
		    $this->oDatPersona_record->cancelarTransaccion('neg_i_Persona_record');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_record', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPersona_record->actualizar($this->idrecord,$this->idpersona,$this->tiporecord,$this->titulo,$this->descripcion,$this->archivo,$this->fecharegistro,$this->mostrar,$this->idusuario);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatPersona_record->cambiarvalorcampo($this->idrecord,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Persona_record', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPersona_record->eliminar($this->idrecord);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrecord($pk){
		try {
			$this->dataPersona_record = $this->oDatPersona_record->get($pk);
			if(empty($this->dataPersona_record)) {
				throw new Exception(JrTexto::_("Persona_record").' '.JrTexto::_("not registered"));
			}
			$this->idrecord = $this->dataPersona_record["idrecord"];
			$this->idpersona = $this->dataPersona_record["idpersona"];
			$this->tiporecord = $this->dataPersona_record["tiporecord"];
			$this->titulo = $this->dataPersona_record["titulo"];
			$this->descripcion = $this->dataPersona_record["descripcion"];
			$this->archivo = $this->dataPersona_record["archivo"];
			$this->fecharegistro = $this->dataPersona_record["fecharegistro"];
			$this->mostrar = $this->dataPersona_record["mostrar"];
			$this->idusuario = $this->dataPersona_record["idusuario"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('persona_record', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPersona_record = $this->oDatPersona_record->get($pk);
			if(empty($this->dataPersona_record)) {
				throw new Exception(JrTexto::_("Persona_record").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPersona_record->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}