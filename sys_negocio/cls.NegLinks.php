<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		26-12-2016
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatHerramientas', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegLinks 
{
	protected $idlink;
	protected $idnivel;
	protected $idunidad;
	protected $idactividad;
	protected $idpersonal;
	protected $texto;
	protected $tool;
	
	protected $dataHerramientas;
	protected $oDatHerramientas;	

	public function __construct()
	{
		$this->oDatHerramientas = new DatHerramientas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatHerramientas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			if(empty($filtros))
			$filtros["tool"]='L';
			return $this->oDatHerramientas->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			if(empty($filtros))
			$filtros["tool"]='L';
			return $this->oDatHerramientas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatHerramientas->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatHerramientas->get($this->idlink);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('links', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$res=null;
			if($this->orden==0){
				$res=$this->buscar(array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'L'));
			}else
			   $res=$this->buscar(array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'idpersonal'=>$this->idpersonal,'L'));
			//$this->oDatLinks->iniciarTransaccion('neg_i_Links');
			if(empty($res[0]))   
				$this->idlink = $this->oDatHerramientas->insertar($this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->texto,$this->orden,'L');
			else{
				$this->idlink=$res[0]["idtool"];
				$this->idlink =$this->oDatHerramientas->actualizar($this->idlink,$this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->texto,$this->orden,'L');
			}
			//$this->oDatLinks->terminarTransaccion('neg_i_Links');	
			return $this->idlink;
		} catch(Exception $e) {	
		   //$this->oDatLinks->cancelarTransaccion('neg_i_Links');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('links', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatHerramientas->actualizar($this->idlink,$this->idnivel,$this->idunidad,$this->idactividad,$this->idpersonal,$this->texto,$this->orden,'L');
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Links', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatHerramientas->eliminar($this->idlink);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdlink($pk){
		try {
			$this->dataHerramientas = $this->oDatHerramientas->get($pk);
			if(empty($this->dataHerramientas)) {
				throw new Exception(JrTexto::_("Links").' '.JrTexto::_("not registered"));
			}
			$this->idlink = $this->dataHerramientas["idtool"];
			$this->idnivel = $this->dataHerramientas["idnivel"];
			$this->idunidad = $this->dataHerramientas["idunidad"];
			$this->idactividad = $this->dataHerramientas["idactividad"];
			$this->idpersonal = $this->dataHerramientas["idpersonal"];
			$this->texto = $this->dataHerramientas["texto"];
			$this->orden = $this->dataHerramientas["orden"];
			$this->tool = $this->dataHerramientas["tool"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('links', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataHerramientas = $this->oDatHerramientas->get($pk);
			if(empty($this->dataHerramientas)) {
				throw new Exception(JrTexto::_("Links").' '.JrTexto::_("not registered"));
			}

			return $this->oDatHerramientas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
		
}