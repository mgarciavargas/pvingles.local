<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-07-2017
 * @copyright	Copyright (C) 19-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatCargos', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegCargos 
{
	protected $idcargo;
	protected $cargo;
	protected $mostrar;
	
	protected $dataCargos;
	protected $oDatCargos;	

	public function __construct()
	{
		$this->oDatCargos = new DatCargos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatCargos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatCargos->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatCargos->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatCargos->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatCargos->get($this->idcargo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$this->oDatCargos->iniciarTransaccion('neg_i_Cargos');
			$this->idcargo = $this->oDatCargos->insertar($this->cargo,$this->mostrar);
			$this->oDatCargos->terminarTransaccion('neg_i_Cargos');	
			return $this->idcargo;
		} catch(Exception $e) {	
		    $this->oDatCargos->cancelarTransaccion('neg_i_Cargos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {	
			return $this->oDatCargos->actualizar($this->idcargo,$this->cargo,$this->mostrar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatCargos->cambiarvalorcampo($this->idcargo,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			return $this->oDatCargos->eliminar($this->idcargo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcargo($pk){
		try {
			$this->dataCargos = $this->oDatCargos->get($pk);
			if(empty($this->dataCargos)) {
				throw new Exception(JrTexto::_("Cargos").' '.JrTexto::_("not registered"));
			}
			$this->idcargo = $this->dataCargos["idcargo"];
			$this->cargo = $this->dataCargos["cargo"];
			$this->mostrar = $this->dataCargos["mostrar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			$this->dataCargos = $this->oDatCargos->get($pk);
			if(empty($this->dataCargos)) {
				throw new Exception(JrTexto::_("Cargos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatCargos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}