drop table cargos;
CREATE TABLE `cargos` (
  `idcargo` int(11) NOT NULL,
  `cargo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `cargos` (`idcargo`, `cargo`, `mostrar`) VALUES
(1, 'Docente', 1),
(2, 'Administrador', 1),
(3, 'Director', 1);


ALTER TABLE `cargos`
  ADD PRIMARY KEY (`idcargo`);

DROP TABLE IF EXISTS `acad_curso`;
CREATE TABLE `acad_curso` (
  `idcurso` int(1) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(1) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idusuario` int(11) NOT NULL,
  `vinculosaprendizajes` text COLLATE utf8_spanish_ci,
  `materialesyrecursos` text COLLATE utf8_spanish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `acad_cursoaprendizaje`;
CREATE TABLE `acad_cursoaprendizaje` (
  `idaprendizaje` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `idcursodetalle` int(11) NOT NULL,
  `idpadre` int(11) NOT NULL,
  `idrecurso` int(11) DEFAULT NULL,
  `descripcion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `acad_cursodetalle`;
CREATE TABLE `acad_cursodetalle` (
  `idcursodetalle` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `orden` bigint(20) NOT NULL,
  `idrecurso` int(11) NOT NULL,
  `tiporecurso` varchar(2) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Examen, nivel , rubrica,etc',
  `idlogro` int(11) NOT NULL,
  `url` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `idpadre` smallint(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


DROP TABLE IF EXISTS `acad_cursosesion`;
CREATE TABLE `acad_cursosesion` (
  `idsesion` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `duracion` time DEFAULT NULL,
  `informaciongeneral` longtext,
  `idcursodetalle` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `acad_grupoaula`;
CREATE TABLE `acad_grupoaula` (
  `idgrupoaula` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'virtual,presencial,mixto',
  `comentario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `nvacantes` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `acad_grupoauladetalle`;
CREATE TABLE `acad_grupoauladetalle` (
  `idgrupoauladetalle` bigint(20) NOT NULL,
  `idgrupoaula` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `iddocente` int(11) NOT NULL,
  `idlocal` int(11) DEFAULT '0',
  `idambiente` int(11) DEFAULT '0' COMMENT 'aula física o laboratorio donde se dará la clase',
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_final` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `acad_horariogrupodetalle`;
CREATE TABLE `acad_horariogrupodetalle` (
  `idhorario` int(11) NOT NULL,
  `idgrupoauladetalle` int(11) NOT NULL,
  `fecha_finicio` timestamp NOT NULL,
  `fecha_final` timestamp NOT NULL,
  `descripcion` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `color` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idhorariopadre` tinyint(4) NOT NULL,
  `diasemana` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


DROP TABLE IF EXISTS `acad_matricula`;
CREATE TABLE `acad_matricula` (
  `idmatricula` int(11) NOT NULL,
  `idgrupoauladetalle` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fecha_matricula` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `ambiente`;
CREATE TABLE `ambiente` (
  `idambiente` int(11) NOT NULL,
  `idlocal` int(11) NOT NULL,
  `numero` varchar(3) COLLATE utf8_spanish_ci DEFAULT NULL,
  `capacidad` int(11) NOT NULL DEFAULT '0',
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `estado` tinyint(4) DEFAULT NULL,
  `turno` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ambiente`
--

INSERT INTO `ambiente` (`idambiente`, `idlocal`, `numero`, `capacidad`, `tipo`, `estado`, `turno`) VALUES
(1, 1, '101', 35, 'A', 1, 'T'),
(2, 1, '102', 35, 'A', 1, 'M'),
(3, 1, '401', 35, 'L', 0, 'N'),
(4, 1, '402', 35, 'L', 1, 'T');

DROP TABLE IF EXISTS `general`;
CREATE TABLE `general` (
  `idgeneral` int(11) NOT NULL,
  `codigo` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_tabla` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


INSERT INTO `general` (`idgeneral`, `codigo`, `nombre`, `tipo_tabla`, `mostrar`) VALUES
(1, 'M', 'Masculino', 'sexo', 1),
(2, 'F', 'Femenino', 'sexo', 1),
(3, '1', 'DNI', 'tipodocidentidad', 1),
(4, '2', 'Passaporte', 'tipodocidentidad', 1),
(5, '1', 'Padre', 'parentesco', 1),
(6, '2', 'Madre', 'parentesco', 1),
(7, '3', 'Tio(a)', 'parentesco', 1),
(8, '3', 'Apoderado', 'parentesco', 1),
(9, 'C', 'Casado', 'estadocivil', 1),
(10, 'S', 'Soltero', 'estadocivil', 1),
(11, 'V', 'Viudo', 'estadocivil', 1),
(12, 'D', 'Divorciado', 'estadocivil', 1),
(13, 'B', 'Becado', 'condicionpersona', 1),
(14, 'O', 'Observado', 'condicionpersona', 1),
(16, '1', 'Primaria', 'tipoestudio', 1),
(17, '2', 'Segundaria', 'tipoestudio', 1),
(18, '3', 'Diplomado', 'tipoestudio', 1),
(19, '4', 'Universitario Bachiller', 'tipoestudio', 1),
(20, '5', 'Universitario-titulado', 'tipoestudio', 1),
(21, '6', 'Maestria', 'tipoestudio', 1),
(22, '7', 'Doctorado', 'tipoestudio', 1),
(23, '1', 'En curso', 'situacionestudio', 1),
(24, '2', 'Graduado', 'situacionestudio', 1),
(25, '3', 'Abandonado', 'situacionestudio', 1),
(26, '1', 'Ing. de Sistemas', 'areaestudio', 1),
(27, '2', 'Derecho', 'areaestudio', 1),
(28, '1', 'Logistica', 'areaempresa', 1),
(29, '2', 'Gerencia General', 'areaempresa', 1),
(30, 'V', 'Vitual', 'tipogrupo', 1),
(31, 'P', 'Presencial', 'tipogrupo', 1),
(32, 'M', 'Mixto', 'tipogrupo', 1),
(33, '1', 'Open', 'estadogrupo', 1),
(34, '2', 'Closed', 'estadogrupo', 1),
(35, '3', 'Anulado', 'estadogrupo', 1),
(36, 'L', 'Laboratorio', 'tipoambiente', 1),
(37, 'A', 'Aula', 'tipoambiente', 1),
(38, 'U', 'Auditorio', 'tipoambiente', 1),
(39, 'M', 'Mañana', 'turno', 1),
(40, 'T', 'Tarde', 'turno', 1),
(41, 'N', 'Noche', 'truno', 1);

DROP TABLE IF EXISTS `local`;
CREATE TABLE `local` (
  `idlocal` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `direccion` text COLLATE utf8_spanish_ci,
  `id_ubigeo` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `vacantes` int(11) DEFAULT NULL,
  `idugel` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `local` (`idlocal`, `nombre`, `direccion`, `id_ubigeo`, `tipo`, `vacantes`, `idugel`) VALUES
(1, 'Institucion educativa 1154', 'una direccion #123', '140202', 'C', 10, 2);



DROP TABLE IF EXISTS `personal`;
CREATE TABLE IF NOT EXISTS `personal` (
  `dni` int(11) NOT NULL,
  `ape_paterno` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `ape_materno` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `fechanac` date DEFAULT NULL,
  `sexo` char(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `estado_civil` char(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `ubigeo` char(8) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `urbanizacion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `celular` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(75) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `idugel` char(4) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `regusuario` int(11) NOT NULL DEFAULT '0',
  `regfecha` date NOT NULL,
  `usuario` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `token` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `rol` int(11) NOT NULL,
  `foto` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `situacion` char(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `idioma` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`dni`),
  KEY `nombre` (`nombre`,`ape_paterno`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `personal` (`dni`, `ape_paterno`, `ape_materno`, `nombre`, `fechanac`, `sexo`, `estado_civil`, `ubigeo`, `urbanizacion`, `direccion`, `telefono`, `celular`, `email`, `idugel`, `regusuario`, `regfecha`, `usuario`, `clave`, `token`, `rol`, `foto`, `estado`, `situacion`, `idioma`) VALUES
(43831104, 'SmartClass', '', 'Soporte', '1986-10-28', 'M', '', '', '', '', '', '', 'smartclass@abacoeducacion.org', '', 0, '2017-11-11', 'admin', '0192023a7bbd73250516f069df18b500', 'a3b1da834b82e4b6887d54de54f29df9', 1, '', 1, '1', 'EN'),
(43371672, 'Muñoz', 'Meza', 'Evelio', '2016-11-14', 'M', 'C', '14010101', 'MARIA PARA DE BELLIDO', 'MEXICO 790', '251924', '979222660', 'emunozmeza@gmail.com', '0001', 1, '2016-11-14', '43371672', 'c4ca4238a0b923820dcc509a6f75849b', 'aaa18d9d0452b32a66696f430e3b617d', 1, '', 1, '1', 'EN'),
(72042592, 'Figueroa', 'Piscoya', 'Eder', NULL, 'M', 'S', '', '', '', '', '', 'eder.figueroa@outlook.com', '', 0, '2016-12-26', 'eder.figueroa', '81dc9bdb52d04dc20036dbd8313ed055', '311b07c848249765d17cfe66a3609fa9', 1, '', 1, '1', 'EN'),
(22222222, 'Eder', 'F.P.', 'Docente', NULL, 'M', 'S', '', '', '', '', '', 'docente1@hotmail.com', '', 1, '2016-12-26', 'doc_eder', '81dc9bdb52d04dc20036dbd8313ed055', '9221a1392d470115ace76a707b26d8c6', 2, '', 1, '1', 'EN'),
(99999999, 'System', '', 'Cv', '2017-02-01', 'M', 'S', '', '', '', '', '', 'info@pvingles.com', '', 0, '0000-00-00', 'pvingles', '0192023a7bbd73250516f069df18b500', 'admin123', 3, '', 1, '1', 'EN'),
(75935728, 'System', '', 'Soporte', '1992-06-21', 'F', 'S', '', '', '', '', '', 'smartclass@abacoeducacion.org', '', 0, '0000-00-00', 'soporte', '012c5d6d1f7dee260b391264fb2f7b17', 'd81cfe5a19e57f898235bd8921ecbcfd', 1, '', 1, '1', 'ES'),
(33333333, 'System', '', 'docente', '2017-02-01', 'M', 'S', '', '', '', '', '', 'docente1@hotmail.com', '', 0, '2017-02-09', 'abel_chingo', '0192023a7bbd73250516f069df18b500', 'bdab77809db16e4f6e7d414671fe5465', 2, '', 1, '1', 'EN'),
(10101011, 'Monja', 'Lopez', 'Yolanda', '2017-02-01', 'F', 'S', '', '', '', '', '', 'yoly4816@hotmail.com', '', 0, '2017-10-02', 'yoly4816', 'e10adc3949ba59abbe56e057f20f883e', 'e10adc3949ba59abbe56e057f20f883e', 1, '', 1, '1', 'EN'),
(55555555, 'Castro', 'Alzamora', 'Manuel', '1966-10-31', 'M', 'S', '', '', '', '', '', 'mcmanolito@gmail.com', '', 0, '2017-02-09', 'mcastro', '9e5cafda6f6da794e3d64fd1baa72867', '9e5cafda6f6da794e3d64fd1baa72867', 1, '', 1, '1', 'EN'),
(66666666, 'Peña', 'Flores', 'Enny', '2017-02-01', 'F', 'C', '', '', '', '', '', 'enrupf@gmail.com', '', 0, '2017-02-09', 'enny', '0192023a7bbd73250516f069df18b500', '88e08ecc241a959699b27adcf6809e45', 1, '', 1, '1', 'EN'),
(77777777, 'Landivar', 'Saavedra', 'Anais', '2017-02-01', 'F', 'S', '', '', '', '', '', 'anais_1469@hotmail.com ', '', 0, '2017-02-09', 'anais', '3710fd04a04df697f0af60d5e1345549', '0a3a45ae10bb5ce739a04e9fb8996a34', 1, '', 1, '1', 'EN'),
(88888888, 'm', '', 'Sharon', '2017-02-01', 'F', 'S', '', '', '', '', '', 'smamud@hotmail.com', '', 0, '2017-02-09', 'smamud', '2f348b9c79d4303031bf7c42ecfe6fbd', '4fdaaf59bc6095b1b6c4276062efc5c4', 1, '', 1, '1', 'EN'),
(11111111, 'Garrido', 'Reque', 'Tatiana', '2017-05-26', 'F', 'S', '', '', '', '', '', '', '', 0, '0000-00-00', 'tatiana', '4e39d9452ab78db254167d7fcf5261e1', '54f74210b70a8baf116a464d971385d2', 1, '', 0, '0', 'EN'),
(21212121, 'Morante', 'Chumpen', 'Monica', NULL, 'F', 'C', '', '', '', '', '', 'mmorante@hotmail.com', '', 0, '2017-06-22', 'monica', 'f32e15e7af11d77eac7ca295b3e9a068', '7fee84ec6965e589e8623dd446f6c764', 1, '', 1, '1', 'EN'),
(10101010, 'Sandoval', 'Cueva', 'Claudia', '1906-06-06', 'F', 'S', '', '', '', '', '', 'sincorreo@hotmail.com', '', 0, '2017-10-02', 'claudiasc', '0192023a7bbd73250516f069df18b500', '3b71ffe8616189ba45ca0f8255ab066c', 1, '', 1, '1', 'EN'),
(12121212, 'Testing', 'Test', 'Samuel', '2017-11-01', '', '', '', '', '', '', '', 'samuel@gmail.com', '', 0, '2017-11-30', 'samuel', '0192023a7bbd73250516f069df18b500', '', 1, '', 1, '1', 'EN'),
(65300999, 'Huamanchumo', 'Arrelucea', 'Angie', '0000-00-00', 'F', 'S', '101010', '-', '-', '933128000', '933128000', 'anha16@hotmail.com', '01', 1, '2018-01-04', 'anha16', 'e1ee2e1df105b85e26c449b9bffe10de', 'internetmovil', 1, '', 1, '1', 'EN'),
(72042593, 'Figueroa', 'Piscoya', 'Eder', NULL, 'M', 'S', '', '', '', '', '', 'eder.figueroa@outlook.com', '', 0, '2016-12-26', 'efigueroap', '81dc9bdb52d04dc20036dbd8313ed055', '311b07c848249765d17cfe66a3609fa9', 1, '', 1, '1', 'EN');

DROP TABLE IF EXISTS `persona_apoderado`;
CREATE TABLE `persona_apoderado` (
  `idapoderado` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ape_paterno` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ape_materno` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `sexo` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `tipodoc` int(11) NOT NULL,
  `ndoc` int(11) NOT NULL,
  `parentesco` int(11) NOT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `celular` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `persona_educacion`;
CREATE TABLE `persona_educacion` (
  `ideducacion` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `institucion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `tipodeestudio` int(2) NOT NULL,
  `titulo` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `areaestudio` int(11) DEFAULT NULL,
  `situacion` int(1) NOT NULL,
  `fechade` date NOT NULL,
  `fechahasta` date DEFAULT NULL,
  `actualmente` int(1) DEFAULT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `persona_experiencialaboral`;
CREATE TABLE `persona_experiencialaboral` (
  `idexperiencia` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `empresa` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `rubro` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `area` int(11) NOT NULL,
  `cargo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `funciones` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `fechade` date NOT NULL,
  `fechahasta` date NOT NULL,
  `actualmente` int(1) NOT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `persona_metas`;
CREATE TABLE `persona_metas` (
  `idmeta` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `meta` text COLLATE utf8_spanish_ci NOT NULL,
  `objetivo` text COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `persona_referencia`;
CREATE TABLE `persona_referencia` (
  `idreferencia` int(11) NOT NULL,
  `idpersona` int(8) NOT NULL,
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `cargo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `relacion` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `persona_rol`;
CREATE TABLE `persona_rol` (
  `iddetalle` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `persona_rol` (`iddetalle`, `idrol`, `idpersonal`) VALUES
(10, 1, 55555555),
(1, 2, 43831104),
(2, 3, 43831104),
(11, 2, 55555555),
(3, 2, 43371672),
(4, 3, 43371672),
(5, 1, 72042592),
(6, 2, 72042592),
(7, 3, 72042592),
(12, 3, 55555555),
(8, 2, 22222222),
(9, 3, 22222222),
(13, 3, 44444444),
(14, 3, 100000000),
(31, 2, 88888888),
(16, 3, 100000001),
(17, 2, 10101010),
(18, 2, 10101011),
(19, 2, 100000001),
(20, 3, 100000001),
(21, 3, 100000002),
(22, 2, 100000002),
(23, 3, 100000003),
(29, 2, 100000003),
(32, 1, 88888888),
(33, 3, 100000004),
(34, 4, 72042593),
(35, 4, 72042592);

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `idrol` int(11) NOT NULL,
  `rol` varchar(35) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`idrol`, `rol`) VALUES
(1, 'Administrador'),
(2, 'Docente'),
(3, 'Alumno'),
(4, 'Supervisor');

ALTER TABLE `acad_curso`
  ADD PRIMARY KEY (`idcurso`);

ALTER TABLE `acad_cursoaprendizaje`
  ADD PRIMARY KEY (`idaprendizaje`);

ALTER TABLE `acad_cursodetalle`
  ADD PRIMARY KEY (`idcursodetalle`);

ALTER TABLE `acad_cursosesion`
  ADD PRIMARY KEY (`idsesion`);

ALTER TABLE `acad_grupoaula`
  ADD PRIMARY KEY (`idgrupoaula`);

ALTER TABLE `acad_grupoauladetalle`
  ADD PRIMARY KEY (`idgrupoauladetalle`);

ALTER TABLE `acad_horariogrupodetalle`
  ADD PRIMARY KEY (`idhorario`);

ALTER TABLE `acad_matricula`
  ADD PRIMARY KEY (`idmatricula`);

ALTER TABLE `ambiente`
  ADD PRIMARY KEY (`idambiente`);

ALTER TABLE `general`
  ADD PRIMARY KEY (`idgeneral`);

ALTER TABLE `local`
  ADD PRIMARY KEY (`idlocal`);

ALTER TABLE `persona_educacion`
  ADD PRIMARY KEY (`ideducacion`);

ALTER TABLE `persona_experiencialaboral`
  ADD PRIMARY KEY (`idexperiencia`);

ALTER TABLE `persona_metas`
  ADD PRIMARY KEY (`idmeta`);

ALTER TABLE `persona_referencia`
  ADD PRIMARY KEY (`idreferencia`);

ALTER TABLE `persona_rol`
  ADD PRIMARY KEY (`iddetalle`);

ALTER TABLE `roles`
  ADD PRIMARY KEY (`idrol`);