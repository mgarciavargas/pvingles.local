/*DISPARADORES SQL*/
DELIMITER @
CREATE TRIGGER DELETE_CURSO BEFORE DELETE ON acad_curso FOR EACH ROW BEGIN DELETE FROM acad_cursodetalle WHERE idcurso=old.idcurso; END;
@;

DELIMITER @
CREATE TRIGGER DELETE_CURSODETALLE BEFORE DELETE ON acad_cursodetalle FOR EACH ROW BEGIN DELETE FROM niveles WHERE idnivel=old.idrecurso AND tipo=old.tiporecurso; END;
@;


ALTER TABLE `niveles` CHANGE `nombre` `nombre` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;
ALTER TABLE `niveles` CHANGE `tipo` `tipo` CHAR(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'N= nivel, U= unidad, L=lesson';
ALTER TABLE `niveles` CHANGE `descripcion` `descripcion` TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL;
ALTER TABLE `niveles` CHANGE `imagen` `imagen` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL;