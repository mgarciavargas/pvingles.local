<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		20-04-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatGrupos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Grupos").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM grupos";
			
			$cond = array();		
			
			if(!empty($filtros["idgrupo"])) {
					$cond[] = "idgrupo = " . $this->oBD->escapar($filtros["idgrupo"]);
			}
			if(!empty($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(!empty($filtros["idambiente"])) {
					$cond[] = "idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(!empty($filtros["idasistente"])) {
					$cond[] = "idasistente = " . $this->oBD->escapar($filtros["idasistente"]);
			}
			if(!empty($filtros["fechainicio"])) {
					$cond[] = "fechainicio = " . $this->oBD->escapar($filtros["fechainicio"]);
			}
			if(!empty($filtros["fechafin"])) {
					$cond[] = "fechafin = " . $this->oBD->escapar($filtros["fechafin"]);
			}
			if(!empty($filtros["dias"])) {
					$cond[] = "dias = " . $this->oBD->escapar($filtros["dias"]);
			}
			if(!empty($filtros["horas"])) {
					$cond[] = "horas = " . $this->oBD->escapar($filtros["horas"]);
			}
			if(!empty($filtros["horainicio"])) {
					$cond[] = "horainicio = " . $this->oBD->escapar($filtros["horainicio"]);
			}
			if(!empty($filtros["horafin"])) {
					$cond[] = "horafin = " . $this->oBD->escapar($filtros["horafin"]);
			}
			if(!empty($filtros["valor"])) {
					$cond[] = "valor = " . $this->oBD->escapar($filtros["valor"]);
			}
			if(!empty($filtros["valor_asi"])) {
					$cond[] = "valor_asi = " . $this->oBD->escapar($filtros["valor_asi"]);
			}
			if(!empty($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(!empty($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(!empty($filtros["matriculados"])) {
					$cond[] = "matriculados = " . $this->oBD->escapar($filtros["matriculados"]);
			}
			if(!empty($filtros["nivel"])) {
					$cond[] = "nivel = " . $this->oBD->escapar($filtros["nivel"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Grupos").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM grupos";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idgrupo"])) {
					$cond[] = "idgrupo = " . $this->oBD->escapar($filtros["idgrupo"]);
			}
			if(!empty($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(!empty($filtros["idambiente"])) {
					$cond[] = "idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(!empty($filtros["idasistente"])) {
					$cond[] = "idasistente = " . $this->oBD->escapar($filtros["idasistente"]);
			}
			if(!empty($filtros["fechainicio"])) {
					$cond[] = "fechainicio = " . $this->oBD->escapar($filtros["fechainicio"]);
			}
			if(!empty($filtros["fechafin"])) {
					$cond[] = "fechafin = " . $this->oBD->escapar($filtros["fechafin"]);
			}
			if(!empty($filtros["dias"])) {
					$cond[] = "dias = " . $this->oBD->escapar($filtros["dias"]);
			}
			if(!empty($filtros["horas"])) {
					$cond[] = "horas = " . $this->oBD->escapar($filtros["horas"]);
			}
			if(!empty($filtros["horainicio"])) {
					$cond[] = "horainicio = " . $this->oBD->escapar($filtros["horainicio"]);
			}
			if(!empty($filtros["horafin"])) {
					$cond[] = "horafin = " . $this->oBD->escapar($filtros["horafin"]);
			}
			if(!empty($filtros["valor"])) {
					$cond[] = "valor = " . $this->oBD->escapar($filtros["valor"]);
			}
			if(!empty($filtros["valor_asi"])) {
					$cond[] = "valor_asi = " . $this->oBD->escapar($filtros["valor_asi"]);
			}
			if(!empty($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(!empty($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(!empty($filtros["matriculados"])) {
					$cond[] = "matriculados = " . $this->oBD->escapar($filtros["matriculados"]);
			}
			if(!empty($filtros["nivel"])) {
					$cond[] = "nivel = " . $this->oBD->escapar($filtros["nivel"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Grupos").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM grupos  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Grupos").": " . $e->getMessage());
		}
	}
	
	public function insertar($iddocente,$idlocal,$idambiente,$idasistente,$fechainicio,$fechafin,$dias,$horas,$horainicio,$horafin,$valor,$valor_asi,$regusuario,$regfecha,$matriculados,$nivel,$codigo)
	{
		try {
			
			$this->iniciarTransaccion('dat_grupos_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupo) FROM grupos");
			++$id;
			
			$estados = array('idgrupo' => $id
							
							,'iddocente'=>$iddocente
							,'idlocal'=>$idlocal
							,'idambiente'=>$idambiente
							,'idasistente'=>$idasistente
							,'fechainicio'=>$fechainicio
							,'fechafin'=>$fechafin
							,'dias'=>$dias
							,'horas'=>$horas
							,'horainicio'=>$horainicio
							,'horafin'=>$horafin
							,'valor'=>$valor
							,'valor_asi'=>$valor_asi
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'matriculados'=>$matriculados
							,'nivel'=>$nivel
							,'codigo'=>$codigo							
							);
			
			$this->oBD->insert('grupos', $estados);			
			$this->terminarTransaccion('dat_grupos_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_grupos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Grupos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $iddocente,$idlocal,$idambiente,$idasistente,$fechainicio,$fechafin,$dias,$horas,$horainicio,$horafin,$valor,$valor_asi,$regusuario,$regfecha,$matriculados,$nivel,$codigo)
	{
		try {
			$this->iniciarTransaccion('dat_grupos_update');
			$estados = array('iddocente'=>$iddocente
							,'idlocal'=>$idlocal
							,'idambiente'=>$idambiente
							,'idasistente'=>$idasistente
							,'fechainicio'=>$fechainicio
							,'fechafin'=>$fechafin
							,'dias'=>$dias
							,'horas'=>$horas
							,'horainicio'=>$horainicio
							,'horafin'=>$horafin
							,'valor'=>$valor
							,'valor_asi'=>$valor_asi
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'matriculados'=>$matriculados
							,'nivel'=>$nivel
							,'codigo'=>$codigo								
							);
			
			$this->oBD->update('grupos ', $estados, array('idgrupo' => $id));
		    $this->terminarTransaccion('dat_grupos_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Grupos").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM grupos  "
					. " WHERE idgrupo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Grupos").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('grupos', array('idgrupo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Grupos").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('grupos', array($propiedad => $valor), array('idgrupo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Grupos").": " . $e->getMessage());
		}
	}
   
		
}