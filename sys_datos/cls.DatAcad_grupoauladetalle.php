<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		06-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_grupoauladetalle extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_grupoauladetalle";
			
			$cond = array();		
			
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
					$cond[] = "idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT ag.idgrupoaula,ag.nombre as strgrupoaula, comentario,nvacantes, idgrupoauladetalle, agd.nombre ,fecha_inicio,fecha_final, tipo, (SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, estado, (SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, idcurso, (SELECT nombre FROM acad_curso ac WHERE agd.idcurso=ac.idcurso) AS strcurso , iddocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE agd.iddocente=dni) AS strdocente, idlocal, (SELECT nombre FROM local l WHERE agd.idlocal=l.idlocal ) AS strlocal, idambiente, (SELECT CONCAT(numero,' ',(SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE agd.idambiente=am.idambiente) AS strambiente FROM acad_grupoaula ag INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula";	
			$cond = array();
			if(isset($filtros["tipo"])) {
					$cond[] = "ag.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "ag.estado = " . $this->oBD->escapar($filtros["estado"]);
			}		
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "agd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "agd.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "agd.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
					$cond[] = "agd.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["fecha_inicio_mayorigual"])) {
					$cond[] = "fecha_inicio >= " . $this->oBD->escapar($filtros["fecha_inicio_mayorigual"]);
			}
			if(isset($filtros["fecha_inicio_menorigual"])) {
					$cond[] = "fecha_inicio <= " . $this->oBD->escapar($filtros["fecha_inicio_menorigual"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}	
			if(isset($filtros["fecha_final_menorigual"])) {
					$cond[] = "fecha_final <= " . $this->oBD->escapar($filtros["fecha_final_menorigual"]);
			}	
			if(isset($filtros["fecha_final_mayorigual"])) {
					$cond[] = "fecha_final >= " . $this->oBD->escapar($filtros["fecha_final_mayorigual"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}						
			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function cursosDocente($filtros=null)
	{
		try {
			$sql = "SELECT GAD.idgrupoauladetalle, GAD.iddocente, GAD.fecha_inicio, GAD.fecha_final, C.*, PC.idproyecto FROM acad_grupoauladetalle GAD JOIN acad_curso C ON GAD.idcurso=C.idcurso JOIN proyecto_cursos PC ON PC.idcurso=C.idcurso";	
			
			$cond = array();


			if(isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "GAD.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idgrupoaula"])) {
				$cond[] = "GAD.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "PC.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
				$cond[] = "GAD.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
				$cond[] = "GAD.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
				$cond[] = "GAD.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "PC.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			//$cond[] = " GA.estado = 1 ";
			$cond[] = " C.estado = 1 ";
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo '<p><b>' . $sql . '</b></p>';
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}

	public function anios(){
		try{
			$sql='SELECT DISTINCT(YEAR(fecha_inicio)) as anios FROM `acad_grupoauladetalle` Order By anios DESC';
			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function filtrogrupos($filtros){
		try{
			$sql='SELECT DISTINCT(ag.idgrupoaula),ag.nombre as strgrupoaula, agd.nombre ,year(fecha_inicio) as anio, tipo, estado, idlocal FROM acad_grupoaula ag INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula';

			$cond = array();
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}

			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}

			if(isset($filtros["anio"])) {
					$cond[] = "year(fecha_inicio) = " . $this->oBD->escapar($filtros["anio"]);
			}

			if(!empty($cond)){
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//echo $sql;

			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function mis_ambientes($filtros){
		try{
			$sql="SELECT DISTINCT
				  A.*
				FROM
				  acad_grupoaula GA
				INNER JOIN acad_grupoauladetalle GAD ON GA.idgrupoaula = GAD.idgrupoaula
				INNER JOIN ambiente A ON GAD.idambiente = A.idambiente";

			$cond = array();
			if(isset($filtros["tipo"])) {
					$cond[] = "GA.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}

			if(isset($filtros["estado"])) {
					$cond[] = "GA.estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			if(isset($filtros["idlocal"])) {
					$cond[] = "GAD.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}

			if(isset($filtros["idambiente"])) {
					$cond[] = "GAD.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}

			if(isset($filtros["anio"])) {
					$cond[] = "year(GAD.fecha_inicio) = " . $this->oBD->escapar($filtros["anio"]);
			}

			if(!empty($cond)){
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//echo $sql;

			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function vermarcacion($filtros){
		try{
			$sql="SELECT ag.idgrupoaula,ag.nombre AS strgrupoaula, agd.idgrupoauladetalle, agd.nombre ,agd.fecha_inicio,agd.fecha_final, tipo, (SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, estado, (SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, idcurso, (SELECT nombre FROM acad_curso ac WHERE agd.idcurso=ac.idcurso) AS strcurso , iddocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE agd.iddocente=dni) AS strdocente, idlocal, (SELECT nombre FROM local l WHERE agd.idlocal=l.idlocal ) AS strlocal, idambiente, (SELECT CONCAT(numero,' ',(SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE agd.idambiente=am.idambiente) AS strambiente,hg.fecha_finicio as hora_entrada, hg.fecha_final as hora_salida,hg.idhorario FROM acad_grupoaula ag INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula LEFT JOIN acad_horariogrupodetalle hg on agd.idgrupoauladetalle=hg.idgrupoauladetalle";
			
			if(isset($filtros["iddocente"])) {
					$cond[] = "agd.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "agd.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "agd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			//$order='';
			if(isset($filtros["fechainicio"])){
					$fechamayor=new DateTime($filtros["fechainicio"]);
					$fechamayor->modify('+15 minutes');
					$f1=$fechamayor->format('Y-m-d H:i:s');
					$fechamenor=new DateTime($filtros["fechainicio"]);
					$fechamenor->modify('-15 minutes');
					$f2=$fechamenor->format('Y-m-d H:i:s');
					$cond[] = "(hg.fecha_finicio between " . $this->oBD->escapar($f2)." AND " . $this->oBD->escapar($f1).")";
			}

			if(isset($filtros["fechafinal"])){
					$fechamayor=new DateTime($filtros["fechafinal"]);
					$fechamayor->modify('+5 minutes');
					$f1=$fechamayor->format('Y-m-d H:i:s');
					$fechamenor=new DateTime($filtros["fechafinal"]);
					$fechamenor->modify('-5 minutes');
					$f2=$fechamenor->format('Y-m-d H:i:s');
					$cond[] = "(hg.fecha_final between " . $this->oBD->escapar($f2)." AND " . $this->oBD->escapar($f1).")";
			}

			if(isset($filtros["fechanext"])){
					$f1=$filtros["fechanext"];					
					$cond[] = "hg.fecha_final > " . $this->oBD->escapar($f1);
			}

			$cond[] = "estado =1 ";
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			//echo $sql;
			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
		
	public function insertar($idgrupoaula,$idcurso,$iddocente,$idlocal,$idambiente,$nombre,$fecha_inicio,$fecha_final)
	{
		try {			
			$this->iniciarTransaccion('dat_acad_grupoauladetalle_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupoauladetalle) FROM acad_grupoauladetalle");
			++$id;			
			$estados = array('idgrupoauladetalle' => $id							
							,'idgrupoaula'=>$idgrupoaula
							,'idcurso'=>$idcurso
							,'iddocente'=>$iddocente
							,'idlocal'=>$idlocal
							,'idambiente'=>$idambiente
							,'nombre'=>$nombre
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final							
							);
			
			$this->oBD->insert('acad_grupoauladetalle', $estados);			
			$this->terminarTransaccion('dat_acad_grupoauladetalle_insert');			
			return $id;
		} catch(Exception $e){
			$this->cancelarTransaccion('dat_acad_grupoauladetalle_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idgrupoaula,$idcurso,$iddocente,$idlocal,$idambiente,$nombre,$fecha_inicio,$fecha_final)
	{
		try {
			$this->iniciarTransaccion('dat_acad_grupoauladetalle_update');
			$estados = array('idgrupoaula'=>$idgrupoaula
							,'idcurso'=>$idcurso
							,'iddocente'=>$iddocente
							,'idlocal'=>$idlocal
							,'idambiente'=>$idambiente
							,'nombre'=>$nombre
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final
							);
			
			$this->oBD->update('acad_grupoauladetalle ', $estados, array('idgrupoauladetalle' => $id));
		    $this->terminarTransaccion('dat_acad_grupoauladetalle_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT ag.idgrupoaula,ag.nombre as strgrupoaula, comentario,nvacantes, idgrupoauladetalle, agd.nombre ,fecha_inicio,fecha_final, tipo, (SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, estado, (SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, idcurso, (SELECT nombre FROM acad_curso ac WHERE agd.idcurso=ac.idcurso) AS strcurso , iddocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE agd.iddocente=dni) AS strdocente, idlocal, (SELECT nombre FROM local l WHERE agd.idlocal=l.idlocal ) AS strlocal, idambiente, (SELECT CONCAT(numero,' ',(SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE agd.idambiente=am.idambiente) AS strambiente FROM acad_grupoaula ag INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula  WHERE idgrupoauladetalle = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_grupoauladetalle', array('idgrupoauladetalle' => $id));

		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_grupoauladetalle', array($propiedad => $valor), array('idgrupoauladetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
   
		
}