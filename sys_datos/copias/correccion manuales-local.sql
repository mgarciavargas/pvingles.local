-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-01-2018 a las 23:22:36
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manuales`
--

DROP TABLE IF EXISTS `manuales`;
CREATE TABLE `manuales` (
  `idmanual` int(11) NOT NULL,
  `titulo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `abreviado` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `manuales`
--

INSERT INTO `manuales` (`idmanual`, `titulo`, `abreviado`, `stock`, `total`) VALUES
(1, 'Libro de Trabajo A1', 'ING01', 1000, 1000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manuales_alumno`
--

DROP TABLE IF EXISTS `manuales_alumno`;
CREATE TABLE `manuales_alumno` (
  `identrega` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  `idalumno` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idmanual` int(11) NOT NULL,
  `idaulagrupodetalle` int(11) DEFAULT NULL,
  `comentario` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha` date NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `serie` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `manuales_alumno`
--

INSERT INTO `manuales_alumno` (`identrega`, `idgrupo`, `idalumno`, `idmanual`, `idaulagrupodetalle`, `comentario`, `fecha`, `estado`, `serie`) VALUES
(1, 2, '43831104', 1, 2, '', '2018-01-17', '1', ''),
(2, 4, '72042592', 1, 4, '', '2018-01-17', '1', ''),
(3, 2, '55555555', 1, 2, '', '2018-01-17', '1', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material_ayuda`
--

DROP TABLE IF EXISTS `material_ayuda`;
CREATE TABLE `material_ayuda` (
  `idmaterial` int(11) NOT NULL,
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `archivo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `material_ayuda`
--

INSERT INTO `material_ayuda` (`idmaterial`, `nombre`, `archivo`, `tipo`, `descripcion`, `mostrar`) VALUES
(1, 'video ayuda01', '/static/media/material_ayuda/vid-20180118043519.mp4', 'vid', 'asdasdasd', 1),
(2, 'logo', '/static/media/material_ayuda/img-20180118051644.jpg', 'img', 'eeeeeeee', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `manuales`
--
ALTER TABLE `manuales`
  ADD PRIMARY KEY (`idmanual`);

--
-- Indices de la tabla `manuales_alumno`
--
ALTER TABLE `manuales_alumno`
  ADD PRIMARY KEY (`identrega`);

--
-- Indices de la tabla `material_ayuda`
--
ALTER TABLE `material_ayuda`
  ADD PRIMARY KEY (`idmaterial`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
