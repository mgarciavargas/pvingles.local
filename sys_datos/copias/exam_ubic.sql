-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2018 a las 21:29:51
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles.local`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_ubicacion`
--

CREATE TABLE `examen_ubicacion` (
  `idexamen_ubicacion` int(11) NOT NULL,
  `idrecurso` int(11) NOT NULL COMMENT 'idexamen de SmartQuiz',
  `idcurso` int(11) NOT NULL,
  `tipo` varchar(4) COLLATE utf8_unicode_ci NOT NULL COMMENT 'exam. ubicación de: [N]ivel, [U]nidad',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `rango_min` float(5,2) DEFAULT NULL COMMENT 'rango mínimo para poder darlo',
  `rango_max` float(5,2) DEFAULT NULL,
  `idexam_prerequisito` int(11) DEFAULT '0' COMMENT 'idexamen_ubicacion que necesita aprobar primero'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `examen_ubicacion`
--
ALTER TABLE `examen_ubicacion`
  ADD PRIMARY KEY (`idexamen_ubicacion`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
