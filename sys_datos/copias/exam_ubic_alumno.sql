-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2018 a las 21:30:04
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles.local`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_ubicacion_alumno`
--

CREATE TABLE `examen_ubicacion_alumno` (
  `idexam_alumno` int(11) NOT NULL,
  `idexamen` int(11) NOT NULL COMMENT 'idexamen_ubicacion',
  `idalumno` int(11) NOT NULL,
  `estado` varchar(4) COLLATE utf8_spanish_ci NOT NULL COMMENT '[O]mitido ; [T]omado ; [TU] Tomado y Ubicado',
  `resultado` text COLLATE utf8_spanish_ci,
  `fecha` timestamp NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `examen_ubicacion_alumno`
--
ALTER TABLE `examen_ubicacion_alumno`
  ADD PRIMARY KEY (`idexam_alumno`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
