-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-01-2018 a las 23:20:54
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `idpersona` bigint(20) NOT NULL,
  `tipodoc` tinyint(4) NOT NULL,
  `dni` int(11) NOT NULL,
  `ape_paterno` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `ape_materno` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `fechanac` date DEFAULT NULL,
  `sexo` char(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `estado_civil` char(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `ubigeo` char(8) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `urbanizacion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `celular` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(75) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `idugel` char(4) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `regusuario` int(11) NOT NULL DEFAULT '0',
  `regfecha` date NOT NULL,
  `usuario` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `token` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `rol` int(11) NOT NULL,
  `foto` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `situacion` char(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `idioma` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'EN'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`idpersona`, `tipodoc`, `dni`, `ape_paterno`, `ape_materno`, `nombre`, `fechanac`, `sexo`, `estado_civil`, `ubigeo`, `urbanizacion`, `direccion`, `telefono`, `celular`, `email`, `idugel`, `regusuario`, `regfecha`, `usuario`, `clave`, `token`, `rol`, `foto`, `estado`, `situacion`, `idioma`) VALUES
(1, 1, 43831104, 'Chingo', 'Tello', 'Abel', '1986-10-27', 'M', 'C', '', '', '', '942171837', '', 'smartclass@abacoeducacion.org', '', 0, '2017-11-11', 'admin', '0192023a7bbd73250516f069df18b500', 'a3b1da834b82e4b6887d54de54f29df9', 1, '43831104 -20180116062511.jpg', 1, '1', 'EN'),
(2, 1, 43371672, 'Muñoz', 'Meza', 'Evelio', '2016-11-14', 'M', 'C', '14010101', 'MARIA PARA DE BELLIDO', 'MEXICO 790', '251924', '979222660', 'emunozmeza@gmail.com', '0001', 1, '2016-11-14', '43371672', 'c4ca4238a0b923820dcc509a6f75849b', 'aaa18d9d0452b32a66696f430e3b617d', 1, '', 1, '1', 'EN'),
(3, 1, 72042592, 'Figueroa', 'Piscoya', 'Eder', '2018-01-16', 'M', 'S', '', '', '', '98989898', '', 'eder.figueroa@outlook.com', '', 0, '2016-12-26', 'eder.figueroa', '81dc9bdb52d04dc20036dbd8313ed055', '311b07c848249765d17cfe66a3609fa9', 1, '72042592 -20180116062353.jpg', 1, '1', 'EN'),
(4, 1, 75935728, 'Soporte', 'System', 'SmartClass', '1992-06-21', 'F', 'S', '', '', '', '', '', 'smartclass@abacoeducacion.org', '', 0, '2018-01-31', 'soporte', '012c5d6d1f7dee260b391264fb2f7b17', 'd81cfe5a19e57f898235bd8921ecbcfd', 1, '', 1, '1', 'ES'),
(5, 1, 10101011, 'Monja', 'Lopez', 'Yolanda', '2017-02-01', 'F', 'S', '', '', '', '', '', 'yoly4816@hotmail.com', '', 0, '2017-10-02', 'yoly4816', 'e10adc3949ba59abbe56e057f20f883e', 'e10adc3949ba59abbe56e057f20f883e', 1, '', 1, '1', 'EN'),
(6, 1, 9678965, 'Castro', 'Alzamora', 'Manuel', '1966-10-31', 'M', 'S', '', '', '', '979797998', '', 'mcmanolito@gmail.com', '', 0, '2017-02-09', 'mcastro', '9e5cafda6f6da794e3d64fd1baa72867', '9e5cafda6f6da794e3d64fd1baa72867', 1, '55555555-20180116062131.jpg', 1, '1', 'EN'),
(7, 1, 88888888, 'Mamut', '', 'Sharon', '2017-02-01', 'F', 'S', '', '', '', '', '', 'smamud@hotmail.com', '', 0, '2017-02-09', 'smamud', '2f348b9c79d4303031bf7c42ecfe6fbd', '4fdaaf59bc6095b1b6c4276062efc5c4', 1, '', 1, '1', 'EN'),
(8, 1, 21212121, 'Morante', 'Chumpen', 'Monica', '2018-01-31', 'F', 'C', '', '', '', '', '', 'mmorante@hotmail.com', '', 0, '2017-06-22', 'monica', 'f32e15e7af11d77eac7ca295b3e9a068', '7fee84ec6965e589e8623dd446f6c764', 1, '', 1, '1', 'EN'),
(9, 1, 10101010, 'Sandoval', 'Cueva', 'Claudia', '1906-06-06', 'F', 'S', '', '', '', '', '', 'sincorreo@hotmail.com', '', 0, '2017-10-02', 'claudiasc', '0192023a7bbd73250516f069df18b500', '3b71ffe8616189ba45ca0f8255ab066c', 1, '', 1, '1', 'EN'),
(10, 1, 12121212, 'Testing', 'Test', 'Samuel', '2017-11-01', '', '', '', '', '', '', '', 'samuel@gmail.com', '', 0, '2017-11-30', 'samuel', '0192023a7bbd73250516f069df18b500', '', 1, '', 1, '1', 'EN'),
(11, 1, 65300999, 'Huamanchumo', 'Arrelucea', 'Angie', '2018-01-01', 'F', 'S', '101010', '-', '-', '933128000', '933128000', 'anha16@hotmail.com', '01', 1, '2018-01-04', 'anha16', 'e1ee2e1df105b85e26c449b9bffe10de', 'internetmovil', 1, '', 1, '1', 'EN');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`idpersona`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
