-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2018 a las 21:46:25
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles.local`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora_smartbook`
--

CREATE TABLE `bitacora_smartbook` (
  `idbitacora` int(11) NOT NULL,
  `idbitacora_alum_smartbook` int(11) NOT NULL,
  `idcurso` int(11) DEFAULT NULL,
  `idsesion` int(11) DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL COMMENT 'idalumno',
  `pestania` varchar(128) NOT NULL,
  `total_pestanias` int(11) NOT NULL,
  `fechahora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `progreso` float(5,2) NOT NULL,
  `otros_datos` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bitacora_smartbook`
--

INSERT INTO `bitacora_smartbook` (`idbitacora`, `idbitacora_alum_smartbook`, `idcurso`, `idsesion`, `idusuario`, `pestania`, `total_pestanias`, `fechahora`, `progreso`, `otros_datos`) VALUES
(2, 1, 1, 18, 72042592, 'div_audios', 8, '2018-01-26 15:03:52', 100.00, '["__xRUTABASEx__/static/media/audio/N1-U1-A1-20170218101501.mp3"]'),
(3, 1, 1, 18, 72042592, 'div_imagen', 8, '2018-01-26 15:06:22', 100.00, '["__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061349.jpg","__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061135.jpg","__xRUTABASEx__/static/media/image/N1-U1-A1-20170314105159.jpg"]'),
(4, 1, 1, 18, 72042592, 'div_videos', 8, '2018-01-26 15:06:52', 100.00, '["__xRUTABASEx__/static/media/video/N1-U1-A1-20170220062626.mp4","__xRUTABASEx__/static/media/video/N1-U1-A1-20170220062626.mp4"]'),
(5, 1, 1, 18, 72042592, 'div_look', 8, '2018-03-16 00:24:25', 100.00, NULL),
(6, 1, 1, 18, 72042592, 'div_practice', 8, '2018-01-26 15:11:24', 56.30, NULL),
(7, 1, 1, 18, 72042592, 'div_voca', 8, '2018-01-26 15:11:39', 100.00, NULL),
(8, 1, 1, 18, 72042592, 'div_autoevaluar', 8, '2018-01-26 15:12:12', 16.70, NULL),
(1, 1, 1, 18, 72042592, 'div_pdfs', 8, '2018-01-26 15:02:08', 100.00, '["__xRUTABASEx__/static/media/pdf/N1-U1-A1-20170221095258.pdf"]'),
(11, 3, NULL, NULL, NULL, 'div_look', 8, '2018-02-28 16:00:59', 100.00, NULL),
(10, 2, NULL, NULL, NULL, 'div_practice', 4, '2018-02-28 00:38:24', 18.80, NULL),
(9, 2, NULL, NULL, NULL, 'div_look', 4, '2018-02-27 16:17:56', 100.00, NULL),
(12, 87, NULL, NULL, NULL, 'div_look', 8, '2018-03-16 17:24:01', 100.00, NULL),
(13, 87, NULL, NULL, NULL, 'div_voca', 8, '2018-03-16 17:24:21', 100.00, NULL),
(14, 87, NULL, NULL, NULL, 'div_practice', 8, '2018-03-16 17:24:29', 5.30, NULL),
(15, 87, NULL, NULL, NULL, 'div_workbook', 8, '2018-03-16 16:55:54', 100.00, NULL),
(16, 88, NULL, NULL, NULL, 'div_look', 2, '2018-03-19 23:35:42', 100.00, NULL),
(17, 88, NULL, NULL, NULL, 'div_practice', 2, '2018-03-19 23:50:03', 0.00, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bitacora_smartbook`
--
ALTER TABLE `bitacora_smartbook`
  ADD PRIMARY KEY (`idbitacora`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
