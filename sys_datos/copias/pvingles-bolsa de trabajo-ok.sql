-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-01-2018 a las 16:21:53
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bolsa_empresas`
--

DROP TABLE IF EXISTS `bolsa_empresas`;
CREATE TABLE `bolsa_empresas` (
  `idempresa` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `rason_social` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ruc` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `logo` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `representante` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuario` varchar(75) COLLATE utf8_spanish_ci DEFAULT NULL,
  `clave` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `correo` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `bolsa_empresas` (`idempresa`, `nombre`, `rason_social`, `ruc`, `logo`, `direccion`, `telefono`, `representante`, `usuario`, `clave`, `correo`, `estado`) VALUES
(1, 'Abaco', 'Educación', '20204283751', '/static/media/empresas/logo-20180129113119.jpg', 'Luis gonzales 146, chiclayo , Lambayeque', '9598847568', 'Manuel Castro', 'mcastro', '0192023a7bbd73250516f069df18b500', 'mcmanuelito@gmail.com', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bolsa_postulante`
--

DROP TABLE IF EXISTS `bolsa_postulante`;
CREATE TABLE `bolsa_postulante` (
  `idpostular` int(11) NOT NULL,
  `fecharegistro` datetime NOT NULL,
  `nombrecompleto` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` tinyint(4) NOT NULL,
  `idpublicacion` int(11) NOT NULL,
  `idpostulante` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `bolsa_postulante` (`idpostular`, `fecharegistro`, `nombrecompleto`, `telefono`, `correo`, `descripcion`, `mostrar`, `idpublicacion`, `idpostulante`) VALUES
(2, '2018-01-29 11:43:10', 'Monja Lopez, Yolanda', '984578123', 'yoly4816@hotmail.com', ' Soy Yolanda experta en inglés. tu me necesitas yo soy tu mejor opción... ', 1, 2, 10101011),
(1, '2018-01-29 11:41:08', 'abel Chingo Tello', '979709503', 'abelchingo@gmail.com', 'Me gustaria participar.. saludos ', 1, 1, 43831104);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bolsa_publicaciones`
--

DROP TABLE IF EXISTS `bolsa_publicaciones`;
CREATE TABLE `bolsa_publicaciones` (
  `idpublicacion` int(11) NOT NULL,
  `idempresa` int(11) NOT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `sueldo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nvacantes` smallint(4) NOT NULL,
  `disponibilidadeviaje` int(11) NOT NULL,
  `duracioncontrato` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `xtiempo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecharegistro` date NOT NULL,
  `fechapublicacion` datetime NOT NULL,
  `cambioderesidencia` int(11) NOT NULL,
  `mostrar` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
INSERT INTO `bolsa_publicaciones` (`idpublicacion`, `idempresa`, `titulo`, `descripcion`, `sueldo`, `nvacantes`, `disponibilidadeviaje`, `duracioncontrato`, `xtiempo`, `fecharegistro`, `fechapublicacion`, `cambioderesidencia`, `mostrar`) VALUES
(2, 1, 'Profesora de Ingles', 'Se necesita una profesional experta en el idioma Inglés para desarrollar material didáctico  \r\nque domine  la computadora y que conozca sobre programas curriculares.', '1000', 1, 1, 'de 1 a 3 Meses', 'Tiempo completo', '2018-01-29', '2018-01-29 11:49:48', 1, 1),
(1, 1, 'Ing. de Sistemas', '1. Con experiencia\r\na). En lenguajes de programacion :\r\n     PHP, Java, Nodejs\r\nb). En base de Datos.\r\n     Mysql, Postgres, Sql Server\r\n2. Que se proactivo   y le guste los retos.', '1500', 3, 1, 'de 3 a 6 Meses', 'Tiempo completo', '2018-01-29', '2018-01-29 11:35:25', 0, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bolsa_empresas`
--
ALTER TABLE `bolsa_empresas`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indices de la tabla `bolsa_postulante`
--
ALTER TABLE `bolsa_postulante`
  ADD PRIMARY KEY (`idpostular`);

--
-- Indices de la tabla `bolsa_publicaciones`
--
ALTER TABLE `bolsa_publicaciones`
  ADD PRIMARY KEY (`idpublicacion`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
