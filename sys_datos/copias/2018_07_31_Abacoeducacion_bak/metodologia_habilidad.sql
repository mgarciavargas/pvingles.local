INSERT INTO `metodologia_habilidad` (`idmetodologia`, `nombre`, `tipo`, `estado`) VALUES (1, 'Look', 'M', 1);
INSERT INTO `metodologia_habilidad` (`idmetodologia`, `nombre`, `tipo`, `estado`) VALUES (2, 'Practice', 'M', 1);
INSERT INTO `metodologia_habilidad` (`idmetodologia`, `nombre`, `tipo`, `estado`) VALUES (3, 'Do it by yourself', 'M', 1);
INSERT INTO `metodologia_habilidad` (`idmetodologia`, `nombre`, `tipo`, `estado`) VALUES (4, 'Listening', 'H', 1);
INSERT INTO `metodologia_habilidad` (`idmetodologia`, `nombre`, `tipo`, `estado`) VALUES (5, 'Reading', 'H', 1);
INSERT INTO `metodologia_habilidad` (`idmetodologia`, `nombre`, `tipo`, `estado`) VALUES (6, 'Writing', 'H', 1);
INSERT INTO `metodologia_habilidad` (`idmetodologia`, `nombre`, `tipo`, `estado`) VALUES (7, 'Speaking', 'H', 1);
INSERT INTO `metodologia_habilidad` (`idmetodologia`, `nombre`, `tipo`, `estado`) VALUES (8, 'Grammar / Structure', 'H', 1);
INSERT INTO `metodologia_habilidad` (`idmetodologia`, `nombre`, `tipo`, `estado`) VALUES (9, 'Vocabulary', 'H', 1);
