-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-08-2018 a las 16:03:08
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles.local`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_cursohabilidad`
--

CREATE TABLE `acad_cursohabilidad` (
  `idcursohabilidad` int(11) NOT NULL,
  `texto` text COLLATE utf8_spanish_ci NOT NULL,
  `tipo` smallint(6) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `idcursodetalle` int(11) DEFAULT NULL,
  `idpadre` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_cursohabilidad`
--

INSERT INTO `acad_cursohabilidad` (`idcursohabilidad`, `texto`, `tipo`, `idcurso`, `idcursodetalle`, `idpadre`) VALUES
(10, 'Habilidad 01, habilidad para probar los registros. Lograr que estas aparezcan en donde se les llamen.', 1, 2, 99, 0),
(11, 'Habilidad 02, para seguir probando la tabla.', 1, 2, 99, 0),
(12, 'Habilidad 03, debe listarse en la sesion 01.', 1, 2, 99, 0),
(13, 'Habilidad 01 de sesión 02, lograr mostrarse correctamente en sesión 02', 1, 2, 100, 0),
(14, 'Habilidad 02, para seguir probando la tabla . esto en sesion 02', 1, 2, 100, 0),
(4, 'Listening', 1, 1, NULL, 0),
(5, 'Reading', 1, 1, NULL, 0),
(6, 'Writing', 1, 1, NULL, 0),
(7, 'Speaking', 1, 1, NULL, 0),
(8, 'Grammar / Structure', 1, 1, NULL, 0),
(9, 'Vocabulary', 1, 1, NULL, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acad_cursohabilidad`
--
ALTER TABLE `acad_cursohabilidad`
  ADD PRIMARY KEY (`idcursohabilidad`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
