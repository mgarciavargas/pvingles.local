/*
Navicat MySQL Data Transfer

Source Server         : AbacoEducacion.org
Source Server Version : 50635
Source Host           : abacoeducacion.org:3306
Source Database       : abacoedu_smartlearn

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2018-07-31 18:03:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for acad_categorias
-- ----------------------------
DROP TABLE IF EXISTS `acad_categorias`;
CREATE TABLE `acad_categorias` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `idpadre` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for acad_curso
-- ----------------------------
DROP TABLE IF EXISTS `acad_curso`;
CREATE TABLE `acad_curso` (
  `idcurso` int(1) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(1) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idusuario` int(11) NOT NULL,
  `vinculosaprendizajes` text COLLATE utf8_spanish_ci,
  `materialesyrecursos` text COLLATE utf8_spanish_ci,
  `color` varchar(35) COLLATE utf8_spanish_ci DEFAULT NULL,
  `objetivos` text COLLATE utf8_spanish_ci,
  `certificacion` text COLLATE utf8_spanish_ci,
  `costo` float NOT NULL DEFAULT '0',
  `silabo` text COLLATE utf8_spanish_ci,
  `e_ini` text COLLATE utf8_spanish_ci,
  `pdf` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `abreviado` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `e_fin` int(11) NOT NULL,
  `aniopublicacion` date DEFAULT '0000-00-00',
  `autor` varchar(200) COLLATE utf8_spanish_ci DEFAULT '',
  `txtjson` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idcurso`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for acad_cursoaprendizaje
-- ----------------------------
DROP TABLE IF EXISTS `acad_cursoaprendizaje`;
CREATE TABLE `acad_cursoaprendizaje` (
  `idaprendizaje` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `idcursodetalle` int(11) NOT NULL,
  `idpadre` int(11) NOT NULL,
  `idrecurso` int(11) DEFAULT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`idaprendizaje`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for acad_cursocategoria
-- ----------------------------
DROP TABLE IF EXISTS `acad_cursocategoria`;
CREATE TABLE `acad_cursocategoria` (
  `id` int(11) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for acad_cursodetalle
-- ----------------------------
DROP TABLE IF EXISTS `acad_cursodetalle`;
CREATE TABLE `acad_cursodetalle` (
  `idcursodetalle` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `orden` bigint(20) NOT NULL,
  `idrecurso` int(11) NOT NULL,
  `tiporecurso` varchar(2) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Examen, nivel , rubrica,etc',
  `idlogro` int(11) NOT NULL,
  `url` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `idpadre` smallint(6) NOT NULL,
  `color` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `esfinal` tinyint(4) DEFAULT '0',
  `txtjson` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idcursodetalle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for acad_cursosesion
-- ----------------------------
DROP TABLE IF EXISTS `acad_cursosesion`;
CREATE TABLE `acad_cursosesion` (
  `idsesion` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `duracion` time DEFAULT NULL,
  `informaciongeneral` longtext,
  `idcursodetalle` int(11) NOT NULL,
  PRIMARY KEY (`idsesion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for acad_grupoaula
-- ----------------------------
DROP TABLE IF EXISTS `acad_grupoaula`;
CREATE TABLE `acad_grupoaula` (
  `idgrupoaula` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'virtual,presencial,mixto',
  `comentario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `nvacantes` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`idgrupoaula`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for acad_grupoauladetalle
-- ----------------------------
DROP TABLE IF EXISTS `acad_grupoauladetalle`;
CREATE TABLE `acad_grupoauladetalle` (
  `idgrupoauladetalle` bigint(20) NOT NULL,
  `idgrupoaula` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `iddocente` int(11) NOT NULL,
  `idlocal` int(11) DEFAULT '0',
  `idambiente` int(11) DEFAULT '0' COMMENT 'aula física o laboratorio donde se dará la clase',
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_final` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idgrupoauladetalle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for acad_horariogrupodetalle
-- ----------------------------
DROP TABLE IF EXISTS `acad_horariogrupodetalle`;
CREATE TABLE `acad_horariogrupodetalle` (
  `idhorario` int(11) NOT NULL,
  `idgrupoauladetalle` int(11) NOT NULL,
  `fecha_inicio` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fecha_final` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descripcion` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `color` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idhorariopadre` tinyint(4) NOT NULL,
  `diasemana` tinyint(4) NOT NULL,
  PRIMARY KEY (`idhorario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for acad_marcacion
-- ----------------------------
DROP TABLE IF EXISTS `acad_marcacion`;
CREATE TABLE `acad_marcacion` (
  `idmarcacion` int(11) NOT NULL,
  `idgrupodetalle` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `idhorario` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `horaingreso` time DEFAULT NULL,
  `horasalida` time DEFAULT NULL,
  `iddocente` int(11) NOT NULL,
  `nalumnos` smallint(6) DEFAULT NULL,
  `observacion` text COLLATE utf8_spanish_ci,
  PRIMARY KEY (`idmarcacion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for acad_matricula
-- ----------------------------
DROP TABLE IF EXISTS `acad_matricula`;
CREATE TABLE `acad_matricula` (
  `idmatricula` int(11) NOT NULL,
  `idgrupoauladetalle` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fecha_matricula` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmatricula`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for acad_sesionhtml
-- ----------------------------
DROP TABLE IF EXISTS `acad_sesionhtml`;
CREATE TABLE `acad_sesionhtml` (
  `idsesion` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `idcursodetalle` int(11) NOT NULL,
  `html` longtext COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idsesion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for actividad_alumno
-- ----------------------------
DROP TABLE IF EXISTS `actividad_alumno`;
CREATE TABLE `actividad_alumno` (
  `idactalumno` int(11) NOT NULL,
  `iddetalleactividad` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `porcentajeprogreso` float NOT NULL,
  `habilidades` varchar(32) NOT NULL,
  `estado` char(2) NOT NULL COMMENT 'termino, pendiente',
  `html_solucion` longtext NOT NULL,
  PRIMARY KEY (`idactalumno`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for actividad_detalle
-- ----------------------------
DROP TABLE IF EXISTS `actividad_detalle`;
CREATE TABLE `actividad_detalle` (
  `iddetalle` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `pregunta` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT '1',
  `url` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'video. Arratre',
  `tipo_desarrollo` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'c=captivate, D=codigo, M=mixto',
  `tipo_actividad` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Examen,Actividad',
  `idhabilidad` varchar(16) COLLATE utf8_spanish_ci DEFAULT NULL,
  `texto` longtext COLLATE utf8_spanish_ci,
  `texto_edit` longtext COLLATE utf8_spanish_ci,
  `titulo` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_spanish_ci,
  `idpersonal` int(11) DEFAULT NULL,
  PRIMARY KEY (`iddetalle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for actividades
-- ----------------------------
DROP TABLE IF EXISTS `actividades`;
CREATE TABLE `actividades` (
  `idactividad` int(11) NOT NULL,
  `nivel` int(11) NOT NULL,
  `unidad` int(11) NOT NULL,
  `sesion` int(11) NOT NULL,
  `metodologia` int(11) NOT NULL,
  `habilidad` varchar(16) DEFAULT NULL,
  `titulo` varchar(250) DEFAULT NULL,
  `descripcion` text,
  `estado` char(1) NOT NULL DEFAULT '1' COMMENT 'activo o inactivo',
  `url` varchar(100) DEFAULT NULL,
  `idioma` char(2) DEFAULT 'EN',
  `idpersonal` int(11) DEFAULT NULL,
  PRIMARY KEY (`idactividad`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for agenda
-- ----------------------------
DROP TABLE IF EXISTS `agenda`;
CREATE TABLE `agenda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_inicio` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fecha_final` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `alumno_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for alumno
-- ----------------------------
DROP TABLE IF EXISTS `alumno`;
CREATE TABLE `alumno` (
  `dni` int(11) NOT NULL,
  `ape_paterno` varchar(50) NOT NULL,
  `ape_materno` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fechanac` date DEFAULT NULL,
  `sexo` char(1) NOT NULL,
  `estado_civil` char(1) NOT NULL,
  `ubigeo` char(8) NOT NULL,
  `urbanizacion` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `email` varchar(75) NOT NULL,
  `idugel` char(4) NOT NULL,
  `regusuario` int(11) NOT NULL,
  `regfecha` date NOT NULL,
  `usuario` varchar(35) NOT NULL,
  `clave` varchar(35) NOT NULL,
  `token` varchar(35) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `situacion` tinyint(1) NOT NULL,
  PRIMARY KEY (`dni`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alumno_logro
-- ----------------------------
DROP TABLE IF EXISTS `alumno_logro`;
CREATE TABLE `alumno_logro` (
  `id_alumno_logro` int(11) NOT NULL AUTO_INCREMENT,
  `id_alumno` int(11) NOT NULL,
  `id_logro` int(11) NOT NULL,
  `idrecurso` int(11) DEFAULT NULL COMMENT 'id de lo que desarrollo para obtener este logro',
  `tiporecurso` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'que es lo que desarrollo para obtener este logro',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bandera` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_alumno_logro`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for ambiente
-- ----------------------------
DROP TABLE IF EXISTS `ambiente`;
CREATE TABLE `ambiente` (
  `idambiente` int(11) NOT NULL,
  `idlocal` int(11) NOT NULL,
  `numero` varchar(3) COLLATE utf8_spanish_ci DEFAULT NULL,
  `capacidad` int(11) NOT NULL DEFAULT '0',
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `estado` tinyint(4) DEFAULT NULL,
  `turno` char(1) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idambiente`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for apps_countries
-- ----------------------------
DROP TABLE IF EXISTS `apps_countries`;
CREATE TABLE `apps_countries` (
  `id_pais` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for asistencia
-- ----------------------------
DROP TABLE IF EXISTS `asistencia`;
CREATE TABLE `asistencia` (
  `idasistencia` int(11) NOT NULL,
  `idgrupoauladetalle` char(9) COLLATE utf8_spanish_ci NOT NULL,
  `idalumno` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `observacion` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `regusuario` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `regfecha` date DEFAULT NULL,
  PRIMARY KEY (`idasistencia`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for aulasvirtuales
-- ----------------------------
DROP TABLE IF EXISTS `aulasvirtuales`;
CREATE TABLE `aulasvirtuales` (
  `aulaid` int(11) NOT NULL,
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_final` datetime NOT NULL,
  `titulo` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci,
  `moderadores` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `video` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `chat` text COLLATE utf8_spanish_ci,
  `notas` text COLLATE utf8_spanish_ci,
  `dirigidoa` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `estudiantes` text COLLATE utf8_spanish_ci NOT NULL,
  `dni` int(8) NOT NULL,
  `fecha_creado` datetime NOT NULL,
  `portada` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `filtroestudiantes` text COLLATE utf8_spanish_ci,
  `nparticipantes` int(2) NOT NULL,
  PRIMARY KEY (`aulaid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for aulavirtualinvitados
-- ----------------------------
DROP TABLE IF EXISTS `aulavirtualinvitados`;
CREATE TABLE `aulavirtualinvitados` (
  `idinvitado` int(11) NOT NULL,
  `idaula` int(11) NOT NULL,
  `dni` int(11) DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `asistio` int(11) DEFAULT NULL,
  `como` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `usuario` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idinvitado`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- ----------------------------
-- Table structure for bib_autor
-- ----------------------------
DROP TABLE IF EXISTS `bib_autor`;
CREATE TABLE `bib_autor` (
  `id_autor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ap_paterno` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ap_materno` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `id_pais` int(11) NOT NULL,
  PRIMARY KEY (`id_autor`),
  KEY `id_pais` (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=536 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_categoria
-- ----------------------------
DROP TABLE IF EXISTS `bib_categoria`;
CREATE TABLE `bib_categoria` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_det_estudio_autor
-- ----------------------------
DROP TABLE IF EXISTS `bib_det_estudio_autor`;
CREATE TABLE `bib_det_estudio_autor` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_estudio` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL,
  PRIMARY KEY (`id_detalle`),
  KEY `id_autor` (`id_autor`),
  KEY `id_estudio` (`id_estudio`)
) ENGINE=InnoDB AUTO_INCREMENT=882 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_detalle_estudio_editorial
-- ----------------------------
DROP TABLE IF EXISTS `bib_detalle_estudio_editorial`;
CREATE TABLE `bib_detalle_estudio_editorial` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_editorial` int(11) NOT NULL,
  `id_estudio` int(11) NOT NULL,
  PRIMARY KEY (`id_detalle`),
  KEY `id_editorial` (`id_editorial`),
  KEY `id_estudio` (`id_estudio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_detalle_subcategoria_estudio
-- ----------------------------
DROP TABLE IF EXISTS `bib_detalle_subcategoria_estudio`;
CREATE TABLE `bib_detalle_subcategoria_estudio` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_subcategoria` int(11) NOT NULL,
  `id_estudio` int(11) NOT NULL,
  PRIMARY KEY (`id_detalle`),
  KEY `id_subcategoria` (`id_subcategoria`),
  KEY `id_estudio` (`id_estudio`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_detalle_usuario_estudio
-- ----------------------------
DROP TABLE IF EXISTS `bib_detalle_usuario_estudio`;
CREATE TABLE `bib_detalle_usuario_estudio` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_personal` int(11) NOT NULL,
  `id_estudio` int(11) NOT NULL,
  `tipo` char(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_detalle`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_editorial
-- ----------------------------
DROP TABLE IF EXISTS `bib_editorial`;
CREATE TABLE `bib_editorial` (
  `id_editorial` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_editorial`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_estudio
-- ----------------------------
DROP TABLE IF EXISTS `bib_estudio`;
CREATE TABLE `bib_estudio` (
  `id_estudio` int(11) NOT NULL AUTO_INCREMENT,
  `paginas` int(11) DEFAULT NULL,
  `codigo_tipo` int(11) DEFAULT NULL,
  `codigo` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre_abreviado` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `edicion` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fec_publicacion` date DEFAULT NULL,
  `fec_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fec_modificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `foto` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `archivo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lugar` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `condicion` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `resumen` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_idioma` varchar(7) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id_detalle` int(11) DEFAULT NULL COMMENT 'tabla detalle_grados_areas',
  `id_setting` int(11) NOT NULL COMMENT 'tabla bib_setting',
  `id_tipo` int(11) NOT NULL,
  `id_editorial` int(11) NOT NULL,
  `padre` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_estudio`)
) ENGINE=MyISAM AUTO_INCREMENT=4002 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for bib_grabacion
-- ----------------------------
DROP TABLE IF EXISTS `bib_grabacion`;
CREATE TABLE `bib_grabacion` (
  `id_grabacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `ruta` text,
  PRIMARY KEY (`id_grabacion`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for bib_historial
-- ----------------------------
DROP TABLE IF EXISTS `bib_historial`;
CREATE TABLE `bib_historial` (
  `id_historial` int(11) NOT NULL AUTO_INCREMENT,
  `id_personal` int(11) NOT NULL,
  `id_estudio` int(11) NOT NULL,
  `descripcion` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `fec_registro` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `tipo_usuario` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`id_historial`),
  KEY `id_estudio` (`id_estudio`)
) ENGINE=InnoDB AUTO_INCREMENT=271 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_idioma
-- ----------------------------
DROP TABLE IF EXISTS `bib_idioma`;
CREATE TABLE `bib_idioma` (
  `id_idioma` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_idioma`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_portada
-- ----------------------------
DROP TABLE IF EXISTS `bib_portada`;
CREATE TABLE `bib_portada` (
  `id_portada` int(11) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_portada`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for bib_recursos
-- ----------------------------
DROP TABLE IF EXISTS `bib_recursos`;
CREATE TABLE `bib_recursos` (
  `id_recursos` int(11) NOT NULL AUTO_INCREMENT,
  `id_personal` int(11) NOT NULL,
  `titulo` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `texto` text COLLATE utf8_spanish_ci,
  `caratula` text COLLATE utf8_spanish_ci,
  `orden` tinyint(4) NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'V=video,P=pdf,I=imagen,A=audios',
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'S=subido, B=biblioteca',
  `publicado` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=si, 0=no',
  `compartido` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=si,0=no',
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_recursos`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for bib_setting
-- ----------------------------
DROP TABLE IF EXISTS `bib_setting`;
CREATE TABLE `bib_setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad_descarga` int(11) NOT NULL,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_subcategoria
-- ----------------------------
DROP TABLE IF EXISTS `bib_subcategoria`;
CREATE TABLE `bib_subcategoria` (
  `id_subcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` int(11) NOT NULL,
  `descripcion` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_subcategoria`),
  KEY `id_categoria` (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_tipo
-- ----------------------------
DROP TABLE IF EXISTS `bib_tipo`;
CREATE TABLE `bib_tipo` (
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `filtro` int(11) NOT NULL,
  `extension` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_registro` int(11) NOT NULL,
  `foto` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bib_tipo_registro
-- ----------------------------
DROP TABLE IF EXISTS `bib_tipo_registro`;
CREATE TABLE `bib_tipo_registro` (
  `id_registro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_registro`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for biblioteca
-- ----------------------------
DROP TABLE IF EXISTS `biblioteca`;
CREATE TABLE `biblioteca` (
  `idbiblioteca` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `link` varchar(150) NOT NULL,
  `tipo` varchar(35) NOT NULL,
  `idpersonal` int(11) NOT NULL DEFAULT '0',
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idleccion` int(11) NOT NULL,
  `fechareg` date DEFAULT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idbiblioteca`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bitacora
-- ----------------------------
DROP TABLE IF EXISTS `bitacora`;
CREATE TABLE `bitacora` (
  `idbitacora` int(11) NOT NULL,
  PRIMARY KEY (`idbitacora`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bitacora_alumno_smartbook
-- ----------------------------
DROP TABLE IF EXISTS `bitacora_alumno_smartbook`;
CREATE TABLE `bitacora_alumno_smartbook` (
  `idbitacora_smartbook` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `idsesion` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `estado` varchar(4) COLLATE utf8_spanish_ci NOT NULL COMMENT '[T]erminado ; [P]endiente',
  `regfecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de la ultima vez que el alumno entró',
  PRIMARY KEY (`idbitacora_smartbook`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for bitacora_smartbook
-- ----------------------------
DROP TABLE IF EXISTS `bitacora_smartbook`;
CREATE TABLE `bitacora_smartbook` (
  `idbitacora` int(11) NOT NULL,
  `idbitacora_alum_smartbook` int(11) NOT NULL,
  `idcurso` int(11) DEFAULT NULL,
  `idsesion` int(11) DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL COMMENT 'idalumno',
  `pestania` varchar(128) NOT NULL,
  `total_pestanias` int(11) NOT NULL,
  `fechahora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `progreso` float(5,2) NOT NULL,
  `otros_datos` text,
  PRIMARY KEY (`idbitacora`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bolsa_empresas
-- ----------------------------
DROP TABLE IF EXISTS `bolsa_empresas`;
CREATE TABLE `bolsa_empresas` (
  `idempresa` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `rason_social` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ruc` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `logo` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `representante` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuario` varchar(75) COLLATE utf8_spanish_ci DEFAULT NULL,
  `clave` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `correo` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idempresa`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for bolsa_postulante
-- ----------------------------
DROP TABLE IF EXISTS `bolsa_postulante`;
CREATE TABLE `bolsa_postulante` (
  `idpostular` int(11) NOT NULL,
  `fecharegistro` datetime NOT NULL,
  `nombrecompleto` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` tinyint(4) NOT NULL,
  `idpublicacion` int(11) NOT NULL,
  `idpostulante` int(11) NOT NULL,
  PRIMARY KEY (`idpostular`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for bolsa_publicaciones
-- ----------------------------
DROP TABLE IF EXISTS `bolsa_publicaciones`;
CREATE TABLE `bolsa_publicaciones` (
  `idpublicacion` int(11) NOT NULL,
  `idempresa` int(11) NOT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `sueldo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nvacantes` smallint(4) NOT NULL,
  `disponibilidadeviaje` int(11) NOT NULL,
  `duracioncontrato` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `xtiempo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecharegistro` date NOT NULL,
  `fechapublicacion` datetime NOT NULL,
  `cambioderesidencia` int(11) NOT NULL,
  `mostrar` tinyint(1) NOT NULL,
  PRIMARY KEY (`idpublicacion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for cargos
-- ----------------------------
DROP TABLE IF EXISTS `cargos`;
CREATE TABLE `cargos` (
  `idcargo` int(11) NOT NULL,
  `cargo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(1) NOT NULL,
  PRIMARY KEY (`idcargo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for chat
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat` (
  `idchat` int(11) NOT NULL,
  `usuario` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`idchat`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for configuracion
-- ----------------------------
DROP TABLE IF EXISTS `configuracion`;
CREATE TABLE `configuracion` (
  `nivel` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `ventana` varchar(20) COLLATE utf8_spanish_ci NOT NULL COMMENT 'L=look,P=practice,D=dobyyourself',
  `atributo` char(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '' COMMENT 'Peso, Intentos',
  `valor` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`nivel`,`ventana`,`atributo`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for configuracion_docente
-- ----------------------------
DROP TABLE IF EXISTS `configuracion_docente`;
CREATE TABLE `configuracion_docente` (
  `idconfigdocente` int(11) NOT NULL,
  `iddocente` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idnivel` int(11) DEFAULT NULL,
  `idunidad` int(11) DEFAULT NULL,
  `idactividad` int(11) DEFAULT NULL,
  `tiempototal` time DEFAULT NULL,
  `tiempoactividades` time DEFAULT NULL,
  `tiempoteacherresrc` time DEFAULT NULL,
  `tiempogames` time DEFAULT NULL,
  `tiempoexamenes` time DEFAULT NULL,
  `escalas` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'JSON'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for dcn
-- ----------------------------
DROP TABLE IF EXISTS `dcn`;
CREATE TABLE `dcn` (
  `iddcn` int(11) NOT NULL,
  PRIMARY KEY (`iddcn`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for detalle_matricula_alumno
-- ----------------------------
DROP TABLE IF EXISTS `detalle_matricula_alumno`;
CREATE TABLE `detalle_matricula_alumno` (
  `iddetalle` int(11) NOT NULL,
  `idmatricula` int(11) NOT NULL,
  `idejercicio` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for diccionario
-- ----------------------------
DROP TABLE IF EXISTS `diccionario`;
CREATE TABLE `diccionario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `palabra` varchar(25) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `definicion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=176024 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for examen_alumno
-- ----------------------------
DROP TABLE IF EXISTS `examen_alumno`;
CREATE TABLE `examen_alumno` (
  `idexaalumno` int(11) NOT NULL,
  `idexamen` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `preguntas` text COLLATE utf8_spanish_ci NOT NULL,
  `resultado` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `puntajehabilidad` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `puntaje` double NOT NULL,
  `resultadojson` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `tiempoduracion` time NOT NULL,
  `fecha` date NOT NULL,
  `intento` int(11) NOT NULL,
  PRIMARY KEY (`idexaalumno`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for examen_tipo
-- ----------------------------
DROP TABLE IF EXISTS `examen_tipo`;
CREATE TABLE `examen_tipo` (
  `idtipo` smallint(6) NOT NULL,
  `tipo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idtipo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for examen_ubicacion
-- ----------------------------
DROP TABLE IF EXISTS `examen_ubicacion`;
CREATE TABLE `examen_ubicacion` (
  `idexamen_ubicacion` int(11) NOT NULL,
  `idrecurso` int(11) NOT NULL COMMENT 'idexamen de SmartQuiz',
  `idcurso` int(11) NOT NULL,
  `tipo` varchar(4) COLLATE utf8_unicode_ci NOT NULL COMMENT 'exam. ubicación de: [N]ivel, [U]nidad',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `rango_min` float(5,2) DEFAULT NULL COMMENT 'rango mínimo para poder darlo',
  `rango_max` float(5,2) DEFAULT NULL,
  `idexam_prerequisito` int(11) DEFAULT '0' COMMENT 'idexamen_ubicacion que necesita aprobar primero',
  PRIMARY KEY (`idexamen_ubicacion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for examen_ubicacion_alumno
-- ----------------------------
DROP TABLE IF EXISTS `examen_ubicacion_alumno`;
CREATE TABLE `examen_ubicacion_alumno` (
  `idexam_alumno` int(11) NOT NULL,
  `idexamen` int(11) NOT NULL COMMENT 'idexamen_ubicacion',
  `idalumno` int(11) NOT NULL,
  `estado` varchar(4) COLLATE utf8_spanish_ci NOT NULL COMMENT '[O]mitido ; [T]omado ; [TU] Tomado y Ubicado',
  `resultado` text COLLATE utf8_spanish_ci,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idexam_alumno`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for examenes
-- ----------------------------
DROP TABLE IF EXISTS `examenes`;
CREATE TABLE `examenes` (
  `idexamen` int(11) NOT NULL,
  `idnivel` int(11) DEFAULT NULL,
  `idunidad` int(11) DEFAULT NULL,
  `idactividad` int(11) DEFAULT NULL,
  `titulo` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `portada` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fuente` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fuentesize` int(11) NOT NULL,
  `tipo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'I=inicio,P=intermedio,F=final',
  `grupo` varchar(110) COLLATE utf8_spanish_ci DEFAULT NULL,
  `aleatorio` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 aletaroio',
  `calificacion_por` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'E=examen, P=pregunta',
  `calificacion_en` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'N=puntaje,P=porcentaje, A=alfanumerico',
  `calificacion_total` text COLLATE utf8_spanish_ci NOT NULL,
  `calificacion_min` int(11) DEFAULT '0',
  `tiempo_por` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'E=examen, P=Preguntas',
  `tiempo_total` time NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nintento` tinyint(4) DEFAULT '1',
  `calificacion` char(1) COLLATE utf8_spanish_ci DEFAULT 'U' COMMENT 'U=ultima nota, M=mejor nota',
  `origen_habilidades` varchar(8) COLLATE utf8_spanish_ci NOT NULL COMMENT 'MANUAL ;  JSON',
  `habilidades_todas` longtext COLLATE utf8_spanish_ci,
  `fuente_externa` text COLLATE utf8_spanish_ci,
  `dificultad_promedio` int(1) NOT NULL DEFAULT '0' COMMENT 'promedio de examenes_preguntas.dificultad',
  PRIMARY KEY (`idexamen`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for examenes_preguntas
-- ----------------------------
DROP TABLE IF EXISTS `examenes_preguntas`;
CREATE TABLE `examenes_preguntas` (
  `idpregunta` int(11) NOT NULL,
  `idexamen` int(11) NOT NULL,
  `pregunta` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `ejercicio` text COLLATE utf8_spanish_ci NOT NULL,
  `idpadre` int(11) NOT NULL DEFAULT '0',
  `tiempo` time NOT NULL,
  `puntaje` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `template` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `habilidades` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `idcontenedor` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dificultad` int(1) NOT NULL DEFAULT '0' COMMENT '1=dificultad mín. ,..., 3|5=dificultad máx.',
  PRIMARY KEY (`idpregunta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for foros
-- ----------------------------
DROP TABLE IF EXISTS `foros`;
CREATE TABLE `foros` (
  `idforo` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `estado` int(11) NOT NULL,
  `reg_usuario` int(11) NOT NULL,
  `rol` int(11) NOT NULL,
  PRIMARY KEY (`idforo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for general
-- ----------------------------
DROP TABLE IF EXISTS `general`;
CREATE TABLE `general` (
  `idgeneral` int(11) NOT NULL,
  `codigo` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_tabla` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(1) NOT NULL,
  PRIMARY KEY (`idgeneral`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for grabaciones
-- ----------------------------
DROP TABLE IF EXISTS `grabaciones`;
CREATE TABLE `grabaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for grupo_matricula
-- ----------------------------
DROP TABLE IF EXISTS `grupo_matricula`;
CREATE TABLE `grupo_matricula` (
  `idgrupo_matricula` int(11) NOT NULL,
  `idalumno` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idgrupo` char(9) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechamatricula` date NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `regusuario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `regfecha` date NOT NULL,
  `subgrupos` text COLLATE utf8_spanish_ci,
  PRIMARY KEY (`idgrupo_matricula`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for grupos
-- ----------------------------
DROP TABLE IF EXISTS `grupos`;
CREATE TABLE `grupos` (
  `idgrupo` char(9) COLLATE utf8_spanish_ci NOT NULL,
  `iddocente` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idlocal` char(4) COLLATE utf8_spanish_ci NOT NULL,
  `idambiente` char(5) COLLATE utf8_spanish_ci NOT NULL,
  `idasistente` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `fechainicio` date NOT NULL,
  `fechafin` date DEFAULT NULL,
  `dias` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `horas` int(11) NOT NULL,
  `horainicio` time NOT NULL,
  `horafin` time NOT NULL,
  `valor` double NOT NULL,
  `valor_asi` double NOT NULL,
  `regusuario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `regfecha` date NOT NULL,
  `matriculados` int(11) NOT NULL,
  `nivel` int(11) DEFAULT '1',
  `codigo` varchar(9) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idgrupo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for herramientas
-- ----------------------------
DROP TABLE IF EXISTS `herramientas`;
CREATE TABLE `herramientas` (
  `idtool` int(11) NOT NULL,
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `titulo` varchar(300) DEFAULT NULL,
  `descripcion` text,
  `texto` text NOT NULL,
  `orden` tinyint(4) NOT NULL,
  `tool` char(1) NOT NULL COMMENT 'V=vocabulario,L=pdf,P=pdf,G=Game,R=recursoteachear,A=audiorecord',
  PRIMARY KEY (`idtool`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for historial_sesion
-- ----------------------------
DROP TABLE IF EXISTS `historial_sesion`;
CREATE TABLE `historial_sesion` (
  `idhistorialsesion` int(11) NOT NULL,
  `tipousuario` char(2) COLLATE utf8_spanish_ci NOT NULL COMMENT 'A=Alumno ; P=Personal',
  `idusuario` int(11) NOT NULL,
  `lugar` char(2) COLLATE utf8_spanish_ci NOT NULL COMMENT 'P=Plataforma; A=Actividades; TR=Teacher Resrc.; G=Games; E=Examen',
  `idcurso` int(11) DEFAULT NULL,
  `fechaentrada` datetime NOT NULL,
  `fechasalida` datetime DEFAULT NULL,
  PRIMARY KEY (`idhistorialsesion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for libre_palabras
-- ----------------------------
DROP TABLE IF EXISTS `libre_palabras`;
CREATE TABLE `libre_palabras` (
  `idpalabra` bigint(20) NOT NULL,
  `idtema` int(11) NOT NULL,
  `palabra` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `audio` text COLLATE utf8_spanish_ci,
  `idagrupacion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `url_iframe` text COLLATE utf8_spanish_ci,
  PRIMARY KEY (`idpalabra`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for libre_tema
-- ----------------------------
DROP TABLE IF EXISTS `libre_tema`;
CREATE TABLE `libre_tema` (
  `idtema` int(11) NOT NULL,
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(128) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idtipo` int(11) NOT NULL,
  `origen` varchar(128) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Página web de origen',
  PRIMARY KEY (`idtema`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for libre_tipo
-- ----------------------------
DROP TABLE IF EXISTS `libre_tipo`;
CREATE TABLE `libre_tipo` (
  `idtipo` int(11) NOT NULL,
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_contenido` varchar(16) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idtipo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for local
-- ----------------------------
DROP TABLE IF EXISTS `local`;
CREATE TABLE `local` (
  `idlocal` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `direccion` text COLLATE utf8_spanish_ci,
  `id_ubigeo` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `vacantes` int(11) DEFAULT NULL,
  `idugel` int(11) DEFAULT NULL,
  PRIMARY KEY (`idlocal`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for logro
-- ----------------------------
DROP TABLE IF EXISTS `logro`;
CREATE TABLE `logro` (
  `id_logro` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'M=medalla, C=certificado',
  `imagen` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `puntaje` int(11) NOT NULL DEFAULT '0',
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_logro`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for manuales
-- ----------------------------
DROP TABLE IF EXISTS `manuales`;
CREATE TABLE `manuales` (
  `idmanual` int(11) NOT NULL,
  `titulo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `abreviado` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`idmanual`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for manuales_alumno
-- ----------------------------
DROP TABLE IF EXISTS `manuales_alumno`;
CREATE TABLE `manuales_alumno` (
  `identrega` int(11) NOT NULL,
  `idgrupo` char(9) COLLATE utf8_spanish_ci NOT NULL,
  `idalumno` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idmanual` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `serie` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`identrega`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for material_ayuda
-- ----------------------------
DROP TABLE IF EXISTS `material_ayuda`;
CREATE TABLE `material_ayuda` (
  `idmaterial` int(11) NOT NULL,
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `archivo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` tinyint(4) NOT NULL,
  PRIMARY KEY (`idmaterial`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for matricula
-- ----------------------------
DROP TABLE IF EXISTS `matricula`;
CREATE TABLE `matricula` (
  `idmatricula` int(11) NOT NULL,
  `idalumno` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idactividad` int(11) DEFAULT NULL,
  `fechamatricula` date NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `regusuario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `regfecha` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for matricula_alumno
-- ----------------------------
DROP TABLE IF EXISTS `matricula_alumno`;
CREATE TABLE `matricula_alumno` (
  `idmatriculaalumno` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `codigo` int(11) NOT NULL COMMENT 'id de examen o id de actividad(leccion)',
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'L=Leccion ; E=Examen',
  `orden` int(11) NOT NULL,
  `fechamatricula` date NOT NULL,
  `fechacaduca` date NOT NULL,
  `regusuario` int(11) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmatriculaalumno`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for mensajes
-- ----------------------------
DROP TABLE IF EXISTS `mensajes`;
CREATE TABLE `mensajes` (
  `idmensaje` int(11) NOT NULL,
  PRIMARY KEY (`idmensaje`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `idmenu` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`idmenu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for metodologia_habilidad
-- ----------------------------
DROP TABLE IF EXISTS `metodologia_habilidad`;
CREATE TABLE `metodologia_habilidad` (
  `idmetodologia` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `tipo` char(1) NOT NULL,
  `estado` int(11) DEFAULT '1',
  PRIMARY KEY (`idmetodologia`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Metodologias y Habilidades';

-- ----------------------------
-- Table structure for niveles
-- ----------------------------
DROP TABLE IF EXISTS `niveles`;
CREATE TABLE `niveles` (
  `idnivel` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `tipo` char(1) NOT NULL COMMENT 'N= nivel, U= unidad, L=lesson',
  `idpadre` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL DEFAULT '0',
  `estado` int(11) NOT NULL COMMENT '1 activo , 0 inactivo',
  `orden` tinyint(4) NOT NULL DEFAULT '0',
  `imagen` varchar(200) DEFAULT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`idnivel`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for nota_detalle
-- ----------------------------
DROP TABLE IF EXISTS `nota_detalle`;
CREATE TABLE `nota_detalle` (
  `idnota_detalle` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `idmatricula` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `iddocente` int(11) NOT NULL,
  `idcursodetalle` int(11) NOT NULL,
  `nota` float(5,2) NOT NULL,
  `tipo` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '[N]ivel; [U]nidad; [L]ección; [E]xamen',
  `observacion` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idnota_detalle`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for notas
-- ----------------------------
DROP TABLE IF EXISTS `notas`;
CREATE TABLE `notas` (
  `idnota` int(11) NOT NULL,
  `idhoja` int(11) NOT NULL COMMENT 'idpestania de tipo=''H''',
  `idcolumna` int(11) DEFAULT NULL COMMENT 'idpestania de tipo=''C'' . Cuando tipo_nota=''P'', entonces idcolumna=NULL',
  `idalumno` int(11) NOT NULL,
  `idarchivo` int(11) NOT NULL,
  `nota_num` float(5,2) DEFAULT NULL,
  `nota_txt` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `aprobado` tinyint(1) DEFAULT '0',
  `observacion` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `fechareg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idnota`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for notas_alumno
-- ----------------------------
DROP TABLE IF EXISTS `notas_alumno`;
CREATE TABLE `notas_alumno` (
  `idalumno` int(11) NOT NULL,
  `nombres` varchar(128) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellidos` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `identificador` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'ID de alumno si es que es importado de otra plataforma',
  `idarchivo` int(11) NOT NULL,
  `origen` varchar(32) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechareg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idalumno`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for notas_archivo
-- ----------------------------
DROP TABLE IF EXISTS `notas_archivo`;
CREATE TABLE `notas_archivo` (
  `idarchivo` int(11) NOT NULL,
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` text COLLATE utf8_spanish_ci,
  `identificador` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'ID del curso, si es que viene importado',
  `iddocente` int(11) NOT NULL COMMENT 'idusuario',
  `fechareg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idarchivo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='puede representar al Curso, Institución o archivo que el docente pueda identificar.';

-- ----------------------------
-- Table structure for notas_pestania
-- ----------------------------
DROP TABLE IF EXISTS `notas_pestania`;
CREATE TABLE `notas_pestania` (
  `idpestania` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `abreviatura` varchar(5) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo_pestania` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT '[C]olumna ; [H]oja',
  `tipo_info` char(1) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Tipo de inform. que guardará la columna: [N]úmero, [R]ango ; [E]scala ; [T]exto ; [A]sistencia',
  `info_valor` text COLLATE utf8_spanish_ci,
  `color` varchar(7) COLLATE utf8_spanish_ci NOT NULL DEFAULT '#ffffff',
  `orden` int(11) NOT NULL,
  `idarchivo` int(11) NOT NULL,
  `idpestania_padre` int(11) DEFAULT NULL COMMENT 'idpestania, cuando una columna pertenece a una Hoja',
  PRIMARY KEY (`idpestania`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for permisos
-- ----------------------------
DROP TABLE IF EXISTS `permisos`;
CREATE TABLE `permisos` (
  `idpermiso` int(11) NOT NULL,
  `rol` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `_list` tinyint(1) NOT NULL,
  `_add` tinyint(1) NOT NULL,
  `_edit` tinyint(1) NOT NULL,
  `_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`idpermiso`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for persona_apoderado
-- ----------------------------
DROP TABLE IF EXISTS `persona_apoderado`;
CREATE TABLE `persona_apoderado` (
  `idapoderado` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ape_paterno` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ape_materno` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `sexo` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `tipodoc` int(11) NOT NULL,
  `ndoc` int(11) NOT NULL,
  `parentesco` int(11) NOT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `celular` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for persona_educacion
-- ----------------------------
DROP TABLE IF EXISTS `persona_educacion`;
CREATE TABLE `persona_educacion` (
  `ideducacion` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `institucion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `tipodeestudio` int(2) NOT NULL,
  `titulo` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `areaestudio` int(11) DEFAULT NULL,
  `situacion` int(1) NOT NULL,
  `fechade` date NOT NULL,
  `fechahasta` date DEFAULT NULL,
  `actualmente` int(1) DEFAULT NULL,
  `mostrar` int(1) NOT NULL,
  PRIMARY KEY (`ideducacion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for persona_experiencialaboral
-- ----------------------------
DROP TABLE IF EXISTS `persona_experiencialaboral`;
CREATE TABLE `persona_experiencialaboral` (
  `idexperiencia` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `empresa` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `rubro` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `area` int(11) NOT NULL,
  `cargo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `funciones` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `fechade` date NOT NULL,
  `fechahasta` date NOT NULL,
  `actualmente` int(1) NOT NULL,
  `mostrar` int(1) NOT NULL,
  PRIMARY KEY (`idexperiencia`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for persona_metas
-- ----------------------------
DROP TABLE IF EXISTS `persona_metas`;
CREATE TABLE `persona_metas` (
  `idmeta` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `meta` text COLLATE utf8_spanish_ci NOT NULL,
  `objetivo` text COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(1) NOT NULL,
  PRIMARY KEY (`idmeta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for persona_referencia
-- ----------------------------
DROP TABLE IF EXISTS `persona_referencia`;
CREATE TABLE `persona_referencia` (
  `idreferencia` int(11) NOT NULL,
  `idpersona` int(8) NOT NULL,
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `cargo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `relacion` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(11) NOT NULL,
  PRIMARY KEY (`idreferencia`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for persona_rol
-- ----------------------------
DROP TABLE IF EXISTS `persona_rol`;
CREATE TABLE `persona_rol` (
  `iddetalle` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  PRIMARY KEY (`iddetalle`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for personal
-- ----------------------------
DROP TABLE IF EXISTS `personal`;
CREATE TABLE `personal` (
  `idpersona` bigint(20) NOT NULL DEFAULT '0',
  `tipodoc` tinyint(4) DEFAULT NULL,
  `dni` int(11) NOT NULL,
  `ape_paterno` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `ape_materno` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `fechanac` date DEFAULT NULL,
  `sexo` char(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `estado_civil` char(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `ubigeo` char(8) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `urbanizacion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `celular` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(75) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `idugel` char(4) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `regusuario` int(11) NOT NULL DEFAULT '0',
  `regfecha` date NOT NULL,
  `usuario` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `token` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `rol` int(11) NOT NULL,
  `foto` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `situacion` char(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `idioma` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`idpersona`),
  KEY `nombre` (`nombre`,`ape_paterno`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pronunciacion
-- ----------------------------
DROP TABLE IF EXISTS `pronunciacion`;
CREATE TABLE `pronunciacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `palabra` varchar(50) NOT NULL,
  `pron` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134723 DEFAULT CHARSET=utf8 COMMENT='tabla para el reconocimiento de voz';

-- ----------------------------
-- Table structure for record
-- ----------------------------
DROP TABLE IF EXISTS `record`;
CREATE TABLE `record` (
  `idrecord` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `link` varchar(150) NOT NULL,
  `idpersonal` int(11) NOT NULL DEFAULT '0',
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idleccion` int(11) NOT NULL,
  `fechareg` date DEFAULT NULL,
  `idherramienta` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idrecord`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for recursos
-- ----------------------------
DROP TABLE IF EXISTS `recursos`;
CREATE TABLE `recursos` (
  `idrecurso` int(11) NOT NULL,
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `titulo` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `texto` text COLLATE utf8_spanish_ci NOT NULL,
  `caratula` text COLLATE utf8_spanish_ci,
  `orden` tinyint(4) NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'V=vocabulario,L=pdf,P=pdf,G=Game,R=recursoteachear,A=audiorecord',
  `publicado` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=si, 0=no',
  `compartido` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=si,0=no',
  PRIMARY KEY (`idrecurso`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for reporte
-- ----------------------------
DROP TABLE IF EXISTS `reporte`;
CREATE TABLE `reporte` (
  `idreporte` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `tipo` varchar(16) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Identificador del tipo de reporte. Ej: Seguimiento x alumno=SEGALUM; Seguimiento x Grupo=SEGGRUPO',
  `id_alumno_grupo` int(11) NOT NULL COMMENT 'Dependiendo del TIPO, aqui se especifica el ID ya sea del Alumno o un Grupo de alumnos',
  `informacion` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'Información del reporte en formato JSON',
  `fechacreacion` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `idrol` int(11) NOT NULL,
  `rol` varchar(35) NOT NULL,
  PRIMARY KEY (`idrol`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rub_detalle_respuestas
-- ----------------------------
DROP TABLE IF EXISTS `rub_detalle_respuestas`;
CREATE TABLE `rub_detalle_respuestas` (
  `id_det_rptas` int(11) NOT NULL AUTO_INCREMENT,
  `id_respuestas` int(11) NOT NULL,
  `id_pregunta` int(11) NOT NULL,
  `respuesta` text NOT NULL,
  `puntaje_obtenido` float NOT NULL,
  `id_dimension` int(11) NOT NULL,
  PRIMARY KEY (`id_det_rptas`)
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for rub_dimension
-- ----------------------------
DROP TABLE IF EXISTS `rub_dimension`;
CREATE TABLE `rub_dimension` (
  `id_dimension` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `tipo_pregunta` varchar(2) NOT NULL COMMENT 'PE=Ponderacion Estandar;  \r\nSN=Si - No ;  \r\nC=Completar;   \r\nA=Alternativas   \r\n',
  `escalas_evaluacion` text CHARACTER SET utf8 NOT NULL,
  `tipo` varchar(1) NOT NULL COMMENT 'N=Nueva ;  R=Repetida',
  `otros_datos` text COMMENT 'Usando en Pond.Est. para guardar cant. de niveles (1-5)',
  `id_rubrica` int(11) NOT NULL,
  `activo` tinyint(5) NOT NULL,
  PRIMARY KEY (`id_dimension`)
) ENGINE=InnoDB AUTO_INCREMENT=594 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for rub_estandar
-- ----------------------------
DROP TABLE IF EXISTS `rub_estandar`;
CREATE TABLE `rub_estandar` (
  `id_estandar` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `id_rubrica` int(11) NOT NULL,
  `puntuacion` text NOT NULL,
  `id_dimension` int(11) NOT NULL,
  `activo` tinyint(5) NOT NULL,
  `tipo` varchar(1) NOT NULL,
  `duplicado_id_estandar` int(11) NOT NULL,
  PRIMARY KEY (`id_estandar`)
) ENGINE=InnoDB AUTO_INCREMENT=698 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for rub_pregunta
-- ----------------------------
DROP TABLE IF EXISTS `rub_pregunta`;
CREATE TABLE `rub_pregunta` (
  `id_pregunta` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `otros_datos` text COMMENT 'Usado para guardar ALTERNATIVAS',
  `id_estandar` int(11) NOT NULL,
  `tipo` varchar(1) NOT NULL,
  `activo` tinyint(5) NOT NULL,
  PRIMARY KEY (`id_pregunta`)
) ENGINE=InnoDB AUTO_INCREMENT=1346 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for rub_respuestas
-- ----------------------------
DROP TABLE IF EXISTS `rub_respuestas`;
CREATE TABLE `rub_respuestas` (
  `id_respuestas` int(11) NOT NULL AUTO_INCREMENT,
  `id_rubrica` int(11) NOT NULL,
  `nombre` varchar(128) DEFAULT NULL COMMENT 'Quien Rellena la Rubrica',
  `institucion` varchar(255) DEFAULT NULL,
  `idlocal` int(11) DEFAULT NULL,
  `iddocente` varchar(8) DEFAULT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(5) NOT NULL,
  PRIMARY KEY (`id_respuestas`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for rub_rubrica
-- ----------------------------
DROP TABLE IF EXISTS `rub_rubrica`;
CREATE TABLE `rub_rubrica` (
  `id_rubrica` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) NOT NULL,
  `descripcion` text,
  `foto` text,
  `tipo_encuestado` varchar(1) DEFAULT NULL COMMENT 'A=Anonimo; P=Publico',
  `opcion_publico` varchar(10) NOT NULL COMMENT 'E=Empresa, I=Institución',
  `autor` varchar(200) NOT NULL,
  `tipo_letra` varchar(32) NOT NULL,
  `tamanio_letra` tinyint(5) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tipo_rubrica` char(2) NOT NULL,
  `puntuacion_general` text NOT NULL,
  `tipo_pregunta` varchar(2) NOT NULL,
  `id_usuario` int(11) NOT NULL COMMENT 'Creador de la Rubrica',
  `activo` int(11) NOT NULL,
  `registros` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_rubrica`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for rub_tipo_letra
-- ----------------------------
DROP TABLE IF EXISTS `rub_tipo_letra`;
CREATE TABLE `rub_tipo_letra` (
  `id_tipoletra` int(11) NOT NULL,
  `descripcion` varchar(30) NOT NULL,
  `activo` tinyint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sis_configuracion
-- ----------------------------
DROP TABLE IF EXISTS `sis_configuracion`;
CREATE TABLE `sis_configuracion` (
  `config` varchar(30) NOT NULL,
  `valor` longtext,
  `autocargar` enum('si','no') NOT NULL,
  PRIMARY KEY (`config`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tarea
-- ----------------------------
DROP TABLE IF EXISTS `tarea`;
CREATE TABLE `tarea` (
  `idtarea` int(11) NOT NULL,
  `idnivel` int(11) DEFAULT NULL,
  `idunidad` int(11) DEFAULT NULL,
  `idactividad` int(11) DEFAULT NULL,
  `idcursodetalle` int(11) DEFAULT NULL,
  `iddocente` int(11) NOT NULL,
  `asignacion` char(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'A' COMMENT '[A]utomatico; [M]anual',
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci,
  `foto` text COLLATE utf8_spanish_ci,
  `habilidades` text COLLATE utf8_spanish_ci,
  `habilidad_destacada` text COLLATE utf8_spanish_ci,
  `puntajemaximo` decimal(5,2) NOT NULL,
  `puntajeminimo` decimal(5,2) NOT NULL,
  `estado` varchar(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'NA' COMMENT 'A=Asignado ; NA=No Asignado',
  `eliminado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idtarea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for tarea_archivos
-- ----------------------------
DROP TABLE IF EXISTS `tarea_archivos`;
CREATE TABLE `tarea_archivos` (
  `idtarea_archivos` int(11) NOT NULL,
  `tablapadre` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'T=Tarea ; R=Tarea_Respuesta',
  `idpadre` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `ruta` text COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'D=Documento ; G=Grabacion_Voz ; V=Video ; L=Link ; A=Actividad ; J=Juego ; E=Examen',
  `puntaje` decimal(5,2) DEFAULT NULL,
  `habilidad` text COLLATE utf8_spanish_ci,
  `texto` longtext COLLATE utf8_spanish_ci COMMENT 'Para guardar JSON y HTML',
  `idtarea_archivos_padre` int(11) DEFAULT NULL,
  PRIMARY KEY (`idtarea_archivos`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for tarea_asignacion
-- ----------------------------
DROP TABLE IF EXISTS `tarea_asignacion`;
CREATE TABLE `tarea_asignacion` (
  `idtarea_asignacion` int(11) NOT NULL AUTO_INCREMENT,
  `idtarea` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  `iddocente` int(11) DEFAULT NULL COMMENT 'docente que asigna la tarea. o docente a cargo de revisar la tarea',
  `fechaentrega` date NOT NULL,
  `horaentrega` time NOT NULL,
  `eliminado` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idtarea_asignacion`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for tarea_asignacion_alumno
-- ----------------------------
DROP TABLE IF EXISTS `tarea_asignacion_alumno`;
CREATE TABLE `tarea_asignacion_alumno` (
  `iddetalle` int(11) NOT NULL,
  `idtarea_asignacion` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `mensajedevolucion` text COLLATE utf8_spanish_ci,
  `notapromedio` decimal(5,2) DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'N=Nuevo; P=Presentado; D=Devuelto; E=Evaluado',
  PRIMARY KEY (`iddetalle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for tarea_respuesta
-- ----------------------------
DROP TABLE IF EXISTS `tarea_respuesta`;
CREATE TABLE `tarea_respuesta` (
  `idtarea_respuesta` int(11) NOT NULL,
  `idtarea_asignacion_alumno` int(11) NOT NULL COMMENT 'tarea_asignacion_alumno->iddetalle',
  `comentario` text COLLATE utf8_spanish_ci,
  `fechapresentacion` date NOT NULL,
  `horapresentacion` time NOT NULL,
  PRIMARY KEY (`idtarea_respuesta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for ubigeo
-- ----------------------------
DROP TABLE IF EXISTS `ubigeo`;
CREATE TABLE `ubigeo` (
  `id_ubigeo` char(6) COLLATE utf8_spanish_ci NOT NULL,
  `pais` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `departamento` char(2) COLLATE utf8_spanish_ci DEFAULT '00',
  `provincia` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT '00',
  `distrito` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT '00',
  `ciudad` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_ubigeo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for ugel
-- ----------------------------
DROP TABLE IF EXISTS `ugel`;
CREATE TABLE `ugel` (
  `idugel` int(11) NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `abrev` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `iddepartamento` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idprovincia` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idugel`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
