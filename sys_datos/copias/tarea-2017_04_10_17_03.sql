/*
Navicat MySQL Data Transfer

Source Server         : PVIngles_AbacoEducacion.org
Source Server Version : 50635
Source Host           : abacoeducacion.org:3306
Source Database       : abacoedu_smartlearn

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2018-04-10 17:03:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tarea
-- ----------------------------
DROP TABLE IF EXISTS `tarea`;
CREATE TABLE `tarea` (
  `idtarea` int(11) NOT NULL,
  `idnivel` int(11) DEFAULT NULL,
  `idunidad` int(11) DEFAULT NULL,
  `idactividad` int(11) DEFAULT NULL,
  `idcursodetalle` int(11) DEFAULT NULL,
  `iddocente` int(11) NOT NULL,
  `asignacion` char(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'A' COMMENT '[A]utomatico; [M]anual',
  `nombre` varchar(32) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci,
  `foto` text COLLATE utf8_spanish_ci,
  `habilidades` text COLLATE utf8_spanish_ci,
  `habilidad_destacada` text COLLATE utf8_spanish_ci,
  `puntajemaximo` decimal(5,2) NOT NULL,
  `puntajeminimo` decimal(5,2) NOT NULL,
  `estado` varchar(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'NA' COMMENT 'A=Asignado ; NA=No Asignado',
  `eliminado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idtarea`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Records of tarea
-- ----------------------------
INSERT INTO `tarea` VALUES ('1', '1', '6', '18', '4', '22222222', 'A', 'Primera tarea de Hello', 'Tarea de reforzamiento.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170630052326.jpg', '', null, '20.00', '10.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('2', '1', '6', '18', '4', '22222222', 'A', 'Nueva Tarea Hello', 'Descripción para tarea de Hello.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061349.jpg', '', null, '20.00', '11.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('3', '1', '6', '18', '4', '22222222', 'A', 'Saludos', 'Completa correctamente las actividades adjuntas. Y si deseas reforzar tu tarea con imagenes, link, u otros archivos; lo puedes hacer.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061135.jpg', '', null, '100.00', '75.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('4', '1', '6', '55', '6', '22222222', 'A', 'Tarea de Sesion 02 (A1- Introduc', '', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061319.jpg', '', null, '90.00', '51.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('5', '1', '6', '18', '4', '22222222', 'A', 'Mi Tarea Ejemplo para Hello', 'El objetivo de esta tarea es determinar tus habilidades', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170327111758.jpg', '', null, '100.00', '80.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('6', '1', '6', '18', '4', '22222222', 'A', 'Tarea de NUmbers', 'Desarrolle la tarea correctamente', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170327120409.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('7', '1', '6', '18', '4', '55555555', 'A', 'Nueva tarea', 'Tarea demo.', '__xRUTABASEx__/static/media/web/nofoto.jpg', '', null, '100.00', '75.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('8', '1', '6', '18', '4', '55555555', 'A', 'Test', 'Reforzar cierto tema...', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180228073125.jpg', '', null, '100.00', '75.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('9', '1', '6', '18', '4', '65300999', 'A', 'Hello', 'The objectives of these tasks are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"Hello\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180317092613.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('10', '1', '6', '56', '5', '65300999', 'A', 'Spell that', 'The objectives of these tasks are to help you practice and improve your listening skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180319022201.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('11', '1', '6', '55', '6', '65300999', 'A', 'Who are you?', 'The objectives of this task are to help you practice and improve your grammar and vocabulary.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180320102147.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('12', '1', '6', '61', '7', '65300999', 'A', 'Who\'s this?', 'The objectives of this task are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"Who\'s this?\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180320023022.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('13', '1', '7', '21', '21', '65300999', 'A', 'Numbers', 'The objectives of these tasks are to improve your listening skills, and practice the vocabulary and grammar you have learnt in the activity \"Numbers\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180320032041.jpeg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('14', '1', '7', '22', '22', '65300999', 'A', 'What\'s this?', 'The objectives of these tasks are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"What\'s this?\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180321095209.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('15', '1', '7', '23', '23', '65300999', 'A', 'Where is...?', 'The objectives of these tasks are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"Where is...?\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180321120607.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('16', '1', '8', '24', '26', '65300999', 'A', 'Countries', 'The objectives of these tasks are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"Countries\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180322100330.png', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('17', '1', '8', '25', '27', '65300999', 'A', 'Nationalities', 'The objectives of these tasks are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"Nationalities.\"', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180322100912.jpg', '', null, '100.00', '70.00', 'NA', '1');
INSERT INTO `tarea` VALUES ('18', '1', '8', '25', '27', '65300999', 'A', 'Nationalities', 'The objectives of these tasks are to improve your listening skills and help you remember and practice the vocabulary and grammar you have learnt in the activity \"Nationalities\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180322100912.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('19', '1', '8', '26', '28', '65300999', 'A', 'Places', 'The objectives of these tasks are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"Places\"', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180323053219.png', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('20', '1', '9', '27', '31', '65300999', 'A', 'My Week', 'The objectives of these tasks are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"My week\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180323053843.png', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('21', '1', '9', '58', '32', '65300999', 'A', 'Routines', 'The objectives of this task are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"Routines\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180324100027.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('22', '1', '9', '29', '33', '65300999', 'A', 'Leisure Activities', 'The objectives of these tasks are to help you practice and improve your grammar and vocabulary.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180326094019.png', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('23', '1', '10', '30', '46', '65300999', 'A', 'Describing People', 'The objectives of this task are to improve your listening skills, and practice the vocabulary and grammar you have learnt in this activity.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180326112436.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('24', '1', '10', '31', '47', '65300999', 'A', 'The body', 'The objectives of these tasks are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"The body\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180326034006.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('25', '1', '10', '32', '48', '65300999', 'A', 'My family', 'The objectives of this task are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"My family\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180327054421.png', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('26', '1', '11', '33', '50', '65300999', 'A', 'At the store', 'The objectives of these tasks are to help you practice and improve your grammar and vocabulary.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180327055822.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('27', '1', '11', '34', '51', '65300999', 'A', 'Task: Shopping', 'The objectives are to help you remember and practice the vocabulary and grammar you have learnt, and improve your writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180328045938.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('28', '1', '11', '35', '52', '65300999', 'A', 'Task: In a supermarket', 'The objectives are to help you practice the vocabulary and grammar you have learnt, and improve your writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180328050915.gif', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('29', '1', '12', '36', '53', '65300999', 'A', 'Test: Food Item', 'The objectives are to help you practice the vocabulary and grammar you have learnt, and improve your writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180331101907.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('30', '1', '12', '37', '54', '65300999', 'A', 'Test: In a restaurant', 'The objectives of these tasks are to help you practice and improve your listening and writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180402050531.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('31', '1', '12', '38', '55', '65300999', 'A', 'Test: Cooking or dining out?', 'The objectives are to help you practice and improve your grammar, vocabulary, and writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180402052511.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('32', '1', '13', '39', '56', '65300999', 'A', 'Test: Clothing', 'The objectives are to help you practice and improve your grammar, vocabulary, and writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180403033935.png', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('33', '1', '13', '40', '57', '65300999', 'A', 'Test: Planets', 'The objectives are to help you practice and improve your grammar, vocabulary, and writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180403034741.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('34', '1', '13', '41', '58', '65300999', 'A', 'Test: Animals', 'The objectives of these tasks are to help you improve your listening and writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180403035504.png', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('35', '1', '14', '42', '59', '65300999', 'A', 'Test: Weather', 'The objectives are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"Weather\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180404041313.png', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('36', '1', '14', '43', '60', '65300999', 'A', 'Test: What am I doing?', 'The objectives of these tasks are to help you remember and practice the vocabulary and grammar you have learnt in the activity \"What am I doing?\".', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180404041706.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('37', '1', '14', '44', '61', '65300999', 'A', 'Test: Help!', 'The objectives of these tasks are to improve your listening and writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180404042529.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('38', '1', '15', '45', '62', '65300999', 'A', 'Test: My hometown', 'The objectives are to help you practice and improve your grammar, vocabulary, and writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180405041258.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('39', '1', '15', '46', '63', '65300999', 'A', 'Test: My house', 'The objectives are to help you practice and improve your listening and writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180405042324.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('40', '1', '15', '47', '64', '65300999', 'A', 'Test: My address', 'The objectives are to help you practice and improve your listening and writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180405043710.png', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('41', '1', '16', '48', '65', '65300999', 'A', 'Test: Where were you?', 'The objectives of these tasks are to help you practice and improve your listening and writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180406034302.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('42', '1', '16', '49', '66', '65300999', 'A', 'Test: My school years', 'The objectives are to help you practice and improve your grammar and vocabulary,', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180406035707.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('43', '1', '16', '50', '67', '65300999', 'A', 'Test: My work', 'The objectives are to help you practice and improve your grammar and vocabulary.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180407104335.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('44', '1', '17', '51', '43', '65300999', 'A', 'Test: Dates and seasons', 'The objectives are to help you practice and improve your grammar and vocabulary.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180409041325.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('45', '1', '17', '52', '44', '65300999', 'A', 'Test: My holidays', 'The objectives are to help you practice and improve your vocabulary and listening skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180409041720.jpg', '', null, '100.00', '70.00', 'NA', '0');
INSERT INTO `tarea` VALUES ('46', '1', '17', '53', '45', '65300999', 'A', 'Test: Journeys', 'The objectives are to help you practice and improve your grammar, vocabulary, and writing skills.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20180409042514.jpg', '', null, '100.00', '70.00', 'NA', '0');
