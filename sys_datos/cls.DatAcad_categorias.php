<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-04-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_categorias extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_categorias").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_categorias";
			
			$cond = array();		
			
			if(isset($filtros["idcategoria"])) {
					$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_categorias").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_categorias";			
			
			$cond = array();		
					
			
			if(isset($filtros["idcategoria"])) {
					$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_categorias").": " . $e->getMessage());
		}
	}
	public function buscarxIDs($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_categorias";			
			
			$cond = array();		
					
			
			if(isset($filtros["idcategoria"])) {
				if( is_array($filtros["idcategoria"]) ) {
					$cond[] = "idcategoria IN (" . implode(',', $filtros["idcategoria"]) . ")";
				} else {
					$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
				}
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_categorias").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$descripcion,$imagen,$idpadre,$estado,$orden)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_categorias_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcategoria) FROM acad_categorias");
			++$id;
			
			$estados = array('idcategoria' => $id
							
							,'nombre'=>$nombre
							,'descripcion'=>$descripcion
							,'imagen'=>$imagen
							,'idpadre'=>$idpadre
							,'estado'=>$estado
							,'orden'=>$orden							
							);
			
			$this->oBD->insert('acad_categorias', $estados);			
			$this->terminarTransaccion('dat_acad_categorias_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_categorias_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_categorias").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$descripcion,$imagen,$idpadre,$estado,$orden)
	{
		try {
			$this->iniciarTransaccion('dat_acad_categorias_update');
			$estados = array('nombre'=>$nombre
							,'descripcion'=>$descripcion
							,'imagen'=>$imagen
							,'idpadre'=>$idpadre
							,'estado'=>$estado
							,'orden'=>$orden								
							);
			
			$this->oBD->update('acad_categorias ', $estados, array('idcategoria' => $id));
		    $this->terminarTransaccion('dat_acad_categorias_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_categorias").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT tb1.*,tb2.nombre AS _nombre  FROM acad_categorias tb1 LEFT JOIN acad_categorias tb2 ON tb1.idpadre=tb2.idpadre  "
					. " WHERE tb1.idcategoria = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_categorias").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_categorias', array('idcategoria' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_categorias").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_categorias', array($propiedad => $valor), array('idcategoria' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_categorias").": " . $e->getMessage());
		}
	}
   
		
}