<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatManuales_alumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Manuales_alumno").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM manuales_alumno";
			
			$cond = array();		
			
			if(isset($filtros["identrega"])) {
					$cond[] = "identrega = " . $this->oBD->escapar($filtros["identrega"]);
			}
			if(isset($filtros["idgrupo"])) {
					$cond[] = "idgrupo = " . $this->oBD->escapar($filtros["idgrupo"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["idmanual"])) {
					$cond[] = "idmanual = " . $this->oBD->escapar($filtros["idmanual"]);
			}
			if(isset($filtros["idaulagrupodetalle"])) {
					$cond[] = "idaulagrupodetalle = " . $this->oBD->escapar($filtros["idaulagrupodetalle"]);
			}
			if(isset($filtros["comentario"])) {
					$cond[] = "comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["serie"])) {
					$cond[] = "serie = " . $this->oBD->escapar($filtros["serie"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Manuales_alumno").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM manuales_alumno";			
			
			$cond = array();
			if(isset($filtros["identrega"])) {
					$cond[] = "identrega = " . $this->oBD->escapar($filtros["identrega"]);
			}
			if(isset($filtros["idgrupo"])) {
					$cond[] = "idgrupo = " . $this->oBD->escapar($filtros["idgrupo"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["idmanual"])) {
					$cond[] = "idmanual = " . $this->oBD->escapar($filtros["idmanual"]);
			}
			if(isset($filtros["idaulagrupodetalle"])) {
					$cond[] = "idaulagrupodetalle = " . $this->oBD->escapar($filtros["idaulagrupodetalle"]);
			}
			if(isset($filtros["comentario"])) {
					$cond[] = "comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["serie"])) {
					$cond[] = "serie = " . $this->oBD->escapar($filtros["serie"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
		
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Manuales_alumno").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idgrupo,$idalumno,$idmanual,$idaulagrupodetalle,$comentario,$fecha,$estado,$serie)
	{
		try {
			
			$this->iniciarTransaccion('dat_manuales_alumno_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(identrega) FROM manuales_alumno");
			++$id;
			
			$estados = array('identrega' => $id							
							,'idgrupo'=>$idgrupo
							,'idalumno'=>$idalumno
							,'idmanual'=>$idmanual
							,'idaulagrupodetalle'=>$idaulagrupodetalle
							,'comentario'=>$comentario
							,'fecha'=>!empty($fecha)?$fecha:date('Y-m-d H:i:s')
							,'estado'=>isset($estado)?$estado:1
							,'serie'=>$serie							
							);
			
			$this->oBD->insert('manuales_alumno', $estados);

			$sql = "SELECT COUNT(*) FROM manuales_alumno WHERE idmanual=".$idmanual." AND  estado=1";
			$nmanules=$this->oBD->consultarEscalarSQL($sql);
			//$this->oBD->update('manuales ', array('stock','total-'.$nmanules), array('idmanual' => $idmanual));
			$this->terminarTransaccion('dat_manuales_alumno_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_manuales_alumno_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Manuales_alumno").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idgrupo,$idalumno,$idmanual,$idaulagrupodetalle,$comentario,$fecha,$estado,$serie)
	{
		try {
			$this->iniciarTransaccion('dat_manuales_alumno_update');
			$estados = array('idgrupo'=>$idgrupo
							,'idalumno'=>$idalumno
							,'idmanual'=>$idmanual
							,'idaulagrupodetalle'=>$idaulagrupodetalle
							,'comentario'=>$comentario
							,'fecha'=>!empty($fecha)?$fecha:date('Y-m-d H:i:s')
							,'estado'=>isset($estado)?$estado:1
							,'serie'=>$serie								
							);
			
			$this->oBD->update('manuales_alumno ', $estados, array('identrega' => $id));
			
			$sql = "SELECT COUNT(*) FROM manuales_alumno WHERE idmanual=".$idmanual." AND  estado=1";
			$nmanules=$this->oBD->consultarEscalarSQL($sql);
			//$this->oBD->update('manuales ', array('stock','total - '.$nmanules), array('idmanual' => $idmanual));
		    $this->terminarTransaccion('dat_manuales_alumno_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Manuales_alumno").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM manuales_alumno  "
					. " WHERE identrega = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Manuales_alumno").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('manuales_alumno', array('identrega' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Manuales_alumno").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('manuales_alumno', array($propiedad => $valor), array('identrega' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Manuales_alumno").": " . $e->getMessage());
		}
	}
   
		
}