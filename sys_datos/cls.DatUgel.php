<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-01-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatUgel extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM ugel";			
			$cond = array();			
			if(!empty($filtros["idugel"])) { $cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);}
			if(!empty($filtros["descripcion"])){ $cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);	}
			if(!empty($filtros["abrev"])){ $cond[] = "abrev = " . $this->oBD->escapar($filtros["abrev"]);}
			if(!empty($filtros["iddepartamento"])){	$cond[] = "iddepartamento = " . $this->oBD->escapar($filtros["iddepartamento"]); }
			if(!empty($filtros["idprovincia"])) { $cond[] = "idprovincia = " . $this->oBD->escapar($filtros["idprovincia"]);}			
			if(!empty($cond)) {	$sql .= " WHERE " . implode(' AND ', $cond); }			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT u.idugel,u.descripcion, u.abrev, d.ciudad as dpto, p.ciudad as pro
			from (ugel u LEFT JOIN ubigeo d on u.iddepartamento=d.id_ubigeo)
			LEFT JOIN ubigeo p on u.idprovincia=p.id_ubigeo";
			
			$cond = array();			
			if(!empty($filtros["idugel"])) {	$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);}
			if(!empty($filtros["descripcion"])) {$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);	}
			if(!empty($filtros["abrev"])) {		$cond[] = "abrev = " . $this->oBD->escapar($filtros["abrev"]); }
			if(!empty($filtros["iddepartamento"])) {$cond[] = "iddepartamento = " . $this->oBD->escapar($filtros["iddepartamento"]);}
			if(!empty($filtros["idprovincia"])) {	$cond[] = "idprovincia = " . $this->oBD->escapar($filtros["idprovincia"]);}			
			if(!empty($cond)){	$sql .= " WHERE " . implode(' AND ', $cond); }			
			//$sql .= " ORDER BY fecha_creado ASC";			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}


	public function ubigeo($filtros=null)
	{
		try {
			$sql = "SELECT * FROM ubigeo";			
			$cond = array();			
			if(!empty($filtros["iddep"])){
				$cond1 = " WHERE provincia ='00' AND distrito='00'";
				//$cond1 = " where iddepartamento= ".$this->oBD->escapar($filtros["iddep"])."provincia ='00' and distrito='00'";
			}

			if(!empty($filtros["idpadre"])) {
				$_iddep=substr($this->oBD->escapar($filtros["idpadre"]),1,2);
				$cond1 = " WHERE departamento= " .$_iddep." AND provincia<>'00' AND distrito='00'";	
			}

			if(!empty($filtros["idpadre1"])) {
				$_iddep=substr($this->oBD->escapar($filtros["idpadre1"]),1,2);
				$_idpro=substr($this->oBD->escapar($filtros["idpadre1"]),3,2);
				$cond1 = " WHERE departamento='" .$_iddep."' AND provincia='".$_idpro."' AND distrito<>'00'";	
			}


			/*if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["abrev"])) {
					$cond[] = "abrev = " . $this->oBD->escapar($filtros["abrev"]);
			}
			if(!empty($filtros["iddepartamento"])) {
					$cond[] = "iddepartamento = " . $this->oBD->escapar($filtros["iddepartamento"]);
			}
			if(!empty($filtros["idprovincia"])) {
					$cond[] = "idprovincia = " . $this->oBD->escapar($filtros["idprovincia"]);
			}*/			
			//if(!empty($cond)) {
				//$sql .= " WHERE " . implode(' AND ', $cond);
				$sql .=$cond1;
			//}
			
			//$sql .= " ORDER BY fecha_creado ASC";
				//echo $sql;
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}

	/*public function listarall()
	{
		try {
			$sql = "SELECT tb1.*,tb2.ciudad AS _ciudad ,tb3.ciudad AS _ciudad  FROM ugel tb1 LEFT JOIN ubigeo tb2 ON tb1.iddepartamento=tb2.departamento  LEFT JOIN ubigeo tb3 ON tb1.idprovincia=tb3.provincia  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}*/
	
	public function insertar($descripcion,$abrev,$iddepartamento,$idprovincia)
	{
		try {
			
			$this->iniciarTransaccion('dat_ugel_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idugel) FROM ugel");
			++$id;
			
			$estados = array('idugel' => $id							
							,'descripcion'=>$descripcion
							,'abrev'=>$abrev
							,'iddepartamento'=>$iddepartamento
							,'idprovincia'=>$idprovincia							
							);
			//var_dump($estados);exit;
			
			$this->oBD->insert('ugel', $estados);			
			$this->terminarTransaccion('dat_ugel_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_ugel_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $descripcion,$abrev,$iddepartamento,$idprovincia)
	{
		try {
			$this->iniciarTransaccion('dat_ugel_update');
			$estados = array('descripcion'=>$descripcion
							,'abrev'=>$abrev
							,'iddepartamento'=>$iddepartamento
							,'idprovincia'=>$idprovincia								
							);
			
			$this->oBD->update('ugel ', $estados, array('idugel' => $id));
		    $this->terminarTransaccion('dat_ugel_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT tb1.*,tb2.ciudad AS _ciudad ,tb3.ciudad AS _ciudad  FROM ugel tb1 LEFT JOIN ubigeo tb2 ON tb1.iddepartamento=tb2.departamento  LEFT JOIN ubigeo tb3 ON tb1.idprovincia=tb3.provincia  "
					. " WHERE tb1.idugel = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('ugel', array('idugel' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('ugel', array($propiedad => $valor), array('idugel' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Ugel").": " . $e->getMessage());
		}
	}		
}