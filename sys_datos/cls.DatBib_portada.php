<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_portada extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_portada").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_portada";
			
			$cond = array();		
			
			if(!empty($filtros["id_portada"])) {
					$cond[] = "id_portada = " . $this->oBD->escapar($filtros["id_portada"]);
			}
			if(!empty($filtros["foto"])) {
					$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_portada").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_portada";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_portada"])) {
					$cond[] = "id_portada = " . $this->oBD->escapar($filtros["id_portada"]);
			}
			if(!empty($filtros["foto"])) {
					$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_portada").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_portada  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_portada").": " . $e->getMessage());
		}
	}
	
	public function insertar($foto,$estado)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_portada_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_portada) FROM bib_portada");
			++$id;
			
			$estados = array('id_portada' => $id							
							,'foto'=>$foto
							,'estado'=>$estado							
							);
			if($estado==1){
				$this->oBD->update('bib_portada ',array('estado'=>0), array('estado' => 1));
			}
			$this->oBD->insert('bib_portada', $estados);			
			$this->terminarTransaccion('dat_bib_portada_insert');

			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_portada_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_portada").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $foto,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_bib_portada_update');
			$estados = array('foto'=>$foto
							,'estado'=>$estado								
							);
			if($estado==1){
				$this->oBD->update('bib_portada ',array('estado'=>0), array('estado' => 1));
			}
			$this->oBD->update('bib_portada ', $estados, array('id_portada' => $id));
		    $this->terminarTransaccion('dat_bib_portada_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_portada").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_portada  "
					. " WHERE id_portada = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_portada").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_portada', array('id_portada' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_portada").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			if($valor==1 && $propiedad=='estado'){
				$this->oBD->update('bib_portada ',array('estado'=>0), array('estado' => 1));
			}
			$this->oBD->update('bib_portada', array($propiedad => $valor), array('id_portada' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_portada").": " . $e->getMessage());
		}
	}
   
		
}