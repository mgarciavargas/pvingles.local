<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		05-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_grupoaula extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_grupoaula";
			
			$cond = array();		
			
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["comentario"])) {
					$cond[] = "comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if(isset($filtros["nvacantes"])) {
					$cond[] = "nvacantes = " . $this->oBD->escapar($filtros["nvacantes"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT ag.*, (SELECT nombre from general g WHERE ag.tipo=g.codigo AND tipo_tabla='tipogrupo') as strtipo FROM acad_grupoaula ag";			
			
			$cond = array();
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["comentario"])) {
					$cond[] = "comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if(isset($filtros["nvacantes"])) {
					$cond[] = "nvacantes = " . $this->oBD->escapar($filtros["nvacantes"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}


	public function buscargrupos($filtros=null){
		try {
			$sql = "SELECT ag.idgrupoaula,ag.nombre as strgrupoaula, comentario,nvacantes, idgrupoauladetalle, agd.nombre ,fecha_inicio,fecha_final, tipo, (SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, estado, (SELECT nombre from general g2 WHERE ag.estado=g2.codigo AND tipo_tabla='estadogrupo') as strestadogrupo, idcurso, (SELECT nombre FROM acad_curso ac WHERE agd.idcurso=ac.idcurso) AS strcurso , iddocente, (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE agd.iddocente=dni) AS strdocente, idlocal, (SELECT nombre FROM local l WHERE agd.idlocal=l.idlocal ) AS strlocal, idambiente, (SELECT CONCAT(numero,' ',(SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE agd.idambiente=am.idambiente) AS strambiente FROM acad_grupoaula ag LEFT JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula ";
			$cond = array();
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "ag.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["texto"])) {
					$cond[] = "ag.nombre " . $this->oBD->Like($filtros["texto"]);
					$cond[] = "comentario " . $this->oBD->Like($filtros["texto"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "agd.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
					$cond[] = "agd.idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "ag.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "agd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "agd.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql;

			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}

	}
	
	
	public function insertar($nombre,$tipo,$comentario,$nvacantes,$estado)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_grupoaula_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupoaula) FROM acad_grupoaula");
			++$id;
			
			$estados = array('idgrupoaula' => $id
							
							,'nombre'=>$nombre
							,'tipo'=>$tipo
							,'comentario'=>$comentario
							,'nvacantes'=>$nvacantes
							,'estado'=>$estado							
							);
			
			$this->oBD->insert('acad_grupoaula', $estados);			
			$this->terminarTransaccion('dat_acad_grupoaula_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_grupoaula_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$tipo,$comentario,$nvacantes,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_acad_grupoaula_update');
			$estados = array('nombre'=>$nombre
							,'tipo'=>$tipo
							,'comentario'=>$comentario
							,'nvacantes'=>$nvacantes
							,'estado'=>$estado								
							);
			
			$this->oBD->update('acad_grupoaula ', $estados, array('idgrupoaula' => $id));
		    $this->terminarTransaccion('dat_acad_grupoaula_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT ag.*, (SELECT nombre from general g WHERE ag.tipo=g.codigo AND tipo_tabla='tipogrupo') as strtipo FROM acad_grupoaula ag WHERE idgrupoaula = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_grupoaula', array('idgrupoaula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_grupoaula', array($propiedad => $valor), array('idgrupoaula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}
   
		
}