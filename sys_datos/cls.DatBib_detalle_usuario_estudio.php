<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_detalle_usuario_estudio extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_detalle_usuario_estudio").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_detalle_usuario_estudio ";
			
			$cond = array();		
			
			if(!empty($filtros["id_detalle"])) {
					$cond[] = "id_detalle = " . $this->oBD->escapar($filtros["id_detalle"]);
			}
			if(!empty($filtros["id_personal"])) {
					$cond[] = "id_personal = " . $this->oBD->escapar($filtros["id_personal"]);
			}
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_detalle_usuario_estudio").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_detalle_usuario_estudio f left join bib_estudio l on l.id_estudio=f.id_estudio";			
			
			$cond = array();		
					

			if(!empty($filtros["id_personal"])) {
					$cond[] = "id_personal = " . $this->oBD->escapar($filtros["id_personal"]);
			}
			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_detalle_usuario_estudio").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_detalle_usuario_estudio  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_detalle_usuario_estudio").": " . $e->getMessage());
		}
	}
	
	public function listar1($dni)
	{
		try {
			$sql = "SELECT * FROM bib_detalle_usuario_estudio f inner join bib_estudio l on l.id_estudio=f.id_estudio where id_personal='$dni'";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_estudio").": " . $e->getMessage());
		}
	}
	
	public function insertar($id_personal,$id_estudio)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_detalle_usuario_estudio_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_detalle) FROM bib_detalle_usuario_estudio");
			++$id;
			
			$estados = array('id_detalle' => $id
							
							,'id_personal'=>$id_personal
							,'id_estudio'=>$id_estudio						
							);
			
			$this->oBD->insert('bib_detalle_usuario_estudio', $estados);			
			$this->terminarTransaccion('dat_bib_detalle_usuario_estudio_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_detalle_usuario_estudio_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_detalle_usuario_estudio").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_personal,$id_estudio,$tipo)
	{
		try {
			$this->iniciarTransaccion('dat_bib_detalle_usuario_estudio_update');
			$estados = array('id_personal'=>$id_personal
							,'id_estudio'=>$id_estudio
							,'tipo'=>$tipo								
							);
			
			$this->oBD->update('bib_detalle_usuario_estudio ', $estados, array('id_detalle' => $id));
		    $this->terminarTransaccion('dat_bib_detalle_usuario_estudio_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_detalle_usuario_estudio").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_detalle_usuario_estudio  "
					. " WHERE id_detalle = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_detalle_usuario_estudio").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_detalle_usuario_estudio', array('id_detalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_detalle_usuario_estudio").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_detalle_usuario_estudio', array($propiedad => $valor), array('id_detalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_detalle_usuario_estudio").": " . $e->getMessage());
		}
	}
   
		
}