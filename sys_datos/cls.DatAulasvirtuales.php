<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-05-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatAulasvirtuales extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Aulasvirtuales").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM aulasvirtuales";
			
			$cond = array();		
			
			if(!empty($filtros["aulaid"])) {
					$cond[] = "aulaid = " . $this->oBD->escapar($filtros["aulaid"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio >= " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(!empty($filtros["fecha_inicio2"])) {
					$cond[] = "fecha_inicio <= " . $this->oBD->escapar($filtros["fecha_inicio2"]);
			}
			if(!empty($filtros["fecha_final"])) {
					$cond[] = "fecha_final <= " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(!empty($filtros["titulo"])) {
					$cond[] = "titulo " . $this->oBD->like($filtros["titulo"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["moderadores"])) {
					$cond[] = "moderadores = " . $this->oBD->escapar($filtros["moderadores"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["video"])) {
					$cond[] = "video = " . $this->oBD->escapar($filtros["video"]);
			}
			if(!empty($filtros["chat"])) {
					$cond[] = "chat = " . $this->oBD->escapar($filtros["chat"]);
			}
			if(!empty($filtros["notas"])) {
					$cond[] = "notas = " . $this->oBD->escapar($filtros["notas"]);
			}
			if(!empty($filtros["dirigidoa"])) {
					$cond[] = "dirigidoa = " . $this->oBD->escapar($filtros["dirigidoa"]);
			}
			if(!empty($filtros["estudiantes"])) {
					$cond[] = "estudiantes = " . $this->oBD->escapar($filtros["estudiantes"]);
			}
			if(!empty($filtros["dni"])) {
					$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(!empty($filtros["fecha_creado"])) {
					$cond[] = "fecha_creado = " . $this->oBD->escapar($filtros["fecha_creado"]);
			}
			if(!empty($filtros["portada"])) {
					$cond[] = "portada = " . $this->oBD->escapar($filtros["portada"]);
			}
			if(!empty($filtros["nparticipantes"])) {
					$cond[] = "nparticipantes = " . $this->oBD->escapar($filtros["nparticipantes"]);
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Aulasvirtuales").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM aulasvirtuales";			
			
			$cond = array();		
					
			if(!empty($filtros["aulaid"])) {
					$cond[] = "aulaid = " . $this->oBD->escapar($filtros["aulaid"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio >= " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(!empty($filtros["fecha_inicio2"])) {
					$cond[] = "fecha_inicio <= " . $this->oBD->escapar($filtros["fecha_inicio2"]);
			}
			if(!empty($filtros["fecha_final"])) {
					$cond[] = "fecha_final <= " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(!empty($filtros["titulo"])) {
					$cond[] = "titulo " . $this->oBD->like($filtros["titulo"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["moderadores"])) {
					$cond[] = "moderadores = " . $this->oBD->escapar($filtros["moderadores"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["video"])) {
					$cond[] = "video = " . $this->oBD->escapar($filtros["video"]);
			}
			if(!empty($filtros["chat"])) {
					$cond[] = "chat = " . $this->oBD->escapar($filtros["chat"]);
			}
			if(!empty($filtros["notas"])) {
					$cond[] = "notas = " . $this->oBD->escapar($filtros["notas"]);
			}
			if(!empty($filtros["dirigidoa"])) {
					$cond[] = "dirigidoa = " . $this->oBD->escapar($filtros["dirigidoa"]);
			}
			if(!empty($filtros["estudiantes"])) {
					$cond[] = "estudiantes = " . $this->oBD->escapar($filtros["estudiantes"]);
			}
			if(!empty($filtros["dni"])) {
					$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(!empty($filtros["fecha_creado"])) {
					$cond[] = "fecha_creado = " . $this->oBD->escapar($filtros["fecha_creado"]);
			}
			if(!empty($filtros["portada"])) {
					$cond[] = "portada = " . $this->oBD->escapar($filtros["portada"]);
			}
			if(!empty($filtros["nparticipantes"])) {
					$cond[] = "nparticipantes = " . $this->oBD->escapar($filtros["nparticipantes"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Aulasvirtuales").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM aulasvirtuales";			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Aulasvirtuales").": " . $e->getMessage());
		}
	}

	
	public function insertar($idnivel,$idunidad,$idactividad,$fecha_inicio,$fecha_final,$titulo,$descripcion,$moderadores,$estado,$video,$chat,$notas,$dirigidoa,$estudiantes,$dni,$fecha_creado,$portada,$filtroestudiantes,$nparticipantes=40)
	{
		try {			
			$this->iniciarTransaccion('dat_aulasvirtuales_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(aulaid) FROM aulasvirtuales");
			++$id;			
			$estados = array('aulaid' => $id
							
							,'idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'moderadores'=>$moderadores
							,'estado'=>$estado
							,'video'=>$video
							,'chat'=>$chat
							,'notas'=>$notas
							,'dirigidoa'=>$dirigidoa
							,'estudiantes'=>$estudiantes
							,'dni'=>$dni
							,'fecha_creado'=>$fecha_creado
							,'portada'=>$portada
							,'filtroestudiantes'=>$filtroestudiantes
							,'nparticipantes'=>$nparticipantes							
							);
			
			$this->oBD->insert('aulasvirtuales', $estados);			
			$this->terminarTransaccion('dat_aulasvirtuales_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_aulasvirtuales_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Aulasvirtuales").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idnivel,$idunidad,$idactividad,$fecha_inicio,$fecha_final,$titulo,$descripcion,$moderadores,$estado,$video,$chat,$notas,$dirigidoa,$estudiantes,$dni,$fecha_creado,$portada,$filtroestudiantes,$nparticipantes=40)
	{
		try {
			$this->iniciarTransaccion('dat_aulasvirtuales_update');
			$estados = array('idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'moderadores'=>$moderadores
							,'estado'=>$estado
							,'video'=>$video
							,'chat'=>$chat
							,'notas'=>$notas
							,'dirigidoa'=>$dirigidoa
							,'estudiantes'=>$estudiantes
							,'dni'=>$dni
							,'fecha_creado'=>$fecha_creado
							,'portada'=>$portada
							,'filtroestudiantes'=>$filtroestudiantes
							,'nparticipantes'=>$nparticipantes
							);
			$this->oBD->update('aulasvirtuales ', $estados, array('aulaid' => $id));
		    $this->terminarTransaccion('dat_aulasvirtuales_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Aulasvirtuales").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM aulasvirtuales WHERE aulaid = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Aulasvirtuales").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('aulasvirtuales', array('aulaid' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Aulasvirtuales").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			return $this->oBD->update('aulasvirtuales', array($propiedad => $valor), array('aulaid' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Aulasvirtuales").": " . $e->getMessage());
		}
	}
   
		
}