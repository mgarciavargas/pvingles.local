<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-05-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_grabacion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("bib_grabacion").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_grabacion";
			
			$cond = array();		
			
			if(!empty($filtros["id_grabacion"])) {
					$cond[] = "id_grabacion = " . $this->oBD->escapar($filtros["id_grabacion"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["ruta"])) {
					$cond[] = "ruta = " . $this->oBD->escapar($filtros["ruta"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("bib_grabacion").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_grabacion";			
			
			$cond = array();		
					
			if(!empty($filtros["id_grabacion"])) {
					$cond[] = "id_grabacion = " . $this->oBD->escapar($filtros["id_grabacion"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["ruta"])) {
					$cond[] = "ruta = " . $this->oBD->escapar($filtros["ruta"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Grabacion").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_grabacion  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("bib_grabacion").": " . $e->getMessage());
		}
	}
	
	public function insertar($nombre,$ruta)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_grabacion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_grabacion) FROM bib_grabacion");
			++$id;
			
			$estados = array('id_grabacion' => $id
							,'nombre'=>$nombre
							,'ruta'=>$ruta
							);
			
			$this->oBD->insert('bib_grabacion', $estados);			
			$this->terminarTransaccion('dat_bib_grabacion_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_grabacion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("bib_grabacion").": " . $e->getMessage());
		}
	}
	public function actualizar($id,$nombre,$ruta)
	{
		try {
			$this->iniciarTransaccion('dat_bib_grabacion_update');
			$estados = array('nombre'=>$nombre
							,'ruta'=>$ruta
							);
			
			$this->oBD->update('bib_grabacion ', $estados, array('id_grabacion' => $id));
		    $this->terminarTransaccion('dat_bib_grabacion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("bib_grabacion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_grabacion  "
					. " WHERE id_grabacion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("bib_grabacion").": " . $e->getMessage());
		}
	}

	public function eliminar2($id)
	{
		try {
			return $this->oBD->delete('bib_grabacion', array('id_grabacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("bib_grabacion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_grabacion', array($propiedad => $valor), array('id_grabacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("bib_grabacion").": " . $e->getMessage());
		}
	}
   
		
}