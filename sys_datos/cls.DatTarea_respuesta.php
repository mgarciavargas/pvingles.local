<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-05-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatTarea_respuesta extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Tarea_respuesta").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM tarea_respuesta";
			
			$cond = array();		
			
			if(!empty($filtros["idtarea_respuesta"])) {
					$cond[] = "idtarea_respuesta = " . $this->oBD->escapar($filtros["idtarea_respuesta"]);
			}
			if(!empty($filtros["idtarea_asignacion_alumno"])) {
					$cond[] = "idtarea_asignacion_alumno = " . $this->oBD->escapar($filtros["idtarea_asignacion_alumno"]);
			}
			if(!empty($filtros["comentario"])) {
					$cond[] = "comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if(!empty($filtros["fechapresentacion"])) {
					$cond[] = "fechapresentacion = " . $this->oBD->escapar($filtros["fechapresentacion"]);
			}
			if(!empty($filtros["horapresentacion"])) {
					$cond[] = "horapresentacion = " . $this->oBD->escapar($filtros["horapresentacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Tarea_respuesta").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM tarea_respuesta";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idtarea_respuesta"])) {
					$cond[] = "idtarea_respuesta = " . $this->oBD->escapar($filtros["idtarea_respuesta"]);
			}
			if(!empty($filtros["idtarea_asignacion_alumno"])) {
					$cond[] = "idtarea_asignacion_alumno = " . $this->oBD->escapar($filtros["idtarea_asignacion_alumno"]);
			}
			if(!empty($filtros["comentario"])) {
					$cond[] = "comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if(!empty($filtros["fechapresentacion"])) {
					$cond[] = "fechapresentacion = " . $this->oBD->escapar($filtros["fechapresentacion"]);
			}
			if(!empty($filtros["horapresentacion"])) {
					$cond[] = "horapresentacion = " . $this->oBD->escapar($filtros["horapresentacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Tarea_respuesta").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM tarea_respuesta  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Tarea_respuesta").": " . $e->getMessage());
		}
	}
	
	public function insertar($idtarea_asignacion_alumno,$comentario,$fechapresentacion,$horapresentacion)
	{
		try {
			
			$this->iniciarTransaccion('dat_tarea_respuesta_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtarea_respuesta) FROM tarea_respuesta");
			++$id;
			
			$estados = array('idtarea_respuesta' => $id
							,'idtarea_asignacion_alumno'=>$idtarea_asignacion_alumno
							,'fechapresentacion'=>$fechapresentacion
							,'horapresentacion'=>$horapresentacion
							);
			if(!empty($comentario)){
				$estados["comentario"] = $comentario;
			}
			$this->oBD->insert('tarea_respuesta', $estados);			
			$this->terminarTransaccion('dat_tarea_respuesta_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_tarea_respuesta_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Tarea_respuesta").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idtarea_asignacion_alumno,$comentario,$fechapresentacion,$horapresentacion)
	{
		try {
			$this->iniciarTransaccion('dat_tarea_respuesta_update');
			$estados = array('idtarea_asignacion_alumno'=>$idtarea_asignacion_alumno
							,'fechapresentacion'=>$fechapresentacion
							,'horapresentacion'=>$horapresentacion
							);
			if(!empty($comentario)){
				$estados["comentario"] = $comentario;
			}
			$this->oBD->update('tarea_respuesta ', $estados, array('idtarea_respuesta' => $id));
		    $this->terminarTransaccion('dat_tarea_respuesta_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea_respuesta").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM tarea_respuesta  "
					. " WHERE idtarea_respuesta = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Tarea_respuesta").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('tarea_respuesta', array('idtarea_respuesta' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Tarea_respuesta").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('tarea_respuesta', array($propiedad => $valor), array('idtarea_respuesta' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea_respuesta").": " . $e->getMessage());
		}
	}
   
		
}