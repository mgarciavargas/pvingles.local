<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatLogro extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Logro").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM logro";
			
			$cond = array();		
			
			if(isset($filtros["id_logro"])) {
					$cond[] = "id_logro = " . $this->oBD->escapar($filtros["id_logro"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo " . $this->oBD->Like($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			
			if(isset($filtros["puntaje"])) {
					$cond[] = "puntaje = " . $this->oBD->escapar($filtros["puntaje"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Logro").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM logro";			
			
			$cond = array();		
					
			
			if(isset($filtros["id_logro"])) {
					$cond[] = "id_logro = " . $this->oBD->escapar($filtros["id_logro"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo " . $this->oBD->Like($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["puntaje"])) {
					$cond[] = "puntaje = " . $this->oBD->escapar($filtros["puntaje"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Logro").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($titulo,$descripcion,$tipo,$imagen,$puntaje,$estado)
	{
		try {
			
			$this->iniciarTransaccion('dat_logro_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_logro) FROM logro");
			++$id;
			
			$estados = array('id_logro' => $id
							
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'tipo'=>$tipo
							,'imagen'=>$imagen
							,'puntaje'=>$puntaje
							,'estado'=>$estado							
							);
			
			$this->oBD->insert('logro', $estados);			
			$this->terminarTransaccion('dat_logro_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_logro_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Logro").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $titulo,$descripcion,$tipo,$imagen,$puntaje,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_logro_update');
			$estados = array('titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'tipo'=>$tipo
							,'imagen'=>$imagen
							,'puntaje'=>$puntaje
							,'estado'=>$estado							
							);
			
			$this->oBD->update('logro ', $estados, array('id_logro' => $id));
		    $this->terminarTransaccion('dat_logro_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Logro").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM logro  "
					. " WHERE id_logro = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Logro").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('logro', array('id_logro' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Logro").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('logro', array($propiedad => $valor), array('id_logro' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Logro").": " . $e->getMessage());
		}
	}
   
		
}