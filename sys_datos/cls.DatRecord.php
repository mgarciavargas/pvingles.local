<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		02-01-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatRecord extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Record").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM record";
			
			$cond = array();		
			
			if(!empty($filtros["idrecord"])) {
					$cond[] = "idrecord = " . $this->oBD->escapar($filtros["idrecord"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["link"])) {
					$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idleccion"])) {
					$cond[] = "idleccion = " . $this->oBD->escapar($filtros["idleccion"]);
			}
			if(!empty($filtros["fechareg"])) {
					$cond[] = "fechareg = " . $this->oBD->escapar($filtros["fechareg"]);
			}
			if(!empty($filtros["idherramienta"])) {
					$cond[] = "idherramienta = " . $this->oBD->escapar($filtros["idherramienta"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Record").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM record";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idrecord"])) {
					$cond[] = "idrecord = " . $this->oBD->escapar($filtros["idrecord"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["link"])) {
					$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idleccion"])) {
					$cond[] = "idleccion = " . $this->oBD->escapar($filtros["idleccion"]);
			}
			if(!empty($filtros["fechareg"])) {
					$cond[] = "fechareg = " . $this->oBD->escapar($filtros["fechareg"]);
			}
			if(!empty($filtros["idherramienta"])) {
					$cond[] = "idherramienta = " . $this->oBD->escapar($filtros["idherramienta"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Record").": " . $e->getMessage());
		}
	}
	
	public function insertar($nombre,$link,$idpersonal,$idnivel,$idunidad,$idleccion,$fechareg,$idherramienta)
	{
		try {
			
			$this->iniciarTransaccion('dat_record_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idrecord) FROM record");
			++$id;			
			$estados = array('idrecord' => $id							
							,'nombre'=>$nombre
							,'link'=>$link
							,'idpersonal'=>$idpersonal
							,'idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idleccion'=>$idleccion
							,'fechareg'=>$fechareg
							,'idherramienta'=>$idherramienta							
							);
			
			$this->oBD->insert('record', $estados);
			$this->terminarTransaccion('dat_record_insert');
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_record_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Record").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$link,$idpersonal,$idnivel,$idunidad,$idleccion,$fechareg,$idherramienta)
	{
		try {
			$this->iniciarTransaccion('dat_record_update');
			$estados = array('nombre'=>$nombre
							,'link'=>$link
							,'idpersonal'=>$idpersonal
							,'idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idleccion'=>$idleccion
							,'fechareg'=>$fechareg
							,'idherramienta'=>$idherramienta								
							);
			
			$this->oBD->update('record ', $estados, array('idrecord' => $id));
		    $this->terminarTransaccion('dat_record_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Record").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM record  "
					. " WHERE idrecord = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Record").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('record', array('idrecord' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Record").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('record', array($propiedad => $valor), array('idrecord' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Record").": " . $e->getMessage());
		}
	}
   
		
}