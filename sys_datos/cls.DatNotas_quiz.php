<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatNotas_quiz extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Notas_quiz").": " . $e->getMessage());
		}
	}

	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM notas_quiz";
			
			$cond = array();		
			
			if(isset($filtros["idnota"])) {
					$cond[] = "idnota = " . $this->oBD->escapar($filtros["idnota"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["nota"])) {
					$cond[] = "nota = " . $this->oBD->escapar($filtros["nota"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Notas_quiz").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM notas_quiz";
			
			$cond = array();		
					
			
			if(isset($filtros["idnota"])) {
					$cond[] = "idnota = " . $this->oBD->escapar($filtros["idnota"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["nota"])) {
					$cond[] = "nota = " . $this->oBD->escapar($filtros["nota"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Notas_quiz").": " . $e->getMessage());
		}
	}
	
	public function insertar($idcursodetalle,$idrecurso,$idalumno,$tipo,$nota,$regfecha)
	{
		try {
			
			$this->iniciarTransaccion('dat_notas_quiz_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idnota) FROM notas_quiz");
			++$id;
			
			$estados = array('idnota' => $id
							
							,'idalumno'=>$idalumno
							,'tipo'=>$tipo
							,'nota'=>$nota
							,'regfecha'=>$regfecha							
							);
			if(!empty($idcursodetalle)){
				$estados['idcursodetalle'] = $idcursodetalle;
			}
			if(!empty($idrecurso)){
				$estados['idrecurso'] = $idrecurso;
			}
			
			$this->oBD->insert('notas_quiz', $estados);			
			$this->terminarTransaccion('dat_notas_quiz_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_notas_quiz_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Notas_quiz").": " . $e->getMessage());
		}
	}

	public function actualizar($id, $idcursodetalle,$idrecurso,$idalumno,$tipo,$nota,$regfecha)
	{
		try {
			$this->iniciarTransaccion('dat_notas_quiz_update');
			$estados = array('idalumno'=>$idalumno
							,'tipo'=>$tipo
							,'nota'=>$nota
							,'regfecha'=>$regfecha								
							);
			if(!empty($idcursodetalle)){
				$estados['idcursodetalle'] = $idcursodetalle;
			}
			if(!empty($idrecurso)){
				$estados['idrecurso'] = $idrecurso;
			}
			
			$this->oBD->update('notas_quiz ', $estados, array('idnota' => $id));
		    $this->terminarTransaccion('dat_notas_quiz_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notas_quiz").": " . $e->getMessage());
		}
	}

	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM notas_quiz  "
					. " WHERE idnota = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Notas_quiz").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('notas_quiz', array('idnota' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Notas_quiz").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('notas_quiz', array($propiedad => $valor), array('idnota' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notas_quiz").": " . $e->getMessage());
		}
	}
}