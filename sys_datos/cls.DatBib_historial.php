<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_historial extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_historial").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_historial";
			
			$cond = array();		
			
			if(!empty($filtros["id_historial"])) {
					$cond[] = "id_historial = " . $this->oBD->escapar($filtros["id_historial"]);
			}
			if(!empty($filtros["id_personal"])) {
					$cond[] = "id_personal = " . $this->oBD->escapar($filtros["id_personal"]);
			}
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}	
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["fec_registro"])) {
					$cond[] = "fec_registro = " . $this->oBD->escapar($filtros["fec_registro"]);
			}
			if(!empty($filtros["tipo_usuario"])) {
					$cond[] = "tipo_usuario = " . $this->oBD->escapar($filtros["tipo_usuario"]);
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_historial").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_historial";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_historial"])) {
					$cond[] = "id_historial = " . $this->oBD->escapar($filtros["id_historial"]);
			}
			if(!empty($filtros["id_personal"])) {
					$cond[] = "id_personal = " . $this->oBD->escapar($filtros["id_personal"]);
			}
			if(!empty($filtros["id_estudio"])) {
					$cond[] = "id_estudio = " . $this->oBD->escapar($filtros["id_estudio"]);
			}	
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["fec_registro"])) {
					$cond[] = "fec_registro = " . $this->oBD->escapar($filtros["fec_registro"]);
			}
			if(!empty($filtros["tipo_usuario"])) {
					$cond[] = "tipo_usuario = " . $this->oBD->escapar($filtros["tipo_usuario"]);
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_historial").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_historial  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_historial").": " . $e->getMessage());
		}
	}
	
	public function insertar($id_personal,$id_estudio,$descripcion,$tipo_usuario,$cantidad)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_historial_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_historial) FROM bib_historial");
			++$id;
			
			$estados = array('id_historial' => $id
							
							,'id_personal'=>$id_personal
							,'id_estudio'=>$id_estudio
							,'descripcion'=>$descripcion
							,'tipo_usuario'=>$tipo_usuario	
							,'cantidad'=>$cantidad						
							);
			$this->oBD->insert('bib_historial', $estados);			
			$this->terminarTransaccion('dat_bib_historial_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_historial_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_historial").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_personal,$id_estudio,$descripcion,$tipo_usuario,$cantidad)
	{
		try {
			$this->iniciarTransaccion('dat_bib_historial_update');
			$estados = array('id_personal'=>$id_personal
							,'id_estudio'=>$id_estudio	
							,'descripcion'=>$descripcion
							,'tipo_usuario'=>$tipo_usuario	
							,'cantidad'=>$cantidad						
							);
			
			$this->oBD->update('bib_historial ', $estados, array('id_historial' => $id));
		    $this->terminarTransaccion('dat_bib_historial_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_historial").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_historial  "
					. " WHERE id_historial = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_historial").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_historial', array('id_historial' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_historial").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_historial', array($propiedad => $valor), array('id_historial' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_historial").": " . $e->getMessage());
		}
	}
   
		
}