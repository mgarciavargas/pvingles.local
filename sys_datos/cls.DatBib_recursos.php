<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_recursos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_recursos";
			
			$cond = array();		
			
			if(!empty($filtros["id_recursos"])) {
					$cond[] = "id_recursos = " . $this->oBD->escapar($filtros["id_recursos"]);
			}
			if(!empty($filtros["id_personal"])) {
					$cond[] = "id_personal = " . $this->oBD->escapar($filtros["id_personal"]);
			}
			if(!empty($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(!empty($filtros["texto"])) {
					$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["caratula"])) {
					$cond[] = "caratula = " . $this->oBD->escapar($filtros["caratula"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["publicado"])) {
					$cond[] = "publicado = " . $this->oBD->escapar($filtros["publicado"]);
			}
			if(!empty($filtros["compartido"])) {
					$cond[] = "compartido = " . $this->oBD->escapar($filtros["compartido"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_recursos";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_recursos"])) {
					$cond[] = "id_recursos = " . $this->oBD->escapar($filtros["id_recursos"]);
			}
			if(!empty($filtros["id_personal"])) {
					$cond[] = "id_personal = " . $this->oBD->escapar($filtros["id_personal"]);
			}
			if(!empty($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(!empty($filtros["texto"])) {
					$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["caratula"])) {
					$cond[] = "caratula = " . $this->oBD->escapar($filtros["caratula"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["publicado"])) {
					$cond[] = "publicado = " . $this->oBD->escapar($filtros["publicado"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["compartido"])) {
					$cond[] = "compartido = " . $this->oBD->escapar($filtros["compartido"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//exit($sql);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}
	}
	public function buscarCambiar($id,$compartido)
	{
		try {
			$this->iniciarTransaccion('dat_bib_recursos_update');
			$estados = array('compartido'=>$compartido															
							);
			
			$this->oBD->update('bib_recursos ', $estados, array('id_recursos' => $id));
		    $this->terminarTransaccion('dat_bib_recursos_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}
		/*try {
			$sql = "SELECT * FROM bib_recursos";			
			
			$cond = array();		
			var_dump($id);
			$estados = array('id_recursos' => $id
							,'compartido'=>1					
							);
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//exit($sql);
			$this->oBD->update('bib_recursos ', $estados, array('id_recursos' => $id,''));
			$this->oBD->update('bib_recursos ', $estados, array('id_recursos' => $id));
		    $this->terminarTransaccion('dat_bib_recursos_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}*/
	}
	public function listarall($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_recursos  ";
            $cond = array();
            if(!empty($filtros["titulo"])) {
					$cond[] = "titulo Like '%" . $filtros["titulo"]."%' ";
			}
            if(!empty($filtros["publicado"])) {
					$cond[] = "publicado = " . $this->oBD->escapar($filtros["publicado"]);
			}
			if(!empty($filtros["compartido"])) {
					$cond[] = "compartido = " . $this->oBD->escapar($filtros["compartido"]);
			}
            if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " GROUP by idpersonal,orden ";
			//echo($sql);
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			//echo($sql);
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}
	}
	public function newOrder($id_personal){
        $orden = $this->oBD->consultarEscalarSQL("SELECT MAX(orden) FROM bib_recursos WHERE id_personal=".$this->oBD->escapar($id_personal));
			++$orden;
        return $orden;
        
    }
	public function insertar($id_personal,$titulo,$texto,$caratula,$estado,$orden,$tipo,$publicado,$compartido,$descripcion)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_recursos_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_recursos) FROM bib_recursos");
			++$id;
			
			$estados = array('id_recursos' => $id
							
							,'id_personal'=>$id_personal
							,'titulo'=>$titulo
							,'texto'=>$texto
							,'caratula'=>$caratula
							,'orden'=>$orden
							,'tipo'=>$tipo
							,'estado'=>$estado
							,'publicado'=>$publicado
							,'compartido'=>$compartido	
							,'descripcion'=>$descripcion						
							);

			$this->oBD->insert('bib_recursos', $estados);			
			$this->terminarTransaccion('dat_bib_recursos_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_recursos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $id_personal,$titulo,$texto,$caratula,$estado,$orden,$tipo,$publicado,$compartido,$descripcion)
	{
		try {
			$this->iniciarTransaccion('dat_bib_recursos_update');
			$estados = array('id_personal'=>$id_personal
							,'titulo'=>$titulo
							,'texto'=>$texto
							,'caratula'=>$caratula
							,'orden'=>$orden
							,'tipo'=>$tipo
							,'estado'=>$estado
							,'publicado'=>$publicado
							,'compartido'=>$compartido								
							,'descripcion'=>$descripcion								
							);
			
			$this->oBD->update('bib_recursos ', $estados, array('id_recursos' => $id));
		    $this->terminarTransaccion('dat_bib_recursos_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_recursos  "
					. " WHERE id_recursos = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_recursos', array('id_recursos' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}
	}
    public function eliminar2($dni,$orden)
	{
		try {
			return $this->oBD->delete('bib_recursos', array('id_personal' => $dni,'orden'=>$orden));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_recursos', array($propiedad => $valor), array('id_recursos' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_recursos").": " . $e->getMessage());
		}
	}
   
		
}