<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_marcacion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_marcacion").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_marcacion";
			
			$cond = array();		
			
			if(isset($filtros["idmarcacion"])) {
					$cond[] = "idmarcacion = " . $this->oBD->escapar($filtros["idmarcacion"]);
			}
			if(isset($filtros["idgrupodetalle"])) {
					$cond[] = "idgrupodetalle = " . $this->oBD->escapar($filtros["idgrupodetalle"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idhorario"])) {
					$cond[] = "idhorario = " . $this->oBD->escapar($filtros["idhorario"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(isset($filtros["horaingreso"])) {
					$cond[] = "horaingreso = " . $this->oBD->escapar($filtros["horaingreso"]);
			}
			if(isset($filtros["horasalida"])) {
					$cond[] = "horasalida = " . $this->oBD->escapar($filtros["horasalida"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["nalumnos"])) {
					$cond[] = "nalumnos = " . $this->oBD->escapar($filtros["nalumnos"]);
			}
			if(isset($filtros["observacion"])) {
					$cond[] = "observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_marcacion").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_marcacion";			
			
			$cond = array();			
			if(isset($filtros["idmarcacion"])) {
					$cond[] = "idmarcacion = " . $this->oBD->escapar($filtros["idmarcacion"]);
			}
			if(isset($filtros["idgrupodetalle"])) {
					$cond[] = "idgrupodetalle = " . $this->oBD->escapar($filtros["idgrupodetalle"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idhorario"])) {
					$cond[] = "idhorario = " . $this->oBD->escapar($filtros["idhorario"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(isset($filtros["horaingreso"])) {
					$cond[] = "horaingreso = " . $this->oBD->escapar($filtros["horaingreso"]);
			}
			if(isset($filtros["horasalida"])) {
					$cond[] = "horasalida = " . $this->oBD->escapar($filtros["horasalida"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["nalumnos"])) {
					$cond[] = "nalumnos = " . $this->oBD->escapar($filtros["nalumnos"]);
			}
			if(isset($filtros["observacion"])) {
					$cond[] = "observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_marcacion").": " . $e->getMessage());
		}
	}

	public function mismarcaciones($filtros=null)
	{
		try {
			$sql = "SELECT m.*, ag.idgrupoaula,ag.nombre as strgrupoaula, idgrupodetalle,  (SELECT nombre from general g1 WHERE ag.tipo=g1.codigo AND tipo_tabla='tipogrupo') as strtipogrupo, estado, (SELECT nombre FROM acad_curso ac WHERE m.idcurso=ac.idcurso) AS strcurso , (SELECT CONCAT(ape_paterno,' ',ape_materno,', ',pe.nombre) FROM personal pe WHERE m.iddocente=dni) AS strdocente, idlocal, (SELECT nombre FROM local l WHERE agd.idlocal=l.idlocal ) AS strlocal, idambiente, (SELECT CONCAT(numero,' ',(SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE agd.idambiente=am.idambiente) AS strambiente FROM acad_grupoaula ag INNER JOIN acad_grupoauladetalle agd ON ag.idgrupoaula=agd.idgrupoaula INNER JOIN acad_marcacion m ON m.idgrupodetalle=agd.idgrupoauladetalle";			
			
			$cond = array();			
			if(isset($filtros["idmarcacion"])) {
					$cond[] = "idmarcacion = " . $this->oBD->escapar($filtros["idmarcacion"]);
			}
			if(isset($filtros["idgrupodetalle"])) {
					$cond[] = "idgrupodetalle = " . $this->oBD->escapar($filtros["idgrupodetalle"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idhorario"])) {
					$cond[] = "idhorario = " . $this->oBD->escapar($filtros["idhorario"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}

			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha >= " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}

			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha <= " . $this->oBD->escapar($filtros["fecha_final"]);
			}

			if(isset($filtros["dni"])) {
					$cond[] = "m.iddocente = " . $this->oBD->escapar($filtros["dni"]);
			}
			
			if(isset($filtros["iddocente"])) {
					$cond[] = "m.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["nalumnos"])) {
					$cond[] = "nalumnos = " . $this->oBD->escapar($filtros["nalumnos"]);
			}
						
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			$sql .= " ORDER BY idmarcacion Desc";
			//echo $sql;			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_marcacion").": " . $e->getMessage());
		}
	}




	
	
	
	public function insertar($idgrupodetalle,$idcurso,$idhorario,$fecha,$horaingreso,$horasalida,$iddocente,$nalumnos,$observacion)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_marcacion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmarcacion) FROM acad_marcacion");
			++$id;
			
			$estados = array('idmarcacion' => $id							
							,'idgrupodetalle'=>$idgrupodetalle
							,'idcurso'=>$idcurso
							,'idhorario'=>$idhorario
							,'fecha'=>$fecha
							,'horaingreso'=>$horaingreso
							,'horasalida'=>$horasalida
							,'iddocente'=>$iddocente
							,'nalumnos'=>$nalumnos
							,'observacion'=>$observacion							
							);
			
			$this->oBD->insert('acad_marcacion', $estados);			
			$this->terminarTransaccion('dat_acad_marcacion_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_marcacion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_marcacion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idgrupodetalle,$idcurso,$idhorario,$fecha,$horaingreso,$horasalida,$iddocente,$nalumnos,$observacion)
	{
		try {
			$this->iniciarTransaccion('dat_acad_marcacion_update');
			$estados = array('idgrupodetalle'=>$idgrupodetalle
							,'idcurso'=>$idcurso
							,'idhorario'=>$idhorario
							,'fecha'=>$fecha
							,'horaingreso'=>$horaingreso
							,'horasalida'=>$horasalida
							,'iddocente'=>$iddocente
							,'nalumnos'=>$nalumnos
							,'observacion'=>$observacion								
							);
			
			$this->oBD->update('acad_marcacion ', $estados, array('idmarcacion' => $id));
		    $this->terminarTransaccion('dat_acad_marcacion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_marcacion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_marcacion  "
					. " WHERE idmarcacion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_marcacion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_marcacion', array('idmarcacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_marcacion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_marcacion', array($propiedad => $valor), array('idmarcacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_marcacion").": " . $e->getMessage());
		}
	}
   
		
}