<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-04-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatHistorial_sesion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM historial_sesion";
			
			$cond = array();		
			
			if(!empty($filtros["idhistorialsesion"])) {
				$cond[] = "idhistorialsesion = " . $this->oBD->escapar($filtros["idhistorialsesion"]);
			}
			if(!empty($filtros["tipousuario"])) {
				$cond[] = "tipousuario = " . $this->oBD->escapar($filtros["tipousuario"]);
			}
			if(!empty($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(!empty($filtros["lugar"])) {
				$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if(!empty($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(!empty($filtros["fechaentrada"])) {
				$cond[] = "fechaentrada = " . $this->oBD->escapar($filtros["fechaentrada"]);
			}
			if(!empty($filtros["fechasalida"])) {
				$cond[] = "fechasalida = " . $this->oBD->escapar($filtros["fechasalida"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM historial_sesion";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idhistorialsesion"])) {
				$cond[] = "idhistorialsesion = " . $this->oBD->escapar($filtros["idhistorialsesion"]);
			}
			if(!empty($filtros["tipousuario"])) {
				$cond[] = "tipousuario = " . $this->oBD->escapar($filtros["tipousuario"]);
			}
			if(!empty($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(!empty($filtros["lugar"])) {
				$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}
			if(!empty($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(!empty($filtros["fechaentrada"])) {
				$cond[] = "fechaentrada = " . $this->oBD->escapar($filtros["fechaentrada"]);
			}
			if(!empty($filtros["fechasalida"])) {
				$cond[] = "fechasalida = " . $this->oBD->escapar($filtros["fechasalida"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM historial_sesion  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}
	
	public function insertar($tipousuario,$idusuario,$lugar,$idcurso,$fechaentrada,$fechasalida)
	{
		try {
			
			$this->iniciarTransaccion('dat_historial_sesion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idhistorialsesion) FROM historial_sesion");
			++$id;
			
			$estados = array('idhistorialsesion' => $id
							,'tipousuario'=>$tipousuario
							,'idusuario'=>$idusuario
							,'lugar'=>$lugar
							,'fechaentrada'=>$fechaentrada
							//,'fechasalida'=>$fechasalida
							);
			if(!empty($idcurso)){
				$estados["idcurso"] = $idcurso;
			}
			$this->oBD->insert('historial_sesion', $estados);			
			$this->terminarTransaccion('dat_historial_sesion_insert');		
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_historial_sesion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $tipousuario,$idusuario,$lugar,$idcurso,$fechaentrada,$fechasalida)
	{
		try {
			$this->iniciarTransaccion('dat_historial_sesion_update');
			$estados = array('tipousuario'=>$tipousuario
							,'idusuario'=>$idusuario
							,'lugar'=>$lugar
							,'fechaentrada'=>$fechaentrada
							,'fechasalida'=>$fechasalida
							);
			
			if(!empty($idcurso)){
				$estados["idcurso"] = $idcurso;
			}
			$this->oBD->update('historial_sesion ', $estados, array('idhistorialsesion' => $id));
		    $this->terminarTransaccion('dat_historial_sesion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM historial_sesion  "
					. " WHERE idhistorialsesion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('historial_sesion', array('idhistorialsesion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('historial_sesion', array($propiedad => $valor), array('idhistorialsesion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}
   
		
}