<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		25-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatBolsa_publicaciones extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bolsa_publicaciones").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bolsa_publicaciones";
			
			$cond = array();		
			
			if(isset($filtros["idpublicacion"])) {
					$cond[] = "idpublicacion = " . $this->oBD->escapar($filtros["idpublicacion"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["sueldo"])) {
					$cond[] = "sueldo = " . $this->oBD->escapar($filtros["sueldo"]);
			}
			if(isset($filtros["nvacantes"])) {
					$cond[] = "nvacantes = " . $this->oBD->escapar($filtros["nvacantes"]);
			}
			if(isset($filtros["disponibilidadeviaje"])) {
					$cond[] = "disponibilidadeviaje = " . $this->oBD->escapar($filtros["disponibilidadeviaje"]);
			}
			if(isset($filtros["duracioncontrato"])) {
					$cond[] = "duracioncontrato = " . $this->oBD->escapar($filtros["duracioncontrato"]);
			}
			if(isset($filtros["xtiempo"])) {
					$cond[] = "xtiempo = " . $this->oBD->escapar($filtros["xtiempo"]);
			}
			if(isset($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["fechapublicacion"])) {
					$cond[] = "fechapublicacion = " . $this->oBD->escapar($filtros["fechapublicacion"]);
			}
			if(isset($filtros["cambioderesidencia"])) {
					$cond[] = "cambioderesidencia = " . $this->oBD->escapar($filtros["cambioderesidencia"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bolsa_publicaciones").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bolsa_publicaciones";			
			
			$cond = array();		
					
			
			if(isset($filtros["idpublicacion"])) {
					$cond[] = "idpublicacion = " . $this->oBD->escapar($filtros["idpublicacion"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["sueldo"])) {
					$cond[] = "sueldo = " . $this->oBD->escapar($filtros["sueldo"]);
			}
			if(isset($filtros["nvacantes"])) {
					$cond[] = "nvacantes = " . $this->oBD->escapar($filtros["nvacantes"]);
			}
			if(isset($filtros["disponibilidadeviaje"])) {
					$cond[] = "disponibilidadeviaje = " . $this->oBD->escapar($filtros["disponibilidadeviaje"]);
			}
			if(isset($filtros["duracioncontrato"])) {
					$cond[] = "duracioncontrato = " . $this->oBD->escapar($filtros["duracioncontrato"]);
			}
			if(isset($filtros["xtiempo"])) {
					$cond[] = "xtiempo = " . $this->oBD->escapar($filtros["xtiempo"]);
			}
			if(isset($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["fechapublicacion"])) {
					$cond[] = "fechapublicacion = " . $this->oBD->escapar($filtros["fechapublicacion"]);
			}
			if(isset($filtros["cambioderesidencia"])) {
					$cond[] = "cambioderesidencia = " . $this->oBD->escapar($filtros["cambioderesidencia"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bolsa_publicaciones").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idempresa,$titulo,$descripcion,$sueldo,$nvacantes,$disponibilidadeviaje,$duracioncontrato,$xtiempo,$fecharegistro,$fechapublicacion,$cambioderesidencia,$mostrar)
	{
		try {
			
			$this->iniciarTransaccion('dat_bolsa_publicaciones_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpublicacion) FROM bolsa_publicaciones");
			++$id;
			
			$estados = array('idpublicacion' => $id
							
							,'idempresa'=>$idempresa
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'sueldo'=>$sueldo
							,'nvacantes'=>$nvacantes
							,'disponibilidadeviaje'=>$disponibilidadeviaje
							,'duracioncontrato'=>$duracioncontrato
							,'xtiempo'=>$xtiempo
							,'fecharegistro'=>$fecharegistro
							,'fechapublicacion'=>$fechapublicacion
							,'cambioderesidencia'=>$cambioderesidencia
							,'mostrar'=>$mostrar							
							);
			
			$this->oBD->insert('bolsa_publicaciones', $estados);			
			$this->terminarTransaccion('dat_bolsa_publicaciones_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bolsa_publicaciones_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bolsa_publicaciones").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idempresa,$titulo,$descripcion,$sueldo,$nvacantes,$disponibilidadeviaje,$duracioncontrato,$xtiempo,$fecharegistro,$fechapublicacion,$cambioderesidencia,$mostrar)
	{
		try {
			$this->iniciarTransaccion('dat_bolsa_publicaciones_update');
			$estados = array('idempresa'=>$idempresa
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'sueldo'=>$sueldo
							,'nvacantes'=>$nvacantes
							,'disponibilidadeviaje'=>$disponibilidadeviaje
							,'duracioncontrato'=>$duracioncontrato
							,'xtiempo'=>$xtiempo
							,'fecharegistro'=>$fecharegistro
							,'fechapublicacion'=>$fechapublicacion
							,'cambioderesidencia'=>$cambioderesidencia
							,'mostrar'=>$mostrar								
							);
			
			$this->oBD->update('bolsa_publicaciones ', $estados, array('idpublicacion' => $id));
		    $this->terminarTransaccion('dat_bolsa_publicaciones_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bolsa_publicaciones").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bolsa_publicaciones  "
					. " WHERE idpublicacion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bolsa_publicaciones").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bolsa_publicaciones', array('idpublicacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bolsa_publicaciones").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bolsa_publicaciones', array($propiedad => $valor), array('idpublicacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bolsa_publicaciones").": " . $e->getMessage());
		}
	}
   
		
}