<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_horariogrupodetalle extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_horariogrupodetalle").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_horariogrupodetalle";
			
			$cond = array();		
			
			if(isset($filtros["idhorario"])) {
					$cond[] = "idhorario = " . $this->oBD->escapar($filtros["idhorario"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["fecha_finicio"])) {
					$cond[] = "fecha_finicio = " . $this->oBD->escapar($filtros["fecha_finicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["color"])) {
					$cond[] = "color = " . $this->oBD->escapar($filtros["color"]);
			}
			if(isset($filtros["idhorariopadre"])) {
					$cond[] = "idhorariopadre = " . $this->oBD->escapar($filtros["idhorariopadre"]);
			}
			if(isset($filtros["diasemana"])) {
					$cond[] = "diasemana = " . $this->oBD->escapar($filtros["diasemana"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_horariogrupodetalle").": " . $e->getMessage());
		}
	}

	public function maxidpadre($idgrupoauladetalle){
		try {
			$sql = "SELECT max(idhorariopadre) as idpadre FROM acad_horariogrupodetalle";			
			$cond = array();
			if(isset($filtros["idhorario"])) {
					$cond[] = "idhorario = " . $this->oBD->escapar($filtros["idhorario"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_horariogrupodetalle").": " . $e->getMessage());
		}

	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_horariogrupodetalle";			
			
			$cond = array();
			if(isset($filtros["idhorario"])) {
					$cond[] = "idhorario = " . $this->oBD->escapar($filtros["idhorario"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["fecha_finicio"])) {
					$cond[] = "fecha_finicio = " . $this->oBD->escapar($filtros["fecha_finicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["color"])) {
					$cond[] = "color = " . $this->oBD->escapar($filtros["color"]);
			}
			if(isset($filtros["idhorariopadre"])) {
					$cond[] = "idhorariopadre = " . $this->oBD->escapar($filtros["idhorariopadre"]);
			}
			if(isset($filtros["diasemana"])) {
					$cond[] = "diasemana = " . $this->oBD->escapar($filtros["diasemana"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_horariogrupodetalle").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idgrupoauladetalle,$fecha_finicio,$fecha_final,$descripcion,$color,$idhorariopadre,$diasemana)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_horariogrupodetalle_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idhorario) FROM acad_horariogrupodetalle");
			++$id;
			
			$estados = array('idhorario' => $id
							
							,'idgrupoauladetalle'=>$idgrupoauladetalle
							,'fecha_finicio'=>$fecha_finicio
							,'fecha_final'=>$fecha_final
							,'descripcion'=>$descripcion
							,'color'=>$color
							,'idhorariopadre'=>$idhorariopadre
							,'diasemana'=>$diasemana							
							);
			
			$this->oBD->insert('acad_horariogrupodetalle', $estados);			
			$this->terminarTransaccion('dat_acad_horariogrupodetalle_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_horariogrupodetalle_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_horariogrupodetalle").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idgrupoauladetalle,$fecha_finicio,$fecha_final,$descripcion,$color,$idhorariopadre,$diasemana)
	{
		try {
			$this->iniciarTransaccion('dat_acad_horariogrupodetalle_update');
			$estados = array('idgrupoauladetalle'=>$idgrupoauladetalle
							,'fecha_finicio'=>$fecha_finicio
							,'fecha_final'=>$fecha_final
							,'descripcion'=>$descripcion
							,'color'=>$color
							,'idhorariopadre'=>$idhorariopadre
							,'diasemana'=>$diasemana								
							);
			
			$this->oBD->update('acad_horariogrupodetalle ', $estados, array('idhorario' => $id));
		    $this->terminarTransaccion('dat_acad_horariogrupodetalle_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_horariogrupodetalle").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_horariogrupodetalle  "
					. " WHERE idhorario = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_horariogrupodetalle").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_horariogrupodetalle', array('idhorario' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_horariogrupodetalle").": " . $e->getMessage());
		}
	}

	public function eliminarxpadre($id)
	{
		try {
			return $this->oBD->delete('acad_horariogrupodetalle', array('idhorariopadre' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_horariogrupodetalle").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_horariogrupodetalle', array($propiedad => $valor), array('idhorario' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_horariogrupodetalle").": " . $e->getMessage());
		}
	}
   
		
}