<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_editorial extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_editorial").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_editorial";
			
			$cond = array();		
			
			if(!empty($filtros["id_editorial"])) {
					$cond[] = "id_editorial = " . $this->oBD->escapar($filtros["id_editorial"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_editorial").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_editorial";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_editorial"])) {
					$cond[] = "id_editorial = " . $this->oBD->escapar($filtros["id_editorial"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_editorial").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_editorial  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_editorial").": " . $e->getMessage());
		}
	}
	
	public function insertar($nombre)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_editorial_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_editorial) FROM bib_editorial");
			++$id;
			
			$estados = array('id_editorial' => $id
							
							,'nombre'=>$nombre							
							);
			
			$this->oBD->insert('bib_editorial', $estados);			
			$this->terminarTransaccion('dat_bib_editorial_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_editorial_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_editorial").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre)
	{
		try {
			$this->iniciarTransaccion('dat_bib_editorial_update');
			$estados = array('nombre'=>$nombre								
							);
			
			$this->oBD->update('bib_editorial ', $estados, array('id_editorial' => $id));
		    $this->terminarTransaccion('dat_bib_editorial_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_editorial").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_editorial  "
					. " WHERE id_editorial = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_editorial").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_editorial', array('id_editorial' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_editorial").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_editorial', array($propiedad => $valor), array('id_editorial' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_editorial").": " . $e->getMessage());
		}
	}
   
		
}