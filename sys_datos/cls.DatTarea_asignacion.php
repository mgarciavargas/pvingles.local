<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-06-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatTarea_asignacion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Tarea_asignacion").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM tarea_asignacion";
			
			$cond = array();		
			
			if(!empty($filtros["idtarea_asignacion"])) {
					$cond[] = "idtarea_asignacion = " . $this->oBD->escapar($filtros["idtarea_asignacion"]);
			}
			if(!empty($filtros["idtarea"])) {
					$cond[] = "idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}
			if(!empty($filtros["idgrupo"])) {
					$cond[] = "idgrupo = " . $this->oBD->escapar($filtros["idgrupo"]);
			}
			if(!empty($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($filtros["fechaentrega"])) {
					$cond[] = "fechaentrega = " . $this->oBD->escapar($filtros["fechaentrega"]);
			}
			if(!empty($filtros["horaentrega"])) {
					$cond[] = "horaentrega = " . $this->oBD->escapar($filtros["horaentrega"]);
			}
			if(!empty($filtros["eliminado"])) {
					$cond[] = "eliminado = " . $this->oBD->escapar($filtros["eliminado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Tarea_asignacion").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			//$sql = "SELECT ta.*, g.idlocal, g.idambiente FROM tarea_asignacion ta JOIN grupos g ON ta.idgrupo=g.idgrupo ";
			$sql = "SELECT ta.*, g.idlocal, g.idambiente FROM tarea_asignacion ta JOIN acad_grupoauladetalle g ON ta.idgrupo=g.idgrupoauladetalle ";
			
			$cond = array();		
					
			
			if(!empty($filtros["idtarea_asignacion"])) {
				$cond[] = "ta.idtarea_asignacion = " . $this->oBD->escapar($filtros["idtarea_asignacion"]);
			}
			if(!empty($filtros["idtarea"])) {
				$cond[] = "ta.idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}
			if(!empty($filtros["idgrupo"])) {
				$cond[] = "ta.idgrupo = " . $this->oBD->escapar($filtros["idgrupo"]);
			}
			if(!empty($filtros["iddocente"])) {
				$cond[] = "ta.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($filtros["fechaentrega"])) {
				$cond[] = "ta.fechaentrega = " . $this->oBD->escapar($filtros["fechaentrega"]);
			}
			if(!empty($filtros["horaentrega"])) {
				$cond[] = "ta.horaentrega = " . $this->oBD->escapar($filtros["horaentrega"]);
			}
			if(!empty($filtros["fechahora_mayorigual"])) {
				$cond[] = "CONCAT(ta.fechaentrega,' ',ta.horaentrega) >= " . $this->oBD->escapar($filtros["fechahora_mayorigual"]);
			}
			if(isset($filtros["eliminado"])) {
				$cond[] = "ta.eliminado = " . $this->oBD->escapar($filtros["eliminado"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY ta.fechaentrega ASC, ta.horaentrega ASC";
			//echo $sql .'<br>';
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Tarea_asignacion").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM tarea_asignacion  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Tarea_asignacion").": " . $e->getMessage());
		}
	}
	
	public function insertar($idtarea,$idgrupo,$iddocente,$fechaentrega,$horaentrega,$eliminado)
	{
		try {
			
			$this->iniciarTransaccion('dat_tarea_asignacion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtarea_asignacion) FROM tarea_asignacion");
			++$id;
			
			$estados = array('idtarea_asignacion' => $id
							
							,'idtarea'=>$idtarea
							,'idgrupo'=>$idgrupo
							,'iddocente'=>$iddocente
							,'fechaentrega'=>$fechaentrega
							,'horaentrega'=>$horaentrega
							,'eliminado'=>$eliminado							
							);
			
			$this->oBD->insert('tarea_asignacion', $estados);			
			$this->terminarTransaccion('dat_tarea_asignacion_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_tarea_asignacion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Tarea_asignacion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idtarea,$idgrupo,$iddocente,$fechaentrega,$horaentrega,$eliminado)
	{
		try {
			$this->iniciarTransaccion('dat_tarea_asignacion_update');
			$estados = array('idtarea'=>$idtarea
							,'idgrupo'=>$idgrupo
							,'iddocente'=>$iddocente
							,'fechaentrega'=>$fechaentrega
							,'horaentrega'=>$horaentrega
							,'eliminado'=>$eliminado								
							);
			
			$this->oBD->update('tarea_asignacion ', $estados, array('idtarea_asignacion' => $id));
		    $this->terminarTransaccion('dat_tarea_asignacion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea_asignacion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM tarea_asignacion  "
					. " WHERE idtarea_asignacion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Tarea_asignacion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('tarea_asignacion', array('idtarea_asignacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Tarea_asignacion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('tarea_asignacion', array($propiedad => $valor), array('idtarea_asignacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea_asignacion").": " . $e->getMessage());
		}
	}
   
		
}