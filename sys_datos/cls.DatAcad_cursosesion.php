<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-11-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatAcad_cursosesion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_cursosesion").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_cursosesion";
			
			$cond = array();		
			
			if(isset($filtros["idsesion"])) {
					$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["duracion"])) {
					$cond[] = "duracion = " . $this->oBD->escapar($filtros["duracion"]);
			}
			if(isset($filtros["informaciongeneral"])) {
					$cond[] = "informaciongeneral = " . $this->oBD->escapar($filtros["informaciongeneral"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_cursosesion").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_cursosesion";			
			
			$cond = array();		
					
			
			if(isset($filtros["idsesion"])) {
					$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["duracion"])) {
					$cond[] = "duracion = " . $this->oBD->escapar($filtros["duracion"]);
			}
			if(isset($filtros["informaciongeneral"])) {
					$cond[] = "informaciongeneral = " . $this->oBD->escapar($filtros["informaciongeneral"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursosesion").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcurso,$duracion,$informaciongeneral,$idcursodetalle)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_cursosesion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idsesion) FROM acad_cursosesion");
			++$id;
			
			$estados = array('idsesion' => $id
							
							,'idcurso'=>$idcurso
							,'duracion'=>$duracion
							,'informaciongeneral'=>$informaciongeneral
							,'idcursodetalle'=>$idcursodetalle							
							);
			
			$this->oBD->insert('acad_cursosesion', $estados);			
			$this->terminarTransaccion('dat_acad_cursosesion_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_cursosesion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_cursosesion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso,$duracion,$informaciongeneral,$idcursodetalle)
	{
		try {
			$this->iniciarTransaccion('dat_acad_cursosesion_update');
			$estados = array('idcurso'=>$idcurso
							,'duracion'=>$duracion
							,'informaciongeneral'=>$informaciongeneral
							,'idcursodetalle'=>$idcursodetalle								
							);
			
			$this->oBD->update('acad_cursosesion ', $estados, array('idsesion' => $id));
		    $this->terminarTransaccion('dat_acad_cursosesion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursosesion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_cursosesion  "
					. " WHERE idsesion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_cursosesion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_cursosesion', array('idsesion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_cursosesion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_cursosesion', array($propiedad => $valor), array('idsesion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursosesion").": " . $e->getMessage());
		}
	}
   
		
}