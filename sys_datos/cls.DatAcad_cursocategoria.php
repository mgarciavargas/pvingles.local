<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-04-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_cursocategoria extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_cursocategoria";
			
			$cond = array();		
			
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["idcategoria"])) {
					$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if(isset($filtros["idsubcategoria"])) {
					$cond[] = "idsubcategoria = " . $this->oBD->escapar($filtros["idsubcategoria"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_cursocategoria";			
			
			$cond = array();		
					
			
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["idcategoria"])) {
					$cond[] = "idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if(isset($filtros["idsubcategoria"])) {
					$cond[] = "idsubcategoria = " . $this->oBD->escapar($filtros["idsubcategoria"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	public function buscarCursos($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_cursocategoria CCAT
				JOIN acad_curso C ON CCAT.idcurso=C.idcurso";			
			
			$cond = array();		
					
			
			if(isset($filtros["id"])) {
					$cond[] = "CCAT.id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["idcategoria"])) {
					$cond[] = "CCAT.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if(isset($filtros["idsubcategoria"])) {
					$cond[] = "CCAT.idsubcategoria = " . $this->oBD->escapar($filtros["idsubcategoria"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "CCAT.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcategoria,$idsubcategoria,$idcurso)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_cursocategoria_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM acad_cursocategoria");
			++$id;
			
			$estados = array('id' => $id
							
							,'idcategoria'=>$idcategoria
							,'idsubcategoria'=>$idsubcategoria
							,'idcurso'=>$idcurso							
							);
			
			$this->oBD->insert('acad_cursocategoria', $estados);			
			$this->terminarTransaccion('dat_acad_cursocategoria_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_cursocategoria_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcategoria,$idsubcategoria,$idcurso)
	{
		try {
			$this->iniciarTransaccion('dat_acad_cursocategoria_update');
			$estados = array('idcategoria'=>$idcategoria
							,'idsubcategoria'=>$idsubcategoria
							,'idcurso'=>$idcurso								
							);
			
			$this->oBD->update('acad_cursocategoria ', $estados, array('id' => $id));
		    $this->terminarTransaccion('dat_acad_cursocategoria_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_cursocategoria  "
					. " WHERE id = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_cursocategoria', array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_cursocategoria', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursocategoria").": " . $e->getMessage());
		}
	}
   
		
}