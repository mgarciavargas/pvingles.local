<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		25-10-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatAgenda extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Agenda").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM agenda";
			
			$cond = array();		
			
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["color"])) {
					$cond[] = "color = " . $this->oBD->escapar($filtros["color"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["alumno_id"])) {
					$cond[] = "alumno_id = " . $this->oBD->escapar($filtros["alumno_id"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Agenda").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM agenda";			
			
			$cond = array();		
					
			
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["color"])) {
					$cond[] = "color = " . $this->oBD->escapar($filtros["color"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}

			if(isset($filtros["fecha_inicio_mayor"])) {
				$cond[] = "fecha_inicio > " . $this->oBD->escapar($filtros["fecha_inicio_mayor"]);
			}
			if(isset($filtros["fecha_inicio_mayorigual"])) {
				$cond[] = "fecha_inicio >= " . $this->oBD->escapar($filtros["fecha_inicio_mayorigual"]);
			}
			if(isset($filtros["fecha_inicio_menor"])) {
				$cond[] = "fecha_inicio < " . $this->oBD->escapar($filtros["fecha_inicio_menor"]);
			}
			if(isset($filtros["fecha_inicio_menorigual"])) {
				$cond[] = "fecha_inicio <= " . $this->oBD->escapar($filtros["fecha_inicio_menorigual"]);
			}
			if(isset($filtros["fecha_final_mayor"])) {
				$cond[] = "fecha_final > " . $this->oBD->escapar($filtros["fecha_final_mayor"]);
			}
			if(isset($filtros["fecha_final_mayorigual"])) {
				$cond[] = "fecha_final >= " . $this->oBD->escapar($filtros["fecha_final_mayorigual"]);
			}
			if(isset($filtros["fecha_final_menor"])) {
				$cond[] = "fecha_final < " . $this->oBD->escapar($filtros["fecha_final_menor"]);
			}
			if(isset($filtros["fecha_final_menorigual"])) {
				$cond[] = "fecha_final <= " . $this->oBD->escapar($filtros["fecha_final_menorigual"]);
			}
			if(isset($filtros["alumno_id"])) {
				$cond[] = "alumno_id = " . $this->oBD->escapar($filtros["alumno_id"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Agenda").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($titulo,$descripcion,$color,$fecha_inicio,$fecha_final,$alumno_id)
	{
		try {
			
			$this->iniciarTransaccion('dat_agenda_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM agenda");
			++$id;
			
			$estados = array('id' => $id
							
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'color'=>$color
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final
							,'alumno_id'=>$alumno_id							
							);
			
			$this->oBD->insert('agenda', $estados);			
			$this->terminarTransaccion('dat_agenda_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_agenda_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Agenda").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $titulo,$descripcion,$color,$fecha_inicio,$fecha_final,$alumno_id)
	{
		try {
			$this->iniciarTransaccion('dat_agenda_update');
			$estados = array('titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'color'=>$color
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final
							,'alumno_id'=>$alumno_id								
							);
			
			$this->oBD->update('agenda ', $estados, array('id' => $id));
		    $this->terminarTransaccion('dat_agenda_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Agenda").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM agenda  "
					. " WHERE id = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Agenda").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			$this->oBD->delete('agenda', array('id' => $id));
			return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Agenda").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('agenda', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Agenda").": " . $e->getMessage());
		}
	}
   
		
}