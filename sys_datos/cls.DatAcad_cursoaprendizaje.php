<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-11-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatAcad_cursoaprendizaje extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_cursoaprendizaje").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_cursoaprendizaje";
			
			$cond = array();		
			
			if(isset($filtros["idaprendizaje"])) {
					$cond[] = "idaprendizaje = " . $this->oBD->escapar($filtros["idaprendizaje"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_cursoaprendizaje").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_cursoaprendizaje";			
			
			$cond = array();		
					
			
			if(isset($filtros["idaprendizaje"])) {
					$cond[] = "idaprendizaje = " . $this->oBD->escapar($filtros["idaprendizaje"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursoaprendizaje").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcurso,$idcursodetalle,$idpadre,$idrecurso,$descripcion)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_cursoaprendizaje_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idaprendizaje) FROM acad_cursoaprendizaje");
			++$id;
			
			$estados = array('idaprendizaje' => $id
							
							,'idcurso'=>$idcurso
							,'idcursodetalle'=>$idcursodetalle
							,'idpadre'=>$idpadre
							,'idrecurso'=>$idrecurso
							,'descripcion'=>$descripcion							
							);
			
			$this->oBD->insert('acad_cursoaprendizaje', $estados);			
			$this->terminarTransaccion('dat_acad_cursoaprendizaje_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_cursoaprendizaje_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_cursoaprendizaje").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso,$idcursodetalle,$idpadre,$idrecurso,$descripcion)
	{
		try {
			$this->iniciarTransaccion('dat_acad_cursoaprendizaje_update');
			$estados = array('idcurso'=>$idcurso
							,'idcursodetalle'=>$idcursodetalle
							,'idpadre'=>$idpadre
							,'idrecurso'=>$idrecurso
							,'descripcion'=>$descripcion								
							);
			
			$this->oBD->update('acad_cursoaprendizaje ', $estados, array('idaprendizaje' => $id));
		    $this->terminarTransaccion('dat_acad_cursoaprendizaje_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursoaprendizaje").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_cursoaprendizaje  "
					. " WHERE idaprendizaje = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_cursoaprendizaje").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_cursoaprendizaje', array('idaprendizaje' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_cursoaprendizaje").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_cursoaprendizaje', array($propiedad => $valor), array('idaprendizaje' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursoaprendizaje").": " . $e->getMessage());
		}
	}
   
		
}