<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatLinks extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Links").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM links";
			
			$cond = array();		
			
			if(!empty($filtros["idlink"])) {
					$cond[] = "idlink = " . $this->oBD->escapar($filtros["idlink"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["texto"])) {
					$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Links").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM links";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idlink"])) {
					$cond[] = "idlink = " . $this->oBD->escapar($filtros["idlink"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["texto"])) {
					$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Links").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM links  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Links").": " . $e->getMessage());
		}
	}
	
	public function insertar($idnivel,$idunidad,$idactividad,$idpersonal,$texto,$orden)
	{
		try {
			
			$this->iniciarTransaccion('dat_links_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idlink) FROM links");
			++$id;
			
			$estados = array('idlink' => $id
							
							,'idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'idpersonal'=>$idpersonal
							,'texto'=>$texto
							,'orden'=>$orden							
							);
			
			$this->oBD->insert('links', $estados);			
			$this->terminarTransaccion('dat_links_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_links_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Links").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idnivel,$idunidad,$idactividad,$idpersonal,$texto,$orden)
	{
		try {
			$this->iniciarTransaccion('dat_links_update');
			$estados = array('idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'idpersonal'=>$idpersonal
							,'texto'=>$texto
							,'orden'=>$orden								
							);
			
			$this->oBD->update('links ', $estados, array('idlink' => $id));
		    $this->terminarTransaccion('dat_links_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Links").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM links  "
					. " WHERE idlink = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Links").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('links', array('idlink' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Links").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('links', array($propiedad => $valor), array('idlink' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Links").": " . $e->getMessage());
		}
	}
   
		
}