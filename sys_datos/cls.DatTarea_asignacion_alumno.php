<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-06-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatTarea_asignacion_alumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Tarea_asignacion_alumno").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM tarea_asignacion_alumno";
			
			$cond = array();		
			
			if(!empty($filtros["iddetalle"])) {
					$cond[] = "iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if(!empty($filtros["idtarea_asignacion"])) {
					$cond[] = "idtarea_asignacion = " . $this->oBD->escapar($filtros["idtarea_asignacion"]);
			}
			if(!empty($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["mensajedevolucion"])) {
					$cond[] = "mensajedevolucion = " . $this->oBD->escapar($filtros["mensajedevolucion"]);
			}
			if(!empty($filtros["notapromedio"])) {
					$cond[] = "notapromedio = " . $this->oBD->escapar($filtros["notapromedio"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Tarea_asignacion_alumno").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT taa.*, p.nombre, p.ape_paterno, p.ape_materno, p.foto FROM tarea_asignacion_alumno taa JOIN personal p ON p.dni=taa.idalumno";
			
			$cond = array();		
					
			
			if(!empty($filtros["iddetalle"])) {
				$cond[] = "taa.iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if(!empty($filtros["idtarea_asignacion"])) {
				$cond[] = "taa.idtarea_asignacion = " . $this->oBD->escapar($filtros["idtarea_asignacion"]);
			}
			if(!empty($filtros["idalumno"])) {
				$cond[] = "taa.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["mensajedevolucion"])) {
				$cond[] = "taa.mensajedevolucion = " . $this->oBD->escapar($filtros["mensajedevolucion"]);
			}
			if(!empty($filtros["notapromedio"])) {
				$cond[] = "taa.notapromedio = " . $this->oBD->escapar($filtros["notapromedio"]);
			}
			if(!empty($filtros["estado"])) {
				if(is_array($filtros["estado"])){
					$cond_OR = array();
					foreach ($filtros["estado"] as $estado) {
						$cond_OR[] = "taa.estado = " . $this->oBD->escapar($estado);
					}
					$cond[] = '( '. implode(' OR ', $cond_OR) .' )';
				}else{
					$cond[] = "taa.estado = " . $this->oBD->escapar($filtros["tarea_alumno"]["estado"]);
				}
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql.'<br>';
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Tarea_asignacion_alumno").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM tarea_asignacion_alumno  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Tarea_asignacion_alumno").": " . $e->getMessage());
		}
	}
	
	public function insertar($idtarea_asignacion,$idalumno,$mensajedevolucion,$notapromedio,$estado)
	{
		try {
			
			$this->iniciarTransaccion('dat_tarea_asignacion_alumno_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM tarea_asignacion_alumno");
			++$id;
			
			$estados = array('iddetalle' => $id
							,'idtarea_asignacion'=>$idtarea_asignacion
							,'idalumno'=>$idalumno
							,'estado'=>$estado							
							);
			if(!empty($notapromedio) && $notapromedio!=''){
				$estados["notapromedio"] = $notapromedio;
			}
			if(!empty($mensajedevolucion) && $mensajedevolucion!=''){
				$estados["mensajedevolucion"] = $mensajedevolucion;
			}

			$this->oBD->insert('tarea_asignacion_alumno', $estados);			
			$this->terminarTransaccion('dat_tarea_asignacion_alumno_insert');			
			return $id;
		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_tarea_asignacion_alumno_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Tarea_asignacion_alumno").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idtarea_asignacion,$idalumno,$mensajedevolucion,$notapromedio,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_tarea_asignacion_alumno_update');
			$estados = array('idtarea_asignacion'=>$idtarea_asignacion
							,'idalumno'=>$idalumno
							,'estado'=>$estado								
							);
			if(!empty($notapromedio) && $notapromedio!=''){
				$estados["notapromedio"] = $notapromedio;
			}
			if(!empty($mensajedevolucion) && $mensajedevolucion!=''){
				$estados["mensajedevolucion"] = $mensajedevolucion;
			}

			$this->oBD->update('tarea_asignacion_alumno ', $estados, array('iddetalle' => $id));
		    $this->terminarTransaccion('dat_tarea_asignacion_alumno_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea_asignacion_alumno").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM tarea_asignacion_alumno  "
					. " WHERE iddetalle = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Tarea_asignacion_alumno").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('tarea_asignacion_alumno', array('iddetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Tarea_asignacion_alumno").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('tarea_asignacion_alumno', array($propiedad => $valor), array('iddetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea_asignacion_alumno").": " . $e->getMessage());
		}
	}
   
		
}