<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		18-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatGeneral extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("General").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM general";
			
			$cond = array();		
			
			if(!empty($filtros["idgeneral"])) {
					$cond[] = "idgeneral = " . $this->oBD->escapar($filtros["idgeneral"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre  " . $this->oBD->Like($filtros["nombre"]);
			}
			if(!empty($filtros["tipo_tabla"])) {
					$cond[] = "tipo_tabla = " . $this->oBD->escapar($filtros["tipo_tabla"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("General").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM general";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idgeneral"])) {
					$cond[] = "idgeneral = " . $this->oBD->escapar($filtros["idgeneral"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre " . $this->oBD->Like($filtros["nombre"]);
			}
			if(!empty($filtros["tipo_tabla"])) {
					$cond[] = "tipo_tabla = " . $this->oBD->escapar($filtros["tipo_tabla"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("General").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($codigo,$nombre,$tipo_tabla,$mostrar)
	{
		try {
			
			$this->iniciarTransaccion('dat_general_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idgeneral) FROM general");
			++$id;
			
			$estados = array('idgeneral' => $id
							
							,'codigo'=>$codigo
							,'nombre'=>$nombre
							,'tipo_tabla'=>$tipo_tabla
							,'mostrar'=>$mostrar							
							);
			
			$this->oBD->insert('general', $estados);			
			$this->terminarTransaccion('dat_general_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_general_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("General").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $codigo,$nombre,$tipo_tabla,$mostrar)
	{
		try {
			$this->iniciarTransaccion('dat_general_update');
			$estados = array('codigo'=>$codigo
							,'nombre'=>$nombre
							,'tipo_tabla'=>$tipo_tabla
							,'mostrar'=>$mostrar								
							);
			
			$this->oBD->update('general ', $estados, array('idgeneral' => $id));
		    $this->terminarTransaccion('dat_general_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("General").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM general  "
					. " WHERE idgeneral = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("General").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('general', array('idgeneral' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("General").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('general', array($propiedad => $valor), array('idgeneral' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("General").": " . $e->getMessage());
		}
	}
   
		
}