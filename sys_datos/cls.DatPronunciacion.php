<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-10-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatPronunciacion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Pronunciacion").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM pronunciacion";
			
			$cond = array();
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["palabra"])) {
					$cond[] = "palabra = " . $this->oBD->escapar($filtros["palabra"]);
			}
			if(isset($filtros["pron"])) {
					$cond[] = "pron = " . $this->oBD->escapar($filtros["pron"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Pronunciacion").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM pronunciacion";
			$cond = array();
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["palabra"])) {
					$cond[] = "trim(palabra) = " . $this->oBD->escapar($filtros["palabra"]);
			}
			if(isset($filtros["pron"])) {
					$cond[] = "trim(pron) = " . $this->oBD->escapar($filtros["pron"]."\r\n");
			}

			if(isset($filtros["texto"])){
				$cond[] = "pron = " . $this->oBD->escapar($filtros["texto"])." OR palabra = " . $this->oBD->escapar($filtros["texto"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}	
			//echo $sql;					
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Pronunciacion").": " . $e->getMessage());
		}
	}

	public function  buscarpronunciacion($palabra){
		try {
			$sql = "SELECT palabra,pron FROM pronunciacion ";
			$cond = array();			
			$texto='';
			if(!empty($palabra)){
			  foreach($palabra as $pal){			  	 
				  $texto.=" palabra = '".$pal."' or";
			  }
			  $texto='WHERE '.substr($texto,0,-2);
			}
			$sql .= $texto;	
			$result=$this->oBD->consultarSQL($sql);
			$datos=array();
			foreach ($result as $res){
				$datos[$res["palabra"]]=$res["pron"];
			}			
			return $datos;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Pronunciacion").": " . $e->getMessage());
		}
	}
	
	public function insertar($palabra,$pron)
	{
		try {
			
			$this->iniciarTransaccion('dat_pronunciacion_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM pronunciacion");
			++$id;			
			$estados = array('id' => $id							
							,'palabra'=>$palabra
							,'pron'=>$pron							
							);
			
			$this->oBD->insert('pronunciacion', $estados);			
			$this->terminarTransaccion('dat_pronunciacion_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_pronunciacion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Pronunciacion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $palabra,$pron)
	{
		try {
			$this->iniciarTransaccion('dat_pronunciacion_update');
			$estados = array('palabra'=>$palabra
							,'pron'=>$pron								
							);
			
			$this->oBD->update('pronunciacion ', $estados, array('id' => $id));
		    $this->terminarTransaccion('dat_pronunciacion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Pronunciacion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM pronunciacion"
					. " WHERE id = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Pronunciacion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('pronunciacion', array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Pronunciacion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('pronunciacion', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Pronunciacion").": " . $e->getMessage());
		}
	}
}