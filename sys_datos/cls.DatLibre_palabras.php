<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-04-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatLibre_palabras extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Libre_palabras").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM libre_palabras";
			
			$cond = array();		
			
			if(isset($filtros["idpalabra"])) {
				$cond[] = "idpalabra = " . $this->oBD->escapar($filtros["idpalabra"]);
			}
			if(isset($filtros["idtema"])) {
				$cond[] = "idtema = " . $this->oBD->escapar($filtros["idtema"]);
			}
			if(isset($filtros["palabra"])) {
				$cond[] = "palabra = " . $this->oBD->escapar($filtros["palabra"]);
			}
			if(isset($filtros["audio"])) {
				$cond[] = "audio = " . $this->oBD->escapar($filtros["audio"]);
			}
			if(isset($filtros["idagrupacion"])) {
				$cond[] = "idagrupacion = " . $this->oBD->escapar($filtros["idagrupacion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Libre_palabras").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT P.*, T.nombre AS tema_nombre, T.descripcion AS tema_descripcion, TI.idtipo, TI.nombre AS tipo_nombre, TI.tipo_contenido FROM libre_palabras P JOIN libre_tema T ON P.idtema=T.idtema  JOIN libre_tipo TI ON T.idtipo=TI.idtipo";
			
			$cond = array();		
					
			
			if(isset($filtros["idpalabra"])) {
				$cond[] = "P.idpalabra = " . $this->oBD->escapar($filtros["idpalabra"]);
			}
			if(isset($filtros["idtema"])) {
				$cond[] = "P.idtema = " . $this->oBD->escapar($filtros["idtema"]);
			}
			if(isset($filtros["palabra"])) {
				$cond[] = "P.palabra = " . $this->oBD->escapar($filtros["palabra"]);
			}
			if(isset($filtros["audio"])) {
				$cond[] = "P.audio = " . $this->oBD->escapar($filtros["audio"]);
			}
			if(isset($filtros["idagrupacion"])) {
				$cond[] = "P.idagrupacion = " . $this->oBD->escapar($filtros["idagrupacion"]);
			}
			if(isset($filtros["tema_nombre"])) {
				$cond[] = "T.nombre = " . $this->oBD->escapar($filtros["tema_nombre"]);
			}
			if(isset($filtros["tipo_nombre"])) {
				$cond[] = "TI.nombre = " . $this->oBD->escapar($filtros["tipo_nombre"]);
			}
			if(isset($filtros["tipo_contenido"])) {
				$cond[] = "TI.tipo_contenido = " . $this->oBD->escapar($filtros["tipo_contenido"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY P.idpalabra ASC";
			#echo $sql;

			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Libre_palabras").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idtema,$palabra,$audio,$idagrupacion,$url_iframe)
	{
		try {
			
			$this->iniciarTransaccion('dat_libre_palabras_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpalabra) FROM libre_palabras");
			++$id;
			
			$estados = array('idpalabra' => $id
							,'idtema'=>$idtema
							);
			if(!empty($palabra)) {
				$estados['palabra'] = $palabra;
			}
			if(!empty($audio)) {
				$estados['audio'] = $audio;
			}
			if(!empty($idagrupacion)) {
				$estados['idagrupacion'] = $idagrupacion;
			}
			if(!empty($url_iframe)) {
				$estados['url_iframe'] = $url_iframe;
			}
			
			$this->oBD->insert('libre_palabras', $estados);			
			$this->terminarTransaccion('dat_libre_palabras_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_libre_palabras_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Libre_palabras").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idtema,$palabra,$audio,$idagrupacion,$url_iframe)
	{
		try {
			$this->iniciarTransaccion('dat_libre_palabras_update');
			$estados = array('idtema'=>$idtema
							);
			if(!empty($palabra)) {
				$estados['palabra'] = $palabra;
			}
			if(!empty($audio)) {
				$estados['audio'] = $audio;
			}
			if(!empty($idagrupacion)) {
				$estados['idagrupacion'] = $idagrupacion;
			}
			if(!empty($url_iframe)) {
				$estados['url_iframe'] = $url_iframe;
			}
			
			$this->oBD->update('libre_palabras ', $estados, array('idpalabra' => $id));
		    $this->terminarTransaccion('dat_libre_palabras_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Libre_palabras").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT tb1.*,tb2.nombre AS _nombre  FROM libre_palabras tb1 LEFT JOIN libre_tema tb2 ON tb1.idtema=tb2.idtema  "
					. " WHERE tb1.idpalabra = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Libre_palabras").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('libre_palabras', array('idpalabra' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Libre_palabras").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('libre_palabras', array($propiedad => $valor), array('idpalabra' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Libre_palabras").": " . $e->getMessage());
		}
	}
   
		
}