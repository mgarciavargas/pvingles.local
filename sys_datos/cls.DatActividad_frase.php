<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		18-11-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatActividad_frase extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Actividad_frase").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM actividad_frase";
			
			$cond = array();		
			
			if(!empty($filtros["idfrase"])) {
					$cond[] = "idfrase = " . $this->oBD->escapar($filtros["idfrase"]);
			}
			if(!empty($filtros["idactividad_detalle"])) {
					$cond[] = "idactividad_detalle = " . $this->oBD->escapar($filtros["idactividad_detalle"]);
			}
			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Actividad_frase").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM actividad_frase";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idfrase"])) {
					$cond[] = "idfrase = " . $this->oBD->escapar($filtros["idfrase"]);
			}
			if(!empty($filtros["idactividad_detalle"])) {
					$cond[] = "idactividad_detalle = " . $this->oBD->escapar($filtros["idactividad_detalle"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Actividad_frase").": " . $e->getMessage());
		}
	}
		
	public function insertar($idactividad_detalle,$descripcion,$frase)
	{
		try {
			
			$this->iniciarTransaccion('dat_actividad_frase_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idfrase) FROM actividad_frase");
			++$id;
			
			$estados = array('idfrase' => $id							
							,'idactividad_detalle'=>$idactividad_detalle
							,'descripcion'=>$descripcion
							,'frase'=>$frase							
							);
			
			$this->oBD->insert('actividad_frase', $estados);			
			$this->terminarTransaccion('dat_actividad_frase_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_actividad_frase_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Actividad_frase").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idactividad_detalle,$descripcion,$frase)
	{
		try {
			$this->iniciarTransaccion('dat_actividad_frase_update');
			$estados = array('idactividad_detalle'=>$idactividad_detalle
							,'descripcion'=>$descripcion
							,'frase'=>$frase								
							);
			
			$this->oBD->update('actividad_frase ', $estados, array('idfrase' => $id));
		    $this->terminarTransaccion('dat_actividad_frase_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Actividad_frase").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM actividad_frase  "
					. " WHERE idfrase = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Actividad_frase").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('actividad_frase', array('idfrase' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Actividad_frase").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('actividad_frase', array($propiedad => $valor), array('idfrase' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Actividad_frase").": " . $e->getMessage());
		}
	}
   
		
}