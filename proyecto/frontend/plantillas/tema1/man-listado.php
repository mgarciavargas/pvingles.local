<?php defined('_BOOT_') or die('');  $version=!empty(_version_)?_version_:'1.0'; ?>
<link href="<?php echo $documento->getUrlStatic()?>/libs/alert/jquery-confirm.min.css?vs=<?php echo $version;?>" rel="stylesheet" type="text/css" />
<link href="<?php echo $documento->getUrlStatic()?>/libs/datatable1.10/media/css/jquery.dataTables.min.css?vs=<?php echo $version;?>" rel="stylesheet" type="text/css" />
<link href="<?php echo $documento->getUrlStatic()?>/libs/datatable1.10/extensions/Buttons/css/buttons.dataTables.min.css?vs=<?php echo $version;?>" rel="stylesheet" type="text/css" />
<jrdoc:incluir tipo="recurso" />
<script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/libs/alert/jquery-confirm.min.js?vs=<?php echo $version;?>"></script>
<script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/libs/datatable1.10/media/js/jquery.dataTables.min.js?vs=<?php echo $version;?>"></script>