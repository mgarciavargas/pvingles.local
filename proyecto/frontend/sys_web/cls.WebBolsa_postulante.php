<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-01-2018 
 * @copyright	Copyright (C) 27-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBolsa_postulante', RUTA_BASE, 'sys_negocio');
class WebBolsa_postulante extends JrWeb
{
	private $oNegBolsa_postulante;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBolsa_postulante = new NegBolsa_postulante;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_postulante', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idpostular"])&&@$_REQUEST["idpostular"]!='')$filtros["idpostular"]=$_REQUEST["idpostular"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["nombrecompleto"])&&@$_REQUEST["nombrecompleto"]!='')$filtros["nombrecompleto"]=$_REQUEST["nombrecompleto"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["correo"])&&@$_REQUEST["correo"]!='')$filtros["correo"]=$_REQUEST["correo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			if(isset($_REQUEST["idpublicacion"])&&@$_REQUEST["idpublicacion"]!='')$filtros["idpublicacion"]=$_REQUEST["idpublicacion"];
			if(isset($_REQUEST["idpostulante"])&&@$_REQUEST["idpostulante"]!='')$filtros["idpostulante"]=$_REQUEST["idpostulante"];
			
			$this->datos=$this->oNegBolsa_postulante->buscar($filtros);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Postulantes'), true);
			$this->esquema = 'bolsa_postulante-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_postulante', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Postulante').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_postulante', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegBolsa_postulante->idpostular = @$_GET['id'];
			$this->datos = $this->oNegBolsa_postulante->dataBolsa_postulante;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Postulante').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bolsa_postulante-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_postulante', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpostular"])&&@$_REQUEST["idpostular"]!='')$filtros["idpostular"]=$_REQUEST["idpostular"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["nombrecompleto"])&&@$_REQUEST["nombrecompleto"]!='')$filtros["nombrecompleto"]=$_REQUEST["nombrecompleto"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["correo"])&&@$_REQUEST["correo"]!='')$filtros["correo"]=$_REQUEST["correo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			if(isset($_REQUEST["idpublicacion"])&&@$_REQUEST["idpublicacion"]!='')$filtros["idpublicacion"]=$_REQUEST["idpublicacion"];
			if(isset($_REQUEST["idpostulante"])&&@$_REQUEST["idpostulante"]!='')$filtros["idpostulante"]=$_REQUEST["idpostulante"];
						
			$this->datos=$this->oNegBolsa_postulante->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarPostulante(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idpostular)) {
				$this->oNegBolsa_postulante->idpostular = $idpostular;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
       		$this->oNegBolsa_postulante->fecharegistro=!empty($fecharegistro)?$fecharegistro:date('Y-m-d H:i:s');
			$this->oNegBolsa_postulante->nombrecompleto=!empty($nombrecompleto)?$nombrecompleto:'';
			$this->oNegBolsa_postulante->telefono=!empty($telefono)?$telefono:'';
			$this->oNegBolsa_postulante->correo=!empty($correo)?$correo:'';
			$this->oNegBolsa_postulante->descripcion=!empty($descripcion)?$descripcion:'';
			$this->oNegBolsa_postulante->mostrar=!empty($mostrar)?$mostrar:1;
			$this->oNegBolsa_postulante->idpublicacion=$idpublicacion;
			$this->oNegBolsa_postulante->idpostulante=$idpostulante;
					
            if($accion=='_add') {
            	$res=$this->oNegBolsa_postulante->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Postulante')).' '.JrTexto::_(' Gracias por postularse, espere un aviso de la empresa'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegBolsa_postulante->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Postulante')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegBolsa_postulante->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}