<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
class WebInstalar extends JrWeb
{
	private $oNegBolsa_empresas;
	public function __construct()
	{
		parent::__construct();
		$this->oNegBolsa_empresas = new NegBolsa_empresas;
		$this->oNegProyecto = new NegProyecto;
	}

	public function defecto(){
		return $this->crearlogin();		
	}

	public function crearlogin(){
		try{
			global $aplicacion;
			$filtros=array();
			$idempresa=(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')?$_REQUEST["idempresa"]:0;			
			$this->documento->setTitulo(JrTexto::_('Proyectos'),true);
			$this->documento->plantilla ='proyecto/instalador';
			$this->esquema = 'instalador/crearlogin';
			$this->oNegBolsa_empresas->idempresa=$idempresa;
			$this->empresa=$this->oNegBolsa_empresas->getXid();
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function paso3(){
		try{
			global $aplicacion;
			$filtros=array();
			$idempresa=(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')?$_REQUEST["idempresa"]:0;
			$idproyecto=(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')?$_REQUEST["idproyecto"]:0;
			@session_start();
			if(!empty($idempresa))$_SESSION["idempresa"]=$idempresa;
			else $idempresa=@$_SESSION["idempresa"];
			if(!empty($idproyecto))$_SESSION["idproyecto"]=$idproyecto;
			else  $idproyecto=@$_SESSION["idproyecto"];
			$this->curusuario = NegSesion::getUsuario();			
			if($this->curusuario["idrol"]!=-1){
				if($idempresa!=$this->curusuario["idempresa"]){	throw new Exception(JrTexto::_('La empresa con la que esta logueda no es la correcta'));}			
				if($idproyecto!=$this->curusuario["idproyecto"]){throw new Exception(JrTexto::_('Al proyecto que usted accede no tiene permisos'));	}
			}
			$this->oNegProyecto->idproyecto = $idproyecto;
			$this->proyecto = $this->oNegProyecto->dataProyecto;
			$this->documento->setTitulo(JrTexto::_('Proyectos'),true);
			$this->documento->plantilla ='proyecto/administrable';
			$this->esquema = 'instalador/addmodulos';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

}