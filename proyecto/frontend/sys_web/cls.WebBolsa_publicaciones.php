<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		25-01-2018 
 * @copyright	Copyright (C) 25-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBolsa_publicaciones', RUTA_BASE, 'sys_negocio');
class WebBolsa_publicaciones extends JrWeb
{
	private $oNegBolsa_publicaciones;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBolsa_publicaciones = new NegBolsa_publicaciones;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_publicaciones', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idpublicacion"])&&@$_REQUEST["idpublicacion"]!='')$filtros["idpublicacion"]=$_REQUEST["idpublicacion"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["sueldo"])&&@$_REQUEST["sueldo"]!='')$filtros["sueldo"]=$_REQUEST["sueldo"];
			if(isset($_REQUEST["nvacantes"])&&@$_REQUEST["nvacantes"]!='')$filtros["nvacantes"]=$_REQUEST["nvacantes"];
			if(isset($_REQUEST["disponibilidadeviaje"])&&@$_REQUEST["disponibilidadeviaje"]!='')$filtros["disponibilidadeviaje"]=$_REQUEST["disponibilidadeviaje"];
			if(isset($_REQUEST["duracioncontrato"])&&@$_REQUEST["duracioncontrato"]!='')$filtros["duracioncontrato"]=$_REQUEST["duracioncontrato"];
			if(isset($_REQUEST["xtiempo"])&&@$_REQUEST["xtiempo"]!='')$filtros["xtiempo"]=$_REQUEST["xtiempo"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["fechapublicacion"])&&@$_REQUEST["fechapublicacion"]!='')$filtros["fechapublicacion"]=$_REQUEST["fechapublicacion"];
			if(isset($_REQUEST["cambioderesidencia"])&&@$_REQUEST["cambioderesidencia"]!='')$filtros["cambioderesidencia"]=$_REQUEST["cambioderesidencia"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			
			$this->datos=$this->oNegBolsa_publicaciones->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bolsa_publicaciones'), true);
			$this->esquema = 'bolsa_publicaciones-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_publicaciones', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Bolsa_publicaciones').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_publicaciones', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegBolsa_publicaciones->idpublicacion = @$_GET['id'];
			$this->datos = $this->oNegBolsa_publicaciones->dataBolsa_publicaciones;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bolsa_publicaciones').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bolsa_publicaciones-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_publicaciones', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpublicacion"])&&@$_REQUEST["idpublicacion"]!='')$filtros["idpublicacion"]=$_REQUEST["idpublicacion"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["sueldo"])&&@$_REQUEST["sueldo"]!='')$filtros["sueldo"]=$_REQUEST["sueldo"];
			if(isset($_REQUEST["nvacantes"])&&@$_REQUEST["nvacantes"]!='')$filtros["nvacantes"]=$_REQUEST["nvacantes"];
			if(isset($_REQUEST["disponibilidadeviaje"])&&@$_REQUEST["disponibilidadeviaje"]!='')$filtros["disponibilidadeviaje"]=$_REQUEST["disponibilidadeviaje"];
			if(isset($_REQUEST["duracioncontrato"])&&@$_REQUEST["duracioncontrato"]!='')$filtros["duracioncontrato"]=$_REQUEST["duracioncontrato"];
			if(isset($_REQUEST["xtiempo"])&&@$_REQUEST["xtiempo"]!='')$filtros["xtiempo"]=$_REQUEST["xtiempo"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["fechapublicacion"])&&@$_REQUEST["fechapublicacion"]!='')$filtros["fechapublicacion"]=$_REQUEST["fechapublicacion"];
			if(isset($_REQUEST["cambioderesidencia"])&&@$_REQUEST["cambioderesidencia"]!='')$filtros["cambioderesidencia"]=$_REQUEST["cambioderesidencia"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
						
			$this->datos=$this->oNegBolsa_publicaciones->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarPublicaciones(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Datos incompletos')));
                exit(0);            
            }
            @extract($_POST);
            if(empty($idempresa)){
            	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Empresa no reconocida')));
                exit(0);
            }
            $accion='_add';            
            if(!empty($idpublicacion)) {
				$this->oNegBolsa_publicaciones->idpublicacion = $idpublicacion;
				$accion='_edit';
			}
           	//$usuarioAct = NegSesion::getUsuario();           	
       		$this->oNegBolsa_publicaciones->idempresa=@$idempresa;
			$this->oNegBolsa_publicaciones->titulo=@$titulo;
			$this->oNegBolsa_publicaciones->descripcion=@$descripcion;
			$this->oNegBolsa_publicaciones->sueldo=@$sueldo;
			$this->oNegBolsa_publicaciones->nvacantes=!empty($nvacantes)?$nvacantes:1;
			$this->oNegBolsa_publicaciones->disponibilidadeviaje=!empty($disponibilidadeviaje)?1:0;
			$this->oNegBolsa_publicaciones->duracioncontrato=@$duracioncontrato;
			$this->oNegBolsa_publicaciones->xtiempo=@$xtiempo;			
			$this->oNegBolsa_publicaciones->fechapublicacion=!empty($fechapublicacion)?$fechapublicacion:date('Y-m-d H:i:s');
			$this->oNegBolsa_publicaciones->cambioderesidencia=!empty($cambioderesidencia)?1:0;
			$this->oNegBolsa_publicaciones->mostrar=!empty($mostrar)?1:0;
					
            if($accion=='_add') {
            	$this->oNegBolsa_publicaciones->fecharegistro=!empty($fecharegistro)?$fecharegistro:date('Y-m-d');
            	$res=$this->oNegBolsa_publicaciones->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Publicación')).' '.JrTexto::_('save successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegBolsa_publicaciones->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Publicación')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegBolsa_publicaciones->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}