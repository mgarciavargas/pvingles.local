<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->datos)) $frm=$this->datos;
$file='<img src="'.$this->documento->getUrlStatic().'/media/nofoto.jpg" class="img-responsive center-block">';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Acad_grupoauladetalle"); ?></li>
  </ol>
</nav>
<?php } ?><div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col">
    <div id="msj-interno"></div>
    <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
    <input type="hidden" name="Idgrupoauladetalle" id="idgrupoauladetalle<?php echo $idgui; ?>" value="<?php echo $this->pk;?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo JrTexto::_($this->frmaccion);?>">
    <div class="row">
          <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Idgrupoaula');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="idgrupoaula<?php echo $idgui; ?>" name="idgrupoaula" required="required" class="form-control" value="<?php echo @$frm["idgrupoaula"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Idcurso');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="idcurso<?php echo $idgui; ?>" name="idcurso" required="required" class="form-control" value="<?php echo @$frm["idcurso"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Iddocente');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="iddocente<?php echo $idgui; ?>" name="iddocente" required="required" class="form-control" value="<?php echo @$frm["iddocente"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Idlocal');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="idlocal<?php echo $idgui; ?>" name="idlocal" required="required" class="form-control" value="<?php echo @$frm["idlocal"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Idambiente');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="idambiente<?php echo $idgui; ?>" name="idambiente" required="required" class="form-control" value="<?php echo @$frm["idambiente"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Nombre');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="nombre<?php echo $idgui; ?>" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Fecha inicio');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="fecha_inicio<?php echo $idgui; ?>" name="fecha_inicio" required="required" class="form-control" value="<?php echo @$frm["fecha_inicio"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Fecha final');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="fecha_final<?php echo $idgui; ?>" name="fecha_final" required="required" class="form-control" value="<?php echo @$frm["fecha_final"];?>">
                          </div>
            
    </div>
        <div class="row"> 
            <div class="col-12 form-group text-center">
              <hr>
              <button id="btn-saveAcad_grupoauladetalle" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Save'));?> </button>
              <button type="button" class="btn btn-warning btn-close" data-dismiss="modal" href="<?php echo JrAplicacion::getJrUrl(array('acad_grupoauladetalle'))?>"> <i class=" fa fa-repeat"></i> <?php echo ucfirst(JrTexto::_('Cancel'));?></button>              
            </div>
        </div>
        </form>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(ev){
      ev.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'acad_grupoauladetalle', 'saveAcad_grupoauladetalle', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
       <?php if(!empty($fcall)){ ?> if(typeof <?php echo $fcall?> == 'function'){
          <?php echo $fcall?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else <?php } ?>  return redir('<?php echo JrAplicacion::getJrUrl(array("Acad_grupoauladetalle"))?>');
      }
     }
  }); 
  
});

</script>