<?php 
$idgui=uniqid(); 
$imgcursodefecto='/static/media/nofoto.jpg';
?>
<style>
tr.trclone{
    display:none;
}
.breadcrumb{
    padding:1ex;
}
.breadcrumb>li{
    text-transform: capitalize;
    color:#fff;
    padding-left:1ex;
}
.breadcrumb>li+li:before {
    color: #e4dddd;
    content: "\f101";
    font: normal normal normal 16px/1 FontAwesome;
}
.breadcrumb>li.active {
    color: #383434;
}
.thead{ color:#fff}
input , .input-group-append{
    margin-top:1ex;
}
label ~ input , label ~ .input-group-append{
    margin-top:0ex;
}
label{ margin-bottom:0px; margin-top:1ex;}
</style>
<div class="container-fluid">
    <!--div class="row" id="breadcrumb" style="padding:1ex;">
        <div class="col-12">
            <ol class="breadcrumb bg-primary">
                <li><a style="color:#fff" href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>&nbsp;<?php echo JrTexto::_("Academico"); ?></a></li> 
                <li class="active" style="color:"> <i class="fa fa-building"></i>&nbsp;<?php echo JrTexto::_("locales"); ?></li>       
            </ol>
        </div>
    </div-->
    <div class="row" id="pnlfiltros">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <form method="post" id="frmambiente"  target="" enctype="" class="form-horizontal form-label-left">
                    <input type="hidden"  id="turno" name="turno" value="T">
                    <div class="row">                        
                        <div class="col-md-4 col-sm-6 col-xs-12">        
                            <div class="cajaselect">
                                <select id="idlocal" name="idlocal" class="form-control" >
                                    <option value=""><?php echo JrTexto::_('Seleccione Local'); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="cajaselect">
                                <select id="tipo" name="tipo" class="form-control" >
                                    <option value=""><?php echo JrTexto::_('Seleccione Tipo'); ?></option>
                                    <option value="A"><?php echo JrTexto::_('Aula'); ?></option>
                                    <option value="L"><?php echo JrTexto::_('Laboratorio'); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="cajaselect">
                                <select id="estado" name="estado" class="form-control" >
                                    <option value=""><?php echo JrTexto::_('Seleccione estado'); ?></option>
                                    <option value="1"><?php echo JrTexto::_('Activo'); ?></option>
                                    <option value="0"><?php echo JrTexto::_('Inactivo'); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 frmeditnover">
                            <div class="input-group">
                                <input type="text" name="texto" id="texto" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Texto a buscar"))?>">
                                <div class="input-group-append"><button class="btn btnbuscar btn-primary"><?php echo  ucfirst(JrTexto::_("buscar"))?> <i class="fa fa-search"></i></button></div>
                            </div>
                        </div>
                        <div class="col-md-12"></div>
                        <input type="hidden" name="idambiente" id="idambiente" value="0">
                        <div class="col-12 col-sm-6 col-md-4 form-group frmeditsiver">
                            <label><?php echo JrTexto::_('Número');?> <span class="required"> (*) </span></label>             
                            <input type="number"  id="numero" name="numero" required="required" class="form-control" placeholder="Numero">
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 form-group frmeditsiver">
                            <label><?php echo JrTexto::_('Capacidad');?> <span class="required"> (*) </span></label>              
                            <input type="number"  id="capacidad" name="capacidad" required="required" class="form-control" placeholder="Capacidad">
                        </div>
                        <div class="col-12 form-group text-center frmeditsiver">
                        <hr>
                        <button id="btn-saveambiente" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Guardar'));?> </button>
                        <button type="button" class="btn btn-warning btn-cancel"> <i class=" fa fa-repeat"></i> <?php echo ucfirst(JrTexto::_('Cancelar'));?></button>              
                        </div>                        
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="pnlambiente">        
        <div class="col-md-12">           
            <div class="card shadow">
                <div class="card-body">	
                <table class="table table-striped table-hover">
                    <thead>
                        <tr class="bg-primary">
                            <th scope="col">#</th>
                            <th scope="col"><?php echo JrTexto::_("Tipo") ;?></th>
                            <th scope="col"><?php echo JrTexto::_("Número") ;?></th>
                            <th scope="col"><?php echo JrTexto::_("Capacidad") ;?></th>
                            <th scope="col"><?php echo JrTexto::_("Estado") ;?></th>                  
                            <th scope="col" class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Acciones');?><span class="btnverfrmambiente btn btn-warning pull-right btn-sm"><i class="fa fa-plus"></i> Agregar</span></span></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>                           
                </div>
            </div>
        </div>
    </div>
    
</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
var imgdefecto='<?php echo $imgcursodefecto; ?>';
var idproyecto=parseInt('<?php echo $this->idproyecto; ?>');
$(document).ready(function(){
    $('.frmeditsiver').hide();
    $('table').on('click','.btnverfrmambiente',function(ev){
        $('#pnlfrmambiente').show();
        $('.frmeditnover').hide();
        $('#pnlambiente').fadeOut('fast');
        $('.frmeditsiver').show('fast');
    }).on('click','.btneditar',function(ev){
        let idambiente=parseInt($(this).attr('data-id')||0);
        if(idambiente>0)
        __sysajax({   
            fromdata:{idambiente:idambiente},
            url:_sysUrlBase_+'/ambiente/buscarjson', 
            showmsjok:false,          
            callback:function(rs){
                if(rs.code=='ok'){
                    let dt=rs.data[0];                 
                    $('#idambiente').val(dt.idambiente);
                    $('#accion').val('edit');
                    $('#idlocal').val(dt.idlocal);
                    $('#tipo').val(dt.tipo);
                    $('#estado').val(dt.estado);
                    $('#numero').val(dt.numero);
                    $('#capacidad').val(dt.capacidad);
                    $('#turno').val(dt.turno);
                    $('#pnlfrmambiente').show();
                    $('.frmeditnover').hide();
                    $('#pnlambiente').fadeOut('fast');
                    $('.frmeditsiver').show('fast');
                }
            }
        })       
    }).on('click','.btn-eliminar',function(ev){
        let idambiente=parseInt($(this).attr('data-id')||0);
        $.confirm({
            title: '<?php echo JrTexto::_('Confirmar acción');?>',
            content: '<?php echo JrTexto::_('Esta seguro que desea eliminar este registro ?'); ?>',
            confirmButton: '<?php echo JrTexto::_('Aceptar');?>',
            cancelButton: '<?php echo JrTexto::_('Cancelar');?>',
            confirmButtonClass: 'btn-success',
            cancelButtonClass: 'btn-danger',
            closeIcon: true,
            confirm: function(){             
                var res = xajax__('', 'ambiente', 'eliminar', idambiente);
                if(res) tabledatos.ajax.reload();
            }
        });      
    });
    $('#pnlfiltros').on('click','.btnbuscar',function(){
        tabledatos.ajax.reload();
    }).on('keyup','#idtexto',function(ev){
        var code = ev.which;
        if(code==13){
            ev.preventDefault();
            tabledatos.ajax.reload();
        }
    })

    $('#frmambiente').on('submit',function(ev){ 
        ev.preventDefault();
        var data=__formdata('frmambiente');
        let idlocal=$('select#idlocal').val()||'';
        if(idlocal==''){
             mostrar_notificacion('Atencion','Seleccione un local','danger');
             $('select#idlocal').focus();
             return;
        }
        let tipo=$('select#tipo').val()||'';
        if(tipo==''){
             mostrar_notificacion('Atencion','Seleccione un tipo','danger');
             $('select#tipo').focus();
             return;
        }
        let estado=$('select#estado').val()||'';
        if(estado==''){
             mostrar_notificacion('Atencion','Seleccione un estado','danger');
             $('select#estado').focus();
             return;
        }
        $('#btn-saveambiente').attr('disabled', true).addClass('disabled');
        __sysajax({   
            fromdata:data,
            url:_sysUrlBase_+'/ambiente/guardarambiente',           
            callback:function(rs){
                $('#btn-saveambiente').removeAttr('disabled', true).removeClass('disabled');
                tabledatos.ajax.reload();
                $('#pnlfrmambiente').fadeOut();
                $('.frmeditnover').show();
                $('#pnlambiente').fadeIn('fast');
                $('.frmeditsiver').hide('fast');
            }
        })
    }).on('click','.btn-cancel',function(ev){
        $('#pnlfrmambiente').fadeOut();
        $('.frmeditnover').show();
        $('#pnlambiente').fadeIn('fast');
        $('.frmeditsiver').hide('fast');
    }); 
    var estados5b84d7058ab71={'1':'<?php echo JrTexto::_("Activo") ?>','0':'<?php echo JrTexto::_("Inactivo") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  
    __sysajax({
        fromdata:{idproyecto:idproyecto},
        showmsjok:false,
        url:_sysUrlBase_+'/local/buscarjson/?json=true',					
        callback:function(rs){          
            let dt=rs.data;
            $('select#idlocal').find('option').remove();
            $.each(dt,function(i,ds){
                $('select#idlocal').append('<option value="'+ds.idlocal+'">'+ds.nombre+'</option>');
            })
            tabledatos.ajax.reload();
        }
    });
    var tabledatos=$('table.table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Tipo") ;?>'},
        {'data': '<?php echo JrTexto::_("Número") ;?>'},
        {'data': '<?php echo JrTexto::_("Capacidad") ;?>'},
        {'data': '<?php echo JrTexto::_("Estado") ;?>'},
        {'data': '<?php echo JrTexto::_("Acciones") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/ambiente/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.idlocal=$('select#idlocal').val(),
            d.tipo=$('select#tipo').val(),
            d.estado=$('select#estado').val(),
            d.texto=$('select#texto').val(),                     
            draw=d.draw;
        },
        "dataSrc":function(json){           
            var data=json.data;          
            json.draw = draw;
            json.recordsTotal = json.data.length||0;
            json.recordsFiltered = json.data.length||0;
            var datainfo = new Array();
            for(var i=0;i< data.length; i++){                   
                datainfo.push({
                    '#':(i+1),
                    '<?php echo JrTexto::_("Tipo") ;?>': data[i].tipo=='A'?'Aula':'Laboratorio',
                    '<?php echo JrTexto::_("Número") ;?>': data[i].numero,
                    '<?php echo JrTexto::_("Capacidad") ;?>': data[i].capacidad,
                    '<?php echo JrTexto::_("Estado") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="'+data[i].idambiente+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5b84d7058ab71[data[i].estado]+'</a>',
                    '<?php echo JrTexto::_("Acciones") ;?>'  :'<a class="btn btn-xs btneditar" data-id="'+data[i].idambiente+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idambiente+'" ><i class="fa fa-trash-o"></i></a>'
                });
            }
            return datainfo;                
        }, error: function(d){console.log(d)}
      },
      language:{url:_sysUrlStatic_+'/libs/datatable1.10/idiomas/ES.json'}
      <?php //echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

   
})
</script>