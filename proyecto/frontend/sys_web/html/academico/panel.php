<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
//$usuarioAct = NegSesion::getUsuario();
//$grupo=!empty($this->datos)?$this->datos:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
	.slick-items{
		position: relative;
	}
	.slick-item .image{
		overflow: hidden;
		width: 100%		
	}
	.slick-item{
		text-align: center !important;	
	}
	.slick-slide img ,.slick-item img{
		display: inline-block;
		text-align: center;
	}
	.slick-item .titulo{
		font-size: 1.6em;
		text-align: center;
	}	
</style>
<div class="row">
	<div class="col-md-12">
		<div class="card" >
			<div class="card-header bg-primary" style="color:#fff; padding:0.8ex 1.5em;">
				<h3 class="panel-title"><?php echo JrTexto::_('Mantenimiento'); ?></h3>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<div class="slick-items">		                                 
							<!--div class="slick-item"> 
								<a href="<?php echo $this->documento->getUrlBase();?>/cargos" class="hvr-float">
									<img class="img-responsive" src="<?php echo URL_MEDIA;?>/static/media/academico/cargos.png">
									<div class="titulo"><?php echo JrTexto::_('Cargos'); ?></div>
								</a>
							</div-->
							<div class="slick-item"> 
								<a href="<?php echo $this->documento->getUrlBase();?>/academico/personal/?page=alumnos" class="hvr-float">
									<img class="img-responsive" src="<?php echo URL_MEDIA;?>/static/media/academico/estudiantes.png">
									<div class="titulo"><?php echo ucfirst(JrTexto::_('Estudiantes')); ?></div>
								</a>
							</div>
							<div class="slick-item"> 
								<a href="<?php echo $this->documento->getUrlBase();?>/academico/personal/page=docentes" class="hvr-float">
									<img class="img-responsive" src="<?php echo URL_MEDIA;?>/static/media/academico/personal.png">
									<div class="titulo"><?php echo JrTexto::_('Personal'); ?></div>
								</a>
							</div>
							<div class="slick-item"> 
								<a href="<?php echo $this->documento->getUrlBase();?>/cursos" class="hvr-float">
									<img class="img-responsive" src="<?php echo URL_MEDIA;?>/static/media/academico/cursos.png">
									<div class="titulo"><?php echo JrTexto::_('cursos'); ?></div>
								</a>
							</div>
							<div class="slick-item"> 
								<a href="<?php echo $this->documento->getUrlBase();?>/academico/ambientes" class="hvr-float">
									<img class="img-responsive" src="<?php echo URL_MEDIA;?>/static/media/academico/infraestructura.png">
									<div class="titulo"><?php echo JrTexto::_('Infraestructura'); ?></div>
								</a>
							</div>
							<!--div class="slick-item"> 
								<a href="<?php echo $this->documento->getUrlBase();?>/logro/listado" class="hvr-float">
									<img class="img-responsive" src="<?php echo URL_MEDIA;?>/static/media/academico/certificados.png">
									<div class="titulo"><?php echo JrTexto::_('Certificates'); ?></div>
								</a>
							</div-->		                   
							<!--div class="slick-item" style="text-align: center !important;"> 
								<a href="<?php echo $this->documento->getUrlBase();?>/tramitedocumentario" class="hvr-float" style="text-align: center !important;">
									<img src="<?php echo URL_MEDIA;?>/static/media/academico/tramitedocumentario.png">
									<div class="titulo"><?php echo JrTexto::_('Documentary procedure'); ?></div>
								</a>
							</div-->
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<div class="col-md-12">
		<div class="card" >
			<div class="card-header bg-primary" style="color:#fff; padding:0.8ex 1.5em;">
				<h3 class="panel-title"><?php echo JrTexto::_('Mantenimiento'); ?></h3>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<div class="slick-items">		                                 
							<div class="slick-item"> 
								<a href="<?php echo $this->documento->getUrlBase();?>/academico/gruposestudio/" class="hvr-float">
									<img class="img-responsive" src="<?php echo URL_MEDIA;?>/static/media/academico/grupoestudio.png">
									<div class="titulo"><?php echo JrTexto::_('Grupo de estudios'); ?></div>
								</a>
							</div>
							<div class="slick-item"> 
								<a href="<?php echo $this->documento->getUrlBase();?>/academico/matriculas" class="hvr-float">
									<img class="img-responsive" src="<?php echo URL_MEDIA;?>/static/media/academico/matriculas.png">
									<div class="titulo"><?php echo JrTexto::_('Matriculas'); ?></div>
								</a>
							</div>
							<!--div class="slick-item"> 
								<a href="<?php echo $this->documento->getUrlBase();?>/marcacion" class="hvr-float">
									<img class="img-responsive " src="<?php echo URL_MEDIA;?>/static/media/academico/marcacion.png">
									<div class="titulo"><?php echo JrTexto::_('Marcación'); ?></div>
								</a>
							</div>
							<div class="slick-item "> 
								<a href="<?php echo $this->documento->getUrlBase();?>/asistencias" class="hvr-float">
									<img class="img-responsive" src="<?php echo URL_MEDIA;?>/static/media/academico/asistencia.png">
									<div class="titulo"><?php echo JrTexto::_('Assistance'); ?></div>
								</a>
							</div-->
							<!--div class="slick-item disabled"> 
								<a href="<?php echo $this->documento->getUrlBase();?>/notas" class="hvr-float">
									<img class="img-responsive" src="<?php echo URL_MEDIA;?>/static/media/academico/notas.png">
									<div class="titulo"><?php echo JrTexto::_('Notes'); ?></div>
								</a>
							</div-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(e){		
 	var slikitems=$('.slick-items').slick(optionslike); 	
    $('header').removeClass('static');
});
</script>