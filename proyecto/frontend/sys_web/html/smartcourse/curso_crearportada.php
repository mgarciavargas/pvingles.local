<?php 
$idgui=uniqid(); 
$imgcursodefecto='/static/media/nofoto.jpg';
?>
<style>

</style>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 1.5ex; margin-top: 1ex;">
            <div class="progress">
                <div class="progresscurso progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-value="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">50% completado</div>
                <div class="progress-bar bg-warning" role="progressbar" aria-value="100" aria-valuemin="0" aria-valuemax="100" style="width: 50%"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="wizard">
                <ul class="nav nav-wizard" id="ultabs">
					<li class="active"><a href="#" data-value="10" data-show="paso1"><b class="number">1.</b> Datos del curso </a></li>
                    <li class="active" ><a href="#" data-value="20" data-show="paso2"><b class="number">2.</b> Categorias </a></li>
                    <li class="active" ><a href="#" data-value="30" data-show="paso3"><b class="number">3.</b> Estructura </a></li>
                    <li class="active" ><a href="#" data-value="50" data-show="paso4"><b class="number">4.</b> Sesiones </a></li>
                    <li class="active selected" ><a href="#" data-value="60" data-show="portada"><b class="number">5.</b> Portada </a></li>
                    <li class="" ><a href="#" data-value="70" data-show="indice"><b class="number">6.</b> Indice </a></li>
                    <li class="" ><a href="#" data-value="80" data-show="contenido"><b class="number">7.</b> Contenidos </a></li>
                    <li class="" ><a href="#" data-value="100" data-show="publicar"><b class="number">8.</b> Publicar </a></li>
                </ul>
            </div>
            <div class="tab-content shadow" id="panelesultabs">
                <div class="tab-pane active" role="tabpanel" id="pasoportada">
					<input type="hidden" name="idcurso" id="idcurso" value="<?php echo $this->idcurso;?>">				
					<div class="card" id="cardestruct">
						<div class="card-body">
							<div class="row btnmanagermenu">
								<div class="col-md-6 col-sm-12">
									<div class="row">
										<div class="col-md-12 text-center">
											<h3>Información de Portada</h3>
										</div>
										<div class="col-md-12 form-group">
											<label style=""><?php echo JrTexto::_('Titulo');?></label> 
											<input type="text" autofocus name="titulo" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Titulo de portada"))?>" >					           
										</div>                                           
										<div class="col-md-12 form-group">
											<label style=""><?php echo JrTexto::_('Resumen');?></label> 
											<textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("descripción corta para la portada"))?>"></textarea>
										</div>
										<div class="col-md-12 text-center form-group">												
											<label><?php echo JrTexto::_("Imagen") ?> </label>                
											<div style="position: relative;" class="frmchangeimage text-center" id="contenedorimagen">
												<input type="hidden" value="" name="imagen" id="imagen">
												<div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
												<div>
													<img src="<?php echo URL_MEDIA.$imgcursodefecto;?>" class="__subirfile img-fluid center-block" data-type="imagen"  id="imagen" style="min-height:100px; max-width: 200px; max-height: 150px;">
												</div>
											</div>													
										</div>									
									</div>
								</div>
								<div class="col-md-6 col-sm-12">	
									<div class="row">
										<div class="col-md-12 text-center"><h3>Estilo de portada</h3></div>										
										<div class="col-md-12 text-center form-group">												
											<label><?php echo JrTexto::_("Imagen de fondo") ?> </label>                
											<div style="position: relative;" class="frmchangeimage text-center" id="contenedorimagen">
												<input type="hidden" value="" name="imagenfondo" id="imagenfondo">
												<div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
												<div id="sysfilelogo">
													<img src="<?php echo URL_MEDIA.$imgcursodefecto;?>" class="__subirfile img-fluid center-block" data-type="imagen"  id="imagenfondo" style="min-height:100px; max-width: 200px; max-height: 150px;">
												</div>
											</div>													
										</div>
										<div class="col-md-12 text-center">
											<label><?php echo JrTexto::_('Color de fondo');?> </label> <br>
                                            <input type="" name="color" id="colorfondo" value="rgb(0,0,0,0)" class="vercolor form-control" >
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="card-footer text-center"><br><hr>
									    <button type="button" onclick="history.back();" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
										<button type="button" class="btnnexttab btn btn-primary"><i class="fa fa-arrow-right"></i> Continuar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
var imgdefecto='<?php echo $imgcursodefecto; ?>';
var idproyecto=parseInt('<?php echo $this->idproyecto; ?>');
var jsoncurso={
    plantilla:{id:0,nombre:'blanco'},
    estructura:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    estilopagina:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    infoportada:{titulo:'',descripcion:'',image:'',colorfondo:'rgba(0,0,0,0)'},
    infoindice:'top',
    infoavance:50
}
$(document).ready(function(){
    var idcurso=$('#idcurso').val();
	$('ul#ultabs').on('click','a',function(ev){
		ev.preventDefault();
		if(idcurso>0){
			let sel=$('ul#ultabs').find('li.selected').children('a');
			let lia=$(this);
			let liatxt=lia.attr('data-show');
			let liav=parseInt(lia.attr('data-value')||0);
			let avtxt=sel.attr('data-show');			
			let av=parseInt(sel.attr('data-value')||0);
			if(jsoncurso.infoavance >= liav && liatxt!=avtxt){
				window.location.href=_sysUrlBase_+'/cursos/crear/?pg='+liatxt+'&idproyecto='+idproyecto+'&idcurso='+idcurso;
			}
		}
	})
	$('.__subirfile').on('click',function(){
        let $img=$(this);            
        __subirfile({file:$img,typefile:'imagen',uploadtmp:true,guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},function(rs){                    
            let f=rs.media.replace(url_media, '');
            $('input#'+$img.attr('id')).val(f);
        });
    });


	$('#cardestruct').on('click','.btnnexttab',function(ev){
		updateavance(75);
		let jsonportada={
			titulo:$('input[name="titulo"]').val(),
			descripcion:$('textarea[name="descripcion"]').val(),
			imagen:$('input#imagen').val(),
			colorfondo:$('input#colorfondo').val(),
			imagenfondo:$('input#imagenfondo').val(),
			tipotexto:''
		}
		jsoncurso.infoportada=jsonportada;
		let txtjson=JSON.stringify(jsoncurso);		
		__sysajax({
			fromdata:{id:idcurso,campo:'txtjson',valor:txtjson},
			url:url_media+'/smartcourse/acad_curso/setCampojson',  
			showmsjok:false,
			callback:function(rs){
				window.location.href=_sysUrlBase_+'/cursos/crear/?pg=indice&idproyecto='+idproyecto+'&idcurso='+idcurso;
			} 
		})
	})
	let updatetabs=false;
	let updateavance=function(newval){
        let vactual=$('.progresscurso').first().attr('aria-value');
        vactual=parseFloat(vactual);
        newval=parseFloat(newval);
        if(vactual<newval){
            $('.progresscurso').first().attr('aria-value',newval);
            $('.progresscurso').css({width:newval+'%'}).text('Curso '+newval+'% completado');
            $('.progresscurso').siblings().css({width:(100 - newval)+'%'});
			jsoncurso.infoavance=newval;                       
        }		
        if(updatetabs==false){
            updatetabs=true;
            $.each($('ul#ultabs').find('a'),function(i,v){
                let va=parseInt($(v).attr('data-value')||0);
                if(va<=newval) $(v).closest('li').addClass('active');
            })
        }
    }

	var cambiarcolor=function(colortmp){
        let colortmp2=colortmp||'rgba(76, 80, 84, 0.75)';
        $('input[name="color"]').val(colortmp2);
        $('input[name="color"]').minicolors({opacity: true,format:'rgb'});
        $('input[name="color"]').minicolors('settings',{value:colortmp2});
    }

	if(parseInt(idcurso)>0){
        __sysajax({
            fromdata:{idcurso:parseInt(idcurso)},
            showmsjok:false,
            url:url_media+'/smartcourse/acad_curso/buscarjson',					
            callback:function(rs){
               let dt=rs.data;
                if(dt.length>0){
                    let info=rs.data[0];                    
                    let jsontmp=JSON.parse(info.txtjson.trim()==''?'{}':info.txtjson);                   
                    $.extend(jsoncurso,jsontmp);
					updateavance(jsoncurso.infoavance);
					//console.log(jsoncurso.infoportada);
					$('input[name="titulo"]').val(jsoncurso.infoportada.titulo);
					$('textarea[name="descripcion"]').val(jsoncurso.infoportada.descripcion);
					$('input#imagen').val(jsoncurso.infoportada.imagen);
					let imagen=jsoncurso.infoportada.imagen||imgdefecto;
					$('img#imagen').attr('src',url_media+imagen);
					
					let imagenfondo=jsoncurso.infoportada.imagenfondo||imgdefecto;
					$('input#imagenfondo').val(jsoncurso.infoportada.imagenfondo);
					$('img#imagenfondo').attr('src',url_media+imagenfondo);
					cambiarcolor(jsoncurso.infoportada.colorfondo);
                } 
            }
        });
    }
})
</script>