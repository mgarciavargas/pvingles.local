<?php 
$idgui=uniqid(); 
$imgcursodefecto='/static/media/nofoto.jpg';
?>
<style>
tr.trclone{
    display:none;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 1.5ex; margin-top: 1ex;">
            <div class="progress">
                <div class="progresscurso progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-value="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%">25% completado</div>
                <div class="progress-bar bg-warning" role="progressbar" aria-value="100" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="wizard">
                <ul class="nav nav-wizard" id="ultabs">
                <li class="active "><a href="#" data-value="10" data-show="paso1"><b class="number">1.</b> Datos del curso </a></li>
                    <li class="active" ><a href="#" data-value="20" data-show="paso2"><b class="number">2.</b> Categorias </a></li>
                    <li class="active" ><a href="#" data-value="30" data-show="paso3"><b class="number">3.</b> Estructura </a></li>
                    <li class="active" ><a href="#" data-value="50" data-show="paso4"><b class="number">4.</b> Sesiones </a></li>
                    <li class="active" ><a href="#" data-value="60" data-show="portada"><b class="number">5.</b> Portada </a></li>
                    <li class="active" ><a href="#" data-value="70" data-show="indice"><b class="number">6.</b> Indice </a></li>
                    <li class="active" ><a href="#" data-value="80" data-show="contenido"><b class="number">7.</b> Contenidos </a></li>
                    <li class="active selected" ><a href="#" data-value="100" data-show="publicar"><b class="number">8.</b> Publicar </a></li>
                </ul>
            </div>
            <div class="tab-content shadow" id="panelesultabs">
                <div class="tab-pane active" role="tabpanel" id="pasocate">
                <input type="hidden" name="idcurso" id="idcurso" value="<?php echo $this->idcurso;?>">
                    <div class="card shadow">
                        <div class="card-body">	
                            <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                    <a href="javascript:void(0)" onclick="history.back();" class="btn btn-block bg-warning" style="color:#fff">
                                        <div><i class="fa fa-arrow-left fa-2x "></i></div>												
                                        <p class="bolder">Regresar</p>
                                    </a>
                                </div>                              
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <a href="javascript:void(0)" class="btnvistaprevia btn btn-block bg-primary" style="color:#fff">
                                        <div><i class="fa fa-eye fa-2x "></i></div>												
                                        <p class="bolder">Vista previa</p>
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <a href="javascript:void(0)" class="btnpublicar btn btn-block bg-success" style="color:#fff">
                                        <div><i class="fa fa-cloud-upload fa-2x "></i></div>
                                        <p class="bolder">Publicar</p>
                                    </a>
                                </div>                                  
                            </div>                           
                        </div>
                    </div>
                </div>              
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
var imgdefecto='<?php echo $imgcursodefecto; ?>';
var idproyecto=parseInt('<?php echo $this->idproyecto; ?>');
var jsoncurso={
    plantilla:{id:0,nombre:'blanco'},
    estructura:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    estilopagina:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    infoportada:{titulo:'',descripcion:'',image:''},
    infoindice:'top',
    infoavance:10
}

$(document).ready(function(){
    var idcurso=$('#idcurso').val();
    let tmpcategorias={};
    $('ul#ultabs').on('click','a',function(ev){
		ev.preventDefault();
		if(idcurso>0){
			let sel=$('ul#ultabs').find('li.selected').children('a');
			let lia=$(this);
			let liatxt=lia.attr('data-show');
			let liav=parseInt(lia.attr('data-value')||0);
			let avtxt=sel.attr('data-show');			
			let av=parseInt(sel.attr('data-value')||0);
			if(jsoncurso.infoavance >= liav && liatxt!=avtxt){
				window.location.href=_sysUrlBase_+'/cursos/crear/?pg='+liatxt+'&idproyecto='+idproyecto+'&idcurso='+idcurso;
			}
		}
	})
    $('.btnvistaprevia').on('click',function(ev){
        window.open(url_media+'/smartcourse/cursos/ver/?idcurso='+idcurso, '_blank');
    })
    $('.btnpublicar').on('click',function(ev){
        updateavance(100);
        window.location.href=_sysUrlBase_+'/cursos/?tpl=top';       
    })
    let updatetabs=false;
    let updateavance=function(newval){
        let vactual=$('.progresscurso').first().attr('aria-value');
        vactual=parseFloat(vactual);
        newval=parseFloat(newval);
        if(vactual<newval){
            $('.progresscurso').first().attr('aria-value',newval);
            $('.progresscurso').css({width:newval+'%'}).text('Curso '+newval+'% completado');
            $('.progresscurso').siblings().css({width:(100 - newval)+'%'});            
            jsoncurso.infoavance=newval;
            let txtjson=JSON.stringify(jsoncurso);
            __sysajax({   
                fromdata:{id:idcurso,campo:'txtjson',valor:txtjson},
                url:url_media+'/smartcourse/acad_curso/setCampojson',  
                showmsjok:false,
            })            
        }        
        if(updatetabs==false){
            updatetabs=true;
            $.each($('ul#ultabs').find('a'),function(i,v){
                let va=parseInt($(v).attr('data-value')||0);
                if(va<=newval) $(v).closest('li').addClass('active');
            })
        }
    }

    if(parseInt(idcurso)>0){
        __sysajax({
            fromdata:{idcurso:parseInt(idcurso)},
            showmsjok:false,
            url:url_media+'/smartcourse/acad_curso/buscarjson',					
            callback:function(rs){
               let dt=rs.data;
                if(dt.length>0){
                    let info=rs.data[0];                    
                    let jsontmp=JSON.parse(info.txtjson.trim()==''?'{}':info.txtjson);                   
                    $.extend(jsoncurso,jsontmp);
                    updateavance(jsoncurso.infoavance);
                } 
            }
        });
    }
})
</script>