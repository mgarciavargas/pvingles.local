<?php 
$idgui=uniqid(); 
$imgcursodefecto='/static/media/nofoto.jpg';
?>
<style>
	#cardsilabo ul#mindice li{
		padding:1ex 0px 0.5ex 0.5ex;
		list-style: decimal;
	}
	#cardsilabo	ul#mindice li .macciones{  
		float: right; display: none;
	}

	#cardsilabo ul#mindice li:hover{
		background: #f5eed69e;
	    border: 1px #f00 dashed;
	    cursor: pointer;	   
	}
	#cardsilabo ul#mindice li:hover li:hover{
		background:#8fff0033;
	}

	#cardsilabo ul#mindice li:hover li:hover li:hover{
		background:#f5e3d67a;
	}

	#cardsilabo ul#mindice li:hover > div > span.macciones{
		display: block;
	}
	.btniconxs{
		width: 23px; height: 23px; padding: 0.45ex;
	}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 1.5ex; margin-top: 1ex;">
            <div class="progress">
                <div class="progresscurso progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-value="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">75% completado</div>
                <div class="progress-bar bg-warning" role="progressbar" aria-value="100" aria-valuemin="0" aria-valuemax="100" style="width: 25%"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="wizard">
                <ul class="nav nav-wizard" id="ultabs">
				<li class="active"><a href="#" data-value="10" data-show="paso1"><b class="number">1.</b> Datos del curso </a></li>
                    <li class="active" ><a href="#" data-value="20" data-show="paso2"><b class="number">2.</b> Categorias </a></li>
                    <li class="active" ><a href="#" data-value="30" data-show="paso3"><b class="number">3.</b> Estructura </a></li>
                    <li class="active selected" ><a href="#" data-value="50" data-show="paso4"><b class="number">4.</b> Sesiones </a></li>
                    <li class="" ><a href="#" data-value="60" data-show="portada"><b class="number">5.</b> Portada </a></li>
                    <li class="" ><a href="#" data-value="70" data-show="indice"><b class="number">6.</b> Indice </a></li>
                    <li class="" ><a href="#" data-value="80" data-show="contenido"><b class="number">7.</b> Contenidos </a></li>
                    <li class="" ><a href="#" data-value="100" data-show="publicar"><b class="number">8.</b> Publicar </a></li>
                </ul>
            </div>
            <div class="tab-content shadow" id="panelesultabs">
                <div class="tab-pane active" role="tabpanel" id="pasosilabus">
                <div class="card text-center" id="cardsilabo">
                    <div class="card-body">
                        <div class="row btnmanagermenu">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <a href="javascript:void(0)" class="importartemas btn btn-block bg-primary" style="color:#fff">
                                            <div><i class="fa fa-cloud-upload fa-2x "></i></div>												
                                            <p class="bolder">Importar Sesiones.</p>
                                        </a>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <a href="javascript:void(0)" class="creartema btn btn-block bg-warning" style="color:#fff">
                                            <div><i class="fa fa-plus fa-2x "></i></div>
                                            <p class="bolder">Nueva Sesión</p>
                                        </a>
                                    </div>                 				
                                </div>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row" id no >
                            <div class="col-12" id="paneldisenio" style="display: none;">
                                <form id="frmdatosdeltema" method="post" target="" enctype="multipart/form-data">
                                    <input type="hidden" name="idcurso" 		id="idcurso" value="<?php echo $this->idcurso;?>">
                                    <input type="hidden" name="idcursodetalle" 	id="idcursodetalle" value="0">
                                    <input type="hidden" name="idpadre" 		id="idpadre" value="0">
                                    <input type="hidden" name="orden" 			id="orden" value="0">
                                    <input type="hidden" name="idrecurso" 		id="idrecurso" value="0">
                                    <input type="hidden" name="idrecursopadre" 	id="idrecursopadre" value="0">
                                    <input type="hidden" name="idrecursoorden" 	id="idrecursoorden" value="0">
									<input type="hidden" name="imagen" 	id="imagen" value="">
									<input type="hidden" name="accimagen" 	id="accimagen" value="sg">
                                    <input type="hidden" name="tiporecurso" id="tiporecurso" value="0">
                                    <input type="hidden" name="idlogro" id="idlogro" value="0">				
                                    <input type="hidden" name="oldimagen" id="oldimagen" value="0">
                                    <input type="hidden" name="accmenu" id="accmenu" value="0">
                                    <input type="hidden" id="esfinal" name="esfinal" value="1" >

                                    <div class="card text-center shadow">				 
                                    <div class="card-body">
                                        <!--h5 class="card-title"><?php //echo JrTexto::_('Datos generales del curso');?></h5-->
                                        <div class="row">
                                            <div class="col-12 col-sm-6 col-md-6">
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <label style=""><?php echo JrTexto::_('Nombre del Tema(Sesión)');?></label> 
                                                        <input type="text" autofocus name="nombre" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Tema 001"))?>" >					           
                                                    </div>
                                                
                                                    
                                                    <div class="col-12 form-group">
                                                        <label style=""><?php echo JrTexto::_('Descripción');?></label> 
                                                        <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del tema"))?>"></textarea>
                                                    </div>
                                                                                    
                                                    <div class="col-6 form-group text-left ">		        	
                                                        <label style=""><?php echo JrTexto::_('Color');?> </label> 
                                                        <input type="" name="color" value="transparent" class="form-control" >			       
                                                    </div>
                                                        
                                                </div>		       
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-6">
                                                <label><?php echo JrTexto::_("imagen tema") ?> (opcional) </label>                
                                                <div style="position: relative;" class="frmchangeimage text-center" id="contenedorimagen">
                                                    <div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
                                                    <div id="sysfilelogo">
                                                    <img src="<?php echo URL_MEDIA.$imgcursodefecto;?>" class="__subirfile img-fluid center-block" data-type="imagen"  id="imagen" style="max-width: 200px; max-height: 150px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>	    			   
                                    </div>
                                    <div class="card-footer text-muted">
                                        <button type="button" class="btncancelar btn btn-warning"><i class=" fa fa-close"></i> Cancelar</button>
                                        <button type="submit" class="btn btn-primary btnguardartema"><i class=" fa fa-save"></i> Guardar Tema</button>
                                    </div>
                                    </div>
                                </form>

                            </div>	
                            <div class="col-12 text-left" id="plnindice">
                                <li class="paraclonar" style="display: none">
                                    <div><span class="mnombre"></span>
                                    <span class="macciones">
                                        <!--i class="mmsubmenu fa fa-sort-down verpopoverm" data-content="Ocultar Submenus" ></i>
                                        <i class="mmsubmenu fa fa-sort-up verpopoverm" data-content="Mostrar Submenus" ></i>
                                        <i class="addcontent fa fa-eye verpopoverm" data-content="Ver contenido" data-placement="top"></i-->
                                        <i class="btn btniconxs btn-sm btn-warning magregar fa fa-plus verpopoverm" data-content="Agregar SubSesión" data-placement="top"></i>
                                        <i class="btn btniconxs btn-sm btn-primary meditar fa fa-pencil verpopoverm" data-content="Editar Sesión" data-placement="top"></i>
                                        <i class="btn btniconxs btn-sm btn-danger meliminar fa fa-trash verpopoverm" data-content="Eliminar Sesión" data-placement="top"></i>
                                        <!--i class="mmove fa fa-arrows-alt verpopoverm" data-content="Mover Sesión" data-placement="right"></i>
                                        <i class="mcopylink fa fa-link verpopoverm" data-content="Copiar link" data-placement="right"></i-->
                                        <!--span style="display:inline-block">								
                                            <i class="mmoveup fa fa-angle-double-up verpopoverm" data-content="Mover Arriba" ></i>
                                            <i class="mmovedown fa fa-angle-double-down verpopoverm" data-content="Mover abajo"  ></i>
                                        </span-->
                                    </span>
                                    </div>
                                </li>
                                <ul id="mindice">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        <button type="button" onclick="history.back()" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
                        <button type="button" class="btnnexttab btn btn-primary">Continuar <i class="fa fa-arrow-right"></i></button>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
var imgdefecto='<?php echo $imgcursodefecto; ?>';
var idproyecto=parseInt('<?php echo $this->idproyecto; ?>');
var jsoncurso={
    plantilla:{id:0,nombre:'blanco'},
    estructura:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    estilopagina:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    infoportada:{titulo:'',descripcion:'',image:''},
    infoindice:'top',
    infoavance:30
}
$(document).ready(function(){
	var idcurso=parseInt($('#idcurso').val());
	$('ul#ultabs').on('click','a',function(ev){
		ev.preventDefault();
		if(idcurso>0){
			let sel=$('ul#ultabs').find('li.selected').children('a');
			let lia=$(this);
			let liatxt=lia.attr('data-show');
			let liav=parseInt(lia.attr('data-value')||0);
			let avtxt=sel.attr('data-show');			
			let av=parseInt(sel.attr('data-value')||0);
			if(jsoncurso.infoavance >= liav && liatxt!=avtxt){
				window.location.href=_sysUrlBase_+'/cursos/crear/?pg='+liatxt+'&idproyecto='+idproyecto+'&idcurso='+idcurso;
			}
		}
	})


	var addtemas=function(donde,temas){
			$.each(temas,function(i,t){
				//console.log(t);
				let dt={hijos:{},imagen:imgdefecto};
				$.extend(dt,t);				
				//if(.hijos==undefined) t.hijos={};
				let hijos=dt.hijos||[];
				let liclonado=$('#plnindice').children('li.paraclonar').clone(true);
				liclonado.removeAttr('style').removeClass('paraclonar');
				liclonado.attr('id','me_'+dt.idcursodetalle);
				liclonado.attr('data-idrecurso',dt.idrecurso);
				liclonado.attr('data-idcursodetalle',dt.idcursodetalle);
				liclonado.attr('data-idpadre',dt.idpadre);
				liclonado.attr('data-orden',dt.orden);
				liclonado.attr('data-tipo',dt.tiporecurso);
				liclonado.attr('data-imagen',dt.imagen);
				liclonado.attr('data-esfinal',dt.esfinal);
				liclonado.children('div').children('span.mnombre').text(dt.nombre);				
				if(hijos.length){
					liclonado.addClass('hijos');
					liclonado.append('<ul id="me_hijo'+dt.idcursodetalle+'"></ul>');					
					addtemas(liclonado.find('#me_hijo'+dt.idcursodetalle),hijos);
				}
				donde.append(liclonado);
			})
			_fnordenar();
		}
		let _buscartemaremove=function(itemas,idcurdet){
			let icont=false;
			if(itemas.length>0){				
				itemas.forEach(function(cur,i,v){
					if(icont==false)
					if(itemas[i].idcursodetalle==idcurdet){		
						itemas.splice(i, i);
						icont=true;
						return true;
					}else{
						let ic=_buscartemaremove(cur.hijos,idcurdet);
						if(ic==true){
							icont=true;
							return true;
						}
					}
				})
			}
		}
		let _modificartema=function(itemas,iddet,dt,edit){
			let icont=false;
			if(itemas!=undefined)
			if(itemas.length>0){				
				itemas.forEach(function(cur,i,v){
					if(icont==false)
					if(itemas[i].idcursodetalle==iddet){
						if(edit) $.extend(itemas[i],dt);
						else{
							if(itemas[i]['hijos']==undefined)itemas[i]['hijos']=[];
							//console.log('eee',itemas[i]['hijos'],cur,i);
							itemas[i]['hijos'].push(dt);
						}
						icont=true;
						return true;
					}else{
						let ic=_modificartema(itemas[i].hijos,iddet,dt,edit);
						if(ic==true){
							icont=true;
							return true;
						}
					}
				})
			}
		}
	$('.__subirfile').on('click',function(){
        let $img=$(this);            
        __subirfile({file:$img,typefile:'imagen',uploadtmp:true,guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},function(rs){                    
            let f=rs.media.replace(url_media, '');
            $('input#'+$img.attr('id')).val(f);
        });
    });
	$('.btnremoveimage').on('click',function(){
        let $img= $(this).closest('#contenedorimagen').find('img');        
        $img.attr('src',imgdefecto);
        $('input#imagen').val('');
    })

    $('#pasosilabus').on('click','.btnnexttab',function(ev){
			ev.preventDefault();
			window.location.href=_sysUrlBase_+'/cursos/crear/?pg=portada&idproyecto='+idproyecto+'&idcurso='+idcurso;

		}).on('click','.creartema',function(ev){
			ev.preventDefault();
	    	let pnlcont=$('#pasosilabus').find('#paneldisenio');
			pnlcont.find('input[name="idcursodetalle"]').val(0);
			pnlcont.find('input[name="idpadre"]').val(0);
			pnlcont.find('input[name="orden"]').val(0);
			pnlcont.find('input[name="idrecurso"]').val(0);
			pnlcont.find('input[name="idrecursopadre"]').val(0);
			pnlcont.find('input[name="idrecursoorden"]').val(0);
			pnlcont.find('input[name="tiporecurso"]').val('M');
			pnlcont.find('input[name="idlogro"]').val(0);
			pnlcont.find('input[name="esfinal"]').val(0);
			pnlcont.find('input[name="nombre"]').val('');
			pnlcont.find('input[name="imagen"]').val('');
			pnlcont.find('textarea[name="descripcion"]').val('');
			pnlcont.find('img#imagen').attr('src',url_media+imgdefecto);
			pnlcont.find('input[name="color"]').val('rgba(0,0,0,1)');
			pnlcont.find('input[name="color"]').minicolors({opacity: true,format:'rgb'});
			pnlcont.find('input[name="color"]').minicolors('settings',{value:'rgba(0,0,0,1)'});
			pnlcont.find('input[name="accmenu"]').val('additem');
			pnlcont.find('input[name="url"]').val('');
			$('#pasosilabus').find('#plnindice').hide(0);
			$('#pasosilabus').find('.card-footer').hide(0);			  	
	    	$('#pasosilabus').find('#paneldisenio').show(0);
	    	$('#pasosilabus').find('.btnmanagermenu').hide(0);
			pnlcont.find('.card-footer').show();
		}).on('click','.importartemas',function(ev){	
		}).on('click','ul#mindice i.magregar',function(ev){
			let li=$(this).closest('li');
			let pnlcont=$('#pasosilabus').find('#paneldisenio');
			pnlcont.find('input[name="idcursodetalle"]').val(0);
			pnlcont.find('input[name="idpadre"]').val(li.attr('data-idcursodetalle'));
			pnlcont.find('input[name="orden"]').val(0);
			pnlcont.find('input[name="idrecurso"]').val(0);
			pnlcont.find('input[name="idrecursopadre"]').val(li.attr('data-idrecurso'));
			pnlcont.find('input[name="idrecursoorden"]').val(0);
			pnlcont.find('input[name="tiporecurso"]').val('M');
			pnlcont.find('input[name="idlogro"]').val(0);
			pnlcont.find('input[name="esfinal"]').val(0);
			pnlcont.find('input[name="nombre"]').val('');
			pnlcont.find('input[name="imagen"]').val('');
			pnlcont.find('textarea[name="descripcion"]').val('');
			pnlcont.find('img#imagen').attr('src',url_media+imgdefecto);
			pnlcont.find('input[name="color"]').val('rgba(0,0,0,1)');
			pnlcont.find('input[name="color"]').minicolors({opacity: true,format:'rgb'});
			pnlcont.find('input[name="color"]').minicolors('settings',{value:'rgba(0,0,0,1)'});
			pnlcont.find('input[name="accmenu"]').val('addsubitem');
			pnlcont.find('input[name="url"]').val('');
			$('#pasosilabus').find('#plnindice').hide(0);
			$('#pasosilabus').find('.card-footer').hide(0);			  	
	    	$('#pasosilabus').find('#paneldisenio').show(0);
	    	$('#pasosilabus').find('.btnmanagermenu').hide(0);
			pnlcont.find('.card-footer').show();
		}).on('click','ul#mindice i.meditar',function(ev){			
			let li=$(this).closest('li');
			let pnlcont=$('#pasosilabus').find('#paneldisenio');
			pnlcont.find('input[name="idcursodetalle"]').val(li.attr('data-idcursodetalle'));
			pnlcont.find('input[name="idpadre"]').val(li.attr('data-idpadre'));
			pnlcont.find('input[name="orden"]').val(li.attr('data-orden')||1);
			pnlcont.find('input[name="idrecurso"]').val(li.attr('data-recurso'));
			pnlcont.find('input[name="idrecursopadre"]').val(li.attr('data-idrecursopadre'));
			pnlcont.find('input[name="idrecursoorden"]').val(li.attr('data-idrecursoorden')||1);
			pnlcont.find('input[name="tiporecurso"]').val(li.attr('data-tiporecurso')||'M');
			pnlcont.find('input[name="idlogro"]').val(li.attr('data-idlogro')||0);
			pnlcont.find('input[name="esfinal"]').val(li.attr('data-esfinal'));
			pnlcont.find('input[name="nombre"]').val(li.children('div').children('.mnombre').text());			
			pnlcont.find('textarea[name="descripcion"]').val(li.attr('data-descripcion'));
			console.log(li.attr('data-imagen'));
			pnlcont.find('input[name="imagen"]').val(li.attr('data-imagen'));
			pnlcont.find('img#imagen').attr('src',url_media+(li.attr('data-imagen')||imgdefecto));
			pnlcont.find('input[name="color"]').val(li.attr('data-color'));
			pnlcont.find('input[name="color"]').minicolors({opacity: true,format:'rgb'});
			pnlcont.find('input[name="color"]').minicolors('settings',{value:li.attr('data-color')});
			pnlcont.find('input[name="url"]').val(li.attr('data-url'));
			pnlcont.find('input[name="accmenu"]').val('edit');
			$('#pasosilabus').find('#plnindice').hide(0);
			$('#pasosilabus').find('.card-footer').hide(0);			  	
	    	$('#pasosilabus').find('#paneldisenio').show(0);
	    	$('#pasosilabus').find('.btnmanagermenu').hide(0);
			pnlcont.find('.card-footer').show();
		}).on('click','ul#mindice i.meliminar',function(ev){
			$li=$(this).closest('li');
			//$(this).popover('dispose');
			__sysajax({
				url:url_media+'/smartcourse/acad_cursodetalle/eliminarMenu',
				fromdata:{
					idcurso:idcurso,
					idcursodetalle:parseInt($li.attr('data-idcursodetalle')),
					idrecurso:parseInt($li.attr('data-idrecurso')),
					tiporecurso:$li.attr('data-tipo'),
				},
				type:'html',
				showmsjok:true,
				callback:function(rs){
					$li.remove();
				}
			});			
		}).on('click','.btncancelar',function(ev){
			$('#pasosilabus').find('#paneldisenio').hide(0);
	    	$('#pasosilabus').find('#plnindice').show(0);
	    	$('#pasosilabus').find('.card-footer').show(0);
	    	$('#pasosilabus').find('.btnmanagermenu').show(0);	    	
		}).on('submit','#frmdatosdeltema',function(ev){			
			ev.preventDefault();
			let frm=$(this);
			let esfinal=frm.find('#esfinal').val()||0;
			if(parseInt(esfinal)==0)frm.find('#tiporecurso').val('M');
			else frm.find('#tiporecurso').val('S');
			let acc=frm.find('input[name="accmenu"]').val();
			var data=__formdata('frmdatosdeltema');

			__sysajax({
				fromdata:data,
        	    url:url_media+'/smartcourse/acad_cursodetalle/guardarMenu',
            	callback:function(rs){
					var dt=rs.newid;					
					if(acc=='additem'||acc=='addsubitem'){
						let liclonado=$('#plnindice').children('li.paraclonar').clone(true);
							liclonado.removeAttr('style').removeClass('paraclonar');
							liclonado.attr('id','me_'+dt.idcursodetalle);
							liclonado.attr('data-idrecurso',parseInt(dt.idnivel));
							liclonado.attr('data-idnivel',parseInt(dt.idnivel));
							liclonado.attr('data-idpadre',parseInt(dt.idpadre));						
							liclonado.attr('data-idrecursopadre',parseInt(dt.idnivelpadre||0));
							liclonado.attr('data-idcursodetalle',dt.idcursodetalle);
							liclonado.attr('data-orden',parseInt(dt.orden));
							liclonado.attr('data-ordenrecurso',parseInt(dt.ordennivel));

							liclonado.attr('data-idlogro',parseInt(frm.find('input[name="idlogro"]').val()||'0'));							
							liclonado.attr('data-tipo',frm.find('input[name="tiporecurso"]').val()||'M');
							liclonado.attr('data-imagen',frm.find('input[name="imagen"]').val()||'');
							liclonado.attr('data-esfinal',frm.find('input[name="esfinal"]').val()||'0');
							liclonado.attr('data-idcurso',idcurso);
							liclonado.children('div').children('span.mnombre').text(frm.find('input[name="nombre"]').val()||'');
							liclonado.attr('data-descripcion',frm.find('textarea[name="descripcion"]').val()||'');
							liclonado.attr('data-color',frm.find('input[name="color"]').val()||'');
							liclonado.attr('data-url',frm.find('textarea[name="url"]').val()||'');
							liclonado.attr('index',dt.orden);
							liclonado.attr('data-index',dt.orden);
						let pnltmp=$('#pasosilabus').find('ul#mindice');
						if(acc=='additem'){
							pnltmp.append(liclonado);
						}else if(acc=='addsubitem'){
							$li=$('#mindice li#me_'+dt.idpadre);
							liul=$li.children('ul');
							if(liul.length==0){
								$li.append('<ul></ul>');
								liul=$li.children('ul');
							}
							liul.append(liclonado);
						}
					}else if(acc=='edit'){
						let pnltmp=$('#pasosilabus').find('ul#mindice');						
						let liclonado=pnltmp.find('#me_'+dt.idcursodetalle);						
							liclonado.attr('id','me_'+dt.idcursodetalle);
							liclonado.attr('data-idrecurso',parseInt(dt.idnivel));
							liclonado.attr('data-idnivel',parseInt(dt.idnivel));
							liclonado.attr('data-idpadre',parseInt(dt.idpadre));						
							liclonado.attr('data-idrecursopadre',parseInt(dt.idnivelpadre||0));
							liclonado.attr('data-idcursodetalle',dt.idcursodetalle);
							liclonado.attr('data-orden',parseInt(dt.orden));
							liclonado.attr('data-ordenrecurso',parseInt(dt.ordennivel));
							liclonado.attr('data-idlogro',parseInt(frm.find('input[name="idlogro"]').val()||'0'));							
							liclonado.attr('data-tipo',frm.find('input[name="tiporecurso"]').val()||'M');
							liclonado.attr('data-imagen',frm.find('input[name="imagen"]').val()||'');
							liclonado.attr('data-esfinal',frm.find('input[name="esfinal"]').val()||'0');
							liclonado.attr('data-idcurso',idcurso);
							liclonado.children('div').children('span.mnombre').text(frm.find('input[name="nombre"]').val()||'');
							liclonado.attr('data-descripcion',frm.find('textarea[name="descripcion"]').val()||'');
							liclonado.attr('data-color',frm.find('input[name="color"]').val()||'');
							liclonado.attr('data-url',frm.find('textarea[name="url"]').val()||'');
							liclonado.attr('index',dt.orden);
							liclonado.attr('data-index',dt.orden);				            			
					} 
					$('#paneldisenio').fadeOut(0);
					$('#plnindice').fadeIn('fast');
					$('.card-footer').fadeIn('fast');
					$('.btnmanagermenu').fadeIn('fast');
					//console.log(infocurso.temas);
					
	        	}
			});
			return false;
		})

		let updatetabs=false;
		let updateavance=function(newval){
			let vactual=$('.progresscurso').first().attr('aria-value');
			vactual=parseFloat(vactual);
			newval=parseFloat(newval);
			if(vactual<newval){
				$('.progresscurso').first().attr('aria-value',newval);
				$('.progresscurso').css({width:newval+'%'}).text('Curso '+newval+'% completado');
				$('.progresscurso').siblings().css({width:(100 - newval)+'%'});
				jsoncurso.infoavance=newval;                       
			}		
			if(updatetabs==false){
				updatetabs=true;
				$.each($('ul#ultabs').find('a'),function(i,v){
					let va=parseInt($(v).attr('data-value')||0);
					if(va<=newval) $(v).closest('li').addClass('active');
				})
			}
		}

		var cargartemashijos=function(hijos){
			var htmltmp=$('<div><div>');			
			$.each(hijos,function(i,dt){
				var liclonado=$('#plnindice').children('li.paraclonar').clone(true);

					liclonado.removeAttr('style').removeClass('paraclonar');
					liclonado.attr('id','me_'+dt.idcursodetalle);
					liclonado.attr('data-idrecurso',parseInt(dt.idnivel));
					liclonado.attr('data-idnivel',parseInt(dt.idnivel));
					liclonado.attr('data-idpadre',parseInt(dt.idpadre));						
					liclonado.attr('data-idrecursopadre',parseInt(dt.padrenivel||0));
					liclonado.attr('data-idcursodetalle',dt.idcursodetalle);
					liclonado.attr('data-idlogro',parseInt(dt.idlogro));
					liclonado.attr('data-orden',parseInt(dt.orden));
					liclonado.attr('data-ordenrecurso',parseInt(dt.padrenivel));
					liclonado.attr('data-tipo',dt.tiporecurso||'M');
					liclonado.attr('data-imagen',dt.imagen);
					liclonado.attr('data-esfinal',dt.esfinal);
					liclonado.attr('data-idcurso',dt.idcurso);
					liclonado.children('div').children('span.mnombre').text(dt.nombre);
					liclonado.attr('data-descripcion',dt.descripcion);
					liclonado.attr('data-color',dt.color);
					liclonado.attr('data-url',dt.url);
					liclonado.attr('index',dt.orden);
					liclonado.attr('data-index',dt.orden);
					if(dt.hijos.length>0){							
						liclonado.append('<ul>'+cargartemashijos(dt.hijos)+'<ul>');
					}
					htmltmp.append(liclonado);
			})
			return htmltmp.html();
		}

		__sysajax({
			donde:$('#plnindice'),
			url:url_media+'/smartcourse/cursos/jsontemas',
			fromdata:{idcurso:idcurso},
			mostrarcargando:true,
			showmsjok:false,
			callback:function(rs){
				dts=rs.data;
				console.log(dts);
				if(dts.length>0){
					$.each(dts,function(i,dt){										
						let liclonado=$('#plnindice').children('li.paraclonar').clone(true);
						liclonado.removeAttr('style').removeClass('paraclonar');
						liclonado.attr('id','me_'+dt.idcursodetalle);
						liclonado.attr('data-idrecurso',parseInt(dt.idnivel));
						liclonado.attr('data-idnivel',parseInt(dt.idnivel));
						liclonado.attr('data-idpadre',parseInt(dt.idpadre));						
						liclonado.attr('data-idrecursopadre',parseInt(dt.padrenivel||0));
						liclonado.attr('data-idcursodetalle',dt.idcursodetalle);
						liclonado.attr('data-idlogro',parseInt(dt.idlogro));
						liclonado.attr('data-orden',parseInt(dt.orden));
						liclonado.attr('data-ordenrecurso',parseInt(dt.padrenivel));
						liclonado.attr('data-tipo',dt.tiporecurso||'M');
						liclonado.attr('data-imagen',dt.imagen);
						liclonado.attr('data-esfinal',dt.esfinal);
						liclonado.attr('data-idcurso',dt.idcurso);
						liclonado.children('div').children('span.mnombre').text(dt.nombre);
						liclonado.attr('data-descripcion',dt.descripcion);
						liclonado.attr('data-color',dt.color);
						liclonado.attr('data-url',dt.url);
						liclonado.attr('index',dt.orden);
						liclonado.attr('data-index',dt.orden);
						if(dt.hijos.length>0){
							liclonado.append('<ul>'+cargartemashijos(dt.hijos)+'<ul>');
						}
						
						$('#pasosilabus').find('ul#mindice').append(liclonado);
					})
				}				
			}
		});
		if(parseInt(idcurso)>0){
			__sysajax({
				fromdata:{idcurso:parseInt(idcurso)},
				showmsjok:false,
				url:url_media+'/smartcourse/acad_curso/buscarjson',					
				callback:function(rs){
					let dt=rs.data;
					if(dt.length>0){
						let info=rs.data[0];                    
						let jsontmp=JSON.parse(info.txtjson.trim()==''?'{}':info.txtjson);                   
						$.extend(jsoncurso,jsontmp);
						updateavance(jsoncurso.infoavance);
						let idplantilla=jsoncurso.plantilla.id;
						$('.btnplantilla[data-idplantilla="'+idplantilla+'"]').children('a').addClass('btn-success');
					} 
				}
			});
		}
})

</script>