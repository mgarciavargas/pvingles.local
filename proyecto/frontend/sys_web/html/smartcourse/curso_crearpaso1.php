<?php 
$idgui=uniqid(); 
$imgcursodefecto='/static/media/nofoto.jpg';
?>
<style>

</style>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 1.5ex; margin-top: 1ex;">
            <div class="progress">
                <div class="progresscurso progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-value="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">% completado</div>
                <div class="progress-bar bg-warning" role="progressbar" aria-value="100" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="wizard">
                <ul class="nav nav-wizard" id="ultabs">
                    <li class="active selected"><a href="#" data-value="10" data-show="paso1"><b class="number">1.</b> Datos del curso </a></li>
                    <li class="" ><a href="#" data-value="20" data-show="paso2"><b class="number">2.</b> Categorias </a></li>
                    <li class="" ><a href="#" data-value="30" data-show="paso3"><b class="number">3.</b> Estructura </a></li>
                    <li class="" ><a href="#" data-value="50" data-show="paso4"><b class="number">4.</b> Sesiones </a></li>
                    <li class="" ><a href="#" data-value="60" data-show="portada"><b class="number">5.</b> Portada </a></li>
                    <li class="" ><a href="#" data-value="70" data-show="indice"><b class="number">6.</b> Indice </a></li>
                    <li class="" ><a href="#" data-value="80" data-show="contenido"><b class="number">7.</b> Contenidos </a></li>
                    <li class="" ><a href="#" data-value="100" data-show="publicar"><b class="number">8.</b> Publicar </a></li>
                </ul>
            </div>
            <div class="tab-content shadow" id="panelesultabs">
                <div class="tab-pane active " role="tabpanel" id="pasoinfo">
                    <form id="frmdatosdelcurso" method="post" target="" enctype="multipart/form-data">
                    <input type="hidden" name="idcurso" id="idcurso" value="<?php echo $this->idcurso;?>">
                    <input type="hidden" name="imagen" id="imagen" value="">
                        <div class="card shadow">
                            <div class="card-body">	
                                <div class="row">
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-12 form-group">
                                                <label style=""><?php echo JrTexto::_('Nombre del curso');?></label> 
                                                <input type="text" autofocus name="nombre" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Curso 01"))?>" >					           
                                            </div>
                                            <div class="col-6 col-sm-12 form-group">							        	
                                                <label style=""><?php echo JrTexto::_('Autor');?></label> 
                                                <input type="text" name="autor"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Apellidos y Nombres del autor "))?>">					           
                                            </div>
                                            <div class="col-6 col-sm-12 form-group">
                                                <label style=""><?php echo JrTexto::_('Fecha de publicación');?></label> 
                                                <input type="date" name="aniopublicacion" class="form-control" placeholder="<?php echo date('Y-m-d'); ?>" >
                                            </div>
                                            <div class="col-12 form-group">
                                                <label style=""><?php echo JrTexto::_('Reseña');?></label> 
                                                <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del curso"))?>"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 text-center form-group">
                                        <div class="row">
                                            <div class="col-12">
                                                <label><?php echo JrTexto::_("Imagen de curso") ?> </label>                
                                                <div style="position: relative;" class="frmchangeimage text-center" id="contenedorimagen">
                                                    <div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
                                                    <div id="sysfilelogo">
                                                    <img src="<?php echo URL_MEDIA.$imgcursodefecto;?>" class="__subirfile img-fluid center-block" data-type="imagen"  id="imagen" style="max-width: 200px; max-height: 150px;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 form-group text-left " style="padding:3em;">		        	
                                                <label style=""><?php echo JrTexto::_('Color del curso');?> </label> <br>
                                                <input type="" name="color" value="rgb(0,0,0,0)" class="vercolor form-control" >			       
                                            </div>
                                        </div>
                                    </div>
                                </div>			            				
                            </div>
                            <div class="card-footer text-center">
                                <button type="button" onclick="history.back();" class="btn btn-warning"><i class=" fa fa-undo"></i> Regresar al listado</button>
                                <button type="submit" class="btn btn-primary" data-pclase="tab-pane" data-showp="#pasocate" ><i class=" fa fa-save"></i> Guardar y continuar <i class=" fa fa-arrow-right"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
var imgdefecto='<?php echo $imgcursodefecto; ?>';
var idproyecto=parseInt('<?php echo $this->idproyecto; ?>');
var jsoncurso={
    plantilla:{id:0,nombre:'blanco'},
    estructura:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    estilopagina:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    infoportada:{titulo:'',descripcion:'',image:''},
    infoindice:'top',
    infoavance:0
}

$(document).ready(function(){
    var idcurso=$('#idcurso').val();
    $('ul#ultabs').on('click','a',function(ev){
		ev.preventDefault();
		if(idcurso>0){
			let sel=$('ul#ultabs').find('li.selected').children('a');
			let lia=$(this);
			let liatxt=lia.attr('data-show');
			let liav=parseInt(lia.attr('data-value')||0);
			let avtxt=sel.attr('data-show');			
			let av=parseInt(sel.attr('data-value')||0);			
			if(jsoncurso.infoavance >= liav && liatxt!=avtxt){
				window.location.href=_sysUrlBase_+'/cursos/crear/?pg='+liatxt+'&idproyecto='+idproyecto+'&idcurso='+idcurso;
			}
		}
	})

    $('.__subirfile').on('click',function(){
        let $img=$(this);            
        __subirfile({file:$img,typefile:'imagen',uploadtmp:true,guardar:true,dirmedia:'cursos/'},function(rs){                    
            let f=rs.media.replace(url_media, '');
            $('input#'+$img.attr('id')).val(f);
        });
    });

    $('.btnremoveimage').on('click',function(){
        let $img= $(this).closest('#contenedorimagen').find('img');        
        $img.attr('src',imgdefecto);
        $('input#imagen').val('');                 
    })

    $('#frmdatosdelcurso').on('submit',function(ev){
        ev.preventDefault();
        idcurso=$('#idcurso').val();
        if(idcurso=='0'){
            jsoncurso.estructura.color=$('input[name="color"]').val();
            jsoncurso.infoportada.titulo=$('input[name="nombre"]').val();
            jsoncurso.infoportada.descripcion=$('textarea[name="descripcion"]').val();
            jsoncurso.infoportada.image=$('input#imagen').val();
            jsoncurso.infoavance=parseInt($('ul#ultabs a[data-show="paso1"]').attr('data-value'));
        }        
        var data=__formdata('frmdatosdelcurso');
        data.append('txtjson',JSON.stringify(jsoncurso));
        __sysajax({   
                fromdata:data,
                url:url_media+'/smartcourse/acad_curso/guardarjson',               
                callback:function(rs){
                    if(rs.code==200){
                        idcurso=rs.newid;
                        $('#idcurso').val(idcurso);
                        __sysajax({
                                showmsjok:false,
                                fromdata:{idproyecto:idproyecto,idcurso:idcurso},
                                url:_sysUrlBase_+'/proyecto_cursos/guardarProyecto_cursos',
                                callback:function(rs){
                                    window.location.href=_sysUrlBase_+'/cursos/crear/?pg=paso2&idcurso='+idcurso;
                            }
                        })
                    }
            }
        })
    });

    var cambiarcolor=function(colortmp){
        let colortmp2=colortmp||'rgba(76, 80, 84, 0.75)';
        $('input[name="color"]').val(colortmp2);
        $('input[name="color"]').minicolors({opacity: true,format:'rgb'});
        $('input[name="color"]').minicolors('settings',{value:colortmp2});
    }

    let updatetabs=false;
    let updateavance=function(newval){
        let vactual=$('.progresscurso').first().attr('aria-value');
        vactual=parseFloat(vactual);
        newval=parseFloat(newval);
        if(vactual<newval){
            $('.progresscurso').first().attr('aria-value',newval);
            $('.progresscurso').css({width:newval+'%'}).text('Curso '+newval+'% completado');
            $('.progresscurso').siblings().css({width:(100 - newval)+'%'});            
        }
        if(updatetabs==false){
            updatetabs=true;
            $.each($('ul#ultabs').find('a'),function(i,v){
                let va=parseInt($(v).attr('data-value')||0);
                if(va<=newval) $(v).closest('li').addClass('active');
            })
        }
              
    }
    cambiarcolor();
    if(parseInt(idcurso)>0){
        __sysajax({
            fromdata:{idcurso:parseInt(idcurso)},
            showmsjok:false,
            url:url_media+'/smartcourse/acad_curso/buscarjson',					
            callback:function(rs){
               let dt=rs.data;
                if(dt.length>0){
                    let info=rs.data[0];
                    $('input[name="idcurso"]').val(info.idcurso);
                    $('input[name="imagen"]').val(info.imagen);
                    $('img#imagen').attr('src',url_media+info.imagen);
                    $('input[name="nombre"]').val(info.nombre);
                    $('input[name="autor"]').val(info.autor);
                    $('input[name="aniopublicacion"]').val(info.aniopublicacion);
                    $('textarea[name="descripcion"]').val(info.descripcion);                     
                    let jsontmp=JSON.parse(info.txtjson.trim()==''?'{}':info.txtjson);
                    cambiarcolor(info.color);               
                    $.extend(jsoncurso,jsontmp);            
                    updateavance(jsoncurso.infoavance);
                                      
                } 
            }
        });
    }
})
</script>