<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$pub=!empty($this->publicaciones)?$this->publicaciones:'';
$ruta=$this->documento->getUrlStatic()."/media/nofoto.jpg";
$isloginuser=NegSesion::existeSesion()==true?true:false;
//var_dump($pub);
if(!empty($pub["logo"])) $ruta=$this->documento->getUrlBase().$pub["logo"];
$file='<img src="'.$ruta.'" class="img-responsive center-block" style="max-width: 200px; max-height: 150px;">';
?>
<div class="container">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
	<?php if(!$ismodal){?><nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-primary no-print">
	    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>/../"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
	    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
	    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Publicación"); ?></li>
	  </ol>
	</nav>
	<?php } ?>
	<div class="row" >
		<div class="col-12 text-primary"><h4><?php echo @$pub["titulo"]; ?> <small> - Vacantes <?php echo !empty($pub["nvacantes"])?$pub["nvacantes"]:1; ?> </small></h4> </div>
		<div class="col-12 text-dark"><h3><?php echo ucfirst(@$pub["nombre"]);?></h3><hr></div>
		<div class="col-12 col-sm-12 col-md-4 form-group">
			<label>Razon social </label> <?php echo @$pub["rason_social"];?><br>
			<label>Direccion </label> <?php echo @$pub["direccion"];?><br>	    			
		</div>
		<div class="col-12 col-sm-12 col-md-4 form-group">
			<label>Correo </label> <?php echo @$pub["correo"];?><br>
			<label>Direccion </label> <?php echo @$pub["direccion"];?><br>	    			
		</div>
		<div class="col-12 col-sm-12 col-md-4 form-group">
			<?php 
			$ruta=$this->documento->getUrlStatic()."/media/nofoto.jpg";
			if(!empty($pub["logo"])) $ruta=$this->documento->getUrlBase().$pub["logo"];?>
			<img src="<?php echo $ruta; ?>" class="img-responsive img-tumhnails center-block" style="max-width: 120px; max-height: 100px;">
		</div>

		<div class="col-12 text-dark form-group"><hr><label>Descripcion </label><br><?php echo $pub["descripcion"];?><hr></div>

		<div class="col-12 form-group">
			<label><?php echo JrTexto::_("Salario") ?></label> 
			S/. <?php echo @$pub["sueldo"]; ?>
		</div>
		<div class="col-12 form-group">
			<label><?php echo JrTexto::_("Tiempo contrato") ?></label> 
			<?php echo @$pub["duracioncontrato"]; ?>
		</div>
		<div class="col-12 form-group">
			<label><?php echo JrTexto::_("Tipo contrato") ?></label> 
			<?php echo @$pub["xtiempo"]; ?>
		</div>
		<div class="col-12 form-group">
			<label><?php echo JrTexto::_("Disponibilidad para viajar") ?></label> 
			<?php echo @$pub["disponibilidadeviaje"]=='1'?'Si':'No'; ?>
		</div>
		<div class="col-12 form-group">
			<label><?php echo JrTexto::_("Disponibilidad para cambiar de residencia") ?></label> 
			<?php echo @$pub["cambioderesidencia"]=='1'?'Si':'No'; ?>
		</div>


	<?php if($this->login==true && !empty($this->postulantes)){
		//var_dump($this->postulantes);
		?>
		<div class="col-12 text-dark"><br><h3>Postulantes</h3><hr></div>
		<div class="col-12">
			<div class="row">
		<div class="col table-responsive border-primary" >
	        <table class="table table-striped table-hover" id="tbpostulantes" >
	              <thead>
	                <tr class="bg-primary">
	                  <th scope="col">#</th>
	                  <th scope="col"><?php echo JrTexto::_("Fecha Registro") ;?></th>
	                    <th scope="col"><?php echo JrTexto::_("Participante") ;?></th>
	                    <th scope="col"><?php echo JrTexto::_("Telefono") ;?></th>
	                    <th scope="col"><?php echo JrTexto::_("Correo") ;?></th>
	                    <th scope="col"><?php echo JrTexto::_("Mensaje") ;?></th>                    
	                    <th scope="col" class="sorting_disabled">
	                    	<span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
	                </tr>
	              </thead>
	              <tbody>
	              	<?php $i=0;
	              	if(!empty($this->postulantes))
	              	 foreach ($this->postulantes as $post){ $i++;?>
	              		<tr>
	              			<td><?php echo $i; ?></td>
	              			<td><?php echo $post["fecharegistro"]; ?></td>
	              			<td><?php echo $post["nombrecompleto"]; ?></td>
	              			<td><?php echo $post["telefono"]; ?></td>
	              			<td><?php echo $post["correo"]; ?></td>
	              			<td><?php echo $post["descripcion"]; ?></td>
	              			<td><a href="<?php echo $this->documento->getUrlBase(); ?>/../personal/perfil/?dni=<?php echo @$post["idpostulante"] ?>">Ver perfil</a></td>
						</tr>
	              	<?php } ?>
	        	  </tbody>
	        </table>
		</div>
	</div>
	</div>
		<?php } ?>
		<div class="col-12 text-center no-print">
       		<hr>
       		<button type="button" class="btn btn-warning btncancelar hvr-grow"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_("Regresar") ?></button>
       		<?php if($isloginuser==true){ ?> 
				<a class="btn btn-success btn-postularme text-white hvr-grow" href="<?php echo $this->documento->getUrlBase(); ?>/defecto/postularme/"><i class="fa fa-thumbs-up"> Postularme</i></a>
				<?php } ?>
			<button type="button" class="btn btn-primary btnprint hvr-grow"><i class="fa fa-print"></i> <?php echo JrTexto::_("Imprimir") ?></button>
			<br><br>            	
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('table#tbpostulantes').DataTable({"searching": false, "language":{"url":_sysUrlStatic_+'/libs/datatable1.10/idiomas/ES.json'}
		<?php //echo $this->documento->getIdioma()!='EN'?('"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>    });


		$('.btncancelar').click(function(){
			redir(_sysUrlBase_);
		})

		$('.btnprint').click(function(){
			window.print();
		})

		$('.btn-postularme').on('click',function(ev){
			ev.preventDefault();
			ev.preventDefault();
			var data={
				idpublicacion:'<?php echo $pub["idpublicacion"]; ?>',	
			}
			var info={url:$(this).attr('href'), ismodal:true, titulo:$(this).attr('data-title'),frmdata:data}      		
			_sysopenmodal_(info);
			return false;
		});
		
	});
</script>