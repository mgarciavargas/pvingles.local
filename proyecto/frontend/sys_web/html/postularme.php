<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$isloginuser=NegSesion::existeSesion()==true?true:false;
if($isloginuser){
    $user=NegSesion::getUsuario();
}
if($ismodal){ ?>
<span style="float: right; color: #777; font-size: 14px;" class="close cerrarmodal" data-dismiss="modal"><i class="fa fa-close"></i></span>
<?php } ?>
<form method="post" id="frmpostularme-<?php echo $idgui;?>"  target="" style="padding: 1em 1em 1ex 1em">
<input type="hidden" name="idpostular" value="">
<input type="hidden" name="idpublicacion" value="<?php echo $_REQUEST["idpublicacion"]; ?>">
<input type="hidden" name="idpostulante" value="<?php echo @$user["dni"]; ?>">
    <div class="row"> 
        <div class="col-12 text-center">
            <h3><?php echo JrTexto::_('Postularme');?></h3>
            <hr>
        </div>
        <div class="col-12 text-center" hidden="true" id="alert<?php echo $idgui; ?>">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong class="titulo"></strong> <span class="mensaje"></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        </div>
        <div class="col-12 form-group">
            <label><?php echo JrTexto::_('Nombre completos');?> <span class="required"> (*) </span></label>             
            <div class="input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span></div>
                <input type="text" name="nombrecompleto" id="nombrecompleto<?php echo $idgui; ?>" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("nombre completo"))?>"  value="<?php echo @$user["nombre_full"]; ?>">
            </div>
        </div>
        <div class="col-12 form-group">
            <label style=""><?php echo JrTexto::_('Correo');?></label>              
            <div class="input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-envelope"></i></span></div>
                <input type="email" name="correo" id="correo<?php echo $idgui; ?>" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("tunombre@hotmail.com"))?>"  value="<?php echo @$user["email"]; ?>">
            </div>            
        </div>
        <div class="col-12 col-sm-6 col-md-6 form-group">
            <label style=""><?php echo JrTexto::_('Telefono');?></label>              
            <div class="input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>
                <input type="text" name="telefono" id="telefono<?php echo $idgui; ?>" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("999 999 999"))?>"  value="<?php echo @$user["telefono"]; ?>">
            </div>            
        </div>
        <div class="col-12 form-group">
            <label style=""><?php echo JrTexto::_('Mensaje al empleador');?></label>              
            <div class="input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-envelope-open-o"></i></span></div>
                <textarea name="descripcion" id="descripcion<?php echo $idgui; ?>" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("999 999 999"))?>"> </textarea>
            </div>            
        </div>
        <div class="col-12 form-group text-center"><br>
            <button type="submit" class="btn btn-success"><i class="fa fa-handshake-o"></i> <?php echo JrTexto::_('Postularme Aqui')?></button>
            <button type="button" class="btn btn-warning btncancel<?php echo $idgui; ?>"><i class="fa fa-refresh"></i> <?php echo JrTexto::_('Cancelar')?></button><br>
        </div> 
    </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
    <?php if($ismodal){?>
        $('.btncancel<?php echo $idgui; ?>').click(function(ev){
            $(this).closest('.modal').modal('hide');
        });
    <?php } ?>

    $('#frmpostularme-<?php echo $idgui;?>').bind({
        submit:function(ev){
            ev.preventDefault();
            var tmpfrm=document.getElementById('frmpostularme-<?php echo $idgui;?>');  
            var formData = new FormData(tmpfrm);            
            var _sysajax={
                fromdata:formData,
                showmsjok:true,
                url:_sysUrlBase_+'/Bolsa_postulante/guardarPostulante',
                callback:function(rs){
                    var alert=$('#alert<?php echo $idgui; ?>');                    
                    alert.find('.titulo').html('');
                    alert.find('.mensaje').html(rs.msj);
                    alert.find('.alert').removeClass('alert-danger').addClass('alert-success');
                    alert.removeAttr('hidden');                    
                    setTimeout(function(){location.reload()},700);
                    return false;
                },callbackerror:function(rs){
                    var alert=$('#alert<?php echo $idgui; ?>');
                    alert.find('.titulo').html('Error');
                    alert.find('.mensaje').html(rs.msj);                    
                    alert.find('alert').removeClass('alert-success').addClass('alert-danger');
                    alert.removeAttr('hidden');
                }
            }
            sysajax(_sysajax);
            return false;
        }
    })
});
</script>