<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<style>
tr.trclone{
    display:none;
}
.breadcrumb{
    padding:1ex;
}
.breadcrumb>li{
    text-transform: capitalize;
    color:#fff;
    padding-left:1ex;
}
.breadcrumb>li+li:before {
    color: #e4dddd;
    content: "\f101";
    font: normal normal normal 16px/1 FontAwesome;
}
.breadcrumb>li.active {
    color: #383434;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<div class="container-fluid">
  <div class="row" id="breadcrumb" style="padding:1ex;">
      <div class="col-12">
          <ol class="breadcrumb bg-primary">
              <li><a style="color:#fff" href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>&nbsp;<?php echo JrTexto::_("Academico"); ?></a></li> 
              <li class="active" style="color:"> <i class="fa fa-building"></i>&nbsp;<?php echo JrTexto::_("locales"); ?></li>       
          </ol>
      </div>
  </div>
  <div class="row border-primary" id="filtros">
    <div class="col-xs-12 col-sm-4 col-md-6"></div>
    <div class="col-xs-12 col-sm-8 col-md-6 form-group">
      <div class="input-group">
        <input type="text" name="texto" id="texto" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("texto a buscar"))?>">
        <div class="input-group-append"><button class="btn btnbuscar btn-primary"><?php echo  ucfirst(JrTexto::_("Buscar"))?> <i class="fa fa-search"></i></button></div>
        <div class="input-group-append"><a class="btn btn-warning btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Local", "Agregar"));?>" data-titulo="<?php echo JrTexto::_("Local").' - '.JrTexto::_("Agregar"); ?>"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('Agregar')); ?> </a></div>
      </div>
    </div>    
  </div>
  <div class="row">
    <div class="col-md-12 border-primary">
        <table class="table table-striped table-hover">
              <thead>
                <tr class="bg-primary">
                  <th scope="col">#</th>
                  <th scope="col"><?php echo JrTexto::_("Nombre") ;?></th>
                  <th scope="col"><?php echo JrTexto::_("Direccion") ;?></th>                   
                  <th scope="col" class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
        </table>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos='';
function refreshdatos(){
    tabledatos.ajax.reload();
}
$(document).ready(function(){  
  var estados={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit='<?php echo ucfirst(JrTexto::_("local"))." - ".JrTexto::_("edit"); ?>';
  var draw=0;

    
  $(".btnbuscar<?php echo $idgui; ?>").click(function(ev){
    refreshdatos();
  }).keyup(function(ev){
    var code = ev.which;
    if(code==13){
      ev.preventDefault();
      refreshdatos();
    }
  });
  tabledatos=$('table.table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
            {'data': '<?php echo JrTexto::_("Direccion") ;?>'},
            /*{'data': '<?php echo JrTexto::_("Id_ubigeo") ;?>'},
            {'data': '<?php echo JrTexto::_("Tipo") ;?>'},
            {'data': '<?php echo JrTexto::_("Vacantes") ;?>'},
            {'data': '<?php echo JrTexto::_("Idugel") ;?>'},
            {'data': '<?php echo JrTexto::_("Idproyecto") ;?>'},*/
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/local/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             //d.texto=$('#texto').val(),
                        
            draw=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Nombre") ;?>': data[i].nombre,
                '<?php echo JrTexto::_("Direccion") ;?>': data[i].direccion,
               /* '<?php //echo JrTexto::_("Id_ubigeo") ;?>': data[i].id_ubigeo,
              '<?php //echo JrTexto::_("Tipo") ;?>': data[i].tipo,
              '<?php //echo JrTexto::_("Vacantes") ;?>': data[i].vacantes,
              '<?php //echo JrTexto::_("Idugel") ;?>': data[i].idugel,
              '<?php //echo JrTexto::_("Idproyecto") ;?>': data[i].idproyecto,*/
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/local/editar/?id='+data[i].idlocal+'" data-titulo="'+tituloedit+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idlocal+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#vent_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'local', 'setCampo', id,campo,data);
          if(res) tabledatos.ajax.reload();
        }
      });
  });

  $('#vent_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('vent_')||'Local';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{header:true,footer:false,borrarmodal:false}); 
  });
  
  $('.table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'local', 'eliminar', id);
        if(res) tabledatos.ajax.reload();
      }
    }); 
  });
});
</script>