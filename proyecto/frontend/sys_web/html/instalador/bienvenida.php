<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->datos)) $frm=$this->datos;
$ruta=$this->documento->getUrlStatic()."/media/nofoto.jpg";
$imgdefecto=$ruta;
if(!empty($frm["logo"])) $ruta=URL_MEDIA.$frm["logo"];
?>
<?php if(!$ismodal){?>
<div class="col-12">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" style="color:#fff">&nbsp;<?php echo JrTexto::_("Asistente crear proyectos"); ?></li>
    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Empresa"); ?></li-->
  </ol>
</nav>
</div>
<?php } ?>
<div class="col-12 text-center">
<h1>Paso 01 : <small>Registrar Empresa</small></h1>
</div>
<div class="col-12">
<form method="post" id="frmempresa"  target="" enctype="multipart/form-data" class="form-horizontal form-label-left" >
	<input type="hidden" name="idempresa" id="idempresa" value="<?php echo @$frm["idempresa"];?>">
	<input type="hidden" name="accion" id="accion" value="<?php echo JrTexto::_($this->frmaccion);?>">
	<input type="hidden" name="logo" id="logo" value="<?php echo @$frm["logo"];?>">
	    <div class="row">
		    <div class="col-12 col-sm-7 col-md-7 form-group">
		    	<div class="row">
		    		<div class="col-12 form-group">
	              		<label><?php echo JrTexto::_('Nombre');?> <span class="required"> (*) </span></label>              
	                	<input type="text"  id="nombre" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>" placeholder="<?php echo JrTexto::_('Nombre de la empresa') ?>">
	            	</div>
	            	<div class="col-12 form-group">
		            	<label><?php echo JrTexto::_('Razon social');?> <span class="required"> (*) </span></label>              
		                <input type="text"  id="rason_social" name="rason_social" class="form-control" value="<?php echo @$frm["rason_social"];?>"  placeholder="<?php echo JrTexto::_('Razon social de la empresa') ?>">
		            </div>
		            <div class="col-12 form-group">
		            	<label><?php echo JrTexto::_('Dirección');?> <span class="required"> (*) </span></label>              
		                <input type="text"  id="direccion" name="direccion" class="form-control" value="<?php echo @$frm["direccion"];?>"  placeholder="<?php echo JrTexto::_('Dirección de la empresa') ?>">
		            </div>
		    	</div>
		    </div>
		    <div class="col-12 col-sm-5 col-md-5 form-group">
		    	<div class="row">
		    		<div class="col form-group text-center">
		              	<label><?php echo JrTexto::_('Logo');?> </label>              
		             	<div style="position: relative;" class="frmchangeimage">
		                	<div class="toolbarmouse text-center">                  
		                  		<span class="btn btnremoveimage"><i class="fa fa-trash"></i></span>
		                	</div>
		                	<div id="sysfilelogo<?php echo $idgui; ?>" class="aquiseveimagen">
                        <img src="<?php echo $ruta;?>" class="__subirfile img-fluid center-block" style="max-width: 200px; max-height: 150px;">
                      </div>                
		              </div>
		            </div>	    		
		    	</div>
		    </div>
            <div class="col-12">
            	<div class="row">
            		<div class="col-12 col-sm-6 col-md-6 form-group">
              			<label><?php echo JrTexto::_('Ruc');?> <span class="required"> </span></label>              
               			<input type="text"  id="ruc<?php echo $idgui; ?>" name="ruc"  class="form-control" value="<?php echo @$frm["ruc"];?>" placeholder="<?php echo JrTexto::_('RUC Ej:12345678901') ?>">
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 form-group">
											<label><?php echo JrTexto::_('Telefono');?> <span class="required"> </span></label>              
											<input type="text"  id="telefono<?php echo $idgui; ?>" name="telefono"  class="form-control" value="<?php echo @$frm["telefono"];?>" placeholder="<?php echo JrTexto::_('Telefono de la empresa Ej: 2014 215 1215') ?>">
                    </div>
            	</div>
            	<hr>
            	<div class="row">
            		<div class="col-12  form-group">
              			<label><?php echo JrTexto::_('Representante');?> <span class="required"> </span></label>              
                		<input type="text"  id="representante<?php echo $idgui; ?>" name="representante"  class="form-control" value="<?php echo @$frm["representante"];?>" placeholder="<?php echo JrTexto::_('Representante de  la empresa Ej. Abel chingo') ?>">
                    </div>
                    <div class="col-12  form-group">
              			<label><?php echo JrTexto::_('Correo');?> <span class="required"> </span></label>              
                		<input type="email" id="correo<?php echo $idgui; ?>" name="correo"  class="form-control" value="<?php echo @$frm["correo"];?>"    placeholder="<?php echo JrTexto::_('Correo de la empresa o representante Ej. soyusuario@miempresa.com') ?>">
                    </div>                  
            	</div>
            </div>
            <div class="col-12 form-group text-center">
            	<hr>
              <button type="submit" class="btn btn-success" ><?php echo ucfirst(JrTexto::_('Continuar'));?> <i class=" fa fa-arrow-right"></i> </button>              
              <br><br>
            </div>
	    </div>
	</form>
</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
	$(document).ready(function(){
        $('.__subirfile').on('click',function(){
					$img=$(this);
          __subirfile({file:$img,typefile:'imagen',guardar:true,dirmedia:'empresa/'},function(rs){
						let f=rs.media.replace(url_media, '');
						$('input#logo').val(f);
					});
				});
				$('.btnremoveimage').on('click',function(){
					$(this).closest('.frmchangeimage').find('img').attr('src',_sysUrlBase_+'/static/media/nofoto.jpg');
					$('input#logo').val('');
					//$(this).closest('.frmchangeimage').find('input[type="file"]').remove();
					//$(this).closest('.frmchangeimage').find('.aquiseveimagen').append('<input name="borrarlogo" value="1" class="invisible">');
				})

		$('#frmempresa').bind({    
     		submit: function(ev){
     			ev.preventDefault();
					var tmpfrm=document.getElementById('frmempresa');					 
					var formData = new FormData(tmpfrm);
					var _sysajax={
						fromdata:formData,
						showmsjok:true,
						url:_sysUrlBase_+'/bolsa_empresas/guardarempresas',					
						callback:function(rs){
							var idempresa=rs.newid;								
							window.location=_sysUrlBase_+'/instalar/?idempresa='+idempresa;
						}
					}
					__sysajax(_sysajax);
					return false;
     		}
        });
    });
</script>