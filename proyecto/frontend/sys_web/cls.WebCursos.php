<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebCursos extends JrWeb
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function defecto(){
        try{
			global $aplicacion;
			$filtros=array();
			$idempresa=(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')?$_REQUEST["idempresa"]:0;
			$idproyecto=(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')?$_REQUEST["idproyecto"]:0;
			@session_start();
			if(!empty($idempresa))$_SESSION["idempresa"]=$idempresa;
			else $idempresa=@$_SESSION["idempresa"];
			if(!empty($idproyecto))$_SESSION["idproyecto"]=$idproyecto;
			else  $idproyecto=@$_SESSION["idproyecto"];
			$this->curusuario = NegSesion::getUsuario();			
			if($this->curusuario["idrol"]!=-1){
				if($idempresa!=$this->curusuario["idempresa"]){	throw new Exception(JrTexto::_('La empresa con la que esta logueda no es la correcta'));}			
				if($idproyecto!=$this->curusuario["idproyecto"]){throw new Exception(JrTexto::_('Al proyecto que usted accede no tiene permisos'));	}
            }
            $this->idempresa=$idempresa;
            $this->idproyecto=$idproyecto;
            $this->rol=$this->curusuario["idrol"];
            $this->documento->setTitulo(JrTexto::_('cursos'),true);
            $this->documento->plantilla ='proyecto/cursos';
            $this->esquema = 'smartcourse/curso'.(abs($this->rol));
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}			
	}

	//pasos para crear cursos

	public function crear(){
        try{
			global $aplicacion;
			$filtros=array();
			$idempresa=(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')?$_REQUEST["idempresa"]:0;
			$idproyecto=(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')?$_REQUEST["idproyecto"]:0;
			$this->idcurso=(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')?$_REQUEST["idcurso"]:0;
			$pg=(isset($_REQUEST["pg"])&&@$_REQUEST["pg"]!='')?$_REQUEST["pg"]:'paso1';
			@session_start();
			if(!empty($idempresa))$_SESSION["idempresa"]=$idempresa;
			else $idempresa=@$_SESSION["idempresa"];
			if(!empty($idproyecto))$_SESSION["idproyecto"]=$idproyecto;
			else  $idproyecto=@$_SESSION["idproyecto"];
			$this->curusuario = NegSesion::getUsuario();
			//var_dump($this->curusuario);		
			/*if($this->curusuario["idrol"]!=-1){
				if($idempresa!=$this->curusuario["idempresa"]){	throw new Exception(JrTexto::_('La empresa con la que esta logueda no es la correcta'));}			
				if($idproyecto!=$this->curusuario["idproyecto"]){throw new Exception(JrTexto::_('Al proyecto que usted accede no tiene permisos'));	}
            }*/
            $this->idempresa=$idempresa;
            $this->idproyecto=$idproyecto;
            $this->rol=$this->curusuario["idrol"];
            $this->documento->setTitulo(JrTexto::_('cursos'),true);
            $this->documento->plantilla ='proyecto/cursos';
            $this->esquema = 'smartcourse/curso_crear'.$pg;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}			
	}

}