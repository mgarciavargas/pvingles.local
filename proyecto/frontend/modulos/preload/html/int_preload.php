<link href="<?php echo $this->documento->getUrlTema()?>/css/preload.css" rel="stylesheet">
<style type="text/css">
  #loader { /*width: 350px;*/ }
  #loader span{
    color: #fff; 
    font-size: 4em;
    font-weight: bold;
    font-family: Helvetica;
    text-shadow: 0 1px 0 #ccc, 0 2px 0 #c9c9c9, 0 3px 0 #bbb, 0 4px 0 #b9b9b9, 0 5px 0 #aaa, 0 6px 1px rgba(0,0,0,.1), 0 0 5px rgba(0,0,0,.1), 0 1px 3px rgba(0,0,0,.3), 0 3px 5px rgba(0,0,0,.2), 0 5px 10px rgba(0,0,0,.25), 0 10px 10px rgba(0,0,0,.2), 0 20px 20px rgba(0,0,0,.15);
    /*animation: preloader_6 5s infinite linear;*/
    animation: preloader_6_span 1s infinite linear;

  }
   #loader span:nth-child(1){ color: #1e7e34; }
   #loader span:nth-child(2){ color: #ffc107; animation-delay: .2s;}
   #loader span:nth-child(3){ color: #007bff; animation-delay: .3s;}
   #loader span:nth-child(4){ animation-delay: .4s;}
   #loader span:nth-child(5){ animation-delay: .5s;}
   #loader span:nth-child(6){ animation-delay: .6s;}
   #loader span:nth-child(6){ animation-delay: .6s;}
   #loader span:nth-child(6){ animation-delay: .6s;}
   #loader span:nth-child(6){ animation-delay: .6s;}
   #loader span:nth-child(6){ animation-delay: .6s;}
   #loader span:nth-child(6){ animation-delay: .6s;}
   #loader span:nth-child(6){ animation-delay: .6s;}
  @keyframes preloader_6_span {
     0% { transform:scale(0.3); color: #1e7e34; }
     50% { transform:scale(1); color: #ffc107;}
     100% { transform:scale(1.5); color: #007bff;}
  }
</style>
<div id="loader-wrapper">
    <div id="loader">
           <!--span >Pro</span>
           <span >yec</span>
           <span >tos</span>
           <span>...</span-->
    	<img src="<?php echo $this->documento->getUrlStatic()?>/media/web/cargando.gif" class="img-fluid" style="max-width:300px; max-height:300px;">
    </div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<div class="modal fade" id="modalclone" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h5 class="modal-title" id="modaltitle" style="width: 100%"></h5>
        <button type="button" class="close cerrarmodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>       
      </div>
      <div class="modal-body" id="modalcontent">
        <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/loading.gif"><br><span style="color: #006E84;"><?php echo JrTexto::_("Loading");?></span></div>
      </div>
      <div class="modal-footer" id="modalfooter">        
        <button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
      </div>
    </div>
  </div>
</div>
<div id="cargando_clone" style="display: none;">
  <div class="col-12 text-center">
    <i class="fa fa-spinner fa-spin" style="font-size: 3em"></i>
    <div class="loader">
           <span>Ca</span>
           <span>rg</span>
           <span>an</span>
           <span>do</span>
           <span>...</span>      
    </div>
  </div>
</div>
<div style="padding:0px; margin: 0px; width: 1px; height: 1px; overflow: hidden;">
  <textarea id="copyallhtml" ></textarea>
  <div style="display:none">
    <div id="clone_progressup"  class="text-center" style="position: absolute; right: 1%; left: 1%; top:1%; bottom: 1%; background: #afaca99c;">
      <div style="position:relative; top: 5%;">
          <i class="fa fa-spinner fa-spin" style="font-size: 1em; color: #461d19;"></i>     
          <div class="progress">        
            <div id="progressavance" class=" progress-bar progress-bar-striped progress-bar-animated focus active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%">
              <div><?php echo JrTexto::_('loading').'...'; ?><span id="cantidadupload">0%</span></div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {    
    setTimeout(function(){
        $('body').addClass('loaded');
        //$('h1').css('color','#222222');
    }, 1000);    
});
</script>