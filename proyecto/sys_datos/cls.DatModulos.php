<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		13-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatModulos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM modulos";
			
			$cond = array();		
			
			if(isset($filtros["idmodulo"])) {
					$cond[] = "idmodulo = " . $this->oBD->escapar($filtros["idmodulo"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["icono"])) {
					$cond[] = "icono = " . $this->oBD->escapar($filtros["icono"]);
			}
			if(isset($filtros["link"])) {
					$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM modulos";			
			
			$cond = array();		
					
			
			if(isset($filtros["idmodulo"])) {
					$cond[] = "idmodulo = " . $this->oBD->escapar($filtros["idmodulo"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["icono"])) {
					$cond[] = "icono = " . $this->oBD->escapar($filtros["icono"]);
			}
			if(isset($filtros["link"])) {
					$cond[] = "link = " . $this->oBD->escapar($filtros["link"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$icono,$link,$estado)
	{
		try {
			
			$this->iniciarTransaccion('dat_modulos_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmodulo) FROM modulos");
			++$id;
			
			$estados = array('idmodulo' => $id
							
							,'nombre'=>$nombre
							,'icono'=>$icono
							,'link'=>$link
							,'estado'=>$estado							
							);
			
			$this->oBD->insert('modulos', $estados);			
			$this->terminarTransaccion('dat_modulos_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_modulos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$icono,$link,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_modulos_update');
			$estados = array('nombre'=>$nombre
							,'icono'=>$icono
							,'link'=>$link
							,'estado'=>$estado								
							);
			
			$this->oBD->update('modulos ', $estados, array('idmodulo' => $id));
		    $this->terminarTransaccion('dat_modulos_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM modulos  "
					. " WHERE idmodulo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('modulos', array('idmodulo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('modulos', array($propiedad => $valor), array('idmodulo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}
   
		
}