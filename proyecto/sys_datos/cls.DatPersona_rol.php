<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatPersona_rol extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM persona_rol";
			
			$cond = array();		
			
			if(isset($filtros["iddetalle"])) {
					$cond[] = "iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if(isset($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(isset($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			//$sql = "SELECT * FROM persona_rol";	
			$sql = "SELECT p.*, rol , p.idempresa , p.idproyecto, (SELECT idioma from proyecto PRO WHERE PRO.idproyecto=p.idproyecto) AS idioma FROM persona_rol p left Join roles r on  p.idrol=r.idrol ";
			
			$cond = array();				
			
			if(isset($filtros["iddetalle"])) {
					$cond[] = "p.iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if(isset($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(isset($filtros["idpersonal"])) {
					$cond[] = "p.idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "p.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "p.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idrol,$idpersonal,$idempresa,$idproyecto)
	{
		try {
			
			$this->iniciarTransaccion('dat_persona_rol_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM persona_rol");
			++$id;
			
			$estados = array('iddetalle' => $id
							
							,'idrol'=>$idrol
							,'idpersonal'=>$idpersonal
							,'idempresa'=>$idempresa
							,'idproyecto'=>$idproyecto							
							);
			
			$this->oBD->insert('persona_rol', $estados);			
			$this->terminarTransaccion('dat_persona_rol_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_persona_rol_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idrol,$idpersonal,$idempresa,$idproyecto)
	{
		try {
			$this->iniciarTransaccion('dat_persona_rol_update');
			$estados = array('idrol'=>$idrol
							,'idpersonal'=>$idpersonal
							,'idempresa'=>$idempresa
							,'idproyecto'=>$idproyecto								
							);
			
			$this->oBD->update('persona_rol ', $estados, array('iddetalle' => $id));
		    $this->terminarTransaccion('dat_persona_rol_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM persona_rol  "
					. " WHERE iddetalle = " . $this->oBD->escapar($id);
			//$sql = "SELECT p.*, rol FROM persona_rol p inner Join roles r on  p.idrol=r.idrol WHERE iddetalle = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('persona_rol', array('iddetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('persona_rol', array($propiedad => $valor), array('iddetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}
   
		
}