<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatPersonal extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT count(*) FROM personal pe left join persona_rol pr on idpersona=idpersonal";
			
			$cond = array();		
			
			if(isset($filtros["idpersona"])) {
				$cond[] = "(idpersona = " . $this->oBD->escapar($filtros["idpersona"])." OR md5(idpersona)=".$this->oBD->escapar($filtros["idpersona"]).")";
			}
			if(isset($filtros["tipodoc"])) {
					$cond[] = "tipodoc = " . $this->oBD->escapar($filtros["tipodoc"]);
			}
			if(isset($filtros["dni"])) {
					$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(isset($filtros["ape_paterno"])) {
					$cond[] = "ape_paterno = " . $this->oBD->escapar($filtros["ape_paterno"]);
			}
			if(isset($filtros["ape_materno"])) {
					$cond[] = "ape_materno = " . $this->oBD->escapar($filtros["ape_materno"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fechanac"])) {
					$cond[] = "fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if(isset($filtros["sexo"])) {
					$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(isset($filtros["estado_civil"])) {
					$cond[] = "estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if(isset($filtros["ubigeo"])) {
					$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if(isset($filtros["urbanizacion"])) {
					$cond[] = "urbanizacion = " . $this->oBD->escapar($filtros["urbanizacion"]);
			}
			if(isset($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["celular"])) {
					$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if(isset($filtros["email"])) {
					$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["idugel"])) {
					$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(isset($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["usuario"])) {
					$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["clave"])) {
					$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["token"])) {
					$cond[] = "token = " . $this->oBD->escapar($filtros["token"]);
			}
			if(isset($filtros["rol"])) {
					$cond[] = "rol = " . $this->oBD->escapar($filtros["rol"]);
			}			
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["situacion"])) {
					$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}
			if(isset($filtros["idioma"])) {
					$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}
			if(isset($filtros["tipousuario"])) {
					$cond[] = "tipousuario = " . $this->oBD->escapar($filtros["tipousuario"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT pe.*, idrol, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, 
			(SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil,
			tipousuario	FROM personal pe LEFT JOIN persona_rol pr ON idpersona=idpersonal";			
			
			$cond = array();		
			if(isset($filtros["idpersona"])) {
				$cond[] = "(idpersona = " . $this->oBD->escapar($filtros["idpersona"])." OR md5(idpersona)=".$this->oBD->escapar($filtros["idpersona"]).")";
			}
			if(isset($filtros["tipodoc"])) {
					$cond[] = "tipodoc = " . $this->oBD->escapar($filtros["tipodoc"]);
			}
			if(isset($filtros["dni"])) {
					$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(isset($filtros["ape_paterno"])) {
					$cond[] = "ape_paterno = " . $this->oBD->escapar($filtros["ape_paterno"]);
			}
			if(isset($filtros["ape_materno"])) {
					$cond[] = "ape_materno = " . $this->oBD->escapar($filtros["ape_materno"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fechanac"])) {
					$cond[] = "fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if(isset($filtros["sexo"])) {
					$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(isset($filtros["estado_civil"])) {
					$cond[] = "estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if(isset($filtros["ubigeo"])) {
					$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if(isset($filtros["urbanizacion"])) {
					$cond[] = "urbanizacion = " . $this->oBD->escapar($filtros["urbanizacion"]);
			}
			if(isset($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["celular"])) {
					$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if(isset($filtros["email"])) {
					$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["idugel"])) {
					$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(isset($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(isset($filtros["usuario"])) {
					$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["clave"])) {
					$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["token"])) {
					$cond[] = "token = " . $this->oBD->escapar($filtros["token"]);
			}
			
			
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["situacion"])) {
					$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}
			if(isset($filtros["idioma"])) {
					$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}
			if(isset($filtros["tipousuario"])) {
					$cond[] = "tipousuario = " . $this->oBD->escapar($filtros["tipousuario"]);
			}
			if(isset($filtros["rol"])) {
				$cond[] = " ( idrol = " . $this->oBD->escapar($filtros["rol"])." OR pe.rol=".$this->oBD->escapar($filtros["rol"]).") ";
			}

			if(isset($filtros["texto"])){
					$cond[] = " (dni='".$this->oBD->escapar($filtros["texto"])."' OR ape_paterno " . $this->oBD->like($filtros["texto"])." OR ape_materno " . $this->oBD->like($filtros["texto"])." OR nombre " . $this->oBD->like($filtros["texto"])." ) ";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}

	public function getxCredencial($usuario, $clave)
	{
		try {
			
			$sql = "SELECT pe.*, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil FROM personal pe "
					. " WHERE (usuario = " . $this->oBD->escapar($usuario)." OR email=". $this->oBD->escapar($usuario).") "
					. " AND clave = '" . md5($clave) . "' AND estado=1";
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}
	public function getxusuarioemail($usuema)
	{
		try {
			$sql = "SELECT pe.*, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil FROM personal pe "
					. " WHERE usuario = " . $this->oBD->escapar($usuema)." OR email = " . $this->oBD->escapar($usuema);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('user'). $e->getMessage());
		}
	}
	public function getxusuario($usuario)
	{
		try {
			$sql = "SELECT pe.*, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil FROM personal pe WHERE usuario = " . $this->oBD->escapar($usuario);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('users'). $e->getMessage());
		}
	}
	public function getxtoken($token)
	{
		try {
			$sql = "SELECT pe.*, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil FROM personal pe  WHERE token = " . $this->oBD->escapar($token);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('user').$e->getMessage());
		}
	}
	
	public function insertar($tipodoc,$dni,$ape_paterno,$ape_materno,$nombre,$fechanac,$sexo,$estado_civil,$ubigeo,$urbanizacion,$direccion,$telefono,$celular,$email,$idugel,$regusuario,$regfecha,$usuario,$clave,$token,$rol,$foto,$estado,$situacion,$idioma,$tipousuario)
	{
		try {
			
			$this->iniciarTransaccion('dat_personal_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersona) FROM personal");
			++$id;
			if(empty($usuario)){
				$n1=substr(str_replace(' ','',trim($nombre)),0,5);
				$n2=substr($ape_paterno,0,1);
				$usuario=ucfirst($n1.$n2.$dni);
				$clave=!empty($clave)?md5($clave):md5($usuario);
				$token=!empty($token)?md5($token):$clave;
			}
			
			$estados = array('idpersona' => $id
							
							,'tipodoc'=>$tipodoc
							,'dni'=>$dni
							,'ape_paterno'=>$ape_paterno
							,'ape_materno'=>$ape_materno
							,'nombre'=>$nombre
							,'fechanac'=>$fechanac
							,'sexo'=>$sexo
							,'estado_civil'=>$estado_civil
							,'ubigeo'=>$ubigeo
							,'urbanizacion'=>$urbanizacion
							,'direccion'=>$direccion
							,'telefono'=>$telefono
							,'celular'=>$celular
							,'email'=>$email
							,'idugel'=>$idugel
							,'regusuario'=>$regusuario
							,'regfecha'=>!empty($regfecha)?$regfecha:date('Y-m-d')
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'token'=>$token
							,'rol'=>!empty($rol)?$rol:0
							,'foto'=>$foto
							,'estado'=>!empty($estado)?$estado:1
							,'situacion'=>!empty($situacion)?$situacion:1
							,'idioma'=>!empty($idioma)?$idioma:'ES'
							,'tipousuario'=>!empty($tipousuario)?$tipousuario:'N'							
							);
			
			$this->oBD->insert('personal', $estados);			
			$this->terminarTransaccion('dat_personal_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_personal_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $tipodoc,$dni,$ape_paterno,$ape_materno,$nombre,$fechanac,$sexo,$estado_civil,$ubigeo,$urbanizacion,$direccion,$telefono,$celular,$email,$idugel,$regusuario,$regfecha,$usuario,$clave,$token,$rol,$foto,$estado,$situacion,$idioma,$tipousuario)
	{
		try {
			$this->iniciarTransaccion('dat_personal_update');
			$estados = array('tipodoc'=>$tipodoc
							,'dni'=>$dni
							,'ape_paterno'=>$ape_paterno
							,'ape_materno'=>$ape_materno
							,'nombre'=>$nombre
							,'fechanac'=>$fechanac
							,'sexo'=>$sexo
							,'estado_civil'=>$estado_civil
							,'ubigeo'=>$ubigeo
							,'urbanizacion'=>$urbanizacion
							,'direccion'=>$direccion
							,'telefono'=>$telefono
							,'celular'=>$celular
							,'email'=>$email
							,'idugel'=>$idugel
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'token'=>$token
							,'rol'=>$rol
							,'foto'=>$foto
							,'estado'=>$estado
							,'situacion'=>$situacion
							,'idioma'=>$idioma
							,'tipousuario'=>$tipousuario								
							);
			
			$this->oBD->update('personal ', $estados, array('idpersona' => $id));
		    $this->terminarTransaccion('dat_personal_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT pe.*, (SELECT nombre from general g WHERE pe.tipodoc=g.codigo AND tipo_tabla='tipodocidentidad') AS strtipodoc, 
			(SELECT nombre from general g WHERE pe.sexo=g.codigo AND tipo_tabla='sexo') AS strsexo, (SELECT nombre from general g WHERE pe.estado_civil=g.codigo AND tipo_tabla='estadocivil') AS strestado_civil FROM personal pe WHERE idpersona = " . $this->oBD->escapar($id)." OR md5(idpersona)=".$this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('personal', array('idpersona' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('personal', array($propiedad => $valor), array('idpersona' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
   
		
}