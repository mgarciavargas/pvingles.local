<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatProyecto_cursos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Proyecto_cursos").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM proyecto_cursos";			
			$cond = array();					
			if(isset($filtros["idproycurso"])) {
					$cond[] = "idproycurso = " . $this->oBD->escapar($filtros["idproycurso"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Proyecto_cursos").": " . $e->getMessage());
		}
	}

	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM proyecto_cursos py INNER JOIN acad_curso ac ON py.idcurso=ac.idcurso";				
			$cond = array();			
			if(isset($filtros["idproycurso"])) {
					$cond[] = "idproycurso = " . $this->oBD->escapar($filtros["idproycurso"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "py.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Proyecto_cursos").": " . $e->getMessage());
		}
	}
	
	public function insertar($idproyecto,$idcurso)
	{
		try{			
			$this->iniciarTransaccion('dat_proyecto_cursos_insert');
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idproycurso) FROM proyecto_cursos");
			++$id;
			
			$estados = array('idproycurso' => $id
							,'idproyecto'=>$idproyecto
							,'idcurso'=>$idcurso							
							);
			$this->oBD->insert('proyecto_cursos', $estados);			
			$this->terminarTransaccion('dat_proyecto_cursos_insert');			
			return $id;
		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_proyecto_cursos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Proyecto_cursos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idproyecto,$idcurso)
	{
		try {
			$this->iniciarTransaccion('dat_proyecto_cursos_update');
			$estados = array('idproyecto'=>$idproyecto
							,'idcurso'=>$idcurso								
							);
			
			$this->oBD->update('proyecto_cursos ', $estados, array('idproycurso' => $id));
		    $this->terminarTransaccion('dat_proyecto_cursos_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Proyecto_cursos").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM proyecto_cursos  "
					. " WHERE idproycurso = " . $this->oBD->escapar($id);
			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Proyecto_cursos").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('proyecto_cursos', array('idproycurso' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Proyecto_cursos").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('proyecto_cursos', array($propiedad => $valor), array('idproycurso' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Proyecto_cursos").": " . $e->getMessage());
		}
	}
}