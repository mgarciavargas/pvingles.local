<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		07-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatProyecto extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM proyecto";
			
			$cond = array();		
			
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}			
			if(isset($filtros["jsonlogin"])) {
					$cond[] = "jsonlogin = " . $this->oBD->escapar($filtros["jsonlogin"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM proyecto";			
			
			$cond = array();		
					
			
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			
			if(isset($filtros["jsonlogin"])) {
					$cond[] = "jsonlogin = " . $this->oBD->escapar($filtros["jsonlogin"]);
			}
			if(isset($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idempresa,$jsonlogin,$fecha)
	{
		try {
			
			$this->iniciarTransaccion('dat_proyecto_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idproyecto) FROM proyecto");
			++$id;
			
			$estados = array('idproyecto' => $id							
							,'idempresa'=>$idempresa							
							,'jsonlogin'=>$jsonlogin
							,'fecha'=>$fecha							
							);
			
			$this->oBD->insert('proyecto', $estados);			
			$this->terminarTransaccion('dat_proyecto_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_proyecto_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idempresa,$jsonlogin,$fecha)
	{
		try {
			$this->iniciarTransaccion('dat_proyecto_update');
			$estados = array('idempresa'=>$idempresa							
							,'jsonlogin'=>$jsonlogin
							,'fecha'=>$fecha								
							);
			
			$this->oBD->update('proyecto ', $estados, array('idproyecto' => $id));
		    $this->terminarTransaccion('dat_proyecto_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM proyecto  "
					. " WHERE idproyecto = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('proyecto', array('idproyecto' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('proyecto', array($propiedad => $valor), array('idproyecto' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Proyecto").": " . $e->getMessage());
		}
	}
   
		
}