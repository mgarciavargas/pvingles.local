<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAmbiente extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Ambiente").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM ambiente";
			
			$cond = array();		
			
			if(isset($filtros["idambiente"])) {
					$cond[] = "idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["numero"])) {
					$cond[] = "numero = " . $this->oBD->escapar($filtros["numero"]);
			}
			if(isset($filtros["capacidad"])) {
					$cond[] = "capacidad = " . $this->oBD->escapar($filtros["capacidad"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["turno"])) {
					$cond[] = "turno = " . $this->oBD->escapar($filtros["turno"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Ambiente").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM ambiente";			
			
			$cond = array();		
					
			
			if(isset($filtros["idambiente"])) {
					$cond[] = "idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["numero"])) {
					$cond[] = "numero = " . $this->oBD->escapar($filtros["numero"]);
			}
			if(isset($filtros["capacidad"])) {
					$cond[] = "capacidad = " . $this->oBD->escapar($filtros["capacidad"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["turno"])) {
					$cond[] = "turno = " . $this->oBD->escapar($filtros["turno"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Ambiente").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idlocal,$numero,$capacidad,$tipo,$estado,$turno)
	{
		try {
			
			$this->iniciarTransaccion('dat_ambiente_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idambiente) FROM ambiente");
			++$id;
			
			$estados = array('idambiente' => $id
							
							,'idlocal'=>$idlocal
							,'numero'=>$numero
							,'capacidad'=>$capacidad
							,'tipo'=>$tipo
							,'estado'=>$estado
							,'turno'=>$turno							
							);
			
			$this->oBD->insert('ambiente', $estados);			
			$this->terminarTransaccion('dat_ambiente_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_ambiente_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Ambiente").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idlocal,$numero,$capacidad,$tipo,$estado,$turno)
	{
		try {
			$this->iniciarTransaccion('dat_ambiente_update');
			$estados = array('idlocal'=>$idlocal
							,'numero'=>$numero
							,'capacidad'=>$capacidad
							,'tipo'=>$tipo
							,'estado'=>$estado
							,'turno'=>$turno								
							);
			
			$this->oBD->update('ambiente ', $estados, array('idambiente' => $id));
		    $this->terminarTransaccion('dat_ambiente_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Ambiente").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT tb1.*,tb2.nombre AS _nombre  FROM ambiente tb1 LEFT JOIN local tb2 ON tb1.idlocal=tb2.idlocal  "
					. " WHERE tb1.idambiente = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Ambiente").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('ambiente', array('idambiente' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Ambiente").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('ambiente', array($propiedad => $valor), array('idambiente' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Ambiente").": " . $e->getMessage());
		}
	}
   
		
}