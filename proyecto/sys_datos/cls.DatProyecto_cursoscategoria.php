<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatProyecto_cursoscategoria extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Proyecto_cursoscategoria").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM proyecto_cursoscategoria";
			
			$cond = array();		
			
			if(isset($filtros["idproycursocat"])) {
					$cond[] = "idproycursocat = " . $this->oBD->escapar($filtros["idproycursocat"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["categoria"])) {
					$cond[] = "categoria = " . $this->oBD->escapar($filtros["categoria"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Proyecto_cursoscategoria").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM proyecto_cursoscategoria";			
			
			$cond = array();		
					
			
			if(isset($filtros["idproycursocat"])) {
					$cond[] = "idproycursocat = " . $this->oBD->escapar($filtros["idproycursocat"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["categoria"])) {
					$cond[] = "categoria = " . $this->oBD->escapar($filtros["categoria"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Proyecto_cursoscategoria").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idproyecto,$idcurso,$categoria)
	{
		try {
			
			$this->iniciarTransaccion('dat_proyecto_cursoscategoria_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idproycursocat) FROM proyecto_cursoscategoria");
			++$id;
			
			$estados = array('idproycursocat' => $id
							
							,'idproyecto'=>$idproyecto
							,'idcurso'=>$idcurso
							,'categoria'=>$categoria							
							);
			
			$this->oBD->insert('proyecto_cursoscategoria', $estados);			
			$this->terminarTransaccion('dat_proyecto_cursoscategoria_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_proyecto_cursoscategoria_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Proyecto_cursoscategoria").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idproyecto,$idcurso,$categoria)
	{
		try {
			$this->iniciarTransaccion('dat_proyecto_cursoscategoria_update');
			$estados = array('idproyecto'=>$idproyecto
							,'idcurso'=>$idcurso
							,'categoria'=>$categoria								
							);
			
			$this->oBD->update('proyecto_cursoscategoria ', $estados, array('idproycursocat' => $id));
		    $this->terminarTransaccion('dat_proyecto_cursoscategoria_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Proyecto_cursoscategoria").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM proyecto_cursoscategoria  "
					. " WHERE idproycursocat = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Proyecto_cursoscategoria").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('proyecto_cursoscategoria', array('idproycursocat' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Proyecto_cursoscategoria").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('proyecto_cursoscategoria', array($propiedad => $valor), array('idproycursocat' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Proyecto_cursoscategoria").": " . $e->getMessage());
		}
	}
   
		
}