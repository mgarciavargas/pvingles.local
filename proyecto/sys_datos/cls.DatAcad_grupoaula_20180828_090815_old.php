<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_grupoaula extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_grupoaula";
			
			$cond = array();		
			
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["comentario"])) {
					$cond[] = "comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if(isset($filtros["nvacantes"])) {
					$cond[] = "nvacantes = " . $this->oBD->escapar($filtros["nvacantes"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_grupoaula";			
			
			$cond = array();		
					
			
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["comentario"])) {
					$cond[] = "comentario = " . $this->oBD->escapar($filtros["comentario"]);
			}
			if(isset($filtros["nvacantes"])) {
					$cond[] = "nvacantes = " . $this->oBD->escapar($filtros["nvacantes"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$tipo,$comentario,$nvacantes,$estado)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_grupoaula_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupoaula) FROM acad_grupoaula");
			++$id;
			
			$estados = array('idgrupoaula' => $id
							
							,'nombre'=>$nombre
							,'tipo'=>$tipo
							,'comentario'=>$comentario
							,'nvacantes'=>$nvacantes
							,'estado'=>$estado							
							);
			
			$this->oBD->insert('acad_grupoaula', $estados);			
			$this->terminarTransaccion('dat_acad_grupoaula_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_grupoaula_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$tipo,$comentario,$nvacantes,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_acad_grupoaula_update');
			$estados = array('nombre'=>$nombre
							,'tipo'=>$tipo
							,'comentario'=>$comentario
							,'nvacantes'=>$nvacantes
							,'estado'=>$estado								
							);
			
			$this->oBD->update('acad_grupoaula ', $estados, array('idgrupoaula' => $id));
		    $this->terminarTransaccion('dat_acad_grupoaula_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_grupoaula  "
					. " WHERE idgrupoaula = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_grupoaula', array('idgrupoaula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_grupoaula', array($propiedad => $valor), array('idgrupoaula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_grupoaula").": " . $e->getMessage());
		}
	}
   
		
}