<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_grupoauladetalle extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_grupoauladetalle";
			
			$cond = array();		
			
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
					$cond[] = "idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_grupoauladetalle";			
			
			$cond = array();		
					
			
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["idambiente"])) {
					$cond[] = "idambiente = " . $this->oBD->escapar($filtros["idambiente"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idgrupoaula,$idcurso,$iddocente,$idlocal,$idambiente,$nombre,$fecha_inicio,$fecha_final)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_grupoauladetalle_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idgrupoauladetalle) FROM acad_grupoauladetalle");
			++$id;
			
			$estados = array('idgrupoauladetalle' => $id
							
							,'idgrupoaula'=>$idgrupoaula
							,'idcurso'=>$idcurso
							,'iddocente'=>$iddocente
							,'idlocal'=>$idlocal
							,'idambiente'=>$idambiente
							,'nombre'=>$nombre
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final							
							);
			
			$this->oBD->insert('acad_grupoauladetalle', $estados);			
			$this->terminarTransaccion('dat_acad_grupoauladetalle_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_grupoauladetalle_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idgrupoaula,$idcurso,$iddocente,$idlocal,$idambiente,$nombre,$fecha_inicio,$fecha_final)
	{
		try {
			$this->iniciarTransaccion('dat_acad_grupoauladetalle_update');
			$estados = array('idgrupoaula'=>$idgrupoaula
							,'idcurso'=>$idcurso
							,'iddocente'=>$iddocente
							,'idlocal'=>$idlocal
							,'idambiente'=>$idambiente
							,'nombre'=>$nombre
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final								
							);
			
			$this->oBD->update('acad_grupoauladetalle ', $estados, array('idgrupoauladetalle' => $id));
		    $this->terminarTransaccion('dat_acad_grupoauladetalle_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_grupoauladetalle  "
					. " WHERE idgrupoauladetalle = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_grupoauladetalle', array('idgrupoauladetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_grupoauladetalle', array($propiedad => $valor), array('idgrupoauladetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_grupoauladetalle").": " . $e->getMessage());
		}
	}
   
		
}