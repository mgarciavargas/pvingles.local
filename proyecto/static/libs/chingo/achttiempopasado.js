/*
Autor: abel chingo tello
require jquery y moment
muestra cuanto tiempo ha pasado desde una fecha
*/
$.fn.achttiempopasado= function(opt){
return this.each(function(){		 
    var el = $(this);
    var op=opt||{};
    var sy=op.start||false;    
    var dn=op.donde||el.attr('data-donde')||'';
    if(dn==''){if(el.is('input')){dn='value';}else{dn='text';}};
    var time=1000;
    var td=op.tiempodesde||el.val()||el.text()||moment().format('YYYY-MM-DD HH:mm:ss'); //tiempo desde
    var ta=op.tiempoactual||el.attr('data-tactual')||moment().format('YYYY-MM-DD HH:mm:ss');
    td=moment(td,'YYYY-MM-DD HH:mm:ss');
    ta=moment(ta,'YYYY-MM-DD HH:mm:ss');
    var tc;
    var asignarfecha=function(rt){
    	if(dn=='value') el.val(rt);
        else if(dn=='text') el.text(rt);
        else if(dn=='') el.attr('data-time',rt);
        else el.attr(dn,rt);
    }
    var iniciar=function(){        
    	var tdm=td; //tiempo desde
        var tam=ta; // tiempo actual
    	var rtc=false;
    	var cb=false;
        if(tdm>tam){ 
        	asignarfecha(td.format('YYYY-MM-DD'));
        	return false;
        } 
        tc=setInterval(function(){
            tam=moment(ta,'YYYY-MM-DD HH:mm:ss');
            var df_mes=df_dias=0;
            var rt='000';
            var df_years=tam.diff(tdm,'years');
            if(df_years>0) rtc=true;
            else{
            	df_mes=tam.diff(tdm,'months');
            	if(df_mes>0) rtc=true;
            	else{
            		df_dias=tam.diff(tdm,'days');
            		if(df_dias>31) rtc=true;
            		else{
            			if(df_dias>1) {rt='Hace '+df_dias+' dias'; rtc=true}
            			else if(df_dias==1) {rt='Hace '+df_dias+' dia'; rtc=true;}
            			else{
            				df_horas=tam.diff(tdm,'hours');
            				if(df_horas>0){
            					if(df_horas==1)rt='Hace 1 Hora';
            					else rt='Hace '+df_horas+' Horas';
            					rtc:true;
            				}else{
            					df_min=tam.diff(tdm,'minutes');
                    			df_seg=tam.diff(tdm,'seconds');
                    			if(df_min>=1){
			                        if(df_min==1) rt='Hace 1 minuto';
			                        else rt='Hace '+df_min+' minutos';
			                    }else rt='Hace '+df_seg+' segundos';
			                    ta=tam.add(1,'seconds').format('YYYY-MM-DD HH:mm:ss');
			                    rtc=false;
            				}
            			}
            		}
            	}
            }
            if(rtc==true){
            	if(rt=='000') rt=tdm.format('YYYY-MM-DD');
            	clearInterval(tc);
            }            
            asignarfecha(rt);
        },time);        
    }
    iniciar();
});
};