var modal_id = 0;
var __stopmedios=function(c){
	if(c.length){
		var a=$(c.find('audio')); if(a.length) a.trigger('pause');
	    var v=$(c.find('video')); if(v.length) v.trigger('pause');
	}
	return true;
}
var __cerrarmodal=function(_md,sb){
	if(_md.length){
    	__stopmedios(_md);
		_md.modal('hide');
		if(sb)_md.on('hidden.bs.modal', function(){_md.remove();});		
	}		
}

function agregar_msj_interno(tipo, mensaje) {
	html = '<div class="alert alert-' + tipo + '" role="alert">'
			+ '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> &nbsp;'
			+ '<span class="sr-only">Error: </span>' + mensaje
			+ '</div>';
	
	$('#msj-interno').empty().html(html);
}


function openModal(tam,titulo,url, destruir,clase,form){	
	return __sysmodal({tam:tam,titulo:titulo,url:url,borrar:destruir,clase:clase});
}

var __sysmodal=function(infodata){
	var dt=infodata||{};
	var sh=dt.header||false;
	var sf=dt.footer||false;
	var sb=dt.borrar||true;
	var tm=dt.tam||'lg';
	var mw=dt.modalancho||'';
	var cl=dt.clase||'';
	var ur=dt.url||'';
	var ht=dt.html||'';
	var mt=dt.method||'POST';
	var fd=dt.fromdata||{};
	var bd=dt.backdrop||'static';
	var kb=dt.keyboard||true;
	var ti=dt.titulo||'';
	var fu=dt.func||'';	
	var _md = $('#modalclone').clone(true);
	if(!sh) _md.find('.modal-header').hide();
	if(!sf) _md.find('.modal-footer').hide();
	_md.attr('id', 'modal-' + modal_id);
    _md.addClass(cl);
    _md.children('.modal-dialog').removeClass('modal-lg modal-sm').addClass('modal-'+tm)
    if(mw!='')_md.children('.modal-dialog').css('width',mw);    
    _md.modal({backdrop: bd, keyboard: kb});    
    $('body').on('click', '#modal-' + modal_id + ' .close' , function (ev){ __cerrarmodal(_md , sb)});
    $('body').on('click', '#modal-' + modal_id + ' .cerrarmodal' , function (ev){ __cerrarmodal(_md , sb)});
    ++modal_id;
    _md.find('#modaltitle').html(ti);
    if(ur!=''){
    	__sysajax({
    		url:ur,
    		method:mt,
    		fromdata:fd,
    		processData: false,
    		type:'html',
    		callback:function(rs){    			
				_md.find('#modalcontent').html(rs);
				if(__isFunction(fu))fu();
    		}
    	});
	}else if(ht!='') _md.find('#modalcontent').html(ht);
    return _md;
}

var __isFile=function(url){
  	url=url.substr(1 + url.lastIndexOf("/"));
  	let index=url.indexOf('?');
  	if(index>-1) url=url.substr(0,index);
  	if(url==''||url=='undefined'||url==undefined) return false;
	return url.lastIndexOf(".")==-1?false:true;	 
}

var __isBase64=function(str) {
    try {
        return btoa(atob(str)) == str;
    } catch (err) {
        return false;
    }
}

var __isFileExist=function(url,fn){
	$.get(url).done(function() { 
        fn(true); 
    }).fail(function(){ 
        fn(false);
    })
}

var __sysfileExists=function(url){
	if(!__isFile(url)) return false;
    var http = new XMLHttpRequest();
    http.open('HEAD', url, true);
	http.send();
	return http.status!=404?true:false;
}

var __isFunction=function(o){ if (typeof o!= 'function') return false; else return true;}
var __idgui=function(){	return Date.now()+(Math.floor(Math.random() * (1 - 100)) + 1);}
var __isdom=function(e){ try{ if(e.length) return true; else return false;}catch(er){return false}}
var __isEmpty=function(e){return !$.trim(e.html());}
var __isJson=function(s){try{JSON.parse(s);}catch(e){return false;}return true;}
var __formdata=function(frmele){
	var tmpfrm='';
	if(frmele!='') var tmpfrm=document.getElementById(frmele);
	else tmpfrm='';
	var dt=new FormData(tmpfrm);
	dt.append('idioma',_sysIdioma_||'ES');
	return dt;
}

var __formDataToJSON=function (frmele){
	var fd=__formdata(frmele);
	convertedJSON = {};
    fd.forEach(function(value, key){ convertedJSON[key] = value; });
    convertedJSON.idioma=_sysIdioma_;    
	return convertedJSON;
}

var __copyhtml=function(html){
	let copyhtml = document.querySelector('#copyallhtml');
	copyhtml.value=html;
	copyhtml.select();	
	rs=document.execCommand("copy");
	copyhtml.value='';
}

var __initEditor=function(tmpid,mplugins){
	var mplugins=mplugins||'';	
	tinymce.init({
	  	relative_urls : false,
	    remove_script_host : false,
	    convert_urls : true,
	    convert_newlines_to_brs : true,
	    menubar: false,
	    //statusbar: false,
	    verify_html : false,
	    //content_css : [_sysUrlBase_+'/static/tema/css/bootstrap.min.css'],
	    selector: '#'+tmpid,
	    height: 210,
	    paste_auto_cleanup_on_paste : true,
	    paste_preprocess : function(pl, o) {
	        var html='<div>'+o.content+'</div>';
	        var txt =$(html).text();
	        o.content = txt;
	        },
	    paste_postprocess : function(pl, o) {
	        o.node.innerHTML = o.node.innerHTML;
	        o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
	        },
	    plugins:[mplugins+" textcolor paste lists advlist" ],  // chingoinput chingoimage chingoaudio chingovideo
	    toolbar: ' | undo redo '+mplugins+' | styleselect | removeformat |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor fontsizeselect| ', //chingoinput chingoimage chingoaudio chingovideo,
	    advlist_number_styles: "default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman",
	});
}


var animacssin=["bounceIn","bounceInDown","bounceInLeft","bounceInRight","bounceInUp","fadeIn","fadeInDown","fadeInDownBig","fadeInLeft","fadeInLeftBig","fadeInRight","fadeInRightBig","fadeInUp","fadeInUpBig","flip","flipInX","flipInY","lightSpeedIn","rotateIn","rotateInDownLeft","rotateInDownRight","rotateInUpLeft","rotateInUpRight","slideInUp","slideInDown","slideInLeft","slideInRight","zoomIn","zoomInDown","zoomInLeft","zoomInRight","zoomInUp","jackInTheBox","rollIn"]; //,"hinge"
var animacssout=["bounceOut","bounceOutDown","bounceOutLeft","bounceOutRight","bounceOutUp","fadeOut","fadeOutDown","fadeOutDownBig","fadeOutLeft","fadeOutLeftBig","fadeOutRight","fadeOutRightBig","fadeOutUp","fadeOutUpBig","flipOutX","flipOutY","lightSpeedOut","rotateOut","rotateOutDownLeft","rotateOutDownRight","rotateOutUpLeft","rotateOutUpRight","slideOutUp","slideOutDown","slideOutLeft","slideOutRight","zoomOut","zoomOutDown","zoomOutLeft","zoomOutRight","zoomOutUp","rollOut"];
var animaccsenf=["bounce","flash","pulse","rubberBand","shake","swing","tada","wobble","jello"];

$.fn.extend({
    animateCss: function(dt){   
        var dtani=dt||{};
        var aninombre=dt.animationName||'';
        var de=dt.de||'in';
        var callback=dt.callback||'';
        var hide=dt.hide||false;
    	if(aninombre==''){
    		if(de=='out') { 
    			var l=animacssout.length-1; 
    			var index=Math.floor(Math.random() * l);
    			aninombre=animacssout[index];
    		}else if(de=='enf'){
    			var l=animacssenf.length-1;
    			var index=Math.floor(Math.random() * l);
    			aninombre=animacssenf[index];
    		}else{    			
    			var l=animacssin.length-1;
    			var index=Math.floor(Math.random() * l);
    			aninombre=animacssin[index];
    		}
    	}
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + aninombre).one(animationEnd, function(){ 
            $(this).removeClass('animated ' + aninombre);            
            if(__isFunction(callback))callback();            
        });
        return this;
    }
});

var __sysajax=function(dt){
	try{
		let opt={donde:false,url:false,type:'json',mostrarcargando:false,method:'POST',fromdata:{},msjatencion:''};
		$.extend(opt,dt);		
		var datasend=dts=opt.fromdata;
		var showmsjok=opt.showmsjok==false?false:true;		
		var callback=opt.callback||false;
		var callbackerror=opt.callbackerror||false;

		var typesend=(dts.constructor.name||'object').toString().toLowerCase();
		var _prd=(typesend=="formdata")?false:true;
		var $progressavance=$(opt.iduploadtmp).length?$(opt.iduploadtmp+' #progressavance '):'';		
		var $divuploadp=$progressavance.length?$progressavance.find('span#cantidadupload'):'';
		var $iduploadtmp=$(opt.iduploadtmp)||'';
		var hayprogress=$progressavance.length?true:false;
		let _donde=opt.donde||false;

		//console.log(mostrarcargando,_donde);
		if(!opt.url) return;
		$.ajax({
		  url: opt.url,
		  type: opt.method,
		  dataType:opt.type,
		  data:  datasend,
		  processData: _prd,
		  contentType: _prd==true?'application/x-www-form-urlencoded; charset=UTF-8':false, 
		  cache: false,
		  xhr:function(){
		  	var xhr = new window.XMLHttpRequest();
		  	try{
	         	xhr.upload.addEventListener("progress", function(evt){ //Upload progress
		          if (evt.lengthComputable && hayprogress){
		            var pc = Math.floor((evt.loaded*100) / evt.total);	           
		            	$progressavance.width(pc+'%');
		            	$divuploadp.text(pc+'%');
		          }
		        }, false);
		        
		        xhr.addEventListener("progress", function(evt){//Download progress
		          if (evt.lengthComputable) {
		            var pc =  Math.floor((evt.loaded*100) / evt.total);
		            //console.log(percentComplete);
		          }
		        }, false);
            }catch(er){
            	console.log(er);
        	}
	        return xhr;
	      },		  		  
		  beforeSend: function(XMLHttpRequest){ 		  	
		  	if((_donde!=false&& opt.type!='json')&&opt.mostrarcargando==true){
		  		$cl=$('#cargando_clone').clone();
		  		$cl.removeAttr('style');
		  		_donde.html($cl.html()); 
		  	}
		  },
		  success: function(data)
		  { if(opt.type=='json'){
		        if(data.code=='Error'){
		          mostrar_notificacion(opt.msjatencion,data.msj,'warning');
		          if(__isFunction(callbackerror))callbackerror(data);
		        }else{
		          if(showmsjok) mostrar_notificacion(opt.msjatencion,data.msj,'success');
		          if(__isFunction(callback))callback(data);
		        }
		    }else{
		    	if(__isFunction(callback))callback(data);
		    }
		    if($iduploadtmp.length) $iduploadtmp.remove();
		  },
		  error: function(er){ console.log(er); if($iduploadtmp.length) $iduploadtmp.remove();},
		  complete: function(xhr){ if($iduploadtmp.length) $iduploadtmp.remove(); }
		});
	}catch(err) {
    	console.log(err.message);
    	return false;
	}
}

function _isFunction(object){ if (typeof object != 'function') return false; else return true;}

var __subirfile=function(obj,fn){
	let opt={file:'',typefile:'',guardar:false,dirmedia:'',oldmedia:'',uploadtmp:false,fntmp:'h'};
	$.extend(opt,obj);
	let mostraren=__isdom(opt.file)?opt.file:false;
	let acepta='';
	if(opt.typefile=='imagen'){acepta='image/x-png, image/gif, image/jpeg, image/*'; opt.uploadtmp=true;}
	else if(opt.typefile=='audio') acepta='audio/mp3';
	else if(opt.typefile=='video') acepta='video/mp4, video/*';
	else if(opt.typefile=='html') acepta='.html , .htm , application/zip';
	else if(opt.typefile=='flash') acepta='.swf , .flv  , application/zip';
	else if(opt.typefile=='pdf') acepta='application/pdf , application/zip';

	let file=document.createElement('input');
		file.id='file_'+__idgui();		
		file.type='file';
		file.accept=acepta;
		file.addEventListener('change',function(ev){			
			if(opt.uploadtmp && mostraren){
				let rd = new FileReader();
				rd.onload = function(filetmp){					
					let filelocal = filetmp.target.result;
					mostraren.attr('src',filelocal);
					let fntm=opt.fntmp;
					if(__isFunction(fntm))fntm(file);
				}
				rd.readAsDataURL(this.files[0]);
			}
			if(opt.guardar){
				let iduploadtmp='';	
				if(mostraren){
					let $idprogress=$('#clone_progressup').clone(true);
					iduploadtmp='idup'+__idgui();
					$idprogress.attr('id',iduploadtmp);
					mostraren.closest('div').append($idprogress);
					opt.oldmedia=mostraren.attr('oldmedia')||'';
					iduploadtmp='#'+iduploadtmp;
				}
				var data=new FormData()
				data.append('media',this.files[0]);
				data.append('dirmedia',opt.dirmedia);
				data.append('oldmedia',opt.oldmedia);
				data.append('typefile',opt.typefile);
				__sysajax({ 
					fromdata:data,
		        	url:_sysUrlBase_+'/media/subir',
		        	iduploadtmp:iduploadtmp ,
		        	callback:function(rs){
		        		if(rs.code=='ok'){
		        			if(rs.media!=''){
		        				if(mostraren){
		        					mostraren.attr('src',rs.media);
		        					mostraren.attr('data-olmedia',rs.media);
		        				}
		        				if(__isFunction(fn))fn(rs);
		        			} 
		        		}
		        	}
		    	});
			}
		});
    file.click();
}

function mostrar_notificacion(t, m, ti) {
	new PNotify({title: t, text: m, type: ti, hide: true});
}
var __addjs=function(id,ruta,fn){
	try{
		if(ruta!=''){
			var script = document.getElementById(id);
			if(script==null) script=document.createElement("script");
			console.log(script);
			script.type = "text/javascript";
			script.src = ruta;
			script.id=id
			script.onload = function(){	if(__isFunction(fn))fn();};
			document.getElementsByTagName('head')[0].appendChild(script);
		}
	}catch(ex){}
}
function _sysfileExists(url){
	if(!_sysisFile(url)) return false;
    var http = new XMLHttpRequest();
    http.open('HEAD', url, true);
    http.send();
    if(http.status!=404) return true;
    return false;
}
function _sysisFile(url){
	url=url.substr(1 + url.lastIndexOf("/"));
	index=url.indexOf('?');
	if(index>-1) url=url.substr(0,index);
	if(url==''||url=='undefined'||url==undefined) return false;
	indexpunto=url.lastIndexOf(".");
	if(indexpunto==-1) return false;
	return true;  
}