<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		27-01-2018
 * @copyright	Copyright (C) 27-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBolsa_postulante', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBolsa_postulante 
{
	protected $idpostular;
	protected $fecharegistro;
	protected $nombrecompleto;
	protected $telefono;
	protected $correo;
	protected $descripcion;
	protected $mostrar;
	protected $idpublicacion;
	protected $idpostulante;	
	protected $dataBolsa_postulante;
	protected $oDatBolsa_postulante;	

	public function __construct()
	{
		$this->oDatBolsa_postulante = new DatBolsa_postulante;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBolsa_postulante->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBolsa_postulante->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBolsa_postulante->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBolsa_postulante->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBolsa_postulante->get($this->idpostular);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bolsa_postulante', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatBolsa_postulante->iniciarTransaccion('neg_i_Bolsa_postulante');
			$this->idpostular = $this->oDatBolsa_postulante->insertar($this->fecharegistro,$this->nombrecompleto,$this->telefono,$this->correo,$this->descripcion,$this->mostrar,$this->idpublicacion,$this->idpostulante);
			$this->oDatBolsa_postulante->terminarTransaccion('neg_i_Bolsa_postulante');	
			return $this->idpostular;
		} catch(Exception $e) {	
		    $this->oDatBolsa_postulante->cancelarTransaccion('neg_i_Bolsa_postulante');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bolsa_postulante', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatBolsa_postulante->actualizar($this->idpostular,$this->fecharegistro,$this->nombrecompleto,$this->telefono,$this->correo,$this->descripcion,$this->mostrar,$this->idpublicacion,$this->idpostulante);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bolsa_postulante', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBolsa_postulante->eliminar($this->idpostular);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpostular($pk){
		try {
			$this->dataBolsa_postulante = $this->oDatBolsa_postulante->get($pk);
			if(empty($this->dataBolsa_postulante)) {
				throw new Exception(JrTexto::_("Bolsa_postulante").' '.JrTexto::_("not registered"));
			}
			$this->idpostular = $this->dataBolsa_postulante["idpostular"];
			$this->fecharegistro = $this->dataBolsa_postulante["fecharegistro"];
			$this->nombrecompleto = $this->dataBolsa_postulante["nombrecompleto"];
			$this->telefono = $this->dataBolsa_postulante["telefono"];
			$this->correo = $this->dataBolsa_postulante["correo"];
			$this->descripcion = $this->dataBolsa_postulante["descripcion"];
			$this->mostrar = $this->dataBolsa_postulante["mostrar"];
			$this->idpublicacion = $this->dataBolsa_postulante["idpublicacion"];
			$this->idpostulante = $this->dataBolsa_postulante["idpostulante"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bolsa_postulante', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBolsa_postulante = $this->oDatBolsa_postulante->get($pk);
			if(empty($this->dataBolsa_postulante)) {
				throw new Exception(JrTexto::_("Bolsa_postulante").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBolsa_postulante->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}		
}