<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		28-08-2018
 * @copyright	Copyright (C) 28-08-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_matricula', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAcad_matricula 
{
	protected $idmatricula;
	protected $idgrupoauladetalle;
	protected $idalumno;
	protected $fecha_registro;
	protected $estado;
	protected $idusuario;
	protected $fecha_matricula;
	
	protected $dataAcad_matricula;
	protected $oDatAcad_matricula;	

	public function __construct()
	{
		$this->oDatAcad_matricula = new DatAcad_matricula;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_matricula->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_matricula->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_matricula->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_matricula->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_matricula->get($this->idmatricula);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_matricula', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_matricula->iniciarTransaccion('neg_i_Acad_matricula');
			$this->idmatricula = $this->oDatAcad_matricula->insertar($this->idgrupoauladetalle,$this->idalumno,$this->fecha_registro,$this->estado,$this->idusuario,$this->fecha_matricula);
			$this->oDatAcad_matricula->terminarTransaccion('neg_i_Acad_matricula');	
			return $this->idmatricula;
		} catch(Exception $e) {	
		    $this->oDatAcad_matricula->cancelarTransaccion('neg_i_Acad_matricula');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_matricula', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_matricula->actualizar($this->idmatricula,$this->idgrupoauladetalle,$this->idalumno,$this->fecha_registro,$this->estado,$this->idusuario,$this->fecha_matricula);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_matricula', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_matricula->eliminar($this->idmatricula);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmatricula($pk){
		try {
			$this->dataAcad_matricula = $this->oDatAcad_matricula->get($pk);
			if(empty($this->dataAcad_matricula)) {
				throw new Exception(JrTexto::_("Acad_matricula").' '.JrTexto::_("not registered"));
			}
			$this->idmatricula = $this->dataAcad_matricula["idmatricula"];
			$this->idgrupoauladetalle = $this->dataAcad_matricula["idgrupoauladetalle"];
			$this->idalumno = $this->dataAcad_matricula["idalumno"];
			$this->fecha_registro = $this->dataAcad_matricula["fecha_registro"];
			$this->estado = $this->dataAcad_matricula["estado"];
			$this->idusuario = $this->dataAcad_matricula["idusuario"];
			$this->fecha_matricula = $this->dataAcad_matricula["fecha_matricula"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_matricula', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_matricula = $this->oDatAcad_matricula->get($pk);
			if(empty($this->dataAcad_matricula)) {
				throw new Exception(JrTexto::_("Acad_matricula").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_matricula->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}