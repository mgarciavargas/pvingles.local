<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		31-05-2017
 * @copyright	Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class NegTools
{
	public function __construct() {}
	
	public static function validar($tipo, $valor, $obligatorio = true, $msj = false, $params = array())
	{
		$valor = strip_tags(trim($valor));
		$valor_ = self::utf8($valor);
		
		if(false == $obligatorio && empty($valor)) {
			return '';
		} else {
			if('letras' == $tipo) {
				if(preg_match("/^[A-Z��������������' ]+$/i", $valor_)) {
					if(is_numeric(@$params['longmax'])) {
						return substr($valor, 0, $params['longmax']);
					}
					return $valor;
				}
			} elseif('alfanum' == $tipo) {
				if(preg_match("/^[A-Z0-9��������������' ]+$/i", $valor_)) {
					if(is_numeric(@$params['longmax'])) {
						return substr($valor, 0, $params['longmax']);
					}
					
					return $valor;
				}
			} elseif('url' == $tipo) {
				if(!filter_var($valor_, FILTER_VALIDATE_URL) === false) {
					return $valor;
				}				
			} elseif('todo' == $tipo) {
				if(!empty($valor_) && is_numeric(@$params['longmax'])) {
					return substr($valor, 0, $params['longmax']);
				}
			} elseif('usuario' == $tipo) {
				$patron = "/^[a-z0-9.\d_]{".$params['longmin'].",".$params['longmax']."}$/i";
				if(preg_match($patron, $valor_)) {
					return $valor;
				}
			} elseif('num' == $tipo) {
				if(is_numeric($valor_)) {
					return $valor;
				}
			} elseif('email' == $tipo) {
				if(preg_match("/^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$/", $valor_)) {
					return $valor;
				}
			} elseif('long' == $tipo) {
				if(is_numeric(@$params['longmin']) && strlen($valor) >= $params['longmin']) {
					if(is_numeric(@$params['longmax']) && strlen($valor) <= $params['longmax']) {
						if(is_numeric(@$params['longigual']) && strlen($valor) == $params['longigual']) {
							return true;
						}
						return true;
					}
					return true;
				}
			} elseif('igual' == $tipo) {
				if($valor_ == $params['espejo']) {
					return true;
				}
			} else {
				return true;
			}
		}
		
		throw new Exception($msj);
	}
	
	public static function chekFecha($fecha, $formato = 'ymd')
    {
		if(empty($fecha)) {
			return false;
		}		
		$separador_tipo = array("/", "-", ".");		
        foreach ($separador_tipo as $separador) {
			$find = stripos($fecha, $separador);
            if(false !== $find) {
                $separador_usado = $separador;
            }
        }				
        $fecha_array = explode($separador_usado, $fecha);		
        if ($formato == 'mdy') {
            return checkdate($fecha_array[0], $fecha_array[1], $fecha_array[2]);
        } elseif ($formato=="ymd") {
            return checkdate($fecha_array[1], $fecha_array[2], $fecha_array[0]);
        } else {
            return checkdate($fecha_array[1], $fecha_array[0], $fecha_array[2]);
        }
    }	
	public static function cambiarFormatoFecha($fecha, $formato = 'ymd')
    {
		if(empty($fecha)) {
			return false;
		}		
		$separador_tipo = array("/", "-", ".");		
        foreach ($separador_tipo as $separador) {
			$find = stripos($fecha, $separador);
            if(false !== $find) {
                $separador_usado = $separador;
            }
        }				
        $fecha_array = explode($separador_usado, $fecha);		
        if ($formato == 'mdy') {
            return $fecha_array[2] . '-' . $fecha_array[0] . '-' . $fecha_array[1];
        } elseif ($formato == "ymd") {
            return $fecha_array[0] . '-' . $fecha_array[1] . '-' . $fecha_array[2];
        } else {
            return $fecha_array[2] . '-' . $fecha_array[1] . '-' . $fecha_array[0];
        }
    }
	
	public static function cambiarFormatoFechaDeA($fecha, $formato_de = 'ymd', $formato_a = 'y-m-d')
    {
		if(empty($fecha)) {
			return false;
		}		
		if('0000-00-00' == $fecha) {
			return '0000-00-00';
		}		
		$separador_tipo = array("/", "-", ".");		
		if('mdy' == $formato_de) {//[0]y-[1]m-[2]d //formato americano
			$fecha = str_replace($separador_tipo, '/', $fecha);
		} elseif('mdy' == $formato_de) {//[0]d-[1]m-[2]y //formato europeo
			$fecha = str_replace($separador_tipo, '-', $fecha);
		} else {//[0]y-[1]-m-d[2] //iso
			$fecha = str_replace($separador_tipo, '-', $fecha);
		}		
		return date($formato_a, strtotime($fecha));
    }	
	public static function getFormatoFecha($fecha, $formato = 'y-m-d')
    {
		$fecha = new DateTime($fecha);
		return $fecha->format($formato);
    }	
	public static function chekFechaHora($fecha_hora, $solo_fecha = false)
    {
		if (true == $solo_fecha) {
			return (date('Y-m-d', strtotime($fecha_hora)) == $fecha_hora) ? true : false;
		} else {
			return (date('Y-m-d H:i:s', strtotime($fecha_hora)) == $fecha_hora) ? true : false;
		}
    }	
	public static function esHora($hora)
    {
		$patron = "/^([0-1][0-9]|[2][0-3])[\:]([0-5][0-9])[\:]([0-5][0-9])$/";		
		if(preg_match($patron, $hora)) {
			return true;
		}
		return false;
    }	
	public static function checkRangoFecha($f_ini, $f_fin, $evaluar) {
		$f_ini = strtotime($f_ini);
		$f_fin = strtotime($f_fin);
		$evaluar = strtotime($evaluar);		
		return (($evaluar >= $f_ini) && ($evaluar <= $f_fin));
	}	
	public static function diasEntreFechas($fecha_ini, $fecha_fin) {
		$fecha_ini = date('Y-m-d', strtotime($fecha_ini));
		$fecha_fin = date('Y-m-d', strtotime($fecha_fin));		
		$dias = (strtotime($fecha_ini) - strtotime($fecha_fin)) / 86400;
		$dias = abs($dias);
		$dias = floor($dias);
		return $dias;
	}	
	public static function horasEntreFechas($fecha_ini, $fecha_fin) {
		$fecha_ini = new DateTime($fecha_ini);
		$fecha_fin = new DateTime($fecha_fin);
		$diff = $fecha_fin->diff($fecha_ini);		
		$horas = $diff->h + ($diff->i / 60);
		$horas = $horas + ($diff->days * 24);		
		return $horas;
	}	
	public static function codificaurl($cadena, $longmax)
	{
		$url = strtolower($cadena);
		$url = preg_replace("/[^a-z0-9\s-]/", "", $url);
		$url = trim(preg_replace("/[\s-]+/", " ", $url));
		$url = trim(substr($url, 0, $longmax));
		$url = preg_replace("/\s/", "-", $url);		
		return $url;
	}
	
    public static function is_bot(){     
        $bots = array(
            'Googlebot', 'Baiduspider', 'ia_archiver',
            'R6_FeedFetcher', 'NetcraftSurveyAgent', 'Sogou web spider',
            'bingbot', 'Yahoo! Slurp', 'facebookexternalhit', 'PrintfulBot',
            'msnbot', 'Twitterbot', 'UnwindFetchor, Java',
            'urlresolver', 'Butterfly', 'TweetmemeBot', 'Facebot', 'AhrefsBot', 'sitemaps');		
        foreach($bots as $b){
            if(stripos($_SERVER['HTTP_USER_AGENT'], $b ) !== false) return true;     
		}		
        return false;
    }	
	public static function sumarDias($fecha_base, $dias)
	{
		$fecha_base = new DateTime($fecha_base);
		$fecha_base->add(new DateInterval('P'.$dias.'D'));		
		return $fecha_base->format('Y-m-d');
	}	
	public static function codificacion($texto)
	{
		return mb_detect_encoding($texto, 'ASCII,UTF-8,ISO-8859-15');
	}	
	public static function utf8($texto)
	{
		return (self::codificacion($texto) == 'UTF-8') ? utf8_decode($texto) : $texto;
	}	
	public static function getCadUrl($text)
	{	// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		// trim
		$text = trim($text, '-');
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		// lowercase
		$text = strtolower($text);		
		if(empty($text)) {
			return 'n-a';
		}		
		return $text;
	}	
	public static function getFechasxNumSemana($semana, $anio)
	{
		//obtenemos el timestamp del primer dia del a�o
		$timestamp = mktime(0, 0, 0, 1, 1, $anio);
		//sumamos el timestamp de la suma de las semanas actuales
		$timestamp += $semana * 7 * 24 * 60 * 60;
		//restamos la posici�n inicial del primer dia del a�o
		$ultimoDia = $timestamp - date("w", mktime(0, 0, 0, 1, 1, $anio))*24*60*60;
		//le restamos los dias que hay hasta llegar al lunes
		$primerDia = $ultimoDia - 86400 * (date('N', $ultimoDia)-1);
		return array('inicio' => date("Y-m-d", $primerDia), 'fin' => date("Y-m-d", $ultimoDia), 'numero' => $semana);
	}	
	public static function date($formato = 'Y-m-d H:i:s', $time_zone = 'America/Lima', $fecha = null, $time_zone_base = 'UTC')
	{
		$formato = empty($formato) ? 'Y-m-d H:i:s' : $formato;
		$time_zone = empty($time_zone) ? 'America/Lima' : $time_zone;
		$fecha = empty($fecha) ? date('Y-m-d H:i:s') : $fecha;
		$time_zone_base = empty($time_zone_base) ? 'UTC' : $time_zone_base;		
		$fecha_actualizada = new DateTime($fecha, new DateTimeZone($time_zone_base));
		$fecha_actualizada->setTimeZone(new DateTimeZone($time_zone));		
		return $fecha_actualizada->format($formato);
	}

	public static function subirImagen($nombre,$img,$ruta,$rename=false,$ancho=100, $alto=100,$ext='png'){
		if(!empty($img)) {
			$dir_fotos = RUTA_BASE . 'static' . SD . 'media' . SD . $ruta . SD;
			$dir_cache = RUTA_BASE . 'sys_cache' . SD;

			if(!is_dir($dir_fotos)) { @mkdir($dir_fotos,0777); @chmod($dir_fotos,0777); }
			$data_img = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img));
            
			if($rename==false) $nom_tmp = $nombre.date('Yhis').'.'.$ext;
			else $nom_tmp=$nombre;
            $solonombre=reset(explode(".", $nom_tmp));
            //sube archivo
			file_put_contents($dir_cache . $nom_tmp, $data_img);
			$ruta_foto = $dir_cache . $nom_tmp;
			if(is_file($ruta_foto)) @chmod($ruta_foto, 0777);
						
			JrCargador::clase('jrAdwen::JrImagen');
			require_once(RUTA_LIBS . 'class_upload' . SD . 'class.upload.php');
			$imagen = new Upload($ruta_foto);
			$imagen->image_convert = $ext;
			JrImagen::crearImagen($imagen, $dir_fotos,$solonombre, $ancho, $alto, false);
			$file=$dir_fotos.$nom_tmp;
			if(is_file($file)){@chmod($file, 0777); @unlink($ruta_foto);}
			$rutaall=SD.'static'.SD.'media'.SD.$ruta.SD.$nom_tmp;
			return  $rutaall; 
		}
	}

	public static function isonline(){
		$ip=$_SERVER['SERVER_ADDR'];
		if($ip==='127.0.0.1') return false;
		else return true;
	}
}