<?php
/**
 * @autor		Abel Chingo Tello , ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersonal', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatRoles', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatPersona_rol', RUTA_BASE, 'sys_datos');
JrCargador::clase('jrAdwen::JrSession');
class NegSesion
{
	protected $oDatpersonarol;	
	protected $oDatPersonal;
	protected $oDatRoles;
	public function __construct()
	{
		$this->oDatPersonal = new DatPersonal;
		$this->oDatpersonarol = new DatPersona_rol;	
		$this->oDatRoles = new DatRoles;		
	}
	public static function existeSesion()
	{
		$sesion = JrSession::getInstancia();		
		$idpersona = $sesion->get('idpersona', false, '__admin_m3c');
		if(false === $idpersona) {
			return false;
		} else {
			return true;
		}
	}	
	public static function getUsuario()
	{
		$sesion = JrSession::getInstancia();
		$idpersona = $sesion->get('idpersona', false, '__admin_m3c');
		if(empty($idpersona)) {
			throw new Exception(JrTexto::_('Please sign in.'));
		}		
		return array('idpersona' => $idpersona
					, 'dni' => $sesion->get('dni', false, '__admin_m3c')
					, 'usuario' => $sesion->get('usuario', false, '__admin_m3c')
					, 'email' => $sesion->get('email', false, '__admin_m3c')
					, 'nombre_full' => $sesion->get('nombre_full', false, '__admin_m3c')
					, 'rol' => $sesion->get('rol', false, '__admin_m3c')
					, 'idrol' => $sesion->get('idrol', false, '__admin_m3c')
					, 'roles' => $sesion->get('roles', false, '__admin_m3c')
					, 'rol_original' => $sesion->get('rol_original', false, '__admin_m3c')	
					, 'clave' => $sesion->get('clave', false, '__admin_m3c')	
					, 'idHistorialSesion' => $sesion->get('idHistorialSesion', false, '__admin_m3c')
					,'idempresa'=>$sesion->get('idempresa', false, '__admin_m3c')
					,'idproyecto'=>$sesion->get('idproyecto', false, '__admin_m3c')
					,'idioma'=>$sesion->get('idioma', false, '__admin_m3c')
					);
	}	
	public function salir()
	{
		return JrSession::limpiarEspacio('__admin_m3c');
	}	
	
	public function ingresar($usu, $clave)
	{//04.04.16
		try {
			if(empty($usu) || empty($clave)) {
				return false;
			}
			$personal = $this->oDatPersonal->getxCredencial($usu, $clave);			
			if(empty($personal)){return false;}

			$usuario=$personal;
			$roles=$this->oDatpersonarol->buscar(array('idpersonal'=>$usuario['idpersona']));			
			$rol='';
			$idRol=-1;			
			if(!empty($roles[0])){
				$rol=$roles[0]["rol"];
				$idRol=$roles[0]["idrol"];
			}else{
				$idrol=$personal["rol"];
				$roles=$this->oDatRoles->buscar(array('idrol'=>$idrol));
				if(!empty($roles[0])) $rol=$roles[0]["rol"];
			}			
			$sesion = JrSession::getInstancia();
			$sesion->set('idpersona', $usuario['idpersona'], '__admin_m3c');
			$sesion->set('dni', $usuario['dni'], '__admin_m3c');
			$sesion->set('usuario', $usuario['usuario'], '__admin_m3c');
			$sesion->set('email', $usuario['email'], '__admin_m3c');
			$sesion->set('rol', $rol , '__admin_m3c');
			$sesion->set('idrol', $idRol , '__admin_m3c'); /*Se agregó este campo para se usado en los Controllers necesarios */
			$sesion->set('roles', $roles , '__admin_m3c');
			$sesion->set('rol_original', $rol , '__admin_m3c');		
			$sesion->set('nombre_full', $usuario['ape_paterno'].' '.$usuario['ape_materno'].", ".$usuario['nombre'], '__admin_m3c');
			$sesion->set('clave', $usuario['clave'], '__admin_m3c');
			return true;
		} catch(Exception $e) {
			return false;
		}
	}

	public function ingresar2($usu, $clave,$emp,$proy)
	{//04.04.16
		try {
			if(empty($usu) || empty($clave)) {
				return false;
			}
			$personal = $this->oDatPersonal->getxCredencial($usu, $clave);						
			if(empty($personal)){return false;}
			$usuario=$personal;
			$roles=$this->oDatpersonarol->buscar(array('idpersonal'=>$usuario['idpersona'],'idproyecto'=>$proy,'idempresa'=>$emp));					
			$rol='';
			$idRol=-1;
			if(!empty($roles)){
				$rol=$roles[0]["rol"];
				$idRol=$roles[0]["idrol"];
				$idioma = $roles[0]["idioma"];
			}else{
				if($usuario["tipousuario"]=='n') return false;
				$rol='Super Admin';
				$idrol='1';
				$idioma = 'EN';
			}			
			$sesion = JrSession::getInstancia();
			$sesion->set('idpersona', $usuario['idpersona'], '__admin_m3c');
			$sesion->set('dni', $usuario['dni'], '__admin_m3c');
			$sesion->set('usuario', $usuario['usuario'], '__admin_m3c');
			$sesion->set('email', $usuario['email'], '__admin_m3c');
			$sesion->set('rol', $rol , '__admin_m3c');
			$sesion->set('idrol', $idRol , '__admin_m3c'); /*Se agregó este campo para se usado en los Controllers necesarios */
			$sesion->set('roles', $roles , '__admin_m3c');
			$sesion->set('rol_original', $rol , '__admin_m3c');		
			$sesion->set('nombre_full', $usuario['ape_paterno'].' '.$usuario['ape_materno'].", ".$usuario['nombre'], '__admin_m3c');
			$sesion->set('clave', $usuario['clave'], '__admin_m3c');
			$sesion->set('idempresa', $emp, '__admin_m3c');
			$sesion->set('idproyecto', $proy, '__admin_m3c');
			$sesion->set('idioma', $idioma, '__admin_m3c');

			/* * * * * CAMBIO DE IDIOMA * * * * * */
			$documento = &JrInstancia::getDocumento();
	        $documento->setIdioma($idioma);
	        NegSesion::set('idioma', $idioma, 'idioma__');

			return true;
		} catch(Exception $e) {
			return false;
		}
	}



	public function cambiar_rol($idrol,$rol,$dni){
		if(empty($rol)||empty($dni)) return false;
		$roles=$this->oDatpersonarol->buscar(array('idpersonal'=>$dni));
		$sesion = JrSession::getInstancia();
		if(!empty($roles)){
			$sesion->set('roles', $roles , '__admin_m3c');
			foreach ($roles as $rol_){
				$rol=$rol_["rol"];
				$idrol_=$rol_["idrol"];
				if($idrol==$idrol_){
					$sesion->set('rol', $rol , '__admin_m3c');
		            $sesion->set('idrol', $idrol_ , '__admin_m3c');
					return true;
				}
			}
			return false;
		}else return false;
		//$sesion->set('rol_original', $rol , '__admin_m3c');
	}
	
	public static function get($prop, $espacio = '__admin_m3c')
	{
		$sesion = JrSession::getInstancia();		
		return $sesion->get($prop, null, $espacio);
	}
	
	public static function set_($prop, $valor)
	{
		$sesion = JrSession::getInstancia();
		$idpersona = $sesion->get('idpersona', false, '__admin_m3c');
		if(empty($idpersona)) {
			throw new Exception(JrTexto::_('Please sign in.'));
		}		
		return $sesion->set($prop, $valor, '__admin_m3c');
	}	
	
	public static function set($prop, $valor, $espacio = '__admin_m3c')
	{
        
		$sesion = JrSession::getInstancia();
		return $sesion->set($prop, $valor, $espacio);
	}

	public static function tiene_acceso($menu, $accion)
	{
		
		if(!NegSesion::existeSesion()) {
			return false;
		}		
		if('superadmin' == NegSesion::get('rol')) {
			return true;
		}
		$oDatRoles=new DatRoles;
		$existe_privilegio = $oDatRoles->existe_permiso(NegSesion::get('rol'),$menu, $accion);
		if(!$existe_privilegio){
			return false;
		}		
		return true;
	}

	public static function tieneRol($nombreRol='')
	{
		$existe = false;
		if(empty($nombreRol)){ return $existe; }
		$sesion = JrSession::getInstancia();
		$arrRoles = $sesion->get('roles', false, '__admin_m3c');
		foreach ($arrRoles as  $r) {
			if(strtolower($r['rol'])===strtolower($nombreRol)){
				$existe = true;
				break;
			}
		}
		return $existe;
	}
}