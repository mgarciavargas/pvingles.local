<?php
/**
 * @autor		Jos� Ricardo Burga Mori, basado en Joomla
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */

defined('RUTA_BASE') or die();

if(!defined('SD')) {
	define('SD', DIRECTORY_SEPARATOR);
}

class JrCargador
{
	/**
	 * Lee una clase desde el directorio especificado
	 */
	public static function clase($clase, $dir = null, $clave = 'sys_lib::', $pref = 'cls.', $salirExcep = true)
	{
		try {

			static $rutas;
			
			if(!isset($rutas)) {
				$rutas = array();
			}
			
			$claveRuta = $clave ? $clave . $clase : $clase;
			
			if(!isset($rutas[$claveRuta])) {//echo $clase.'==>'.$dir.'<br>';
				if(!$dir) {
					$dir = dirname(__FILE__) . SD;
				}
				$partes = explode('::', $clase);
				$nomClase = array_pop($partes);
				$nomClase = ucfirst($nomClase);
				
				//define la ruta del archivo
				$archivo = $pref . $nomClase . '.php';
				$ruta_ = $dir . implode(SD, $partes) . SD . $archivo;
				
				$rs	= JrCargador::registrar($nomClase, $ruta_);
				//echo $ruta_.'<br>';
				if(!is_file($ruta_)) {
					throw new Exception(JrTexto::_('Archivo no encontrado').': '.$ruta_);
				}
				
				@require_once($ruta_);
				
				$rutas[$claveRuta] = $rs;
			}
			
			return $rutas[$claveRuta];
		} catch(Exception $e) {
			if(true == $salirExcep) {
				JrCargador::clase('jrAdwen::JrWebExcepcion');
				echo new JrWebExcepcion($e->getMessage());
				exit();
			} else {
				throw new Exception($e->getMessage());
			}
		}
	}
	
	/**
	 * Agrega una clase a la autocarga
	 */
	public static function &registrar($clase = null, $archivo = null)
	{
		static $clases;
		
		if(!isset($clases)) {
			$clases = array();
		}
		//print_r($clase);
		if($clase && is_file($archivo)) {
			$clase = strtolower($clase);
			$clases[$clase] = $archivo;
		}
		
		return $clases;
	}
	
	public static function cargar($clase)
	{
		$clase = strtolower($clase);
		
		if(class_exists($clase)) {
			  return;
		}
		
		$clases = JrCargador::registrar();
		if(array_key_exists(strtolower($clase), $clases)) {
			require_once($clases[$clase]);
			return true;
		}
		return false;
	}
	
	public static function archivo($archivo, $dir = null, $clave = 'sys_lib::')
	{
		try {
			static $rutas;
			
			if(!isset($rutas)) {
				$rutas = array();
			}
			
			$claveRuta = $clave ? $clave . $archivo : $archivo;
			
			if(!isset($rutas[$claveRuta])) {
				if(!$dir) {
					$dir = dirname(__FILE__) . SD;
				}
				
				$partes = explode('::', $archivo);
				$nomArchivo_ = array_pop($partes);
				
				//define la ruta
				$ruta_  = str_replace('::', SD, $archivo);
				$ruta_ = $dir . $ruta_ . '.php';
				
				$rs	= JrCargador::registrar($nomArchivo_, $ruta_);
				
				require_once($ruta_);
				
				$rutas[$claveRuta] = $rs;
			}
			
			return $rutas[$claveRuta];
		} catch(Exception $e) {
			JrCargador::clase('jrAdwen::JrWebExcepcion');
			echo new JrWebExcepcion($e->getMessage());
			exit();
		}
	}
}

function __autoload($clase)
{
	if(JrCargador::cargar($clase)) {
		return true;
	}
	return false;
}

function jrExit($message = 0) {
    exit($message);
}

function jrClase($ruta) {
	return JrCargador::clase($ruta);
}