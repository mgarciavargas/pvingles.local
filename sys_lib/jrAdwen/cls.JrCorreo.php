<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */

class JrCorreo
{
	private $remiteEmail = null;
	private $remiteNom = null;
	private $destinatarios = array();
	private $destinatarioscc = array();
	private $destinatariosbcc = array();
	private $adjuntos = array();
	private $asunto = null;
	private $mensaje = null;
	private $tipo = 'HTML';

	public function __construct()
	{
	}
	
	public function agreDestinatario($direccion, $nombre = null)
	{//preg_match
		$direccion = urldecode($direccion);
		if(preg_match("/\r/", $direccion) || preg_match("/\n/", $direccion)) {
			return;
		}
		
		$nombre = urldecode($nombre);
		if(preg_match("/\r/", $nombre) || preg_match("/\n/", $nombre)) {
			return;
		}
		
		if(!empty($nombre)) {
			$this->destinatarios[] = $nombre . ' <'.$direccion.'>';
		} else {
			$this->destinatarios[] = $direccion;
		}
	}
	
	public function limpiarDestinatarios()
	{
		$this->destinatarios = array();
	}

	public function setRemitente($email, $nombre = null)
	{
		$email = urldecode($email);
		if(preg_match("/\r/", $email) || preg_match("/\n/", $email)) {
			return;
		}
		
		$nombre = urldecode($nombre);
		if(preg_match("/\r/", $nombre) || preg_match("/\n/", $nombre)) {
			return;
		}
		//strtr($_POST['from'],"\r\n\t",'???');
		$this->remiteEmail = $email;
		$this->remiteNom = $nombre;
	}

	public function setAsunto($asunto)
	{
		$this->asunto = (string) $asunto;
	}

	public function setMensaje($mensaje)
	{
		$this->mensaje = $mensaje;
	}
	
	public function enviar()
	{
		if(0 == count($this->destinatarios)) {
			throw new Exception('Correo sin destinatario');
		}
	
		$destinatarios_ = implode(", ", $this->destinatarios);
		
		$cabeceras_ = "From: ".$this->remiteNom." <".$this->remiteEmail.">\r\n";
		//$cabeceras_ = "To: <".$destinatarios_.">\r\n";
		//$cabeceras_ .= "Return-Path: <".$this->remiteEmail.">\r\n";
		$cabeceras_ .= "Reply-To: ".$this->remiteEmail."\r\n"; 
		//$cabeceras_ .= "Cc: ".$this->remiteEmail."\r\n";
		//$cabeceras_ .= "Bcc: ".$this->remiteEmail."\r\n";
		$cabeceras_ .= "X-Sender: ".$this->remiteEmail."\r\n";
		$cabeceras_ .= "X-Mailer: [".$this->remiteNom."]\r\n";
		$cabeceras_ .= "X-Priority: 3 \r\n";
		$cabeceras_ .= "MIME-Version: 1.0 \r\n";
		$cabeceras_ .= "Content-Transfer-Encoding: 7bit \r\n";
		$cabeceras_ .= 'Disposition-Notification-To: "'.$this->remiteNom.'" <'.$this->remiteEmail."> \r\n";
		//$cabeceras_ .= "\nX-Mailer: PHP/" . phpversion();

		if('HTML' == $this->tipo) {
			$cabeceras_ .= "Content-type: text/html; charset=iso-8859-1 \r\n";
			$mensaje_ = $this->mensaje;
		} else {
			$cabeceras_ .= "Content-Type: text/plain; charset=iso-8859-1 \r\n";
			$mensaje_ = wordwrap($this->mensaje, 80);
		}
		//echo($cabeceras_);
		//exit();
		return @mail($destinatarios_, $this->asunto, $mensaje_, $cabeceras_);
	}
	
	/*
	 * Phpmailer
	 */
	public function addDestinarioPhpmailer($direccion, $nombre = null)
	{//preg_match
		$direccion = urldecode($direccion);
		if(preg_match("/\r/", $direccion) || preg_match("/\n/", $direccion)) {
			return;
		}
		
		$nombre = urldecode($nombre);
		if(preg_match("/\r/", $nombre) || preg_match("/\n/", $nombre)) {
			return;
		}
		
		$this->destinatarios[] = array('email' => $direccion, 'nombre' => $nombre);
	}
	
	public function sendPhpmailer()
	{
		if(0 == count($this->destinatarios)) {
			throw new Exception('Correo sin destinatario');
		}
		
		JrCargador::clase('PHPMailer::PHPMailerAutoload');
		$mail = new PHPMailer(true);
		
		$config_ = ConfigSitio::get('salida_correos');
		
		//$mail->IsSMTP();
		//$mail->SMTPAuth = true;
		//$mail->SMTPSecure   = "ssl";

		//$mail->Host = 'mail.abacoeducacion.org';//edutec1.com'; // 'mail.abacovirtual.edu.pe';
		//$mail->Port = 110; //110;
		
		//$mail->Username = 'info@abacoeducacion.org';//'info@edutec1.com'; //'prueba@abacovirtual.edu.pe';
       // $mail->Password = 'info2018@';//'info2017'; // '9?zKaz8Uu3W';
		
        $mail->From = $this->remiteEmail;
		$mail->FromName = $this->remiteNom;
		
		foreach($this->destinatarios as $destinatario) {
			$mail->AddAddress($destinatario['email'], $destinatario['nombre']);
		}
		//$mail->AddBCC("ricardo.burgam@gmail.com", 'Gerencia');
        $mail->Subject = utf8_decode($this->asunto);
		
		//adjuntos
		if(!empty($this->adjuntos)){
			foreach ($this->adjuntos as $nombre=>$ruta) {
				$mail->AddAttachment($ruta, $nombre);
				$arrRuta = explode('.', $ruta);
				$extension = end($arrRuta);
				if( $extension== 'png' || $extension=='jpg'){
					$mail->AddEmbeddedImage($ruta, $nombre);
				}
			}
		}
        $mail->Body = $this->mensaje;
		$mail->IsHTML(true);
        //$mail->WordWrap = 50;
        /*echo $this->mensaje;*/
        //$mail->Body  = '<h1>Mensaje de prueba</h1> <p>probando email</p>';
        //var_dump($mail);
        //exit();
		if(!$mail->Send()) {
			//print_r($mail->ErrorInfo);
			return false;
		}

		return true;
	}

	public function addadjuntos($ruta, $nombre_mostrar)
	{
		if(!empty($nombre_mostrar)) {
			$this->adjuntos[$nombre_mostrar] = $ruta;
		} else {
			$this->adjuntos[] = $ruta;
		}
	}


	public function addemails($direccion, $nombre = null,$adonde='Des')
	{//preg_match
		$direccion = urldecode($direccion);
		if(preg_match("/\r/", $direccion) || preg_match("/\n/", $direccion)) {
			return;
		}
		
		$nombre = urldecode($nombre);
		if(preg_match("/\r/", $nombre) || preg_match("/\n/", $nombre)) {
			return;
		}
		if($adonde=='Des')
			if(!empty($nombre)) {
				$this->destinatarios[] = $nombre . ' <'.$direccion.'>';
			} else {
				$this->destinatarios[] = $direccion;
			}
		elseif($adonde=='Cc')
			if(!empty($nombre)) {
				$this->destinatarioscc[] = $nombre . ' <'.$direccion.'>';
			} else {
				$this->destinatarioscc[] = $direccion;
			}
		elseif($adonde=='Bcc')
			if(!empty($nombre)) {
				$this->destinatariosbcc[] = $nombre . ' <'.$direccion.'>';
			} else {
				$this->destinatariosbcc[] = $direccion;
			}
	}


	public function sendMail($tipo='HTML'){
			if(0 == count($this->destinatarios)) {
				throw new Exception('Correo sin destinatario');
			}
			$destinatarios = implode(", ", $this->destinatarios);
			$haycc=false;
			if(count($this->destinatarioscc)>0){
				$haycc==true;
				$destinatarioscc = implode(", ", $this->destinatarioscc);
			}
			$haybcc=false;
			if(count($this->destinatariosbcc)>0) {
				$haybcc=true;
				$destinatariosbcc = implode(", ", $this->destinatariosbcc);
			}
			

			$cabeceras = "From: ".$this->remiteNom." <".$this->remiteEmail.">\r\n";
			$cabeceras .= "Reply-To: ".$this->remiteEmail."\r\n"; 
			$cabeceras .= "MIME-Version: 1.0\r\n" ;
			if($haycc) $cabecera .= "Cc: ".$destinatarioscc."\r\n";
		    if($haybcc) $cabecera .= "Bcc: ".$destinatariosbcc."\r\n";
			$cabeceras .= "X-Priority: 1 (Highest)\n";
	        /*$cabeceras .= "X-MSMail-Priority: High\n";
	        $cabeceras .= "Importance: High\n";*/
	       
	        if('HTML' == $tipo) {
				$cabeceras .= "Content-type: text/html; charset=iso-8859-1 \r\n";
				$mensaje = $this->mensaje;
			} else {
				$cabeceras .= "Content-Type: text/plain; charset=iso-8859-1 \r\n";
				$mensaje = wordwrap($this->mensaje, 80);
			}
			/*echo $cabeceras;
			echo $mensaje;
			exit();*/
			$enviado = @mail($destinatarios, $this->asunto, $mensaje, $cabeceras);
			if ($enviado) return true;
			else return false;
	}
}
