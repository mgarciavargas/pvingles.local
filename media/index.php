<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <!-- wavesurfer.js -->
        <script src="wavesurfer.min.js"></script>
        <!--script src="js/plugin/wavesurfer.microphone.min.js"></script-->
        <!--script src="js/plugin/wavesurfer.regions.js"></script-->
        <!-- Demo -->
        <script src="main.js"></script>
        <script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
    </head>

    <body >
        <div class="container">
            <div id="waveform" style="width: 100px"></div>
            <button id="xyz">play and pause</button>
            <div id="grabado" style="width: 100px"></div>
            <div id="wavegrabar" style="width: 100px"></div>
            <button onclick="startRecording(this);">record</button>
            <button onclick="stopRecording(this);" disabled>stop</button>
            <ul id="recordingslist"></ul>
            <div class="" id="textograbado">Aqui grabando</div>
            <pre id="log"></pre>
 <script>
 var recognition=null;  
 var aquitexto=$('#textograbado');

 
  window.speechRecognition = window.speechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.webkitSpeechRecognition;
  if(window.speechRecognition == undefined)
  {
    alert("Speech Recogniztion API Not Supported");
  }else{
    recognition = new speechRecognition();
    recognition.continuous = true;
    recognition.interimResults = true;
    recognition.addEventListener( 'start', recognitionStarted );
    recognition.addEventListener( 'end', recognitionEnded );
    recognition.addEventListener( 'result', recognitionTalked );
    recognition.addEventListener( 'error', function ( error ){ console.log( error ); } );
  }

  function recognitionStarted ()
  {
    console.log( 'recognition started' );
  }

  function recognitionEnded ()
  {
    console.log( 'recognition stopped' );
  }

  function recognitionTalked (event)
  {
    var text = '';
    if(event.results &&  event.results.length )
    {
      for ( var i = event.resultIndex, len = event.results.length; i < len; ++i )
      {
        text += event.results[i][0].transcript;
      }
      aquitexto.html(text);
    }
  }

  function __log(e, data) {
    log.innerHTML += "\n" + e + " " + (data || '');
  }

  var audio_context;
  var recorder;
  var recording = 0;

  function startRecording(button){
    recording = recording + 1;
    recorder.clear();
    recorder && recorder.record();
    button.disabled = true;    
    button.nextElementSibling.disabled = false;
    recognition.start();
    __log('Recording...');
  }

  function stopRecording(button) {
    recorder && recorder.stop();
    button.disabled = true;
    button.previousElementSibling.disabled = false;
    recognition.stop();
    __log('Stopped recording.'); 
    createDownloadLink();
  }

  function createDownloadLink() {
      recorder.exportMP3(function(blob){
        var a = Date.now();
        var length = ((blob.size*8)/128000);
        console.log(blob);
        var url = URL.createObjectURL(blob);

        var link = document.createElement('a');
        link.href = url;
        link.innerHTML = "Recording #" + recording + " in mp3";
        link.download = "example.mp3";
        var data = new FormData();
        data.append('file', blob);
        $.ajax({
            url :  "save.php",
            type: 'POST',
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
              alert("boa!");
            },    
            error: function() {
              alert("not so boa!");
            }
        });
        var wavesurfer = Object.create(WaveSurfer);
        wavesurfer.init({
            container: document.querySelector('#grabado'),
            waveColor: '#A8DBA8',
            progressColor: '#3B8686',
            backend: 'MediaElement'
        });
    // Load audio from URL
    wavesurfer.load(link.href);
    document.querySelector('#grabado').addEventListener('click', wavesurfer.playPause.bind(wavesurfer));
        var li2 = document.createElement('li');
        li2.appendChild(link);
        document.getElementById("recordingslist").appendChild(li2);
  });
}



  window.onload = function init(){
    audioRecorder.requestDevice(function(recorderObject){
      recorder = recorderObject;
    }, {recordAsOGG: false});
  };


  </script>
            <script src="audioRecord.js?id=1"></script>
        </div>
    </body>
</html>