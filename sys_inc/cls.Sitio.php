<?php
/**
 * @autor		Abel Chingo Tello : ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016 Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class Sitio extends JrAplicacion{
	public static $msjs;	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function iniciar($compat = array())
	{
		if(true === $this->inicio) {
			return;
		}		
		JrCargador::clase('jrAdwen::JrModulo', RUTA_LIBS, 'jrAdwen::');
		JrCargador::clase('sys_inc::ConfigSitio', RUTA_BASE, 'sys_inc::');
		JrCargador::clase('sys_datos::DatBase', RUTA_BASE, 'sys_datos');
		JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
		JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE, 'sys_negocio');
		$oConfigSitio = ConfigSitio::getInstancia();
		parent::iniciar($oConfigSitio->get_());
	}	
	public function enrutar($rec = null, $ax = null)
 	{

		 $permitenologueado=array('aulavirtual', 'service','pronunciacion','proyecto','paginas');
		if(IS_LOGIN===true&&!NegSesion::existeSesion()&&!in_array($rec,$permitenologueado)){
			parent::enrutar('sesion');
		}else{	
			$documento =& JrInstancia::getDocumento();
			$documento->setTitulo(NegSesion::get('nombre'), true);
			parent::enrutar($rec, $ax);
		}
 	}	
	public static function &getInstancia()
	{
		if(empty(self::$instancia)) {
			self::$instancia = new self;
		}
		return self::$instancia;
	}
	public function error($msj, $plantilla = null)
	{
		JrCargador::clase('sys_web::WebExcepcion', RUTA_SITIO, 'sys_web');
		$oWebExcepcion = new WebExcepcion;
		return $oWebExcepcion->error($msj, $plantilla);
	}
	public function noencontrado()
	{
		JrCargador::clase('sys_web::WebExcepcion', RUTA_SITIO, 'sys_web');
		$oWebExcepcion = new WebExcepcion;
		return $oWebExcepcion->noencontrado();
	}
}