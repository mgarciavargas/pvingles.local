ALTER TABLE  `acad_curso` ADD  `objetivos` TEXT NULL ,
ADD  `certificacion` TEXT NULL ,
ADD  `costo` FLOAT NULL DEFAULT  '0',
ADD  `silabo` VARCHAR( 100 ) NULL ,
ADD  `e_ini` TEXT NULL ,
ADD  `e_fin` TEXT NULL ,
ADD  `pdf` VARCHAR( 100 ) NULL ,
ADD  `abreviado` VARCHAR( 20 ) NULL ,
ADD  `aniopublicacion` INT( 4 ) NULL ,
ADD  `autor` VARCHAR( 200 ) NULL ;

ALTER TABLE `acad_curso` DROP ` aniopublicacion `;
ALTER TABLE `acad_curso` ADD `aniopublicacion` DATE NULL AFTER `abreviado`;
ALTER TABLE `acad_curso` ADD `txtjson`;
ALTER TABLE `acad_curso` CHANGE `color` `color` VARCHAR(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL;