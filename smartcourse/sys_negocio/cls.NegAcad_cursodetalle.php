<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		09-02-2018
 * @copyright	Copyright (C) 09-02-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_cursodetalle', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatNiveles', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAcad_cursodetalle 
{
	protected $idcursodetalle;
	protected $idcurso;
	protected $orden;
	protected $idrecurso;
	protected $tiporecurso;
	protected $idlogro;
	protected $url;
	protected $idpadre;
	protected $color;
	protected $esfinal;
	
	protected $dataAcad_cursodetalle;
	protected $oDatAcad_cursodetalle;
	protected $oDatNiveles;

	public function __construct()
	{
		$this->oDatAcad_cursodetalle = new DatAcad_cursodetalle;
		$this->oDatNiveles = new DatNiveles;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_cursodetalle->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_cursodetalle->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getMaxorden($idcurso,$idpadre)
	{
		try {
			return $this->oDatAcad_cursodetalle->maxorden($idcurso,$idpadre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_cursodetalle->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function sesiones($idcurso,$idpadre){ // muestra toda la secuencia de sesiones segun el curso y el idpadre
		try{
			$datos=$this->oDatAcad_cursodetalle->buscarconnivel(array("idcurso"=>$idcurso,'idpadre'=>$idpadre));
			$datos2=array();
			$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false,),); 			
			if(!empty($datos)){				
				foreach ($datos as $dt){
					$tipo=$dt["tiporecurso"];
					if($tipo=='E'){
						$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
						$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';						
						$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$dt['idrecurso'].'&pr='.IDPROYECTO, false, stream_context_create($arrContextOptions));
                            $dataExam = json_decode($dataExam, true);
                            if($dataExam['code']==200){
                                $dt["nombre"] = $dataExam['data']['titulo'];
                                $dt["imagen"] = $dataExam['data']['portada'];
                            }
					}
					$datos3=$this->sesiones($idcurso,$dt["idcursodetalle"]);
					if(!empty($datos3)){ $dt["hijo"]=$datos3; }
					$datos2[]=$dt;
				}
			}else return null;
			return $datos2;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	
	//Listan el menu ok  ---------------
	public function verhijos($dt,$id){
		$sd=array();
		$dt2=$dt;
		foreach ($dt as $k =>$ds2){
			$idpadre=$ds2["idpadre"];			
			if($idpadre==$id){			
				unset($dt2[$k]);
				$ds2['hijos']=$this->verhijos($dt2,$ds2["idcursodetalle"]);
				array_push($sd,$ds2);
			}
		}
		return $sd;	
	}

	public function buscarconnivel($filtros,$maxlimit=150000){
		try{
			//var_dump($filtros);
			$this->oDatAcad_cursodetalle->setLimite(0, $maxlimit);
			$dt=$this->oDatAcad_cursodetalle->buscarconnivel($filtros);
			//var_dump($dt);
			$dts=array();
			$dpmay=0;
			$dpmen=1000000000;	
			foreach ($dt as $ds){
				$idpadre=$ds["idpadre"];
				$tipo=$ds["tiporecurso"];
				if($idpadre<=$dpmen) $dpmen=$idpadre;
				if($tipo=='E'||$tipo=='EU'){	
					$ds["imagen"] = '/static/media/imagenes/examen_default.png';
					$arrContextOptions=array("ssl"=>array( "verify_peer"=>false, "verify_peer_name"=>false, ),);
					//echo URL_SMARTQUIZ.'service/exam_info?idexam='.$ds['idrecurso'].'&pr='.IDLEARN."<br>";
					$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$ds['idrecurso'].'&pr='.IDLEARN, false, stream_context_create($arrContextOptions));
                    $dataExam = json_decode($dataExam, true);
                    if($dataExam['code']==200){
                        $ds["nombre"] = $dataExam['data']['titulo'];
                        $ds["imagen"] = $dataExam['data']['portada'];
                    }                  
				}
				if(empty($ds["imagen"])) $ds["imagen"]='/static/media/cursos/nofoto.jpg';
				array_push($dts,$ds);
			}

			$td1=$dts;
			//var_dump($td1);
			$dat=array();		
			foreach($td1 as $k=>$ds){ $ipa=$ds["idpadre"];
			//echo " <br>".$dpmen." - - ".$ipa;
				if($dpmen==$ipa){
					unset($dts[$k]);
					$ds['hijos']=$this->verhijos($dts,$ds["idcursodetalle"]);
					array_push($dat,$ds);
				}
			}	
			//var_dump($dat);
			return $dat;
		} catch(Exception $e){
			var_dump($e->Message());
			throw new Exception($e->getMessage());	
		}
	}
	//

	public function importaramenu($idcurso,$userid,$datos,$tipo,$clonar){
		try {
			$this->oDatAcad_cursodetalle->iniciarTransaccion('import_cursodetalle');
			//$this->oDatAcad_cursodetalle 
			//$this->oDatNiveles
			$padres=array();
			$padresupdate=array();
			if($tipo=='nivel'){
				if($clonar=='no'){ // asignara los registros al curso ...
					if(!empty($datos))
					foreach ($datos as $k => $v){
						$update =false;
						if($v->idpadre==0)$idpadre=0;
						else{
							if(!empty($padres[$v->idnivel])) $idpadre=$padres[$v->idnivel];
							else{
								$idpadre=-1;
								$update=true;
							}
						}
						$idcursodet = $this->oDatAcad_cursodetalle->insertar($idcurso,$v->orden,$v->idnivel,$v->tipo,0,'',$idpadre,'',0);
						$padres[$v->idnivel]=$idcursodet;
						if($update==true){
							$padresupdate[$idcursodet]=$v;
						}						
					}
					if(!empty($padresupdate)){
						foreach ($padresupdate as $k => $v){							
							$this->oDatAcad_cursodetalle->actualizar($k,$idcurso,$v->orden,$v->idnivel,$v->tipo,'0','',$padres[$v->idpadre],'',0);
						}
					}
				}else{//clonara los registros
					
				}
			}else{
				if($clonar=='no'){
					if(!empty($datos))						
					foreach ($datos as $k => $v){						
						$update =false;
						if($v->idpadre==0)$idpadre=0;
						else{
							if(!empty($padres[$v->idcursodetalle])) $idpadre=$padres[$v->idcursodetalle];
							else{
								$idpadre=-1;
								$update=true;
							}
						}
						$data=array();
						$data["idcurso"]=$idcurso;
						$data["idpadre"]=$v->idpadre;
						$idcursodet = $this->oDatAcad_cursodetalle->duplicar($v->idcursodetalle,$data);
						$padres[$v->idcursodetalle]=$idcursodet;
						if($update==true){
							$padresupdate[$idcursodet]=$v;
						}
					}
					if(!empty($padresupdate)){
						foreach ($padresupdate as $k => $v){							
							$this->oDatAcad_cursodetalle->actualizar($k,$idcurso,$v->orden,$v->idnivel,$v->tiporecurso,'0','',$padres[$v->idpadre],'',0);
						}
					}
				}else{//clonara los registros
					//var_dump($datos);
				}
			}


			$this->oDatAcad_cursodetalle->terminarTransaccion('import_cursodetalle');
		} catch(Exception $e) {
			$this->oDatAcad_cursodetalle->cancelarTransaccion('import_cursodetalle');
			throw new Exception($e->getMessage());
		}
	}
	// --------------------

	public function eliminarMenu($idcurso,$idcursodetalle){
		$id=$idcursodetalle;
		try{
			$this->oDatAcad_cursodetalle->iniciarTransaccion('negeliminar'.$id);
			$datos2=$this->oDatAcad_cursodetalle->buscar(array("idpadre"=>$id));
			if(!empty($datos2)){
				foreach ($datos as $dt){
					$this->eliminarMenu($dt["idcurso"],$dt["idcursodetalle"]);
				}
			}
			$aeli=$this->oDatAcad_cursodetalle->buscar(array("idcursodetalle"=>$id));
			if(!empty($aeli[0])){
				$curdet=$aeli[0];
				$idrecurso=$curdet["idrecurso"];
				$tipo=$curdet["tiporecurso"];
				$this->oDatAcad_cursodetalle->eliminar($id);
				//falta elimimnar en niveles
			}
			$this->oDatAcad_cursodetalle->terminarTransaccion('negeliminar'.$id);
		}catch(Exception $e) {	
		    $this->oDatAcad_cursodetalle->cancelarTransaccion('negeliminar'.$id);		
			throw new Exception($e->getMessage());
		}
	}

	public function buscarRecurso($filtros){
		try{
			return $this->oDatNiveles->buscar($filtros);
		} catch(Exception $e){
			throw new Exception($e->getMessage());	
		}
	}


	public function listar()
	{
		try {
			return $this->oDatAcad_cursodetalle->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getNivelPadre($curso_det)
	{
		try {
			if(empty($curso_det)){throw new Exception(JrTexto::_('curso_det missing')); }
			$padre = $this->oDatAcad_cursodetalle->get($curso_det['idpadre']);
			$nivel_padre = $this->oDatNiveles->get($padre['idrecurso']);
			$response = array_merge($padre, $nivel_padre);
			return $response;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_cursodetalle->get($this->idcursodetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregaromodificar($idcursodetalle, $nombre='',$idrecursoPadre='',$idpersonal='',$imagen='',$descripcion='',$ordenrecurso=0,$iscloonado=false){
		try {
			$this->oDatAcad_cursodetalle->iniciarTransaccion('neg_i_Acad_cursodetalle');
			$nivel=$this->oDatNiveles->insertoupdate($this->idrecurso, $nombre,$this->tiporecurso,$idrecursoPadre,$idpersonal,1,$ordenrecurso,$imagen,$descripcion);
			$idrecurso=$nivel["idnivel"];
			$cursodetalle=$this->oDatAcad_cursodetalle->insertoupdate($idcursodetalle,$this->idcurso,$this->orden,$idrecurso,$this->tiporecurso,$this->idlogro,$this->url,$this->idpadre,$this->color,$this->esfinal);
			$this->oDatAcad_cursodetalle->terminarTransaccion('neg_i_Acad_cursodetalle');
			$menu=array_merge($nivel,$cursodetalle);
			return $menu;
			}catch(Exception $e){
		    	$this->oDatAcad_cursodetalle->cancelarTransaccion('neg_i_Acad_cursodetalle');		
			throw new Exception($e->getMessage());
		}
	}


	public function agregar()
	{
		try{
			/*if(!empty($this->idrecurso)){
				$nivel=$this->oDatNiveles->buscar(array('idnivel'));
			}*/
			/*
			if(!NegSesion::tiene_acceso('acad_cursodetalle', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_cursodetalle->iniciarTransaccion('neg_i_Acad_cursodetalle');
			$this->idcursodetalle = $this->oDatAcad_cursodetalle->insertar($this->idcurso,$this->orden,$this->idrecurso,$this->tiporecurso,$this->idlogro,$this->url,$this->idpadre,$this->color,$this->esfinal);
			$this->oDatAcad_cursodetalle->terminarTransaccion('neg_i_Acad_cursodetalle');	
			return $this->idcursodetalle;
		} catch(Exception $e) {	
		    $this->oDatAcad_cursodetalle->cancelarTransaccion('neg_i_Acad_cursodetalle');		
			throw new Exception($e->getMessage());
		}
	}

	public function agregarvarios($idcurso,$detallecursos,$idpadre=0){
		try {
			$univel=$uunidad=$ulession=$idpadre=$id=$idpadre;			
			foreach($detallecursos as $k=>$v){
				$tipo=$v->tipo;
				$idpadre=($tipo=='N')?0:(($tipo=='U')?$univel:$uunidad);
				$orden=$this->getMaxorden($idcurso,$idpadre);
				$orden++;
				$nombre=$v->nombre;
				$idrecurso=$v->idnivel;
				$color=$v->color;
				$esfinal=$v->esfinal;
				$id=$this->oDatAcad_cursodetalle->insertar($idcurso,$orden,$idrecurso,$tipo,0,'',$idpadre,$color,$esfinal);
				if($tipo=='N'){
					$univel=$id;
				}elseif($tipo=='U'){
					$uunidad=$id;
				}
				
			}
		} catch(Exception $e) {			    		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_cursodetalle', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_cursodetalle->actualizar($this->idcursodetalle,$this->idcurso,$this->orden,$this->idrecurso,$this->tiporecurso,$this->idlogro,$this->url,$this->idpadre,$this->color,$this->esfinal);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatAcad_cursodetalle->cambiarvalorcampo($this->idcursodetalle,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($idpadre)
	{
		try{				
			$datos2=$this->oDatAcad_cursodetalle->buscar(array("idpadre"=>$idpadre));
			if(!empty($datos2)){
				foreach ($datos2 as $v1){
					$datos2=$this->oDatAcad_cursodetalle->buscar(array("idpadre"=>$v1["idcursodetalle"]));
					if(!empty($datos2)){
						foreach ($datos2 as $dt2){					
							$this->oDatAcad_cursodetalle->eliminar2("idpadre",$dt2["idcursodetalle"]);
						}
					}
					$this->oDatAcad_cursodetalle->eliminar2("idpadre",$v1["idcursodetalle"]);
				}
				$this->oDatAcad_cursodetalle->eliminar2("idpadre",$idpadre);	
			}				
			return $this->oDatAcad_cursodetalle->eliminar2("idcursodetalle",$idpadre);			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcursodetalle($pk){
		try {
			$this->dataAcad_cursodetalle = $this->oDatAcad_cursodetalle->get($pk);
			if(empty($this->dataAcad_cursodetalle)) {
				throw new Exception(JrTexto::_("Acad_cursodetalle").' '.JrTexto::_("not registered"));
			}
			$this->idcursodetalle = $this->dataAcad_cursodetalle["idcursodetalle"];
			$this->idcurso = $this->dataAcad_cursodetalle["idcurso"];
			$this->orden = $this->dataAcad_cursodetalle["orden"];
			$this->idrecurso = $this->dataAcad_cursodetalle["idrecurso"];
			$this->tiporecurso = $this->dataAcad_cursodetalle["tiporecurso"];
			$this->idlogro = $this->dataAcad_cursodetalle["idlogro"];
			$this->url = $this->dataAcad_cursodetalle["url"];
			$this->idpadre = $this->dataAcad_cursodetalle["idpadre"];
			$this->color = $this->dataAcad_cursodetalle["color"];
			$this->esfinal = $this->dataAcad_cursodetalle["esfinal"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_cursodetalle', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_cursodetalle = $this->oDatAcad_cursodetalle->get($pk);
			if(empty($this->dataAcad_cursodetalle)) {
				throw new Exception(JrTexto::_("Acad_cursodetalle").' '.JrTexto::_("not registered"));
			}
			return $this->oDatAcad_cursodetalle->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}	
		
}