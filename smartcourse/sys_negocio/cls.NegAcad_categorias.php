<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-04-2018
 * @copyright	Copyright (C) 17-04-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_categorias', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAcad_categorias 
{
	protected $idcategoria;
	protected $nombre;
	protected $descripcion;
	protected $imagen;
	protected $idpadre;
	protected $estado;
	protected $orden;
	
	protected $dataAcad_categorias;
	protected $oDatAcad_categorias;	

	public function __construct()
	{
		$this->oDatAcad_categorias = new DatAcad_categorias;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_categorias->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_categorias->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_categorias->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_categorias->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_categorias->get($this->idcategoria);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_categorias', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_categorias->iniciarTransaccion('neg_i_Acad_categorias');
			$this->idcategoria = $this->oDatAcad_categorias->insertar($this->nombre,$this->descripcion,$this->imagen,$this->idpadre,$this->estado,$this->orden);
			$this->oDatAcad_categorias->terminarTransaccion('neg_i_Acad_categorias');	
			return $this->idcategoria;
		} catch(Exception $e) {	
		    $this->oDatAcad_categorias->cancelarTransaccion('neg_i_Acad_categorias');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_categorias', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_categorias->actualizar($this->idcategoria,$this->nombre,$this->descripcion,$this->imagen,$this->idpadre,$this->estado,$this->orden);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_categorias', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_categorias->eliminar($this->idcategoria);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcategoria($pk){
		try {
			$this->dataAcad_categorias = $this->oDatAcad_categorias->get($pk);
			if(empty($this->dataAcad_categorias)) {
				throw new Exception(JrTexto::_("Acad_categorias").' '.JrTexto::_("not registered"));
			}
			$this->idcategoria = $this->dataAcad_categorias["idcategoria"];
			$this->nombre = $this->dataAcad_categorias["nombre"];
			$this->descripcion = $this->dataAcad_categorias["descripcion"];
			$this->imagen = $this->dataAcad_categorias["imagen"];
			$this->idpadre = $this->dataAcad_categorias["idpadre"];
			$this->estado = $this->dataAcad_categorias["estado"];
			$this->orden = $this->dataAcad_categorias["orden"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_categorias', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_categorias = $this->oDatAcad_categorias->get($pk);
			if(empty($this->dataAcad_categorias)) {
				throw new Exception(JrTexto::_("Acad_categorias").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_categorias->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}