<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-03-2017 
 * @copyright	Copyright (C) 24-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebGames extends JrWeb
{
	public function __construct()
	{
		parent::__construct();
	}

	public function defecto(){
		return $this->ver();
	}

	public function ver(){
		try{			
			global $aplicacion;

			$usuarioAct = NegSesion::getUsuario();		
			$this->documento->setTitulo(JrTexto::_('SmartGame'));
			$idgame=!empty($_REQUEST["idgame"])?$_REQUEST["idgame"]:0;
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'blanco';
			$this->link =	URL_RAIZ."game/ver/" .$idgame;
			$this->esquema = 'games/ver';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
		//$paginaext="https://www.abacoeducacion.org/web/smartquiz/examenes/resolver/?idexamen=$idexa&id=$idalu&pr=edukt2017&u=$nomalu";
	}

	public function buscar(){
		try{			
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();		
			$this->documento->setTitulo(JrTexto::_('SmartGame'));		
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'blanco';
			$this->esquema = 'games/buscar';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}