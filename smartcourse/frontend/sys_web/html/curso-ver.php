<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$curso=!empty($this->curso)?$this->curso:'';
$idcursodetalle=!empty($this->idcursodetalle)?$this->idcursodetalle:'0';
$idcurso=!empty($curso["idcurso"])?$curso["idcurso"]:'';
$imgdefault=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
$img=is_file(RUTA_MEDIA.@$curso["imagen"])?URL_MEDIA.@$curso["imagen"]:$imgdefault;
$urlmedia=URL_MEDIA;
$haycontenidoinicial='';
if(file_exists(RUTA_MEDIA.'static'.SD.'media'.SD.'cursos'.SD.'curso_'.$idcurso.SD.'ses_0.html')) {
	$haycontenidoinicial=URL_MEDIA.'/static/media/cursos/curso_'.$idcurso."/ses_0.html";
}
?>
<style type="text/css">
body{ margin: 0px; }
.anillado{
	background-image: url(<?php echo $urlmedia; ?>/static/media/cursos/anillado.png);
    background-size: contain;
    height: calc(100%);
    position: absolute;
    width: 2em;
    margin-left: -2em;
}
.tabtop{ position: relative; width: 100%; height: 50px; }
.tabtop ul{ position:  absolute; right:  0px;  top: 1.2ex;}
ul.toolbar1{ display: inline-block; margin-bottom: 0px; padding-left:1ex; padding-top: 1ex; }
ul.toolbar1 li{display: inline-block; padding-left: 1ex;}


ul.toolbar1 ~ .showindicepage{ display: none; width: 340px; font-size: 1em; background: #2480dd; color: #333; }	
ul.toolbar1.active ~ .showindicepage{ position: absolute; left:50px; display: block; z-index: 9999; }
#indicetop{ margin: 1px; padding: 1px; padding-bottom: 2px; }
#indicetop > div{ padding: 5px; font-size: 14px; color: #000000cf; background: #fdf9f93d; border: 1px solid #f9f7f7a8; font-weight: bold; cursor: pointer;}
#indicetop .pagevisto:before{
	content: "\f00c";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
   /* text-decoration: inherit;
--adjust as necessary--*/
    color:#28a745;
    font-size: 16px;
    padding-right: 0.5ex;
     /*position: absolute;
   top: 10px;
    left: 0px;*/
}
.pnledittabmenu a{min-width: 150px; background-size: 100% 100%;	border: 0px;}

.menubooktop >ul{border-bottom: 0px;} 
.contentpages .showindicepage{background: #fff3f300;}
.modal{ color: #756c6c;}
</style>
<div class="row">
	<div class="col-md-6" style="height: 50px;">		
	    <ul class="toolbar1">	      
	      <li><a class=" btnshowsesiones" href="javascript:void(0)"><i class="fa fa-th fa-2x"></i></a></li>
	      <li><a class="iconsesion" href="javascript:void(0)" data-link="<?php echo $haycontenidoinicial; ?>"><i class="fa fa-home fa-2x"></i></a></li>
	      <li class="nombrecurso">"<?php echo $curso["nombre"]; ?>"</li>
	      <li class="nombresesion"></li>	    	    
	    </ul>
	    <div class="showindicepage">
	    	<div class="pnlsesions text-center">
				<h2>Sesiones</h2>
				<div class="row" id="indicetop">
				</div>
			</div>
	    </div>
	</div>
	<div class="col-md-12" id="paneldisenio" style="height:calc(100vh - 52px);  overflow-y: auto;">
	</div>
	<div id="curdetalletmp" style="display: none;"></div>
</div>
<script type="text/javascript">
	var $copyhtml='';
	var idcurso=parseInt('<?php echo $idcurso; ?>');
	var iddetalle=parseInt('<?php echo $idcursodetalle; ?>');
	var _sysUrlMedia_='<?php echo URL_MEDIA; ?>';
	var _sysUrlRaiz_='<?php echo URL_RAIZ;?>';
	var imgdefault='<?php echo $imgdefault;?>';

	var __subirmediamdini=function(obj,type,typefile){
    	var oldmedia=obj.attr('data-oldmedia')||'';
    	var file=document.createElement('input');
	    	file.type='file';
	    	file.accept=type;
	    	file.id='file_'+__idgui();
	    	file.addEventListener('change',function(ev){
	    		let $idprogress=$('#clone_progressup').clone(true);
				let iduploadtmp='idup'+__idgui();
				$idprogress.attr('id',iduploadtmp);
				obj.append($idprogress);							
				var data=__formdata('');
				data.append('media',this.files[0]);
				data.append('dirmedia','curso_'+idcurso+'/');
				data.append('oldmedia',oldmedia);
				data.append('typefile',typefile);
				__sysajax({ 
					fromdata:data,
                	url:_sysUrlBase_+'/acad_cursodetalle/uploadmedia',
                	iduploadtmp: '#'+iduploadtmp,
                	callback:function(rs){
                		if(rs.code=='ok'){
                			if(rs.media!=''){
                				__cargarplantilla({url:'/plantillas/'+typefile,donde:'contentpages',data:{link:rs.media}});
                				//obj.attr('data-oldmedia',rs.media).children('i').addClass('btn-success').removeClass('btn-primary'); 
                			} 
                		}
                	}
            	});
	    	});
	    file.click();
	}

	var __addexamen=function(obj,fncall){
		//let md=__sysmodal({titulo:'Examenes',html:$('#config_plantilla').html()});
		obj.off('addassesment');
		let lstmp='btnaddexamen'+__idgui();
		obj.addClass(lstmp);
		let url=_sysUrlBase_+'/quiz/buscar/?plt=modal&fcall='+lstmp;
		let md=__sysmodal({titulo:'Quiz',url:url});
		obj.on('addassesment',function(ev){
			$(this).off(lstmp);
			$(this).removeClass(lstmp);
			var idexamen=$(this).attr('data-idexamen');
			let link=_sysUrlBase_+'/quiz/ver/?idexamen='+idexamen;
			if(_isFunction(fncall)) fncall(link);	
			//__cargarplantilla({url:link,donde:'contentpages'});			
		})
	}
	var __addgame=function(obj,fncall){
		obj.off('addgame');
		let lstmp='addgame'+__idgui();
		obj.addClass(lstmp);
		let url=_sysUrlBase_+'/games/buscar/?plt=modal&fcall='+lstmp;		
		let md=__sysmodal({titulo:'Games',url:url});
		obj.on('addgame',function(ev){
			$(this).off(lstmp);
			$(this).removeClass(lstmp);
			var idgame=$(this).attr('data-idgame');
			let link=_sysUrlRaiz_+'game/ver/?idgame='+idgame+'&tmp='+lstmp;
			if(_isFunction(fncall)) fncall(link); 		
			//__cargarplantilla({url:link,donde:'contentpages'});			
		})
	}

	var __addtics=function(obj,met,fncall){
		obj.off('addtics');
		let lstmp='addtics'+__idgui();
		obj.addClass(lstmp);
		let url=_sysUrlBase_+'/tics/buscar/?plt=modal&fcall='+lstmp+'&met='+met;
		let md=__sysmodal({titulo:'Games',url:url});
		obj.on('addtics',function(ev){
			$(this).off(lstmp);
			$(this).removeClass(lstmp);
			let idtic=$(this).attr('data-idtic'); // idactividad
			let idsesion=$(this).attr('data-idsesion');
			let link=_sysUrlRaiz_+'actividad/ver/?idactividad='+idtic+"&met="+met+"&idsesion="+idsesion+'&tmp='+lstmp;	
			obj.attr('data-link',link);
			if(_isFunction(fncall)) fncall(link); 
			//__cargarplantilla({url:link,donde:'contentpages'});			
		})
	}

	let __subirmediamd=function(obj,type,typefile,fncall){
		$('audio').trigger('pause');
		$('video').trigger('pause');
    	var oldmedia=obj.attr('data-oldmedia')||'';
    	var file=document.createElement('input');
	    	file.type='file';
	    	file.accept=type;
	    	file.id='file_'+__idgui();
	    	file.addEventListener('change',function(ev){
	    		let $idprogress=$('#clone_progressup').clone(true);
				let iduploadtmp='idup'+__idgui();
				$idprogress.attr('id',iduploadtmp);
				obj.append($idprogress);							
				var data=__formdata('');
				data.append('media',this.files[0]);
				data.append('dirmedia','curso_'+idcurso+'/');
				data.append('oldmedia',oldmedia);
				data.append('typefile',typefile);
				__sysajax({ 
					fromdata:data,
                	url:_sysUrlBase_+'/acad_cursodetalle/uploadmedia',
                	iduploadtmp: '#'+iduploadtmp,
                	callback:function(rs){
                		if(rs.code=='ok'){
                			if(rs.media!=''){
                			   if(_isFunction(fncall)) fncall(rs);             						                				
                			}
                		}
                	}
            	});
	    	});
	    	file.click();
    }

	var __paneltools=function(obj){
		let item=obj.item;
		let txt=obj.texto||'';
		let fncall=obj.fncall;
		$('audio').trigger('pause');
		$('video').trigger('pause');	
		//
		let fuente=item.attr('data-fuente')||'';
		let zise=item.attr('data-zise')||'';
		let colortexto=item.attr('data-colortexto')||'';
		let colorfondo=item.attr('data-colorfondo')||'';
		let imagenfondo=item.attr('data-imagenfondo')||'';
		let datatype=item.attr('data-type')||'';
		let datalink=item.attr('data-link')||'';
		console.log(colorfondo,colortexto);
		let md=__sysmodal({titulo:'',html:$('#clone_editmenuwitget').html()});
		md.find('input.texto').val(txt.trim());
		md.find('select.tipofuente').val(fuente.trim());
		md.find('select.sizefuente').val(zise.trim());
		if(colortexto!='')
		md.find('input.colortexto').removeClass('sincolor').val(colortexto).show().siblings('.chkcambiarcolor').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
			
		if(colorfondo!='')
		md.find('input.colorfondo').removeClass('sincolor').val(colorfondo).show().siblings('.chkcambiarcolor').addClass('fa-circle-o').removeClass('fa-check-circle').find('span').text('No');
			
		if(imagenfondo!='')md.find('img.imagenfondo').attr('src',imagenfondo).attr('data-oldmedia',imagenfondo);
		md.find('input.texto').on('change',function(){txt=$(this).val()});
		md.find('select.tipofuente').on('change',function(){fuente= $(this).val();});
		md.find('select.sizefuente').on('change',function(){zise=$(this).val();});
		md.find('input.colortexto').on('change',function(){colortexto=$(this).hasClass('sincolor')?'':$(this).val();});
		md.find('input.colorfondo').on('change',function(){colorfondo=$(this).hasClass('sincolor')?'':$(this).val();});
		md.find('.chkcambiarcolor').on('click',function(ev){
			let input= $(this).siblings('input');
			if($(this).hasClass('fa-circle-o')){
	        	$('span',this).text(' Si');	
	        	input.addClass('sincolor');
	       		input.hide().trigger('change');
	        	$(this).removeClass('fa-circle-o').addClass('fa-check-circle');
	      	}else{
	        	$('span',this).text(' No');
	        	input.removeClass('sincolor');
	        	input.val(input.attr('data-color')).show();
	        	input.trigger('change');
	        	$(this).addClass('fa-circle-o').removeClass('fa-check-circle');
	      	}
	      	ev.stopPropagation();});
		md.on('click','.subirimg',function(ev){
			let img=$(this).closest('.frmchangeimage').find('img');
			__subirmediamd($(this),'image/x-png, image/gif, image/jpeg, image/*','imagen',function(rs){
				img.attr('src',rs.media);
				img.attr('data-oldmedia',rs.media);
				imagenfondo=rs.media;
			});
		});
		md.find('.cicon[data-type="'+datatype+'"]').attr('data-oldmedia',datalink).children('i').removeClass('btn-primary').addClass('btn-success');
		md.on('click','.cicon',function(ev){
			ev.preventDefault();
		    let it=$(this);
		    let type=it.attr('data-type')||'';
		    //if(type===''||type==datatype) return false;
		    it.attr('data-oldmedia',datalink)
		    it.siblings().removeAttr('data-oldmedia').children('i.btn-success').removeClass('btn-success').addClass('btn-primary');		    	
		    datatype=type;
		    let _cont=$('#bookplantilla').children('#contentpages');
		    switch (type){
		    	case 'smarticlock':
			    	__addtics(it,1,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=link; 
			    	});
			        break;
			    case 'smarticlookPractice':
			    	__addtics(it,2,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=link; 
			    	});
			        break;
			    case 'smarticDobyyourselft':
			    	__addtics(it,3,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=link; 
			    	});
			        break;
			    case 'game':
			    	__addgame(it,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=link; 
			    	});
			        break;
			    case 'smartquiz':
			    	__addexamen(it,function(link){ 
			    		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=link; 
			    	});
			        break;			    
				case 'pdf':
				   	__subirmediamd(it,'application/pdf , application/zip','pdf',function(rs){
				   		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=rs.media;
			    		it.attr('data-oldmedia',rs.media);
				   	});				    	
				    break;
				case 'html':
				   	__subirmediamd(it,'.html , .htm , application/zip','html',function(rs){
				   		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=rs.media;	
			    		it.attr('data-oldmedia',rs.media);		    		
				   	});				    	
				    break;
				case 'flash':
					__subirmediamd(it,'.swf , .flv  , application/zip','flash',function(rs){
				   		it.children('i').addClass('btn-success').removeClass('btn-primary');
				   		it.attr('data-oldmedia',rs.media);
			    		datalink=rs.media;
				   	});
				    break;
				case 'audio':
				   	__subirmediamd(it,'audio/mp3','audio',function(rs){
				   		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=rs.media;
			    		it.attr('data-oldmedia',rs.media);
			    		item.attr('data-idinfotmp','aud'+__idgui());
				   	});				    	
				    break;
				case 'video':
				   	__subirmediamd(it,'video/mp4, video/*','video',function(rs){
				   		it.children('i').addClass('btn-success').removeClass('btn-primary');
			    		datalink=rs.media;
			    		it.attr('data-oldmedia',rs.media);
				   	});				    	
				    break;
				case 'imagen':
				    	__subirmediamd(it,'image/x-png, image/gif, image/jpeg, image/*','imagen',function(rs){
				    		it.children('i').addClass('btn-success').removeClass('btn-primary');
				    		item.attr('data-idinfotmp','img'+__idgui());
					   		it.attr('data-oldmedia',rs.media);
				    		datalink=rs.media;				    		
				    	});
				        break;
				 case 'indice':
				 		it.children('i').addClass('btn-success').removeClass('btn-primary');
				 		item.attr('data-idinfotmp','indice'+__idgui());
			    		datalink='';
				        break;
				case 'texto':
						it.children('i').addClass('btn-success').removeClass('btn-primary');
						item.attr('data-idinfotmp','txt'+__idgui());
				        datalink='';
				        break;

				case 'widgets':
						it.children('i').addClass('btn-success').removeClass('btn-primary');
						item.attr('data-idinfotmp','widgets'+__idgui());
			    		datalink='';
				    	/*_cont.html('<div class="widgetjson"></div><div class="widgetcont"></div>');
						$widcont=_cont.children('.widgetcont');
						var dt={data:{idcurso:idcurso,idcursodetalle:iddetalle},url:'/widgets/'};
						__cargarWidget(dt,$widcont);*/
				        break;
							 
				}
			ev.stopPropagation();
		}).on('click','.guardarcambios',function(ev){		    
		   	if(_isFunction(fncall)) fncall({
		    	texto:txt,				
				fuente:fuente,
				zise:zise,
				colortexto:colortexto,
				colorfondo:colorfondo,
				imagenfondo:imagenfondo,
				datatype:datatype,
				datalink:datalink
		    }); 
		   	md.modal('hide');
	    }).on('click','.cerrarmodal',function(){
			md.modal('hide');
		})
		md.find('.verpopover').popover({ trigger: 'hover',delay:100});
	}

	var _guardarhtml=function(obj){
		$('audio').trigger('pause');
		$('video').trigger('pause');
	    let stpl=$('#contentpages').find('.savetemplatedatainfo');
	    if(stpl.length){
	    	let id=stpl.attr('id');
	    	let info=stpl.find('.tplinfotmp');
	    	let datainfo={};
	    	$.each(info,function(i,v){
	    		vid=$(this).attr('data-texto');
	    		datainfo[vid]=$(this).html();
	    	})
	    	$('[data-idinfotmp="'+id+'"]').attr('data-datainfo',JSON.stringify(datainfo));
	    }

		let bookplantilla=$('#bookplantilla').clone(false);
		bookplantilla.find('audio').trigger('pause');
		bookplantilla.find('video').trigger('pause');
		let estilo=$('#mainestilo').attr('data-tmpurl')||'';
		let namecss=(bookplantilla.attr('data-vistatlp')||'smartbook')+'.css';
		bookplantilla.attr('data-estilo',estilo+namecss);

		let page=$('<div></div>');
		page.append(bookplantilla);		
		var html=page.html();
		var data=__formdata('');
		data.append('idcurso',idcurso);
		data.append('idcursodetalle',iddetalle);
		data.append('html',html);
		__sysajax({ 
			fromdata:data,
        	url:_sysUrlBase_+'/acad_cursodetalle/guardarSesionhtml',
        	callback:function(rs){
        		$('._btnversesion[data-iddetalle="'+iddetalle+'"]').attr('data-link','/'+rs.archivo);
        	}
    	});
	}

	$(document).ready(function(){
		var idcurso='<?php echo @$idcurso; ?>';
		var tmpmenu=<?php echo json_encode($this->menus); ?>;		
		let __updateindicetop=function(menu){
			let imen=1;
			let html='';
			$.each(tmpmenu,function(i,m){
				let link=m["html"]||'';
				let lin=(link!=undefined&&link!='')?' data-link="/'+link+'"':'';				
				html+='<div class="col-md-6 text-left hvr-glow _btnversesion" '+lin+' data-iddetalle="'+m["idcursodetalle"]+'">' +imen+'. '+ m["nombre"]+'</div>';				
				imen++;
			});
			$('#indicetop').html(html);
		}
		__updateindicetop();
		$('ul.toolbar1').on('click','.btnshowsesiones',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			$(this).closest('.toolbar1').toggleClass('active');
		}).on('click','.iconsesion',function(ev){
			ev.preventDefault();			
			let link=$(this).attr('data-link')||'';
			if(link==''){						
				var itema=$(this);
				__paneltools({
					item:itema,
					texto:'',
					fncall:function(rs){						
						if(rs.colorfondo=='') itema.attr('data-colorfondo','transparent').css({'background-color':'transparent'});					
						else  itema.attr('data-colorfondo',rs.colorfondo).css({'background-color':rs.colorfondo});
						if(rs.colortexto!='') itema.attr('data-colorfondo',rs.colortexto).css({'color':rs.colortexto});
						if(rs.imagenfondo.trim()!='')itema.css({'background-image':'url('+rs.imagenfondo+')','background-size':'100% 100%','min-width':'150px','border':'0px'}).attr('data-imagenfondo',rs.imagenfondo);
						else itema.css({'background-image':'none'});
						if(rs.datalink.trim()!='')itema.attr('data-link',rs.datalink);
						if(rs.datatype.trim()!='')itema.attr('data-type',rs.datatype);
						if(rs.fuente.trim()!='')itema.attr('data-fuente',rs.fuente).css({'font-family':rs.fuente});
						if(rs.zise.trim()!='')itema.attr('data-zise',rs.zise).css({'font-size':rs.zise})
						itema.attr('data-donde','contentpages');
						itema.trigger('click');
					}
				});
				ev.stopPropagation();
			}else{
				__sysajax({
					url:link,
					fromdata:{mostrarcargando:false},
					type:'html',
					donde:$('#bookplantilla'),		
					callback:function(rs){						
						let cont=$('<div>'+rs+'</div>');
						let tpl=cont.find('#bookplantilla');
						let cssfile=tpl.attr('data-cssfile')||'';
						let imgfondo=tpl.attr('data-imagenfondo')||'';
						let textocolor=tpl.attr('data-colortexto')||'';
						let fondocolor=tpl.attr('data-colorfondo')||'';
						if(cssfile!='')	$('#mainestilo').attr('href',$('#mainestilo').attr('data-tmpurl')+cssfile);
						if(imgfondo!='')$('body').css({'background-image':'url('+imgfondo+')'});
						else $('body').css({'background-image':'none'});
						if(textocolor!='')$('body').css({'color':textocolor});
						if(fondocolor!='')$('body').css({'background-color':fondocolor});

						$('#bookplantilla').html(tpl.html());
					}
				});	
			}
			ev.stopPropagation();
			//$('#mindice').find('li').first().next().find('.addcontent').trigger('click');			
		})

		$('body').on('click','._btnversesion',function(ev){
			ev.preventDefault();
			$('audio').trigger('pause');
			$('video').trigger('pause');
			obj=$(this);
			obj.addClass('pagevisto')
			url=obj.attr('data-link')||'';
			donde=$(this).attr('data-donde')||'contentpages';
			iddetalle=$(this).attr('data-iddetalle');
			if(url==''){
				alert('contenido no asigando');
				return false;
			}			
			__sysajax({
					url:_sysUrlMedia_+url,
					fromdata:{mostrarcargando:false},
					type:'html',
					donde:$('#bookplantilla'),		
					callback:function(rs){						
						let cont=$('<div>'+rs+'</div>');
						let tpl=cont.find('#bookplantilla');
						let cssfile=tpl.attr('data-cssfile')||'';
						let imgfondo=tpl.attr('data-imagenfondo')||'';
						let textocolor=tpl.attr('data-colortexto')||'';
						let fondocolor=tpl.attr('data-colorfondo')||'';
						if(cssfile!='')	$('#mainestilo').attr('href',$('#mainestilo').attr('data-tmpurl')+cssfile);
						if(imgfondo!='')$('body').css({'background-image':'url('+imgfondo+')'});
						else $('body').css({'background-image':'none'});
						if(textocolor!='')$('body').css({'color':textocolor});
						if(fondocolor!='')$('body').css({'background-color':fondocolor});
						$('#bookplantilla').html(tpl.html());
						//$('#bookplantilla').find('.menubooktop ul li:first a').trigger('click');
					}
				});
			ev.stopPropagation();
		})

		__cargarplantilla({
			url:'/disenio/',
			donde:'paneldisenio',
			data:{
				plantilla:'ninguno',
				idcurso:idcurso,
				idcursodetalle:iddetalle
				},
			fncall:function(rs){
				tpl=$('#bookplantilla');
				cssfile=tpl.attr('data-cssfile')||'';
				imgfondo=tpl.attr('data-imagenfondo')||'';
				textocolor=tpl.attr('data-colortexto')||'';
				fondocolor=tpl.attr('data-colorfondo')||'';
				if(cssfile!='') {
					$('#mainestilo').attr('href',$('#mainestilo').attr('data-tmpurl')+cssfile);
					$('body').css({'background-image':'url('+imgfondo+')'});
					$('body').css({'background-color':fondocolor});
					$('body').css({'color':textocolor});
				}
			}
		});
		$('body').click(function(){
			$('.toolbar1').removeClass('active');
		})
		
		$('.verpopover').popover({ trigger: 'hover',delay:100});
	});
</script>