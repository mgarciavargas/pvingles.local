<?php 
$idgui=uniqid();
$imgcursodefecto=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
?>
<style type="text/css">
	#cardsilabo ul#mindice li{
		padding:1ex 0px 0.5ex 0.5ex;
		list-style: decimal;
	}
	#cardsilabo	ul#mindice li .macciones{  
		float: right; display: none;
	}

	#cardsilabo ul#mindice li:hover{
		background: #f5eed69e;
	    border: 1px #f00 dashed;
	    cursor: pointer;	   
	}
	#cardsilabo ul#mindice li:hover li:hover{
		background:#8fff0033;
	}

	#cardsilabo ul#mindice li:hover li:hover li:hover{
		background:#f5e3d67a;
	}

	#cardsilabo ul#mindice li:hover > div > span.macciones{
		display: block;
	}
	.btniconxs{
		width: 23px; height: 23px; padding: 0.45ex;
	}
</style>
<div class="card text-center" id="cardsilabo">
	<div class="card-body">
    	<div class="row btnmanagermenu">
    		<div class="col-12">
    			<div class="row">
    				<div class="col-md-4 col-sm-6 col-xs-12">
						<a href="javascript:void(0)" class="importartemas btn btn-block bg-primary" style="color:#fff">
							<div><i class="fa fa-cloud-upload fa-2x "></i></div>												
							<p class="bolder">Importar Sesiones.</p>
						</a>
					</div>
    				<div class="col-md-4 col-sm-6 col-xs-12">
						<a href="javascript:void(0)" class="creartema btn btn-block bg-warning" style="color:#fff">
							<div><i class="fa fa-plus fa-2x "></i></div>
							<p class="bolder">Nueva Sesión</p>												
						</a>
					</div>                 				
    			</div>
    		</div>
    		<div class="col-12">
    			<hr>
    		</div>
    	</div>
    	<div class="row" id no >
    		<div class="col-12" id="paneldisenio" style="display: none;">
    			<form id="frmdatosdeltema" method="post" target="" enctype="multipart/form-data">
					<input type="hidden" name="idcurso" 		id="idcurso" value="0">
					<input type="hidden" name="idcursodetalle" 	id="idcursodetalle" value="0">
					<input type="hidden" name="idpadre" 		id="idpadre" value="0">
					<input type="hidden" name="orden" 			id="orden" value="0">

					<input type="hidden" name="idrecurso" 		id="idrecurso" value="0">
					<input type="hidden" name="idrecursopadre" 	id="idrecursopadre" value="0">
					<input type="hidden" name="idrecursoorden" 	id="idrecursoorden" value="0">

					<input type="hidden" name="tiporecurso" id="tiporecurso" value="0">
					<input type="hidden" name="idlogro" id="idlogro" value="0">				
					<input type="hidden" name="oldimagen" id="oldimagen" value="0">
					<input type="hidden" name="accmenu" id="accmenu" value="0">
					<input type="hidden" id="esfinal" name="esfinal" value="1" >

					<div class="card text-center shadow">				 
					  <div class="card-body">
					    <!--h5 class="card-title"><?php //echo JrTexto::_('Datos generales del curso');?></h5-->
					    <div class="row">
					    	<div class="col-12 col-sm-6 col-md-6">
					    		<div class="row">
						    		<div class="col-12 form-group">
							            <label style=""><?php echo JrTexto::_('Nombre del Tema(Sesión)');?></label> 
							            <input type="text" autofocus name="nombre" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Tema 001"))?>" >					           
							        </div>
							       
							        
							        <div class="col-12 form-group">
							            <label style=""><?php echo JrTexto::_('Descripción');?></label> 
							            <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del tema"))?>"></textarea>
							        </div>
							       							        
							        <div class="col-6 form-group text-left ">		        	
							            <label style=""><?php echo JrTexto::_('Color');?> </label> 
							            <input type="" name="color" value="transparent" class="form-control" >			       
							        </div>
							       	       
				                </div>		       
					    	</div>
					    	<div class="col-12 col-sm-6 col-md-6">
					    		<div class="row">
						    		<div class="col-12 form-group text-center">
							            <label style="float: none"><?php echo JrTexto::_('Portada');?></label> 
							            <div style="position: relative;" class="frmchangeimage">
							                <div class="toolbarmouse text-center">                  
							                  <span class="btn btnaddfile"><i class="fa fa-pencil"></i></span>
							                  <span class="btn resetfile"><i class="fa fa-trash"></i></span>
							                </div>			               
						                	<img id="verimg" src="<?php echo $imgcursodefecto; ?>" class="img-responsive center-block" style="width:60%; height:200px;">
				                            <input type="file" class="input-file-invisible" name="imagen" style="display: none;">			               
							                <small>Tamaño 250px x 360px</small>                
							            </div>				           
							        </div>
						        </div>
					    	</div>
					    </div>	    			   
					  </div>
					  <div class="card-footer text-muted">
					  	<button type="button" class="btncancelar btn btn-warning"><i class=" fa fa-close"></i> Cancelar</button>
						<button type="submit" class="btn btn-primary btnguardartema"><i class=" fa fa-save"></i> Guardar Tema</button>
					  </div>
					</div>
				</form>

    		</div>	
			<div class="col-12 text-left" id="plnindice">
				<li class="paraclonar" style="display: none">
					<div><span class="mnombre"></span>
					<span class="macciones">
						<!--i class="mmsubmenu fa fa-sort-down verpopoverm" data-content="Ocultar Submenus" ></i>
						<i class="mmsubmenu fa fa-sort-up verpopoverm" data-content="Mostrar Submenus" ></i>
						<i class="addcontent fa fa-eye verpopoverm" data-content="Ver contenido" data-placement="top"></i-->
						<i class="btn btniconxs btn-warning magregar fa fa-plus verpopoverm" data-content="Agregar SubSesión" data-placement="top"></i>
						<i class="btn btniconxs btn-primary meditar fa fa-pencil verpopoverm" data-content="Editar Sesión" data-placement="top"></i>
						<i class="btn btniconxs btn-danger meliminar fa fa-trash verpopoverm" data-content="Eliminar Sesión" data-placement="top"></i>
						<!--i class="mmove fa fa-arrows-alt verpopoverm" data-content="Mover Sesión" data-placement="right"></i>
						<i class="mcopylink fa fa-link verpopoverm" data-content="Copiar link" data-placement="right"></i-->
						<!--span style="display:inline-block">								
							<i class="mmoveup fa fa-angle-double-up verpopoverm" data-content="Mover Arriba" ></i>
							<i class="mmovedown fa fa-angle-double-down verpopoverm" data-content="Mover abajo"  ></i>
						</span-->
					</span>
					</div>
				</li>
				<ul id="mindice">
					
				</ul>
			</div>
		</div>
	</div>
	<div class="card-footer text-muted">
	  	<button type="button" class="btnbacktab btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
	    <button type="button" class="btnnexttab btn btn-primary">Continuar <i class="fa fa-arrow-right"></i></button>
	</div>
</div>
<script type="text/javascript">	
	$(document).ready(function(ev){
		//let idgui='<?php //echo $idgui; ?>';		
	})
</script>