<?php 
$idgui=uniqid(); 
$imgcursodefecto=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
$fileurl=is_file(RUTA_MEDIA.@$curso["imagen"])?URL_MEDIA.@$curso["imagen"]:$imgcursodefecto;
?>
<style type="text/css">
.activepestania{
	background-color: #ccc;
	color: #fff;
}
#ultabs{ background: #ecf2f9e0; padding: 0.25ex; }	
#ultabs li b.number{ font-size: 1.5em; }
#ultabs li.active{ background:#047bfc;}
#ultabs li.active a { color:#f7f1f1; }
#ultabs li.active:after { border-left: 16px solid #047bfc; }
#ultabs li.visto{ background:#047bfc;}
#ultabs li.visto a { color:#f7f1f1; }
#ultabs li.visto:after { border-left: 16px solid #047bfc; }
.vh0{ padding: 0px; }
#paso02 .paso2tabshow02 , #paso02 .pasotematabshow, ._opcionespestania{ display: none }
#paso02 .paso2tabshow02.active , #paso02 .pasotematabshow.active, ._opcionespestania.active { display: flex; }
.cicon{	font-size: 2em;  padding: 0.25ex 0.5ex;  text-align: center;	position: relative;}
.cicon i{ padding: 0.5ex 1ex;  width: 100%;  text-align: center; }
.cicon i span{ font-size: 15px; }
	/**/
ul.menumaintop{ display: inline-block; margin-bottom: 0px; padding-left:1ex; padding-top: 1ex; }
ul.menumaintop li{display: inline-block; padding-left: 1ex;}

.showindicepage{max-width: 440px; font-size: 1em; background: #2480dd; color: #333; }
ul.menumaintop ~ .showindicepage{ display: none; }	
ul.menumaintop.active ~ .showindicepage{ position: absolute; top:35px; left:0px; display: block; z-index: 9999; }
ul.menumaintop a{ margin-top:-5px}
ul.menumaintop a i{ font-size:2.5em}
.showindicepage .pnlsesions{
    color: #FFFFFF;
    width:100%;
    background-color:#0584E5;
    text-align:center;
}
.showindicepage .pnlsesions h2{padding: 8px 20px;}
.showindicepage ._btnversesion{
	color: #333;
    border: solid 2px #0584e5;
    min-height: 60px;
    width: 50%;
    padding: 10px;
    text-align: left;
    font-size: 1.25em;
    font-weight: bold;
	background-color:#dbdcdc;
}
.showindicepage ._btnversesion:hover{
	border: solid 2px #dbdcdc;
}

.indicetop{ margin: 1px; padding: 1px; padding-bottom: 2px; }
.indicetop > div{ padding: 5px; font-size: 14px; color: #000000cf; background: #fdf9f93d; border: 1px solid #f9f7f7a8; font-weight: bold; cursor: pointer;}

.indicetop .pagevisto:before{ content: "\f00c"; font-family: FontAwesome; font-style: normal; font-weight: normal; color:#28a745;
    font-size: 16px; padding-right: 0.5ex;}

.circular-menu{	margin: 0 auto;	position: relative;}
.menucircle{
	width: 100%;
  	height: 100%;
  	opacity: 0;
    -webkit-transform: scale(0);
  	-moz-transform: scale(0);
  	transform: scale(0);
  	-webkit-transition: all 0.4s ease-out;
  	-moz-transition: all 0.4s ease-out;
  	transition: all 0.4s ease-out;
  	margin: 0 auto;
  	position: relative;
}

.open.menucircle{
  	opacity: 1;
  	-webkit-transform: scale(1);
  	-moz-transform: scale(1);
  	transform: scale(1);
  	width: 100%;
  	height: 100%;
}

.menucircle .menucircleitem{
  	text-decoration: none;
  	background: #eee;
  	border-radius: 50%;
  	color: #bbb;
  	padding: 1ex;
  	display: block;
  	position: absolute;
  	text-align: center;
  	display: flex;
  	justify-content: center;
  	align-content: center;
  	flex-direction: column;
  	z-index: 2;
  	background-size: 100% 100% !important;
}

.menucircle .menucircleitem:hover{
    background: #ccc;
    border-radius: 50%;
    color: #fff;
    padding: 1ex;
    display: flex;
    justify-content: center;
    align-content: center;
    flex-direction: column;
    z-index: 3;
    background-size: 100% 100%;
}
.menucircle ~ .openmenucircle{
	position: absolute;
	cursor: pointer;
	text-decoration: none;
	text-align: center;
	color: #444;
	border-radius: 50%;
  	display: block;
  	padding: 1ex;
  	background: #dde;
  	display: flex;
    justify-content: center;
    align-content: center;
  	flex-direction: column;
  	background-size: 100% 100%;
}

.menucircle ~ .openmenucircle:hover{opacity: 0.75; background-size: 100% 100%;}
ul.sysmenumain li{
	/*z-index:2;*/
	font-size: 1.5em;
    text-shadow: 3px 1px 11px rgba(152, 150, 150, 1);
}
ul.sysmenumain li a{
	min-width:110px;
	text-align:center;
}
</style>

<div class="row">
	<div class="col-md-12" id="previewcurso" style="height:calc(100vh - 1px); margin: 0px; padding: 0px auto;">
		<link id="mainestilo" rel="stylesheet" type="text/css" data-tmpurl="<?php echo $this->documento->getUrlTema()."/css/"?>" href="<?php echo $this->documento->getUrlTema()."/css/smartbook.css"; ?>">				
		<div class="row tabstop" id="bookplantilla"  >
			<ul class="menumaintop">
		      <!--li><a class="noalumno btnconfigurarSesion" href="javascript:void(0)"><i class="fa fa-cog fa-2x"></i></a></li-->
		      <li><a class="btnshowexit" href="javascript:void(0)" data-link=""><i class="fa fa-sign-out"></i></a></li>
		      <li><a class="btnshowsesiones" href="javascript:void(0)"><i class="fa fa-th"></i></a></li>
		      <li><a class="btnshowhome" href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
		      <li class="nombrecurso"></li>				      	    	    
		    </ul>
		    <div class="showindicepage">
		    	<div class="pnlsesions text-center">
					<h2>Sesiones</h2>
					<div class="row indicetop">
						<div class="cloneindice col-md-6 text-left hvr-rectangle-out _btnversesion" hidden data-iddetalle="547"></div>								
					</div>
				</div>
		    </div>					
			<div class="marcopage">
				<div class="menubooktop">						
					<ul class="nav nav-tabs pull-right sysmenumain">						    	
					</ul>
				</div>
				<div class="anillado"></div>
				<div class="col-12" id="contentpages" style="padding: 0px;"></div>
			</div>
			<?php // var_dump($_REQUEST); ?>
		</div>
	</div>
</div>

<script type="text/javascript">
	var infocurso=<?php echo $this->curso; ?>;
	console.log(infocurso);
	var nombreusuario='<?php echo $this->usuarionombre;?>';
	var datosdelcurso=infocurso.datos||{};
	var temas=infocurso.temas||{};
	var chkjson=datosdelcurso.txtjson==''||datosdelcurso.txtjson=='1'?{}:datosdelcurso.txtjson;
	var jsoncurso=_isJson(chkjson)?JSON.parse(chkjson):{};
	console.log(jsoncurso);
	var idcurso=parseInt(datosdelcurso.idcurso||0);
	var datenow='<?php echo date('Y-m-d'); ?>';
	var imgdefecto='<?php echo $imgcursodefecto; ?>';
	var urlmedia='<?php echo URL_MEDIA; ?>';
	var pasoscurso=10;
	var plantilla=jsoncurso.plantilla||{id:0,nombre:'blanco'};
	var estructura=jsoncurso.estructura||{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:imgdefecto};
	var estilopagina=jsoncurso.estilopagina||{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:imgdefecto};
	var infoportada=jsoncurso.infoportada||{titulo:'',descripcion:'',image:imgdefecto};
	var infoindice=jsoncurso.infoindice||'top';
	var infoavance=jsoncurso.infoavance||0;	
	var curtemadefault={tipo:'#showpaddcontenido',imagenfondo:'',infoavancetema:0,colorfondo:'rgba(0,0,0,0)'};
	var curtema={index:0,idtema:0,txtjson:{tipo:'#showpaddcontenido',imagenfondo:'',infoavancetema:0,colorfondo:'rgba(0,0,0,0)'}};
	var infoavancetema=0;
	var curtemaidpadre=0;
	var curtemaindex=0;
	
	var curtemaoption={};
	var _sysUrlRaiz_='<?php echo URL_RAIZ;?>';
	var url_media='<?php echo URL_MEDIA;?>';

	$(document).ready(function(ev){
		var bookplantilla=$('#bookplantilla');
		var marcopage=bookplantilla.children('.marcopage');	
		var contentpages=marcopage.children('#contentpages');
		let addcontent=$('#showpaddcontenido._addcontenido #addcontentclone').clone();
		$('#editlistadoopcion #aquitambienaddcontent').append(addcontent);

		
		let _diseniarcurso=function(){
			let previewcurso=$('#previewcurso');
			let mainestilo=previewcurso.children('#mainestilo');
			let bookplantilla=previewcurso.children('#bookplantilla');			
			mainestilo.attr('href',mainestilo.attr('data-tmpurl')+plantilla.nombre+'.css?idtmp'+__idgui());
			//console.log(estructura);
			bookplantilla.css(estructura);
			disenioestiloP_(estilopagina);					
		}
		let _diseniarPortada=function(){
			let tmp=infoportada;					
			let previewcurso=$('#previewcurso');			
			let bookplantilla=previewcurso.children('#bookplantilla');
			let marcopage=bookplantilla.children('.marcopage');
			let contentPage=marcopage.children('#contentpages');
			img2=(tmp.imagen||'').replace(url_media,'');
		    let imagen=(img2=='undefined'||img2=='')?'':(url_media+img2);
			let htmlimagen=imagen!=''?'<div class="col-12 tituloPortada"><img src="'+imagen+'" class="img im-responsive img-thumbnail" style="max-height:350px; max-width:350px;"></div>':'';
			html='<div class="row text-center">';
			html+='<div class="col-12 align-middle"><br><br><br><h2>'+tmp.titulo+'</h2>';
			html+='<br><br><h5>'+tmp.descripcion+'</h5><br><hr><br>';
			html+=htmlimagen;
			html+='<br><br><b>Autor: '+(infocurso.datos.autor||'')+'</b><br><hr>';
			html+='</div>';
			img2=(tmp.imagenfondo||'').replace(url_media,'');
		    let imagenfondo=(img2=='undefined'||img2=='')?'':(url_media+img2);
			let cssadd={'background-image':'url('+imagenfondo+')','background-color':tmp.colorfondo||'','font-family':tmp.tipotexto||''};
			contentPage.css(cssadd);
			contentPage.html(html);
		}
		let _diseniarIndice=function(mostrar){
			_show=mostrar||false;			
			let bookplantilla=$('#previewcurso').children('#bookplantilla');
			let mainmenutop=bookplantilla.children('.menumaintop');
			let btnshowsesiones=mainmenutop.find('.btnshowsesiones').attr('data-menuen',infoindice);
			let pnltmp=bookplantilla.children('.showindicepage').children('.pnlsesions').children('.indicetop');
			if(temas.length>0){
				pnltmp.children('.cloneindice').siblings().remove();
				j=0;				
				$.each(temas,function(i,v){ 
					let jsontmp=JSON.parse(v.txtjson);
					let txti='';
					if(jsontmp.typelink!='smartquiz'){
						j++;
						txti='<b  style="font-size: 1.25em;">'+j+'.</b> ';
					}else txti='<i class="fa fa-list" style="font-size: 1.5em;"></i> ';					
					let indiceclon=pnltmp.children('.cloneindice').clone(true);
					indiceclon.removeClass('cloneindice').removeAttr('hidden').attr('data-iddetalle',v.idcursodetalle);
					indiceclon.html(txti+v.nombre);
					pnltmp.append(indiceclon);							
				})				
			}
			if(mostrar==true)
            if(infoindice=='top'){
            	mainmenutop.removeClass('active').addClass('active');
            	contentpages.html('');
            }else if(infoindice=='enpagina'){
            	mainmenutop.removeClass('active');            	
				let pnltmp=bookplantilla.children('.showindicepage').clone(true);
				contentpages.html('<div class="row" style="margin-top:1em;"><div class="col-md-3 col-sm-12 col-xs-12"></div><div id="addindice" class="col-md-6 col-sm-12 col-xs-12"></div><div class="col-md-3 col-sm-12 col-xs-12"></div></div>');
				pnltmp.find('._btnversesion').removeClass('col-md-6').addClass('col-12');
				contentpages.find('#addindice').append(pnltmp);
            }
		}
		let disenioestiloP_=function(estilopagina){
			let previewcurso=$('#previewcurso');
			let bookplantilla=previewcurso.children('#bookplantilla');
			let marcopage=bookplantilla.children('.marcopage');
			marcopage.css(estilopagina);
		}

		let __disenioaddcontenido=function(){
			let vercurtema=curtema.txtjson;			
			if(vercurtema.tipo=='#showpaddcontenido'){
				let link=vercurtema.link;
				let typelink=vercurtema.typelink||'';
				if(link!=''&&typelink!=''){
					let data=new FormData();					
					__sysajax({
						fromdata:{link:link},
						type:'html',
						showmsjok:false,
	        	    	url:_sysUrlBase_+'/plantillas/'+typelink,
	        	    	callback:function(rs){
	        	    		contentpages.html(rs);
	        	    	}
	        	    })
				}
   			}
		}

		let __disenioaddpestanias=function(tmpsesion){		
			let tmpcurtema={};
			let vercurtema=curtemadefault;	
			let mostrartab=false;		
			if(tmpsesion!=undefined){				
				tmpcurtema=tmpsesion;
				vercurtema=tmpsesion.txtjson;		
			}else{
				tmpcurtema=curtema;
				vercurtema=curtema.txtjson;
			}
			contentpages.html('');			
			if(vercurtema.tipo=='#showpaddopciones'){
				let tipofile=vercurtema.tipofile;
				marcopage.removeClass('arriba abajo derecha circulo menus').addClass(tipofile);
				let options=vercurtema.options||{};
				let hijos=tmpcurtema.hijos||{};				
				if(tipofile=='arriba'||tipofile=='derecha'||tipofile=='abajo'){					
					let pnlmenu=marcopage.children('.menubooktop').children('ul.sysmenumain');
					pnlmenu.find('li').remove();
					mostrartab=true;					
					if(hijos.length){						
						$.each(hijos,function(i,v){
							//console.log(v);						
							let li='<li data-iddetalle="'+v.idcursodetalle+'" class="_btnversesion nav-item hvr-pop" ><a class="nav-link" href="javascript:void(0)">'+v.nombre+'</a></li>';
							pnlmenu.append(li);
						})
					}else{						
						$.each(options,function(i,v){
							img2=(v.imagenfondo||'').replace(url_media,'');
							img2=img2=='/static/media/nofoto.jpg'?'':img2;
		    				let imagen=(img2=='undefined'||img2=='')?'':(url_media+img2);

							let htmlimagen=imagen!=''?'<div class="col-12 tituloPortada"><img src="'+imagen+'" class="img im-responsive img-thumbnail" style="max-height:35px; max-width:250px;"></div>':v.nombre;
     						let li='<li  data-imagenfondo="'+(v.imagenfondo||'')+'"  data-imagenfondopagina="'+(v.imagenfondopagina||'')+'" data-color="'+v.color+'" data-colorfondopagina="'+(v.colorfondopagina||'')+'" data-colorfondo="'+v.colorfondo+'" data-link="'+v.link+'" data-type="'+v.type+'" class="_vercontenido_ nav-item hvr-grow" style="background-color:'+v.colorfondo+'"><a class="nav-link" style="color:'+v.color+'" href="javascript:void(0)">'+htmlimagen+'</a></li>';
							pnlmenu.append(li);
						})
					}
					if(mostrartab==true){
						//console.log('cragarhijo por defecto');
						marcopage.children('.menubooktop').children('ul.sysmenumain').children('li:first-child').trigger('click');
					}
				}else if(tipofile=='menus'){					
					
					let html='<div class="row" style="margin-top:1em;"><div class="col-md-3 col-sm-12 col-xs-12"></div>';
					html+='<div class="col-md-6 col-sm-12 col-xs-12"><div class="showindicepage">';
					if(hijos.length){
						html+='<div class="pnlsesions text-center"><h3>Sesiones :'+tmpcurtema.nombre+'</h3></div>';
						$.each(hijos,function(i,v){
							html+='<div class="row indicetop"><div data-iddetalle="'+v.idcursodetalle+'" class="_btnversesion col-12 text-left hvr-glow" >'+i+'. '+v.nombre+'</div></div>';
						})
					}else{

						html+='<div class="pnlsesions text-center"><h3>Opciones :'+tmpcurtema.nombre+'</h3></div>';					
						$.each(options,function(i,v){
							html+='<div class="row indicetop"><div data-color="'+v.color+'" data-colorfondo="'+v.colorfondo+'" data-link="'+v.link+'" data-type="'+v.type+'" class="_vercontenido_ col-12 text-left hvr-glow" style="color:'+v.color+'">'+i+'. '+v.nombre+'</div></div>';						
						})
					}
					html+='</div></div></div><div class="col-md-3 col-sm-12 col-xs-12"></div></div>';
					contentpages.html(html);			
				}else if(tipofile=='circulo'){
					let idgui=__idgui();
					let html='<div class="widgetcont" style="height: calc(100vh - 100px);"><div class="row"><div class="col-12 text-center"><h3>'+tmpcurtema.nombre+'</h3></div></div>';
					html+='<div class="row" style="margin: 0px; padding: 0px;"><div class="col-12"><div class="nav circular-menu"><div class="menucircle open" id="menucircle'+idgui+'"> ';
					if(hijos.length){						
						$.each(hijos,function(i,v){
							let colorfondo=v.colorfondo||'rgba(0,0,0,0)';
							html+='<div data-iddetalle="'+v.idcursodetalle+'" class="_btnversesion menucircleitem" style="background-color:'+colorfondo+'; color:'+v.color+'"><span>'+(i+1)+'. '+v.nombre+'</span></div>';	

						})
					}else{
						$.each(options,function(i,v){
							let colorfondo=v.colorfondo||'rgba(0,0,0,0)';
							html+='<div data-color="'+v.color+'" data-colorfondo="'+colorfondo+'" data-link="'+v.link+'" data-type="'+v.type+'" class="_vercontenido_ menucircleitem" style="background-color:'+colorfondo+'; color:'+v.color+'"><span>'+(i+1)+'. '+v.nombre+'</span></div>';					
						})
					}
					html+='</div><span class="openmenucircle"><span>Menus</span></span>';
					html+='</div></div></div></div';
					contentpages.html(html);
					var opt={idcurso:idcurso,idcursodetalle:tmpcurtema.idtema}
					setTimeout(function(){$('#menucircle'+idgui).menucircle(opt);},450);
					
				}				
			}
		}

		var _buscartemas=function(itemas,idcurdet){
			let curt={};
			let icont=false;
			let rdt={dato:{},icon:false};	
			if(itemas.length>0){				
				itemas.forEach(function(cur,i,v){
					if(icont==false){																
						if(parseInt(cur.idcursodetalle)==parseInt(idcurdet)){
							icont=true;
							rdt={dato:cur,icont:true};
						}else{							
							let hijos=cur.hijos||[];
							dt=_buscartemas(hijos,idcurdet);
							if(dt.icont==true){
								icont=true;
								rdt={dato:dt.dato,icont:true};
							}
						}
					}
				});
			}
			return rdt;			
		}

		let __temamostrar=function(){				
			let pnledittema=$('#showtemaidedit');
			$('.paso2tabshow02').removeClass('active');
			pnledittema.addClass('active');
			pnledittema.find('.pasotematabshow').removeClass('active');
			pnledittema.find('#addtemacontenido').addClass('active');
			let txttemajson=curtemadefault;
			let tmptema=curtema;
			if(curtema.txtjson!='') txttemajson=_isJson(curtema.txtjson)?JSON.parse(curtema.txtjson):curtemadefault;

			let colorfondo=txttemajson.colorfondo==''?'rgba(0,0,0,0)':txttemajson.colorfondo;
			let imagenfondo=txttemajson.imagenfondo;
			curtema.idtema=curtema.idcursodetalle;
			curtema.txtjson=txttemajson;			
			infoavancetema=txttemajson.infoavancetema;			
			updatetema_(temas,curtema.idcursodetalle,curtema);

			pnledittema.find('h3.nombretema').text(curtema.nombre);			
			let addcont=pnledittema.find('#addtemacontenido');
			addcont.find('input[name="fondocolor"]').val(colorfondo);
			addcont.find('input[name="fondocolor"]').minicolors({opacity: true,format:'rgb'});			
			addcont.find('input[name="fondocolor"]').minicolors('settings',{value:colorfondo});						
			addcont.find('img#verimg').attr('src',imagenfondo!=''?imagenfondo:imgdefecto);

			addcont.find('.btntemaaddinfo').removeClass('active btn-success').addClass('btn-secondary');
			addcont.find('.btntemaaddinfo[data-showp="'+txttemajson.tipo+'"]').addClass('active btn-success').removeClass('btn-secondary');
			disenioestiloP_({'background-color':colorfondo,'background-image':'url('+imagenfondo+')'})			
			$('#previewcurso #bookplantilla .marcopage #contentpages').html('');
			updateprogresscurso();
			if(curtema.txtjson.tipo=='#showpaddcontenido') __disenioaddcontenido();
			else if(curtema.txtjson.tipo=='#showpaddopciones') __disenioaddpestanias();
		}

		var buscartema2_=function(itemas,buscar,idpadre){			
			idpadre=idpadre||0;
			let encontrado=false;
			let rdt={dato:{},encontrado:false};
			if(itemas.length>0){			
				itemas.forEach(function(cur,i,v){					
					if(encontrado==false){
						cur.index=i;
						if(cur.idcursodetalle==buscar.idet && buscar.por=='idet'){
							encontrado=true;
							cur.index=i;
							if(cur.txtjson=='')cur.txtjson=curtemadefault;							
							rdt={dato:cur,encontrado:true};
						}else if(i==buscar.index && buscar.por=='index' && cur.idpadre==idpadre){
							encontrado=true;
							cur.index=i;
							if(cur.txtjson=='')cur.txtjson=curtemadefault;
							rdt={dato:cur,encontrado:true};
						}else{
							let hijos=cur.hijos||[];							
							if(hijos.length>0){								
								dt=buscartema2_(hijos,buscar,idpadre);								
								if(dt.encontrado==true){
									encontrado=true;
									rdt={dato:dt.dato,encontrado:true};
								}
							}
						}
					}
				})
			}
			return rdt;
		}

		
		var _vertemapadreohermano=function(tmpcurtema){
			curtemaidpadre=tmpcurtema.idpadre;
			curtemaindex=tmpcurtema.index+1;
			let tmpcurpadre=buscartema2_(temas,{por:'idet',idet:curtemaidpadre},curtemaindex).dato;
			curtemaidpadre=tmpcurpadre.idpadre;
			curtemaindex=tmpcurpadre.index+1;
			if(tmpcurpadre.idcursodetalle!=undefined){
				let curtema2=buscartema2_(temas,{por:'index',index:curtemaindex},curtemaidpadre).dato;
				if(curtema2.idcursodetalle==undefined)
					curtema2=_vertemapadreohermano(tmpcurpadre);
				return curtema2;
			} else tmpcurpadre;
		}
		
		var _siguienteteam=function(){
			let tmpcurtema=curtema;
			curtemaindex=tmpcurtema.index;
			curtemaidpadre=tmpcurtema.idpadre;
			let hijos=tmpcurtema.hijos||[];
			let otrotema=false;
			if(hijos.length){
				curtema=hijos[0];				
				curtemaindex=curtema.index;
				curtemaidpadre=curtema.idpadre;				
				otrotema=true;
			}else{
				curtemaindex++;
				let tmpcurtema2=buscartema2_(temas,{por:'index',index:curtemaindex},curtemaidpadre).dato;
				if(tmpcurtema2.idcursodetalle==undefined){
					if(curtemaidpadre==0) return false;					
					let tmpcurtema3=_vertemapadreohermano(tmpcurtema);
					if(tmpcurtema3.idcursodetalle==undefined) otrotema=false;
					else {
						curtema=tmpcurtema3;						
						otrotema=true;
					}
				}else{
					curtema=tmpcurtema2;
					otrotema=true;
				}
			}			
			return otrotema;
		}

		var _backtema=function(){
			curtemaindex=curtema.index;
			curtemaidpadre=curtema.idpadre;
			if(curtemaindex==0 && curtemaidpadre==0) return false;
			curtemaindex--;
			if(curtemaindex>=0){
				tmpcurtema=buscartema2_(temas,{por:'index',index:curtemaindex},curtemaidpadre).dato;
				if(tmpcurtema.idcursodetalle==undefined) return false;
				else{
					curtema=tmpcurtema;
					return true;
				}
			}else{				
				curtema=buscartema2_(temas,{por:'idet',idet:curtema.idpadre},curtema.idpadre).dato;				
				if(curtema.idcursodetalle==undefined) return false;
				else{					
					return true;
				}
			}
		}



		$('#showindice').find('.btnplantillaindice[data-nombre="'+infoindice+'"]').children('a').addClass('btn-success');

		/* Para manejo de la pagina */
		$('ul.menumaintop').on('click','.btnshowsesiones',function(ev){
			ev.preventDefault();
			let tipomenu=$(this).attr('data-menuen')||'top';
			if(tipomenu=='top')	
				$(this).closest('ul').toggleClass('active');
			else{
				$(this).closest('ul').removeClass('active');
				let bookplantilla=$('#previewcurso').children('#bookplantilla');
				let pnltmp=bookplantilla.children('.showindicepage').clone(true);
				let marcopage=bookplantilla.children('.marcopage');
				let contentPage=marcopage.children('#contentpages');
				contentPage.html('<div class="row" style="margin-top:1em;"><div class="col-md-3 col-sm-12 col-xs-12"></div><div id="addindice" class="col-md-6 col-sm-12 col-xs-12"></div><div class="col-md-3 col-sm-12 col-xs-12"></div></div>');
				pnltmp.find('._btnversesion').removeClass('col-md-6').addClass('col-12');
				contentPage.find('#addindice').append(pnltmp);
			}
			ev.stopPropagation();
		}).on('click','.btnshowhome',function(ev){
			ev.preventDefault();			
			_diseniarPortada();
			ev.stopPropagation();
		}).on('click','.btnshowexit',function(ev){
			//window.parent.close();
			if(confirm("Va a cerrar la pestana")) close();
			window.open('','_parent',''); 
   			window.close();
		})

		$('#previewcurso').on('click','._vercontenido_',function(ev){
			ev.preventDefault();
			let menu=$(this);
			let link=menu.attr('data-link');
			let typelink=menu.attr('data-type')||'';
			let img=menu.attr('data-imagenfondo')||'';
			img=img=='/static/media/nofoto.jpg'?'':img;
			let cssadd={'background-image':'url('+img+')','color':(menu.attr('data-color')||''),'background-color':(menu.attr('data-colorfondopagina')||'')};
			
			let colorfondo='';
			let imagenfondo='';
			link=link.replace('://www.','://');
			if(link!=''&&typelink!='')
			__sysajax({
				fromdata:{link:link},
				type:'html',
				showmsjok:false,
    	    	url:_sysUrlBase_+'/plantillas/'+typelink,
    	    	callback:function(rs){					
    	    		contentpages.html(rs).css(cssadd);
    	    	}
    	    })
		}).on('click','._btnversesion',function(ev){
			ev.preventDefault();			
		    let iddet=$(this).attr('data-iddetalle');
		    tmpsesion=buscartema2_(temas,{por:'idet',idet:iddet},0).dato;
			
		    if(tmpsesion.idcursodetalle!=undefined){
				let pnlmenu=marcopage.children('.menubooktop').children('ul.sysmenumain');
					pnlmenu.find('li').remove();		    	
		    	try{if(typeof(tmpsesion.txtjson)=='string')tmpsesion.txtjson=JSON.parse(tmpsesion.txtjson);}catch(ex){}		    	
				if(tmpsesion.txtjson.tipo=='#showpaddcontenido'){
					let link=tmpsesion.txtjson.link;
					let typelink=tmpsesion.txtjson.typelink||'';
					link=link.replace('://www.','://');
					if(link!=''&&typelink!='')
					__sysajax({
						fromdata:{link:link},
						type:'html',
						showmsjok:false,
	        	    	url:_sysUrlBase_+'/plantillas/'+typelink,
	        	    	callback:function(rs){
	        	    		contentpages.html(rs);
	        	    	}
	        	    });
	   			}else if('#showpaddopciones')
		    	__disenioaddpestanias(tmpsesion);
		    }
		})

		$(document).on('click','body',function(ev){
			$('#bookplantilla').children('.menumaintop').removeClass('active');
		})
		
		_diseniarcurso();
		_diseniarIndice({},false);
		$('ul.menumaintop .btnshowhome').trigger('click');
	})
</script>