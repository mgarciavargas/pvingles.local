<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .breadcrumb {background-color:rgb(64, 113, 191); padding: 1ex;  }
  .breadcrumb a{ color: #fff; }
  .breadcrumb li.active{ color:#d2cccc; }
  .panel-user{ padding:0.4ex; border: 1px solid rgba(90, 137, 248, 0.41); position: relative; margin-bottom: 1ex; }
  .panel-user .item{ height:150px; overflow: auto; width: 100%;}
  .panel-user .item img{ max-height: 100%; max-width: 100%; }
  .panel-user .pnlacciones{ display: none;}
  .panel-user.active .pnlacciones{ display: block; top: 0px;     position: absolute; width: 100%;}
  .panel-user .nombre{ text-align: center; font-size: 1em; max-height: 60px; overflow: hidden;}
  .form-group{ margin-bottom: 0px; }
  .input-group { margin-top: 0px; }
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_('SmartQuiz'); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="row" id="ventana-abc1">
  <div class="col-md-12">
    <div class="panel mostrarcollapse activeshow" style="border: 1px solid #dad7d7;   margin-bottom:0px;  ">
      <div class="panel-heading bg-primary">
        <div class="row">
        <div class="col-md-9 form-group">
          <div class="input-group">
            <input type="text" name="texto" id="textoabc1" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
            <span class="input-group-addon btn btnbuscar btn btn-success" style="margin-top: 0px;"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
          </div>
        </div>
        <div class="col-md-3 form-group">
          <button class="cerrarmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar</button>
        </div>
        </div>
      </div>
      <div class="panel-body">            
        <div class="row" id="datalistadoabc1">
          <div id="cargando" style="display: none;">cargando</div>
          <div id="sindatos" style="display: none;">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <div class="jumbotron">
                    <h1><strong>Opss!</strong></h1>                
                    <div class="disculpa"><?php echo JrTexto::_('Empty data')?>.</div>                
                </div>
            </div>
          </div>
          <div id="error" style="display: none;">Error</div>
          <div class="row col-12" id="data"></div>
        </div>
      </div>
    </div>
  
  <div id="controlesabc1" style="display: none;">
    <div class="pnlacciones text-center">
     <a class="btn-selected btn btn-success btn-xs" title="<?php echo ucfirst(JrTexto::_('Selected ')).' '.JrTexto::_('Assessment'); ?>"><i class="btnselected fa fa-hand-o-down"></i></a>      
      <!--a class="btnvermodal btn btn-danger btn-xs" title="<?php echo ucfirst(JrTexto::_('Edit')); ?>"><i class="fa fa-pencil"></i></a--> 
      <!--a class="btn-eliminar btn btn-danger  btn-xs" title="<?php //echo ucfirst(JrTexto::_('Remove')).' '.JrTexto::_('Certificate'); ?>"><i class="fa fa-trash-o"></i></a-->
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
var tabledatosabc1='';
function refreshdatosabc1(){
    tabledatosabc1();
}
$(document).ready(function(){  
  var estadosabc1={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>'}; 
  $('#ventana-abc1')
  .on('click','.btnbuscar',function(ev){
    refreshdatosabc1();
  }).on('keyup','#textoabc1',function(ev){
    if(ev.keyCode == 13)
    refreshdatosabc1();
  }).on("mouseenter",'.panel-user', function(){
      if(!$(this).hasClass('active')){ 
        $(this).siblings('.panel-user').removeClass('active');
        $(this).addClass('active');
      }
  }).on("mouseleave",'.panel-user', function(){     
      $(this).removeClass('active');
  }).on("click",'.btn-selected', function(){
      $(this).addClass('active');
      var idgame=$(this).closest('.panel-user').attr('data-id');
      <?php 
        if(!empty($ventanapadre)){?>
          var btn=$('.<?php echo $ventanapadre; ?>');
          if(btn.length){         
            btn.attr('data-idgame',idgame);
            btn.trigger("addgame");
          }
          <?php }  ?>
     $(this).closest('.modal').find('.cerrarmodal').trigger('click');         
  });

  tabledatosabc1=function(){
    var frm=document.createElement('form');
    var formData = new FormData();
        formData.append('nombre', $('#textoabc1').val()); 
        var url= _sysUrlBase_+'/../tools/json_buscarGames';
       
        $.ajax({
          url: url,
          type: "GET",
          //data:  formData,
          contentType: false,
          processData: false,
          dataType :'json',
          cache: false,
          beforeSend: function(XMLHttpRequest){
            $('#datalistadoabc1 #cargando').show('fast').siblings('div').hide('fast');
          },      
          success: function(res)
          {  
                var html='';
                var controles=$('#controlesabc1').html();                         
                var midata=res.data;
                if(midata.length){
                  var html='';
                  var controles=$('#controlesabc1').html();
                  $.each(midata,function(i,v){
                    let titulo=v.titulo||'';                 
                    if(titulo!==''){                    
                    html+='<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" data-id="'+v.idtool+'" style="margin-bottom: 1ex;">'+controles;
                    //html+='<div class="item"><img class="img-responsive" src="'+srcimg+'" width="100%">'; 
                    html+='<div class="nombre"><strong>'+v.titulo+'</strong></div>';
                    html+='</div></div></div>';
                  }
                });
                $('#datalistadoabc1 #data').html(html).show('fast').siblings('div').hide('fast');
              }else     
                $('#datalistadoabc1 #sindatos').show('fast').siblings('div').hide('fast');
           
          },
          error: function(e) 
          {
            $('#datalistadoabc1 #error').show('fast').siblings('div').hide('fast');
          }
        });
  }
  tabledatosabc1();
});
</script>