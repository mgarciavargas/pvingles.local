<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$curso=!empty($this->curso)?$this->curso:'';
$idcurso=!empty($curso["idcurso"])?$curso["idcurso"]:'';
?>
<style type="text/css">
	ol.m1{list-style: none; padding: 0px; margin-bottom: 0px;}
	ol.m1 li { list-style: none; display: inline-block; margin: -3px;  padding: 1ex 1em; border-radius: 1ex 3ex 1px 0px; min-width: 120px; }
	ol.m1 li a{color:#FFF; display: block; text-align: center; text-decoration: none;   position: relative;   z-index: 0;  }
   	ol.m1 li.active, ol.m1 li:hover{ z-index: 1;  border-bottom: 10px solid ; }
	#cntm1{ /*margin-top:-5px; border:1px solid; border-top: 10px solid;	/*padding-top: 1ex;*/ }
   	.titulocurso{ color:#fff; padding: 1ex; }
   	.paraclonar{display:none}
   	.fa-trash{color:#e2160a}
   	.fa-plus{color:#fbbe0b;}
   	.fa-pencil{color:#3e3c3c;}
   	.fa-eye{color:#6cdcf7;}
</style>
<input type="hidden" name="idcurso" id="idcursoprincipal" value="<?php echo $idcurso;  ?>">
<div class="row">
	<div class="col-12" style="display: none;">	
		<ol class="m1">			
			<li  class="bg-primary border-primary hvr-float-shadow">
				<a data-color="primary" data-url="/peditar" class="hvr-grow-rotate" href="javascript:void(0)">Editar</a></li>
			<li  class="bg-success border-success hvr-float-shadow">
				<a data-color="success" data-url="/pconfig" class="hvr-grow-rotate" href="javascript:void(0)">Configurar</a></li>
			<li  class="bg-warning border-warning hvr-float-shadow">
				<a data-color="warning" data-url="/plantillas" class="hvr-grow-rotate" href="javascript:void(0)">Plantillas</a></li>
			<li class="bg-danger border-danger hvr-float-shadow"><a id="salir" class="hvr-grow-rotate" href="javascript:void(0)">Salir</a></li>
			<li class="bg-primary border-primary hvr-float-shadow crearcurso" style="display: none">
				<a data-color="primary" data-url="/peditar/curso/" class="hvr-grow-rotate" href="javascript:void(0)">Crear curso</a></li>
		</ol>
	</div>
	<div class="col-12" id="cntm1">
		
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('ol.m1 li').click(function(ev){
			if($(this).hasClass('active')) return false;
			var dm=$(this).children('a');
			var color=dm.attr('data-color');
			var obj={
				url: dm.attr('data-url'),
				donde:'cntm1',
				data:{
						color:color,
						idcurso:$('#idcursoprincipal').val()
					}
			}
			$(this).addClass('active').siblings('li').removeClass('active');
			$('#cntm1').removeAttr('class').addClass('col-12 border-'+color);
			__cargarplantilla(obj);
		});
		<?php if(!empty($curso)){ ?>
			$('.m1 li:first-child').trigger('click');
		<?php }else{?>
			$('.m1 li.crearcurso').trigger('click');
		<?php } ?>
	})
</script>
