<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Acad_cursodetalle"); ?></li>
  </ol>
</nav>
<?php } ?><div class="form-view" id="vent_<?php echo $idgui; ?>" >
  <div class="row b1 border-primary">
                                                                                                                                                                          
                     <div class="col-12 col-sm-6 col-md-4 form-group">
                      <label><?php echo ucfirst(JrTexto::_("esfinal"));?></label>
                      <div class="cajaselect">
                      <select name="esfinal" id="esfinal<?php echo $idgui;?>" class="form-control">
                          <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                          <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                          <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                      </select></div>
                    </div>
                    

    <div class="col-12 col-sm-6 col-md-4 form-group"><br>
      <div class="input-group">
      <input type="text" name="texto" id="texto<?php echo $idgui; ?>" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
      <div class="input-group-append"><button class="btn btnbuscar<?php echo $idgui; ?> btn-primary"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></button></div>
      <div class="input-group-append"><a class="btn btn-warning btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Acad_cursodetalle", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Acad_cursodetalle").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('add')); ?> </a></div>


    </div>
    </div>    
</div>

  <div class="row">
    <div class="col table-responsive b1 border-primary">
        <table class="table table-striped  table-responsive table-hover">
              <thead>
                <tr class="bg-primary">
                  <th scope="col">#</th>
                  <th scope="col"><?php echo JrTexto::_("Idcurso") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Orden") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Idrecurso") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Tiporecurso") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Idlogro") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Url") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Idpadre") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Color") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Esfinal") ;?></th>
                    <th scope="col" class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5a7e405b0341c='';
function refreshdatos5a7e405b0341c(){
    tabledatos5a7e405b0341c.ajax.reload();
}
$(document).ready(function(){  
  var estados5a7e405b0341c={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5a7e405b0341c='<?php echo ucfirst(JrTexto::_("acad_cursodetalle"))." - ".JrTexto::_("edit"); ?>';
  var draw5a7e405b0341c=0;

  
  $('#esfinal<?php echo $idgui;?>').change(function(ev){
    refreshdatos5a7e405b0341c();
  });  
  $(".btnbuscar<?php echo $idgui; ?>").click(function(ev){
    refreshdatos5a7e405b0341c();
  }).keyup(function(ev){
    var code = ev.which;
    if(code==13){
      ev.preventDefault();
      refreshdatos5a7e405b0341c();
    }
  });
  tabledatos5a7e405b0341c=$('#vent_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Idcurso") ;?>'},
            {'data': '<?php echo JrTexto::_("Orden") ;?>'},
            {'data': '<?php echo JrTexto::_("Idrecurso") ;?>'},
            {'data': '<?php echo JrTexto::_("Tiporecurso") ;?>'},
            {'data': '<?php echo JrTexto::_("Idlogro") ;?>'},
            {'data': '<?php echo JrTexto::_("Url") ;?>'},
            {'data': '<?php echo JrTexto::_("Idpadre") ;?>'},
            {'data': '<?php echo JrTexto::_("Color") ;?>'},
            {'data': '<?php echo JrTexto::_("Esfinal") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/acad_cursodetalle/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.esfinal=$('#cbesfinal').val(),
             //d.texto=$('#texto').val(),
                        
            draw5a7e405b0341c=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5a7e405b0341c;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Idcurso") ;?>': data[i].idcurso,
              '<?php echo JrTexto::_("Orden") ;?>': data[i].orden,
                '<?php echo JrTexto::_("Idrecurso") ;?>': data[i].idrecurso,
                '<?php echo JrTexto::_("Tiporecurso") ;?>': data[i].tiporecurso,
              '<?php echo JrTexto::_("Idlogro") ;?>': data[i].idlogro,
                '<?php echo JrTexto::_("Url") ;?>': data[i].url,
              '<?php echo JrTexto::_("Idpadre") ;?>': data[i].idpadre,
              '<?php echo JrTexto::_("Color") ;?>': data[i].color,
              '<?php echo JrTexto::_("Esfinal") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="esfinal"  data-id="'+data[i].idcursodetalle+'"> <i class="fa fa'+(data[i].esfinal=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5a7e405b0341c[data[i].esfinal]+'</a>',
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/acad_cursodetalle/editar/?id='+data[i].idcursodetalle+'" data-titulo="'+tituloedit5a7e405b0341c+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idcursodetalle+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#vent_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'acad_cursodetalle', 'setCampo', id,campo,data);
          if(res) tabledatos5a7e405b0341c.ajax.reload();
        }
      });
  });

  $('#vent_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5a7e405b0341c';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('vent_')||'Acad_cursodetalle';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{header:true,footer:false,borrarmodal:false}); 
  });
  
  $('#vent_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'acad_cursodetalle', 'eliminar', id);
        if(res) tabledatos5a7e405b0341c.ajax.reload();
      }
    }); 
  });
});
</script>