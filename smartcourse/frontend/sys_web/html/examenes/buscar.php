<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .breadcrumb {background-color:rgb(64, 113, 191); padding: 1ex;  }
  .breadcrumb a{ color: #fff; }
  .breadcrumb li.active{ color:#d2cccc; }
  .panel-user{ padding:0.4ex; border: 1px solid rgba(90, 137, 248, 0.41); position: relative; margin-bottom: 1ex; }
  .panel-user .item{ height:150px; overflow: auto; width: 100%;}
  .panel-user .item img{ max-height: 100%; max-width: 100%; }
  .panel-user .pnlacciones{ display: none;}
  .panel-user.active .pnlacciones{ display: block; top: 0px;     position: absolute; width: 100%;}
  .panel-user .nombre{ text-align: center; font-size: 0.75em; max-height: 75px; overflow: hidden;}
  .form-group{ margin-bottom: 0px; }
  .input-group { margin-top: 0px; }
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_('SmartQuiz'); ?></li>       
    </ol>
</div> </div>
<?php } ?>


<div class="row" id="ventana-abc2">
  <div class="col-md-12">
    <div class="panel mostrarcollapse activeshow" style="border: 1px solid #dad7d7;   margin-bottom:0px;  ">
      <div class="panel-heading bg-primary">
        <div class="row">
        <?php if(substr($this->idproyecto,0,2)=='PY'){?>
          <div class="col-md-3 form-group">
          <input type="hidden" id="idproyecto" value="<?php echo $this->idproyecto ?>">
          </div>
       <?php }else{?>
        <div class="col-md-3 form-group">
           <div class="cajaselect">           
              <select name="proyecto" id="idproyecto" class="form-control">
                <option value="edukt2017">Edukat</option>
                <option value="smartlearn2017">Ingles</option>                              
              </select>
            </div>
        </div>
        <?php } ?>
        <div class="col-md-6 form-group">
          <div class="input-group">
            <input type="text" name="texto" id="textoabc2" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
            <span class="input-group-addon btn btnbuscar btn btn-success" style="margin-top: 0px;"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
          </div>
        </div>
        <div class="col-md-3 form-group">
          <button class="cerrarmodal btn btn-warning"><i class="fa fa-close"></i> Cerrar</button>
        </div>
        </div>
      </div>
      <div class="panel-body">            
        <div class="row" id="datalistadoabc2">
          <div id="cargando" style="display: none;">cargando</div>
          <div id="sindatos" style="display: none;">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <div class="jumbotron">
                    <h1><strong>Opss!</strong></h1>                
                    <div class="disculpa"><?php echo JrTexto::_('Empty data')?>.</div>                
                </div>
            </div>
          </div>
          <div id="error" style="display: none;">Error</div>
          <div class="row col-12" id="data"></div>
        </div>
      </div>
    </div>
  
  <div id="controlesabc2" style="display: none;">
    <div class="pnlacciones text-center">
     <a class="btn-selected btn btn-success btn-xs" title="<?php echo ucfirst(JrTexto::_('Selected ')).' '.JrTexto::_('Assessment'); ?>"><i class="btnselected fa fa-hand-o-down"></i></a>      
      <!--a class="btnvermodal btn btn-danger btn-xs" title="<?php echo ucfirst(JrTexto::_('Edit')); ?>"><i class="fa fa-pencil"></i></a--> 
      <!--a class="btn-eliminar btn btn-danger  btn-xs" title="<?php //echo ucfirst(JrTexto::_('Remove')).' '.JrTexto::_('Certificate'); ?>"><i class="fa fa-trash-o"></i></a-->
    </div>
  </div>
</div>
</div>
<div id="venpadre" data-value=".<?php echo $ventanapadre; ?>"></div>
<!--button id="ejecutarjs" >aaa</button-->
<script type="text/javascript">
var tabledatosabc2='';
function refreshdatosabc2(){
    tabledatosabc2();
}
$(document).ready(function(){
var estadosabc2={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>'}; 
  $('#ventana-abc2')
  .on('change','#idproyecto',function(){
    refreshdatosabc2();    
  })
  .on('click','.btnbuscar',function(ev){
    refreshdatosabc2();
  }).on('keyup','#textoabc2',function(ev){
    if(ev.keyCode == 13)
    refreshdatosabc2();
  }).on("mouseenter",'.panel-user', function(){
      if(!$(this).hasClass('active')){ 
        $(this).siblings('.panel-user').removeClass('active');
        $(this).addClass('active');
      }
  }).on("mouseleave",'.panel-user', function(){     
      $(this).removeClass('active');
  }).on("click",'.btn-selected', function(){
      $(this).addClass('active');
      var idexamen=$(this).closest('.panel-user').attr('data-id');
      <?php 
        if(!empty($ventanapadre)){?>
          var btn=$('.<?php echo $ventanapadre; ?>');
          if(btn.length){         
            btn.attr('data-idexamen',idexamen);
            btn.trigger("addassesment");
          }
          <?php }  ?>
      $(this).closest('.modal').find('.cerrarmodal').trigger('click');      
  });

  tabledatosabc2=function(){
    var frm=document.createElement('form');
    var pr=$('#idproyecto').val();
    var formData = new FormData();
        formData.append('t', $('#textoabc2').val()); 
        formData.append('pr',pr);
        var url= '<?php echo URL_SMARTQUIZ; ?>service/exam_list/?pr='+pr+'&t='+$('#textoabc2').val();
        console.log('si');
        $.ajax({
          url: url,
          type: "GET",
          data:  formData,
          contentType: false,
          processData: false,
          dataType :'json',
          cache: false,
          beforeSend: function(XMLHttpRequest){
            $('#datalistadoabc2 #cargando').show('fast').siblings('div').hide('fast');
          },      
          success: function(res)
          {                      
            if(res.code==200){
              var midata=res.data;

              if(midata.length){
                var html='';
                var controles=$('#controlesabc2').html();
                $.each(midata,function(i,v){
                  var urlimg=_sysUrlStatic_+'/media/imagenes/cursos/';
                  var srcimg=_sysfileExists(v.portada)?(v.portada):(urlimg+'nofoto.jpg');
                  html+='<div class="col-md-3 col-sm-4 col-xs-12"><div class="panel-user" data-id="'+v.idexamen+'">'+controles;
                  html+='<div class="item"><img class="img-responsive" src="'+srcimg+'" width="100%"></div>'; 
                  html+='<div class="nombre"><strong>'+v.titulo+'</strong></div>';
                  html+='</div></div>';
                });
                $('#datalistadoabc2 #data').html(html).show('fast').siblings('div').hide('fast');
              }else     
                $('#datalistadoabc2 #sindatos').show('fast').siblings('div').hide('fast');
            }else{
              $('#datalistadoabc2 #error').html(res.message).show('fast').siblings('div').hide('fast');
            }
          },
          error: function(e) 
          {
            $('#datalistadoabc2 #error').show('fast').siblings('div').hide('fast');
          }
        });
  }
  tabledatosabc2(); 
});
</script>