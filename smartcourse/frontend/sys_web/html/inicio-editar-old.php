<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$curso=!empty($this->curso)?$this->curso:'';
$idcurso=!empty($curso["idcurso"])?$curso["idcurso"]:'';
$img=is_file(RUTA_MEDIA.@$curso["imagen"])?URL_MEDIA.@$curso["imagen"]:URL_MEDIA.'/static/media/cursos/nofoto.jpg';
?>
<style type="text/css">
	#mindice{ height: calc(100% - 100px); overflow-y: hidden;}
	#mindice:hover{overflow-y: auto; }
	#mindice, #mindice ul{ list-style:none; margin:0 auto; padding: 0px; color:#fff;}
	#mindice li{
		border-top:1px solid #878e98;		
		/*padding: 0.4ex 1ex;*/
		width: 100%;
	}

	#mindice li > div{
		content: " ";
		font-size: 17px;
		padding: 1ex 0ex;
	}
	#mindice li ul li > div{padding-left: 1.5ex;}
	#mindice li ul li ul li > div{padding-left: 3ex;}
	#mindice li ul li ul li ul li > div{padding-left: 4.5ex;}
	#mindice li ul li ul li ul li ul li> div{padding-left: 6ex;}
	#mindice li ul li ul li ul li ul li ul li> div{padding-left: 7.5ex;}
	#mindice li ul li ul li ul li ul li ul li ul li> div{padding-left: 9ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 10.5ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 12ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 13.5ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 15ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 16.5ex;}
	#mindice li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li ul li> div{padding-left: 17ex;}

	#mindice li > div span.macciones{float: right;}
	#mindice li > div span.macciones i{cursor: pointer;}
	#mindice li > div span.macciones i.mmsubmenu{display: none;}
	#mindice li.hijos > div ~ ul{display: none;}
	#mindice li.hijos.active > div ~ ul{display: block;}	
	#mindice li.hijos.active > div span.macciones i.mmsubmenu.fa-sort-down{ display: none;}
	#mindice li.hijos.active > div span.macciones i.mmsubmenu.fa-sort-up{ display: inline-block;}
	#mindice li.hijos > div span.macciones i.mmsubmenu.fa-sort-down{ display: inline-block;}
	#mindice li.hijos > div span.macciones i.mmsubmenu.fa-sort-up{ display: none;}
	#mindice li span.macciones i.mmovedown, #mindice li span.macciones i.mmoveup{display:block !important;}	
	#mindice li span.macciones i.mmoveup{margin-top: -1.1ex; }
	#mindice > li:nth-child(2) > div  span.macciones i.mmoveup{display: none !important;}
	#mindice > li:last-child > div span.macciones i.mmovedown{display: none !important;}
	#mindice li ul li:nth-child(2) span.macciones i.mmoveup{margin-top: -1.1ex; display: block !important;}
	#mindice li ul li:first-child > div span.macciones i.mmoveup{ display: none !important;}
	#mindice li ul li:last-child > div span.macciones i.mmovedown{ display: none !important;}
	#toolbarcurso .fa { color:#FFF !important; }
	#mindice li .macciones{ display: none; }
	#mindice li:hover > div > .macciones{ display: block; }
</style>
<div class="row">
	<div class="col-lg-3 col-md-4 col-sm-4 bg-primary" style="height: calc(100vh - 2px)">
		<div class="text-center">
			<div class="btn-group" id="toolbarcurso">
	  			<button type="button" onclick="redir(_sysUrlBase_+'/?acc=crear');" class="btn crearnuevo<?php echo $idgui;?> btn-sm btn-info verpopover" data-placement="bottom" title="Nuevo" data-content="permite crear un nuevo curso"><i class="fa fa-file-o"></i></button>
	  			<button type="button" class="abrir<?php echo $idgui; ?> btn btn-sm btn-info verpopover" data-placement="bottom" title="Abrir" data-content="permite abrir un curso "><i class="fa fa-folder-open"></i></button>
	  			<button type="button" class="btn editar<?php echo $idgui; ?> btn btn-sm btn-info verpopover" data-placement="bottom" title="Editar" data-content="permite Editar los datos del curso"><i class="fa fa-pencil"></i></button> 
	  			<button type="button" class="btn eliminar<?php echo $idgui; ?> btn-sm btn-info verpopover" data-placement="bottom" title="Eliminar" data-content="elimina este curso completamente"><i class="fa fa-trash"></i></button>
	  			<button type="button" class="import<?php echo $idgui; ?> btn btn-sm btn-info verpopover" data-placement="bottom" title="Importar" data-content="Importar Menu"><i class="fa fa-cloud-upload"></i></button>
	  			<button type="button" class="export<?php echo $idgui; ?> btn btn-sm btn-info verpopover" data-placement="bottom" title="Exportar" data-content="Exportar"><i class="fa fa-cloud-download"></i></button>
	  			<button type="button"  onclick="redir(_sysUrlBase_+'/');" class="export<?php echo $idgui; ?> btn btn-sm btn-info verpopover" data-placement="bottom" title="" data-content="Regresar al listado"><i class="fa fa-sign-out"></i></button>
			</div>	
			<hr>					
		</div>
		<div class="row">
			<div class="col-12 text-center"><h6 class="titulocurso"><?php echo !empty($curso["nombre"])?$curso["nombre"]:'Titulo del curso'; ?></h6></div>
			<div class="col-6 text-center">				
				<img src="<?php echo $img; ?>" class="img-fluid img-circle" style="max-width: 150px; max-height: 150px;">
			</div>
			<div class="col-6 text-center"><br><br>				
				<button type="button" onclick="redir(_sysUrlBase_+'/');" class="btn btn-sm btn-info verpopover" title="" data-content="Ir al listado de cursos">
					<i class="fa fa-eye"></i> Ver cursos</button><br>
		  		<button type="button" class="crearmenu<?php echo $idgui; ?> btn btn-sm btn-danger verpopover" title="Crear menu" data-content="Esta opcion le permitira crear menu">
		  			<i class="fa fa-plus"></i> Crear Menu</button>
			</div>
			<div class="col-12"><br></div>
		</div>

		<div class="row">
			<div class="col-12">
				<ul id="mindice">
					<li class="paraclonar">
						<div><span class="mnombre">Friends</span>
						<span class="macciones">
							<i class="mmsubmenu fa fa-sort-down verpopoverm" data-content="Ocultar Submenus" ></i>
							<i class="mmsubmenu fa fa-sort-up verpopoverm" data-content="Mostrar Submenus" ></i>
							<i class="addcontent fa fa-eye verpopoverm" data-content="Ver contenido" data-placement="top"></i>
							<i class="magregar fa fa-plus verpopoverm" data-content="Agregar Submenu" data-placement="top"></i>
							<i class="meditar fa fa-pencil verpopoverm" data-content="Editar Menu" data-placement="top"></i>
							<i class="meliminar fa fa-trash verpopoverm" data-content="Eliminar" data-placement="top"></i>
							<i class="mmove fa fa-arrows-alt verpopoverm" data-content="Mover Menu" data-placement="right"></i>
							<i class="mcopylink fa fa-link verpopoverm" data-content="Copiar link" data-placement="right"></i>
							<!--span style="display:inline-block">								
								<i class="mmoveup fa fa-angle-double-up verpopoverm" data-content="Mover Arriba" ></i>
								<i class="mmovedown fa fa-angle-double-down verpopoverm" data-content="Mover abajo"  ></i>
							</span-->
						</span>
						</div>
					</li>
					<?php 		
					var_dump($this->menus);			
					if(!empty($this->menus)){
					foreach($this->menus as $m){
						$html_=$this->agregarhijos($m);
						echo $html_;
						?>
					<?php }} ?>
				</ul>
			</div>
		</div>	
	</div>
	<div class="col-lg-9 col-md-8 col-sm-8" id="paneldisenio" style="height:calc(100vh - 2px);  overflow-y: auto;">
		
	</div>	
</div>
<script type="text/javascript">
	$copyhtml='';
	$(document).ready(function(){
		var idcurso='<?php echo @$idcurso; ?>';
		$('.crearmenu<?php echo $idgui; ?>').click(function(ev){
			__cargarplantilla({
				url:'/acad_cursodetalle/vermenus/',
				donde:'paneldisenio',
				data:{
					idcurso:'<?php echo @$idcurso; ?>',
					endonde:'mindice',
					idpadre:0,
					acc:'additem'
					}
			});
		});
		$('#toolbarcurso').on('click','.editar<?php echo $idgui;?>',function(ev){
			__cargarplantilla({
				url:'/peditar/curso/',
				donde:'paneldisenio',
				data:{idcurso:'<?php echo @$idcurso; ?>'}
			});
			return false;
		}).on('click','.abrir<?php echo $idgui; ?>',function(ev){
		}).on('click','.guardar<?php echo $idgui; ?>',function(ev){
		}).on('click','.eliminar<?php echo $idgui; ?>',function(ev){
			__sysajax({
				url:_sysUrlBase_+'/acad_curso/eliminar',
				fromdata:{
					idcurso:'<?php echo @$idcurso; ?>',					
				},				
				showmsjok:true,
				callback:function(rs){
					setTimeout(redir(_sysUrlBase_+'/'),300);
				}
			});
		}).on('click','.import<?php echo $idgui; ?>',function(ev){
			__cargarplantilla({
				url:'/peditar/importar/',
				donde:'paneldisenio',
				data:{idcurso:'<?php echo @$idcurso; ?>'}
			});
		}).on('click','.export<?php echo $idgui; ?>',function(ev){
			console.log('exportar');
		});

		/*$('ul#mindice').on('click','li.hijos div span:first-child',function(ev){			
			$(this).closest('li').toggleClass('active');
		}).on('click','i.magregar',function(ev){		
			$li=$(this).closest('li');
			__cargarplantilla({
				url:'/acad_cursodetalle/vermenus/',
				donde:'paneldisenio',
				data:{
					idcurso:'<?php //echo @$idcurso; ?>',
					endonde:'mindice',
					idpadre:$li.attr('data-idcursodetalle'),
					idrecursopadre:$li.attr('data-idrecurso'),
					acc:'addsubitem'
					}
			});			
		}).on('click','i.meditar',function(ev){
			$li=$(this).closest('li');
			__cargarplantilla({
				url:'/acad_cursodetalle/vermenus/',
				donde:'paneldisenio',
				data:{
					endonde:'mindice',
					idcurso:'<?php //echo @$idcurso; ?>',
					idcursodetalle:$li.attr('data-idcursodetalle'),					
					idpadre:$li.attr('data-idpadre')||0,
					idrecurso:$li.attr('data-idrecurso'),
					acc:'edit'
					}
			});				
		}).on('click','i.meliminar',function(ev){
			$li=$(this).closest('li');
			$(this).popover('dispose');
			__sysajax({
				url:_sysUrlBase_+'/acad_cursodetalle/eliminarMenu',
				fromdata:{
					idcurso:'<?php //echo @$idcurso; ?>',
					idcursodetalle:$li.attr('data-idcursodetalle'),
					idrecurso:$li.attr('data-idrecurso'),
					tiporecurso:$li.attr('data-tipo'),
				},
				type:'html',
				showmsjok:true,
				callback:function(rs){
					$li.remove();
				}
			});
		}).on('click','.fa-sort-down',function(ev){
			$(this).closest('li').toggleClass('active');
		}).on('click','.fa-sort-up',function(ev){
			$(this).closest('li').toggleClass('active');
		/*}).on('click','i.mmoveup',function(ev){
			$li=$(this).closest('li');
			$li.insertBefore($li.prev());
		}).on('click','i.mmovedown',function(ev){
			$li=$(this).closest('li');
			$li.insertAfter($li.next());*/
		/*}).on('click','i.addcontent',function(){
			__cargarplantilla({
				url:'/disenio/',
				donde:'paneldisenio',
				data:{
					plantilla:'ninguno',
					idcurso:'<?php //echo @$idcurso; ?>',
					idcursodetalle:$(this).closest('li').attr('data-idcursodetalle')
					//endonde:'mindice',
					//menupadre:idli,
					}
			});
			//$(this).
		}).on('click','i.mcopylink',function(){
			let li=$(this).closest('li');
			let link=li.attr('data-link')||'';
			if(link=='') link='static/media/cursos/curso_'+idcurso+'/ses_'+li.attr('data-idcursodetalle')+'.php';
			__copyhtml(link);
		})

		let _reordenar=function(dt){
			let item=dt.obj||'';
			let max=dt.max||0;
			let min=dt.min||0;
			let res=dt.res||0;
			let items=dt.items||'';
			if(item==''||item==undefined||items=='') return ;
			$hijos=item.children(items);
			let ordenmen=min;
			var datosupdate={};
			$.each($hijos,function(i,v){
				let hi=$(v);
				if(!hi.hasClass('paraclonar')){
					if(i>=min && i<=max){
						hi.attr('data-orden',ordenmen).removeAttr('data-oldindex');
						datosupdate[ordenmen]=hi.attr('data-idcursodetalle');
						ordenmen++;
					}		
				}
			})
			let idcurso=<?php //echo $_REQUEST["idcurso"];?>;
			__sysajax({
				url:_sysUrlBase_+'/acad_cursodetalle/reordenar',
				fromdata:{idcurso:idcurso , dataorden:JSON.stringify(datosupdate)},
				type:'json',
				showmsjok:true
			});
		}
		$('#mindice').disableSelection();
		$('#mindice').sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9,
			start:function(ev,ui){
				var li=$(ui.item);
				var pos=parseInt(li.index()+1);				
				if(li.closest('ul').attr('id')=='mindice') pos--;
				li.addClass('enmovimiento').attr('data-oldindex',pos);
			},
			update:function(ev,ui){
			let li=$(ui.item);			
			li.removeClass('enmovimiento');
			let pos=parseInt(li.index()+1);
			let posant=parseInt(li.attr('data-oldindex')||1);
			if(li.closest('ul').attr('id')=='mindice') {pos--;}
			ul=li.closest('ul');
			_reordenar({obj:ul,min:((pos<posant)?pos:posant),max:((pos>posant)?pos:posant),items:'li'});
		}});

		$('#mindice').find('ul').each(function(i,v){
			$('ul#'+$(v).attr('id')).disableSelection();
			$('ul#'+$(v).attr('id')).sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9,
				start:function(ev,ui){
					var li=$(ui.item);
					var pos=parseInt(li.index()+1);
					if(li.closest('ul').attr('id')=='mindice') pos--;
					li.addClass('enmovimiento').attr('data-oldindex',pos);
				},
				update:function(ev,ui){				
				let li=$(ui.item);
				li.removeClass('enmovimiento');
				let pos=parseInt(li.index()+1);
				let posant=parseInt(li.attr('data-oldindex')||1);
				if(li.closest('ul').attr('id')=='mindice') {pos--; }
				ul=li.closest('ul');
				_reordenar({obj:ul,min:((pos<posant)?pos:posant),max:((pos>posant)?pos:posant),items:'li'})
			
		}});
		});
		$('.verpopover').popover({ trigger: 'hover',delay:100});
		/*_resizewindow=function(){
			var hei=(window.innerHeight > 0) ? window.innerHeight : screen.height;
			$('#paneldisenio').height((hei-50)+'px');
		}

		$(window).resize(function(){
			_resizewindow();
		});*/
		$lis=$('#mindice').find('li[data-link][data-link!=""]');
		if($lis.length>0) $lis.first().children('div').children('.macciones').children('i.addcontent').trigger('click');
		else $('#mindice').children('li').next().children('div').children('.macciones').children('i.addcontent').trigger('click');
		//_resizewindow();
	});
</script>