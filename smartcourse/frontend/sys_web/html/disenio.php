<?php 
	$rustatic=$this->documento->getUrlStatic();
	$idgui=uniqid();
	$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
	$idcursodet=!empty($_REQUEST["idsesion"])?$_REQUEST["idsesion"]:0;
	$fileurl=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
	$urlmedia=URL_MEDIA;
	$haycontenido=0;
	if(!empty($this->sesion["html"]))
		if(file_exists(RUTA_MEDIA.$this->sesion["html"])) $haycontenido=RUTA_MEDIA.$this->sesion["html"];

	if($haycontenido==0&&$idcursodet==0) 
		if(file_exists(RUTA_MEDIA.'static'.SD.'media'.SD.'cursos'.SD.'curso_'.$idcurso.SD.'ses_0.html')) $haycontenido=RUTA_MEDIA.'static'.SD.'media'.SD.'cursos'.SD.'curso_'.$idcurso.SD."ses_0.html";

?>
<style type="text/css">		
	#bookplantilla.tabstop{ height: calc(100% - 50px); }		
	.cicon{	font-size: 2em;  padding: 0.25ex 0.5ex;  text-align: center;	position: relative;}
	.cicon i{ padding: 0.5ex 1ex;  width: 100%;  text-align: center; }
	.cicon i span{ font-size: 15px; }
	#accionesedicion{bottom: 1ex; position: absolute; right: 1ex; }	

	.nav-item.noalumno a{

	}

	.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active, .nav-link.active, .nav-item {
	    color: #495057;
	    background-color: transparent;
	    border-color: transparent;
	}
	.nav-item{
			/*margin-left: 20px;
		    background: #f00;*/
			margin-bottom: 0px;
		    border-top-right-radius: 1ex;
		    border-top-left-radius: 5px;
		    /* margin-bottom: -1px; */
		   
		    /* border-radius: 1ex; */
	}
	.nav-item:before{
		position: absolute;
	    /*margin-left: -24px;*/
	    content: '';
	    left: 0px;
	   /* border-bottom: 50px solid #ccc;*/
	    border-left: 25px solid transparent;	
	}

	.nav-item: before{
		    /* color: #d4dae0;*/
		    /* border: 1px solid #000; */
		    /* margin-left: -2px; */
		    height: 49px;
		    /* background-color: #fff; */
		    /* border-color: #dee2e6 #dee2e6 #fff; */
	}
	.nav-item a, .nav-item a{
		height: 50px;
		background-color: transparent; 
	}
	.pnledittabmenu a{min-width: 150px; background-size: 100% 100%;	border: 0px;}

	iframe body{ margin: 0px !important; padding: 0px; }


</style>
<link id="mainestilo" rel="stylesheet" type="text/css" data-tmpurl="<?php echo $this->documento->getUrlTema()."/css/"?>" href="<?php echo $this->documento->getUrlTema()."/css/smartbook.css"; ?>">
<?php
 if(!empty($haycontenido))  require_once($haycontenido); 
else { ?>
<div class="row tabstop" id="bookplantilla">	
	<div class="menubooktop">
		<ul class="nav nav-tabs pull-right sysmenumain">
	    	<li class="nav-item noalumno pnladdtabmenu hvr-grow"><a class="nav-link active" href="javascript:void(0)"><i class="fa fa-plus"></i></a></li>
		</ul>
	</div>
	<div class="anillado"></div>
	<div class="col-12" id="contentpages"></div>
	<?php // var_dump($_REQUEST); ?>
</div>
<?php } ?>
<div class="tools" style="position: absolute; bottom: 0px; display: none;">
	<span>La Sesion de este curso terminara en </span>	
</div>
<div id="accionesedicion">	
	  <button type="button" class="btn btn-success verpopover savecambios" title="Guardar" data-content="Guarda los cambios para este menu"><i class="fa fa-save"></i> Guardar</button>
	   <button type="button" class="btn btn-success verpopover borrarcambios" title="Borrar" data-content="Borra todo el contenido"><i class="fa fa-trash"></i> Borrar</button>
	  <!--button type="button" class="btn btn-warning verpopover" data-content="Importar desde una plantilla"><i class="fa fa-upload"></i> Importar</button>
	  <button type="button" class="btn btn-info verpopover" data-content="Espotar a plantilla"><i class="fa fa-download"></i> Exportar</button-->		
</div>


<script type="text/javascript">
	$(document).ready(function(){

		var idcurso=parseInt('<?php echo $idcurso; ?>');
		var iddetalle=parseInt('<?php echo $idcursodet; ?>');
		let _sysUrlMedia_='<?php echo $urlmedia; ?>';

		$('#accionesedicion').on('click','.savecambios',function(ev){
			ev.preventDefault();
			_guardarhtml($(this));
			ev.stopPropagation();
		}).on('click','.borrarcambios',function(ev){
			$('#contentpages').html('');
		})
				
		$('#paneldisenio').on('click','.pnladdtabmenu a',function(ev){
			ev.preventDefault();
			var _item=$(this).closest('li');
			var item=_item.clone(false);
			item.removeClass('pnladdtabmenu noalumno').addClass('pnledittabmenu hvr-grow');
			var itema=item.children('a');
			__paneltools({
				item:itema,
				texto:'',
				fncall:function(rs){
					itema.text(rs.texto);
					if(rs.colorfondo=='') itema.attr('data-colorfondo','transparent').css({'background-color':'transparent'});					
					else  itema.attr('data-colorfondo',rs.colorfondo).css({'background-color':rs.colorfondo});
					if(rs.colortexto!='') itema.attr('data-colorfondo',rs.colortexto).css({'color':rs.colortexto});
					if(rs.imagenfondo.trim()!='')itema.css({'background-image':'url('+rs.imagenfondo+')','background-size':'100% 100%','min-width':'150px','border':'0px'}).attr('data-imagenfondo',rs.imagenfondo);
					else itema.css({'background-image':'none'});
					if(rs.datalink.trim()!='')itema.attr('data-link',rs.datalink);
					if(rs.datatype.trim()!='')itema.attr('data-type',rs.datatype);
					if(rs.fuente.trim()!='')itema.attr('data-fuente',fuente).css({'font-family':fuente});
					if(rs.zise.trim()!='')itema.attr('data-zise',zise).css({'font-size':rs.zise})
					itema.attr('data-donde','contentpages');
					item.insertBefore(_item);					
					itema.trigger('click');
				}
			});
			ev.stopPropagation();	
		}).on('contextmenu','.pnledittabmenu a',function(ev){
			ev.preventDefault();	
			$('video').trigger("pause");
			$('audio').trigger("pause");
			var itema=$(this);
			__paneltools({
				item:itema,
				texto:$(this).text(),
				fncall:function(rs){
					itema.text(rs.texto);
					if(rs.colorfondo=='') itema.attr('data-colorfondo','transparent').css({'background-color':'transparent'});					
					else  itema.attr('data-colorfondo',rs.colorfondo).css({'background-color':rs.colorfondo});
					if(rs.colortexto!='') itema.attr('data-colorfondo',rs.colortexto).css({'color':rs.colortexto});
					if(rs.imagenfondo.trim()!='')itema.css({'background-image':'url('+rs.imagenfondo+')','background-size':'100% 100%','min-width':'150px','border':'0px'}).attr('data-imagenfondo',rs.imagenfondo);
					else itema.css({'background-image':'none'});
					if(rs.datalink.trim()!='')itema.attr('data-link',rs.datalink);
					if(rs.datatype.trim()!='')itema.attr('data-type',rs.datatype);
					if(rs.fuente.trim()!='')itema.attr('data-fuente',rs.fuente).css({'font-family':rs.fuente});
					if(rs.zise.trim()!='')itema.attr('data-zise',rs.zise).css({'font-size':rs.zise})
					itema.attr('data-donde','contentpages');					
					itema.trigger('click');
				}
			});
			ev.stopPropagation();
		}).on('click','.pnledittabmenu a',function(ev){
			ev.preventDefault();			
			$('audio').trigger('pause');
			$('video').trigger('pause');
			let imtea=$(this);
			let txt=imtea.text()||'';
			let fuente=imtea.attr('data-fuente')||'';
			let colortexto=imtea.attr('data-colortexto')||'';
			let colorfondo=imtea.attr('data-colorfondo')||'';
			let imagenfondo=imtea.attr('data-imagenfondo')||'';
			let datatype=imtea.attr('data-type')||'';
			let datalink=imtea.attr('data-link')||'';
			data={link:datalink,mostrarcargando:false}

			if(colorfondo!='transparent'&&colorfondo!='')$('#contentpages').css({'border-color':colorfondo});
			if(colorfondo!='')$('.pnlsesions').css({'background-color':colorfondo,'color':colortexto});
			if(datatype=='smarticlock'||datatype=='smarticlookPractice'||datatype=='smarticDobyyourselft'||datatype=='game'||datatype=='smartquiz')
				datatype='html';

			let dataidinfotmp=imtea.attr('data-idinfotmp')||'';

			if(dataidinfotmp!=''&&dataidinfotmp!=undefined){
				data.idtmp=dataidinfotmp;
				info=imtea.attr('data-datainfo')||'';
				if(info!=''&&info!='{}') data.infodata=JSON.parse(info);
			}
			__cargarplantilla({url:'/plantillas/'+datatype,donde:'contentpages',data});	
			ev.stopPropagation();	
		}).on('click','.txtedit',function(ev){//solo autor	
			ev.preventDefault();
			ev.stopPropagation();
			haytxt=$(this).children('textarea').length;
			if(haytxt==0){
				$(this).closest('._subirfileedit').addClass('nohover');
				txtaqui=$(this);
				htmledit=txtaqui.html();
				idtmp='idtmp'+Date.now();
				html='<textarea id="'+idtmp+'" style="width:100%; height:100%;" row="10">'+htmledit+'</textarea>';
				txtaqui.html(html);
				__initEditor(idtmp,'savehtml');
				ev.stopPropagation();
			}
		})
		$(document).click(function(ev){
			$('.toolbar1').removeClass('active');
		})		
		$('#paneldisenio ul.tabmainmenu').sortable({cursor:"move",placeholder: "sortable-placeholder",opacity: 0.9});
		let _cont=$('#bookplantilla').children('#contentpages');
	});
</script>
