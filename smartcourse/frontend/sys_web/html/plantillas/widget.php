<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
$idcursodet=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:0;
?>
<style type="text/css">
.circular-menu{	margin: 0 auto;	position: relative;}
.menucircle{
	width: 100%;
  	height: 100%;
  	opacity: 0;
    -webkit-transform: scale(0);
  	-moz-transform: scale(0);
  	transform: scale(0);
  	-webkit-transition: all 0.4s ease-out;
  	-moz-transition: all 0.4s ease-out;
  	transition: all 0.4s ease-out;
  	margin: 0 auto;
  	position: relative;
}

.open.menucircle{
  	opacity: 1;
  	-webkit-transform: scale(1);
  	-moz-transform: scale(1);
  	transform: scale(1);
  	width: 100%;
  	height: 100%;
}

.menucircle .menucircleitem{
  	text-decoration: none;
  	background: #eee;
  	border-radius: 50%;
  	color: #bbb;
  	padding: 1ex;
  	display: block;
  	position: absolute;
  	text-align: center;
  	display: flex;
  	justify-content: center;
  	align-content: center;
  	flex-direction: column;
  	z-index: 2;
  	background-size: 100% 100% !important;
}

.menucircle .menucircleitem:hover{
    background: #ccc;
    border-radius: 50%;
    color: #fff;
    padding: 1ex;
    display: flex;
    justify-content: center;
    align-content: center;
    flex-direction: column;
    z-index: 3;
    background-size: 100% 100%;
}
.menucircle ~ .openmenucircle{
	position: absolute;
	cursor: pointer;
	text-decoration: none;
	text-align: center;
	color: #444;
	border-radius: 50%;
  	display: block;
  	padding: 1ex;
  	background: #dde;
  	display: flex;
    justify-content: center;
    align-content: center;
  	flex-direction: column;
  	background-size: 100% 100%;
}

.menucircle ~ .openmenucircle:hover{opacity: 0.75; background-size: 100% 100%;}
.rheight{position: relative;height: 100%;}
._additem, .iedit, .iremove{cursor: pointer;}
</style>
<div class="widgetjson"></div>
<div class="widgetcont" style="height: calc(100vh - 100px);">
<div class="row" style="margin: 0px; padding: 0px;">
  <div class="txtedit text-left" style="width: 100%;"><p>Click Aqui para modificar el texto</p></div>
</div>
<div class="row" style="margin: 0px; padding: 0px;">	
	<div class="col-12">
		<div class="nav circular-menu">
			<div class="menucircle open" id="menucircle<?php echo $idgui; ?>">
				<div class="menucircleitem">
					<span>texto 01</span>
					<span class="options noalumno">
						<i class="fa fa-pencil iedit"></i>
						<i class="fa fa-trash iremove"></i>
					</span>
				</div>				
			</div>	
			<span class="openmenucircle">
				<span class="">Nombre del curso</span>
				<span class="options noalumno">
            <i class=" fa fa-plus _additem"></i>
            <i class="fa fa-pencil iedit"></i>
        </span>
			</span>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	var opt={idcurso:<?php echo $idcurso; ?>,idcursodetalle:<?php echo $idcursodet; ?>}
	setTimeout(function(){$('#menucircle<?php echo $idgui; ?>').menucircle(opt);},350);
});
</script>