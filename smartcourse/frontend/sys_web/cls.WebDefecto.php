<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
/*JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
class WebDefecto extends JrWeb
{
	private $oNegCurso;

	public function __construct()
	{
		parent::__construct();		
        $this->oNegAcad_categorias = new NegAcad_categorias;
        $this->oNegCursodetalle = new NegAcad_cursodetalle;
        $this->oNegCurso = new NegAcad_curso;
	}

	public function agregarhijos($m){
		try{	
			$ht='';
			$mh=!empty($m["hijos"])?true:false;
			if($mh==true){
				$hth='';
				foreach ($m["hijos"] as $mj){
					$hth.=$this->agregarhijos($mj);
				}				
			}
			$mht=!empty($m["html"])?'data-link="'.$m["html"].'"':'';
			$ht='<li class="'.($mh?'hijos':'').'" id="me_'.$m["idcursodetalle"].'" data-idrecurso="'.$m["idrecurso"].'" data-idcursodetalle="'.$m["idcursodetalle"].'" data-idpadre="'.$m["idpadre"].'" data-orden="'.$m["orden"].'" data-tipo="'.$m["tiporecurso"].'" '.$mht.' data-imagen="'.$m["imagen"].'" data-esfinal="'.$m["esfinal"].'" >
				<div>
					<span class="mnombre">'.$m["nombre"].'</span>
					<span class="macciones">
						<i class="mmsubmenu fa fa-sort-down verpopoverm" data-content="Ocultar Submenus" ></i>
						<i class="mmsubmenu fa fa-sort-up verpopoverm" data-content="Mostrar Submenus" ></i>
						<i class="addcontent fa fa-eye verpopoverm" data-content="Ver contenido" data-placement="top"></i>
						<i class="magregar fa fa-plus verpopoverm" data-content="Agregar Submenu" data-placement="top"></i>
						<i class="meditar fa fa-pencil verpopoverm" data-content="Editar Menu" data-placement="top"></i>							
						<i class="meliminar fa fa-trash verpopoverm" data-content="Eliminar" data-placement="top"></i>
						<i class="mmove fa fa-arrows-alt verpopoverm" data-content="Mover Menu" data-placement="right"></i>
						<!--i class="mcopylink fa fa-link verpopoverm" data-content="Mover Menu" data-placement="right"></i-->
						<!--span style="display:inline-block">								
							<i class="mmoveup fa fa-angle-double-up verpopoverm" data-content="Mover Arriba"></i>
							<i class="mmovedown fa fa-angle-double-down verpopoverm" data-content="Mover abajo"></i>
						</span-->
					</span>
				</div>
				'.($mh?('<ul id="me_hijo'.$m["idcursodetalle"].'">'.$hth.'</ul>'):'').'
			</li>';
			return $ht;
		}catch(Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function defecto(){
		global $aplicacion;
		$acc=!empty($_REQUEST['acc']) ? $_REQUEST['acc'] : '';
		$this->documento->script('tinymce.min', '/libs/tinymce/');
        $this->documento->script('savehtml', '/libs/tinymce/plugins/chingo/');      
		if(empty($acc))
			return $this->filtrarcursos();
		elseif($acc=='crear'){
			return $this->crear();
		}else{
			return $this->vercurso();
		}
	}

	public function crear(){
		try{
			global $aplicacion;
			$this->documento->script('tinymce.min', '/libs/tinymce/');
        	$this->documento->script('savehtml', '/libs/tinymce/plugins/chingo/');
			$usuActivo = NegSesion::getUsuario();
			$rolActual =$usuActivo["idrol"];
			if($rolActual==3){ exit('no tiene suficientes permisos');}
			$this->documento->setTitulo(JrTexto::_('Crear Curso'),true);
			$this->esquema = 'inicio';
			$this->documento->plantilla ='general';
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0])) $this->curso=$curso[0];
			}			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function filtrarcursos(){
		try{
			global $aplicacion;
			$this->documento->script('slick.min', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			@extract($_REQUEST);
			$this->documento->setTitulo(JrTexto::_('Listado de Cursos'),true);
			$filtros=array();
			$filtros["idpadre"]=0;
			$this->categorias=$this->oNegAcad_categorias->buscar($filtros);
			//$this->vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:'vista01';
			$this->esquema = 'cursos';
			$this->documento->plantilla ='general';
			$this->usuActivo = NegSesion::getUsuario();
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->redir('../');
		}
	}

	public function __cursosxCategoria($filtros=array()){
		try{
			$this->cursos=$this->oNegCurso->buscarxcategoria($filtros);
			return $this->cursos;
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function buscarxcategoriajson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$filtros=array();
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegCurso->buscarxcategoria($filtros);
			if(empty($this->datos)) echo json_encode(array('code'=>'ok','data'=>''));
			else{
				$cat=array();
				foreach ($this->datos as $cur){
					if(empty($cat[$cur["idcategoria"]])){
						$cat[$cur["idcategoria"]]=array();
						$cat[$cur["idcategoria"]]["nombre"]=$cur["categoria"];
						$cat[$cur["idcategoria"]]["hijos"]=array();
					}
					$imgcursodefecto=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
					$imgtmp=substr(RUTA_MEDIA,0,-1).$cur["imagen"];                 
                    $cur["imagen"]=is_file($imgtmp)?URL_MEDIA.$cur["imagen"]:$imgcursodefecto; 
					$cat[$cur["idcategoria"]]["hijos"][$cur["idcurso"]]=$cur;
				}
				$this->datos=$cat;
			}
			echo json_encode(array('code'=>'ok','data'=>json_encode($this->datos)));
			exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function verlistado()
	{
		try{			
			global $aplicacion;	
			$filtro=array();
			@extract($_REQUEST);
			$this->cursos=$this->oNegCurso->buscar($filtro); // agregar los filtros de busqueda
			$vista=!empty($_REQUEST["view"])?$_REQUEST["view"]:'vista01';
			$this->esquema = 'listado-cursos-'.$vista;
			$this->usuActivo = NegSesion::getUsuario();
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'general';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function silabus(){
		try{
			global $aplicacion;
			$this->usuActivo = NegSesion::getUsuario();
			$this->documento->plantilla ='general';
			$rolActual=$this->usuActivo["idrol"];
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			$this->categorias=$this->oNegAcad_categorias->buscar(array('idpadre'=>0));
			$this->esquema = 'silabus';
			$this->idcurso=$idcurso;
			if(!empty($idcurso)){	
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));	
				if(!empty($curso[0])){			
					JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
					$cursodetalle=new NegAcad_cursodetalle;
					$this->curso=$curso[0];
					$this->idcurso=$this->curso["idcurso"];
					$menushtml=$cursodetalle->buscarconnivel(array('idcurso'=>$this->curso["idcurso"],'htmlnotnull'=>true),1);
					$this->menushtml=!empty($menushtml[0])?$menushtml[0]:'';
					$this->categoriasdelcurso=$this->oNegCurso->buscarxcategoria(array('idcurso'=>$this->curso["idcurso"],'solocategoria'=>true));
					$this->menus=$this->oNegCursodetalle->buscarconnivel(array('idcurso'=>$this->curso["idcurso"]));
				}
			}
			return parent::getEsquema();	
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	} 

	/*public function vercursoautor(){
		try{
			global $aplicacion;
			$this->usuActivo = NegSesion::getUsuario();
			$this->documento->setTitulo(JrTexto::_('Curso'),true);
			$this->esquema = 'curso-ver-autor';
			$this->documento->plantilla ='general';
			//var_dump($this->usuActivo);
			$rolActual=$this->usuActivo["idrol"];
			if($rolActual==3){ exit('no tieme suficientes permisos');}
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0])){					
					JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
					$cursodetalle=new NegAcad_cursodetalle;
					$this->curso=$curso[0];
					$menushtml=$cursodetalle->buscarconnivel(array('idcurso'=>$this->curso["idcurso"],'htmlnotnull'=>true),1);
					$this->menushtml=!empty($menushtml[0])?$menushtml[0]:'';
					if(empty($this->menushtml)) return $aplicacion->redir('../recursos/listar/'.$idcurso);
				}  else {$this->esquema='error/404';}
			} else {$this->esquema='error/404';}	
			return parent::getEsquema();	
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}*/

	public function vercurso(){
		try{			
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Curso'),true);
			$this->esquema = 'curso-ver';
			$this->documento->plantilla ='general';
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0])){
					JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
					$cursodetalle=new NegAcad_cursodetalle;
					$this->curso=$curso[0];
					$this->menus=$cursodetalle->buscarconnivel(array('idcurso'=>$this->curso["idcurso"]));					
					//$this->menushtml=!empty($menushtml[0])?$menushtml[0]:'';
					return parent::getEsquema();					
				} 
			} 
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function vermenuupdate(){
		try{
			$this->documento->plantilla = 'blanco';
			global $aplicacion;
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			$this->menus=$this->oNegCursodetalle->buscarconnivel(array('idcurso'=>$idcurso));
			$this->esquema = 'menu-ver';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}