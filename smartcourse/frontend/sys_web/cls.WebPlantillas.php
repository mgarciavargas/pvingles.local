<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
/*JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE, 'sys_negocio');*/
class WebPlantillas extends JrWeb
{
	//private $oNegEmpresas;

	public function __construct()
	{
		parent::__construct();		
        //$this->oNegAulavirtualinvitados = new NegAulavirtualinvitados;*/
	}

	public function defecto(){
		return $this->ver();		
	}

	public function ver(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Smartcourse'),true);
			$this->esquema = 'inicio-plantillas';
			$this->documento->plantilla =!empty($_REQUEST["plt"])?$_REQUEST["plt"]:'general';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function pdf(){
		try{			
			global $aplicacion;
			$this->link=!empty($_REQUEST["link"])?$_REQUEST["link"]:'';
			$this->esquema = 'plantillas/pdf';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function flash(){
		try{
			global $aplicacion;
			$this->link=!empty($_REQUEST["link"])?$_REQUEST["link"]:'';
			$this->esquema = 'plantillas/flash';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}	
	}

	public function html(){
		try{
			global $aplicacion;
			$this->link=!empty($_REQUEST["link"])?$_REQUEST["link"]:'';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function SmartQuiz(){
		try{
			global $aplicacion;
			$this->link=!empty($_REQUEST["link"])?$_REQUEST["link"]:'';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function smarticlock(){
		try{
			global $aplicacion;
			$this->link=!empty($_REQUEST["link"])?$_REQUEST["link"]:'';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function smarticlookPractice(){
		try{
			global $aplicacion;
			$this->link=!empty($_REQUEST["link"])?$_REQUEST["link"]:'';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function smarticDobyyourselft(){
		try{
			global $aplicacion;
			$this->link=!empty($_REQUEST["link"])?$_REQUEST["link"]:'';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function game(){
		try{
			global $aplicacion;
			$this->link=!empty($_REQUEST["link"])?$_REQUEST["link"]:'';
			$this->esquema = 'plantillas/html';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function video(){
		try{
			global $aplicacion;
			$this->link=!empty($_REQUEST["link"])?$_REQUEST["link"]:'';
			$this->esquema = 'plantillas/video';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function audio(){
		try{
			global $aplicacion;
			$this->idtmp=!empty($_REQUEST["idtmp"])?$_REQUEST["idtmp"]:'';
			$this->link=!empty($_REQUEST["link"])?$_REQUEST["link"]:'';
			$this->esquema = 'plantillas/audio';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function imagen(){
		try{
			global $aplicacion;
			//var_dump($_REQUEST);
			$this->idimagen=!empty($_REQUEST["idtmp"])?$_REQUEST["idtmp"]:'';
			$this->link=!empty($_REQUEST["link"])?$_REQUEST["link"]:'';
			$this->esquema = 'plantillas/imagen';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function indice(){
		try{
			global $aplicacion;
			$this->idtmp=!empty($_REQUEST["idtmp"])?$_REQUEST["idtmp"]:'';			
			$this->esquema = 'plantillas/indice';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function texto(){
		try{
			global $aplicacion;
			$this->idtmp=!empty($_REQUEST["idtmp"])?$_REQUEST["idtmp"]:'';			
			$this->esquema = 'plantillas/texto';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function widgets(){
		try{
			global $aplicacion;
			$this->idtmp=!empty($_REQUEST["idtmp"])?$_REQUEST["idtmp"]:'';			
			$this->esquema = 'plantillas/widget';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function portada1(){
		try{
			global $aplicacion;
			//var_dump($_REQUEST);
			$this->idimagen=!empty($_REQUEST["idtmp"])?$_REQUEST["idtmp"]:'';			
			$this->esquema = 'plantillas/portada1';
			$this->documento->plantilla='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}