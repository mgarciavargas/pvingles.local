<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-03-2017 
 * @copyright	Copyright (C) 24-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebQuiz extends JrWeb
{
	public function __construct()
	{
		parent::__construct();
	}

	public function defecto(){
		return $this->ver();
	}

	public function ver(){
		try{			
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			/*$filtros=array();*/
			$this->documento->setTitulo(JrTexto::_('SmartQuiz'));
			$urlsmartQuiz='https://www.abacoeducacion.org/web/smartquiz/';//!empty(URL_SMARTQUIZ)?URL_SMARTQUIZ:'https://www.abacoeducacion.org/web/smartquiz/';
			$idproyecto='Smartlearn';//!empty(IDPROYECTO)?IDPROYECTO:'smartlearn';

			$idproyecto='&pr='.(!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$idproyecto);
			$urlsmartQuiz=(!empty($_REQUEST["SMARTQUIZ"])?$_REQUEST["SMARTQUIZ"]:$urlsmartQuiz);
			$idexamen='?idexamen='.(!empty($_REQUEST["idexamen"])?$_REQUEST["idexamen"]:240);

			$usuario='&u='.(!empty($_REQUEST["usuario"])?$_REQUEST["usuario"]:$usuarioAct['usuario']);
			$id='&id='.(!empty($_REQUEST["idusuario"])?$_REQUEST["idusuario"]:$usuarioAct['idpersona']);
			$clave='&p='.(!empty($_REQUEST["clave"])?$_REQUEST["clave"]:$usuarioAct['clave']);			
			$this->link = $urlsmartQuiz.'examenes/resolver/'.$idexamen.$idproyecto.$id.$usuario.$clave;
			
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'blanco'; 			 	
			$this->esquema = 'examenes/resolver';
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
		//$paginaext="https://www.abacoeducacion.org/web/smartquiz/examenes/resolver/?idexamen=$idexa&id=$idalu&pr=edukt2017&u=$nomalu";
	}

	public function buscar(){
		try{			
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();		
			$this->documento->setTitulo(JrTexto::_('SmartQuiz'));
			$idproyecto='Smartlearn';//!empty(IDPROYECTO)?IDPROYECTO:'smartlearn';
			$this->idproyecto=!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$idproyecto;		
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'blanco';          
			$this->esquema = 'examenes/buscar';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function crear(){
		try{
			//$paginaext="https://www.abacoeducacion.org/web/smartquiz/examenes/ver/?type=admin&id=$idalu&pr=edukt2017&u=$nomalu";
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();		
			$this->documento->setTitulo(JrTexto::_('SmartQuiz'));
			$urlsmartQuiz='https://www.abacoeducacion.org/web/smartquiz/';//!empty(URL_SMARTQUIZ)?URL_SMARTQUIZ:'https://www.abacoeducacion.org/web/smartquiz/';
			$idproyecto='smartlearn';//!empty(IDPROYECTO)?IDPROYECTO:'smartlearn';

			$idproyecto='&pr='.(!empty($_REQUEST["idproyecto"])?$_REQUEST["idproyecto"]:$idproyecto);
			$urlsmartQuiz=(!empty($_REQUEST["SMARTQUIZ"])?$_REQUEST["SMARTQUIZ"]:$urlsmartQuiz);			
			$usuario='&u='.(!empty($_REQUEST["usuario"])?$_REQUEST["usuario"]:$usuarioAct['usuario']);
			$id='&id='.(!empty($_REQUEST["idusuario"])?$_REQUEST["idusuario"]:$usuarioAct['idpersona']);
			$clave='&p='.(!empty($_REQUEST["clave"])?$_REQUEST["clave"]:$usuarioAct['clave']);			
			$this->link = $urlsmartQuiz.'examenes/ver/?type=admin'.$idproyecto.$id.$usuario.$clave;
			//var_dump($this->link);
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'blanco'; 			 	
			$this->esquema = 'examenes/resolver';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}