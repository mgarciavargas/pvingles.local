<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-02-2018 
 * @copyright	Copyright (C) 08-02-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
class WebAcad_curso extends JrWeb
{
	private $oNegAcad_curso;
	
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_curso = new NegAcad_curso;
		
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_curso', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["vinculosaprendizajes"])&&@$_REQUEST["vinculosaprendizajes"]!='')$filtros["vinculosaprendizajes"]=$_REQUEST["vinculosaprendizajes"];
			if(isset($_REQUEST["materialesyrecursos"])&&@$_REQUEST["materialesyrecursos"]!='')$filtros["materialesyrecursos"]=$_REQUEST["materialesyrecursos"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["autor"])&&@$_REQUEST["autor"]!='')$filtros["autor"]=$_REQUEST["autor"];
			if(isset($_REQUEST["aniopublicacion"])&&@$_REQUEST["aniopublicacion"]!='')$filtros["aniopublicacion"]=$_REQUEST["aniopublicacion"];

			$this->datos=$this->oNegAcad_curso->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Acad_curso'), true);
			$this->esquema = 'acad_curso-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_curso', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Acad_curso').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_curso', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_curso->idcurso = @$_GET['id'];
			$this->datos = $this->oNegAcad_curso->dataAcad_curso;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Acad_curso').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_curso-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_curso', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["vinculosaprendizajes"])&&@$_REQUEST["vinculosaprendizajes"]!='')$filtros["vinculosaprendizajes"]=$_REQUEST["vinculosaprendizajes"];
			if(isset($_REQUEST["materialesyrecursos"])&&@$_REQUEST["materialesyrecursos"]!='')$filtros["materialesyrecursos"]=$_REQUEST["materialesyrecursos"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];			
			if(isset($_REQUEST["autor"])&&@$_REQUEST["autor"]!='')$filtros["autor"]=$_REQUEST["autor"];
			if(isset($_REQUEST["aniopublicacion"])&&@$_REQUEST["aniopublicacion"]!='')$filtros["aniopublicacion"]=$_REQUEST["aniopublicacion"];
						
			$this->datos=$this->oNegAcad_curso->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarcategorias(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;	
			$acc=!empty($_REQUEST["acc"])?$_REQUEST["acc"]:'';
			if($acc=='I'){
				$idcat=!empty($_REQUEST["idcat"])?$_REQUEST["idcat"]:'';
				$idcur=!empty($_REQUEST["idcur"])?$_REQUEST["idcur"]:'';
				$idnew=$this->oNegAcad_curso->insertcategoriasxcurso($idcat,$idcur);				
				echo json_encode(array('code'=>'ok','data'=>$idnew));
				exit(0);
			}else if($acc=='D'){
				$id=isset($_REQUEST["id"])?$_REQUEST["id"]:'';				
				if($id!='')	$this->oNegAcad_curso->eliminarcategoriasxcurso($id);
				echo json_encode(array('code'=>'ok'));
			}
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}



	public function guardarAcad_curso(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;		
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
           // var_dump($_POST);
            @extract($_POST);
            $accion='_add';  
            $usuarioAct = NegSesion::getUsuario();          
            if(!empty($idcurso)) {
				$this->oNegAcad_curso->idcurso = $idcurso;
				if(!empty($vinculosaprendizajes))$this->oNegAcad_curso->vinculosaprendizajes=$vinculosaprendizajes;
				if(!empty($materialesyrecursos))$this->oNegAcad_curso->materialesyrecursos=$materialesyrecursos;
				if(!empty($abreviatura))$this->oNegAcad_curso->abreviatura=@$abreviatura;
				if(!empty($estado))$this->oNegAcad_curso->estado=@$estado;
				$accion='_edit';
			}else{
				$this->oNegAcad_curso->imagen_='';
				$this->oNegAcad_curso->vinculosaprendizajes=!empty($vinculosaprendizajes)?$vinculosaprendizajes:'';
				$this->oNegAcad_curso->materialesyrecursos=!empty($materialesyrecursos)?$materialesyrecursos:'';
			}
			$this->oNegAcad_curso->nombre=@trim($nombre);       		
			$this->oNegAcad_curso->descripcion=@trim($descripcion);
			$this->oNegAcad_curso->estado=@$estado;	
			$this->oNegAcad_curso->color=@$color;			
			$this->oNegAcad_curso->autor=!empty($autor)?$autor:$usuarioAct["nombre_full"];
			$this->oNegAcad_curso->aniopublicacion=!empty($aniopublicacion)?$aniopublicacion:date('Y');
						
			if($accion=='_add'){				
				$this->oNegAcad_curso->idusuario=@$usuarioAct['idpersona'];
            	$this->oNegAcad_curso->fecharegistro=!empty($fecharegistro)?$fecharegistro:date('Y-m-d');
            	$newid=$this->oNegAcad_curso->agregar();
            	$newimagen='';
			}else{
				$newimagen=URL_MEDIA.$this->oNegAcad_curso->imagen;
				$newid=$this->oNegAcad_curso->editar();
			}

			if(!empty($_FILES['imagen']['name']) && empty($resetimagen)){
				$file=$_FILES["imagen"];
				$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
				$newfoto='logo_curso.'.$ext;
				$dir=RUTA_MEDIA.SD.'static' . SD . 'media' . SD . 'cursos'.SD.'curso_'.$newid;
				if(!file_exists($dir)){@mkdir($dir, 0777, true);}
				if(move_uploaded_file($file["tmp_name"],$dir. SD.$newfoto)) 
		  		{	//$oldimagen=$this->oNegAcad_curso->imagen;		  			
		  			$newimagen='/static/media/cursos/curso_'.$newid."/".$newfoto;//$this->oNegAcad_curso->imagen=$newimagen;		  			
		  			$this->oNegAcad_curso->setCampo($newid,'imagen',$newimagen);//if($accion=='_edit'){if(is_file(RUTA_MEDIA.$oldimagen)) unlink(RUTA_MEDIA.$oldimagen);}
		  			$newimagen=URL_MEDIA.$newimagen;
		  		}
			}elseif(!empty($resetimagen)){
				$this->oNegAcad_curso->setCampo($newid,'imagen','');
			}
       							
            if($accion=='_add'){ 
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Curso')).' '.JrTexto::_('saved successfully'),'newid'=>$newid,'newimagen'=>$newimagen)); 
            }else{            	
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Curso')).' '.JrTexto::_('update successfully'),'newid'=>$newid,'newimagen'=>$newimagen)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}


	public function guardarjson(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;		
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }          
            @extract($_POST);
            $accion='_add';  
            $usuarioAct = NegSesion::getUsuario();          
            if(!empty($idcurso)){
				$this->oNegAcad_curso->idcurso = $idcurso;
				if(!empty($vinculosaprendizajes))$this->oNegAcad_curso->vinculosaprendizajes=$vinculosaprendizajes;
				if(!empty($materialesyrecursos))$this->oNegAcad_curso->materialesyrecursos=$materialesyrecursos;
				if(!empty($abreviatura))$this->oNegAcad_curso->abreviatura=@$abreviatura;				
				$accion='_edit';
			}else{
				$this->oNegAcad_curso->imagen_='';
				$this->oNegAcad_curso->vinculosaprendizajes=!empty($vinculosaprendizajes)?$vinculosaprendizajes:'';
				$this->oNegAcad_curso->materialesyrecursos=!empty($materialesyrecursos)?$materialesyrecursos:'';
			}
			$this->oNegAcad_curso->nombre=@trim($nombre);       		
			$this->oNegAcad_curso->descripcion=@trim($descripcion);
			$this->oNegAcad_curso->estado=@$estado;	
			$this->oNegAcad_curso->color=@$color;
			$this->oNegAcad_curso->autor=!empty($autor)?$autor:'';
			$this->oNegAcad_curso->imagen=!empty($imagen)?$imagen:'';
			$this->oNegAcad_curso->aniopublicacion=!empty($aniopublicacion)?$aniopublicacion:date('Y-m-d');
						
			if($accion=='_add'){				
				$this->oNegAcad_curso->idusuario=@$usuarioAct['idpersona'];
            	$this->oNegAcad_curso->fecharegistro=!empty($fecharegistro)?$fecharegistro:date('Y-m-d');
            	$newid=$this->oNegAcad_curso->agregar();				
				echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Curso')).' '.JrTexto::_('saved successfully'),'newid'=>$newid)); 
			}else{				
				$newid=$this->oNegAcad_curso->editar();
				echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Curso')).' '.JrTexto::_('update successfully'),'newid'=>$newid)); 
			}
			$this->oNegAcad_curso->setCampo($newid,'txtjson',$txtjson);
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$this->oNegAcad_curso->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function setCampojson(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;		
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }           
            @extract($_POST);
            if(!empty($id)&&!empty($campo)&&isset($valor)){
            	$this->oNegAcad_curso->setCampo($id,$campo,$valor);
            }
            echo json_encode(array('code'=>'200','msj'=>'ok'));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function eliminar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;	
			@extract($_POST);	
            if(empty($idcurso)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $this->oNegAcad_curso->idcurso = $idcurso;
            $this->oNegAcad_curso->eliminar();
            echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Curso')).', '.JrTexto::_('successfully removed')));            
            exit(0);            
		}catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}    
}