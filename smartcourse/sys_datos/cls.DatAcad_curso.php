<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-02-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_curso extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_curso";
			
			$cond = array();		
			
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}

			if(isset($filtros["aniopublicacion"])) {
					$cond[] = "aniopublicacion = " . $this->oBD->escapar($filtros["aniopublicacion"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT *, (select count(idcurso) from acad_cursodetalle n WHERE n.idcurso=c.idcurso AND tiporecurso='N') as nnivel,
					(select count(idcurso) from acad_cursodetalle u WHERE u.idcurso=c.idcurso AND tiporecurso='U') as nunidad,
					(select count(idcurso) from acad_cursodetalle l WHERE l.idcurso=c.idcurso AND tiporecurso='L') as nactividad,
					(select count(idcurso) from acad_cursodetalle e WHERE e.idcurso=c.idcurso AND tiporecurso='E') as nexamenes from acad_curso c";			
			
			$cond = array();
			if(isset($filtros["idcurso"])) {
					$cond[] = "c.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}

			if(isset($filtros["aniopublicacion"])) {
					$cond[] = "aniopublicacion = " . $this->oBD->escapar($filtros["aniopublicacion"]);
			}
			
			if(isset($filtros["autor"])) {
					$cond[] = "autor = " . $this->oBD->like($filtros["autor"]);
			}

			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}						
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if(!empty($filtros["orderby"])){
				if($filtros["orderby"]!='no'){
					$sql.=" ORDER BY ".$filtros["orderby"]." ASC";
				}
			}		
			//echo $sql;	
			//$sql .= " ORDER BY fecha_creado ASC";			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	public function buscarxcategoria($filtros=null){
		try{
			$sql = "SELECT ac.*,id as idcursocategoria,act.nombre as categoria, acc.idcategoria, act.descripcion as descripcioncategoria from acad_curso ac INNER JOIN acad_cursocategoria  acc ON ac.idcurso=acc.idcurso INNER JOIN acad_categorias  act ON acc.idcategoria=act.idcategoria";
			if(isset($filtros["solocategoria"])){
				$sql = "SELECT id as idcursocategoria,act.nombre as categoria, acc.idcategoria from acad_curso ac INNER JOIN acad_cursocategoria  acc ON ac.idcurso=acc.idcurso INNER JOIN acad_categorias  act ON acc.idcategoria=act.idcategoria";
			}

			if(isset($filtros["aniopublicacion"])) {
					$cond[] = "aniopublicacion = " . $this->oBD->escapar($filtros["aniopublicacion"]);
			}
			if(isset($filtros["autor"])) {
					$cond[] = "autor = " . $this->oBD->like($filtros["autor"]);
			}

			if(isset($filtros["idcategoria"])) {
					$cond[] = "acc.idcategoria = " . $this->oBD->escapar($filtros["idcategoria"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "ac.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}

			if(isset($filtros["texto"])){
				$cond[] = "concat(ac.nombre,' ',ac.descripcion,' ',act.nombre,' ',act.descripcion, autor) " . $this->oBD->Like($filtros["texto"]);
			}
						
			if(isset($filtros["estado"])) {
					$cond[] = "ac.estado = " . $this->oBD->escapar($filtros["estado"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if(!empty($filtros["orderby"])){
				if($filtros["orderby"]!='no'){
					$sql.=" ORDER BY ".$filtros["orderby"]." ASC";
				}
			}
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	public function insertar($nombre,$imagen,$descripcion,$estado,$fecharegistro,$idusuario,$vinculosaprendizajes,$materialesyrecursos,$color,$abreviado,$autor,$aniopublicacion)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_curso_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcurso) FROM acad_curso");
			++$id;			
			$estados = array('idcurso' => $id							
							,'nombre'=>$nombre
							,'imagen'=>$imagen
							,'descripcion'=>$descripcion
							,'estado'=>!empty($estado)?$estado:0
							,'fecharegistro'=>!empty($fecharegistro)?$fecharegistro:date('Y-m-d H:i:s')
							,'idusuario'=>$idusuario
							,'vinculosaprendizajes'=>!empty($vinculosaprendizajes)?$vinculosaprendizajes:''
							,'materialesyrecursos'=>!empty($materialesyrecursos)?$materialesyrecursos:''
							,'color'=>!empty($color)?$color:''
							,'abreviado'=>!empty($abreviado)?$abreviado:''
							,'autor'=>!empty($autor)?$autor:''
							,'aniopublicacion'=>!empty($aniopublicacion)?$aniopublicacion:date('Y-m-d')
							,'txtjson'=>''
							);
			
			$this->oBD->insert('acad_curso', $estados);			
			$this->terminarTransaccion('dat_acad_curso_insert');			
			return $id;
		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_curso_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	
	public function actualizar($id, $nombre,$imagen,$descripcion,$estado,$fecharegistro,$idusuario,$vinculosaprendizajes,$materialesyrecursos,$color,$abreviado,$autor,$aniopublicacion)
	{
		try {
			$this->iniciarTransaccion('dat_acad_curso_update');
			$estados = array('nombre'=>$nombre
							,'imagen'=>$imagen
							,'descripcion'=>$descripcion
							,'estado'=>!empty($estado)?$estado:0
							,'fecharegistro'=>$fecharegistro
							,'idusuario'=>$idusuario
							,'vinculosaprendizajes'=>$vinculosaprendizajes
							,'materialesyrecursos'=>$materialesyrecursos
							,'color'=>$color
							,'abreviado'=>!empty($abreviado)?$abreviado:''
							,'autor'=>!empty($autor)?$autor:''
							,'aniopublicacion'=>!empty($aniopublicacion)?$aniopublicacion:date('Y-m-d')
							//,'txtjson'=>$txtjson
							);
			
			$this->oBD->update('acad_curso ', $estados, array('idcurso' => $id));
		    $this->terminarTransaccion('dat_acad_curso_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *, (select count(idcurso) from acad_cursodetalle n WHERE n.idcurso=c.idcurso AND tiporecurso='N') as nnivel,
(select count(idcurso) from acad_cursodetalle u WHERE u.idcurso=c.idcurso AND tiporecurso='U') as nunidad,
(select count(idcurso) from acad_cursodetalle l WHERE l.idcurso=c.idcurso AND tiporecurso='L') as nactividad,
(select count(idcurso) from acad_cursodetalle e WHERE e.idcurso=c.idcurso AND tiporecurso='E') as nexamenes  FROM acad_curso  c "
					. " WHERE c.idcurso = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			$idremove=$this->oBD->delete('acad_curso', array('idcurso' => $id));
			$this->oBD->delete('acad_cursodetalle', array('idcurso' => $id));
			return $idremove;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}

	public function insertcategoriasxcurso($idcategoria,$idcurso){
		try {
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id) FROM acad_cursocategoria");
			++$id;			
			$estados = array('id' => $id							
							,'idcategoria'=>$idcategoria
							,'idcurso'=>$idcurso
							);			
			$this->oBD->insert('acad_cursocategoria', $estados);
			return $id;			
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}		
	}
	public function updatecategoriasxcurso($id,$idcategoria,$idcurso){
		try {			
			$estados = array('idcategoria'=>$idcategoria,'idcurso'=>$idcurso);
			$this->oBD->update('acad_cursocategoria', $estados,array('id' => $id));
			return $id;			
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}		
	}

	public function eliminarcategoriasxcurso($id){
		try {					
			$this->oBD->delete('acad_cursocategoria', array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}		
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_curso', array($propiedad => $valor), array('idcurso' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_curso").": " . $e->getMessage());
		}
	}
}