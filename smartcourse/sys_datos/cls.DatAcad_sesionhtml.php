<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		06-03-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_sesionhtml extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_sesionhtml").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_sesionhtml";
			
			$cond = array();		
			
			if(isset($filtros["idsesion"])) {
					$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["html"])) {
					$cond[] = "html = " . $this->oBD->escapar($filtros["html"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_sesionhtml").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_sesionhtml";			
			
			$cond = array();		
					
			
			if(isset($filtros["idsesion"])) {
					$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["html"])) {
					$cond[] = "html = " . $this->oBD->escapar($filtros["html"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_sesionhtml").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcurso,$idpersonal,$idcursodetalle,$html)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_sesionhtml_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idsesion) FROM acad_sesionhtml");
			++$id;
			
			$estados = array('idsesion' => $id
							
							,'idcurso'=>$idcurso
							,'idpersonal'=>$idpersonal
							,'idcursodetalle'=>$idcursodetalle
							,'html'=>$html							
							);
			
			$this->oBD->insert('acad_sesionhtml', $estados);			
			$this->terminarTransaccion('dat_acad_sesionhtml_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_sesionhtml_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_sesionhtml").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso,$idpersonal,$idcursodetalle,$html)
	{
		try {
			$this->iniciarTransaccion('dat_acad_sesionhtml_update');
			$estados = array('idcurso'=>$idcurso
							,'idpersonal'=>$idpersonal
							,'idcursodetalle'=>$idcursodetalle
							,'html'=>$html								
							);
			
			$this->oBD->update('acad_sesionhtml ', $estados, array('idsesion' => $id));
		    $this->terminarTransaccion('dat_acad_sesionhtml_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_sesionhtml").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_sesionhtml  "
					. " WHERE idsesion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_sesionhtml").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_sesionhtml', array('idsesion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_sesionhtml").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_sesionhtml', array($propiedad => $valor), array('idsesion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_sesionhtml").": " . $e->getMessage());
		}
	}
   
		
}