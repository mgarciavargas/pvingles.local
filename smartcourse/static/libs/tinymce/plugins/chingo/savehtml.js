tinymce.PluginManager.add('savehtml', function(editor, url) { 
    function showDialog(){ 
       var id =tinymce.activeEditor.id; 
       var content=tinymce.activeEditor.getContent();
       $('#'+id).parent().html(content).closest('._subirfileedit').removeClass('nohover');   
    }
  
    editor.addButton('savehtml', {
        tooltip: 'Save edit',
        icon: 'save',
        onclick: showDialog,
    });
    editor.addMenuItem('savehtml', {
        icon: 'save',
        text: 'Save',
        onclick: showDialog,
        context: 'Save',
        prependToContext: true
    });
    editor.addCommand('mcesavehtml', showDialog);
    this.showDialog = showDialog;
});