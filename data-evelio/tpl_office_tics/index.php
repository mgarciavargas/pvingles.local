<?php
$carpeta = $dst; /* dir_gerenciales.php::$dst */
$num_sesion = $nro_ses; /* dir_gerenciales.php::$nro_ses */

$http = !empty($_SERVER['HTTPS'])?'https://':'http://';
$host= !empty($_SERVER["HTTP_HOST"])?$_SERVER["HTTP_HOST"]:'abacoeducacion.org';
$URL_BASE = $http.$host.'/pvingles.local';
$server_dir = dirname(dirname(dirname(__FILE__)));


if(!is_dir($carpeta)) {
	echo ($carpeta." no es un directorio valido");
}

$strContenido = '';
$arrFiles = scanear_directorio($carpeta); #dir_gerenciales.php::scanear_directorio()
limpiarArrDir($arrFiles);
foreach ($arrFiles as $f) {
	$file = $carpeta."/".$f;
	if( is_file($file) ) {
		$fileName = pathinfo($file, PATHINFO_FILENAME);
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		if($ext=='swf') {
			$real_file = $carpeta."/ses".$num_sesion.".swf";
			$ruta_flash = str_replace($server_dir, $URL_BASE, $file);
			$strContenido='<object type="application/x-shockwave-flash" data="'.$ruta_flash.'" width="100%" height="100%" id="flash" style="visibility: visible;" class="redim_elem">
					<param name="movie" value="'.$ruta_flash.'" />
					<param name="quality" value="high" />
					<param name="scale" value="exactFit">
					<param name="wmode" value="transparent"><param name="allowFullScreen" value="true">
				</object>
						';
		} else if($ext=='mp4') {
			$strContenido='<video src="./'.$f.'" id="vid_sesion" controls="true" controlsList="nodownload" class="redim_elem"></video>
				';
		} else if($ext=='flv') {
			$strContenido='./'.$f;
			#++$i;
		} else if($ext=='pdf') {
			$strContenido='<iframe src="./'.$f.'" id="frame_pdf" frameborder="0" width="100%" class="redim_elem"></iframe>
				';
			#++$i;
		}
	}
}
echo ''; ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="./css/general.css" />
<?php if ($ext=='flv') { ?>
		<link rel="stylesheet" type="text/css" href="./jplayer/dist/skin/blue.monday/css/jplayer.blue.monday.min.css" />
		<script type="text/javascript" src="./jplayer/lib/jquery.min.js"></script>
		<script type="text/javascript" src="./jplayer/dist/jplayer/jquery.jplayer.min.js"></script>
		<script type="text/javascript">
		//<![CDATA[
		$(document).ready(function(){

			$("#jquery_jplayer_1").jPlayer({
				ready: function () {
					$(this).jPlayer("setMedia", {
						title: "Video Tutor",
						flv: '<?php echo $strContenido; ?>',
					});
				},
				swfPath: "./jplayer/dist/jplayer",
				solution: "flash, html",
				supplied: "flv",
				size: {
					width: "100%",
					height: "calc(100vh - 85px)",
					cssClass: "jp-video-360p"
				},
				useStateClassSkin: true,
				autoBlur: false,
				smoothPlayBar: true,
				keyEnabled: true,
				remainingDuration: true,
				toggleDuration: true
			});
		});
		//]]>
		</script>
<?php } ?>
	</head>
	<body>
<?php if ($ext=='flv') { ?>
		<div id="jp_container_1" class="jp-video jp-video-full" role="application" aria-label="media player">
			<div class="jp-type-single">
				<div id="jquery_jplayer_1" class="jp-jplayer"></div>
				<div class="jp-gui">
					<div class="jp-video-play">
						<button class="jp-video-play-icon" role="button" tabindex="0">play</button>
					</div>
					<div class="jp-interface">
						<div class="jp-progress">
							<div class="jp-seek-bar">
								<div class="jp-play-bar"></div>
							</div>
						</div>
						<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
						<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
						<div class="jp-controls-holder">
							<div class="jp-controls">
								<button class="jp-play" role="button" tabindex="0">play</button>
								<button class="jp-stop" role="button" tabindex="0">stop</button>
							</div>
							<div class="jp-volume-controls">
								<button class="jp-mute" role="button" tabindex="0">mute</button>
								<button class="jp-volume-max" role="button" tabindex="0">max volume</button>
								<div class="jp-volume-bar">
									<div class="jp-volume-bar-value"></div>
								</div>
							</div>
							<div class="jp-toggles">
								<button class="jp-repeat" role="button" tabindex="0">repeat</button>
								<button class="jp-full-screen" role="button" tabindex="0">full screen</button>
							</div>
						</div>
					</div>
				</div>
				<div class="jp-no-solution">
					<span>Update Required</span>
					To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
				</div>
			</div>
		</div>
<?php } else { ?>
		<div class="container">
			<div class="main clearfix">
				<?php echo $strContenido; ?>
			</div>
		</div><!-- /container -->
<?php } ?>
	</body>
</html>