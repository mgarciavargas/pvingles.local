<?php
$carpeta = $dst; /* dir_gerenciales.php::$dst */

$http = !empty($_SERVER['HTTPS'])?'https://':'http://';
$host= !empty($_SERVER["HTTP_HOST"])?$_SERVER["HTTP_HOST"]:'abacoeducacion.org';
$URL_BASE = $http.$host.'/pvingles.local';
$server_dir = dirname(dirname(dirname(__FILE__)));


if(!is_dir($carpeta)) {
	echo ($carpeta." no es un directorio valido");
}

$strContenido = '';
$strPilaArch = '';
$i = 1;
$arrTipoArch = array(
	'mp4' => 'Vídeo',
	'flv' => 'Vídeo',
	'pdf' => 'Manual',
);
$arrFiles = scanear_directorio($carpeta); /* dir_gerenciales.php::scanear_directorio() */
limpiarArrDir($arrFiles);
foreach ($arrFiles as $f) {
	$file = $carpeta."/".$f;
	if( is_file($file) ) {
		/*$arrName = explode('.', $f);
		$ext = $arrName[ count($arrName)-1 ];*/
		$fileName = pathinfo($file, PATHINFO_FILENAME);
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		if($ext=='jpg') {
			$strContenido.='<div class="bb-item">
							<a href="#"><img src="./'.$f.'" alt="image'.$i.'"/></a>
						</div>
						';
			++$i;
		} else if($ext=='mp4') {
			$strContenido.='<video src="./'.$f.'" id="vid_sesion" class="redim_elem" controls="true" controlsList="nodownload"></video>
						';
			#++$i;
		} else if($ext=='flv') {
			$strContenido.='<video src="./'.$fileName.'.mp4" id="vid_sesion" class="redim_elem" controls="true" controlsList="nodownload"></video>
						';
			#++$i;
		} else if($ext=='pdf') {
			$strContenido.='<iframe src="./'.$f.'" id="frame_pdf" frameborder="0" width="100%" class="redim_elem"></iframe>
						';
			#++$i;
		}

		/* Pila de Archivos */
		if($ext=='mp4' || $ext=='pdf') {
			$strPilaArch.='<li class="item-arch">
							<a href="#" data-cargar="./'.$f.'" data-tipo="'.$ext.'">'.$arrTipoArch[$ext].' '.$i.'</a>
						</li>';
			++$i;
		} else if($ext=='flv') {
			$strPilaArch.='<li class="item-arch">
							<a href="#" data-cargar="./'.$fileName.'.mp4" data-tipo="mp4">'.$arrTipoArch[$ext].' '.$i.'</a>
						</li>';
			++$i;
		}
	}
}
echo ''; ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="./css/bookblock.css" />
		<link rel="stylesheet" type="text/css" href="./css/general.css" />
		<!-- custom demo style -->
		<script src="./js/modernizr.custom.js"></script>
	</head>
	<body>
		<div class="container">
			<div class="main clearfix">
				<div class="<?php echo ($ext=='jpg')?'bb-custom-wrapper':''; ?> ">
<?php if($ext=='jpg') { ?>
					<nav>
						<!--a id="bb-nav-first" href="#" class="bb-custom-icon bb-custom-icon-first">First page</a-->
						<a id="bb-nav-prev" href="#" class="bb-custom-icon bb-custom-icon-arrow-left">Previous</a>
						<a id="bb-nav-next" href="#" class="bb-custom-icon bb-custom-icon-arrow-right">Next</a>
						<!--a id="bb-nav-last" href="#" class="bb-custom-icon bb-custom-icon-last">Last page</a-->
					</nav>
<?php } 
if(!empty($strPilaArch)) { ?>
					<ul class="pila_archivos">
						<?php echo $strPilaArch; ?>
					</ul>
<?php } ?>
					<div <?php echo ($ext=='jpg')?'id="bb-bookblock" class="bb-bookblock"':'';?> >
						<?php echo $strContenido; ?>

					</div>
				</div>
			</div>
		</div><!-- /container -->
		<script src="./js/jquery.min.js"></script>
		<script src="./js/jquerypp.custom.js"></script>
<?php if($ext=='jpg') { ?>
		<script src="./js/jquery.bookblock.min.js"></script>
		<script>
			var Page = (function() {
				
				var config = {
						$bookBlock : $( '#bb-bookblock' ),
						$navNext : $( '#bb-nav-next' ),
						$navPrev : $( '#bb-nav-prev' ),
						$navFirst : $( '#bb-nav-first' ),
						$navLast : $( '#bb-nav-last' )
					},
					init = function() {
						config.$bookBlock.bookblock( {
							speed : 800,
							shadowSides : 0.8,
							shadowFlip : 0.7
						} );
						initEvents();
					},
					initEvents = function() {
						
						var $slides = config.$bookBlock.children();

						// add navigation events
						config.$navNext.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'next' );
							return false;
						} );

						config.$navPrev.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						} );

						config.$navFirst.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'first' );
							return false;
						} );

						config.$navLast.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'last' );
							return false;
						} );
						
						// add swipe events
						$slides.on( {
							'swipeleft' : function( event ) {
								config.$bookBlock.bookblock( 'next' );
								return false;
							},
							'swiperight' : function( event ) {
								config.$bookBlock.bookblock( 'prev' );
								return false;
							}
						} );

						// add keyboard events
						$( document ).keydown( function(e) {
							var keyCode = e.keyCode || e.which,
								arrow = {
									left : 37,
									up : 38,
									right : 39,
									down : 40
								};

							switch (keyCode) {
								case arrow.left:
									config.$bookBlock.bookblock( 'prev' );
									break;
								case arrow.right:
									config.$bookBlock.bookblock( 'next' );
									break;
							}
						} );
					};

					return { init : init };

			})();
		</script>
		<script>
			Page.init();
		</script>
		<script>
			$(document).ready(function() {
				var $img = $('.bb-bookblock .bb-item img').first();
				var w = $img.outerWidth();
				var h = $img.outerHeight();
				$('#bb-bookblock').css({
					"height": h,
					"width": w
				});	
			});
		</script>
<?php } else if($ext=='mp4'||$ext=='flv' || $ext=='pdf') { ?>
		<script>
			function calcDimension () {
				var $video = $('.redim_elem').first();
				var w = $(window).width() - 25;
				var h = $(window).height() - 20;
				$video.css({
					"height": h,
					"width": w
				});	
			}
			$(document).ready(function() {
				calcDimension();

				$(window).resize(function(e) {
					calcDimension();
				});


				$('.pila_archivos .item-arch a').click(function(e) {
					e.preventDefault();
					var tipo = $(this).attr('data-tipo');
					var contenido = $(this).attr('data-cargar');
					if(tipo=='mp4') {
						$('#vid_sesion').attr('src',contenido);
					} else if(tipo=='pdf') {
						$('#frame_pdf').attr('src',contenido);
					}

					return false;
				});
			});
		</script>
<?php } ?>
	</body>
</html>