<?php 
$mysqli = new mysqli("127.0.0.1","root","","cursos_postgres");
$http = !empty($_SERVER['HTTPS'])?'https://':'http://';
$host= !empty($_SERVER["HTTP_HOST"])?$_SERVER["HTTP_HOST"]:'abacoeducacion.org';
$URL_BASE = $http.$host.'/pvingles.local';
$server_dir = dirname(dirname(__FILE__));
$dir_cursos = $server_dir.'/static/media/cursos';

$dir_tmpl = "./tpl_gerenciales";
$dir_Gerenciales = $dir_cursos.'/Habilidades Gerenciales';

if(!is_dir($dir_Gerenciales)) {
	exit($dir_Gerenciales." no es un directorio valido");
}
if ($mysqli->connect_errno){
	exit("No se pudo conectar :(");
}

/*$arrCarpetas = array(
	'apertura',
	'ejercicios',
	'guía',
	'objetivos',
	'revisión',
	'video'
);*/

/* * * * 		main()		* * * */
$arrTemas = scan_directorio($dir_Gerenciales);
limpiarArrDir($arrTemas);
foreach ($arrTemas as $t) { echo "<div style='margin-left:30px'>";
	mensaje(null, "Carpeta : ".$t );
	$carpeta = $dir_Gerenciales.'/'.$t.'/ses1';
	$dir_tema = iconv("UTF-8", "windows-1254", $carpeta);
	$arrPestanias = scan_directorio($dir_tema);
	limpiarArrDir($arrPestanias);
	foreach ($arrPestanias as $p) {
		$ruta = iconv("UTF-8", "windows-1254",  $carpeta.'/'.$p );
		delete_dir_files($ruta , 'css');
		delete_dir_files($ruta , 'js');
		delete_dir_files($ruta , 'index.html');
	}
	break;
echo "</div>"; }

function mensaje($color='#38cfd6', $textShow='', $array=array()) {
	if(empty($color)) { $color = '#38cfd6'; }
	echo "<pre class='msj-debug'> <span style='background:".$color."; color: white;'>";
	echo $textShow;
	echo "</span></pre>";
	if(!empty($array)){ var_dump($array); }
}

function scan_directorio($dir='') {
	mensaje( 'green', 'LEYENDO : '.$dir );
	if(is_dir($dir)) {
		$arrDirectorio = array();
		$arr = scandir( $dir );
		foreach ($arr as $d) {
			$arrDirectorio[] = iconv("windows-1254", "UTF-8", $d);
		}
		var_dump($arrDirectorio);
		return $arrDirectorio;
	} else {
		mensaje( 'red', '"'.$dir.'" : No es un directorio válido.' );
		return null;
	}
}

function limpiarArrDir(&$arr) {
	$i = array_search('.', $arr);
	unset($arr[$i]);
	$j = array_search('..', $arr);
	unset($arr[$j]);
	$arr = array_values($arr);
}

function delete_dir_files($ruta, $target) {
	$ruta_ful = $ruta.'/'.$target;
	if( !is_dir($ruta_ful) && !file_exists($ruta_ful) ) {
		mensaje('red', "FILE o DIR no existe : ".$ruta_ful );
		return false;
	}
	if( is_file($ruta_ful) ) {
		mensaje('black', "Borrando FILE : ".$ruta_ful );
		unlink($ruta_ful);
	} else {
		mensaje('black', "Borrando DIR : ".$ruta_ful );
		array_map('unlink', glob($ruta_ful."/*.*"));
		rmdir($ruta_ful);
	}
}