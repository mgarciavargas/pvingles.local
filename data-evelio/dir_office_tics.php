<?php 
$mysqli = new mysqli("127.0.0.1","root","","cursos_postgres");
$http = !empty($_SERVER['HTTPS'])?'https://':'http://';
$host= !empty($_SERVER["HTTP_HOST"])?$_SERVER["HTTP_HOST"]:'abacoeducacion.org';
$URL_BASE = $http.$host.'/pvingles.local';
$server_dir = dirname(dirname(__FILE__));
$dir_cursos = $server_dir.'/static/media/cursos';

$dir_tmpl = "./tpl_office_tics";
$dir_Office = $server_dir.'/static/media/cursos';

if(!is_dir($dir_Office)) {
	exit($dir_Office." no es un directorio valido");
}
if ($mysqli->connect_errno){
	exit("No se pudo conectar :(");
}

$cont = $x = 0;
$arrDirCursos = array();
$i = null;
$arrDirCursos = scan_directorio($dir_Office);

$arrPestanias = array(
	'objetivos',
	'sesion',
	'video tutor',
	'video interactivo',
	'presentacion',
	'manual',
);

$mysqli->query("SET NAMES 'utf8'");
$q_cursos = $mysqli->query( "SELECT idcurso, nombre, abreviado FROM acad_curso WHERE estado=1 
	AND idcurso IN (35) 
	ORDER BY nombre" );
while($cur=$q_cursos->fetch_assoc()){
	$arrDirSesiones = array();

	if( in_array($cur['nombre'], $arrDirCursos) ) {
		mensaje(null, "Curso de BD : ".$cur['nombre'] );
		$i = array_search($cur['nombre'], $arrDirCursos);

		$dir_sesiones = $dir_Office.'/'.utf8_decode($arrDirCursos[$i]);

		if(!is_dir($dir_sesiones)) {
			mensaje( 'red', $dir_sesiones." no es un directorio valido" );
		} else {
			$x++;
			$arrDirSesiones = scan_directorio($dir_sesiones);

			$new_dir_Curso = $dir_cursos.'/curso_'.$cur['idcurso'];
			mkdir($new_dir_Curso, 0777);

			$mysqli->query("SET NAMES 'utf8'");
			$q_cursosDet = $mysqli->query( 
				"SELECT 
					CD.idcursodetalle, CD.tiporecurso, CD.url,
					CD.orden , N.nombre AS nivel_nombre, CD.idrecurso
				FROM acad_cursodetalle CD
				LEFT JOIN niveles N on CD.idrecurso = N.idnivel
				WHERE CD.idcurso = ". $cur["idcurso"] .
				" ORDER BY CD.orden"
			);

			while($det=$q_cursosDet->fetch_assoc()){echo "<div style='margin-left:60px;'>";
				$arrDirContenido = array();
				$sesion = 'ses'.$det['orden'];
				if( in_array($sesion, $arrDirSesiones) ) {
					mensaje(
						'blue',
						"Sesion de BD : ".$det['nivel_nombre']."/ses".$det['orden']
					);
					$i = array_search($sesion, $arrDirSesiones);

					$dir_contenidoSes = $dir_sesiones.'/'.$sesion;
					if(!is_dir($dir_contenidoSes)) { 
						mensaje( 'red', $dir_contenidoSes." no es un directorio valido" );
					} else { 
						$arrEstructura = $arrOptions = array();
						/* Crear la nueva carpeta del CursoDetalle */
						$new_dir_Sesion = $new_dir_Curso.'/detalle_'.$det['idcursodetalle'];
						mkdir($new_dir_Sesion, 0777);

						/* Copiar contenido de la carpeta original a la nueva creada del CursoDetalle */
						/*$carpeta_actual_parsed = iconv("UTF-8", "windows-1254", $dir_contenidoSes);
						var_dump("carpeta_actual_parsed : ". $dir_contenidoSes );*/
						recurse_copy($dir_contenidoSes , $new_dir_Sesion);


						$arrDirContenido = scan_directorio($new_dir_Sesion);
						limpiarArrDir($arrDirContenido);
						foreach ($arrDirContenido as $index => $pest) { echo "<div style='margin-left:60px;'>";
							$source = $dir_tmpl.'/libs';
							$source_flv = $dir_tmpl.'/libs_flv';
							$carpeta_contenido = $new_dir_Sesion.'/'.$pest;

							if( is_file($carpeta_contenido.'/ses'.$det['orden'].'.swf') 
								|| is_file($carpeta_contenido.'/ses'.$det['orden'].'.pdf')
								|| is_file($carpeta_contenido.'/ses'.$det['orden'].'d.pdf')
								|| is_file($carpeta_contenido.'/ses'.$det['orden'].'.mp4') 
							) {
								recurse_copy($source, $carpeta_contenido);
								create_index($carpeta_contenido, $dir_tmpl, $det['orden']);
								
							} else if( is_file($carpeta_contenido.'/ses'.$det['orden'].'.flv') ) {
								recurse_copy($source_flv, $carpeta_contenido);
								create_index($carpeta_contenido, $dir_tmpl, $det['orden']);

							}

							$indice = array_search($pest, $arrPestanias);
							if(is_int($indice)) {
								$rutaIndex = 'index.html';
								if($pest=='objetivos' && !is_file($carpeta_contenido.'/index.html')) {
									$rutaIndex = 's'.$det['orden'].'/index.html';
								}
								if($pest=='sesion' && !is_file($carpeta_contenido.'/index.html')) {
									$rutaIndex = 's'.$det['orden'].'/ses'.$det['orden'].'.html';
								}
								$arrOptions[ $indice+1 ] = array(
									"nombre" => ucfirst($pest),
									"id" => uniqid(),
									"link" => str_replace($server_dir, '', $carpeta_contenido.'/'.$rutaIndex),
									"type" => "html",
									"color" => "rgba(0, 0, 0, 1)",
									"colorfondo" => "rgba(254, 227, 174, 1)"
								);
							}
							
							#break;
						echo "</div>";}
						
						var_dump("arrOptions  : ");
						var_dump($arrOptions);

						ksort($arrOptions);
						$arrEstructura = array(
							"options" => $arrOptions,
							"tipo" => "#showpaddopciones",
							"tipofile" => "arriba",
							"imagenfondo" => "",
							"infoavancetema" => 100
						);

						$q_updateDet = $mysqli->query( 
							"UPDATE `acad_cursodetalle` 
							SET `txtjson`= '".json_encode($arrEstructura)."' 
							WHERE `idcursodetalle`=".$det['idcursodetalle']
						);
					}
				} else {
					mensaje(
						'red',
						"Sesion de BD : ".$det['nivel_nombre']." : /ses".$det['orden']." no encontró carpeta /ses".$det['orden']." : ",
						$det
					);
				}
				
				#break;
			echo "</div>";}
		}

	} else {
		mensaje('red', "Curso de BD : ".$cur['nombre']." No encontró carpeta", $cur);
	}

	#if($x==1) break;
	echo "<hr>";
}

function mensaje($color='#38cfd6', $textShow='', $array=array()) {
	if(empty($color)) { $color = '#38cfd6'; }
	echo "<pre class='msj-debug'> <span style='background:".$color."; color: white;'>";
	echo $textShow;
	echo "</span></pre>";
	if(!empty($array)){ var_dump($array); }
}

function scan_directorio($dir='') {
	$arrDirectorio = array();
	$arr = scandir($dir);
	foreach ($arr as $d) {
		$arrDirectorio[] = iconv("windows-1254", "UTF-8", $d);
	}
	mensaje( 'green', 'LEYENDO : '.$dir );
	var_dump($arrDirectorio);
	return $arrDirectorio;
}

function scanear_directorio($dir='') { /* para create_index()::index.php */
	if(is_dir($dir)) {
		$arrDirectorio = array();
		$arrDirectorio = scandir( $dir );
		return $arrDirectorio;
	} else {
		return null;
	}
}

function limpiarArrDir(&$arr) {
	$i = array_search('.', $arr);
	unset($arr[$i]);
	$j = array_search('..', $arr);
	unset($arr[$j]);
	$arr = array_values($arr);
}

function recurse_copy($src,$dst) { 
    $dir = opendir($src);
    if(!is_dir($dst)) {
    	mensaje( 'brown', 'CREANDO DIR : '. $dst );
    	mkdir($dst, 0777);
    }
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
            	mensaje( 'orange', 'COPIANDO : '. $src . '/' . $file ."     ====>>>     ".$dst . '/' . $file );
                copy($src.'/'.$file  ,  $dst.'/'.$file); 
            } 
        } 
    } 
    closedir($dir); 
}

function create_index($dst, $dir_tmpl, $nro_ses) {
	try {
		$archivo = $dst.'/' . 'index' . '.html';
		mensaje( 'brown', 'CREATING FILE : '.$archivo );
		if(file_exists($archivo)) @unlink($archivo);
		$fp = fopen($archivo, "a");
		$tpl_esquema = $dir_tmpl.'/index.php';

		ob_start();
		require($tpl_esquema);
		$datos = ob_get_contents();
		ob_end_clean();

		$write = fputs($fp, $datos);
		fclose($fp);
		@chmod($archivo,0777);

		$preview_link = str_replace($server_dir, $URL_BASE, $archivo);
		echo '<a href="'.$preview_link.'" target="_blank">'.$preview_link.'</a>';
		//return true;
	} catch (Exception $e) {
		mensaje( 'red', 'Error Creando index.hmtl : ' .$e->getMessage() );
		//return null;
	}
}