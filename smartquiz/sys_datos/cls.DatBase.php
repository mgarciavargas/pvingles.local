<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
#JrCargador::clase('sys_datos::BD::BDPostgreSQL', RUTA_BASE, 'sys_datos::BD');
class DatBase
{
	protected $oBD;
	protected $num_regs;
	protected $usuario;	
	protected $usarBD= 'mysql'; /* 'pg'=BDPostgreSQL / 'mysql'=BDMySQLI */
	/*public function __construct()
	{
		$this->usarBD = 'pg';
	}*/
	public function conectar()
	{
		try {
			if($this->usarBD=='mysql'){
				JrCargador::clase('sys_datos::BD::BDMySQLI', RUTA_BASE, 'sys_datos::BD');
				$this->oBD = BDMySQLI::getInstancia(
										'127.0.0.1' 	// 'abacoeducacion.org'
										,'smartquiz' 	// 'abacoedu_smartquiz'
										,'root' 		// 'abacoedu_data'
										,'' 	// 'abacochiclayo2017@'
										);
			}
			if($this->usarBD=='pg'){
				JrCargador::clase('sys_datos::BD::BDPostgreSQL', RUTA_BASE, 'sys_datos::BD');
				$this->oBD = BDPostgreSQL::getInstancia(
									/*
										'127.0.0.1',
										'dbelearning1',
										'postgres',
										'postgres'
									*/
										'abacotraining.com',
										'abacovir_dbelearning',
										'abacovir_abacodb',
										'Serverabacodb1'
										);
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function setUsarBD($value)
	{
		$this->usarBD = $value;
	}
	public function getUsarBD()
	{
		return $this->usarBD;
	}
	public function iniciarTransaccion($id_transaccion = '')
	{
		$this->oBD->iniciarTransaccion($id_transaccion);
	}
	
	public function terminarTransaccion($id_transaccion = '')
	{
		$this->oBD->terminarTransaccion($id_transaccion);
	}
	
	public function cancelarTransaccion($id_transaccion = '')
	{
		$this->oBD->truncarTransaccion($id_transaccion);
	}
	
	public function setLimite($desde, $desplazamiento)
	{
		$this->oBD->setLimite($desde, $desplazamiento);
	}
	
	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function __set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($nombre);
		
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}
}