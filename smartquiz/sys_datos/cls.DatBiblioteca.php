<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-11-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatBiblioteca extends DatBase
{
	public function __construct()
	{
		try {
			#parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT COUNT(*) FROM biblioteca";
			
			$cond = array();		
			
			if(!empty($filtros["idbiblioteca"])) {
					$cond[] = "idbiblioteca = " . $this->oBD->escapar($filtros["idbiblioteca"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre " . $this->oBD->like($filtros["nombre"]);
			}
			if(!empty($filtros["link"])) {
					$cond[] = "link  " . $this->oBD->like($filtros["link"]);
			}
			if(!empty($filtros["tipo"])) {
                   if($filtros["tipo"]!='all')
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["fechareg"])) {
					$cond[] = "fechareg = " . $this->oBD->escapar($filtros["fechareg"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT * FROM biblioteca";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idbiblioteca"])) {
					$cond[] = "idbiblioteca = " . $this->oBD->escapar($filtros["idbiblioteca"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre " . $this->oBD->like($filtros["nombre"]);
			}
			if(!empty($filtros["link"])) {
					$cond[] = "link  " . $this->oBD->like($filtros["link"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["fechareg"])) {
					$cond[] = "fechareg = " . $this->oBD->escapar($filtros["fechareg"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql.=' ORDER BY idbiblioteca DESC';

			$this->oBD->setLimite(0,99999);

			//echo $sql; exit(0);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			parent::conectar();
			$sql = "SELECT  *  FROM biblioteca  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}
	
	public function insertar($nombre,$link,$tipo,$idproyecto,$idpersonal,$fechareg,$estado)
	{
		try {
			parent::conectar();
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idbiblioteca) FROM biblioteca");
			++$id;
			
			$estados = array('idbiblioteca' => $id							
							,'nombre'=>$nombre
							,'link'=>$link
							,'tipo'=>$tipo
							,'idproyecto'=>$idproyecto
							,'idpersonal'=>$idpersonal
							#,'fechareg'=>$fechareg
							,'estado'=>1
							);
			
			 $this->oBD->insert('biblioteca', $estados);			
	
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_biblioteca_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$link,$tipo,$idproyecto,$idpersonal,$fechareg,$estado)
	{
		try {
			parent::conectar();
			$this->iniciarTransaccion('dat_biblioteca_update');
			$estados = array('nombre'=>$nombre
							,'link'=>$link
							,'tipo'=>$tipo
							,'idproyecto'=>$idproyecto
							,'idpersonal'=>$idpersonal
							#,'fechareg'=>$fechareg
							,'estado'=>$estado							
							);
			
			$this->oBD->update('biblioteca ', $estados, array('idbiblioteca' => $id));
		    $this->terminarTransaccion('dat_biblioteca_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			parent::conectar();
			$sql = "SELECT  *  FROM biblioteca  "
					. " WHERE idbiblioteca = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			parent::conectar();
			return $this->oBD->delete('biblioteca', array('idbiblioteca' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			parent::conectar();
			$this->oBD->update('biblioteca', array($propiedad => $valor), array('idbiblioteca' => $id));
			return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}
}