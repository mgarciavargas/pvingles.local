<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-04-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatActividad_alumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM actividad_alumno";
			
			$cond = array();		
			
			if(!empty($filtros["idactalumno"])) {
					$cond[] = "idactalumno = " . $this->oBD->escapar($filtros["idactalumno"]);
			}
			if(!empty($filtros["iddetalleactividad"])) {
					$cond[] = "iddetalleactividad = " . $this->oBD->escapar($filtros["iddetalleactividad"]);
			}
			if(!empty($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(!empty($filtros["porcentajeprogreso"])) {
					$cond[] = "porcentajeprogreso = " . $this->oBD->escapar($filtros["porcentajeprogreso"]);
			}	
			if(!empty($filtros["habilidades"])) {
					$cond[] = "habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}	
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM actividad_alumno";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idactalumno"])) {
					$cond[] = "idactalumno = " . $this->oBD->escapar($filtros["idactalumno"]);
			}
			if(!empty($filtros["iddetalleactividad"])) {
					$cond[] = "iddetalleactividad = " . $this->oBD->escapar($filtros["iddetalleactividad"]);
			}
			if(!empty($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(!empty($filtros["porcentajeprogreso"])) {
					$cond[] = "porcentajeprogreso = " . $this->oBD->escapar($filtros["porcentajeprogreso"]);
			}	
			if(!empty($filtros["habilidades"])) {
					$cond[] = "habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}	
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM actividad_alumno  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	
	public function insertar($iddetalleactividad,$idalumno,$fecha,$porcentajeprogreso,$habilidades,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_actividad_alumno_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idactalumno) FROM actividad_alumno");
			++$id;
			
			$estados = array('idactalumno'=>$id
							,'iddetalleactividad'=>$iddetalleactividad
							,'idalumno'=>$idalumno
							,'fecha'=>$fecha
							,'porcentajeprogreso'=>$porcentajeprogreso
							,'habilidades'=>$habilidades
							,'estado'=>$estado
							);
			$this->oBD->insert('actividad_alumno', $estados);
			$this->terminarTransaccion('dat_actividad_alumno_insert');
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_actividad_alumno_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $iddetalleactividad,$idalumno,$fecha,$porcentajeprogreso,$habilidades,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_actividad_alumno_update');
			$estados = array('iddetalleactividad'=>$iddetalleactividad
							,'idalumno'=>$idalumno
							,'fecha'=>$fecha
							,'porcentajeprogreso'=>$porcentajeprogreso
							,'habilidades'=>$habilidades
							,'estado'=>$estado
							);
			
			$this->oBD->update('actividad_alumno ', $estados, array('idactalumno' => $id));
		    $this->terminarTransaccion('dat_actividad_alumno_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM actividad_alumno  "
					. " WHERE idactalumno = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('actividad_alumno', array('idactalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('actividad_alumno', array($propiedad => $valor), array('' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
   
	public function ultimaActividad($idalumno)
	{
		try {
			$sql = "SELECT  *  FROM actividad_alumno WHERE idactalumno = (SELECT MAX(idactalumno) FROM actividad_alumno WHERE idalumno=".$this->oBD->escapar($idalumno)." )";
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
}