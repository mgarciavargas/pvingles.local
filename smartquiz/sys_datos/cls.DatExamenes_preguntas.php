<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatExamenes_preguntas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Examenes_preguntas").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			#parent::conectar();
			$sql = "SELECT COUNT(*) FROM examenes_preguntas";
			$cond = array();
			if(!empty($filtros["idpregunta"])) {
					$cond[] = "idpregunta = " . $this->oBD->escapar($filtros["idpregunta"]);
			}
			if(!empty($filtros["idexamen"])) {
					$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}			
			if(!empty($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}			
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["habilidad"])) {
					$cond[] = "habilidades LIKE " . $this->oBD->escapar('%'.$filtros["habilidad"].'%');
			}
			if(!empty($filtros["template"])) {
					$cond[] = "template = " . $this->oBD->escapar($filtros["template"]);
			}
			if(!empty($filtros["template"])) {
					$cond[] = "template = " . $this->oBD->escapar($filtros["template"]);
			}
			if(!empty($filtros["idcontenedor"])) {
					$cond[] = "idcontenedor = " . $this->oBD->escapar($filtros["idcontenedor"]);
			}
			if(!empty($filtros["dificultad"])) {
					$cond[] = "dificultad = " . $this->oBD->escapar($filtros["dificultad"]);
			}
			if(!empty($filtros["idpregunta_origen"])) {
					$cond[] = "idpregunta_origen = " . $this->oBD->escapar($filtros["idpregunta_origen"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Examenes_preguntas").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			#parent::conectar();
			$sql = "SELECT * FROM examenes_preguntas";		
			
			$cond = array();		
			if(!empty($filtros["idpregunta"])) {
					$cond[] = "idpregunta = " . $this->oBD->escapar($filtros["idpregunta"]);
			}
			if(!empty($filtros["idexamen"])) {
					$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}
			if(!empty($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
						
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["habilidad"])) {
					$cond[] = "habilidades LIKE " . $this->oBD->escapar('%'.$filtros["habilidad"].'%');
			}
			if(!empty($filtros["template"])) {
					$cond[] = "template = " . $this->oBD->escapar($filtros["template"]);
			}	
			if(!empty($filtros["idcontenedor"])) {
					$cond[] = "idcontenedor = " . $this->oBD->escapar($filtros["idcontenedor"]);
			}
			if(!empty($filtros["dificultad"])) {
					$cond[] = "dificultad = " . $this->oBD->escapar($filtros["dificultad"]);
			}
			if(!empty($filtros["idpregunta_origen"])) {
					$cond[] = "idpregunta_origen = " . $this->oBD->escapar($filtros["idpregunta_origen"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY idpregunta ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Examenes_preguntas").": " . $e->getMessage());
		}
	}
	
	public function insertar($idexamen,$pregunta,$descripcion,$ejercicio,$idpadre,$tiempo,$puntaje,$idpersonal,$template,$habilidad,$idcontenedor,$dificultad,$idpregunta_origen)
	{
		try {
			#parent::conectar();
			
			$this->iniciarTransaccion('dat_examenes_preguntas_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpregunta) FROM examenes_preguntas");
			++$id;
			
			$estados = array('idpregunta' => $id							
							,'idexamen'=>$idexamen
							,'pregunta'=>$pregunta
							,'descripcion'=>$descripcion
							,'ejercicio'=>$ejercicio
							,'idpadre'=>$idpadre
							,'tiempo'=>$tiempo
							,'puntaje'=>$puntaje
							,'idpersonal'=>$idpersonal
							,'habilidades'=>$habilidad					
							);

			/* Fields accept NULL: */
			if (!empty($template)) { $estados['template'] = $template; }
			if (!empty($idcontenedor)) { $estados['idcontenedor'] = $idcontenedor; }
			if (isset($dificultad)) { $estados['dificultad'] = $dificultad; }
			if (!empty($idpregunta_origen)) { $estados['idpregunta_origen'] = $idpregunta_origen; }

			$this->oBD->insert('examenes_preguntas', $estados);			
			$this->terminarTransaccion('dat_examenes_preguntas_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_examenes_preguntas_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Examenes_preguntas").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idexamen,$pregunta,$descripcion,$ejercicio,$idpadre,$tiempo,$puntaje,$idpersonal,$template,$habilidad,$idcontenedor,$dificultad,$idpregunta_origen)
	{
		try {
			#parent::conectar();
			$this->iniciarTransaccion('dat_examenes_preguntas_update');
			$estados = array('idexamen'=>$idexamen
							,'pregunta'=>$pregunta
							,'descripcion'=>$descripcion
							,'ejercicio'=>$ejercicio
							,'idpadre'=>$idpadre
							,'tiempo'=>$tiempo
							,'puntaje'=>$puntaje
							,'idpersonal'=>$idpersonal
							,'habilidades'=>$habilidad							
							);
			
			/* Fields accept NULL: */
			if (!empty($template)) { $estados['template'] = $template; }
			if (!empty($idcontenedor)) { $estados['idcontenedor'] = $idcontenedor; }
			if (!empty($dificultad)) { $estados['dificultad'] = $dificultad; }
			if (!empty($idpregunta_origen)) { $estados['idpregunta_origen'] = $idpregunta_origen; }

			$res=$this->oBD->update('examenes_preguntas ', $estados, array('idpregunta' => $id));
		    $this->terminarTransaccion('dat_examenes_preguntas_update');
		    return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examenes_preguntas").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			#parent::conectar();
			$sql = "SELECT  *  FROM examenes_preguntas  "
					. " WHERE idpregunta = " . $this->oBD->escapar($id);			
			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Examenes_preguntas").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {			
			#parent::conectar();
			return $this->oBD->delete('examenes_preguntas', array('idpregunta' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Examenes_preguntas").": " . $e->getMessage());
		}
	}

	public function eliminarxfiltro($filtros=null)
	{
		try {			
			#parent::conectar();
			return $this->oBD->delete('examenes_preguntas', $filtros);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete Block")." ".JrTexto::_("Examenes_preguntas").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			#parent::conectar();
			$this->oBD->update('examenes_preguntas', array($propiedad => $valor), array('idpregunta' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examenes_preguntas").": " . $e->getMessage());
		}
	}

	public function insert_select($campo=null)
	{
		try {
			#parent::conectar();

			$this->iniciarTransaccion('dat_examenes_preguntas_duplicate');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpregunta) FROM examenes_preguntas");
			++$id;

			$sql = "INSERT INTO examenes_preguntas".
				"(idpregunta, idexamen, pregunta, descripcion, ejercicio, idpadre, tiempo, puntaje, idpersonal, template, habilidades, idcontenedor, dificultad, idpregunta_origen) ".
				"SELECT ".
					$id.
					" , ".(!empty($campo['idexamen'])? $campo['idexamen']:" idexamen ").
					" , ".(!empty($campo['pregunta'])? $campo['pregunta']:" pregunta ").
					" , ".(!empty($campo['descripcion'])? $campo['descripcion']:" descripcion ").
					" , ".(!empty($campo['ejercicio'])? $campo['ejercicio']:" ejercicio ").
					" , ".(!empty($campo['idpadre'])? $campo['idpadre']:" idpadre ").
					" , ".(!empty($campo['tiempo'])? $campo['tiempo']:" tiempo ").
					" , ".(!empty($campo['puntaje'])? $campo['puntaje']:" puntaje ").
					" , ".(!empty($campo['idpersonal'])? $campo['idpersonal']:" idpersonal ").
					" , ".(!empty($campo['template'])? $campo['template']:" template ").
					" , ".(!empty($campo['habilidades'])? $campo['habilidades']:" habilidades ").
					" , ".(!empty($campo['idcontenedor'])? $campo['idcontenedor']:" idcontenedor ").
					" , ".(!empty($campo['dificultad'])? $campo['dificultad']:" dificultad ").
					" , ".(!empty($campo['idpregunta_origen'])? $campo['idpregunta_origen']:" idpregunta_origen ").
				" FROM examenes_preguntas ".
				" WHERE idpregunta = ". $this->oBD->escapar($campo['idpregunta_origen']);
			//echo '<pre>'.$sql.'</pre>'; exit(0);
			$resp = $this->oBD->ejecutarSQL($sql);
			$this->terminarTransaccion('dat_examenes_preguntas_duplicate');	
			return $id;
		} catch (Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Duplicate")." ".JrTexto::_("Examenes_preguntas").": " . $e->getMessage());
		}
	}
}