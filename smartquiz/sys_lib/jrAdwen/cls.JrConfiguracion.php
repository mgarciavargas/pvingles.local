<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */

defined('RUTA_BASE') or die();

class JrConfiguracion
{
	protected static $configs = array();
	protected static $instancia;
	
	public function __construct()
	{
		self::$configs['nombre_sitio'] = 'Sitio web';
		self::$configs['com_defecto'] = 'defecto';

	}
	
	public static function getInstancia()
	{
		if(!is_object(self::$instancia)) {
			self::$instancia = new self;
		}
		
		return self::$instancia;
	}
	
	public static function get($propiedad)
	{
		JrConfiguracion::getInstancia();
		
		if(isset(self::$configs[$propiedad])) {
			return self::$configs[$propiedad];
		} else {
			return null;
		}
	}
	
	public static function get_()
	{
		return self::$configs;
	}
}