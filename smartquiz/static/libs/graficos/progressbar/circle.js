//plugin creado abel chingo Tello
//sirve para crear graficos pie (circulos cargando su porcentaje)
//derechos reservador abel_chingo@hotmail.com
$.fn.graficocircle=function(){		
  	  $(this).each(function(){
  	  var info=$(this);
  	  var d = new Date();     
      var id="idtmp"+d.getDate()+"_"+d.getMonth()+"_"+d.getFullYear()+''+d.getHours()+''+d.getMinutes()+''+d.getSeconds();
      var obj = document.createElement("div");
      obj.id=id;
      info.prepend(obj);
  	  var val1=info.attr('data-value')||100;
  	  var nval=(val1/100);
  	  var texto=info.attr('data-texto')||'';
  	  var bar = new ProgressBar.Circle(obj, {
		  strokeWidth: info.attr('data-canchoout')||18,   // ancho de color externo
		  trailWidth: info.attr('data-canchoin')||15,  //ancho de color interno
		  easing: info.attr('data-canimacion')||'easeInOut',  // animacion
		  duration: info.attr('data-ctiempo')||1400, //tiempo duracion
		  color: info.attr('data-ccolorout')||'#3555a7',  //color externo
		  trailColor: info.attr('data-ccolorin')||'#ccc', //color interno		 
		  svgStyle: null,
		  step: function(state, circle) {
		    var value = Math.round(circle.value() * 100);
		    var colortxt=info.attr('data-ccolortxt')||'#333'; //color de texto porcentaje
		    var txt=(texto!='')?('<br>'+texto):'';
		    if (value === 0) {
		      circle.setText('<b style="color:'+colortxt+'">'+value+'%'+txt+'</b>');
		    } else {
		      circle.setText('<b style="color:'+colortxt+'">'+value+'%'+txt+'</b>');
		    }
		   }
		}
	);
	bar.animate(nval);
	});
  }