/*
Autor: abel chingo tello
require jquery
*/
$.fn.cronometroa0=function(eve,fcall){
    this.each(function(i,v){     
        var obj=$(this);
        var timercronometro;
        var curtime={};       
        var showhora=false;
        var showmin=false;
        var showseg=false;
        var iniciado=false;
        var termino=false;
        var formato=function(){
            var time=obj.text();
            var text = time.split(':');
            if (text.length == 1) {
                curtime.seg=parseInt(text[0]);
                showseg=true;
            }else if (text.length == 2) {
                curtime.min=parseInt(text[0]);
                curtime.seg=parseInt(text[1]);
                showmin=true;
                showseg=true;
            }else if (text.length == 3) {
                curtime.hor=parseInt(text[0]);
                curtime.min=parseInt(text[1]);
                curtime.seg=parseInt(text[2]);
                showmin=true;
                showseg=true;
                showhora=true;
            }
        }
        var formato2=function(){
            var hor='00', min='00',seg='00';
            if(showhora) if(curtime.hor>9) hor=curtime.hor; else hor='0'+curtime.hor;
            if(showmin) if(curtime.min>9) min=curtime.min; else min='0'+curtime.min;
            if(showseg) if(curtime.seg>9) seg=curtime.seg; else seg='0'+curtime.seg;
            obj.text((showhora?(hor+':'):'')+(showmin?(min+':'):'')+seg);
            if(hor=='00'&&min=='00'&&seg=='00'){
                clearInterval(timercronometro);
                iniciado=false;
                if(!termino){
                    termino=true;
                    if(typeof fcall !== 'undefined' && jQuery.isFunction( fcall )){
                        fcall();
                    }
                }
            }
        }
        var iniciar=function(){
            iniciado=true;
            var fr=formato();
            if(fr==false) return false;
            timercronometro=setInterval(function(){
                curtime.seg-1;
                if (curtime.seg == 0) {
                    if (curtime.min == 0) {
                        if (curtime.hor > 0) {
                            curtime.hor--
                            curtime.min = 59;
                            curtime.seg = 59;
                        }
                    }else{
                        curtime.min--;
                        curtime.seg = 59;
                    }
                } else {
                    curtime.seg--;
                }
               formato2();        
            }, 1000);
        }
        obj.on('oncropausar',function(ev,fcall1){
            if(!termino){
                clearInterval(timercronometro);
                iniciado=false;
                 if(typeof fcall1 !== 'undefined' && jQuery.isFunction( fcall1 )){
                        fcall1();
                    }
            }
        });        

        obj.on('oncroiniciar',function(ev){
            if(iniciado==true) return false;
            iniciar();
        });

        obj.on('oncroreiniciar',function(ev,txt){
            clearInterval(timercronometro);
            termino=false;
            showhora=false;
            showmin=false;
            showseg=false;
            obj.text(txt);
            iniciar();
        });
        if(eve=='oncroiniciar'){
            obj.trigger('oncroiniciar');
        }
        
    });

    return this;
}