var _canvaspaint=acccionincanvas;
var freeDrawing=false;
var _fontFamily='Arial';
var _fontSize=12;
var _opacity=0.8;
$(document).mousemove(function(e){
	var offset=$("#micanvas").offset();
    divPos = {
        left: e.pageX - offset.left,
        top: e.pageY - offset.top
    };
});
function toolactive(obj){
	$('.herramienta-dibujo a').removeClass('active');
	if(obj)$(obj).addClass('active')
}

$('#vpizarra')
.on('click','.herramienta-dibujo .toolpuntero',function(){
	toolactive(this);
    canvasfabric.isDrawingMode=false;
    _canvaspaint='puntero';
}).on('click','.herramienta-dibujo .toollapiz',function(){
    toolactive(this);
    canvasfabric.isDrawingMode=true;
    canvasfabric.freeDrawingBrush.width=2;
    canvasfabric.freeDrawingBrush.opacity=_opacity;
}).on('click','.herramienta-dibujo .toolresaltador',function(){
	toolactive(this);
    canvasfabric.isDrawingMode=true;
    canvasfabric.freeDrawingBrush.width=20;
    canvasfabric.freeDrawingBrush.opacity=_opacity;
    //canvasfabric.isDrawingMode=true;
}).on('click','.herramienta-dibujo .toollinea',function(){
	toolactive(this);
	canvasfabric.isDrawingMode=false;
    var isMouseDown=false;
    _canvaspaint='linea';
    var reflinea;
        canvasfabric.on('mouse:down',function(event){           
            isMouseDown=true;            
            if(_canvaspaint=='linea'){
            	 var linea = new fabric.Line([0, 0, 0, 0],{left: divPos.left, top: divPos.top, stroke: colorborde});
            canvasfabric.add(linea);
            reflinea=linea;  //**Reference of rectangle object
           }
        });
        canvasfabric.on('mouse:move', function(event){
            if(!isMouseDown){ return; }
            if(_canvaspaint!='linea'){return;}
                var posX=divPos.left;
                var posY=divPos.top;    
                reflinea.set('width',Math.abs(posX-reflinea.get('left')));
                reflinea.set('height',Math.abs(posY-reflinea.get('top')));           
                canvasfabric.renderAll(); 
        });
        canvasfabric.on('mouse:up',function(){
        	if(_canvaspaint!='linea'){return;}
            canvasfabric.add(reflinea);            
            isMouseDown=false;
            _canvaspaint=false;
            canvasfabric.off('mouse:down');
            canvasfabric.off('mouse:move');
            canvasfabric.off('mouse:up');
            toolactive();            
        });
}).on('click','.herramienta-dibujo .toolsquare',function(){
	toolactive(this);
	canvasfabric.isDrawingMode=false;
    var isMouseDown=false;
    _canvaspaint='square';
    var refsquare;
        canvasfabric.on('mouse:down',function(event){           
          isMouseDown=true;            
          if(_canvaspaint!='square') return false;
          var rect1 = new fabric.Rect({ width: 0, height: 0, left:divPos.left, top:divPos.top, angle: 0, fill: colorfondo,opacity:_opacity  });
          canvasfabric.add(rect1);
          refsquare=rect1;  //**Reference of rectangle object
        });
        canvasfabric.on('mouse:move', function(event){
            if(!isMouseDown){ return; }
            if(_canvaspaint!='square') return false;
                var posX=divPos.left;
                var posY=divPos.top;    
                refsquare.set('width',Math.abs((posX-refsquare.get('left'))));
                refsquare.set('height',Math.abs((posY-refsquare.get('top'))));           
                canvasfabric.renderAll(); 
        });
        canvasfabric.on('mouse:up',function(){
        	if(_canvaspaint!='square') return false;
            canvasfabric.add(refsquare);            
            isMouseDown=false;
            _canvaspaint=false;             
            canvasfabric.off('mouse:down');
            canvasfabric.off('mouse:move');
            canvasfabric.off('mouse:up'); 
            toolactive();      
        });
}).on('click','.herramienta-dibujo .toolcircle',function(){
	toolactive(this);
	canvasfabric.isDrawingMode=false;
    var isMouseDown=false;
    _canvaspaint='circle';
    var refCircle;
        canvasfabric.on('mouse:down',function(event){
            isMouseDown=true;
            if(_canvaspaint!='circle') return false;
            var circle=new fabric.Circle({left:divPos.left,top:divPos.top, radius:0, stroke:colorborde, strokeWidth:2, fill:colorfondo,opacity:_opacity });
            canvasfabric.add(circle);
            refCircle=circle;
        });
        canvasfabric.on('mouse:move', function(event){
            if(!isMouseDown){ return; }
            if(_canvaspaint!='circle') return false;
            var posX=divPos.left;
            var posY=divPos.top;
            refCircle.set('radius',Math.abs((posX-refCircle.get('left'))));
            canvasfabric.renderAll();
        });
        canvasfabric.on('mouse:up',function(){
        	if(_canvaspaint!='circle') return false;
        	canvasfabric.add(refCircle);
            isMouseDown=false;
            _canvaspaint=false;
            canvasfabric.off('mouse:down');
            canvasfabric.off('mouse:move');
            canvasfabric.off('mouse:up');
            toolactive();
        });
}).on('click','.herramienta-dibujo .toolborrador',function(){
	canvasfabric.isDrawingMode=false;	
	if(canvasfabric.getActiveGroup()){
	      canvasfabric.getActiveGroup().forEachObject(function(o){ canvasfabric.remove(o) });
	      canvasfabric.discardActiveGroup().renderAll();
	}else{
		var activeobject=canvasfabric.getActiveObject();
		if(activeobject){
			activeobject.remove();
	    	canvasfabric.remove(activeobject);
	    }
	}		
    canvasfabric.renderAll.bind(canvasfabric);	
}).on('click','.herramienta-dibujo .toolborrador2',function(){
        canvasfabric.clear();
        canvasfabric.renderAll.bind(canvasfabric);
    
}).on('click','.herramienta-dibujo .tooltext',function(){
	canvasfabric.isDrawingMode=false;
    var isMouseDown=false;
    	_canvaspaint='text';
        canvasfabric.on('mouse:down',function(event){ 
           if(_canvaspaint!='text') return false;
            var texto = new fabric.IText("text",{fontFamily: _fontFamily,fontSize:_fontSize ,left:divPos.left, top:divPos.top,fill:colorfondo,opacity:1});
            canvasfabric.add(texto);
        });
        canvasfabric.on('mouse:up',function(event){         
            isMouseDown=false;
            _canvaspaint=false;
            canvasfabric.off('mouse:down');
            canvasfabric.off('mouse:up'); 
        });        
}).on('click','.herramienta-dibujo .tooltextfont ul li a',function(){
    if(canvasfabric.getActiveGroup()){
          canvasfabric.getActiveGroup().forEachObject(function(o){ o.set('fontFamily', _fontFamily) });
          canvasfabric.discardActiveGroup().renderAll();
    }else{
        var activeobject=canvasfabric.getActiveObject();
        if(activeobject){
            activeobject.set('fontFamily', _fontFamily);    
            canvasfabric.renderAll();       
        }
    }       
   _fontFamily=$(this).text();     
}).on('click','.herramienta-dibujo .tooltextsize ul li a',function(){
    if(canvasfabric.getActiveGroup()){
          canvasfabric.getActiveGroup().forEachObject(function(o){ o.set('opacity', _opacity) });
          canvasfabric.discardActiveGroup().renderAll();
    }else{
        var activeobject=canvasfabric.getActiveObject();
        if(activeobject){
            activeobject.set('opacity', _opacity);    
            canvasfabric.renderAll();       
        }
    }      
   _opacity=$(this).text(); 
   canvasfabric.renderAll();   
   $(this).closest('.tooltextsize').find('button span.text').text(_fontSize); 
}).on('click','.herramienta-dibujo .toolimage',function(){
	canvasfabric.isDrawingMode=false;
    var file=document.createElement('input');
    file.setAttribute("type", "file");
    file.setAttribute("accept", "image/*");
    file.click();
    var image='';
    var freeDrawing=true,isMouseDown=false;
    $(file).change(function(){
    	console.log(file);
    	freeDrawing=true;
    	image=window.URL.createObjectURL($(this).get(0).files[0]);
    });
    canvasfabric.on('mouse:down',function(event){           
        isMouseDown=true;
         if(freeDrawing){         	
         	fabric.Image.fromURL(image, function(oImg) {			   
		    	oImg.set({left:divPos.left});
		        oImg.set({top:divPos.top});		        
		        canvasfabric.add(oImg);
		    });
        }
    });
    canvasfabric.on('mouse:up',function(){         
        isMouseDown=false;
        freeDrawing=false;  // **Disables line drawing
    });
}).on('click','.herramienta-dibujo .tooldownload',function(){
	canvasfabric.getElement().toBlob(function(blob) {
		var link = document.createElement("a");
	    var objurl = URL.createObjectURL(blob);
	    link.download = "miimagen.png";
        link.target = '_blank';
	    link.href = objurl;
	    link.click();
	});
}).on('click','.herramienta-dibujo .toolcolorfondocanvas',function(){	
    canvasfabric.backgroundColor=colorcanvas;
    canvasfabric.renderAll();
}).on('click','.herramienta-dibujo .toolcolorline',function(){
    canvasfabric.freeDrawingBrush.color=colorborde;
    if(canvasfabric.getActiveObject()){
		canvasfabric.getActiveObject().setStroke(this.value);
    	canvasfabric.renderAll();
	}
}).on('click','.herramienta-dibujo .toolcolorfill',function(){	
});

