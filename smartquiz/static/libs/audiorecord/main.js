
var isurfer=0;
var wave=[];    
    var onda=function(id){
      if(wave['wave'+id]==undefined){
        wave['wave'+id] = Object.create(WaveSurfer); 
      }
      return wave['wave'+id];
    }

    var recrearonda=function(obj){
        var donde=obj.data('donde');
        var ir=obj.data('ir');
        blob=blobs['audio_'+ir];
        var _donde=$('#'+donde).find('.btnAction');        
        var url = URL.createObjectURL(blob);   
        var htmlsend='<span data-ir="'+ir+'" class="btn btn-primary btnSave"><i class="fa fa-envelope-o"></i></span>';
        var isu=crearonda(url,donde,htmlsend);
        
    }
function crearonda(url,donde,btnsend){
    isurfer++;  
    if(donde!=""||donde!=undefined){ 
    wavesurfer=onda(donde);
    var idsurf=donde+'_'+isurfer;
    var conda=$('#'+donde);
    var lista=conda.find('.lista').html()||'';
        lista='<div><table class="lista table table-responsive table-striped">'+lista+'</table></div>';
    var btnsend=btnsend||'';
    html='<div class="row">'+
            '<div class="col-md-12">'+
                '<div id="'+idsurf+'"></div>'+
                '<div><span data-ord="'+idsurf+'" class="btn btn-primary" id="btn_'+idsurf+'_"><i class="fa fa-play"></i></span> <span class="btnAction">'+btnsend+'</span></div>'+
            '</div>'+lista+
         '</div>';
    if(conda.html()!=''){
        wavesurfer.destroy();
    }
    conda.html(html);
    wavesurfer.init({
        container: document.querySelector('#'+idsurf),
        waveColor: '#A8DBA8',
        progressColor: '#3B8686',
        backend: 'MediaElement'
    });
    wavesurfer.load(url);
    document.querySelector('#btn_'+idsurf+'_').addEventListener('click', wavesurfer.playPause.bind(wavesurfer));
    }
    return isurfer;
}

function startRecording(){
    recording = recording + 1;
    recorder.clear();
    recorder && recorder.record();
}

var blobs=[];
function stopRecording(donde,ismultiple){
    recorder && recorder.stop();
    var ismultiple=ismultiple||false;
    recorder.exportMP3(function(blob){
        var url = URL.createObjectURL(blob);   
        var ir=crearonda(url,donde);
        if(ismultiple==true){
            blobs['audio_'+ir]=blob;
            htmllista='<tr><td class="text-right"><a class="aplayblob" data-donde="'+donde+'" data-ir="'+ir+'" >audio '+(ir-1)+'</a></td>'+
                      '<td class="text-center"><span class="btn btn-sm btnremovelista"><i class="fa fa-trash"></i></span></td></tr>';
            $('#'+donde).find('table.lista').append(htmllista);           
        }else
            blobs['audio_'+0]=blob; 
  });
}



function subirfile(oi,datai,fcal){ //funcion que sibira audio grabado
    var miblob=blobs['audio_'+oi];    
    var data = new FormData();
    data.append('filearchivo', miblob);
    data.append('nivel', datai.idNivel);
    data.append('unidad', datai.idUnidad);
    data.append('leccion', datai.idActividad);
    data.append('name',datai.namefile);
    data.append('type',miblob.type);
    $.ajax({
        url : _sysUrlBase_+"/biblioteca/cargarblob",
        type: 'POST',
        data: data,
        contentType: false,
        processData: false,
        success: function(res) {
           fcal(res);
        },    
        error: function(e) {
            console.log('Error inesperado intententelo mas tarde'+e);
        }
    });
}

function init(){
    audioRecorder.requestDevice(function(recorderObject){
      recorder = recorderObject;
    }, {recordAsOGG: false});
};
init();