tinymce.PluginManager.add('inputadd', function(editor, url) { 
    editor.on('click', function(ev) {
        var obj=ev.target;
        var acteditor=editor.getBody();
        var actobj=$(acteditor).find('._enedicion_');
        actobj.removeClass('_enedicion_');
        if($(obj).is('[data-mce-object]')){  
            $(obj).addClass('_enedicion_');
            editarinput();
        }else{
            $('.save-setting-textbox').trigger('click');
        }
    });

    function editarinput(){
        $('#activaredicioninput').trigger('click');
    }

    function showDialog(){
        var win, width, height, data={}, dom=editor.dom,element=editor.selection;
        var txt=element.getContent(),audiourl='';
        data = getData(editor.selection.getNode());
        var obj=data["data-mce-object"];
        if(obj==undefined){
            if(txt==''||txt==undefined){
                alert('You must select text.');
                return false;
            }else{
                var txt_escaped = txt.replace(/"/g, "''"); /* para que reoconozca texto entre comillas. */
               var acteditor=editor.getBody();
               var actobj=$(acteditor).find('._enedicion_');
               actobj.removeClass('_enedicion_');
               addclass=$('.plantilla-completar.enedicion').attr('data-addclass')||'';
               var html='<img class="mce-object-input valinput _enedicion_ '+addclass+'" data-texto="'+txt_escaped+'" type="text" width="100" height="25" data-mce-object="input" data-mce-selected=1>';
               var node=htmlToData(html);
               var el=editor.dom.create('img',node);
               editor.selection.setNode(el);
               editor.selection.select(editor.dom.select(el)[0]);
               editarinput();
            }
        }
    }

    function getSource() {
        var elm = editor.selection.getNode();
        if (elm.getAttribute('data-mce-object')) {
            return editor.selection.getContent();
        }
    }

    function dataToHtml(data) {
        var html = '',_addcss='';
        if (!data.texto) {
            data.texto = '';
        }
        var audio='';
        if (data.audio) {
            audio = 'data-audio="'+data.audio+'"';
        }
        if (data.visible==true) _addcss += ' ishidden';
        if (data.ayuda==true) _addcss += ' isayuda';
        if (data.write==true) _addcss += ' iswrite';
        //if (data.action) 
        _addcss += ' '+data.action;    
        if (!data.options) {
            data.options = '';
        }
        html='<input type="text" '+audio+' data-texto="'+dataTexto+'" data-options="'+data.options+'" class="mce-object-input valinput '+_addcss+'" />'; 
        return html;
    }

    function htmlToData(html) {
        var data = {};        
        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',
            start: function(name, attrs) {                             
                data = tinymce.extend(attrs.map, data);              
            }
        }).parse(html);
        return data;
    }

    function getData(element) {
        if (element.getAttribute('data-mce-object')) {
            return htmlToData(editor.serializer.serialize(element, {selection: true}));
        }

        return {};
    }

    function sanitize(html) {
        if (editor.settings.media_filter_html === false) {
            return html;
        }

        var writer = new tinymce.html.Writer(), blocked;

        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: false,
            special: 'script,noscript',

            comment: function(text) {
                writer.comment(text);
            },

            cdata: function(text) {
                writer.cdata(text);
            },

            text: function(text, raw) {
                writer.text(text, raw);
            },

            start: function(name, attrs, empty) {
                blocked = true;

                for (var i = 0; i < attrs.length; i++) {
                    if (attrs[i].name.indexOf('on') === 0) {
                        return;
                    }

                    if (attrs[i].name == 'style') {
                        attrs[i].value = editor.dom.serializeStyle(editor.dom.parseStyle(attrs[i].value), name);
                    }
                }

                writer.start(name, attrs, empty);
                blocked = false;
            },

            end: function(name) {
                if (blocked) {
                    return;
                }

                writer.end(name);
            }
        }, new tinymce.html.Schema({})).parse(html);

        return writer.getContent();
    }

    function updateHtml(html, data, updateAll) {
        var writer = new tinymce.html.Writer();
        var sourceCount = 0, hasImage;

        function setAttributes(attrs, updatedAttrs) {
            var name, i, value, attr;

            for (name in updatedAttrs) {
                value = "" + updatedAttrs[name];

                if (attrs.map[name]) {
                    i = attrs.length;
                    while (i--) {
                        attr = attrs[i];

                        if (attr.name == name) {
                            if (value) {
                                attrs.map[name] = value;
                                attr.value = value;
                            } else {
                                delete attrs.map[name];
                                attrs.splice(i, 1);
                            }
                        }
                    }
                } else if (value) {
                    attrs.push({
                        name: name,
                        value: value
                    });

                    attrs.map[name] = value;
                }
            }
        }

        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',

            comment: function(text) {
                writer.comment(text);
            },

            cdata: function(text) {
                writer.cdata(text);
            },

            text: function(text, raw) {
                writer.text(text, raw);
            },

            start: function(name, attrs, empty){

                if (updateAll){
                    setAttributes(attrs, {
                        texto: data.texto,
                        ayuda: data.ayuda,
                        visible: data.visible,
                        write: data.write,
                        action: data.action,
                        audio:data.audio
                    });
                }
                writer.start(name, attrs, empty);
            },

            end: function(name) {
                if (name == "input" && updateAll && !hasImage) {
                    var imgAttrs = [];
                    imgAttrs.map = {};
                    writer.start("img", imgAttrs, true);
                }

                writer.end(name);
            }
        }, new tinymce.html.Schema({})).parse(html);

        return writer.getContent();
    }

    editor.on('ResolveName', function(e) {
        var name;
        if (e.target.nodeType == 1 && (name = e.target.getAttribute("data-mce-object"))) {
            e.name = name;
        }
    });

    editor.on('preInit', function() {
         // Converts iframe, video etc into placeholder images
        editor.parser.addNodeFilter('input,div', function(nodes, name) {
            var i = nodes.length, ai, node, placeHolder, attrName, attrValue, attribs, innerHtml; 
            while (i--) {
                node = nodes[i];
                if (!node.parent){continue;}
                if(node.name=='div'){
                     attribs = node.attributes;
                    if(attribs==undefined) continue;
                }else{
                     attribs = node.attributes;
                }

                placeHolder = new tinymce.html.Node('img', 1);
                placeHolder.shortEnded = true;               
                ai = attribs.length;
                var ismedia=false;
                var media=null;

                while (ai--) {
                    attrName = attribs[ai].name;
                    attrValue = attribs[ai].value;
                    placeHolder.attr(attrName, attrValue);
                    if((attrValue=='audio'||attrValue=='video') && ismedia==false){
                        ismedia=true;
                        media=attrValue;
                    }                    
                }

                if(ismedia){
                    _media=node.firstChild;
                   if(_media!=undefined){
                    _mediaattr=_media.attributes;
                    a2 = _mediaattr.length;
                    while (a2--){
                        atName = _mediaattr[a2].name;
                        atValue = _mediaattr[a2].value;
                        if(atName==='src')atName='data-src';
                        placeHolder.attr(atName, atValue); 
                    }
                   }
                   placeHolder.attr({
                    width:(attrValue=='video'?'200':"100"),
                    height:(attrValue=='video'?'200':"25"),
                    //style: node.attr('style'),
                    "data-mce-object":media,
                    class:' mce-object-'+media
                    });
                }else{                
                    placeHolder.attr({
                        width: "100",
                        height:  "25" ,
                        style: node.attr('style'),
                        "data-mce-object": name,
                    });
                }
                node.replace(placeHolder);
            }
        });

        // Replaces placeholder images with real elements for video, object, iframe etc
        editor.serializer.addAttributeFilter('data-mce-object', function(nodes, name) {
            var i = nodes.length, node, realElm, ai, attribs, innerHtml, innerNode, realElmName;

            while (i--) {
                node = nodes[i];
                if (!node.parent) {
                    continue;
                }
                attribs = node.attributes;
                if(attribs==undefined)continue;
                var obj=attribs.map['data-mce-object'];
                var audio=null;
                if(obj==='audio'||obj==='video'){                    
                    realElmName='div';
                    audio=new tinymce.html.Node(obj,1);
                }
                else realElmName = node.attr(name);

                realElm = new tinymce.html.Node(realElmName, 1);
                ai = attribs.length;
                while (ai--) {
                    var attrName = attribs[ai].name;                    
                    if(obj==='audio'||obj==='video'){
                        if(attrName=='data-src') audio.attr('src',attribs[ai].value);
                        if(attrName=='alt') audio.attr('alt',attribs[ai].value);
                    }
                    realElm.attr(attrName, attribs[ai].value);                                  
                }
                if(obj==='audio'||obj==='video'){
                   audio.attr('controls','true');                   
                   if(obj=='video'){
                    audio.attr('class','col-md-12 col-sm-12 col-xs-12 data-mce-video embed-responsive-item');
                    realElm.attr('class','col-md-12 col-sm-12 col-xs-12 embed-responsive embed-responsive-16by9 mce-object-video');
                   }
                   if(obj=='audio'){
                    audio.attr('class','data-mce-audio');
                    realElm.attr('class','mce-object-audio');
                   }
                   realElm.attr('alt',obj+' Player');
                   realElm.attr('style',null);
                   realElm.attr('data-src',null);
                   realElm.append(audio); 
                }          
                node.replace(realElm);                
            }
        });
    });

    
    editor.on('ObjectSelected', function(e) {
        var objectType = e.target.getAttribute('data-mce-object');
    });    
    editor.addButton('inputadd', {
        tooltip: 'Insert/edit textbox',
        icon: 'fa fa-edit',
        onclick: showDialog,
        stateSelector: ['img[data-mce-object=input]']
    });
    editor.addMenuItem('inputadd', {
        icon: 'fa fa-edit',
        text: 'Insert/edit textbox',
        onclick: showDialog,
        context: 'insert',
        prependToContext: true
    });
    editor.addCommand('mceInput', showDialog);
    this.showDialog = showDialog;
});