tinymce.PluginManager.add('chingoaudio', function(editor, url){ 
    function showDialog(){
        var rutabase=_sysUrlBase_;
        var win, width, height, data={}, dom=editor.dom,element=editor.selection;
        var txt=element.getContent(),audiourl='';
        data = getData(editor.selection.getNode());
        var obj=data["data-mce-object"];       
        win = editor.windowManager.open({
            title: 'Selected Audio',
            url: rutabase+'/biblioteca/?plt=tinymce&type=audio',
            width: 400,
            height: 600
        //    onclose:cerrarpanel
        },{
            editor:editor,
            obj:obj
        });

    }


    function htmlToData(html) {
        var data = {};        
        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',
            start: function(name, attrs) {                             
                data = tinymce.extend(attrs.map, data);              
            }
        }).parse(html);
        return data;
    }
    function getData(element) {
        if (element.getAttribute('data-mce-object')){            
            var $=tinymce.dom.DomQuery;
            var jedit=$(tinymce.activeEditor.getBody());
            var el = $(element);
            var tipo=el.attr('data-mce-object');
            var img=$(jedit).find('img');
            $(img).removeClass('mce-imageinchanged'+editor.id+' mce-inputinchanged'+editor.id+' mce-audioinchanged'+editor.id+' mce-videoinchanged'+editor.id);            
            el.addClass('mce-'+tipo+'inchanged'+editor.id);
            return htmlToData(editor.serializer.serialize(element, {selection: true}));
        }
        return {};
    }

    editor.on('ResolveName', function(e) {
        var name;
        if (e.target.nodeType == 1 && (name = e.target.getAttribute("data-mce-object"))) {
            e.name = name;
        }
    });

    editor.on('preInit', function() {
        editor.parser.addNodeFilter('div', function(nodes, name) {
            var i = nodes.length, ai, node, placeHolder, attrName, attrValue, attribs, innerHtml;  
            while (i--) {
                node = nodes[i];
                if (!node.parent) {
                    continue;
                }
                attribs = node.attributes;
                if(attribs==undefined) continue;
                placeHolder = new tinymce.html.Node('img', 1);
                placeHolder.shortEnded = true;                
                ai = attribs.length;
                var ismedia=false;
                var media=null;
                while (ai--){
                    attrName = attribs[ai].name;
                    attrValue = attribs[ai].value;                    
                    placeHolder.attr(attrName, attrValue); 
                    if((attrValue=='audio'||attrValue=='video') && ismedia==false){
                        ismedia=true;
                        media=attrValue;
                    }
                }
                if(ismedia==false)continue;
                else{
                   _media=node.firstChild;
                   if(_media!=undefined){
                    _mediaattr=_media.attributes;
                    a2 = _mediaattr.length;
                    while (a2--){
                        atName = _mediaattr[a2].name;
                        atValue = _mediaattr[a2].value;
                        if(atName==='src')atName='data-src';
                        placeHolder.attr(atName, atValue); 
                    }
                   }

                }
                placeHolder.attr({
                    width:(attrValue=='video'?'200':"100"),
                    height:(attrValue=='video'?'200':"25"),
                    //style: node.attr('style'),
                    "data-mce-object":media,
                    class:' mce-object-'+media
                });
                node.replace(placeHolder);
            }
        }); 
    });

    editor.addButton('chingoaudio', {
        tooltip: 'Insert/edit Audio',
        icon: 'fa fa-music',
        onclick: showDialog,
        stateSelector: ['img[data-mce-object=audio]']
    });
    editor.addMenuItem('chingoaudio', {
        icon: 'fa fa-music',
        text: 'Insert/edit Audio',
        onclick: showDialog,
        context: 'insert',
        prependToContext: true
    });
    editor.addCommand('mceAudio', showDialog);
    this.showDialog = showDialog;
});