var getAlumnos = function() {
	var proyecto_slug = $('#opcEmpresa').val() || 0;
	$.ajax({
		url: _sysUrlBase_ + '/personal/jxBuscar',
		type: 'GET',
		dataType: 'json',
		data: {"slug": proyecto_slug},
		beforeSend: function() {
			$('#opcAlumno').html('<option value="">- '+MSJES_PHP.select_student+' -</option>');
			$('#opcExamen').html('<option value="">- '+MSJES_PHP.select_exam+' -</option>');
			$('#ifrReporte').attr('src','');
		}
	}).done(function(resp) {
		if(resp.code=='ok') {
			var strOpt = '';
			$.each(resp.data, function(i, alum) {
				strOpt += '<option value="'+alum.identificador+'">'+alum.ape_paterno+' '+alum.ape_materno+' '+alum.nombre+'</option>';
			});
			$('#opcAlumno').append(strOpt);
		} else {
			mostrar_notificacion(MSJES_PHP.attention, data.msj, 'error');
		}
	}).fail(function(err) {
		console.log("error : ", err);
	}).always(function() {});	
};

var getExamenesAlumno = function() {
	var identificador = $('#opcAlumno').val() || 0;
	$.ajax({
		url: _sysUrlBase_ + '/examenes/jxExamRendidos',
		type: 'GET',
		dataType: 'json',
		data: {"identificador": identificador},
		beforeSend: function() {
			$('#opcExamen').html('<option value="">- '+MSJES_PHP.select_exam+' -</option>');
			$('#ifrReporte').attr('src','');
		}
	}).done(function(resp) {
		if(resp.code=='ok') {
			var strOpt = '';
			$.each(resp.data, function(i, exam) {
				strOpt += '<option value="'+exam.idexamen+'">'+exam.titulo+'</option>';
			});
			$('#opcExamen').append(strOpt);
		} else {
			mostrar_notificacion(MSJES_PHP.attention, data.msj, 'error')
		}
	}).fail(function(err) {
		console.log("error : ", err);
	}).always(function() {});	
};

$(document).ready(function() {
	$('#opcEmpresa').change(getAlumnos);
	$('#opcAlumno').change(getExamenesAlumno);
	$('#opcExamen').change(function(){$('#ifrReporte').attr('src','');});
	$('.btn-filtrar').click(function(e) {
		var proyecto_slug = $('#opcEmpresa').val();
		var idalumno = $('#opcAlumno').val();
		var idexamen = $('#opcExamen').val();

		var srcIframe = _sysUrlBase_ + '/reporte/resultado_alumno/?slug='+proyecto_slug+'&identificador='+idalumno+'&idexamen='+idexamen;

		$('#ifrReporte').attr('src', srcIframe);
		return false;
	});

	cargarMensajesPHP('#msjes_idioma');
});