var existeSlug = function() {
	var slug = $('#hSlugProyecto').val();
	if( slug!==""){
		getReport(slug);
	}
};

var getReport =  function(slug=null) {
	if(slug==null){ return false; }

	$.ajax({
		url: _sysUrlBase_ + '/reporte/x_resultadosxproyecto',
		type: 'GET',
		dataType: 'json',
		data: {"slug": slug},
	}).done(function(resp) {
		if(resp.code=="ok") {
			$("#resultados").html('');
			$.each(resp.data, function(i, alum) {
				var nombre_alumno = alum.ape_paterno + ' ' + alum.ape_materno + ', ' + alum.nombre;
				var dni = alum.dni;
				var arrData = [];
				$.each(alum.resultados, function(i, result) {
					var nodo = {
						"name" : result.exam_titulo,
						"y" : (parseFloat(result.result_puntaje)*100)/parseFloat(result.exam_calificacion_total),
						"intento": result.result_intento,
					};
					arrData.push(nodo);
				});
				var html = '<div class="col-xs-12 resultado_alumno">';
				html += '<h3>'+nombre_alumno+'</h3>'
				html += '<div id="chart_'+dni+'"></div>'
				html += '</div>';
				$("#resultados").append(html);

				// Create the chart
				Highcharts.chart('chart_'+dni, {
					chart: {
						type: 'column'
					},
					title: {
						text: 'Resultado de los examenes rendidos'
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						type: 'category'
					},
					yAxis: {
						title: {
							text: 'Score (base 100%)'
						}

					},
					legend: {
						enabled: false
					},
					plotOptions: {
						series: {
							borderWidth: 0,
							dataLabels: {
								enabled: true,
								format: '{point.y:.1f}%'
							}
						}
					},

					tooltip: {
						headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
						pointFormat: '<span style="color:{point.color}">{point.name} ({point.intento}° intento)</span>: <b>{point.y:.2f}%</b> of total<br/>'
					},

					series: [{
						name: 'Result',
						colorByPoint: true,
						data: arrData
					}],
					/*drilldown: {
						series: [{
							name: 'Microsoft Internet Explorer',
							id: 'Microsoft Internet Explorer',
							data: [
							[
							'v11.0',
							24.13
							],
							[
							'v8.0',
							17.2
							],
							[
							'v9.0',
							8.11
							],
							[
							'v10.0',
							5.33
							],
							[
							'v6.0',
							1.06
							],
							[
							'v7.0',
							0.5
							]
							]
						}, {
							name: 'Chrome',
							id: 'Chrome',
							data: [
							[
							'v40.0',
							5
							],
							[
							'v41.0',
							4.32
							],
							[
							'v42.0',
							3.68
							],
							[
							'v39.0',
							2.96
							],
							[
							'v36.0',
							2.53
							],
							[
							'v43.0',
							1.45
							],
							[
							'v31.0',
							1.24
							],
							[
							'v35.0',
							0.85
							],
							[
							'v38.0',
							0.6
							],
							[
							'v32.0',
							0.55
							],
							[
							'v37.0',
							0.38
							],
							[
							'v33.0',
							0.19
							],
							[
							'v34.0',
							0.14
							],
							[
							'v30.0',
							0.14
							]
							]
						}, {
							name: 'Firefox',
							id: 'Firefox',
							data: [
							[
							'v35',
							2.76
							],
							[
							'v36',
							2.32
							],
							[
							'v37',
							2.31
							],
							[
							'v34',
							1.27
							],
							[
							'v38',
							1.02
							],
							[
							'v31',
							0.33
							],
							[
							'v33',
							0.22
							],
							[
							'v32',
							0.15
							]
							]
						}, {
							name: 'Safari',
							id: 'Safari',
							data: [
							[
							'v8.0',
							2.56
							],
							[
							'v7.1',
							0.77
							],
							[
							'v5.1',
							0.42
							],
							[
							'v5.0',
							0.3
							],
							[
							'v6.1',
							0.29
							],
							[
							'v7.0',
							0.26
							],
							[
							'v6.2',
							0.17
							]
							]
						}, {
							name: 'Opera',
							id: 'Opera',
							data: [
							[
							'v12.x',
							0.34
							],
							[
							'v28',
							0.24
							],
							[
							'v27',
							0.17
							],
							[
							'v29',
							0.16
							]
							]
						}]
					}*/
				});
			});
		} else {
			mostrar_notificacion('Oups!',resp.msje, 'error');
		}
	}).fail(function(err) {
		console.log("error : " ,err);
	});
	
};

$(document).ready(function() {
	$('#opcEmpresa').change(function(e) {
		var slug = $(this).val();
		$('#hSlugProyecto').val(slug);

	});

	$('.btn-filtrar').click(function(e) {
		existeSlug();
	})

	existeSlug();
});