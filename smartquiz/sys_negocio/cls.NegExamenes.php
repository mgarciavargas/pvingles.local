<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-03-2017
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatExamenes', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegExamenes 
{
	protected $idexamen;
	protected $titulo;
	protected $descripcion;
	protected $portada;
	protected $fuente;
	protected $fuentesize;
	protected $grupo;
	protected $aleatorio;
	protected $calificacion_por;
	protected $calificacion_en;
	protected $calificacion_total;
	protected $calificacion_min;
	protected $tiempo_por;
	protected $tiempo_total;
	protected $tipo;
	protected $estado;
	protected $idpersonal;
	protected $fecharegistro;
	protected $nintento;
	protected $calificacion;
	protected $habilidades_todas;
	protected $origen_habilidades;
	protected $fuente_externa;
	protected $dificultad_promedio;
	protected $idproyecto;
	protected $tiene_certificado;
	protected $nombre_certificado;
	
	protected $dataExamenes;
	protected $oDatExamenes;	

	public function __construct()
	{
		$this->oDatExamenes = new DatExamenes;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatExamenes->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function setUsarBD($bd_usar)
	{
		try {
			$this->oDatExamenes->setUsarBD($bd_usar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getUsarBD()
	{
		try {
			return $this->oDatExamenes->getUsarBD();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatExamenes->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatExamenes->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarRendidos($filtros = array())
	{
		try {
			return $this->oDatExamenes->buscarRendidos($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatExamenes->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatExamenes->get($this->idexamen);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			//$this->oDatExamenes->iniciarTransaccion('neg_i_Examenes');
			$this->idexamen = $this->oDatExamenes->insertar($this->titulo,$this->descripcion,$this->portada,$this->fuente,$this->fuentesize,$this->aleatorio,$this->calificacion_por,$this->calificacion_en,$this->calificacion_total,$this->calificacion_min,$this->tiempo_por,$this->tiempo_total,$this->tipo,$this->estado,$this->idpersonal,$this->nintento,$this->calificacion,$this->habilidades_todas,$this->origen_habilidades,$this->fuente_externa,$this->dificultad_promedio,$this->idproyecto, $this->tiene_certificado, $this->nombre_certificado);
			//$this->oDatExamenes->terminarTransaccion('neg_i_Examenes');	
			return $this->idexamen;
		} catch(Exception $e) {	
		   //$this->oDatExamenes->cancelarTransaccion('neg_i_Examenes');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {

			return $this->oDatExamenes->actualizar($this->idexamen, $this->titulo,$this->descripcion,$this->portada,$this->fuente,$this->fuentesize,$this->aleatorio,$this->calificacion_por,$this->calificacion_en,$this->calificacion_total,$this->calificacion_min,$this->tiempo_por,$this->tiempo_total,$this->tipo,$this->estado,$this->idpersonal,$this->nintento,$this->calificacion,$this->habilidades_todas,$this->origen_habilidades,$this->fuente_externa,$this->dificultad_promedio,$this->idproyecto, $this->tiene_certificado, $this->nombre_certificado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			return $this->oDatExamenes->eliminar($this->idexamen);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdexamen($pk){
		try {
			$this->dataExamenes = $this->oDatExamenes->get($pk);
			if(empty($this->dataExamenes)) {
				throw new Exception(JrTexto::_("Examenes").' '.JrTexto::_("not registered"));
			}
			$this->idexamen = $this->dataExamenes["idexamen"];
			$this->titulo = $this->dataExamenes["titulo"];
			$this->descripcion = $this->dataExamenes["descripcion"];
			$this->portada = $this->dataExamenes["portada"];
			$this->fuente = $this->dataExamenes["fuente"];
			$this->fuentesize = $this->dataExamenes["fuentesize"];
			$this->aleatorio = $this->dataExamenes["aleatorio"];
			$this->calificacion_por = $this->dataExamenes["calificacion_por"];
			$this->calificacion_en = $this->dataExamenes["calificacion_en"];
			$this->calificacion_total = $this->dataExamenes["calificacion_total"];
			$this->calificacion_min = $this->dataExamenes["calificacion_min"];
			$this->tiempo_por = $this->dataExamenes["tiempo_por"];
			$this->tiempo_total = $this->dataExamenes["tiempo_total"];
			$this->tipo = $this->dataExamenes["tipo"];
			$this->estado = $this->dataExamenes["estado"];
			$this->idpersonal = $this->dataExamenes["idpersonal"];
			$this->fecharegistro = $this->dataExamenes["fecharegistro"];
			$this->nintento = $this->dataExamenes["nintento"];
			$this->calificacion = $this->dataExamenes["calificacion"];
			$this->habilidades_todas = $this->dataExamenes["habilidades_todas"];
			$this->origen_habilidades = $this->dataExamenes["origen_habilidades"];
			$this->fuente_externa = $this->dataExamenes["fuente_externa"];
			$this->dificultad_promedio = $this->dataExamenes["dificultad_promedio"];
			$this->idproyecto = $this->dataExamenes["idproyecto"];
			$this->tiene_certificado = $this->dataExamenes["tiene_certificado"];
			$this->nombre_certificado = $this->dataExamenes["nombre_certificado"];
			//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			$this->dataExamenes = $this->oDatExamenes->get($pk);
			if(empty($this->dataExamenes)) {
				throw new Exception(JrTexto::_("Examenes").' '.JrTexto::_("not registered"));
			}
			return $this->oDatExamenes->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}	
}