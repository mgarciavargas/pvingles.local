<?php
/**
 * @autor		Abel Chingo Tello , ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersonal', RUTA_BASE, 'sys_datos');
/*JrCargador::clase('sys_datos::DatAlumno', RUTA_BASE, 'sys_datos');*/
JrCargador::clase('sys_datos::DatRol', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatRol_personal', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatProyecto', RUTA_BASE, 'sys_datos');
JrCargador::clase('jrAdwen::JrSession');
class NegSesion
{
	/*protected $oDatAlumno;	*/
	protected $oDatPersonal;
	protected $oDatRol;
	protected $oDatRol_personal;
	public function __construct()
	{
		$this->oDatPersonal = new DatPersonal;
		$this->oDatRol_personal = new DatRol_personal;
		$this->oDatProyecto = new DatProyecto;
		/*$this->oDatAlumno = new DatAlumno;	*/	
	}	
	public static function existeSesion()
	{
		$sesion = JrSession::getInstancia();		
		$idusuario = $sesion->get('idusuario', false, '__smartquiz');
		if(false === $idusuario) {
			return false;
		} else {
			return true;
		}
	}	
	public static function getUsuario()
	{
		$sesion = JrSession::getInstancia();
		$idusuario = $sesion->get('idusuario', false, '__smartquiz');
		if(empty($idusuario)) {
			throw new Exception(JrTexto::_('Please sign in.'));
		}		
		return array('idusuario' => $idusuario
					, 'dni' => $sesion->get('dni', false, '__smartquiz')
					, 'email' => $sesion->get('email', false, '__smartquiz')
					, 'proyecto' => $sesion->get('proyecto', false, '__smartquiz')
					, 'identificador' => $sesion->get('identificador', false, '__smartquiz')
					, 'rol' => $sesion->get('rol', false, '__smartquiz')
					, 'roles_all' => $sesion->get('roles_all', false, '__smartquiz')
					, 'nombre_full' => $sesion->get('nombre_full', false, '__smartquiz')
					, 'imagen' => $sesion->get('imagen', false, '__smartquiz')
					, 'token' => $sesion->get('token', false, '__smartquiz')
					, 'callback' => $sesion->get('callback', false, '__smartquiz')
					, 'otrosParams' => $sesion->get('otrosParams', false, '__smartquiz')
					/*, 'idHistorialSesion' => $sesion->get('idHistorialSesion', false, '__smartquiz')*/
				);
	}	
	public function salir()
	{
		return JrSession::limpiarEspacio('__smartquiz');
	}
	
	public function ingresar($usu, $clave)
	{//04.04.16
		try {
			if(empty($usu) || empty($clave)) { return false; }
			$arrRoles = array();
			$rol_actual = '';

			$usuario = $this->oDatPersonal->getxCredencial($usu, $clave);
			if(empty($usuario)) {
				return false;
			}

			$arrRoles = $this->oDatRol_personal->buscar(array('idpersonal'=>$usuario['dni']));
			$rol_actual = @$arrRoles[0]['rol'];

			$proyecto = $this->oDatProyecto->get($usuario['idproyecto']);

			$sesion = JrSession::getInstancia();
			$sesion->set('idusuario', $usuario['dni'], '__smartquiz');
			$sesion->set('dni', $usuario['dni'], '__smartquiz');
			$sesion->set('email', @$usuario['email'], '__smartquiz');
			$sesion->set('proyecto', $proyecto, '__smartquiz');
			$sesion->set('identificador', $usuario['identificador'], '__smartquiz');
			$sesion->set('rol', $rol_actual , '__smartquiz');
			$sesion->set('roles_all', $arrRoles , '__smartquiz');
			$sesion->set('nombre_full', $usuario['ape_paterno']." ".$usuario['ape_materno'].", ".$usuario['nombre'], '__smartquiz');
			$sesion->set('imagen', trim(@$usuario['foto']), '__smartquiz');
			$sesion->set('token',md5(@$usuario['usuario'].' '.@$usuario['clave']), '__smartquiz');

			return true;
		} catch(Exception $e) {
			return false;
		}
	}
	
	public static function get($prop, $espacio = '__smartquiz')
	{
		$sesion = JrSession::getInstancia();		
		return $sesion->get($prop, null, $espacio);
	}
	
	public static function set_($prop, $valor)
	{
		$sesion = JrSession::getInstancia();		
		$idusuario = $sesion->get('idusuario', false, '__smartquiz');
		if(empty($idusuario)) {
			throw new Exception(JrTexto::_('Please sign in.'));
		}		
		return $sesion->set($prop, $valor, '__smartquiz');
	}	
	
	public static function set($prop, $valor, $espacio = '__smartquiz')
	{
        
		$sesion = JrSession::getInstancia();
		return $sesion->set($prop, $valor, $espacio);
	}	

	public static function tiene_acceso($menu, $accion)
	{
		
		if(!NegSesion::existeSesion()) {
			return false;
		}		
		if('superadmin' == NegSesion::get('rol')) {
			return true;
		}
		$oDatRol=new DatRol;
		$existe_privilegio = $oDatRol->existe_permiso(NegSesion::get('rol'),$menu, $accion);
		if(!$existe_privilegio){
			return false;
		}		
		return true;
	}

	public function ingresarxToken($token='', $tipoAcceso=null)
	{
		try {
			if(empty($token)){ return false; }
			$arrRoles = array();
			$esNuevo = false;
			$rol_actual = '';

			$usuario = $this->oDatPersonal->getxToken($token);
			if(empty($usuario)) {
				return false;
			}

			$arrRoles = $this->oDatRol_personal->buscar(array('idpersonal'=>$usuario['dni']));
			if($tipoAcceso=='admin'){ $arrRolBuscar =  array('1','2'); }
			else{ $arrRolBuscar =  array('3'); }
			$rolCreadorExams = $this->buscarRol(@$arrRoles, $arrRolBuscar);
			if(!empty($rolCreadorExams)){ $rol_actual=$rolCreadorExams['rol']; }
			else{ $rol_actual = @$arrRoles[0]['rol']; }
			$proyecto = $this->oDatProyecto->getXSlug($project_slug);

			$sesion = JrSession::getInstancia();
			$sesion->set('idusuario', $usuario['dni'], '__smartquiz');
			$sesion->set('dni', $usuario['dni'], '__smartquiz');
			$sesion->set('email', $usuario['email'], '__smartquiz');
			$sesion->set('proyecto', $proyecto, '__smartquiz');
			$sesion->set('identificador', $usuario['identificador'], '__smartquiz');
			$sesion->set('rol', $rol_actual , '__smartquiz');
			$sesion->set('roles_all', $arrRoles , '__smartquiz');
			$sesion->set('nombre_full', $usuario['ape_paterno']." ".$usuario['ape_materno'].", ".$usuario['nombre'], '__smartquiz');
			$sesion->set('imagen', trim(@$usuario['foto']), '__smartquiz');
			$sesion->set('token', $token, '__smartquiz');

			return true;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function ingresarxIdentifUser_Proyecto($params = array())
	{
		try {
			@extract($params);

			if( empty($identifier) && empty($project_slug) ){ return false; }
			$arrRoles = array();
			$esNuevo = false;
			$rol_actual = '';
			$proyecto = $this->oDatProyecto->getXSlug($project_slug);
			if(empty($proyecto)) {
				$proyecto = $this->registrarProyecto(array("proyecto"=>$project_slug, "lang"=>$lang));
			}
			$usuario = $this->oDatPersonal->getxIdentificadorProyecto($identifier,$project_slug);
			if(empty($usuario)){
				$usuario = $this->registrarUsuario(array('tipo'=>$tipoAcceso, 'usuario'=>$user, 'clave'=>$password, 'identificador'=>$identifier, 'proyecto'=>$project_slug));
				$esNuevo = true;
			}
			if(!$esNuevo && !empty($user)){ $this->oDatPersonal->set($usuario['dni'], 'usuario', $user); }
			if(!$esNuevo && !empty($password)){ $this->oDatPersonal->set($usuario['dni'], 'clave', $password); }
			
			$arrRoles = $this->oDatRol_personal->buscar(array('idpersonal'=>$usuario['dni']));
			if($tipoAcceso=='admin'){ $arrRolBuscar =  array('1','2'); }
			else{ $arrRolBuscar =  array('3'); }
			$rolCreadorExams = $this->buscarRol(@$arrRoles, $arrRolBuscar);
			if(!empty($rolCreadorExams)){ $rol_actual=$rolCreadorExams['rol']; }
			else{ $rol_actual = @$arrRoles[0]['rol']; }
			

			$sesion = JrSession::getInstancia();
			$sesion->set('idusuario', $usuario['dni'], '__smartquiz');
			$sesion->set('dni', $usuario['dni'], '__smartquiz');
			$sesion->set('email', $usuario['email'], '__smartquiz');
			$sesion->set('proyecto', $proyecto, '__smartquiz');
			$sesion->set('identificador', $identifier, '__smartquiz');
			$sesion->set('rol', $rol_actual , '__smartquiz');
			$sesion->set('roles_all', $arrRoles , '__smartquiz');
			$sesion->set('nombre_full', $usuario['ape_paterno']." ".$usuario['ape_materno'].", ".$usuario['nombre'], '__smartquiz');
			$sesion->set('imagen', trim(@$usuario['foto']), '__smartquiz');
			$sesion->set('token', $usuario['token'], '__smartquiz');
			$sesion->set('callback', @$callback, '__smartquiz');
			$sesion->set('otrosParams', @$otrosParams, '__smartquiz');

			return true;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function registrarUsuario($arrParams=null)
	{
		try {
			if(empty($arrParams)){ throw new Exception(JrTexto::_('No se pudo obtener datos nuevos de usuario')); }
			if( empty(@$arrParams['identificador']) && empty($arrParams['proyecto']) && empty($arrParams['token']) ){ 
				throw new Exception(JrTexto::_('Credencial(es) no reconocida(s)')); 
			}
			$id = $p = $u = $c = $t = ''; 
			if(!empty(@$arrParams['identificador'])){ $id = $arrParams['identificador']; }
			if(!empty(@$arrParams['proyecto'])){ 
				$proy = $this->oDatProyecto->getXSlug($arrParams['proyecto']);
				$p = $proy['idproyecto']; 
			}
			if(!empty(@$arrParams['usuario'])){ $u = $arrParams['usuario']; }else{ $u=$arrParams['identificador']; }
			if(!empty(@$arrParams['clave'])){ $c = $arrParams['clave']; } else { $c = 'alumno'; }
			if(!empty(@$arrParams['token'])){ $t = $arrParams['token']; } else { $t = $this->generarToken($arrParams); }

			$arrRol = array('3'); /* [3]: Rol alumno. Es el rol que como minimo debe tener */
			if(@$arrParams['tipo'] == 'admin'){
				$arrRol[] = '2'; // en DB  [2]:docente
			}
			$idUsuario = $this->oDatPersonal->insertar(' desde ',$_SERVER["HTTP_HOST"],' Usuario ',null,null,null,null,null,null,null,null,null,$p,'_self_',$u,$c,$t,'','1',$id,'ES');
			foreach ($arrRol as $idRol) {
				$idRolPersona = $this->oDatRol_personal->insertar($idRol, $idUsuario);
			}
			$usuario = $this->oDatPersonal->get($idUsuario);

			return $usuario;
		} catch (Exception $e) {
			throw new Exception(JrTexto::_('Something went wrong. '. $e->getMessage() ));
		}
	}

	private function registrarProyecto($arrParams=null)
	{
		try {
			if(empty($arrParams)){ throw new Exception(JrTexto::_('No se pudo obtener datos nuevo proyecto')); }
			if( empty(@$arrParams['proyecto']) && empty($arrParams['lang']) ){ 
				throw new Exception(JrTexto::_('Falta parametro para registrar Proyecto')); 
			}

			$idNewProyecto = $this->oDatProyecto->insertar(
				$arrParams["proyecto"].' '.date('Y'),
				'Creado vía URL y datos mediante parametros GET',
				$arrParams["proyecto"],
				date('Y-m-d H:i:s'),
				date('Y-m-d H:i:s',strtotime(date("Y-m-d H:i:s", mktime()) . " + 365 day")),
				1,
				$arrParams["lang"]
			);
			$proyecto = $this->oDatProyecto->get($idNewProyecto);

			return $proyecto;
		} catch (Exception $e) {
			throw new Exception(JrTexto::_('Something went wrong. '. $e->getMessage() ));
		}
	}

	private function generarToken($params=array())
	{
		if(empty($params)){ return null; }
		$token =  md5( @$params['identificador'].' '.@$params['proyecto_slug'] );
		return $token;
	}

	private function buscarRol($arrTodosRoles, $arrRolesBuscar)
	{
		if(empty($arrTodosRoles) && empty($arrRolesBuscar)){ return null; }
		foreach ($arrTodosRoles as $r) {
			if( in_array($r['idrol'], $arrRolesBuscar) ){
				return $r;
			}
		}
	}
}