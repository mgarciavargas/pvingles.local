<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		09-06-2016
 * @copyright	Copyright (C) 09-06-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatUsuario', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegUsuario 
{
	protected $idusuario;
	protected $nombres;
	protected $apellidos;
	protected $clave;
	protected $estado;
	protected $regusuario;
	protected $regfecha;
	protected $codigo=null;
	protected $idempresa;
	protected $usuario;
	
	public $dataUsuario;
	protected $oDatUsuario;	

	public function __construct()
	{
		$this->oDatUsuario = new DatUsuario;
		
	}
	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}

	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatUsuario->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatUsuario->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatUsuario->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatUsuario->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	
	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('usuario', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatUsuario->iniciarTransaccion('neg_i_Usuario');
			$this->idusuario = $this->oDatUsuario->insertar($this->apellidos,$this->nombres,md5($this->clave),$this->estado,$this->regusuario,$this->regfecha,$this->codigo,$this->idempresa, $this->usuario);
			$this->oDatUsuario->terminarTransaccion('neg_i_Usuario');	
			return $this->idusuario;
		} catch(Exception $e) {	
		   $this->oDatUsuario->cancelarTransaccion('neg_i_Usuario');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('usuario', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatUsuario->iniciarTransaccion('neg_e_Usuario');
			$this->oDatUsuario->actualizar($this->idusuario,$this->apellidos,$this->nombres,md5($this->clave),$this->estado,$this->regusuario,$this->regfecha,$this->codigo,$this->idempresa);

			if(!empty($this->clave)) {
				$this->oDatUsuario->set($this->idusuario,'clave', md5($this->clave));
			}
			$this->oDatUsuario->terminarTransaccion('neg_e_Usuario');
			return $this->idusuario;
		} catch(Exception $e) {
		    $this->oDatUsuario->cancelarTransaccion('neg_e_Usuario');
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($pk)
	{
		try {
			if(!NegSesion::tiene_acceso('Usuario', 'eliminar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatUsuario->iniciarTransaccion('neg_d_Usuario');
			$data=$this->oDatUsuario->eliminar($pk);
			$this->oDatUsuario->terminarTransaccion('neg_d_Usuario');	
			return $data;
		} catch(Exception $e) {
			 $this->oDatUsuario->cancelarTransaccion('neg_d_Usuario');
			throw new Exception($e->getMessage());
		}
	}

	public function cambiarClave($clave, $re_clave)
	{
		try {
			if(empty($clave)) {
				throw new Exception(JrTexto::_('Enter password'));
			}
			
			if($clave != $re_clave) {
				throw new Exception(JrTexto::_('Passwords do not match'));
			}
			
			$this->oDatUsuario->setClave($this->idusuario, $clave);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function procesarSolicitudCambioClave($usuario)
	{
		try {
			
			$usuario = $this->oDatUsuario->getxusuarioapellidos($usuario);			
			if(!empty($usuario)) {
				$this->token = JrSession::crearRandom();				
				$this->oDatUsuario->set($usuario['idusuario'],'token',$this->token);				
				return $usuario;
			}			
			return null;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdusuario($pk)
	{
		try {
			$this->dataUsuario = $this->oDatUsuario->get($pk);
			if(empty($this->dataUsuario)) {
				throw new Exception(JrTexto::_("Usuario").' '.JrTexto::_("not registered"));
			}
			$this->idusuario = $this->dataUsuario["idusuario"];
			$this->apellidos = $this->dataUsuario["apellidos"];
			$this->nombres = $this->dataUsuario["nombres"];
			$this->clave = $this->dataUsuario["clave"];
			$this->estado = $this->dataUsuario["estado"];
			$this->regusuario = $this->dataUsuario["regusuario"];
			$this->regfecha = $this->dataUsuario["regfecha"];
			$this->codigo = $this->dataUsuario["codigo"];
			$this->idempresa = $this->dataUsuario["idempresa"];
			$this->usuario = $this->dataUsuario["idusuario"];
			
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}
	
	/*public function setApellidos($apellidos)
	{
		try {
			$this->apellidos = NegTools::validar('todo', $apellidos, false, JrTexto::_('Enter an apellidos'), array('longmax' => 30));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}

	public function setNombres($nombres)
	{
		try {
			$this->nombres = NegTools::validar('todo', $nombres, false, JrTexto::_('Enter Name'), array('longmax' => 30));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}

	public function setClave($clave)
	{
		try {
			if(empty($clave)) {
				throw new Exception(JrTexto::_('Enter password'));
			}
			$this->clave = $clave;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}

	public function setEstado($estado)
	{
		try {
			if(empty($estado)) {
				throw new Exception(JrTexto::_('Select a estado'));
			}
			$this->estado = $estado;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}*/

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('usuario', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataUsuario = $this->oDatUsuario->get($pk);
			if(empty($this->dataUsuario)) {
				throw new Exception(JrTexto::_("User").' '.JrTexto::_("not registered"));
			}

			$this->oDatUsuario->iniciarTransaccion('neg_uc_Usuario');
			$data=$this->oDatUsuario->set($pk, $propiedad, $valor);
			$this->oDatUsuario->terminarTransaccion('neg_uc_Usuario');	
			return $data;
		} catch(Exception $e) {
			 $this->oDatUsuario->cancelarTransaccion('neg_uc_Usuario');
			throw new Exception($e->getMessage());
		}

	}
}