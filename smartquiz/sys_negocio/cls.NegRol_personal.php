<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		29-09-2017
 * @copyright	Copyright (C) 29-09-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRol_personal', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegRol_personal 
{
	protected $iddetalle;
	protected $idrol;
	protected $idpersonal;
	
	protected $dataRol_personal;
	protected $oDatRol_personal;	

	public function __construct()
	{
		$this->oDatRol_personal = new DatRol_personal;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRol_personal->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatRol_personal->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRol_personal->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatRol_personal->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatRol_personal->get($this->iddetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rol_personal', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			#$this->oDatRol_personal->iniciarTransaccion('neg_i_Rol_personal');
			$this->iddetalle = $this->oDatRol_personal->insertar($this->idrol,$this->idpersonal);
			#$this->oDatRol_personal->terminarTransaccion('neg_i_Rol_personal');	
			return $this->iddetalle;
		} catch(Exception $e) {	
		    #$this->oDatRol_personal->cancelarTransaccion('neg_i_Rol_personal');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rol_personal', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatRol_personal->actualizar($this->iddetalle,$this->idrol,$this->idpersonal);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Rol_personal', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatRol_personal->eliminar($this->iddetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIddetalle($pk){
		try {
			$this->dataRol_personal = $this->oDatRol_personal->get($pk);
			if(empty($this->dataRol_personal)) {
				throw new Exception(JrTexto::_("Rol_personal").' '.JrTexto::_("not registered"));
			}
			$this->iddetalle = $this->dataRol_personal["iddetalle"];
			$this->idrol = $this->dataRol_personal["idrol"];
			$this->idpersonal = $this->dataRol_personal["idpersonal"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('rol_personal', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataRol_personal = $this->oDatRol_personal->get($pk);
			if(empty($this->dataRol_personal)) {
				throw new Exception(JrTexto::_("Rol_personal").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRol_personal->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}