<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('jrAdwen::JrModulo', RUTA_LIBS, 'jrAdwen::');
JrCargador::clase('modulos::nav_docente::NavDocente', RUTA_SITIO, 'modulos::nav_docente');
$oMod = new NavDocente;
echo $oMod->mostrar();