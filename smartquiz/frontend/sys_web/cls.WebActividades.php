<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-11-2016 
 * @copyright	Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividades', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
class WebActividades extends JrWeb
{
	private $oNegActividades;
	private $oNegNiveles;
	private $oNegMetodologia;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegActividades = new NegActividades;
		$this->oNegNiveles = new NegNiveles;
		$this->oNegMetodologia = new NegMetodologia_habilidad;
	}

	public function defecto(){
		
		
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Actividades', 'listar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegActividades->buscar();

			
			/*$this->fknivel=$this->oNegActividades->listarles();
			$this->fkunidad=$this->oNegActividades->listarles();
			$this->fksesion=$this->oNegActividades->listarles();*/
			//$this->fkmetodologia=$this->oNegActividades->listardologia_habilidad();
			//$this->fkhabilidad=$this->oNegActividades->listardologia_habilidad();			
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Actividades'), true);
			$this->esquema = 'actividades-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;	
			
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:$_idnivel;
			
			$this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_GET["txtUnidad"])?$_GET["txtUnidad"]:($_idunidad);
			
			$this->sesiones=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
            $_idsesion=!empty($this->sesiones[0]["idnivel"])?$this->sesiones[0]["idnivel"]:0;
            $this->idsesion=!empty($_GET["txtsesion"])?$_GET["txtsesion"]:($_idsesion);
			
			$this->metodologias=$this->oNegMetodologia->buscar(array('tipo'=>'M'));
            /*$_idmeto=!empty($this->metodologias[0]["idnivel"])?$this->metodologias[0]["idnivel"]:0;
            $this->idmeto=!empty($_GET["txtmetodologia"])?$_GET["txtmetodologia"]:($_idmeto);*/
			
			$this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
			
			
			
					
			if(!NegSesion::tiene_acceso('Actividades', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			
			$this->frmaccion='New';
			$this->documento->setTitulo(JrTexto::_('Actividades').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:$_idnivel;
			
			$this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_GET["txtUnidad"])?$_GET["txtUnidad"]:($_idunidad);
			
			$this->sesiones=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
            $_idsesion=!empty($this->sesiones[0]["idnivel"])?$this->sesiones[0]["idnivel"]:0;
            $this->idsesion=!empty($_GET["txtsesion"])?$_GET["txtsesion"]:($_idsesion);
			
			$this->metodologias=$this->oNegMetodologia->buscar(array('tipo'=>'M'));
			
			$this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
			$_idhabi=!empty($this->habilidades[0]["idnivel"])?$this->habilidades[0]["idnivel"]:0;
            $this->idhabi=!empty($_GET["txtHabilidad"])?$_GET["txtHabilidad"]:($_idhabi);
			
			if(!NegSesion::tiene_acceso('Actividades', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegActividades->idactividad = @$_GET['id'];
			$this->datos = $this->oNegActividades->dataActividades;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Actividades').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegActividades->idactividad = @$_GET['id'];
			$this->datos = $this->oNegActividades->dataActividades;
			
			$this->fknivel=$this->oNegActividades->listarles();
			$this->fkunidad=$this->oNegActividades->listarles();
			$this->fksesion=$this->oNegActividades->listarles();
			$this->fkmetodologia=$this->oNegActividades->listardologia_habilidad();
			$this->fkhabilidad=$this->oNegActividades->listardologia_habilidad();			
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/cropper.min.js");
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/main3.js");
							$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Actividades').' /'.JrTexto::_('see'), true);
			$this->esquema = 'actividades-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			/*$this->fknivel=$this->oNegActividades->listarles();
			$this->fkunidad=$this->oNegActividades->listarles();
			$this->fksesion=$this->oNegActividades->listarles();
			$this->fkmetodologia=$this->oNegActividades->listardologia_habilidad();
			$this->fkhabilidad=$this->oNegActividades->listardologia_habilidad();*/
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'actividades-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}		
		
	}
	
	
	// ========================== Funciones xajax ========================== //
	public function xSaveActividades(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdactividad'])) {
					$this->oNegActividades->idactividad = $frm['pkIdactividad'];
				}
				JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
				$this->oNegActividades->__set('nivel',@$frm["txtNivel"]);
					$this->oNegActividades->__set('unidad',@$frm["txtunidad"]);
					$this->oNegActividades->__set('sesion',@$frm["txtSesion"]);					
					$this->oNegActividades->__set('metodologia',@$frm["txtMetodologia"]);
					$lista=@$frm["txtHabilidad"];
					//foreach($lista as $indice => $valor){
					for ($i=0;$i<count(@$frm["txtHabilidad"]);$i++){
						if ($i==0){
							@$habi.=@$frm["txtHabilidad"][$i];
						}else{
							@$habi.=" ".@$frm["txtHabilidad"][$i];
						}
					}
					$this->oNegActividades->__set('habilidad',@$habi);
					$this->oNegActividades->__set('titulo',@$frm["txtTitulo"]);
					$this->oNegActividades->__set('descripcion',@$frm["txtDescripcion"]);
					/*$this->oNegActividades->__set('estado',@$frm["txtEstado"]);
					$this->oNegActividades->__set('idioma',@$frm["txtIdioma"]);*/
					
				   if(@$frm["accion"]=="Nuevo"){
					
						/*$txtUrl=NegTools::subirImagen("url_",@$frm["txtUrl"], "actividades",false,100,100 );
						$this->oNegActividades->__set('url',@$txtUrl);*/
										    $res=$this->oNegActividades->agregar();
					}else{
					
						$archivo=basename($frm["txtUrl_old"]);
						if(!empty($frm["txtUrl"])) $txtUrl=NegTools::subirImagen("url_",@$frm["txtUrl"], "actividades",false,100,100 );
						$this->oNegActividades->__set('url',@$txtUrl);
						@unlink(RUTA_SITIO . SD ."static/media/actividades/".$archivo);
										    $res=$this->oNegActividades->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegActividades->idactividad);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
/*
	public function xGetxIDActividades(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegActividades->__set('idactividad', $pk);
				$this->datos = $this->oNegActividades->dataActividades;
				$res=$this->oNegActividades->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}*/
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegActividades->__set('idactividad', $pk);
				$res=$this->oNegActividades->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	     
}