<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-11-2016 
 * @copyright	Copyright (C) 19-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBiblioteca', RUTA_BASE, 'sys_negocio');
class WebBiblioteca extends JrWeb
{
	private $oNegBiblioteca;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBiblioteca = new NegBiblioteca;
		$this->oNegBiblioteca->setUsarBD('mysql');
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			$this->type=!empty($_GET["type"])?$_GET["type"]:'all';
			$dir =  RUTA_BASE . 'static' . SD . 'media' . SD ;
			$this->datos  = $this->dirToArray($dir,$this->type);
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('pagination.min', '/libs/pagination/');
            $this->documento->stylesheet('pagination.min', '/libs/pagination/');
			$this->documento->plantilla = 'modal';
			//!empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Biblioteca'), true);
			$this->esquema = 'biblioteca';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function listadojson()
	{
		try {
			$usuarioAct = NegSesion::getUsuario();
			$this->documento->plantilla = 'returnjson';
			$type=!empty($_REQUEST["type"])?$_REQUEST["type"]:'';
	        $nombre=!empty($_REQUEST["texto"])?$_REQUEST["texto"]:'';
			$filtros=array();
			if(!empty($type) && $type!='all' ) $filtros['tipo']=$type;
	        if(!empty($nombre) ){$filtros['nombre']=$nombre;}
	        $filtros["estado"] = 1;
	        $filtros["idproyecto"] = $usuarioAct["proyecto"]["idproyecto"];
			$this->datos=$this->oNegBiblioteca->buscar($filtros);

			echo json_encode(array('code'=>'ok','data'=>$this->datos));
			exit(0);

		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
			exit(0);
		}
	}

	/* // SOLO USAR PARA LLENAR LA TABLA 'biblioteca' CUANDO ESTA, ESTÁ VACÍA.
	public function llenar_bd()
	{

		try{
			global $aplicacion;

			$this->type=!empty($_GET["type"])?$_GET["type"]:'all';
			$dir =  RUTA_BASE . 'static' . SD . 'media' . SD ;
			$datos  = $this->dirToArray($dir,$this->type);

			$usuarioAct = NegSesion::getUsuario();
			foreach ($datos as $k=>$v) {

				echo '<p>$v = '.$v.'</p>';
				$nombre=explode(SD, $v);
				echo '<pre>'.var_dump($nombre).'</pre>';
				echo '<p> count-1 = '.$nombre[(count($nombre)-1)].'</p>';
				echo '<p> count-2 = '.$nombre[(count($nombre)-2)].'</p>';
				echo '<p> count-3 = '.$nombre[(count($nombre)-3)].'</p>';

				$tipo = $nombre[(count($nombre)-2)];
				$nombre_arch = $nombre[(count($nombre)-1)];
				$nombre_sin_ext = explode('.',$nombre_arch);

		   		$this->oNegBiblioteca->__set('nombre', $nombre_sin_ext[0]);
				$this->oNegBiblioteca->__set('link', $nombre_arch );
				$this->oNegBiblioteca->__set('tipo', $tipo );
				$this->oNegBiblioteca->__set('idproyecto',@$usuarioAct["proyecto"]["idproyecto"]);
				$this->oNegBiblioteca->__set('idpersonal',@$usuarioAct["dni"]);
				$this->oNegBiblioteca->__set('estado', 1);
				$res=$this->oNegBiblioteca->agregar();
				if($res) echo "<br><b>*********** INSERTADO! *********</b><br>";
			}

			echo "<h1>ÉXITO!!!</h1>";
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}*/

	public function cargarmedia(){
		try {			
			$this->documento->plantilla = 'returnjson';

			if(empty($_POST["tipo"])||empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
			$intipo='';
			$tipo=@$_POST["tipo"];
			if($tipo=='video'){
				$intipo=array('mp4','mov');
			}elseif($tipo=='image'){
				$intipo=array('jpg', 'jpeg','gif','png');
			}elseif($tipo=='audio'){
				$intipo=array('mp3','wav', 'm4a');
			}elseif($tipo=='pdf'){
				$intipo=array('pdf', 'PDF');
			}elseif($tipo=='ppt'){
				$intipo=array('ppt', 'PPT', 'pptx', 'PPTX');
			}elseif($tipo=='xls'){
				$intipo=array('xls', 'XLX', 'xls', 'XLSX');
			}
			
			if(!in_array($ext, $intipo)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
				exit(0);
			}
			try{
				$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo ;

				$nombre_sin_ext = str_replace('.'.$ext, '', $file["name"]);
				$new_name = $this->normalizarNombre($file["name"]);				
				$new_name = $this->nombrefile($dir_media.SD.$new_name);

				@mkdir($dir_media,0775,true);
				@chmod($dir_media,0775);
				if(move_uploaded_file($file["tmp_name"],$dir_media.SD.$new_name)) 
			  	{
			   		global $aplicacion;			
					$usuarioAct = NegSesion::getUsuario();
			   		$this->oNegBiblioteca->__set('nombre',$nombre_sin_ext);
					$this->oNegBiblioteca->__set('link',$new_name);
					$this->oNegBiblioteca->__set('tipo',$tipo);
					$this->oNegBiblioteca->__set('idproyecto',@$usuarioAct["proyecto"]["idproyecto"]);
					$this->oNegBiblioteca->__set('idpersonal',@$usuarioAct["dni"]);
					$this->oNegBiblioteca->__set('estado', 1);

					$res=$this->oNegBiblioteca->agregar();	
					echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File Upload success'),'namelink'=>$tipo.SD.$new_name,'nombre'=>$nombre_sin_ext));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error upload File')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}
	}


	public function cargarblob(){
		try {			
			$this->documento->plantilla = 'returnjson';
			if(empty($_POST["type"])||empty($_POST["name"])||empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$type=$_POST["type"];
			$intipo='';
			$tipe_=@explode('/',@$_POST["type"]);
            $tipo=$tipe_[0];
            $ext=$tipe_[1];
			if($tipo=='image'){
				$intipo=array('jpg','gif','png');
			}elseif($tipo=='audio'){
				$intipo=array('mp3','wav');
			}
			$name=@$_POST["name"];			
			if(!in_array($ext, $intipo)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
				exit(0);
			}
			try{

				$newname="N".@$_POST["nivel"].'-'."U".@$_POST["unidad"].'-'."A".@$_POST["leccion"].'-'.date("Ymdhis").".".$ext;
				
				$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo ;
				@mkdir($dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo,'0777');
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newname)) 
			  	{
			   		global $aplicacion;			
					$usuarioAct = NegSesion::getUsuario();
			   		$this->oNegBiblioteca->__set('nombre',$name);
					$this->oNegBiblioteca->__set('link',$newname);
					$this->oNegBiblioteca->__set('tipo',$tipo);
					$this->oNegBiblioteca->__set('idpersonal',@$usuarioAct["dni"]);
					$this->oNegBiblioteca->__set('idnivel',@$_POST["nivel"]);
					$this->oNegBiblioteca->__set('idunidad',@$_POST["unidad"]);
					$this->oNegBiblioteca->__set('idleccion',@$_POST["leccion"]);
					$this->oNegBiblioteca->__set('fechareg',date('Y/m/d'));
					$res=$this->oNegBiblioteca->agregar();
			   		echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File Upload success'),'namelink'=>$newname,'nombre'=>$name,'id'=>$res));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error upload File')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}

	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$idFile = $args[0];
				$link = $args[1];
				$tipo = $args[2];
				$this->oNegBiblioteca->__set('idbiblioteca', $idFile);

				/*$dir_media = RUTA_BASE . 'static' . SD . 'media' ;
				@unlink($dir_media.$idFile);
                $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Delete record success')), 'success');*/

				//$res=$this->oNegBiblioteca->eliminar();
				$res=$this->oNegBiblioteca->eliminar_logico();
				if(!empty($res)){
                	$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Delete record success')), 'success');
					$oRespAjax->setReturnValue($res);
				}else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}

			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	private function dirToArray($dir,$type,$path='') { 
		$result = array();
		$cdir = scandir($dir);
		$tmptype["image"]=array('png','gif','jpg','jpeg','bmp');
		$tmptype["audio"]=array('mp3','wav','mid');
		$tmptype["video"]=array('mp4','mpeg');
		$tmptype["file"]=array('pdf','doc','docx','xls','xlsx','ppt','pptx','txt');
		$tmptype["pdf"]=array('pdf');
		foreach ($cdir as $key => $value) 
		{
			if (!in_array($value,array(".",".."))){ 
				if (is_dir($dir . DIRECTORY_SEPARATOR . $value)){
					$path1=$path.DIRECTORY_SEPARATOR . $value;
					$res=$this->dirToArray($dir . DIRECTORY_SEPARATOR . $value,$type,$path1);
					$result=array_merge($result,$res);
				}else{
					$ext=strtolower(pathinfo($value, PATHINFO_EXTENSION));
					if(!empty($tmptype[$type])){	         		
						if(in_array($ext,$tmptype[$type])){
							$result[] = $path.DIRECTORY_SEPARATOR.$value; 
						}
					}else
					$result[] = $path.DIRECTORY_SEPARATOR.$value; 
				}	         	
			} 
		}
		return $result; 
	} 

	private function nombrefile($file,$nameorgi='',$cont=0){		
		$nameinfo=pathinfo($file);
		$name=$nameinfo['filename'].".".$nameinfo['extension'];
		if(file_exists($file)){
			$cont++;
			$nameorgi=!empty($nameorgi)?$nameorgi:$name;	
			$newname=$nameorgi."_".$cont;
			$file=str_replace($name, $newname, $file);
			$name = $this->nombrefile($file,$nameorgi,$cont);
			return $name;
		}else 
		return $name;
	}

	private function normalizarNombre($nombre='')
	{
		$new_nombre = '';
		$unwanted_array = array(
			'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 
			'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 
			'Þ'=>'B',
			'Ç'=>'C', 'ç'=>'c', 
			'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 
			'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 
			'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 
			'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 
			'Ñ'=>'N', 'ñ'=>'n', 
			'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 
			'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ð'=>'o', 
			'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 
			'ù'=>'u', 'ú'=>'u', 'û'=>'u', 
			'Ý'=>'Y',  
			'ý'=>'y', 'ÿ'=>'y',
			'Š'=>'S', 'š'=>'s', 'ß'=>'Ss', 'þ'=>'b',
			'Ž'=>'Z', 'ž'=>'z', 
			' '=>'_',
		);
		$new_nombre = strtr($nombre, $unwanted_array);
		return $new_nombre;
	}


	/************* Modulo Biblioteca - Enny ****************/
	public function modulo_biblioteca(){
		header('Location: '.$this->documento->getUrlBase().'/modbiblioteca');
		exit(0);
	}

	public function getVarsGlobales(){
		$this->documento->plantilla = 'returnjson';
		$usuario = NegSesion::getUsuario();
		if(empty($usuario)) {header('Location: '.$this->documento->getUrlBase());}
		$url_base = $this->documento->getUrlBase();
		$url_static = $this->documento->getUrlStatic();
		echo json_encode(array('code'=>'ok', 'data'=>array('usuario'=>$usuario, 'url_base'=>$url_base, 'url_static'=>$url_static)));
	}

}