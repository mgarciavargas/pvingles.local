<?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       16-11-2016 
 * @copyright   Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamenes_preguntas', RUTA_BASE, 'sys_negocio');
class WebTemplate extends JrWeb
{
    private $oNegExamenes_preguntas;
    public function __construct()
    {
        parent::__construct();
        $this->oNegPreguntas = new NegExamenes_preguntas;
        $this->oNegPreguntas->setUsarBD('mysql');
    }

    public function defecto(){
        try {
            global $aplicacion;
            throw new Exception(JrTexto::_('403 Forbidden access').'!');
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    /*public function image(){
        try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Image Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/image';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function video(){
        try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Video Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/video';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function audio(){
        try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Audio Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/audio';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }*/

    public function click_drag(){
        try {
            global $aplicacion;
            if(!empty($_REQUEST["idpregunta"])){
                $filtro['idpregunta']=$_REQUEST["idpregunta"];
                $preguntas=$this->oNegPreguntas->buscar($filtro);
                if(!empty($preguntas))
                    $this->pregunta=$preguntas[0];
            }
            $this->documento->setTitulo(JrTexto::_('Click and Drag Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/multiplantilla';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function options(){
        try {
            global $aplicacion;
            if(!empty($_REQUEST["idpregunta"])){
                $filtro['idpregunta']=$_REQUEST["idpregunta"];
                $preguntas=$this->oNegPreguntas->buscar($filtro);
                if(!empty($preguntas))
                    $this->pregunta=$preguntas[0];
            }
            $this->documento->setTitulo(JrTexto::_('Options Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/multiplantilla';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function select_box(){
        try {
            global $aplicacion;
            if(!empty($_REQUEST["idpregunta"])){
                $filtro['idpregunta']=$_REQUEST["idpregunta"];
                $preguntas=$this->oNegPreguntas->buscar($filtro);
                if(!empty($preguntas))
                    $this->pregunta=$preguntas[0];
            }
            $this->documento->setTitulo(JrTexto::_('Select Box Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/multiplantilla';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function gap_fill(){
        try {
            global $aplicacion;
            if(!empty($_REQUEST["idpregunta"])){
                $filtro['idpregunta']=$_REQUEST["idpregunta"];
                $preguntas=$this->oNegPreguntas->buscar($filtro);
                if(!empty($preguntas))
                    $this->pregunta=$preguntas[0];
            }
            $this->documento->setTitulo(JrTexto::_('Gap Fill Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/multiplantilla';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function true_false(){
        try {
            global $aplicacion;
            if(!empty($_REQUEST["idpregunta"])){
                $filtro['idpregunta']=$_REQUEST["idpregunta"];
                $preguntas=$this->oNegPreguntas->buscar($filtro);
                if(!empty($preguntas))
                    $this->pregunta=$preguntas[0];
            }
            $this->documento->setTitulo(JrTexto::_('True or False Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/true_false';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function join(){
        try {
            global $aplicacion;
            if(!empty($_REQUEST["idpregunta"])){
                $filtro['idpregunta']=$_REQUEST["idpregunta"];
                $preguntas=$this->oNegPreguntas->buscar($filtro);
                if(!empty($preguntas))
                    $this->pregunta=$preguntas[0];
            }
            $this->documento->setTitulo(JrTexto::_('Join Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/join';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function order_simple(){
        try {
            global $aplicacion;
            if(!empty($_REQUEST["idpregunta"])){
                $filtro['idpregunta']=$_REQUEST["idpregunta"];
                $preguntas=$this->oNegPreguntas->buscar($filtro);
                if(!empty($preguntas))
                    $this->pregunta=$preguntas[0];
            }
            $this->documento->setTitulo(JrTexto::_('Order Letters/Words Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/order_simple';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function order_paragraph(){
        try {
            global $aplicacion;
            if(!empty($_REQUEST["idpregunta"])){
                $filtro['idpregunta']=$_REQUEST["idpregunta"];
                $preguntas=$this->oNegPreguntas->buscar($filtro);
                if(!empty($preguntas))
                    $this->pregunta=$preguntas[0];
            }
            $this->documento->setTitulo(JrTexto::_('Order Paragraphs Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/order_paragraph';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function tag_image(){
        try {
            global $aplicacion;
            if(!empty($_REQUEST["idpregunta"])){
                $filtro['idpregunta']=$_REQUEST["idpregunta"];
                $preguntas=$this->oNegPreguntas->buscar($filtro);
                if(!empty($preguntas))
                    $this->pregunta=$preguntas[0];
            }
            $this->documento->setTitulo(JrTexto::_('Tag Image Template'), true);
            $this->documento->plantilla = 'modal';
            $this->esquema = 'examenes/plantillas/tag_image';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
}