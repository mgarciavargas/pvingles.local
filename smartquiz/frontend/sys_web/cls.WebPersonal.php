<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-07-2017 
 * @copyright	Copyright (C) 24-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
class WebPersonal extends JrWeb
{
	private $oNegPersonal;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersonal = new NegPersonal;
	}

	public function defecto(){
		return $aplicacion->redir(JrAplicacion::getJrUrl(array('examenes', 'publicados')), false);
		//return $this->listado();
	}
	/*
	public function listado()
	{
		try{
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$this->usuarioAct = NegSesion::getUsuario();
			$this->raiz = _http_.$_SERVER["SERVER_NAME"];
			$this->uniqid = uniqid();
			$this->datos=$this->oNegPersonal->buscar(['idempresa'=>$this->usuarioAct['idempresa'], ]);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'inicio';
			$this->documento->setTitulo(JrTexto::_('Alumno'), true);
			$this->esquema = 'alumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function verficha(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Student').' /'.JrTexto::_('Ficha'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->esquema = 'alumno/ficha';
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!=''){
				$this->oNegPersonal->idalumno=$_REQUEST["id"];
				$this->datos=$this->oNegPersonal->dataAlumno;
				if(empty($this->datos)){
					throw new Exception(JrTexto::_('Restricted access').'!!');	
					return;
				}
			}else{
				throw new Exception(JrTexto::_('Restricted access').'!!');	
				return;
			}			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function cambiarclave(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Student').' /'.JrTexto::_('Change Password'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->user='alumno';
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!=''){
				$this->oNegPersonal->idalumno=$_REQUEST["id"];
				$this->datos=$this->oNegPersonal->dataAlumno;
				if(empty($this->datos)){
					throw new Exception(JrTexto::_('Restricted access').'!!');	
					return;
				}
			}else{
				throw new Exception(JrTexto::_('Restricted access').'!!');	
				return;
			}			
			$this->esquema = 'usuario/cambiarclave';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}	
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegPersonal->idalumno = @$_GET['id'];
			$this->datos = $this->oNegPersonal->dataAlumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->usuarioAct = NegSesion::getUsuario();
			//$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->estado_civil=$this->oNegEstado_civil->buscar();
			$this->area=$this->oNegCargos->buscar(array('tipo'=>'A', 'idempresa'=>$this->usuarioAct['idempresa']));
			//$this->fkubigeo=$this->oNegUbigeo->buscar();
			//$this->fkidugel=$this->oNegUgel->buscar();
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'inicio';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	*/
	// ========================== Funciones ajax ========================== //

	public function jxBuscar(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Alumno', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}
			$filtros=array();
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["ape_paterno"]!='')$filtros["ape_paterno"]=$_REQUEST["ape_paterno"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["ape_materno"]!='')$filtros["ape_materno"]=$_REQUEST["ape_materno"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fechanac"])&&@$_REQUEST["fechanac"]!='')$filtros["fechanac"]=$_REQUEST["fechanac"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["estado_civil"])&&@$_REQUEST["estado_civil"]!='')$filtros["estado_civil"]=$_REQUEST["estado_civil"];
			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"];
			if(isset($_REQUEST["urbanizacion"])&&@$_REQUEST["urbanizacion"]!='')$filtros["urbanizacion"]=$_REQUEST["urbanizacion"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["token"])&&@$_REQUEST["token"]!='')$filtros["token"]=$_REQUEST["token"];
			if(isset($_REQUEST["foto"])&&@$_REQUEST["foto"]!='')$filtros["foto"]=$_REQUEST["foto"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["identificador"])&&@$_REQUEST["identificador"]!='')$filtros["identificador"]=$_REQUEST["identificador"];
			if(isset($_REQUEST["idioma"])&&@$_REQUEST["idioma"]!='')$filtros["idioma"]=$_REQUEST["idioma"];
			
			if(isset($_REQUEST["slug"])&&@$_REQUEST["slug"]!='')$filtros["slug"]=$_REQUEST["slug"];

			$this->oNegPersonal->setLimite(0,99999);
			$data=$this->oNegPersonal->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$data));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
            exit(0);
        }
	}

	/*
	public function json_datosxalumno()
	{
		$this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST["id"]) || empty($_POST["tipo"])) { 
                throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $promXHab = [];
            $this->alumnos = [];
            if($_POST['tipo']=='A'){
            	$filtros["idalumno"]=$_POST["id"];
            } elseif ($_POST['tipo']=='G') {
            	$filtros['idgrupo']=$_POST["id"];
            }
            $exams_alum=$this->oNegGrupo_matricula->buscar($filtros);
            foreach ($exams_alum as $al) {
            	$this->oNegPersonal->idalumno = $al['idalumno'];
            	$alumno=$this->oNegPersonal->getXid();
            	
            	$ultimaActividad=$this->oNegActividad_alumno->ultimaActividadxAlum($al['idalumno']);
            	
            	if($alumno['estado']==1){
            		$new_alum = [];
	            	$new_alum = [
	            		'idalumno' => $alumno['idalumno'],
	            		'apellidopaterno' => $alumno['apellidopaterno'],
	            		'apellidomaterno' => $alumno['apellidomaterno'],
	            		'nombre' => $alumno['nombre'],
	            		'email' => $alumno['email'],
	            		'foto' => $alumno['foto'],
	            		'ultima_actividad' => $ultimaActividad,
	            	];
	            	$this->alumnos[] = $new_alum;
            	}
            }
            $data=array('code'=>'ok','data'=>$this->alumnos);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
	}

	public function guardarAlumno(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkidalumno)) {
				$this->oNegPersonal->idalumno = $frm['pkidalumno'];
				$accion='_edit';
			}

        	global $aplicacion;         
        	$usuarioAct = NegSesion::getUsuario();
        	@extract($_POST);
        	JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
			$this->oNegPersonal->apellidopaterno=@$txtApellidopaterno;
			$this->oNegPersonal->apellidomaterno=@$txtApellidomaterno;
			$this->oNegPersonal->nombre=@$txtNombre;
			$this->oNegPersonal->fechanacimiento=@$txtFechanacimiento;
			$this->oNegPersonal->sexo=@$txtSexo;
			$this->oNegPersonal->dni=@$txtDni;
			$this->oNegPersonal->idestadocivil=@$txtidEstadocivil;
			$this->oNegPersonal->ubigeo=@$txtUbigeo;
			$this->oNegPersonal->direccion=@$txtDireccion;
			$this->oNegPersonal->telefono=@$txtTelefono;
			$this->oNegPersonal->celular=@$txtCelular;
			$this->oNegPersonal->email=@$txtEmail;
			$this->oNegPersonal->idugel=@$txtIdugel;
			$this->oNegPersonal->regusuario=@$txtRegusuario;
			$this->oNegPersonal->regfecha=@$txtRegfecha;
			$this->oNegPersonal->usuario=@$txtUsuario;
			$this->oNegPersonal->clave=@$txtClave;
			$this->oNegPersonal->clave1=@$txtclave1;
			$this->oNegPersonal->estado=@$txtEstado;
			$this->oNegPersonal->observacion=@$txtobservacion;
					
            if($accion=='_add') {
            	$res=$this->oNegPersonal->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Alumno')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPersonal->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Alumno')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}
	*/

	
	// ========================== Funciones xajax ========================== //
	/*
	public function xSaveAlumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				$usuarioAct = NegSesion::getUsuario();
				
				if(!empty($frm['pkidalumno'])) {
					$this->oNegPersonal->idalumno = $frm['pkidalumno'];
				}
				JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
				$this->oNegPersonal->apellidopaterno=@$frm["txtApellidopaterno"];
				$this->oNegPersonal->apellidomaterno=@$frm["txtApellidomaterno"];
				$this->oNegPersonal->nombre=@$frm["txtNombre"];
				$this->oNegPersonal->fechanacimiento=@$frm["txtFechanacimiento"];
				$this->oNegPersonal->sexo=@$frm["txtSexo"];
				$this->oNegPersonal->dni=@$frm["txtDni"];
				$this->oNegPersonal->idestadocivil=@$frm["txtIdEstadocivil"];
				$this->oNegPersonal->ubigeo=@$frm["txtUbigeo"];
				$this->oNegPersonal->direccion=@$frm["txtDireccion"];
				$this->oNegPersonal->telefono=@$frm["txtTelefono"];
				$this->oNegPersonal->celular=@$frm["txtCelular"];
				$this->oNegPersonal->email=@$frm["txtEmail"];
				$this->oNegPersonal->idugel=@$frm["txtIdugel"];
				$this->oNegPersonal->usuario=@$frm["txtUsuario"];
				$this->oNegPersonal->clave=@$frm["txtClave"];
				$this->oNegPersonal->clave1=@$frm["txtclave1"];
				$this->oNegPersonal->estado=@$frm["txtEstado"];
				$this->oNegPersonal->idcargo=@$frm["txtIdcargo"];
				$this->oNegPersonal->observacion=@$frm["txtobservacion"];
				$this->oNegPersonal->regusuario=@$usuarioAct["idusuario"];
				$this->oNegPersonal->idempresa=@$usuarioAct["idempresa"];
				$this->oNegPersonal->regfecha=date('Y-m-d');
				
			   if(@$frm["accion"]=="Nuevo"){
				
					$txtFoto=NegTools::subirImagen("foto_",@$frm["txtFoto"], "alumno",false,100,100 );
					$this->oNegPersonal->__set('foto',@$txtFoto);
									    $res=$this->oNegPersonal->agregar();
				}else{
				
					$archivo=basename($frm["txtFoto_old"]);
					if(!empty($frm["txtFoto"])) $txtFoto=NegTools::subirImagen("foto_",@$frm["txtFoto"], "alumno",false,100,100 );
					$this->oNegPersonal->__set('foto',@$txtFoto);
					@unlink(RUTA_SITIO . SD ."static/media/alumno/".$archivo);
									    $res=$this->oNegPersonal->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegPersonal->idalumno);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAlumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersonal->__set('idalumno', $pk);
				$this->datos = $this->oNegPersonal->dataAlumno;
				$res=$this->oNegPersonal->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersonal->__set('idalumno', $pk);
				$res=$this->oNegPersonal->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegPersonal->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
	*/
}