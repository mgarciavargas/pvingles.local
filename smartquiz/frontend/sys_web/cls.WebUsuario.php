<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-11-2016 
 * @copyright	Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
//JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUsuario', RUTA_BASE, 'sys_negocio');
class WebUsuario extends JrWeb
{
	/*private $oNegAlumno;
	private $oNegPersonal;*/
	private $oNegUsuario;
	public function __construct()
	{
		parent::__construct();		
		/*$this->oNegPersonal = new NegPersonal;
		$this->oNegAlumno=new NegAlumno;*/
		$this->oNegUsuario=new NegUsuario;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			$this->usuarioAct = NegSesion::getUsuario();
			/*if($this->usuarioAct["rol"]!='superadmin'){
				throw new Exception(JrTexto::_('Access denied'));
			}*/
			$this->documento->script('moment', '/libs/moment/');
			$this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
            $this->documento->script('datetimepicker', '/libs/datepicker/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
           
			#$this->datos=$this->oNegUsuario->buscar(array('idempresa'=>$this->usuarioAct["idempresa"]));
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'inicio';
			$this->documento->setTitulo(JrTexto::_('Usuario'), true);
			$this->esquema = 'usuario/listado';			
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	public function buscarjson()
	{
		try{
			global $aplicacion;
			$filtros=array();
			$filtros["idusuario"]=!empty($_REQUEST["idusuario"])?$_REQUEST["idusuario"]:'';			
			$this->usuarioAct = NegSesion::getUsuario();
			if($this->usuarioAct["rol"]!='Alumno')
				$this->datos=$this->oNegPersonal->buscar($filtros);
			else
				$this->datos=$this->oNegAlumno->buscar($filtros);
			$this->documento->plantilla = 'returnjson';
			if(count($this->datos)==1) $this->datos=$this->datos[0];
			$data=array('code'=>'ok','data'=>$this->datos);
			echo json_encode($data);
			//return parent::getEsquema();
		}catch(Exception $e) {
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			$this->usuarioAct = NegSesion::getUsuario();
			if($this->usuarioAct["rol"]!='superadmin'){
				throw new Exception(JrTexto::_('Access denied'));
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			$this->usuarioAct = NegSesion::getUsuario();
			if($this->usuarioAct["rol"]!='superadmin'){
				throw new Exception(JrTexto::_('Access denied'));
			}
			$this->frmaccion='Editar';
			$this->oNegAlumno->idusuario = @$_GET['id'];
			$this->datos = $this->oNegAlumno->dataAlumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver()
	{
		try{
			global $aplicacion;						
			$this->oNegAlumno->idusuario = @$_GET['id'];
			$this->datos = $this->oNegAlumno->dataAlumno;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('see'), true);
			$this->esquema = 'alumno-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'usuario/agregar';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'inicio';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones xajax ========================== //
	public function xSaveUsuario(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				$usuarioAct = NegSesion::getUsuario();
				if(!empty($frm['pkIdUsuario'])) {
					$this->oNegUsuario->idusuario = $frm['pkIdUsuario'];
				}
				$this->oNegUsuario->__set('apellidos',@$frm["txtApellidos"]);
				$this->oNegUsuario->__set('nombres',@$frm["txtNombres"]);
				$this->oNegUsuario->__set('usuario',@$frm["txtUsuario"]);
				$this->oNegUsuario->__set('clave',@$frm["txtClave"]);
				$this->oNegUsuario->__set('estado',@$frm["txtEstado"]);
				$this->oNegUsuario->__set('regusuario',$usuarioAct['idusuario']);
				$this->oNegUsuario->__set('regfecha',date('Y-m-d'));
				$this->oNegUsuario->__set('codigo','');
				$this->oNegUsuario->__set('idempresa',$usuarioAct['idempresa']);

			    if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegUsuario->agregar();
				}else{
					$res=$this->oNegUsuario->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegUsuario->idusuario);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xSetUsuario(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {             
				if(empty($args[0])) { return;}
				$usuarioAct = NegSesion::getUsuario();
				if(isset($usuarioAct)){
					$rol=$usuarioAct['rol'];
					$data=$args[0];
					$res=0;
					if($rol=='Alumno'){
						$this->oNegUsuario->idusuario = $usuarioAct['idusuario'];				
						$this->oNegUsuario->__set($data["campo"],$data["value"]);
						$res=$this->oNegUsuario->setCampo($usuarioAct['idusuario'],$data["campo"],trim($data["value"]));
					}else{
						$this->oNegPersonal->idusuario = $usuarioAct['idusuario'];
						$this->oNegPersonal->__set($data["campo"],$data["value"]);
						$res=$this->oNegPersonal->setCampo($usuarioAct['idusuario'],$data["campo"],trim($data["value"]));						
					}

					if($res==1) {
						$oRespAjax->setReturnValue(true);
						$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_('Save changed success ')), 'success');
					}
					else $oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xBuscar(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Usuario', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}
			$filtros=array();
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["apellidos"])&&@$_REQUEST["apellidos"]!='')$filtros["apellidos"]=$_REQUEST["apellidos"];
			if(isset($_REQUEST["nombres"])&&@$_REQUEST["nombres"]!='')$filtros["nombres"]=$_REQUEST["nombres"];
			/*if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];*/
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["codigo"])&&@$_REQUEST["codigo"]!='')$filtros["codigo"]=$_REQUEST["codigo"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			$this->datos=$this->oNegUsuario->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array( 'code'=>'Error','msj'=>$e->getMessage() ));
            exit(0);
        }
	}

	public function xGuardar()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			$usuarioAct = NegSesion::getUsuario();
			if(!empty(@$_POST['pkIdUsuario'])) {
				$this->oNegUsuario->idusuario = $_POST['pkIdUsuario'];
			}
			$this->oNegUsuario->__set('apellidos',@$_POST["txtApellidos"]);
			$this->oNegUsuario->__set('nombres',@$_POST["txtNombres"]);
			$this->oNegUsuario->__set('usuario',@$_POST["txtUsuario"]);
			$this->oNegUsuario->__set('clave',@$_POST["txtClave"]);
			$this->oNegUsuario->__set('estado',@$_POST["txtEstado"]);
			$this->oNegUsuario->__set('regusuario',$usuarioAct['idusuario']);
			$this->oNegUsuario->__set('regfecha',date('Y-m-d'));
			$this->oNegUsuario->__set('codigo','');
			$this->oNegUsuario->__set('idempresa',$usuarioAct['idempresa']);

			if(@$_POST["accion"]=="Nuevo"){
				$resp=$this->oNegUsuario->agregar();
				$mensaje = JrTexto::_('Registro creado');
			}else{
				$resp=$this->oNegUsuario->editar();
				$mensaje = JrTexto::_('Registro actualizado');
		    }

			echo json_encode(array( 'code'=>'ok','data'=>$resp, 'msj'=>$mensaje ));
		} catch (Exception $e) {
			echo json_encode(array( 'code'=>'Error','msj'=>$e->getMessage() ));
		}
	}

}