 <?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017 
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamenes', RUTA_BASE, 'sys_negocio');
/*JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_tipo', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegExamenes_preguntas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto_config', RUTA_BASE, 'sys_negocio');
/*JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');*/
/*
JrCargador::clase('sys_negocio::NegTarea_archivos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_respuesta', RUTA_BASE, 'sys_negocio');
*/
class WebExamenes extends JrWeb
{
	protected $oNegExamenes;
	/*protected $oNegNiveles;
	protected $oNegExamen_tipo;*/
	protected $oNegpreguntas;
    protected $oNegMetodologia;
    protected $oNegExamen_alumno;
    protected $oNegAlumno;
    protected $oNegNegPersonal;
    protected $oNegProyecto_config;
    /*private $oNegGrupo_matricula;*/

    /*private $oNegTarea_archivos;
    private $oNegTarea;  
    private $oNegTarea_asignacion;
    private $oNegTarea_asignacion_alumno;
    private $oNegTarea_respuesta;*/

	public function __construct()
	{
		parent::__construct();
		/*$this->oNegNiveles = new NegNiveles;
		$this->oNegExamen_tipo = new NegExamen_tipo;*/
		$this->oNegExamenes = new NegExamenes;
		$this->oNegpreguntas = new NegExamenes_preguntas;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegExamen_alumno = new NegExamen_alumno;
        $this->oNegAlumno = new NegAlumno;
        $this->oNegPersonal = new NegPersonal;
        $this->oNegProyecto_config = new NegProyecto_config;
        /*$this->oNegGrupo_matricula = new NegGrupo_matricula;*/
        /*$this->oNegTarea_archivos = new NegTarea_archivos;
        $this->oNegTarea = new NegTarea;    
        $this->oNegTarea_asignacion = new NegTarea_asignacion;
        $this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;
        $this->oNegTarea_respuesta = new NegTarea_respuesta;*/
	}

	public function defecto(){
		return $this->publicados();
	}

	public function publicados(){
		global $aplicacion;
		$this->documento->setTitulo(JrTexto::_('Exams'), true);
		/*$idnivel_=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
		$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
        $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
        $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

        $_idunidad=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
        $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
        $idunidad_=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
        $this->idunidad=!empty($_idunidad)?$_idunidad:($idunidad_);

        $_idactividad=!empty(JrPeticion::getPeticion(4))?JrPeticion::getPeticion(4):0;
        $this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
       	$idactividad_=!empty($this->actividades[0]["idnivel"])?$this->actividades[0]["idnivel"]:0;
       	$this->idactividad=!empty($_idactividad)?$_idactividad:($idactividad_);*/
       	
       	$_idtipo=!empty(JrPeticion::getPeticion(5))?JrPeticion::getPeticion(5):0;
       	/*$this->tipoexamen=$this->oNegExamen_tipo->buscar(array('estado'=>1));*/
       	$idtipo_=!empty($this->tipoexamen[0]["idtipo"])?$this->tipoexamen[0]["idtipo"]:0;
       	$this->tipo=!empty($_idtipo)?$_idtipo:($idtipo_);
		$usuarioAct = NegSesion::getUsuario();
		$rol=$usuarioAct["rol"];
		
		$filtros = array();
		/*$filtros["idnivel"]=$this->idnivel;
		$filtros["idunidad"]=$this->idunidad;
		$filtros["idactividad"]=$this->idactividad;
		$filtros["tipo"]=$this->tipo;*/
		$filtros['idproyecto'] = $usuarioAct['proyecto']['idproyecto'];
		if(strtolower($rol)==strtolower('Alumno')){	
			$this->documento->plantilla = 'examenes/general';
			$filtros["estado"]=1;
			$this->esquema = 'examenes/alu_publicados';
		}else{
			$this->documento->plantilla = 'examenes/inicio';
			$this->examenes=$this->oNegExamenes->buscar($filtros);
        	$this->esquema = 'examenes/publicados';
		}
		@NegSesion::set('idexamencur', 0, '_examen_');
        return parent::getEsquema();
	}

	public function publicadosjson(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			/*$filtros["idnivel"]=!empty($_REQUEST["nivel"])?$_REQUEST["nivel"]:0;			
	        $filtros["idunidad"]=!empty($_REQUEST["unidad"])?$_REQUEST["unidad"]:0;
	        $filtros["idactividad"]=!empty($_REQUEST["actividad"])?$_REQUEST["actividad"]:0; 
	       	$filtros["tipo"]=!empty($_REQUEST["tipo"])?$_REQUEST["tipo"]:0;*/
	       	$filtros["estado"]=1;
	       	$filtros["titulo"]=!empty($_REQUEST["titulo"])?$_REQUEST["titulo"]:0;
			$usuarioAct = NegSesion::getUsuario();
			$rol=$usuarioAct["rol"];
			$filtros['idproyecto'] = $usuarioAct['proyecto']['idproyecto'];

			$this->oNegExamenes->setLimite(0,9999);
			if(strtolower($rol)==strtolower('Alumno'))
				//$this->esquema = 'alumno/examenes';
				$examenes=$this->oNegExamenes->buscar($filtros);
			else{
				/*$filtro["idpersonal"]=$usuarioAct["idusuario"];*/
				$examenes=$this->oNegExamenes->buscar($filtros);        	
			}
			$data=array('code'=>'ok','data'=>$examenes);
	        echo json_encode($data); exit();
	        return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
	}

	public function ver()
	{
		try {
			global $aplicacion;
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->accion = 'N';
            if( !empty(@$_GET['acc']) ) {
            	if(@$_GET['acc']=="generar") {
            		$this->accion = 'G';
            	} else if(@$_GET['acc']=="ubicacion") {
            		$this->accion = 'U';
            	}
            }
			$usuarioAct = NegSesion::getUsuario();
			$rol=$usuarioAct["rol"];
			$filtros=array();
			$idexamen=!empty(NegSesion::get('idexamencur','_examen_'))?NegSesion::get('idexamencur','_examen_'):-1;
			$this->idexamen=!empty(@$_GET["idexamen"])?$_GET["idexamen"]:$idexamen;//nuevo o edicion
			$this->idcurso = @$_GET["idcurso"]; // curso de SmartLearn del cual sacar Habilidades via JSON
			$filtros["idexamen"]=$this->idexamen;
			$filtros['idproyecto'] = $usuarioAct['proyecto']['idproyecto'];

			if(ucfirst($rol)==ucfirst('Alumno')){
				return $aplicacion->redir(JrAplicacion::getJrUrl(array('examenes', 'publicados')), false);
			}else{
				if($rol!='superadmin'){
					$filtros["idpersonal"]=$usuarioAct["idusuario"];
				}
	        	$this->esquema = 'examenes/setting';
			}

			$examen=$this->oNegExamenes->buscar($filtros);
			if(!empty($examen)){
				$this->examen = $examen[0];
				NegSesion::set('idexamencur', $this->examen["idexamen"], '_examen_');
				$this->accion = $this->examen['tipo'];
			}else{
				$this->examen=null;
				NegSesion::set('idexamencur', 0, '_examen_');
			}
			if(!empty($this->idcurso)){ $this->examen["origen_habilidades"] = 'JSON'; }
			/* buscar si hay Config de UrlSkills */
			$this->config_proyecto = $this->oNegProyecto_config->buscar($filtros);

	       	$this->cant_intentos = 5; //se listará en el <select id="nintento">
            $this->max_intento = 9999; //se listará en el <select id="nintento"> al final

			$this->documento->setTitulo(JrTexto::_('Exams').' /'.JrTexto::_('See'), true);
	       	$this->documento->plantilla = 'examenes/inicio';
	        return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function preguntas()
	{
		try {
			global $aplicacion;	

			$this->documento->setTitulo(JrTexto::_('Exams').' /'.JrTexto::_('preguntas'), true);
			$this->documento->stylesheet('jquery-ui.min', '/tema/css/');
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->script('jquery.md5', '/tema/js/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');

            $this->documento->script('multiplantilla', '/js/examen/');
            $this->documento->script('true_false', '/js/examen/');
            $this->documento->script('join', '/js/examen/');
            $this->documento->script('order_simple', '/js/examen/');
            $this->documento->script('order_paragraph', '/js/examen/');
            $this->documento->script('tag_image', '/js/examen/');
            $this->documento->script('editactividad','/js/new/');
            $this->documento->script('actividad_completar','/js/new/');
            /*$this->documento->script('funciones','/tema/js/');*/

			/*$idnivel_=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
	        $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
	        $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

	        $_idunidad=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
	        $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
	        $idunidad_=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
	        $this->idunidad=!empty($_idunidad)?$_idunidad:($idunidad_);

	        $_idactividad=!empty(JrPeticion::getPeticion(4))?JrPeticion::getPeticion(4):0;
	        $this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
	       	$idactividad_=!empty($this->actividades[0]["idnivel"])?$this->actividades[0]["idnivel"]:0;
	       	$this->idactividad=!empty($_idactividad)?$_idactividad:($idactividad_);*/
	       	/*$this->tipoexamen=$this->oNegExamen_tipo->buscar(array('estado'=>1));*/
			$this->usuarioAct = NegSesion::getUsuario();
            $rol=$this->usuarioAct["rol"];

            $_idexamen=!empty(NegSesion::get('idexamencur','_examen_'))?NegSesion::get('idexamencur','_examen_'):-1;
	       	$this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:$_idexamen;
	       	NegSesion::set('idexamencur', $this->idexamen, '_examen_');
	       	$filtros["idexamen"]=$this->idexamen;
			$filtros['idproyecto'] = $this->usuarioAct['proyecto']['idproyecto'];
            if( strtolower($rol) == strtolower('Alumno')){
				/*$filtros["estado"]=1;
				$this->esquema = 'alumno/examenes';*/
            	return $aplicacion->redir(JrAplicacion::getJrUrl(array('examenes', 'publicados')), false);
			}else{
				if($rol!='superadmin'){
					$filtros["idpersonal"]=$this->usuarioAct["idusuario"];
				}
				$this->esquema = 'examenes/question';
			}
	       	$exam=$this->oNegExamenes->buscar($filtros);
	       	if(!empty($exam)){
				$this->examen=$exam[0];
				$filtros_preg["idexamen"]=$this->idexamen;
        		$filtros_preg["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
        		$this->oNegpreguntas->setLimite(0,9999);
        		$this->preguntas=$this->oNegpreguntas->mostrarPreguntas($filtros_preg);
        		$this->accion=$this->examen['tipo'];
			} else {
				throw new Exception(JrTexto::_('Exam does not exist').'!');
			}
			/*$this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));*/
			$this->dificultad = array(JrTexto::_('easy'), JrTexto::_('medium'), JrTexto::_('difficult') );
			$this->habilidades=json_decode($this->examen['habilidades_todas'], true);
			$this->documento->plantilla = 'examenes/inicio';			
	        return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function preview()
	{
		try {
			global $aplicacion;
			$this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->script('mitimer', '/libs/chingo/');
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.md5', '/tema/js/');
            $this->documento->stylesheet('circletimer', '/libs/circletimer/');
            $this->documento->script('circletimer.min', '/libs/circletimer/');
            $this->documento->script('multiplantilla', '/js/examen/');
            $this->documento->script('true_false', '/js/examen/');
            $this->documento->script('order_simple', '/js/examen/');
            $this->documento->script('order_paragraph', '/js/examen/');
            $this->documento->script('join', '/js/examen/');
            $this->documento->script('tag_image', '/js/examen/');
            $this->documento->script('editactividad','/js/new/');
            $this->documento->script('actividad_completar','/js/new/');
            $this->documento->script('funciones','/tema/js/');
            $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
			$this->documento->script('circle', '/libs/graficos/progressbar/');
			$usuarioAct = NegSesion::getUsuario();
			$rol=$usuarioAct["rol"];
			$_idexamen=!empty(NegSesion::get('idexamencur','_examen_'))?NegSesion::get('idexamencur','_examen_'):0;
		    $this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:$_idexamen;
		   	NegSesion::set('idexamencur', $this->idexamen, '_examen_');
		   	$filtros["idexamen"]=$this->idexamen;
			$filtros['idproyecto'] = $usuarioAct['proyecto']['idproyecto'];
		   	$this->documento->plantilla = 'examenes/inicio';
		   	if(strtolower($rol)==strtolower('alumno')){
				/*$filtros["estado"]=1;
				$this->esquema = 'alumno/examenes';*/
				return $aplicacion->redir(JrAplicacion::getJrUrl(array('examenes', 'publicados')), false);
			}else{
				if($rol!='superadmin'){
					$filtros["idpersonal"]=$usuarioAct["idusuario"];
				}
		    	$this->esquema = 'examenes/preview';
			}
			$exam=$this->oNegExamenes->buscar($filtros);
		   	if(!empty($exam)){
				$this->examen=$exam[0];
				$filtros_preg["idexamen"]=$this->idexamen;
				$filtros_preg["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
				$this->oNegpreguntas->setLimite(0,9999);
				$this->preguntas=$this->oNegpreguntas->mostrarPreguntas($filtros_preg);
				/*$this->exanivel=$this->oNegNiveles->buscar(array('tipo'=>'N','idnivel'=>$this->examen["idnivel"]));
				$this->exanunidad=$this->oNegNiveles->buscar(array('tipo'=>'U','idnivel'=>$this->examen["idunidad"]));
				$this->exaactividad=$this->oNegNiveles->buscar(array('tipo'=>'L','idnivel'=>$this->examen["idactividad"]));*/
				/*$this->exatype=	$this->tipoexamen=$this->oNegExamen_tipo->buscar(array('idtipo'=>$this->examen["tipo"]));*/
			}else{
				throw new Exception(JrTexto::_('Exam does not exist').'!');
			}
			//$this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
            $this->habilidades=json_decode($this->examen['habilidades_todas'], true);
            $this->showBtnPrint = true;
            $this->max_intento = 9999;
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function reportes()
    {
        try {
            global $aplicacion;
            $usuarioAct = NegSesion::getUsuario();
            $rol=$usuarioAct["rol"];
            if( strtolower($rol) == strtolower('Alumno')){
            	return $aplicacion->redir(JrAplicacion::getJrUrl(array('examenes', 'publicados')), false);
            }
            $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
            $this->documento->script('circle', '/libs/graficos/progressbar/');

            $_idexamen=!empty(NegSesion::get('idexamencur','_examen_'))?NegSesion::get('idexamencur','_examen_'):0;
            $this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:0;
            if(empty($this->idexamen))$this->idexamen=$_idexamen;
            $filtros["idexamen"]=$this->idexamen; 
            if($rol!='superadmin'){
				$filtros["idpersonal"]=$usuarioAct["idusuario"];
			}
			$filtros['idproyecto'] = $usuarioAct['proyecto']['idproyecto'];
            $this->examen=$this->oNegExamenes->buscar($filtros);
            $this->alumnos=$this->oNegPersonal->buscar();
            $this->habilidades=@json_decode($this->examen["habilidades_todas"], true);

            if(!empty($this->examen)){
                $filter['idexamen'] = $this->idexamen;
                $exam_alumnos =$this->oNegExamen_alumno->buscar($filter);
                $this->cantAprob = 0;
                $this->cantDesaprob = 0;
                foreach ($exam_alumnos as $exam_alu) {
                    if($exam_alu['puntaje']>$this->examen[0]['calificacion_min']){
                        $this->cantAprob += 1;
                    }else{
                        $this->cantDesaprob += 1;
                    }
                }
            }

            $this->documento->setTitulo(JrTexto::_('Exams').' /'.JrTexto::_('reports'), true);
            $this->esquema = 'examenes/reports';
            $this->documento->plantilla = 'examenes/inicio';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}

	public function imprimir(){
		try {
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
            $rol=$usuarioAct["rol"];
            if( strtolower ($rol) == strtolower ('Alumno')){
            	return $aplicacion->redir(JrAplicacion::getJrUrl(array('examenes', 'publicados')), false);
            }
		    $this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:0;
		    $filtros["idexamen"]=$this->idexamen;
			$filtros['idproyecto'] = $usuarioAct['proyecto']['idproyecto'];
		    if($rol=='alumno'){
				$filtros["estado"]=1;
				$this->esquema = 'alumno/examenes';
			}else{
				if($rol!='superadmin'){
					$filtros["idpersonal"]=$usuarioAct["idusuario"];
				}			
		    	$this->esquema = 'examenes/imprimir';
			}
		    $exam=$this->oNegExamenes->buscar($filtros);
		   	if(!empty($exam)){
				$this->examen=$exam[0];
				$filtros_preg["idexamen"]=$this->idexamen;
				$filtros_preg["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
				$this->oNegpreguntas->setLimite(0,9999);
				$this->preguntas=$this->oNegpreguntas->mostrarPreguntas($filtros_preg);

                /*this->exanivel=$this->oNegNiveles->buscar(array('tipo'=>'N','idnivel'=>$this->examen["idnivel"]));
                $this->exanunidad=$this->oNegNiveles->buscar(array('tipo'=>'U','idnivel'=>$this->examen["idunidad"]));
                $this->exaactividad=$this->oNegNiveles->buscar(array('tipo'=>'L','idnivel'=>$this->examen["idactividad"]));*/
                /*$this->exatype= $this->tipoexamen=$this->oNegExamen_tipo->buscar(array('idtipo'=>$this->examen["tipo"]));*/
			}else{
				throw new Exception(JrTexto::_('Exam does not exist').'!');
			}
			$this->documento->plantilla = 'modal';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

    public function resolver() //antes teacherresrc_view()
    {
        try {
            global $aplicacion;
            /* librerias */
             $this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/');
             $this->documento->script('cronometro', '/libs/chingo/');
             $this->documento->script('mitimer', '/libs/chingo/');
             $this->documento->script('jquery-ui.min', '/tema/js/');
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('jquery-confirm.min', '/libs/alert/');
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('jquery.md5', '/tema/js/');
             $this->documento->stylesheet('circletimer', '/libs/circletimer/');
             $this->documento->script('circletimer.min', '/libs/circletimer/');

             $this->documento->script('multiplantilla', '/js/examen/');
             $this->documento->script('true_false', '/js/examen/');
             $this->documento->script('order_simple', '/js/examen/');
             $this->documento->script('order_paragraph', '/js/examen/');
             $this->documento->script('join', '/js/examen/');
             $this->documento->script('tag_image', '/js/examen/');
             $this->documento->script('editactividad','/js/new/');
             $this->documento->script('actividad_completar','/js/new/');
             $this->documento->script('funciones','/tema/js/');
             $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
             $this->documento->script('circle', '/libs/graficos/progressbar/');

             $this->documento->script('alu_preview', '/js/examen/');
            
            $this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:-1;
            $filtros["idexamen"]=$this->idexamen;

            $this->nro_intento = 0;
            $this->cant_intentos = 5;
            $this->max_intento = 9999;
            $this->usuarioAct = NegSesion::getUsuario();
            #var_dump($this->usuarioAct);

            $rol=$this->usuarioAct["rol"];
			$filtros['idproyecto'] = $this->usuarioAct['proyecto']['idproyecto'];

            if(strtolower($rol)==strtolower('Alumno')){
                $filtros["estado"]=1;
                $exams_alum = $this->oNegExamen_alumno->buscar(array( 'idexamen'=>$this->idexamen, 'idalumno'=>$this->usuarioAct['idusuario'], 'order_by'=>array('intento DESC') ));
                if(!empty($exams_alum)){
                	$this->nro_intento = (int)$exams_alum[0]['intento'];
                }
                $this->nro_intento++;
                $this->documento->plantilla = 'verblanco'; //'examenes/general';
                $this->esquema = 'examenes/alu_preview';
            }else{
                $this->documento->plantilla = 'verblanco';
                $this->esquema = 'examenes/preview';
            }
			$exam=$this->oNegExamenes->buscar($filtros);
			//var_dump($filtros);
            if(!empty($exam)){
                $this->examen=$exam[0];
            }else{
                throw new Exception(JrTexto::_('Exam does not exist').'!');
            }
	        $this->habilidades=json_decode($this->examen['habilidades_todas'], true);

            if($this->nro_intento<=(int)$this->examen['nintento']){
            	/**
            	* si el numero de intentos del ultimo registro  es <= a la cant de intentos, podrá resolver su examen
            	**/
	            $filtros_preg["idexamen"]=$this->idexamen;
	            $filtros_preg["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
	            $this->oNegpreguntas->setLimite(0,9999);
	            $this->preguntas=$this->oNegpreguntas->mostrarPreguntas($filtros_preg);

	            if(empty($this->preguntas)){
	                throw new Exception(JrTexto::_("There are no question in this exam").".");
	            }
            }else{
            	/**
            	* cargar plantilla de resultado del examen
            	**/
            	$this->resultado = $this->setResultadoExam($this->examen, $exams_alum);
                $this->documento->plantilla = 'verblanco';
                $this->esquema = 'examenes/solo_resultado';
            }

            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

	/* ========================== Funciones AJAX ========================== */
	public function guardar()
	{
		$this->documento->plantilla = 'returnjson';
        try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            global $aplicacion;         
            $usuarioAct = NegSesion::getUsuario(); 			
				
			if(!empty($idexamen)){
				$this->oNegExamenes->idexamen = $idexamen;
				$accion="editar";
			}
			if(@$calificacionen==='A') $txttotal=@$txtAlfanumerico;
			else $txttotal=@$calificaciontotal;
			$txtportada=@str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',$portada);
			/*$this->oNegExamenes->__set('idnivel',@$nivel);
			$this->oNegExamenes->__set('idunidad',@$unidad);
			$this->oNegExamenes->__set('idactividad',@$actividad);*/
			$this->oNegExamenes->__set('titulo',@$titulo);
			$this->oNegExamenes->__set('descripcion',@$descripcion);
			$this->oNegExamenes->__set('portada',$txtportada);
			$this->oNegExamenes->__set('fuente',@$fuente);
			$this->oNegExamenes->__set('fuentesize',@$fuentesize);
			$this->oNegExamenes->__set('tipo',@$tipo);
			/*$this->oNegExamenes->__set('grupo',@$engrupo);*/
			$this->oNegExamenes->__set('aleatorio',@$aleatorio);
			$this->oNegExamenes->__set('calificacion_por',@$calificacionpor);
			$this->oNegExamenes->__set('calificacion_en',@$calificacionen);			
			$this->oNegExamenes->__set('calificacion_total',$txttotal);
			$this->oNegExamenes->__set('calificacion_min',!empty($calificacionmin)?$calificacionmin:0);
			$this->oNegExamenes->__set('tiempo_por',@$tiempopor);
			$this->oNegExamenes->__set('tiempo_total',@$tiempototal);
			$this->oNegExamenes->__set('estado',!empty($estado)?$estado:0);
			$this->oNegExamenes->__set('nintento',@$nintento);
			$this->oNegExamenes->__set('calificacion',@$calificacion);
			$this->oNegExamenes->__set('habilidades_todas',@$habilidades_todas);
			$this->oNegExamenes->__set('origen_habilidades', (@$tipo=='G')?'JSON':@$origen_habilidades);
			$this->oNegExamenes->__set('fuente_externa',@$fuente_externa);
			$this->oNegExamenes->__set('tiene_certificado',@$tiene_certificado);
			$this->oNegExamenes->__set('idproyecto',@$usuarioAct["proyecto"]["idproyecto"]);
			
			/* Guardar el archivo de certificado */
			if(!empty(@$nombre_certificado)){
				$file = $_FILES["file_certificado"];
				$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
				$tipo_permitido=array('jpg', 'jpeg','gif','png');
				if(!in_array($ext, $tipo_permitido)){
					$msjeError = 'El certificado no tiene extensión de imagen.';
				}
				try{
					$dir_certificados = RUTA_BASE.'static'.SD.'img'.SD.'certificados'.SD ;
					@mkdir($dir_certificados, 0775, true);
					@chmod($dir_certificados, 0775);
					$nombrefile=$file["name"];
					if(move_uploaded_file($file["tmp_name"], $dir_certificados.$nombrefile)) {
						$this->oNegExamenes->__set('nombre_certificado', str_replace(RUTA_BASE, '__xRUTABASEx__'.SD, $dir_certificados.$nombrefile) );
				   		$msjeError = null;
				  	}
				}catch(Exception $e) {
					$msjeError = 'No se pudo subir el Certificado.';
				}
			}

		    if(@$accion=="editar"){
				$res=$this->oNegExamenes->editar();
			}else{
				$this->oNegExamenes->__set('idpersonal',@$usuarioAct["idusuario"]);
				$res=$this->oNegExamenes->agregar();
		    }
		    @NegSesion::set('idexamencur', $res, '_examen_');
			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Assessment')).' '.JrTexto::_('saved successfully'),'new'=>$res)); //
            exit(0);				
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e->getMessage() ));
            exit(0);
		} 		
	}

	public function guardarPreguntas()
	{
		$this->documento->plantilla = 'returnjson';
        try {

            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            global $aplicacion;         
            $usuarioAct = NegSesion::getUsuario(); 			
				
			if(!empty($idexamen)&&!empty($idpregunta)){			
				$res=$this->oNegpreguntas->idpregunta = $idpregunta;
				$accion="editar";
			}
			$ejercicio=@trim(str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',@$ejercicio));
			$this->oNegpreguntas->__set('idexamen',$idexamen);
			$this->oNegpreguntas->__set('pregunta',@$pregunta);
			$this->oNegpreguntas->__set('descripcion',@$descripcion);
			$this->oNegpreguntas->__set('ejercicio',@$ejercicio);
			$this->oNegpreguntas->__set('idpadre',@$idpadre);
			$this->oNegpreguntas->__set('tiempo',@$tiempo);
			$this->oNegpreguntas->__set('puntaje',@$puntaje);
			$this->oNegpreguntas->__set('template',@$template);
			$this->oNegpreguntas->__set('habilidad',@$habilidad);
			$this->oNegpreguntas->__set('idcontenedor',@$idcontenedor);
			$this->oNegpreguntas->__set('dificultad',@$dificultad);

		    if(@$accion=="editar"){
				$res=$this->oNegpreguntas->editar();
			}else{
				$this->oNegpreguntas->__set('idpersonal',@$usuarioAct["idusuario"]);
				$res=$this->oNegpreguntas->agregar();
		    }
		    $this->actualizarDificultadExam($idexamen);
			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Question')).' '.JrTexto::_('saved successfully'),'new'=>$res)); //
            exit(0);				
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		} 	
	}

	public function buscarpreguntasjson()
	{
		$this->documento->plantilla = 'returnjson';
        try {
        	$filtros["idexamen"]=!empty($_REQUEST["exa"])?$_REQUEST["exa"]:1;
        	$filtros["orden"]=!empty($_REQUEST["orden"])?$_REQUEST["orden"]:0; //0 asc, 1 aleatorio;
        	$this->oNegpreguntas->setLimite(0,9999);
        	$preguntas=$this->oNegpreguntas->mostrarPreguntas($filtros);
			echo json_encode(array('code'=>'ok','preguntas'=>$preguntas)); //
            exit(0);				
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		} 
		try {
            global $aplicacion;
            $this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.md5', '/tema/js/');
            $this->documento->stylesheet('circletimer', '/libs/circletimer/');
            $this->documento->script('circletimer.min', '/libs/circletimer/');

            $this->documento->script('multiplantilla', '/js/examen/');
            $this->documento->script('true_false', '/js/examen/');
            $this->documento->script('order_simple', '/js/examen/');
            $this->documento->script('order_paragraph', '/js/examen/');
            $this->documento->script('join', '/js/examen/');
            $this->documento->script('tag_image', '/js/examen/');
            $this->documento->script('editactividad','/js/new/');
            $this->documento->script('actividad_completar','/js/new/');
            $this->documento->script('funciones','/tema/js/');
            $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
            $this->documento->script('circle', '/libs/graficos/progressbar/');
            $usuarioAct = NegSesion::getUsuario();
            $rol=$usuarioAct["rol"];
            $_idexamen=!empty(NegSesion::get('idexamencur','_examen_'))?NegSesion::get('idexamencur','_examen_'):0;
            $this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:0;
            if(empty($this->idexamen))$this->idexamen=$_idexamen;
            else NegSesion::set('idexamencur', $this->idexamen, '_examen_');
            $filtros["idexamen"]=$this->idexamen;
			$filtros['idproyecto'] = $usuarioAct['proyecto']['idproyecto'];
            $this->documento->plantilla = 'examenes/inicio';
            if($rol=='alumno'){
                $filtros["estado"]=1;
                $this->esquema = 'alumno/examenes';
            }else{
                $filtros["idpersonal"]=$usuarioAct["idusuario"];              
                $this->esquema = 'examenes/preview';
            }
            $exam=$this->oNegExamenes->buscar($filtros);
            if(!empty($exam)){
                $this->examen=$exam[0];
                $filtros_preg["idexamen"]=$this->idexamen;
                $filtros_preg["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
                $this->oNegpreguntas->setLimite(0,9999);
                $this->preguntas=$this->oNegpreguntas->mostrarPreguntas($filtros_preg);
                /*$this->exanivel=$this->oNegNiveles->buscar(array('tipo'=>'N','idnivel'=>$this->examen["idnivel"]));
                $this->exanunidad=$this->oNegNiveles->buscar(array('tipo'=>'U','idnivel'=>$this->examen["idunidad"]));
                $this->exaactividad=$this->oNegNiveles->buscar(array('tipo'=>'L','idnivel'=>$this->examen["idactividad"]));*/
                /*$this->exatype= $this->tipoexamen=$this->oNegExamen_tipo->buscar(array('idtipo'=>$this->examen["tipo"]));*/
            }else{
                throw new Exception(JrTexto::_('Exam does not exist').'!');
            }
            $this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}

	public function duplicarPreguntas()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('incomplete data to duplicate')));
                exit(0);            
            }

			@extract($_POST);

			$response = $this->oNegpreguntas->duplicar($idexamen, $idpreguntas);

			//actualizar Habilidades_todas del examen actual($idexamen)
			$this->actualizar_habilidades($idexamen);

			//actualizar Dificultad del examen actual($idexamen)
			$this->actualizarDificultadExam($idexamen);

			echo json_encode(array('code'=>'ok','data'=>$response));
            exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		}
	}

    public function buscar_exams()
    {
        $this->documento->plantilla = 'returnjson';
        try{
            global $aplicacion;
            $usuarioAct = NegSesion::getUsuario();
            $this->documento->setTitulo(JrTexto::_('Exams'), true);
            $filtros=array();
            $filtros["titulo"]=!empty($_POST["txtBuscar"])?$_POST["txtBuscar"]:'';
            $filtros["idnivel"]=!empty($_POST["nivel"])?$_POST["nivel"]:0;
            $filtros["idunidad"]=!empty($_POST["unidad"])?$_POST["unidad"]:0;
            $filtros["idactividad"]=!empty($_POST["actividad"])?$_POST["actividad"]:0;
            $filtros["estado"]=1;
			$filtros['idproyecto'] = $usuarioAct['proyecto']['idproyecto'];
            //$filtros["tipo"]=!empty($_POST["tipo"])?$_POST["tipo"]:0;
            //$filtros["idpersonal"]=$usuarioAct["idusuario"];
            //print_r($filtros);
            $examenes=$this->oNegExamenes->buscar($filtros);
            $data=array('code'=>'ok','data'=>$examenes);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }

    public function promedioxalumno($idalumno=null){
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST["id"]) && $idalumno==null) { 
                throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $filtros["idalumno"]=($idalumno==null)?$_POST["id"]:$idalumno;
            $promXHab = [];
            $exams_alum=$this->oNegExamen_alumno->buscar($filtros);
            $sumaPuntaje = 0.0;
            foreach ($exams_alum as $e) {
                $this->oNegExamenes->idexamen=$e['idexamen'];
                $exam=$this->oNegExamenes->getXid();
                $jsonHab = json_decode($e['puntajehabilidad'],true);
                foreach ($jsonHab as $idHab=>$ptjeObtenido) {
                    if(!isset($promXHab[$idHab])){
                        $promXHab[$idHab] = [];
                        $promXHab[$idHab] = [
                            'suma'=>0.0,
                            'cant'=>0,
                            'promedio'=>0.0,
                        ];
                    }
                    $promXHab[$idHab]['suma']+=(float) $ptjeObtenido;
                    $promXHab[$idHab]['cant']+=1;
                    $promXHab[$idHab]['promedio']=$promXHab[$idHab]['suma']/$promXHab[$idHab]['cant'];
                }
                $calif_max = (float) $exam['calificacion_total'];
                $pntje_base100 = $e['puntaje']*100/$calif_max;
                $sumaPuntaje +=$pntje_base100;
            }
            $promExamenes = [
                'promPuntaje'=>(count($exams_alum)>0)?$sumaPuntaje/count($exams_alum):0.0,
                'promXHab'=>$promXHab,
            ];
            if($idalumno==null){
                $data=array('code'=>'ok','data'=>$promExamenes);
                echo json_encode($data);
            } else {
                return $promExamenes;
            }
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }

	public function eliminarxPadre()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('incomplete data to delete block')));
                exit(0);            
            }
			@extract($_POST);

            $filtros=array();

			$res=$this->oNegpreguntas->eliminarxcontenedor(array('idexamen'=> $idexamen,'idcontenedor'=>$idpadre));
			
			//actualizar Habilidades_todas del examen actual($idexamen)
			$this->actualizar_habilidades($idexamen);

			//actualizar Dificultad del examen actual($idexamen)
			$this->actualizarDificultadExam($idexamen);

			echo json_encode(array('code'=>'ok','data'=>$res));
            exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		} 
	}

	public function get_result()
	{
		try {
			global $aplicacion;
			$idalumno = !empty($_REQUEST["user_id"])?$_REQUEST["user_id"]:null;
			$idexamen = !empty($_REQUEST["exam_id"])?$_REQUEST["exam_id"]:null;
			$filtros = array(
				"idexamen" => $idexamen,
				"idalumno" => $idalumno,
			);
			$filtros['idproyecto'] = $usuarioAct['proyecto']['idproyecto'];
            $exam_alum = $this->oNegExamen_alumno->buscar($filtros);
            $examen = $this->oNegExamenes->buscar($filtros);
            if(!empty($examen)){
            	$examen = $examen[0];
            }
            
            $resultado = $this->setResultadoExam($this->examen, $exams_alum);
            if(empty($resultado)){
            	$response = array('code'=>'Error', 'msg'=>JrTexto::_('Something went wrong'), 'data'=>array() );
            }else{
            	$response = array('code'=>'Success', 'msg'=>JrTexto::_('Results of exam'), 'data'=>$resultado );
            }
            
            echo json_encode($response);
		} catch (Exception $e) {
			$data=array('code'=>'Error','msg'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
		}
		exit(0);
	}

	public function jxExamRendidos()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;	
			if(strtolower($this->usuarioAct['rol'])=='alumno'){ throw new Exception(JrTexto::_("You must not be here")); }

			$filtros=array();

			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["identificador"])&&@$_REQUEST["identificador"]!='')$filtros["identificador"]=$_REQUEST["identificador"];
			if(isset($_REQUEST["idexamen"])&&@$_REQUEST["idexamen"]!='')$filtros["idexamen"]=$_REQUEST["idexamen"];

			$this->oNegExamenes->setLimite(0,99999);
			$data=$this->oNegExamenes->buscarRendidos($filtros);

			$response=array('code'=>'ok', 'data'=>$data, 'msg'=>"success" );
            echo json_encode($response);
			exit(0);
		} catch (Exception $e) {
			$response=array('code'=>'Error','msg'=>JrTexto::_($ex->getMessage()));
            echo json_encode($response);
			exit(0);
		}
	}

	/* ========================== Funciones xAJAX ========================== */
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegExamenes->__set('idexamen', $pk);
				$res=$this->oNegExamenes->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminarPregunta(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegpreguntas->idpregunta=$pk;
				$idExamen = $this->oNegpreguntas->idexamen;
				$res=$this->oNegpreguntas->eliminar();

				//actualizar Habilidades_todas del examen actual($pk)
				$this->actualizar_habilidades($idExamen);

				//actualizar Dificultad del examen actual($pk)
				$this->actualizarDificultadExam($idExamen);

				if(!empty($res))
					$oRespAjax->setReturnValue($pk);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegExamenes->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xSetCampoPregunta(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$this->oNegpreguntas->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	/*public function xEliminarxPadre(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$res=$this->oNegpreguntas->eliminarxcontenedor($pk);
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			}catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}*/

	/* ========================== Funciones Privadas ========================== */
    private function setResultadoExam($examen=null, $arrExamenes_alum=null)
    {
    	if($examen==null && $arrExamenes_alum==null){ return null; }
    	$datos = $resultado = array();
    	$calificacion = $examen['calificacion']; // U=Último  ;  M=Mejor
    	foreach ($arrExamenes_alum as $exam) {
	    	if($calificacion=='U'){
	    		$datos[ $exam['intento'] ] = $exam;
	    	}
	    	if($calificacion=='M'){
	    		$datos[ $exam['puntaje'] ] = $exam;
	    	}
    	}
    	ksort($datos); // ordena de menor a mayor array de clave=>valor.
    	$resultado = array_pop($datos); //extrae el último elemento de $datos
    	return $resultado;
    }

	private function actualizarDificultadExam($idExamen=null)
	{
		try {
			global $aplicacion;
			if(empty($idExamen)){ 
				throw new Exception(JrTexto::_("Error updating Exam difficulty"));
			}
			$this->oNegpreguntas->setLimite(0,9999);
			$exam_preg=$this->oNegpreguntas->buscar(array('idexamen'=>$idExamen));
			if(empty($exam_preg)){ return false; }
			$suma = $cont = 0;
			foreach ($exam_preg as $preg) {
				$dificultad = (int)$preg['dificultad'];
				if($dificultad>0){
					$suma += $dificultad;
					$cont++;
				}
			}
			if($cont==0){ return false; }
			$promedio = round($suma/$cont);
			$resp=$this->oNegExamenes->setCampo($idExamen, 'dificultad_promedio', $promedio);
			return $resp;
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function actualizar_habilidades($idexamen=null)
	{
		try {
			if(empty($idexamen)){  throw new Exception(JrTexto::_("Error updating Exam::habilidades_todas")); }
			$arrHabilidadesTodas = $arrHabExamActual = array();
			$preguntas = $this->oNegpreguntas->buscar(array("idexamen"=>$idexamen));
			foreach ($preguntas as $k => $preg) {
				$arrSkills = explode('|', $preg['habilidades']);
				foreach ($arrSkills as $val) {
					if( !in_array($val, $arrHabExamActual)){ array_push($arrHabExamActual, $val); }
				}
			}
			$origen_hab = 'JSON';
			foreach ($preguntas as $i => $p) {
				//obtener de que pregunta se ha duplicado.
				$exam_origen = null;
				if(empty($p['idpregunta_origen'])) {
					$exam_origen = $this->oNegExamenes->buscar(array("idexamen"=>$p['idexamen']));
					$habilidades_todas = json_decode($exam_origen[0]['habilidades_todas'], true);
					$origen_hab = $exam_origen[0]['origen_habilidades'];
					foreach ($habilidades_todas as $index => $hab) {
						if( !in_array($hab['skill_id'], $arrHabExamActual)){ array_push($arrHabExamActual, $hab['skill_id']); }
					}
				} else {
					$this->oNegpreguntas->setLimite(0,9999);
					$pregunta_origen = $this->oNegpreguntas->buscar(array("idpregunta"=>$p['idpregunta_origen']));
					if(!empty($pregunta_origen)){
						$pregunta_origen = $pregunta_origen[0];
						//obtener de que examen viene esa pregunta_origen
						$exam_origen = $this->oNegExamenes->buscar(array("idexamen"=>$pregunta_origen['idexamen']));
					}
				}

				//del examen obtenido extraer las habilidades y agregarlas a $arrHabilidadesTodas
				if(!empty($exam_origen)){
					$exam_origen = $exam_origen[0];
					$habilidades_origen = json_decode($exam_origen['habilidades_todas'], true);

					if(!empty($habilidades_origen)){
						foreach ($habilidades_origen as $i => $h) {
							if( in_array($h['skill_id'], $arrHabExamActual) && !in_array($h, $arrHabilidadesTodas)){ $arrHabilidadesTodas[]=$h; }
						}
					}
				}
			}
			$this->oNegExamenes->setCampo($idexamen, 'origen_habilidades', $origen_hab); /* <-- por si al caso */
			return $this->oNegExamenes->setCampo($idexamen, 'habilidades_todas', json_encode($arrHabilidadesTodas));
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

}