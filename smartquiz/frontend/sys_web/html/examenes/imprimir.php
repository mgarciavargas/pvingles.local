<?php
$examen = $this->examen;
$preguntas=@$this->preguntas;
$rutabase = $this->documento->getUrlBase();
$srcPortada = (!empty($examen['portada']!=''))? str_replace('__xRUTABASEx__', $rutabase, $examen['portada']) : '';
$tipoEvaluacion = '';
if($examen['calificacion_en']=='N') $tipoEvaluacion = 'pts.';
if($examen['calificacion_en']=='P') $tipoEvaluacion = '%';
$calif_minima = $examen["calificacion_min"];
$calif_total = $examen["calificacion_total"];
if($examen['calificacion_en']=='A'){
    $calif_minima = 51;
    $jsonEscalas = json_decode($examen["calificacion_total"]);

    /*foreach ($jsonEscalas as $k=>$val) {
        $calif_total .= '<div>'+$k+': '+$val+'</div>';
    }*/
}
if($examen['dificultad_promedio']=='1'){ $dificultad='easy'; }
else if($examen['dificultad_promedio']=='2'){ $dificultad='medium'; }
else if($examen['dificultad_promedio']=='3'){ $dificultad='difficult'; }
?>
<div class="row">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_verdad_falso.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_ordenar.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_fichas.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_imagen_puntos.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_completar.css">
<style>
    .contenedor-preguntas{
        overflow: hidden;
        border-bottom: 2px solid #bbb;
    }
    .contenedor-preguntas:last-child{border: none;}

    #imprimir-view .h1, #imprimir-view .h2, #imprimir-view .h3, #imprimir-view h1, #imprimir-view h2, #imprimir-view h3 {
        margin-top: 0;
        margin-bottom: 0;
    }
    
    #imprimir-view .break-word{
        word-break: break-word;
        padding-top: 7px;
    }
    /*estilos de impresion*/
    @media print{
        .contenedor-preguntas{
            page-break-after: always;
            break-after: always;
        }
        /* **** Join (Fichas) **** */
        .plantilla.plantilla-fichas .ejerc-fichas .ficha {
            width: 23%;
        }
    
        /* **** Verdadero o Falso **** */
        .plantilla.plantilla-verdad_falso .premise>div:first-child{
            width: 60%;
        }
        .plantilla.plantilla-verdad_falso .premise>.options{
            width: 40%;
            float: right;
        }

        /* **** Ordenar Parrafo **** */
        .plantilla.plantilla-ordenar.ord_parrafo .drag-parr,
        .plantilla.plantilla-ordenar.ord_parrafo .drop-parr{
            width: 50%;
        }

        /* **** Ordenar Parrafo **** */
        .plantilla.plantilla-img_puntos .contenedor-img{
            width: 75%;
        }
        .plantilla.plantilla-img_puntos .tag-alts{
            width: 25%;
        }
        .plantilla.plantilla-img_puntos .dot-container>.dot{
            background: #fff;
            border: 5px solid #4c5e9b;
        }
        .plantilla.plantilla-img_puntos .dot-container>.dot:after{
            border:  3px solid #efefef;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }
    }
</style>

    <div class="col-md-12">
        <div class="panel panel-primary" id="imprimir-view">
            <div class="panel-heading">
            	<h3 class="panel-title"><?php echo ucfirst(@$this->exatype[0]["tipo"]); ?></h3>
            </div>
            <div class="panel-body row"  style="font-family: '<?php echo $examen['fuente']; ?>'; font-size: <?php echo $examen['fuentesize'].'px'; ?>;">
                <div id="blocker-exercise" style="position: absolute; height: 100%; background: rgba(255,255,255,0); width: 100%; z-index: 10; "></div>

            <?php if(!empty($examen)){?>
                <div class="col-xs-12 encabezado">
                    <div class="col-xs-12 text-center title_desc-zone">
                        <h2><?php echo ucfirst($examen["titulo"]); ?></h2>
                        <small><?php echo ucfirst($examen["descripcion"]); ?></small>
                        
                    </div>
                    <div class="form-horizontal ">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo ucfirst(JrTexto::_("full name")); ?>:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                     <!--
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label class="col-sm-6 control-label"><?php echo ucfirst(JrTexto::_("Level")); ?>:</label>
                                <div class="col-sm-6 break-word"><?php echo @$this->exanivel[0]["nombre"]; ?></div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label class="col-sm-6 control-label"><?php echo ucfirst(JrTexto::_("Unit")); ?>:</label>
                                <div class="col-sm-6 break-word"><?php echo @$this->exanunidad[0]["nombre"]; ?></div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label class="col-sm-6 control-label"><?php echo ucfirst(JrTexto::_("Activity")); ?>:</label>
                                <div class="col-sm-6 break-word"><?php echo @$this->exaactividad[0]["nombre"]; ?></div>
                            </div>
                        </div>
                     -->
                        <?php if($examen['tiempo_por']=='E'){ ?>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label class="col-sm-6 control-label"><?php echo ucfirst(JrTexto::_("time")); ?>:</label>
                                <div class="col-sm-6 break-word"><?php echo $examen['tiempo_total']; ?></div>
                            </div>
                        </div>
                        <?php }?>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label class="col-sm-6 control-label"><?php echo ucfirst(JrTexto::_("Minimum score")); ?>:</label>
                                <div class="col-sm-6 break-word"><?php echo $calif_minima.$tipoEvaluacion; ?></div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label class="col-sm-6 control-label"><?php echo ucfirst(JrTexto::_("Maximum score")); ?>:</label>
                                <div class="col-sm-6 break-word"><?php echo $calif_total.$tipoEvaluacion; ?></div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label class="col-sm-6 control-label"><?php echo ucfirst(JrTexto::_("Difficulty")); ?>:</label>
                                <div class="col-sm-6 break-word"><?php echo ucfirst(JrTexto::_($dificultad)); ?></div>
                            </div>
                        </div>
                    </div>
                    <hr class="col-xs-12">
                </div>
            <?php } 
            if(!empty($preguntas))
            foreach ($preguntas as $pregunta){ $codeidgui=uniqid(); ?>
                <div class="col-xs-12 contenedor-preguntas">
                    <?php
                    if($pregunta["F"]){
                        $preg=$pregunta["F"][0];
                        $tpltipo=$preg["template"];
                        $tpl=($tpltipo=='image'||$tpltipo=='audio'||$tpltipo=='video')?'media':'ejerc';
                    ?>
                    <div class="col-xs-12 tmpl tmpl-<?php echo $tpl; ?>" data-tipo="<?php echo $tpltipo; ?>" data-cls="<?php echo $tpltipo; ?>_question" data-idpregunta="<?php echo $preg["idpregunta"]; ?>" data-habilidades="<?php echo $preg["habilidades"]; ?>" style="padding: 0 20%;">                       
                    	<?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$preg["ejercicio"]); ?>
                    </div>
                    <?php } if($pregunta["P"]){
                    foreach ($pregunta["P"] as $preg) {
                        $tpltipo=$preg["template"];
                         $tpl=($tpltipo=='image'||$tpltipo=='audio'||$tpltipo=='video')?'media':'ejerc';?>
                        <div class="col-xs-12 tmpl tmpl-<?php echo $tpl; ?>" data-tmpl="<?php echo $tpltipo; ?>" data-cls="<?php echo $tpltipo; ?>_question" data-idpregunta="<?php echo $preg["idpregunta"]; ?>" data-habilidades="<?php echo $preg["habilidades"]; ?>" data-tiempo="<?php echo $preg["tiempo"]; ?>" data-puntaje="<?php echo $preg["puntaje"]; ?>">
                        	
                            <?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$preg["ejercicio"]); ?>
                        </div>
                    <?php } } ?>
                </div>
            <?php } ?> 

            </div>
            
        </div>
    </div>
</div>