<?php
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
$html_edicion = $this->pregunta['ejercicio'];
$rutabase = $this->documento->getUrlBase();
?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_imagen_puntos.css">

<div class="plantilla plantilla-img_puntos editando" id="tmp_<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>" data-clone="#panelEjercicio">
    <input type="hidden" value="<?php echo $this->pregunta['dificultad']; ?>"  name="hPregDificultad" id="hPreguntaDificultad">
    <div id="panelEjercicio">
      <input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
      <div class="row">
        <div class="col-xs-12 botones-edicion nopreview">
            <a class="btn btn-primary selimage" data-tipo="image" data-url=".img-dots">
                <i class="fa fa-image" ></i> 
                <?php echo ucfirst(JrTexto::_('choose')).' '.JrTexto::_('image'); ?> 
            </a>
            <a class="btn btn-primary start-tag"> 
                <i class="fa fa-dot-circle-o"></i>
                <?php echo ucfirst(JrTexto::_('start tagging')); ?> 
            </a>
            <a class="btn btn-red stop-tag" style="display: none;">
                <i class="fa fa-stop"></i> 
                <?php echo ucfirst(JrTexto::_('stop tagging')); ?> 
            </a>
            <a class="btn btn-danger delete-tag pull-right">
                <i class="fa fa-trash-o"></i> 
                <?php echo ucfirst(JrTexto::_('delete tag')); ?>s 
            </a>
            <a class="btn btn-danger stop-del-tag pull-right" style="display: none;">
                <i class="fa fa-stop"></i> 
                <?php echo ucfirst(JrTexto::_('stop deleting tag')); ?>s 
            </a>
            <a class="btn btn-success back-edit-tag hidden" style="display: none !important;">
                <i class="fa fa-pencil"></i> 
                <?php echo ucfirst(JrTexto::_('back').' '.JrTexto::_('and').' '.JrTexto::_('edit')); ?>
            </a>

            <div class="hidden dot-container edition clone_punto" data-id="clone_punto<?php echo $idgui; ?>">
                <div class="dot"></div>
                <div class="dot-tag"> </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-9 contenedor-img">
            <picture>
                <img src="" class="img-responsive valvideo img-dots" style="display: none;">
                <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/noimage.png" class="img-responsive nopreview" style="width: initial; margin:0 auto;">
            </picture>
            <div class="mask-dots">
            </div>
        </div>

        <div class="col-xs-12 col-md-3 tag-alts-edit nopreview"> </div>

        <div class="col-xs-12 col-md-3 tag-alts" style="display: none;">
            <div class="tag" id="tag_123456789">
                <div class="arrow"></div>
                Una etiqueta.
            </div>
        </div>
      </div>
    </div>
    <a id="generarhtml" class="btn btn-success generate-tag hidden" style="display: none !important;">
        <i class="fa fa-rocket"></i> 
        <?php echo ucfirst(JrTexto::_('finish').' '.JrTexto::_('and').' '.JrTexto::_('generate')); ?>
    </a>
</div>


<section id="sectionPreCarga" class="hidden" style="display: none !important;">
<?php 
if($html_edicion!=''){ 
    echo str_replace('__xRUTABASEx__', $rutabase, $html_edicion);
} ?>
</section>


<script>
$(document).ready(function() {
    var $plantilla = $('#tmp_<?php echo $idgui; ?>');
    var isEditando = $plantilla.hasClass('editando');
    var ALTO_DotContainer = ANCHO_DotContainer = 40; /* .dot-container width and height = 40px */

    var calcularPosicionPunto = function(e){
        var widthImg= $('.img-dots', $plantilla)[0].getBoundingClientRect().width,
            heightImg= $('.img-dots', $plantilla)[0].getBoundingClientRect().height,
            offset = $('.mask-dots.start-tagging', $plantilla).offset();        
        var posX = e.pageX - offset.left - (ALTO_DotContainer/2),
            posY = e.pageY - offset.top  - (ANCHO_DotContainer/2);
        var posicPorcentaje = {
            'x' : (posX*100)/widthImg ,
            'y' : (posY*100)/heightImg ,
        };
        return posicPorcentaje;
    };

    var crearPunto = function(e, idCloneFrom, idCloneTo){
        var now = Date.now();
        var posicion = calcularPosicionPunto(e);
        var newPunto = $(idCloneFrom, $plantilla).clone();
        newPunto.removeClass('clone_punto').removeClass('hidden').attr('id', 'dot_'+now).removeAttr('data-id');
        newPunto.css({
            left: posicion.x+'%',
            top: posicion.y+'%',
        });
        var cant = $(idCloneTo, $plantilla).children().length;
        newPunto.find('.dot-tag').html(cant+1);
        $(idCloneTo, $plantilla).append(newPunto);
        return { 'cant':(cant+1), 'now': now};
    };

    var crearInput = function(nroPunto, now, placeholder){
        var htmlInput='<div class="input-group" id="edit_tag_'+now+'">'+
                        '<span class="input-group-addon">'+nroPunto+'</span>'+
                        '<input type="text" class="form-control" placeholder="'+placeholder+'">'+
                    '</div>';
        $('.tag-alts-edit', $plantilla).append(htmlInput);
    };

    var renameTags = function($containerDots, $containerInputs){
        var i = 0;
        $containerDots.find('.dot-container').each(function() {
            i++;
            var indexId = $(this).attr('id').split('_')[1];
            $(this).find('.dot-tag').text(i);
            $containerInputs.find('#edit_tag_'+indexId+' span.input-group-addon').text(i);
        });
    };

    var eliminarTag = function( $tag, $containerInputs ){
        var $containerDots = $tag.parent();
        var id = $tag.attr('id');
        if(id!='' && id!=undefined) {
            var index = id.split('_')[1];
            $tag.remove();
            $containerInputs.find('#edit_tag_'+index).remove();
            renameTags($containerDots, $containerInputs);
        }
    };

    var agregarRandom = function(html, $aDonde, index){
        var $child = $aDonde.children();
        if( $child.length == 0 ){
            $aDonde.html(html);
        } else {
            var posic = Math.floor(Math.random()*$child.length);
            while( posic == index ){
                posic = Math.floor(Math.random()*$child.length);
            }
            var aleat = ($child.length == 1) ? 2 : Math.floor(Math.random()*10);
            if(aleat%2 == 0) { $child.eq(posic).before(html); }
            else { $child.eq(posic).after(html); }
        }
    };

    var generarTags = function($containerInputs, $containerTags){
        $containerTags.html('');
        $containerInputs.find('.input-group').each(function() {
            var index = $containerInputs.find('.input-group').index( $(this) );
            var id = $(this).attr('id');
            var indexId = id.split('_')[id.split('_').length-1];
            var value = $(this).find('input').val();
            var number = $(this).find('span').text();
            $(this).find('input').attr('value', value);
            var newTag = '<div class="tag" id="tag_'+indexId+'" data-number="'+number+'">'+
                            '<div class="arrow"></div>'+ value +
                        '</div>';

            agregarRandom(newTag, $containerTags, (index+1));

        });
    };

/* * * * Edicion de Tag_Image * * * */
    $('.plantilla-img_puntos')
    .on('click', '.botones-edicion .selimage', function(e){
        if(isEditando==false) return false;
        e.preventDefault();
        var txt = MSJES_PHP.select_upload;
        selectedfile(e,this,txt);
    })
    .on('click', '.botones-edicion .start-tag', function(e){        
        e.preventDefault();
        $('.mask-dots', $plantilla).addClass('start-tagging');
        $(this).hide();
        $(this).siblings('.btn').addClass('disabled');
        $('.stop-tag', $plantilla).show().removeClass('disabled');
    })
    .on('click', '.botones-edicion  .stop-tag', function(e) {
        e.preventDefault();
        $('.mask-dots', $plantilla).removeClass('start-tagging');
        $(this).hide();
        $(this).siblings('.btn').removeClass('disabled');
        $('.start-tag', $plantilla).show().removeClass('disabled');
    })
    .on('click', '.botones-edicion  .delete-tag', function(e) {
        e.preventDefault();
        if(isEditando==false) return false;
        $('.mask-dots', $plantilla).addClass('delete-tagging');
        $(this).hide();
        $(this).siblings('.btn').addClass('disabled');
        $('.stop-del-tag', $plantilla).show().removeClass('disabled');
    })
    .on('click', '.botones-edicion  .stop-del-tag', function(e) {
        e.preventDefault();
        $('.mask-dots', $plantilla).removeClass('delete-tagging');
        $(this).hide();
        $(this).siblings('.btn').removeClass('disabled');
        $('.delete-tag', $plantilla).show().removeClass('disabled');
    })
    .on('click', '.contenedor-img .mask-dots.start-tagging', function(e) {
        var resp = crearPunto(e, '.clone_punto', '.mask-dots');
        crearInput(resp.cant, resp.now, MSJES_PHP.write_tag);
    })
    .on('click', '.contenedor-img .mask-dots.delete-tagging .dot', function(e) {
        eliminarTag( $(this).parent(), $('.tag-alts-edit', $plantilla) );
    })
    .on('click', '.contenedor-img .mask-dots .dot-container.edition>.dot', function(e) {
        var indexId = $(this).parent().attr('id').split('_')[1];
        $('#edit_tag_'+indexId+' input', $plantilla).focus();
    })
    .on('focusin', '.tag-alts-edit input[type="text"]', function(e) {
        var indexId = $(this).parent().attr('id').split('_').pop();
        $('.mask-dots #dot_'+indexId, $plantilla).addClass('hover');
    })
    .on('focusout', '.tag-alts-edit input[type="text"]', function(e) {
        e.stopPropagation();
        $('.mask-dots .dot-container', $plantilla).removeClass('hover');
    })
    .on('click', '.generate-tag', function(e) {
        e.preventDefault();
        if(isEditando==false) return false;
        $plantilla.removeClass('editando');
        $('.botones-edicion', $plantilla).hide();
        //$('.back-edit-tag', $plantilla).show();
        generarTags($('.tag-alts-edit', $plantilla), $('.tag-alts', $plantilla));
        $('.tag-alts-edit', $plantilla).hide();
        $('.mask-dots .dot-container', $plantilla).each(function() {
            $(this).removeClass('edition');
            $(this).find('.dot-tag').attr('data-number', $(this).find('.dot-tag').text() );
        });
        
        if($('.mask-dots', $plantilla).hasClass('delete-tagging')){ $('.stop-del-tag', $plantilla).trigger('click') }
        if($('.mask-dots', $plantilla).hasClass('start-tagging')){ $('.stop-tag', $plantilla).trigger('click'); }

        $('.tag-alts', $plantilla).show();
        $('.mask-dots', $plantilla).addClass('playing');
    })
    .on('click', '.back-edit-tag', function(e) {
        e.preventDefault();
        if(isEditando==false) return false;
        //$plantilla.addClass('editando');
        $('.botones-edicion', $plantilla).show();
        //$('.btn.stop-tag, .btn.stop-del-tag').hide();
        $('.tag-alts', $plantilla).hide();

        $('.mask-dots .dot-container', $plantilla).each(function() {
            $(this).addClass('edition');
            $(this).find('.dot-tag').text( $(this).find('.dot-tag').attr('data-number') );
            var numero=$(this).find('.dot-tag').attr('data-number');
            $(this).find('.dot-tag').removeAttr('data-number').removeClass('corregido good bad');
            $(this).removeClass('hover');
            //$(this).attr('id', 'dot_'+ANS_TAG[idgui][numero]);
        });
        $('.tag-alts-edit', $plantilla).show();
        $('.mask-dots', $plantilla).removeClass('playing');
    });

    var existeHtml = <?php echo ($html_edicion!='')? 'true':'false'  ?>;
    if(existeHtml){
        var precarga = $('#sectionPreCarga').clone();
        var html = precarga.find('.ejercicio').html();
        $('#panelEjercicio').html(html); /*evita recargar el titulo y desrcip infinitas veces*/
        $('.back-edit-tag', $plantilla).trigger('click')
        $('#sectionPreCarga').remove();
    } else {
        $('.selimage', $plantilla).trigger('click');
    }
});
</script>