<?php $usuarioActivo=NegSesion::getUsuario(); 
$usuariodni=@$usuarioActivo["dni"];
$usuarioRol=@$usuarioActivo["rol"];
?>
<style>
	.portada{
		overflow: hidden;
	}
	.portada img{
		height: 150px;
	}
</style>

<div class="row" id="btns-creacion">
	<div class="col-xs-12 col-sm-4">
		<a href="<?php echo $this->documento->getUrlBase() ?>/examenes/ver" class="btn btn-block btn-success verexamen">
			<div><i class="fa fa-plus fa-4x"></i></div>
			<h3 class="bolder"><?php echo ucfirst(JrTexto::_("New")) ?></h3>
			<p><?php echo ucfirst(JrTexto::_("Create a new assessment")) ?>.</p>
		</a>
	</div>
	<div class="col-xs-12 col-sm-4">
		<a href="<?php echo $this->documento->getUrlBase() ?>/examenes/ver/?acc=generar" class="btn btn-block btn-blue verexamen">
			<div>
				<span class="fa-stack fa-lg fa-2x" >
					<i class="fa fa-file-o fa-stack-2x"></i>
					<i class="fa fa-cog fa-stack-1x"></i>
				</span>
			</div>
			<h3 class="bolder"><?php echo ucfirst(JrTexto::_("Generated")) ?></h3>
			<p><?php echo ucfirst(JrTexto::_("You can select questions from other assessments")) ?>.</p>
		</a>
	</div>
	<div class="col-xs-12 col-sm-4">
		<a href="<?php echo $this->documento->getUrlBase() ?>/examenes/ver/?acc=ubicacion" class="btn btn-block btn-yellow verexamen">
			<div><i class="fa fa-compass fa-4x"></i></div>
			<h3 class="bolder"><?php echo ucfirst(JrTexto::_("Placement")) ?></h3>
			<p><?php echo ucfirst(JrTexto::_("It gives you results of the student's initial knowledge")) ?>.</p>
		</a>
	</div>
</div>
<div class="panel panel-info" id="pnl-listado">
	<div class="panel-heading">
		<!--a href="<?php echo $this->documento->getUrlBase() ?>/examenes/ver" class="verexamen btn btn-success"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_("Add")) ?></a> &nbsp;
		<a href="<?php echo $this->documento->getUrlBase() ?>/examenes/ver/?acc=generar" class="verexamen btn btn-blue">
			<span class="fa-stack fa-lg" style="font-size: 0.6em;">
				<i class="fa fa-file-o fa-stack-2x"></i>
				<i class="fa fa-cog fa-stack-1x"></i>
			</span>
			<?php echo ucfirst(JrTexto::_("Generate")) ?>
		</a-->
		<div class="col-xs-6 col-sm-4 pull-right">
			<div class="input-group">
				<input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="<?php echo ucfirst (JrTexto::_("Search")); ?>...">
				<span role="button" class="input-group-addon btn buscar-exam"><i class="fa fa-search"></i></span>
			</div>
		</div>
	</div>
	<div class="panel-body pnllisexamenes">
		<?php  if(!empty($this->examenes)){
		foreach ($this->examenes as $e){
			if(!empty($e["portada"]))
				$portada=@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$e["portada"]);
			else
				$portada=$this->documento->getUrlStatic().'/media/imagenes/levels/a1.png';
			if($usuariodni==$e["idpersonal"] || $usuarioRol=='superadmin') {
				$ruta='/examenes/ver/?idexamen='.$e["idexamen"];
			} else {
				$ruta='/examenes/resolver/?idexamen='.$e["idexamen"];
			}
		?>
		<div class="col-xs-6 col-sm-3 col-md-2" style="padding: 0.7ex">
			<a class="" href="<?php echo $this->documento->getUrlBase().$ruta;?>"  title="<?php echo $e["titulo"]; ?>">
				<div class="exa-item">
					<div class="titulo"><?php echo $e["titulo"]; ?></div>
					<div class="portada"><img class="img-responsive center-block" src="<?php echo $portada;?>"></div>
				</div>
			</a>
		</div>
		<?php } 
		} else { ?>
			<h3 class="text-center col-xs-12">
				<i class="fa fa-ban fa-3x"></i>
				<p class="col-xs-12"><?php echo ucfirst(JrTexto::_('there are no').' '.JrTexto::_('assessments')) ?></p>
				<p class="col-xs-12"><?php echo JrTexto::_('Click on "Add" button to start') ?></p>
			</h3>
		<?php } ?>	
	</div>	
</div>

<script type="text/javascript">
$(document).ready(function(){
	var restaurarListado = function(){
		$('#level-item').val('');
		$('#unit-item').val('');
		$('#activity-item').val('');
		$('#tipo-examen').val('')
		$('#txtTitulo').val('');
		cargarexamenes();
	};
	var cargarexamenes=function(){
		var msjeAttention = '<?php echo JrTexto::_('Attention') ?>';
		var dniuser='<?php echo $usuariodni ?>';
		var nivel=$('#level-item').val()||0;
		var unidad=$('#unit-item').val()||0;
		var actividad=$('#activity-item').val()||0;
		var tipo=$('#tipo-examen').val()||0;
		var titulo=$('#txtTitulo').val()||'';
		var url=_sysUrlBase_+'/examenes/publicadosjson';
		try{
			var formData = new FormData();
			formData.append("nivel", nivel);
			formData.append("unidad", unidad);
			formData.append("actividad", actividad);
			formData.append("tipo", tipo);
			formData.append("titulo", titulo);
			$.ajax({
				url: url,
				type: "POST",
				data:  formData,
				contentType: false,
				dataType :'json',
				cache: false,
				processData:false,
				beforeSend: function(XMLHttpRequest){
					$('.pnllisexamenes').html('<div style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/img/sistema/loading.gif"><br><span style="font-size: 1.2em; font-weight: bolder; color: #006E84;"><?php echo ucfirst(JrTexto::_("loading")); ?>...</span></div>')
				},
				success: function(data)
				{
					if(data.code==='ok'){
						var btnLimpiarBusqueda = '<button class="btn btn-default limpiar-busqueda">Limpiar búsqueda</button>';
						var examenes=data.data;
						html='';
						if(examenes)
							$.each(examenes,function(i,obj){
								if(obj.portada){
									var portada=obj.portada.replace(/__xRUTABASEx__/gi,_sysUrlBase_);
								} else {
									var portada=_sysUrlBase_+'/img/sistema/examen_default.png';
								}
								if(dniuser==obj.idpersonal){ var ruta='/examenes/ver/?idexamen='+obj.idexamen; }
								else { var ruta='/examenes/resolver/?idexamen='+obj.idexamen; }
								//var ruta='/examenes/resolver/?idexamen='+obj.idexamen;

								html+='<div class="col-xs-6 col-sm-3 col-md-2 " style="padding: 0.7ex">';
								html+='<a href="'+_sysUrlBase_+ruta+'" title="'+obj.titulo+'">';
								html+='<div class="exa-item">';
								html+='<div class="titulo">'+obj.titulo+'</div>';
								html+='<div class="portada"><img class="img-responsive" width="100%" src="'+portada+'"></div>';
								html+='</div></a></div>';
							});
						if(html!=''){
							$('.pnllisexamenes').html(html);
						}else{
							$('.pnllisexamenes').html('<div class="col-xs-12 text-center"><h3><?php echo JrTexto::_("No data for this search"); ?></h3></div>');
						}	
						if( $('#txtTitulo').val()!='' ){
							$('.pnllisexamenes').append('<div class="col-xs-12 text-center">'+btnLimpiarBusqueda+'</div>');
						}
					}else{
						mostrar_notificacion(msjeAttention, data.mensaje, 'warning');
					}
					$('#procesando').hide('fast');
					return false;
				},
				error: function(xhr,status,error){
					mostrar_notificacion(msjeAttention, status, 'warning');
					$('#procesando').hide('fast');
					return false;
				}               
			});
		}catch(error){
			mostrar_notificacion(msjeAttention, status, 'warning');
		}
	};
	var leerniveles=function(data){
		try{
			var res = xajax__('', 'niveles', 'getxPadre', data);
			if(res){ return res; }
			return false;
		}catch(error){
			return false;
		}       
	};
	var addniveles=function(data,obj){
		var objini=obj.find('option:first').clone();
		obj.find('option').remove();
		obj.append(objini);
		if(data!==false){
			var html='';
			$.each(data,function(i,v){
				html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
			});
			obj.append(html);
		}
		id=obj.attr('id');
		if(id==='activity-item')	cargarexamenes();
	};

	/*$('#level-item').change(function(){
		var idnivel=$(this).val();
		var data={tipo:'U','idpadre':idnivel}
		var donde=$('#unit-item');
		if(idnivel!=='') addniveles(leerniveles(data),donde);
		else addniveles(false,donde);
		donde.trigger('change');
	});
	$('#unit-item').change(function(){
		var idunidad=$(this).val();
		var data={tipo:'L','idpadre':idunidad}
		var donde=$('#activity-item');
		if(idunidad!=='') addniveles(leerniveles(data),donde);
		else addniveles(false,donde);
	});
	$('#activity-item').change(function(){
		cargarexamenes();
	});
	$('#tipo-examen').change(function(){
		cargarexamenes();
	});*/
	$('.btn.buscar-exam').click(function(e) {
		cargarexamenes();
	});
	$('body').on('keyup', '#txtTitulo', function(e) {
		if(e.which == 13){
			$(".btn.buscar-exam").trigger('click');
		}
	}).on('click', '.btn.limpiar-busqueda', function(e) {
		e.preventDefault();
		restaurarListado();
	});

	/*$('.panel').on('click','.verexamen',function(){
		var nivel=$('#level-item').val();
		var unidad=$('#unit-item').val();
		var actividad=$('#activity-item').val();
		var tipo=$('#tipo-examen').val();
		redir(_sysUrlBase_+'/examenes/ver/'+nivel+'/'+unidad+'/'+actividad+'/');			
	});*/

	showexamen('home');
});
</script>