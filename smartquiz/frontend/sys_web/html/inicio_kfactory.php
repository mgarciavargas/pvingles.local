<?php 
$menu = array(
	array('nombre' => ucfirst(JrTexto::_('area')), 'link'=>'/cargos/?nivel=1', 'icon'=>'fa-th-large', 'descripcion'=>''),
	array('nombre' => ' Sub-'.ucfirst(JrTexto::_('area')), 'link'=>'/cargos/?nivel=2', 'icon'=>'fa-th', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Obligatory courses')), 'link'=>'/curso_obligado', 'icon'=>'fa-book', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Register')).' '.ucfirst(JrTexto::_('administrator')), 'link'=>'/usuario', 'icon'=>'fa-lock ', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Register')).' '.ucfirst(JrTexto::_('student')), 'link'=>'/alumno', 'icon'=>'fa-graduation-cap ', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Reports')), 'link'=>'/reportes', 'icon'=>'fa-bar-chart', 'descripcion'=>''),
); ?>
<div class="col-xs-12">
	<div class="panel">
		<div class="panel-heading">
			<h1>
				<i class="fa fa-user"></i> <?php echo ucfirst(JrTexto::_('administrative module')); ?>
				<small>
					<i class="fa fa-angle-double-right"></i>
					<?php echo $this->usuarioAct['nombre_full'] ?>
				</small>
			</h1>
		</div>

		<div class="panel-body">
			<div class="row"><div class="col-xs-12" style="padding: 0;">
				<?php foreach ($menu as $m) { ?>
				<div class="col-xs-6 col-sm-6 col-md-3 btn-panel-container panelcont-xs">
					<a href="<?php echo $this->documento->getUrlBase().$m['link']; ?>" class="btn btn-block btn-turquesa btn-panel">
						<i class="btn-icon fa <?php echo $m['icon']; ?>"></i>
						<?php echo $m['nombre']; ?>
						<!--span class="color-red-dark border-red-dark badge">4</span-->
					</a>
				</div>
				<?php }?>
			</div></div>
		</div>
	</div>
</div>