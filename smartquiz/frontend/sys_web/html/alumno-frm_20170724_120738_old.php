<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Alumno'); ?><small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkDni" id="pkdni" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtApe_paterno">
              <?php echo JrTexto::_('Ape paterno');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtApe_paterno" name="txtApe_paterno" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["ape_paterno"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtApe_materno">
              <?php echo JrTexto::_('Ape materno');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtApe_materno" name="txtApe_materno" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["ape_materno"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('Nombre');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFechanac">
              <?php echo JrTexto::_('Fechanac');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFechanac" name="txtFechanac" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["fechanac"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtSexo">
              <?php echo JrTexto::_('Sexo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtSexo" name="txtSexo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["sexo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado_civil">
              <?php echo JrTexto::_('Estado civil');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtEstado_civil" name="txtEstado_civil" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["estado_civil"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtUbigeo">
              <?php echo JrTexto::_('Ubigeo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtUbigeo" name="txtUbigeo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["ubigeo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtUrbanizacion">
              <?php echo JrTexto::_('Urbanizacion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtUrbanizacion" name="txtUrbanizacion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["urbanizacion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDireccion">
              <?php echo JrTexto::_('Direccion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDireccion" name="txtDireccion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["direccion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTelefono">
              <?php echo JrTexto::_('Telefono');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTelefono" name="txtTelefono" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["telefono"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCelular">
              <?php echo JrTexto::_('Celular');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtCelular" name="txtCelular" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["celular"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEmail">
              <?php echo JrTexto::_('Email');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtEmail" name="txtEmail" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["email"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdugel">
              <?php echo JrTexto::_('Idugel');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdugel" name="txtIdugel" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idugel"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRegusuario">
              <?php echo JrTexto::_('Regusuario');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtRegusuario" name="txtRegusuario" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["regusuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRegfecha">
              <?php echo JrTexto::_('Regfecha');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtRegfecha" name="txtRegfecha" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["regfecha"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtUsuario">
              <?php echo JrTexto::_('Usuario');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtUsuario" name="txtUsuario" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["usuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtClave">
              <?php echo JrTexto::_('Clave');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtClave" name="txtClave" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["clave"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtToken">
              <?php echo JrTexto::_('Token');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtToken" name="txtToken" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["token"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFoto">
              <?php echo JrTexto::_('Foto');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFoto" name="txtFoto" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["foto"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtEstado" name="txtEstado" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["estado"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtSituacion">
              <?php echo JrTexto::_('Situacion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtSituacion" name="txtSituacion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["situacion"];?>">
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveAlumno" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('alumno'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Alumno', 'saveAlumno', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Alumno"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Alumno"))?>');
        <?php endif;?>       }
     }
  });







  
  
});


</script>

