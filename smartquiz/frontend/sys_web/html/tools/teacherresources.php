<?php defined('RUTA_BASE') or die(); 
$idgui = uniqid(); 
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
$vocAdmin=null;
$vocdocente=null;
$voc=null;
$img=null;
$vid=null;
$audios=null;
$pdf1=null;
$vocabul=null;
$ejerc=null;
$games=null;
$exam=null;
$publicado=0;
$compartido=0;
$editando=false;
$orden=$this->orden;
$idrecurso='';
if(!empty($this->datos)){
 foreach ($this->datos as $voc){        
        $vocAdmin=$voc;        
        if($voc["idpersonal"]===$usuarioAct["dni"]&&$rolActivo!=1){
            $vocdocente=$voc;
        }
        if(!empty($voc["publicado"])){
            $publicado=$voc["publicado"];
        }
        if(!empty($voc["compartido"])){
            $compartido=$voc["compartido"];
        }
        if(!empty($voc["idrecurso"])){
            $idrecurso=$voc["idrecurso"];
        }
    }   
    $editando=true;
}
if(!empty($this->datosi)) $img=$this->datosi[0]; 
if(!empty($this->datosv)) $vid=$this->datosv[0];
if(!empty($this->datosVoc)) $vocabul=$this->datosVoc[0];
if(!empty($this->datosp)) $pdf1=$this->datosp[0];
if(!empty($this->datosE)) $ejerc=$this->datosE[0];
if(!empty($this->datosG)) $games=$this->datosG[0];
if(!empty($this->datosX)) $exam=$this->datosX[0];
if(!empty($this->datosA)) $audios=$this->datosA[0];

$rutabase = $this->documento->getUrlBase();
$txtini='description';
 if(empty($vocAdmin)){
    $vocAdmin["texto"]=$txtini;
    }
  if(empty($vocdocente)){
    $vocdocente["texto"]=$txtini;
    }
$nivel = $this->idnivel;
$unidad = $this->idunidad;
$actividad = $this->idactividad;
?>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlStatic() ?>/libs/resource/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/css/tool_teacherresources.css">

<div id="teacher_resrc-tool" class="editando" data-idautor="<?php echo $voc['idpersonal']; ?>">
    <div class="botones-generales text-center <?php echo $editando?'editando':''; ?>" data-id="<?php echo $idrecurso; ?>" >
        <a class="btn btn-info preview" href="#">
            <i class="fa fa-eye"></i> <?php echo JrTexto::_("Preview") ?>
        </a>
        <a class="btn btn-info back"  href="#" style="display: none;">
            <i class="fa fa-arrow-left"></i> <?php echo JrTexto::_("Back") ?>
        </a>
        <a href="#" class="btn btn-success <?php echo $publicado==1?'':'btn-inactive'; ?> btnpublicar"><i class="fa fa-address-book-o"></i> <?php echo ucfirst(JrTexto::_('post')) ?></a>
        <a href="#" class="btn btn-primary <?php echo $compartido==1?'':'btn-inactive'; ?> btncompartir"><i class="fa fa-globe"></i> <?php echo ucfirst(JrTexto::_('share')) ?></a>
    </div>
    <table class="cuaderno" cellpadding="0" cellspacing="0">
        <tr>
            <td class="cuaderno-izq" background="<?php echo $this->documento->getUrlStatic() ?>/libs/resource/images/cuadernillo1.png?1" style="background-repeat:no-repeat" width="74"></td>
            <td class="cuaderno-contenido" background="<?php echo $this->documento->getUrlStatic() ?>/libs/resource/images/cuadernillo2.png?2" style="background-repeat:repeat; padding-right: 15px;" align="center">

                <div id="div_presentacion" class="row panel-resource aquihtml" style="background-image: url('<?php echo @str_replace('__xRUTABASEx__', $rutabase, $voc["caratula"]) ;?>'); ">
                    <div style="padding-right: 12px;padding-left: 12px;">
                        <div class="col-xs-12 controles-edicion" style="display: block;">      
                            <div class="btn btn-primary edithtml "><i class="fa fa-text-width"></i> <?php echo JrTexto::_('Edit text') ?></div>
                            <div class="btn btn-primary savehtml " style="display: none;"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save text') ?></div>
                        </div>

                        <h1 class="col-xs-12 txttitulo div_fondo" style=""><?php echo $voc["titulo"] ;?></h1>

                        <div class="col-xs-12 txtpresentacion div_fondo"><?php echo $voc["texto"] ;?></div>

                        <div class="col-xs-12 div_txttitulo" id="div_txttitulo" style="display: none;">

                            <label class="control-label col-md-2" for="txtTitulo">
                                <?php echo JrTexto::_('Titulo');?>
                            </label>
                            <input type="text"  id="txtTitulo" name="txtTitulo" required="required" class="form-control" value="<?php echo $voc["titulo"] ;?>" >

                            <textarea class="txtpresentacionedit" id="txtarea_pnl1<?php echo $idgui; ?>" style="display: none;">
                                <?php echo @@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$vocdocente["texto"]); ?>
                            </textarea>
<!--
                            <input type="checkbox" class="checkbox-ctrl col-md-6" name="chk" id="chk">
                            <label class="control-label" for="txtPublicar" style="float: left">
                                <?php echo JrTexto::_('Publicar');?>
                            </label>
-->
                            <a class="biblioteca-portada btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a image from our multimedia library or upload your own video')) ?>" data-tipo="image" data-url=".imgtemporal" style="display:">
|                               <i class="fa fa-picture-o"></i> 
                                <?php echo ucfirst(JrTexto::_('select')).' '.JrTexto::_('image'); ?>
                            </a>
                            <img src="<?php echo @str_replace('__xRUTABASEx__', $rutabase, $voc["caratula"]) ;?>" class="imgtemporal hidden">

                        </div>
                    </div>
                    <div class="creado-por div_fondo">
                        <?php echo JrTexto::_("Created by") ?>: 
                        <span><?php echo $usuarioAct['nombre_full'] ;?></span>
                    </div>
                </div>

                <div id="div_imagen" class="row panel-resource" style="display: block; padding:0 15px;">
                    <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("Images") ?></h1>
                    <div class="col-xs-12 controles-edicion nopreview">
                        <a class="open-biblioteca btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a image from our multimedia library or upload your own video')) ?>" data-tipo="image" data-url=".imgtemporal">
                            <i class="fa fa-picture-o"></i> 
                            <?php echo ucfirst(JrTexto::_('select')).' '.JrTexto::_('image') ?>
                        </a>
                        <img class="hidden imgtemporal temp" src="" style="display: none;">
                    </div>
                    <div class="row">
                        <div class="col-xs-12 image-selecc contenedor-slider">
                            <div class="myslider" style="width:90%; border: solid 0px #f00; ">
                                   <?php
                                   //echo $img["caratula"];
                                   $imgs = explode(",", $img["caratula"]);
                                   $canttext = explode(",", $img["texto"]);
                                   if ($img["caratula"]){
                                        $cantimg=count($imgs)-1;
                                    }else{
                                        $cantimg=-1;
                                   }
                                   
                                   for ($y=0;$y<=$cantimg;$y++){                                    
                                        echo '<a href="#" class="miniatura miniaturai" > <span class="del-miniatura nopreview"></span><img src="'.@str_replace('__xRUTABASEx__', $rutabase, $imgs[$y]).'" alt="image" style="height: 86px; width: 100%" data-tit="'.@$canttext[$y].'" data-pos="'.$y.'" ></a>';
                                   }
                                   ?>

                            </div>
                        </div>                        
                        <div class="col-xs-12 mascara_video">
                            <img class="image_resrc img-responsive img-thumbnail" src="" style="display: none; height: 300px;">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/noimage.png" class="img-responsive nopreview mask" style="/*width: 100%;height: 400px*/">
                        </div>
                        <div>
                            <input type="text"  id="txtDesc" name="txtDesc" required="required" class="form-control descripcion-media hidden" value="" style="width: 50%;" >
                            <div class="btn btn-primary saveimage nopreview hidden"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save Image') ?></div>

                            <input type="hidden"  id="txtpos" name="txtpos" required="required" value="">
                            <input type="hidden" class="contenido-caratula" id="txtimg" name="txtimg" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, $img["caratula"]); ?>">
                            <input type="hidden" class="contenido-texto" id="txtTit" name="txtTit" required="required" class="form-control" value="<?php echo $img["texto"]?>">
                        </div>
                    </div>
                </div>

                <div id="div_videos" class="row panel-resource" style="display: block">
                    <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("Videos") ?></h1>
                    <div class="col-xs-12 controles-edicion nopreview">
                        <a class="open-biblioteca btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a video from our multimedia library or upload your own video')) ?>" data-tipo="video" data-url=".vidtemporal">
                            <i class="fa fa-video-camera"></i>
                            <?php echo ucfirst(JrTexto::_('select')) ?> video
                        </a>
                        <video class="hidden vidtemporal temp" src="" style="display: none"></video>
                    </div>
                    <div class="col-xs-12 video-selecc contenedor-slider">
                        <div class="myslider">
                            <?php
                               //echo $img["caratula"];
                               $vids = explode(",", $vid["caratula"]);
                               $canttextv = explode(",", $vid["texto"]);
                               if ($vid["caratula"]){
                                    $cantvid=count($vids)-1;
                                }else{
                                    $cantvid=-1;
                               }
                               
                               for ($y=0;$y<=$cantvid;$y++){                                    
                                    echo '<a href="#" class="miniatura miniaturav" ><span class="del-miniatura nopreview"></span><video src="'.@str_replace('__xRUTABASEx__', $rutabase, $vids[$y]).'" alt="video" style="height: 86px; width: 100%" data-tit="'.$canttextv[$y].'" data-pos="'.$y.'" id="video'.$y.'"> </video></a>';
                               }
                               ?>
                        </div>
                    </div>

                    <div class="col-xs-12 mascara_video">
                        <video controls="true" class="valvideo video_resrc" src="" style="display: none;width: 100%;height: 300px;"></video>
                        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/novideo.png" class="img-responsive nopreview mask" style="/*width: 100%;height: 300px;*/">

                    </div>
                    <div style="/*width: 100%; position: relative; top:300px;*/">
                        <input type="text"  id="txtDescv" name="txtDescv" required="required" class="form-control descripcion-media hidden" value="" style="width: 50%" >
                        <div class="btn btn-primary savevideo nopreview hidden"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save Video') ?></div>

                        <input type="hidden"  id="txtposv" name="txtposv" required="required" value="">
                        <input type="hidden" class="contenido-caratula" id="txtvideo" name="txtvideo" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, $vid["caratula"]); ?>">
                        <input type="hidden" class="contenido-texto" id="txtTitv" name="txtTitv" required="required" class="form-control" value="<?php echo $vid["texto"]?>">
                    </div>
                </div>

                <div id="div_audios" class="row panel-resource" style="display: block">
                    <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("Audios") ?></h1>
                    <div class="col-xs-12 controles-edicion nopreview">
                        <a class="open-biblioteca btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select an audio from our multimedia library or upload your own audio')) ?>" data-tipo="audio" data-url=".audtemporal">
                            <i class="fa fa-video-camera"></i>
                            <?php echo ucfirst(JrTexto::_('select')) ?> audio
                        </a>
                        <audio class="hidden audtemporal temp" src="" style="display: none"></audio>
                    </div>
                    <div class="col-xs-12 audio-selecc contenedor-slider">
                        <div class="myslider">
                            <?php
                               //echo $img["caratula"];
                               $auds = explode(",", $audios["caratula"]);
                               $canttextA = explode(",", $audios["texto"]);
                               if ($audios["caratula"]){
                                    $cantvid=count($auds)-1;
                                }else{
                                    $cantvid=-1;
                               }
                               
                               for ($y=0;$y<=$cantvid;$y++){
                                    echo '<a href="#" class="miniatura miniatura_a"><span class="del-miniatura nopreview"></span><span href="#" class="file" src="'.@str_replace('__xRUTABASEx__', $rutabase, $auds[$y]).'" alt="audio" style="height: 86px; width: 100%" data-pos="'.$y.'" data-tit="'.$canttextA[$y].'" id="img'.$y.'"><i class="fa fa-file-audio-o fa-4x"></i></span></a>';
                               }
                               ?>
                        </div>
                    </div>

                    <div class="col-xs-12 mascara_video">
                        <audio controls="true" class="valvideo audio_resrc" src="" style="display: none;width: 100%;"></audio>
                        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/noaudio.png" class="img-responsive nopreview mask" style="/*width: 100%;height: 300px;*/">

                    </div>
                    <div style="/*width: 100%; position: relative; top:300px;*/">
                        <input type="text"  id="txtDesca" name="txtDesca" required="required" class="form-control descripcion-media hidden" value="" style="width: 50%" >
                        <div class="btn btn-primary saveaudio nopreview hidden"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save Audio') ?></div>

                        <input type="hidden"  id="txtposa" name="txtposa" required="required" value="">
                        <input type="hidden" class="contenido-caratula" id="txtaudio" name="txtaudio" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, $audios["caratula"]); ?>">
                        <input type="hidden" class="contenido-texto" id="txtTita" name="txtTita" required="required" class="form-control" value="<?php echo $audios["texto"]?>">
                    </div>
                </div>

                <div id="div_pdfs" class="row panel-resource" style="display: block">
                    <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("PDF") ?></h1>
                    <div class="col-xs-12 controles-edicion nopreview">
                        <a class="open-biblioteca btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a file from our multimedia library or upload your own video')) ?>" data-tipo="pdf" data-url=".ifrtemporal">
                            <i class="fa fa-file-pdf-o"></i> 
                            <?php echo ucfirst(JrTexto::_('select')).' '.JrTexto::_('file') ?>
                        </a>
                        <iframe class="hidden ifrtemporal temp" src="" style="display: none"></iframe>
                    </div>
                    <div class="col-xs-12 pdf-selecc contenedor-slider">
                        <div class="myslider">
                            <?php
                               //echo $img["caratula"];
                               $pdfs = explode(",", $pdf1["caratula"]);
                               $canttextp = explode(",", $pdf1["texto"]);
                               if ($pdf1["caratula"]){
                                    $cantpdf=count($pdfs)-1;
                                }else{
                                    $cantpdf=-1;
                               }

                               for ($y=0;$y<=$cantpdf;$y++){
                                    echo '<a href="#" class="miniatura miniaturap" ><span class="del-miniatura nopreview"></span><span class="file" src="'.@str_replace('__xRUTABASEx__', $rutabase, $pdfs[$y]).'" alt="pdf" style="height: 86px; width: 100%" data-tit="'.@$canttextp[$y].'" data-pos="'.$y.'" id="pdf'.$y.'"><i class="fa fa-file-pdf-o fa-4x"></i><span class="nombre-file">'.@$canttextp[$y].'</span></span></a>';
                               }
                            ?>
                        </div>
                    </div>                        
                    <div class="col-xs-12 mascara_video">
                        <iframe src="" frameborder="0" class="pdf_resrc" style="width: 100%; height: 375px; display: none;"></iframe>
                        <!--<img src="<?php echo $this->documento->getUrlStatic() ?>/media/web/pdf_file.jpg" class="img-responsive nopreview mask" style="/*width: 100%;height: 300px;*/">-->
                        <div class="mask">
                            <img src="<?php echo $this->documento->getUrlStatic() ?>/media/web/pdf_file.jpg" class="img-responsive center-block <?php echo !empty($pdf)?'hidden':''; ?>">
                            <span class="<?php echo !empty($pdf)?'hidden':''; ?>"><?php echo JrTexto::_('there is no file to display') ?></span>
                        </div>
                    </div>                        

                    <div style="/*width: 100%;position: relative; top:300px;*/" >
                        <input type="hidden"  id="txtDescp" name="txtDescp" required="required" class="form-control descripcion-media hidden" value="" style="width: 50%" >
                        <!--
                        <div class="btn btn-primary savepdf nopreview hidden" style="display:;"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save PDF') ?></div>
                        -->

                        <input type="hidden" id="txtposp" name="txtposp" required="required" value="">
                        <input type="hidden" class="contenido-caratula" id="txtpdf" name="txtpdf" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, $pdf1["caratula"]); ?>">
                        <input type="hidden" class="contenido-texto" id="txtTitp" name="txtTitp" required="required" class="form-control" value="<?php echo $pdf1["texto"]?>">

                    </div>
                </div>

                <div id="div_voca" class="row panel-resource" style="display: none">
                    <div class="col-xs-12">
                        <h1 class="title-resrc"><?php echo JrTexto::_("Vocabulary") ?></h1>
                        <div class="col-xs-12 form-inline frmDiccionario nopreview">
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control palabra" placeholder="<?php echo ucfirst(JrTexto::_('search'));?>...">
                                <span class="input-group-btn">
                                    <a href="#" class="btn btn-default search-word btn-block"><i class="fa fa-search"></i></a>
                                </span>
                            </div>
                        </div>
                        
                        <ul class="col-xs-12 result-busqueda">
                            <?php
                            $arrVocabul = json_decode($vocabul['caratula'], true);
                            if(!empty($arrVocabul)){
                                foreach ($arrVocabul as $k=>$v) {
                                    ?>
                                    <a class="col-xs-12 list-group-item palabra_vocab active" data-id="id_<?php echo $k; ?>">
                                        <div  class="col-xs-offset-1 col-xs-11">
                                            <h4>
                                                <span class="word"><?php echo $v['palabra']; ?></span>
                                                <small class="type"><?php echo $v['tipo']; ?></small>
                                            </h4>
                                            <p class="def"><?php echo $v['definicion']; ?></p>
                                        </div>
                                    </a>
                                    <?php
                                }
                            } else { ?>
                                <div class="nopreview"><?php echo ucfirst(JrTexto::_('There are no words to display'));?>.</div>
                            <?php } ?>
                        </ul>

                    </div>
                </div>

                <div id="div_act" class="row panel-resource" style="display: none">
                    <style type="text/css">
                        #pnlactividadesresourses .selejericio div{
                            margin: 0.3ex;
                            position: relative;
                            cursor: pointer;
                        }

                        #pnlactividadesresourses .selejericio.active div::before{
                            position: absolute;
                            right: 0.5ex;
                            content: "\2713"
                        }
                    </style>
                    <h1 class="title-resrc"><?php echo ucfirst(JrTexto::_("Exercise")) ?>s</h1>
                    <div>
                        <form class="form-inline">
                            <div class="input-group">
                              <input type="text" id="txtsearchactividad" class="form-control" placeholder="<?php echo JrTexto::_('Search'); ?>" aria-describedby="basic-addon1">
                              <span class="input-group-addon btnsearchactividad" id="basic-addon1"><i class="fa fa-search"></i></span>
                          </div>
                        </form>
                    </div>   
                    <div class="row">
                        <div class="col-md-12" id="pnlactividadesresourses">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="pnlveractividadesresourses">
                            <a class="col-xs-12 col-sm-2 btn btn-primary hidden save ejercicio pull-right nopreview"><i class="fa fa-save"></i> <?php echo ucfirst(JrTexto::_("save")).' '.JrTexto::_("exercise") ?>s</a>
                            <div class="col-xs-10 col-sm-10 pull-right">
                                <ul class="nav nav-tabs listEjercicios">
                                <?php
                                $arrIdEjerc = explode(',', $ejerc['caratula']);
                                if(!empty($arrIdEjerc[0])){
                                    $i = 1;
                                    foreach ($arrIdEjerc as $k=>$v) {
                                        $active = ($i==1)?'class="active"':'';
                                        echo '<li id="'.$v.'" '.$active.'><a data-toggle="tab" href="#eje'.$v.'" aria-expanded="true">'.$i.'</a></li>';
                                        $i++;
                                    }
                                } ?>
                                </ul>
                            </div>
                            <div class="col-xs-12">
                                <div class="tab-content aquicargan">
                                <?php
                                if(!empty($arrIdEjerc[0])){
                                    $x = 1;
                                    foreach ($arrIdEjerc as $k=>$v) {
                                        $active = ($x==1)?' in active ':'';
                                        echo '<div id="eje'.$v.'" class="tabhijo tab-pane fade'.$active.'"></div>';
                                        $x++;
                                    }
                                } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="div_examen" class="row panel-resource" style="display: none">
                    <div class="col-xs-12">
                        <h1 class="title-resrc"><?php echo JrTexto::_("Assessments") ?></h1>
                        <form class="form-inline">
                            <div class="input-group">
                              <input type="text" id="txtBuscarExam" class="form-control" placeholder="<?php echo ucfirst(JrTexto::_('search').' '.JrTexto::_('Assessment')); ?>s..." aria-describedby="basic-addon1">
                              <span class="input-group-btn">
                                <a class="btn btn-default buscar-exam"><i class="fa fa-search"></i></a>
                              </span>
                            </div>
                        </form>

                        <div class="row resultado-buscar"></div>

                        <?php 
                        $idExam = ($exam['caratula']!=null)? $exam['caratula']:0;
                        ?>
                        <iframe class="contenedor-examview" src="<?php echo (!empty($idExam!=0))? $this->documento->getUrlBase().'/examenes/teacherresrc_view?idexamen='.$idExam : '';  ?>" data-idexam="<?php echo $idExam; ?>" style="border: 0;  width: 100%;  height: 495px; <?php echo (!empty($idExam!=0))? '': 'display: none;';?>"></iframe>
                    </div>
                </div>
                
                <div id="div_games" class="row panel-resource" style="display: none">
                    <div class="col-xs-12">
                        <h1 class="title-resrc"><?php echo JrTexto::_("Game") ?>s</h1>
                        <div class="col-xs-12">
                            <form class="form-inline">
                                <div class="input-group">
                                  <input type="text" id="txtBuscarGame" class="form-control" placeholder="<?php echo ucfirst(JrTexto::_('search').' '.JrTexto::_('game')); ?>s..." aria-describedby="basic-addon1">
                                  <span class="input-group-btn">
                                    <a class="btn btn-default buscar-games"><i class="fa fa-search"></i></a>
                                  </span>
                                </div>
                            </form>
                            
                            <div class="row resultado-buscar"></div>

                            <div class="row lista-juegos">
                                <a class="col-xs-12 col-sm-2 btn btn-primary hidden save game pull-right nopreview"><i class="fa fa-save"></i> <?php echo ucfirst(JrTexto::_("save")).' '.JrTexto::_("game") ?>s</a>
                                <div class="col-xs-12 col-sm-10 pull-right">
                                    <ul class="nav nav-tabs">
                                    <?php
                                    $arrIdGames = explode(',', $games['caratula']);
                                    if(!empty($arrIdGames[0])){
                                        $i = 1;
                                        foreach ($arrIdGames as $key=>$val) {
                                            $active = ($i==1)?'class="active"':'';
                                            echo '<li id="'.$val.'" '.$active.'><a data-toggle="tab" href="#game_'.$val.'" aria-expanded="true">'.$i.'</a></li>';
                                            $i++;
                                        }
                                    } ?>
                                    </ul>
                                </div>
                                <div class="col-xs-12">
                                    <iframe src="<?php echo (!empty($arrIdGames[0]))? $this->documento->getUrlBase().'/game/ver/'.$arrIdGames[0] : ''; ?>" class="tab-content" style="width: 100%; height: 495px;"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </td>
            <td class="cuaderno-der" background="<?php echo $this->documento->getUrlStatic() ?>/libs/resource/images/cuadernillo3.png?3" style="background-repeat:no-repeat" width="74"></td>
        </tr>
    </table>

    <div class="" id="main" style="position:absolute; top:-65px; right: 5px;">
        <ul id="" class="tabs">
            <li id="fuxia" class="hvr-bounce-in" >
                <a class="vertical" href="javascript:ver_div('div_presentacion')">
                    <?php echo JrTexto::_("Presentation") ?>
                </a>
            </li>
            <li id="morado" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_pdfs')">
                    <span><?php echo JrTexto::_("PDF") ?></span>
                </a>
            </li>
            <li id="azul2" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_voca')">
                    <span><?php echo JrTexto::_("Vocabulary") ?></span>
                </a>
            </li>
            <li id="celeste" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_audios')">
                    <span><?php echo JrTexto::_("Audios") ?></span>
                </a>
            </li>
            <li id="verde" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_imagen')">
                    <span><?php echo JrTexto::_("Images") ?></span>
                </a>
            </li>
            <li id="mostaza" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_videos')">
                    <span><?php echo JrTexto::_("Videos") ?></span>
                </a>
            </li>
            <li id="naranja" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_act')">
                    <span><?php echo ucfirst(JrTexto::_("Exercise")) ?>s</span>
                </a>
            </li>
            <li id="rojo" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_games')">
                    <span><?php echo JrTexto::_("Game") ?>s</span>
                </a>
            </li>
            <li id="marron" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_examen')">
                    <span><?php echo JrTexto::_("Assessments") ?></span>
                </a>
            </li>

            <li id="azul" class="hvr-bounce-in">
                <a class="vertical" href="javascript:window.close()">
                    <span><?php echo JrTexto::_("Exit") ?></span>
                </a>
            </li>

        </ul>    
    </div>
</div>
<script type="text/javascript">
//document.getElementById('div_imagen').style.display="none";
var _IDHistorialSesion=0;

var optSlike={
    infinite: false,
    navigation: false,
    slidesToScroll: 1,
    centerPadding: '60px',
    slidesToShow: 5,
    responsive:[
    { breakpoint: 1200, settings: {slidesToShow: 5} },
    { breakpoint: 992, settings: {slidesToShow: 4 } },
    { breakpoint: 880, settings: {slidesToShow: 3 } },
    { breakpoint: 720, settings: {slidesToShow: 2 } },
    { breakpoint: 320, settings: {slidesToShow: 1} }
    ]
};

var pos=-1;
var posv=-1;
var showdiv=[];
function ver_div(div){
    $('.panel-resource').hide();
    $('#'+div).show();
    if($('#'+div+' .myslider').hasClass('slick-slider')) $('#'+div+' .myslider').slick('unslick');
    if( $('#'+div+' .myslider').length>0 ){
        $('#'+div+' .myslider').slick(optSlike);
        if($.inArray(div,showdiv)==-1){
            showdiv.push(div);
            $('#'+div+' .myslider').find('a.miniatura').eq(0).trigger('click');
        }
        
    }
    $('audio').each(function(){ this.pause() });
    $('video').each(function(){ this.pause() });
}

function fileExists(url) {
    var rspta = false;
    if(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.send();
        rspta = (req.status==200);
    }
    return rspta;
}

function existeEnSlider(url, $panel){
    var rspta = true;
    if(url){
        var archivo = $panel.find('.myslider *[src="'+url+'"]');
        rspta = (archivo.length>0);
    }
    return rspta;
}

function insertarFileLista(){
    var tipo_file = $('.panel-resource:visible .open-biblioteca').attr('data-tipo');
    var tag_file = '<span class="del-miniatura nopreview"></span>';
    if(tipo_file=='video'){
        var $panel = $('#div_videos');
        var src = $panel.find('.vidtemporal').attr('src');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        var posv = $panel.find('.miniatura').length-1;
        if(existeArchivo){
            posv = posv + 1;
            tag_file += '<video src="'+src+'" alt="'+tipo_file+'" style="height: 86px; width: 100%" data-pos="'+posv+'" data-tit="null" id="video'+posv+'"> </video>';

            var vidold= $('#txtvideo').val();
            if (vidold) vidold=vidold+',';
            $('#txtvideo').val( vidold+src);

            $('#txtDescv').val('null');
            
            var txtTitv= $('#txtTitv').val();
            if (txtTitv) txtTitv=txtTitv+',';
            $('#txtTitv').val( txtTitv+'null');
            //alert("as");
            class_mini ="miniaturav";
            var tipo_file = 'V';
            var sw = 'OK';
        }
    }else if(tipo_file=='image'){
        var $panel = $('#div_imagen');
        var src = $panel.find('.imgtemporal').attr('src');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        var pos = $panel.find('.miniatura').length-1;
        if(existeArchivo){
            pos = pos + 1;
            tag_file += '<img src="'+src+'" alt="'+tipo_file+'" style="height: 86px; width: 100%" data-pos="'+pos+'" data-tit="null" id="img'+pos+'">';
            
            var imgold= $('#txtimg').val();
            if (imgold) imgold=imgold+',';
            $('#txtimg').val( imgold+src);

            $('#txtDesc').val('null');
            
            var txtTit= $('#txtTit').val();
            if (txtTit) txtTit=txtTit+',';
            $('#txtTit').val( txtTit+'null');
       
            class_mini ="miniaturai";
            var tipo_file = 'I';
            var sw = 'OK';
        }
    }else if(tipo_file=='audio'){
        var $panel = $('#div_audios');
        var dataAudio = $panel.find('.audtemporal').attr('data-audio');
        var src = _sysUrlStatic_+'/media/audio/'+dataAudio;
        var nombre = $panel.find('.audtemporal').attr('data-nombre-file');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        var posA = $panel.find('.miniatura').length-1;
        if(existeArchivo){
            posA = posA + 1;

            tag_file += '<span hre="#" class="file" src="'+src+'" alt="'+tipo_file+'" style="height: 86px; width: 100%" data-pos="'+posA+'" data-tit="'+nombre+'" id="img'+posA+'"><i class="fa fa-file-audio-o fa-4x"></i></span>';

            var audioOld= $('#txtaudio').val();
            if (audioOld) audioOld=audioOld+',';
            $('#txtaudio').val( audioOld+src);

            $('#txtDesca').val(nombre);

            var txtTita= $('#txtTita').val();
            if (txtTita) txtTita=txtTita+',';
            $('#txtTita').val( txtTita+nombre);

            class_mini ="miniatura_a";
            var tipo_file = 'A';
            var sw = 'OK';
        }
    }else if(tipo_file=='pdf'){
        var $panel = $('#div_pdfs');
        var src = $panel.find('.ifrtemporal').attr('src');
        var nombre = $panel.find('.ifrtemporal').attr('data-nombre-file');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        if(existeArchivo){
            
            posv = posv + 1;

            tag_file += '<span class="file" src="'+src+'" alt="'+tipo_file+'" style="height: 86px; width: 100%" data-pos="'+posv+'" data-tit="'+nombre+'" id="img'+posv+'"><i class="fa fa-file-pdf-o fa-4x"></i><span class="nombre-file">'+nombre+'</span></span>';

            var pdfold= $('#txtpdf').val();
            if (pdfold) pdfold=pdfold+',';
            $('#txtpdf').val( pdfold+src);

            $('#txtDescp').val(nombre);

            $('#txtposp').val(posv);
            var txtTitp= $('#txtTitp').val();
            if (txtTitp) txtTitp=txtTitp+',';
            $('#txtTitp').val( txtTitp+'null');

            class_mini ="miniaturap";
            var tipo_file = 'D';
            var sw = nombre;
        }
    }

    if(existeArchivo){
        if(!estaCargadoEnSlider){
            var _html = '<a href="#" class="miniatura '+class_mini+'">'+tag_file+'</a>';
            $panel.find('.myslider').slick('unslick');
            $panel.find('.myslider').append(_html);
            $panel.find('.myslider').slick(optSlike);
            $panel.find('.temp').attr('src','');
            guardarTResource(tipo_file,src,sw);
        } else {
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo JrTexto::_('The file has been already loaded');?>.', 'warning');
        }
    } else {
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo JrTexto::_('The file does not exist');?>.', 'error');
    }
}

function cambiarPortada(){
    var $divPresentacion = $('#div_presentacion');
    var src = $divPresentacion.find('.imgtemporal').attr('src');
    $divPresentacion.css({
        "background-image": 'url('+src+')',
    });
    //$divPresentacion.find('.imgtemporal').attr('src','');
}

var guardarTResource = function($tipo,$file,$sw){
    var data = new Array();
    
    data.idNivel = <?php echo $nivel?>;
    data.idUnidad = <?php echo $unidad?>;
    data.idActividad = <?php echo $actividad?>;
    data.tipo = $tipo;
    data.orden = <?php echo $this->orden; ?>;
    data.compartido=$('.btncompartir').hasClass('btn-inactive')?0:1;
    data.publicado=$('.btnpublicar').hasClass('btn-inactive')?0:1;

    if ($tipo=='P'){
        data.texto= document.getElementById('txtarea_pnl1<?php echo $idgui; ?>').value;
        data.titulo= document.getElementById('txtTitulo').value;
        data.caratula= $('#div_presentacion').find('.imgtemporal').attr('src');
    }else if ($tipo=='I'){

        data.texto= $('#txtTit').val();
        data.titulo= "";
        data.pos= $('#txtpos').val();            
        //alert(data.pos);            
        data.caratula= $('#txtimg').val();

        if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$('#txtDesc').val();
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTit').val(texto);
            data.texto= $('#txtTit').val();

            $("#img"+data.pos).attr("data-tit",$('#txtDesc').val());

            data.titulo= "";
            data.pos= "null";
        }
    }else if ($tipo=='V'){
        data.texto= $('#txtTitv').val();
        data.titulo= "";
        data.pos= $('#txtposv').val();
        data.caratula= $('#txtvideo').val();
        //alert(data.caratula);
        if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$('#txtDescv').val();
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTitv').val(texto);
            data.texto= $('#txtTitv').val();
            $("#video"+data.pos).attr("data-tit",$('#txtDescv').val());
            data.titulo= "";
            data.pos= "null";
        }
    }else if ($tipo=='A'){
        data.texto= $('#txtTita').val();
        data.titulo= "";
        data.pos= $('#txtposa').val();
        data.caratula= $('#txtaudio').val();
        //alert(data.caratula);
        if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$('#txtDesca').val();
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTita').val(texto);
            data.texto= $('#txtTita').val();
            $("#video"+data.pos).attr("data-tit",$('#txtDesca').val());
            data.titulo= "";
            data.pos= "null";
        }
    }else if ($tipo=='D'){
        data.texto= $('#txtTitp').val();            
        data.titulo= "";
        data.pos= $('#txtposp').val();
        data.caratula= $('#txtpdf').val();
        //alert(data.caratula);

        //if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$sw;
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTitp').val(texto);
            data.texto= $('#txtTitp').val();
            $("#pdf"+data.pos).attr("data-tit",$('#txtDescp').val());
            data.titulo= "";
            data.pos= "null";
        //}
        //retunr;
    }else if ($tipo=='O'){
        data.titulo = 'Vocabulary';
        data.texto = '';
        data.caratula = $file; /*para vocabulary, $file es un Json en string.*/
    }else if($tipo=='E'){
        data.titulo = 'Exercises';
        data.texto = '';
        data.caratula = $file;
    }else if($tipo=='G'){
        data.titulo = 'Games';
        data.texto = '';
        data.caratula = $file;
    }else if($tipo=='X'){
        data.titulo = 'Exam';
        data.texto = '';
        data.caratula = $file;
    }
    //alert($file);
    //data.pkIdlink= idpk||0;
    var res = xajax__('', 'Resources', 'saveResource', data);
    if(res){
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo JrTexto::_('saved successfully');?>.', 'success');
    }
};

var IDGame = '';

$(document).ready(function(){
    $('.btnpublicar').click(function(ev){
        var obj=$(this);
        var data =$(this).hasClass('btn-inactive')?1:0;
       if(data==1){
           $(this).removeClass('btn-inactive').addClass('btn-success');
        }else{
           $(this).addClass('btn-inactive').removeClass('btn-success');
        }
        var parent=$(this).parent();
        var id=parent.attr('data-id'); 
        if(id!=undefined&&id!=''){
            var res = xajax__('', 'Recursos', 'setCampo', id,'publicado',data);
        }
         
    });

    $('.btncompartir').click(function(ev){
        var obj=$(this);
        var data =$(this).hasClass('btn-inactive')?1:0;
        if(data==1){
           $(this).removeClass('btn-inactive').addClass('btn-primary');
        }else{
           $(this).addClass('btn-inactive').removeClass('btn-primary');
        }
        var parent=$(this).parent();
        var id=parent.attr('data-id');
        if(id!=undefined&&id!=''){
            var res = xajax__('', 'Recursos', 'setCampo', id,'compartido',data);
        }
    });
    var ajustarAltura = function(){
        $('.cuaderno-contenido').children('.panel-resource').css({
            'max-height': '610px',
        });
    }; 
    var cargarAutorLibro = function(){
        var idautor = $('#teacher_resrc-tool').attr('data-idautor');
        if(idautor==undefined || idautor=='') return false;
        var formData = new FormData();
        formData.append("dni", idautor.trim());
        $.ajax({
            url: _sysUrlBase_+'/usuario/buscarjson/',
            type: "POST",
            data:  {'dni' : idautor},
            dataType: 'json',
            success: function(resp){
                if(resp.code==='ok'){
                    var datainfo=resp.data;
                    var nombre_full=datainfo.ape_paterno+' '+datainfo.ape_materno+' '+datainfo.nombre;
                    if(nombre_full.trim()!='') $('.creado-por span').html(nombre_full);
                }
            },
            error: function(e){
                $('.autor span#infoautor').html('Error?');
            }
        });
    };
    var mostrarOcultarBtnGuardar= function($this){
        var idPanel = $this.closest('.panel-resource').attr('id');
        var $panel = $('#'+idPanel);
        var cant_tabs = $panel.find('ul.nav-tabs li').length;
        if(cant_tabs>=1) $panel.find('.btn.save').removeClass('hidden');
        else $panel.find('.btn.save').addClass('hidden');
    };
    ajustarAltura(); /* ¡¡¡MUY NECESARIO PARA QUE NO SE DESBORDE HACIA ABAJO!!! */
    cargarAutorLibro();

 /*** #Fn generales(Img,Vid,Pdf) ***/
    $('.panel-resource')
        .on('click',".open-biblioteca",function(e){ 
            e.preventDefault();
            var txt= '<?php echo JrTexto::_('Select or upload'); ?>';
            selectedfile(e,this,txt, 'insertarFileLista');
        }).on('click',".miniaturai",function(e){ 
            e.preventDefault();
            var id = $(this).parents('.panel-resource').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $('#txtDesc').val(tit).removeClass('hidden');
            $('#txtDesc').siblings('.btn.saveimage').removeClass('hidden');
            $('#txtpos').val(pos);
        }).on('click',".miniaturav",function(e){ 
            e.preventDefault();
            var id = $(this).closest('.panel-resource').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            console.log($panel.find('.'+tipo+'_resrc'));
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $('#txtDescv').val(tit).removeClass('hidden');
            $('#txtDescv').siblings('.btn.savevideo').removeClass('hidden');
            $('#txtposv').val(pos);
            
            //alert("as");
        }).on('click',".miniatura_a",function(e){ 
            e.preventDefault();
            var id = $(this).closest('.panel-resource').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            console.log($panel.find('.'+tipo+'_resrc'));
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $('#txtDesca').val(tit).removeClass('hidden');
            $('#txtDesca').siblings('.btn.saveaudio').removeClass('hidden');
            $('#txtposa').val(pos);
            
            //alert("as");
        }).on('click',".miniaturap",function(e){ 
            e.preventDefault();
            var id = $(this).closest('.panel-resource').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $('#txtDescp').val(tit).removeClass('hidden');
            $('#txtDescp').siblings('.btn.savepdf').removeClass('hidden');
            $('#txtposp').val(pos);
            
            //alert("as");
        }).on('click', '.miniatura>.del-miniatura', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var idPanel = $(this).closest('.panel-resource').attr('id');
            var $miniatura = $(this).parent();
            var src = $miniatura.children('*[src]').attr('src');
            var tipo = $miniatura.children('*[src]').attr('alt');
            var $slider = $(this).parents('.myslider');

            var all_media = $('#'+idPanel+' .contenido-caratula').val()
            var all_textos = $('#'+idPanel+' .contenido-texto').val()
            var arrMedia = all_media.split(',');
            var arrTextos = all_textos.split(',');
            var index = arrMedia.indexOf(src);
            var sw = 'OK';
            arrMedia.splice(index, 1);
            arrTextos.splice(index, 1);

            var new_media = arrMedia.join(',');
            var new_textos = arrTextos.join(',');
            $('#'+idPanel+' .contenido-caratula').attr('value', new_media);
            $('#'+idPanel+' .contenido-texto').attr('value', new_textos);

            $slider.slick('unslick');
            $miniatura.remove();
            $slider.slick(optSlike);
            if(tipo=='image'){
                var type = 'I';
            }else if(tipo=='video'){
                var type = 'V';
            }else if(tipo=='audio'){
                var type = 'A';
            }else if(tipo=='pdf'){
                var type = 'D';
                sw = ''
            }
            guardarTResource(type, '', sw);
        });

    $('#teacher_resrc-tool')
        .on('click', '.live-edit', function(e){
            console.log('live edit CLICK');
            if(!$('#teacher_resrc-tool').hasClass('editando')){
                return false;
            }
            $(this).css({"border": "0px"}); 
            addtext1(e,this);
        })
        .on('blur','.live-edit>input',function(e){                e.preventDefault();
            $(this).closest('.live-edit').removeAttr('Style'); 
            addtext1blur(e,this);
        })
        .on('keypress','.live-edit>input', function(e){
            if(e.which == 13){ 
                e.preventDefault();          
                $(this).trigger('blur');
            } 
        })
        .on('keyup','input', function(e){
            if(e.which == 27){ 
                $(this).attr('data-esc',"1");
                $(this).trigger('blur');
            }
        });
    
    $('#div_imagen').on('click','.saveimage',function(){
        //alert("img");
        var txtimg= $('#txtimg').val();        
        //alert(txtimg);
        guardarTResource('I',txtimg,'');
    });

    $('#div_videos').on('click','.savevideo',function(){
        //alert("img");
        var txtvid= $('#txtvid').val();        
        //alert(txtimg);
        guardarTResource('V',txtvid,'');
    });

    $('#div_audios').on('click','.saveaudio',function(){
        //alert("img");
        var txtaud= $('#txtaudio').val();        
        //alert(txtimg);
        guardarTResource('A',txtaud,'');
    });

    $('#div_pdfs').on('click','.savepdf',function(){
        //alert("img");
        var txtpdf= $('#txtpdf').val();        
        //alert(txtimg);
        guardarTResource('D',txtpdf,'');
    });
   
    $("form").keypress(function(e) {
        if (e.which == 13) {
            e.preventDefault();
        }
    });

    $('#teacher_resrc-tool .preview').click(function(){
        $(this).hide();
        $(this).siblings('.back').show();
        $('#div_presentacion .btn-toolbar').hide();
        $('form.form-inline').hide();
        $('#pnlactividadesresourses').hide();
        $('#div_voca input.palabra').val('');
        $('#div_voca .btn.search-word').trigger('click');
        $('#div_voca .result-busqueda .palabra_vocab').removeClass('palabra_vocab').removeClass('active');
        $('#div_games .resultado-buscar').hide();
        $('#div_examen .resultado-buscar').hide();
        $('.controles-edicion').hide();
        $('.embed-responsive #txtDesc').hide();
        $('.embed-responsive .saveimage').hide();

        $('#teacher_resrc-tool .nopreview').hide();
        $('#teacher_resrc-tool .descripcion-media').attr('readonly','readonly');
        $('.botones-generales').children('a:not(.preview,.back)').hide();
        ver_div('div_presentacion');        
    });

    $('#teacher_resrc-tool .back').click(function(){
        $(this).hide();
        $(this).siblings('.preview').show();
        $('#div_presentacion .btn-toolbar').show();
        $('form.form-inline').show();
        $('#pnlactividadesresourses').show();
        $('#div_voca .result-busqueda .list-group-item').addClass('palabra_vocab').addClass('active');
        $('#div_games .resultado-buscar').show();
        $('#div_examen .resultado-buscar').show();
        $('.controles-edicion').show();
        $('#div_imagen #txtDesc').show();
        $('.embed-responsive #txtDesc').show();
        $('.embed-responsive .saveimage').show();
        $('.botones-generales').children('a:not(.preview,.back)').show();
        $('#teacher_resrc-tool .nopreview').show();
        $('#teacher_resrc-tool .descripcion-media').removeAttr('readonly');
    });

 /*** #Presentacion ***/
    var edithtml=function(id){
        tinymce.init({
            relative_urls : false,
            convert_newlines_to_brs : true,
            menubar: false,
            statusbar: false,
            verify_html : false,
            content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
            selector: '#'+id,
            height: 250,       
          plugins:["table textcolor" ], 
          toolbar: 'undo redo | table | styleselect |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor'
        });
    }; 
    $('#div_presentacion').on('click','.edithtml',function(){
        $(this).hide("fast");
        $(this).siblings('.savehtml').show();
        var content=$(this).parents('.aquihtml');
        var txt=content.find('.txtpresentacion');
        var txtedit=content.find('.txtpresentacionedit');
            txtedit.val(txt.html());
            txt.hide();
            txtedit.show();

            var txt1=content.find('.txttitulo');
            var txttit=content.find('.div_txttitulo');            
            txt1.hide();
            txttit.show();
            edithtml(txtedit.attr('id'));
    });
    $('#div_presentacion').on('click','.savehtml',function(){
        $(this).hide("fast");
        var content=$(this).closest('.aquihtml');
        tinyMCE.triggerSave();
        var txt=content.find('.txtpresentacion');
        var txtedit=content.find('.txtpresentacionedit'); 
            txt.show();        
            txt.html(txtedit.val()); 
            tinymce.remove('#'+txtedit.attr('id')); 
            txtedit.hide();          

            var txt1=content.find('.txttitulo');
            var txttit=content.find('.div_txttitulo');            
            var txttit1=document.getElementById('txtTitulo').value;
            txt1.show();
            txt1.html(txttit1); 
            txttit.hide();
            guardarTResource('P','','');
       $(this).siblings('.edithtml').show();
    });
    $('#div_presentacion').on('click', '.biblioteca-portada', function(e) {
        e.preventDefault();
        var txt= '<?php echo JrTexto::_('Select or upload'); ?>';
        selectedfile(e,this,txt, 'cambiarPortada');
    });


 /**** #Vocabulario ****/
    var activarListElem = function($li_item){
        $li_item.toggleClass('active');
        if($li_item.hasClass('active')){ var isChecked= true; }
        else{ var isChecked= false; }
        $li_item.find('input.checkbox-ctrl').prop('checked', isChecked);
    };
    var actualizarJsonVocab = function(){
        var jsonVocab = {};
        var $li_palabra = $('#div_voca .result-busqueda .palabra_vocab.active');
        $li_palabra.each(function() {
            var id = $(this).attr('data-id');
            id = id.replace('id_','');
            var palabra = $(this).find('.word').text();
            var tipo = $(this).find('.type').text();
            var definicion = $(this).find('.def').text();
            jsonVocab[id] = {
                'palabra' : palabra,
                'tipo' : tipo,
                'definicion' : definicion,
            };
        });
        return jsonVocab;
    };
    var guardarVocabulario = function(){
        var jsonVocab = actualizarJsonVocab();
        var stringVocab = JSON.stringify(jsonVocab);
        guardarTResource('O', stringVocab, '');
    };
    $('#div_voca')
        .on('click', '.btn.search-word', function(e) {
            e.preventDefault();
            var arrVocab = [];
            var htmlseleccionado = '';
            $('#div_voca .list-group-item.active').each(function() {
                htmlseleccionado+= $(this).wrapAll('<div>').parent().html();
                arrVocab.push( $(this).attr('data-id') );
            });
            var value = $('input.palabra').val().toUpperCase();
            if(value.trim().length>0){
                $('#div_voca .result-busqueda').html('<div style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');

                $.ajax({
                    url: _sysUrlBase_+'/diccionario/ver',
                    type: 'GET',
                    dataType: 'json',
                    data: {palabra: value},
                })

                .done(function(res) {
                    $('#div_voca .result-busqueda').html('');
                    if(res.data.length>0){
                        var _html = htmlseleccionado;
                        $.each(res.data, function(key, val) {
                            var id = val.id;
                            var palabra = val.palabra;
                            var tipo = val.tipo.trim();
                            var def = val.definicion;
                            if(arrVocab.indexOf('id_'+id)==-1){
                                _html += '<a class="col-xs-12 list-group-item palabra_vocab" data-id="id_'+id+'"><div class="col-xs-offset-1 col-xs-11"><h4><span  class="word">'+palabra+'</span>' ;
                                if(tipo.length!=0) _html += ' <small class="type">['+ tipo +']</small>';
                                _html += '</h4> <p class="def">'+def+'</p></div></a>';
                            }
                        });
                        $('#div_voca .result-busqueda').html(_html);
                    } else {
                        $('#div_voca .result-busqueda').html('');
                        if(htmlseleccionado!=''){
                            $('#div_voca .result-busqueda').html(htmlseleccionado);
                        }
                        var _html = '<div><?php echo ucfirst(JrTexto::_('we could not find your word'));?>.</div>';
                        $('#div_voca .result-busqueda').append(_html);
                    }
                })
                .fail(function(err) {
                    $('#div_voca .result-busqueda').html('');
                    console.log(err);
                    if(htmlseleccionado!=''){
                        $('#div_voca .result-busqueda').html(htmlseleccionado);
                    }
                    var _html = '<div><?php echo ucfirst(JrTexto::_('Sorry, something went wrong'));?>.</div>';
                        $('#div_voca .result-busqueda').append(_html);
                });
            } else {
                $('#div_voca .result-busqueda').html('');
                if(htmlseleccionado!=''){
                    $('#div_voca .result-busqueda').html(htmlseleccionado);
                }else{
                    var _html = '<div><?php echo ucfirst(JrTexto::_('The result of your search will be shown here'));?>.</div>';
                    $('#div_voca .result-busqueda').append(_html);
                }
            }
        })
        .on('keypress', '.frmDiccionario input.palabra', function(e) {
            if(e.which==13){
                $('.btn.search-word').trigger('click');
            }
        })
        .on('click', '.result-busqueda .palabra_vocab', function(e) {
            e.preventDefault();
            activarListElem( $(this) );
            guardarVocabulario();
        });

 /**** #Ejercicios ****/
    var ejericiosactidades=[];
    var buscarActividades=function(txt,fn){
        var formData = new FormData();
       // var idmet='1&2'; 
        var inarray=[];
        var seleccionado=$('#div_act #pnlactividadesresourses').find('.selejericioact.active');
        var htmlseleccionado='<div>';
        var isel=1;
        $.each(seleccionado,function(i,v){
            vi=$(v).attr('data-idejercicio');
            inarray.push('eje'+vi);
            $('span.nNum',v).text(isel++);
            htmlseleccionado+= $(v).wrapAll('<div>').parent().html();
        });
        htmlseleccionado+='</div>';
        formData.append("nivel",'<?php echo $this->idnivel?>');
        formData.append("unidad",'<?php echo $this->idunidad?>');
        formData.append("actividad",'<?php echo $this->idactividad?>');
        formData.append("titulo",txt);
        $.ajax({
            url: _sysUrlBase_+'/Actividad/buscarjson/',
            type: "POST",
            data:  formData,
            contentType: false,
            processData: false,
            dataType :'json',
            cache: false,
            processData:false,
            beforeSend: function(XMLHttpRequest){
                  $('#div_act #pnlactividadesresourses').html('<div style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
            },success: function(data){
                if(data.code==='ok'){
                    var title=seleccionado.length>0?htmlseleccionado:'';
                    var datainfo=data.data;
                    var i=isel;
                    if(datainfo!=undefined) 
                        $.each(datainfo,function(ii,vv){                        
                         if(vv.met.idmetodologia!=3){;
                            var iac=0;
                            $.each(vv.act.det,function(iii,obj){
                                if(inarray.indexOf('eje'+obj.iddetalle)==-1){
                                    if($('#div_act #pnlveractividadesresourses #'+obj.iddetalle).length==0) {
                                        var activo = ' ';
                                        var clsBtn = ' btn-info ';
                                    }
                                    else{
                                        var activo = ' active ';
                                        var clsBtn = ' btn-success ';
                                    }
                                    title+='<div class="col-md-3 col-sm-4 col-xs-6 selejericioact'+activo+'" data-idejercicio="'+obj.iddetalle+'" style="padding:2px;"><div class="'+clsBtn+'"><span class="nNum">'+(i++)+'.</span> '+obj.titulo+'</div></div>';
                                    var itext=obj.texto
                                    ejericiosactidades['eje'+obj.iddetalle]=itext.replace(/__xRUTABASEx__/gi,_sysUrlBase_) ;
                                }
                            });
                         }
                        });
                        title+='</div>';
                        $('#div_act #pnlactividadesresourses').html(title);
                    }else{
                        
                    }
            },
            error: function(e){
                $('.autor span#infoautor').html('<?php echo JrTexto::_('Error'); ?>');
            }
        });
    };

    var cargarEjercicios = function(){
        $('#div_act ul li').each(function() {
            var id =$(this).attr('id');
            if(id!=''){
                $.ajax({
                    url: _sysUrlBase_+'/actividad/jsonGetXId',
                    type: 'GET',
                    dataType: 'json',
                    data: {idAct: id},
                })
                .done(function(resp) {
                    if(resp.code=='ok' && resp.data.length!=0){
                        var html_texto = resp.data.texto;
                        var parsedTexto = html_texto.replace(/__xRUTABASEx__/g, _sysUrlBase_);
                        $('#div_act .tab-content.aquicargan #eje'+id).html(parsedTexto);
                    }
                })
                .fail(function(err, textStatus, errorThrown) {
                    throw textStatus +': '+ errorThrown;
                });
            }else {
                throw 'There was an error loading exercises from Data Base. Exercise ID is empty.';
            }
            
        });
    };
    cargarEjercicios();
    $('#div_act .btnsearchactividad').click(function(){
        txt=$('#txtsearchactividad').val();
        buscarActividades(txt);
        mostrarOcultarBtnGuardar($(this));
    });
    $('#div_act').on('click','.selejericioact',function(ev){
        ev.preventDefault();
        var obj=$(this);
         iddetalle=obj.attr('data-idejercicio');
        var pnl=$('#div_act #pnlveractividadesresourses');
        var pnlul=$('ul.listEjercicios',pnl);
        var pnlcontent=$('.tab-content.aquicargan',pnl);
        if(!obj.hasClass('active')){
            obj.addClass('active');           
            var txt=ejericiosactidades['eje'+iddetalle];
            $('div',obj).addClass('btn-success').removeClass('btn-info'); 
            var nli=$('li',pnlul).length+1;
            var newli='<li id="'+iddetalle+'"><a data-toggle="tab" href="#eje'+iddetalle+'">'+nli+'</a></li>';
            var newcontent='<div id="eje'+iddetalle+'" class="tabhijo tab-pane fade in" >'+txt+'</div>';
            pnlul.append(newli);
            pnlcontent.append(newcontent); 
            $('li#'+iddetalle+' a',pnlul).trigger('click');          
        }else{
           obj.removeClass('active');
           $('div',obj).addClass('btn-info').removeClass('btn-success');
           $('li#'+iddetalle,pnlul).remove();
           $('.tab-pane#eje'+iddetalle,pnlcontent).remove();
           nli=1;
           $('li',pnlul).each(function(i,v){
                $('a',v).text(nli++);
           });
           $('li:last-child>a',pnlul).trigger('click');
        }
        mostrarOcultarBtnGuardar($(this));
    });
    $('.tab-content.aquicargan').tplcompletar({editando:false});
    //buscarActividades('a');

    $('#div_act .save.ejercicio').click(function(e) {
        e.preventDefault();
        var arrIdEjerc = [];
        $("#div_act ul.listEjercicios li").each(function() {
            var idEjerc = $(this).attr('id');
            arrIdEjerc.push(idEjerc);
        });
        var strArray = arrIdEjerc.join(',');
        guardarTResource('E',strArray,'OK');
    });

 /*** #Juegos ***/
    var arrJuegos = [];
    var buscarJuegos = function(){
        var value = $('#div_games #txtBuscarGame').val();
        $('#div_games .resultado-buscar').html('<div style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
        $.ajax({
            url: _sysUrlBase_+'/tools/json_buscarGames',
            type: 'POST',
            dataType: 'json',
            data: {txtBuscar: value, nivel: '<?php echo $this->idnivel?>', unidad: '<?php echo $this->idunidad?>', actividad: '<?php echo $this->idactividad?>'},
        })
        .done(function(resp) {
            $('#div_games .resultado-buscar').html('');
            var i = 1;
            $.each(resp.data, function(key, val) {
                if($('#div_games .lista-juegos #'+val.idtool).length==0) {
                    var activo = ' ';
                    var clsBtn = ' btn-info ';
                }
                else{
                    var activo = ' active ';
                    var clsBtn = ' btn-success ';
                }
                var item = '<div class="col-md-3 col-sm-4 col-xs-6 item-juego '+activo+'" data-idgame="'+val.idtool+'"><a class="btn '+clsBtn+' btn-block btn-xs">'+i+'. '+val.titulo+'</a></div>';
                $('#div_games .resultado-buscar').append(item);
                
                arrJuegos['game_'+val.idtool] = val;
                i++;
            });
        })
        .fail(function() {
            console.log("error");
        });
        
    };
    $('#div_games')
    .on('click', '.btn.buscar-games', function(e) {
        e.preventDefault();
        buscarJuegos();
        mostrarOcultarBtnGuardar($(this));
    })
    .on('keypress', '#txtBuscarGame', function(e) {
        if(e.which==13){ e.preventDefault(); buscarJuegos(); }
    })
    .on('click', '.item-juego', function(e) {
        e.preventDefault();
        var $this=$(this);
        idGame=$this.attr('data-idgame');
        var $pnl=$('#div_games .lista-juegos');
        var $pnl_ul=$('ul.nav',$pnl);
        var $pnl_content=$('.tab-content',$pnl);
        if(!$this.hasClass('active')){
            $this.addClass('active');
            var titulo = arrJuegos['game_'+idGame].titulo;
            var descripcion = arrJuegos['game_'+idGame].descripcion;
            var txt=arrJuegos['game_'+idGame].texto;
            var txt_html = txt.replace(/__xRUTABASEx__/gi,_sysUrlBase_);
            $('.btn',$this).addClass('btn-success').removeClass('btn-info'); 
            var nli=$('li',$pnl_ul).length+1;
            var newli='<li id="'+idGame+'"><a data-toggle="tab" href="#game_'+idGame+'">'+nli+'</a></li>';
            //var newcontent='<div id="game_'+idGame+'" class="tabhijo tab-pane fade in" ><iframe src="'+_sysUrlBase_+'/game/ver/'+idGame+'" style="width:100%; height:420px;"></div>';
            $pnl_ul.append(newli);
            //$pnl_content.append(newcontent);
            $pnl_content.find('.nopreview').remove();
            $('li#'+idGame+' a',$pnl_ul).trigger('click');
        }else{
           $this.removeClass('active');
           $('.btn',$this).addClass('btn-info').removeClass('btn-success');
           $('li#'+idGame,$pnl_ul).remove();
           $('.tab-pane#game_'+idGame,$pnl_content).remove();
           nli=1;
           $('li',$pnl_ul).each(function(i,v){
                $('a',v).text(nli++);
           });
           $('#div_games iframe.tab-content').attr('src', '');
           $('li:last-child>a',$pnl_ul).trigger('click');
        }
        mostrarOcultarBtnGuardar($(this));
    })
    .on('click', 'ul.nav-tabs>li', function(e) {
        var idGame = $(this).attr('id');
        $('#div_games iframe.tab-content').attr('src', _sysUrlBase_+'/game/ver/'+idGame);
    })
    .on('click', '.save.game', function(e) {
        e.preventDefault();
        var arrIdJuegos = [];
        $("#div_games ul.nav-tabs li").each(function() {
            var idJuego = $(this).attr('id');
            arrIdJuegos.push(idJuego);
        });
        var strArray = arrIdJuegos.join(',');
        guardarTResource('G',strArray,'OK');
    });

    ver_div('div_presentacion');
    <?php if(strtolower($rolActivo)=='alumno'){?>
        $('#teacher_resrc-tool .preview').trigger('click');
        $('#teacher_resrc-tool .preview').hide();
        $('#teacher_resrc-tool .back').hide();
    <?php } ?>

/*** #Examenes ***/
    var buscarExams = function(){
        var value = $('#div_examen #txtBuscarExam').val();
        $('#div_examen .resultado-buscar').html('<div style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
        $.ajax({
            url: _sysUrlBase_+'/examenes/buscar_exams/',
            type: 'POST',
            dataType: 'json',
            data: {txtBuscar: value, nivel: '<?php echo $this->idnivel?>', unidad: '<?php echo $this->idunidad?>', actividad: '<?php echo $this->idactividad?>'},
        })
        .done(function(resp) {
            $('#div_examen .resultado-buscar').html('');
            var i = 1;
            $.each(resp.data, function(key, val) {
                if($('#div_examen iframe').attr('data-idexam')==val.idexamen){
                    var activo = ' active ';
                    var clsBtn = ' btn-success ';
                }else{
                    var activo = ' ';
                    var clsBtn = ' btn-info ';
                }
                var item = '<div class="col-md-3 col-sm-4 col-xs-6 item-exam '+activo+'" data-idexam="'+val.idexamen+'"><a class="btn '+clsBtn+' btn-block btn-xs">'+i+'. '+val.titulo+'</a></div>';
                $('#div_examen .resultado-buscar').append(item);
                i++;
            });
        })
        .fail(function() {
            console.log("error");
        });
    };
    $('#div_examen').on('click', '.btn.buscar-exam', function(e) {
        e.preventDefault();
        buscarExams();
    }).on('keypress', '#txtBuscarExam', function(e) {
        if(e.which==13){ e.preventDefault(); buscarExams(); }
    }).on('click', '.item-exam', function(e) {
        e.preventDefault();
        var idExam = $(this).attr('data-idexam');
        var rutaExam = _sysUrlBase_+'/examenes/teacherresrc_view?idexamen='+idExam;
        var $iframe = $('#div_examen iframe.contenedor-examview');
        if(!$(this).hasClass('active')){
            /*reset todos los '.item-exam' y 'a.btn'*/
            $('#div_examen .resultado-buscar .item-exam').removeClass('active');
            $('#div_examen .resultado-buscar .item-exam>a.btn').removeClass('btn-success').addClass('btn-info');
            
            /* asignar al seleccionado las clases de Activo */
            $(this).addClass('active');
            $(this).find('a.btn').removeClass('btn-info').addClass('btn-success');
            
            /* cargar el id-exam al <iframe> */
            if(idExam!=0){
                $iframe.attr('src', rutaExam);
                $iframe.attr('data-idexam', idExam);
                $iframe.show();
            }
        } else {
            $(this).removeClass('active');
            $(this).find('a.btn').addClass('btn-info').removeClass('btn-success');
            idExam = 0;
            $iframe.hide();
            $iframe.attr('src', '');
            $iframe.attr('data-idexam', '0');
        }
        guardarTResource('X', idExam, 'OK');
    });




    /******************************************************/
    var registrarHistorialSesion = function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/agregar',
            type: 'POST',
            dataType: 'json',
            data: {'lugar': 'TR', 'fechaentrada': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                console.log("success");
                console.log(resp);
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                console.log(resp);
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log("error");
            console.log(xhr.responseText);
        });
        
    };

    registrarHistorialSesion();
    $(window).on('beforeunload', function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/editar',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {'idhistorialsesion': _IDHistorialSesion, 'fechasalida': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                console.log("success");
                console.log(resp);
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log("error");
            console.log(xhr.responseText);
            return false;
        });
    });
});



</script>