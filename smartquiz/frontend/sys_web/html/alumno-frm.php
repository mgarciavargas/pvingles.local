<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="panel">
    <div class="panel-body">
      <div id="msj-interno"></div>
      <h1 class="border-bottom">
        <?php echo ucfirst(JrTexto::_("Student"));?>
        <small>
          <i class="fa fa-angle-double-right"></i>
          <?php echo ucfirst(JrTexto::_($this->frmaccion));?>
        </small>
      </h1>
      <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkDni" id="pkdni" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtDni">
              <?php echo JrTexto::_('D.N.I.');?> <span class="required"> * :</span>
            </label>
            <div class="col-md-4 col-sm-5 col-xs-12">
              <input type="text"  id="txtDni" name="txtDni" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["dni"];?>" maxlength="8">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtApellidopaterno">
              <?php echo JrTexto::_('Apellido paterno');?> <span class="required"> * :</span>
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input type="text"  id="txtApellidopaterno" name="txtApellidopaterno" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["apellidopaterno"];?>" maxlength="20">

            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtApellidomaterno">
              <?php echo JrTexto::_('Apellido materno');?> <span class="required"> * </span>:
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input type="text"  id="txtApellidomaterno" name="txtApellidomaterno" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["apellidomaterno"];?>" maxlength="20">

            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('Nombres');?> <span class="required"> * </span>:
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>" maxlength="30">

            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtSexo">
              <?php echo JrTexto::_('Sexo');?> :
            </label>
            <div class="col-md-4 col-sm-5 col-xs-12">
              <select id="txtsexo" name="txtSexo" class="form-control">
                <option value="">- <?php echo ucfirst(JrTexto::_('Select')); ?> -</option>
                <option value="F"><?php echo JrTexto::_('Female'); ?></option>
                <option value="M"><?php echo JrTexto::_('Male'); ?></option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtFechanacimiento">
              <?php echo JrTexto::_('Fecha de nacimiento');?> :
            </label>
            <div class="col-md-4 col-sm-5 col-xs-12">
              <div class="input-group datetimepicker" style="margin: 0;">
                <input name="txtFechanacimiento" id="txtFechanacimiento" class="verdate form-control col-md-7 col-xs-12" required="required" type="text" value="<?php echo @$frm["fechanacimiento"];?>">
                <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtIdEstadocivil">
              <?php echo JrTexto::_('Estado civil');?> :
            </label>
            <div class="col-md-4 col-sm-5 col-xs-12">
              <select id="txtIdEstadocivil" name="txtIdEstadocivil" class="form-control">
                <option value="">- <?php echo ucfirst(JrTexto::_('Select')); ?> -</option>
                <?php  if(!empty($this->estado_civil))
                foreach ($this->estado_civil as $estado_civil) { ?>
                <option value="<?php echo $estado_civil["idestadocivil"]?>" <?php echo $estado_civil["idestadocivil"]==@$frm["idestadocivil"]?"selected":""; ?> ><?php echo $estado_civil["descripcion"] ?></option>
                <?php } ?> 
              </select>
            </div>
          </div>

          <!--div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtUbigeo">
              <?php echo JrTexto::_('Ubigeo');?>
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtubigeo" name="txtUbigeo" class="form-control">
                <option value=""><?php echo ucfirst(JrTexto::_('Select')); ?></option>
                <?php if(!empty($this->fkubigeo))
                foreach ($this->fkubigeo as $fkubigeo) { ?><option value="<?php echo $fkubigeo["id_ubigeo"]?>" <?php echo $fkubigeo["id_ubigeo"]==@$frm["ubigeo"]?"selected":""; ?> ><?php echo $fkubigeo["pais"] ?></option><?php } ?>
              </select>
            </div>
          </div-->

          <!--div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtUrbanizacion">
              <?php echo JrTexto::_('Urbanizacion');?> <span class="required"> * :</span>
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input type="text"  id="txtUrbanizacion" name="txtUrbanizacion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["urbanizacion"];?>">

            </div>
          </div-->

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtDireccion">
              <?php echo JrTexto::_('Dirección');?> :
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input type="text"  id="txtDireccion" name="txtDireccion" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["direccion"];?>" maxlength="60">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtTelefono">
              <?php echo JrTexto::_('Teléfono');?> :
            </label>
            <div class="col-md-4 col-sm-5 col-xs-12">
              <input type="text"  id="txtTelefono" name="txtTelefono" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["telefono"];?>" maxlength="12">

            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtCelular">
              <?php echo JrTexto::_('Celular');?> :
            </label>
            <div class="col-md-4 col-sm-5 col-xs-12">
              <input type="text"  id="txtCelular" name="txtCelular" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["celular"];?>" maxlength="12">

            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtEmail">
              <?php echo JrTexto::_('Email');?> :
            </label>
            <div class="col-md-4 col-sm-5 col-xs-12">
              <input type="email" id="txtEmail" name="txtEmail" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["email"];?>" maxlength="40">

            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtFoto">
              <?php echo JrTexto::_('Foto');?> :
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <div class="row">
                <input type="hidden" name="txtFoto" id="txtFoto">
                <input type="hidden" name="txtFoto_old" value="<?php echo @$frm["img"];?>">
                <div class="col-md-3">
                  <div class="img-container" style="position: relative;">
                    <input type="file" class="subir_foto" data-tipo="image" data-target="hImgPortada" accept="image/*" style="position: absolute; opacity: 0; height: 150px; cursor: pointer;" >
                    <img src="<?php echo !empty($frm["img"])?$this->documento->getUrlBase().$frm["img"]:($this->documento->getUrlStatic()."/img/sistema/alumno_avatar.png");?>" class="img-responsive center-block" alt="imgfoto" data-ancho="100"  data-alto="100" style="height: 135px;  display: inline;">
                  </div>
                </div>
              </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * </span>:
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <a style="cursor:pointer;" class="chkformulario fa <?php echo @$frm["estado"]=='A'?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["estado"];?>"  data-valueno="I" data-value2="<?php echo @$frm["estado"]=='A'?'A':'I';?>">
                <span> <?php echo JrTexto::_(@$frm["estado"]=='A'?"Activo":"Inactivo");?></span>
                <input type="hidden" name="txtEstado" value="<?php echo !empty($frm["estado"])?$frm["estado"]:'I';?>" > 
              </a>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtObservacion">
              <?php echo JrTexto::_('Observacion');?> :
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <textarea class="form-control" rows="3" name="txtObservacion" id="txtObservacion"></textarea>
            </div>
          </div>

          <div class="clearfix"><hr></div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtUsuario">
              <?php echo JrTexto::_('Usuario');?> <span class="required"> * </span>:
            </label>
            <div class="col-md-5 col-sm-6 col-xs-12">
              <input type="text"  id="txtUsuario" name="txtUsuario" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["usuario"];?>" maxlength="15">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtClave">
              <?php echo JrTexto::_('Clave');?> <span class="required"> * </span>:
            </label>
            <div class="col-md-5 col-sm-6 col-xs-12">
              <input type="password" id="txtClave" name="txtClave" class="form-control" required="required"/>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtClaveConfirmar">
              <?php echo JrTexto::_('Confirmar clave');?> :
            </label>
            <div class="col-md-5 col-sm-6 col-xs-12">
              <input type="password" id="txtClaveConfirmar" name="txtClaveConfirmar" class="form-control"/> 
            </div>
          </div>

          <div class="clearfix"><hr></div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtIdcargo">
              <?php echo JrTexto::_('Área');?> <span class="required"> * </span>:
            </label>
            <div class="col-md-5 col-sm-6 col-xs-12">
              <select id="txtIdcargo" name="txtIdcargo" class="form-control">
                <option value="">- <?php echo ucfirst(JrTexto::_('Select')); ?> -</option>
                <?php  if(!empty($this->area))
                foreach ($this->area as $a) { ?>
                <option value="<?php echo $a["idcargo"]?>" <?php echo $a["idcargo"]==@$frm["idcargo"]?"selected":""; ?> ><?php echo $a["descripcion"] ?></option>
                <?php } ?> 
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-4 col-xs-12" for="txtIdugel">
              <?php echo JrTexto::_('Cargo');?> <span class="required"> * </span>:
            </label>
            <div class="col-md-5 col-sm-6 col-xs-12">
              <select id="txtIdugel" name="txtIdugel" class="form-control">
                <option value="">- <?php echo ucfirst(JrTexto::_('Select')); ?> -</option>
                <option value="J"><?php echo JrTexto::_('Head of area'); ?></option>
                <option value="E"><?php echo JrTexto::_('Employee'); ?></option>
              </select>
            </div>
          </div>

          <div class="ln_solid"></div>
          
          <div class="form-group text-center">
            <div class="col-xs-12">
              <a type="button" class="btn btn-default btn-lg" href="javascript:history.back();"><i class=" fa fa-arrow-left"></i> <?php echo ucfirst(JrTexto::_('Go Back'));?></a>&nbsp;&nbsp;
              <button id="btn-saveAlumno" type="submit" class="btn btn-success btn-lg" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            </div>
          </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
var subirmedia=function(tipo,file, $target){
  var formData = new FormData();
  formData.append("tipo",tipo);
  formData.append("filearchivo",file[0].files[0]);
  $.ajax({
    url: '<?php echo $this->documento->getUrlBase(); ?>/alumno/xSubirFoto',
    type: "POST",
    data:  formData,
    contentType: false,
    processData: false,
    dataType :'json',
    cache: false,
    processData:false,
    xhr:function(){
      var xhr = new window.XMLHttpRequest();
      /*Upload progress*/
      xhr.upload.addEventListener("progress", function(evt){

        if (evt.lengthComputable) {
          var percentComplete = Math.floor((evt.loaded*100) / evt.total);            
          $('#divprecargafile .progress-bar').width(percentComplete+'%');
          $('#divprecargafile .progress-bar span').text(percentComplete+'%');
        }
      }, false);
      /*Download progress*/
      xhr.addEventListener("progress", function(evt){
        if (evt.lengthComputable) {
          var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
          /*Do something with download progress*/
          /*console.log(percentComplete);*/
        }
      }, false);
      return xhr;
    },
    beforeSend: function(XMLHttpRequest){
      div=$('#divprecargafile');
      $('#divprecargafile').removeClass('progress-bar-danger progress-bar-success progress-bar-striped progress-bar-animated').addClass('progress-bar-striped progress-bar-animated');        
      $('#divprecargafile .progress-bar').width('0%');
      $('#divprecargafile .progress-bar span').text('0%'); 
      $('#divprecargafile').show('fast'); 
      $('#btn-saveBib_libro').attr('disabled','disabled');
    },      
    success: function(data){
      if(data.code==='ok'){
        $('#divprecargafile .progress-bar').width('100%');
        $('#divprecargafile .progress-bar').html('Complete <span>100%</span>');
        $('#divprecargafile').addClass('progress-bar-success').removeClass('progress-bar-animated');
        if(tipo=='image'){
          $target.siblings('#imgPortada').attr('src', _sysUrlStatic_+'/libreria/'+tipo+'/'+data.namelink);
        }else if(tipo=='pdf'){
          $target.closest('.btn-default').removeClass('btn-default').addClass('btn-info');
        }
        $target.attr('value', data.namelink);
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
        $('#btn-saveBib_libro').removeAttr('disabled');
      }else{
        $('#divprecargafile').addClass('progress-bar-warning progress-bar-animated');
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
        return false;
      }
    },
    error: function(e){
      $('#divprecargafile').addClass('progress-bar-danger progress-bar-animated');
      $('#divprecargafile .progress-bar').html('Error <span>-1%</span>'); 
      mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',e,'warning');
      return false;
    },
    complete: function(xhr){
      $('#divprecargafile .progress-bar').html('Complete <span>100%</span>'); 
      $('#divprecargafile').addClass('progress-bar-success progress-bar-animated').hide('slow');
    }
  });
};
$(document).ready(function(){  

  $('#txtFechanacimiento').datetimepicker({
    format: 'YYYY-MM-DD',
    locale: '<?php echo $this->documento->getIdioma()=="ES"? "es":"en";?>',
    maxDate: "<?php echo date("Y-m-d H:i:s") ?>",
  });

  $('#frm-<?php echo $id_vent;?>').bind({    
    submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'alumno', 'saveAlumno', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("alumno"))?>');
      }
    }
  });

  $('.subir_foto').on('change',function(e){
    var file=$(this);
    var tipo = $(this).attr('data-tipo');
    var target = '#'+$(this).attr('data-target');
    var $target = $(target);
    if(file.val()=='') return false;
    subirmedia(tipo,file, $target);
    e.preventDefault();
    e.stopPropagation();
  });

  $('.cargarfile').on('change',function(){
    var file=$(this);
    agregar_msj_interno('success', '<?php echo JrTexto::_("loading");?> '+file.attr('data-texto')+'...','msj-interno',true);
    $('#frmAlumno').attr('action',file.attr('data-action'));
    $('#frmAlumno').attr('target','if_cargar_'+file.attr('data-campo'));
    $('#frmAlumno').attr('enctype','multipart/form-data');
    $('#frmAlumno').submit();                
  });

  $('#txtClaveConfirmar').change(function(e) {
    var txtConfirmar = $(this).val();
    var txtClave = $('#txtClave').val();
    if(txtConfirmar!==txtClave){
      $(this).closest('.form-group').addClass('has-error');
      if($('#mensaje_claveconfirmar').length==0){
        $(this).closest('.form-group').append('<div class="col-xs-12 col-sm-3" id="mensaje_claveconfirmar"></div>');
      }
      $('#mensaje_claveconfirmar').html('<small class="color-danger">Las claves no coinciden.</small>');
    }else{
      $(this).closest('.form-group').removeClass('has-error');
      $('#mensaje_claveconfirmar').html('');
    }
  });

  $('*[required]').focusout(function(e) {
    var txt = $(this).val().trim();
    if(txt===''){ $(this).closest('.form-group').addClass('has-error'); }
    else{ $(this).closest('.form-group').removeClass('has-error'); }
  });

  $('#txtDni').change(function(e) {
    var $txtDni = $(this);
    var dni_val = $txtDni.val().trim();
    if(txtDni!=''){
      $.ajax({
        url: _sysUrlBase_+'/alumno/xBuscar',
        type: 'POST',
        dataType: 'json',
        data: {
          dni: dni_val,
          idempresa: '<?php echo $this->usuarioAct['idempresa']; ?>'
        },
        beforeSend: function(){
          if($('#mensaje_dni').length==0){$txtDni.closest('.form-group').append('<div class="col-xs-12 col-sm-4" id="mensaje_dni"></div>')}
          $('#mensaje_dni').html('<i class="fa fa-spinner fa-pulse fa-fw fa-2x"></i>');
          $('#btn-saveAlumno').attr('disabled', 'disabled');
        },
      }).done(function(resp) {
        if(resp.code="ok"){
          if(resp.data.length!=0){
            $('#mensaje_dni').html('<span class="color-danger">Este DNI ya está registrado</span>');
            $txtDni.closest('.form-group').addClass('has-error');
          }else{
            $('#mensaje_dni').html('');
            $txtDni.closest('.form-group').removeClass('has-error');
            $('#btn-saveAlumno').removeAttr('disabled');
          }
        }else{
          mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', resp.msj, 'error');
        }
      }).fail(function(xhr, textStatus, errorThrown) {
        throw errorThrown;
          mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
      });
    }
  });

});

$('.chkformulario').bind({
  click: function() {     
    if($(this).hasClass('fa-circle-o')) {
      $('span',this).text(' <?php echo JrTexto::_("Active");?>');
      $('input',this).val('A');
      $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
    }else {
      $('span',this).text(' <?php echo JrTexto::_("Inactive");?>');
      $('input',this).val('I');
      $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
    }      
  }
});

function result_archivo(result, mensaje,txt) {
  if(result) {
    $("#txt"+txt).val(mensaje);
    agregar_msj_interno2("success", '<?php echo JrTexto::_("File updated successfully");?>',"msj-interno2",true);
    var tipo=$("#id"+txt).attr("data-type");
    if(tipo=="imagen"||tipo=="video"||tipo=="audio")
      $("#id"+txt).removeAttr("style").attr("src", "https://192.168.11.55/pvingles.local" + mensaje );
    else
      $("#id"+txt).removeAttr("style").attr("href",  "https://192.168.11.55/pvingles.local" + mensaje );
  }else{
    agregar_msj_interno2("warning", mensaje,"msj-interno2",true);
  }
}
</script>

