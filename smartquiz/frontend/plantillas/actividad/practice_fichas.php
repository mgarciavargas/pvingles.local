<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(dirname(__FILE__))).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
$documento =&JrInstancia::getDocumento();
$met=empty($_GET["met"])?exit(0):$_GET["met"];
$orden=empty($_GET["ord"])?1:$_GET["ord"];
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
?>
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_fichas.css">

<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Practice">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Fichas">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">

<div class="plantilla plantilla-fichas" id="tmp_<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>">
    <div class="row nopreview _lista-fichas" id="lista-fichas<?php echo $idgui ?>">
        
    </div>
    <div class="row nopreview">
        <div class="col-xs-12 text-center botones-creacion">
            <a href="#" class="btn btn-primary add-ficha" data-clone-from="#tmp_<?php echo $idgui; ?> #clone" data-clone-to="#lista-fichas<?php echo $idgui ?>"><i class="fa fa-plus-square"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('group').' '.JrTexto::_('of').' '.JrTexto::_('sheet'); ?>s</a>
            <a href="#" class="btn btn-success generar-fichas" data-clone-from="#tmp_<?php echo $idgui; ?> #to_generate" data-clone-to="#ejerc-fichas<?php echo $idgui ?>"><i class="fa fa-rocket"></i> <?php echo JrTexto::_('finish').' '.JrTexto::_('and').' '.JrTexto::_('generate'); ?></a>
        </div>

        <div class="col-xs-12 text-center botones-editar" style="display: none;">
            <a href="#" class="btn btn-success back-edit"><i class="fa fa-pencil"></i> <?php echo JrTexto::_('back').' '.JrTexto::_('and').' '.JrTexto::_('edit'); ?></a>
        </div>

        <div class="hidden col-xs-12 ficha-edit" data-clase="f_" id="clone">
            <div class="col-xs-12 col-sm-11">
                <div class="col-xs-12 part-1">
                    <div class="col-xs-12 col-sm-5 col-md-3 btns-media">
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-default btn-block selectmedia renameDataUrl istooltip" data-tipo="image" data-url=".img_1_" title="<?php echo JrTexto::_('select').' '.JrTexto::_('image') ?>"><i class="fa fa-picture-o"></i></a>
                            <img src="" class="hidden    renameClass" data-clase="img_1_">
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-default btn-block selectmedia    renameDataUrl istooltip"  data-tipo="audio" data-url=".audio_1_" title="<?php echo JrTexto::_('select') ?> audio"><i class="fa fa-headphones"></i></a>
                            <audio src="" class="hidden    renameClass" data-clase="audio_1_"></audio>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-9">
                        <input type="text" class="form-control    renameClass" data-clase="txt_1_" name="txtPalabra">
                    </div>
                </div>

                <div class="col-xs-12 part-2">
                    <hr>
                    <div class="col-xs-12 ficha-row">
                        <div class="col-xs-12 col-sm-5 col-md-3 btns-media">
                            <div class="col-xs-6">
                                <a href="#" class="btn btn-default btn-block selectmedia renameDataUrl istooltip" data-tipo="image" data-url=".img_2_" title="<?php echo JrTexto::_('select').' '.JrTexto::_('image') ?>"><i class="fa fa-picture-o"></i></a>
                                <img src="" class="hidden    renameClass" data-clase="img_2_">
                            </div>
                            <div class="col-xs-6">
                                <a href="#" class="btn btn-default btn-block selectmedia     renameDataUrl istooltip" data-tipo="audio" data-url=".audio_2_" title="<?php echo JrTexto::_('select') ?> audio"><i class="fa fa-headphones"></i></a>
                                <audio src="" class="hidden    renameClass" data-clase="audio_2_"></audio>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-9">
                            <input type="text" class="form-control    renameClass" data-clase="txt_2_" name="txtPalabra">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-1 btns-ficha">
                <a href="#" class="col-xs-6 col-sm-12 btn btn-danger delete-ficha istooltip" title="<?php echo JrTexto::_('discard').' '.JrTexto::_('sheet'); ?>"><i class="fa fa-trash-o"></i></a>
                
                <a href="#" class="col-xs-6 col-sm-12 btn btn-primary add-ficha-row istooltip" data-clone-from="#clone_row" data-clone-to="#f_ .part-2" title="<?php echo JrTexto::_('add').' '.JrTexto::_('row'); ?>"><i class="fa fa-plus-square-o"></i></a>
            </div>
        </div>

        <div class="hidden col-xs-12 ficha-row" id="clone_row">
            <div class="col-xs-12 col-sm-5 col-md-3 btns-media">
                <div class="col-xs-6">
                    <a href="#" class="btn btn-default btn-block selectmedia renameDataUrl istooltip" data-tipo="image" data-url=".img_2_"  title="<?php echo JrTexto::_('Select').' '.JrTexto::_('image') ?>"><i class="fa fa-picture-o"></i></a>
                    <img src="" class="hidden    renameClass" data-clase="img_2_">
                </div>
                <div class="col-xs-6">
                    <a href="#" class="btn btn-default btn-block selectmedia renameDataUrl istooltip" data-tipo="audio" data-url=".audio_2_" title="<?php echo JrTexto::_('select') ?> audio"><i class="fa fa-headphones"></i></a>
                    <audio src="" class="hidden    renameClass" data-clase="audio_2_"></audio>
                </div>
            </div>
            <div class="col-xs-10 col-sm-5 col-md-8">
                <input type="text" class="form-control    renameClass" data-clase="txt_2_" name="txtPalabra">
            </div>
            <div class="col-xs-2 col-sm-2 col-md-1">
                <a href="#" class="btn color-danger delete-ficha-row istooltip" title="<?php echo JrTexto::_('discard').' '.JrTexto::_('row'); ?>"><i class="fa fa-trash"></i></a>
            </div>
        </div>

        <div class="hidden ficha" data-key="" id="to_generate">
            <img src="" class="img-responsive hide">
            <a href="#" class="btn btn-orange btn-block hide"><i class="fa fa-play"></i></a>
            <p class="hide">word</p>
        </div>
    </div>
    <div class="row ejerc-fichas tpl_plantilla" id="ejerc-fichas<?php echo $idgui ?>" style="display: none;">
        <div class="col-xs-12 partes-1">

        </div>
        <div class="col-xs-12 partes-2">

        </div>

        <audio src="" class="hidden" id="audio-ejercicio<?php echo $idgui ?>" style="display: none;"></audio>
    </div>
</div>