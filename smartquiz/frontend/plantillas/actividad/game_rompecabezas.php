<?php 
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta.'ini_app.php');
    defined('RUTA_BASE') or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $idgui = uniqid();
    $usuarioAct = NegSesion::getUsuario();
    $nivel=empty($_GET["nivel"])?1:$_GET["nivel"];
    $unidad=empty($_GET["unidad"])?1:$_GET["unidad"];
    $actividad=empty($_GET["actividad"])?1:$_GET["actividad"];
    $idgame=empty($_GET["idgame"])?null:$_GET["idgame"];
?>
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_rompecabezas.css">
<style type="text/css">
  .edit-puzzle{
    z-index: 9;
  }  
</style>
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<div class="plantilla plantilla-rompecabezas" data-idgui="<?php echo $idgui ?>">
    <div class="row botones-puzzle nopreview" style="display: none;">
        <div class="col-xs-12 col-sm-10 pull-right text-right">
            <a class="btn btn-success edit-puzzle">
                <i class="fa fa-pencil-square-o"></i> 
                <?php echo JrTexto::_('Edit').' '.JrTexto::_('puzzle') ?>
            </a>
            <a class="btn btn-primary save-puzzle">
                <i class="fa fa-floppy-o"></i> 
                <?php echo JrTexto::_('Save').' '.JrTexto::_('puzzle') ?>
            </a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-5 video-loader nopreview">
            <div class="btnselectedfile btn btn-primary nopreview" data-tipo="image" data-url=".img_1<?php echo $idgui ?>">
                <i class="fa fa-image" ></i> 
                <?php echo ucfirst(JrTexto::_('select').' '.JrTexto::_('image')); ?>
            </div>
            <div class="btnselectedfile btn btn-orange nopreview" data-tipo="audio" data-url=".audioimg_1<?php echo $idgui ?>">
                <i class="fa fa-music" ></i> 
                <?php echo ucfirst(JrTexto::_('select')); ?> audio
            </div>
        </div>
        <div class="col-xs-12" id="puzzle-containment">              
            <div class="col-xs-6 text-center">
                <img  class="img_1<?php echo $idgui ?> img-responsive valvideo" src="" style="display: none">
                <img src="<?php echo $documento->getUrlStatic(); ?>/media/web/noimage.png" class="img-responsive img-thumbnail embed-responsive-item">
            </div>
            <div class="col-xs-6" >
                <div id="pile" style="display: none;"></div>
            </div>
            <div id="panel_finish" class="col-xs-6" style="display: none;">
                <h2 class="palabra-respuesta"></h2>
                <a href="#" class="btn btn-default start-puzzle"><i class="fa fa-undo"></i> <?php echo JrTexto::_('restart') ?></a>
            </div>
        </div>
    </div>
    <br>
    <div class="row rspta-nivel-contenedor nopreview">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group palabra-rspta">
                <label><?php echo JrTexto::_('what is in the image?') ?></label>
                <input type="text" name="txtRespuesta" id="txtRespuesta" class="form-control" disabled="disabled">
                <span class="glyphicon glyphicon-warning-sign form-control-feedback hidden"></span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 nivel-dificultad">
            <label><?php echo JrTexto::_('choose difficulty') ?>: </label>
            <div class="btn-group btn-group-justified" role="group">
                <div class="btn-group" role="group">
                    <a class="btn btn-default disabled start-puzzle" data-grid="3"><i class="fa fa-star"></i></a>
                </div>
                <div class="btn-group" role="group">
                    <a class="btn btn-default start-puzzle disabled" data-grid="4"><i class="fa fa-star"></i><i class="fa fa-star"></i></a>
                </div>
                <div class="btn-group" role="group">
                    <a class="btn btn-default start-puzzle disabled" data-grid="6"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></a>
                </div>
                <div class="btn-group" role="group">
                    <a class="btn btn-default start-puzzle disabled" data-grid="10"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></a>
                </div>
            </div>
        </div>

    </div>
    <audio src="" controls="true" class="audioimg_1<?php echo $idgui ?> hidden" style="display: none !important;"></audio>
</div>
<script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery-ui.min.js"></script>
<script type="text/javascript">
$('.plantilla-rompecabezas .btnselectedfile').click(function(e){ 
    var txt="<?php echo JrTexto::_('Selected or upload'); ?> ";
    selectedfile(e,this,txt);
});

$('.img_1<?php echo $idgui ?>').load(function() {
    var src = $(this).attr('src');
    if(src!=''){
        $('.palabra-rspta input[disabled="disabled"]').removeAttr('disabled');
        $('.palabra-rspta input').focus();
        $('.nivel-dificultad a.disabled').removeClass('disabled');
    }
});


var actualizarPanelFinish = function(id_pnlFinish, cant_grid){
    var $pnlFinish = $(id_pnlFinish);
    var rspta = $('#txtRespuesta').val();
    $pnlFinish.find('.palabra-respuesta').text(rspta);
    $pnlFinish.find('a.start-puzzle').attr('data-grid', cant_grid);
};

var validarInput = function(){
    var $input = $('input#txtRespuesta');
    if($input.length>0){ /* en vista de edicion (Admin) */
        if($input.val()!='' && $input.val()!=undefined ){
            if( $input.parent().hasClass('has-warning') ){
                $input.parent().removeClass('has-warning has-feedback');
                $input.siblings('span.glyphicon').addClass('hidden');
            }
            $input.attr('value', $input.val());
            var rspta = true;
        } else {
            $input.parent().addClass('has-warning has-feedback');
            $input.siblings('span.glyphicon').removeClass('hidden');
            $input.focus();
            var rspta = false;
        }
    } else { /* en vista de (Alumno) */
        var rspta = true;
    }
    return rspta;
};

$('.start-puzzle').click(function(e) {
    e.preventDefault();
    if( validarInput() ){
        $('.btn.edit-puzzle').addClass('active');
        var container    = '#puzzle-containment';
        var id_img       = '.img_1<?php echo $idgui ?>';
        var id_pnlFinish = '#panel_finish';
        var id_pila      = '#pile';
        var cant_grid    = $(this).data('grid');

        if( $(container).hasClass('puzzle-started') ){
            $(id_img).snapPuzzle('destroy');
            $(container).removeClass('puzzle-started');
        }
        
        $(id_img).attr('data-width', $(id_img).width());
        $(id_img).attr('data-height', $(id_img).height());

        start_puzzle(container, id_pnlFinish, id_img, id_pila, cant_grid);

        $('.plantilla-rompecabezas .botones-puzzle').show('fast');
        $('.plantilla-rompecabezas .video-loader').hide('fast');
        $(id_pila).show();
        $(this).parents('.rspta-nivel-contenedor').hide();
        actualizarPanelFinish(id_pnlFinish, cant_grid);

    }
});

$('.btn.edit-puzzle').click(function(e) {
    e.preventDefault();
    $(this).removeClass('active');
    var container    = '#puzzle-containment';
    var id_img       = '.img_1<?php echo $idgui ?>';
    var id_pila      = '#pile';
    var id_pnlFinish = '#panel_finish';

    if( $(container).hasClass('puzzle-started') ){
        $(id_img).snapPuzzle('destroy');
        $(container).removeClass('puzzle-started');

        $('.plantilla-rompecabezas .botones-puzzle').hide('fast');
        $('.plantilla-rompecabezas .video-loader').show('fast');
        $(id_pila).hide();
        $('.rspta-nivel-contenedor').show('fast');
        $(id_pnlFinish).hide();
    }
});

$('#txtRespuesta').keyup(function(e) {
    validarInput();
});

$(window).resize(function(){
    var container    = '#puzzle-containment';
    var id_img       = '.img_1<?php echo $idgui ?>';
    var id_pila      = '#pile';

    if( $(container).hasClass('puzzle-started') ){
        $(id_pila).height($(id_img).height());
        $(id_img).snapPuzzle('refresh');
    }
});

$('h2.palabra-respuesta').click(function(e) {
    reproducirAudio();
});

$('.btn.save-puzzle').click(function(e) {
    e.preventDefault();
    msjes = {
        'attention' : '<?php echo JrTexto::_("Attention");?>',
        'guardado_correcto' : '<?php echo JrTexto::_("Game").' '.JrTexto::_("saved successfully");?>'
    };
    var data={
      idgame:$('#idgame').val(),
      nivel:'<?php echo $nivel;?>',
      unidad:'<?php echo $unidad;?>',
      actividad:'<?php echo $actividad;?>',
      texto:$('.games-main').html(),
      titulo:$('.titulo-game').val(),
      descripcion:$('.descripcion-game').val()
    }
    var idgame=saveGame(data,msjes);
    $('#idgame').val(idgame);
});

/* 
* esta funcion esta en 'tool_games.js' y se 
* debe iniciar usando el plugin iniciarJuego() en 'plugin_games.js'
*/

/*var iniciarRompecabezas = function(){
    var img_src = $('.img_1<?php echo $idgui ?>').attr('src');
    if(img_src.length>0){
        var container    = '#puzzle-containment';
        var img          = '.img_1<?php echo $idgui ?>';
        var id_pnlFinish = '#panel_finish';
        var id_pila      = '#pile';
        var cant_grid    = $(id_pnlFinish).find('a.start-puzzle').data('grid');

        $(id_pila).html('');
        $(id_pila).removeClass('snappuzzle-pile');

        $(img).siblings('.snappuzzle-slot').remove();
        $(img).unwrap();
        $('#puzzle-containment').removeClass('puzzle-started');
        $(id_pila).height($(img).data('height'));
        start_puzzle( container, id_pnlFinish, img, id_pila, cant_grid )

    }

    var valRol = $('#hRol').val();
    if( valRol!='' && valRol!=undefined ){
        if(valRol != 1){
            $('.plantilla-rompecabezas').find('.nopreview').remove();
        }
    }

};*/

$(document).ready(function() {
    /*iniciarRompecabezas();*/
});

</script>
<div class="modal fade" id="modalclone" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close cerrarmodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="modaltitle"></h4>
      </div>
      <div class="modal-body" id="modalcontent">
        <h4><i class="fa fa-cog fa-spin fa-fw"></i> <?php echo JrTexto::_('Loading') ?></h4>
      </div>
      <div class="modal-footer" id="modalfooter">        
        <button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
      </div>
    </div>
  </div>
</div>