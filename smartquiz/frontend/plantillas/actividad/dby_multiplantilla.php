<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(dirname(__FILE__))).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
$documento =&JrInstancia::getDocumento();
$met=empty($_GET["met"])?exit(0):$_GET["met"];
$orden=empty($_GET["ord"])?1:$_GET["ord"];
$intentos=$_GET["inten"];
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
?>
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_completar.css">

<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="DBY">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Arrastre">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<div class="plantilla plantilla-completar" id="tmp_<?php echo $idgui; ?>" data-tipo-tmp='' data-idgui="<?php echo $idgui ?>">
  <div class="row">
    <div class="col-md-12">
      <div class="tpl_plantilla" data-idgui="<?php echo $idgui; ?>">
        <div class="row active" id="tpl<?php echo $idgui; ?>">
          <div class="col-md-12 col-sm-12 col-xs-12 discol1" style="padding: 1ex;" id="txt<?php echo $idgui; ?>">
            <div class="col-md-12 col-sm-12 col-xs-12 removelineedit img-thumbnail" >
              <div class="panel panelEjercicio" id="pnl_edithtml<?php echo $idgui; ?>">
              </div>              
            </div>            
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 discol2" style="padding: 1ex;" id="alter<?php echo $idgui; ?>" >
            <div class="col-md-12 col-sm-12 col-xs-12 removelineedit2 tpl_alternatives img-thumbnail " >
              <div class="panel panelAlternativas text-center" id="pnl_editalternatives<?php echo $idgui; ?>">
              </div>
            </div>
          </div>
        </div>        
      </div>       
      <textarea class="txtareaedit" id="txtarea<?php echo $idgui; ?>" style="display: none"></textarea>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  iniciarCompletar_DBY('<?php echo $idgui; ?>');
});
</script>