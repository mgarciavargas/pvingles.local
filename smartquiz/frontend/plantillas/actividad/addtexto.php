<?php 
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta.'ini_app.php');
    defined('RUTA_BASE') or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $adonde=empty($_GET["adonde"])?'':$_GET["adonde"];
    $func=empty($_GET["fun"])?'':$_GET["fun"];
    $code=empty($_GET["code"])?'':$_GET["code"];
    $idgui = uniqid();
?>
<div class="form-group">
    <label for="texto"><?php echo JrTexto::_('Add text'); ?></label>
    <input type="text" class="form-control txt<?php echo $idgui; ?>" >
</div>
<hr>
<div class="botones-accion" style="overflow: hidden;">
<button class="btn btn-primary saveguardar<?php echo $idgui; ?> pull-right"><i class="fa fa-save"></i></button>
</div>
<script type="text/javascript">
    $('.saveguardar<?php echo $idgui; ?>').click(function(){
        var txtadd=$('.txt<?php echo $idgui; ?>').val();
      <?php if(!empty($adonde)){?>
      $('<?php echo $adonde;?>').html(txtadd);
      $('<?php echo $adonde;?>_in').val(txtadd);
      <?php } ?>
      <?php if(!empty($func)){?>        
        var code='<?php echo $code; ?>';
       <?php echo $func."(code,txtadd);";
       } ?>
      $('#addinfotxt').modal('hide');
    });
</script>