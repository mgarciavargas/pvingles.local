<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(__FILE__)).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
$documento =&JrInstancia::getDocumento();
?>
<style>
    #lista_plantillas{
        position: relative;
    }

    #lista_plantillas .menu-title{
        background-color: #1c82cc;
        color: #fff;
        margin: 0 -12px;
        padding: 10px 5px;
    }

    #lista_plantillas .close{
        background-color: #fff;
        border-radius: 50%;
        opacity: 1;
        position: absolute;
        right: 0;
        top: 0;
    }

    #lista_plantillas .img-responsive{
        padding: 0;
    }

    #lista_plantillas .nombre-tpl{
        font-size: 1.2em;
        font-weight: bold;
        margin-top: .8em;
    }
</style>

<div id="lista_plantillas">
    <a class="close color-red"><i class="fa fa-times-circle"></i></a>
    <h3 class="menu-title"><?php echo ucfirst(JrTexto::_('choose')).' '.JrTexto::_('template'); ?></h3>
    <ul class="list-group">
        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="image" data-multimedia="true" data-tipo="image" data-url=".image_question">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/examen/imagen.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('Image'); ?></div>
        </a>

        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="audio" data-multimedia="true" data-tipo="audio" data-url=".audio_question">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/audio.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('Audio'); ?></div>
        </a>

        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="video" data-multimedia="true" data-tipo="video" data-url=".video_question">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/video.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('Video'); ?></div>
        </a>

        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="click_drag" data-clase="isdrop" data-videodemo="examen/click_drag">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/examen/click_drag.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('Click and drag'); ?></div>
        </a>

        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="options" data-clase="isclicked" data-videodemo="examen/options">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/examen/options.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('Options'); ?></div>
        </a>

        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="select_box" data-clase="ischoice" data-videodemo="examen/select_box">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/examen/select_box.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('Select Box'); ?></div>
        </a>

        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="gap_fill" data-clase="iswrite"  data-videodemo="examen/select_box">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/examen/gap_fill.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('Gap fill'); ?></div>
        </a>

        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="true_false" data-videodemo="examen/true_false">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/examen/true_false.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('True or false'); ?></div>
        </a>

        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="join" data-videodemo="examen/join">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/examen/join.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('Join-Audio-Text-Image'); ?></div>
        </a>

        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="order_simple" data-videodemo="examen/order_simple">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/examen/order1.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('Put in order'); ?></div>
        </a>

        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="order_paragraph" data-videodemo="examen/order_paragraph">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/examen/order2.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('Put in order-Paragraph'); ?></div>
        </a>
        
        <a href="#" class=" row list-group-item opc-tmpl" data-tmpl="tag_image" data-videodemo="examen/tag_image">
            <img src="<?php echo $documento->getUrlStatic(); ?>/sysplantillas/examen/tag_image.png" class="col-xs-3 img-responsive" alt="tpl-img">
            <div class="col-xs-9 nombre-tpl"><?php echo JrTexto::_('Tag image'); ?></div>
        </a>
    </ul>
</div>