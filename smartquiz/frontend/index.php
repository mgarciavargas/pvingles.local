<?php
/**
 * @autor       Abel Chingo Tello
 * @fecha       08/09/2016
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR . 'ini_app.php');
$aplicacion->iniciar();
//$idi = !empty(NegSesion::get('idioma','idioma__'))?NegSesion::get('idioma','idioma__'):NegConfiguracion::get_('idioma_defecto');
//$documento = &JrInstancia::getDocumento();
//$documento->setIdioma($idi);
$aplicacion->enrutar(JrPeticion::getPeticion(0), JrPeticion::getPeticion(1));
$aplicacion->despachar();
echo $aplicacion->presentar();

function mostar__($rec, $ax) {
	global $aplicacion;	
	$aplicacion->iniciar();
	$aplicacion->enrutar($rec, $ax);
	echo $aplicacion->presentar();
}