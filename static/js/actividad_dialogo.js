var validarDialogo = function($pnActive){
    var idPaneDialogo = $pnActive.attr('id');
    var nroPage = $pnActive.parents('.plantilla').find('a[href="#'+idPaneDialogo+'"]').text();
    var cant_dbox_orden0 = $('.d-box[data-orden="0"]',$pnActive).length;
    if(cant_dbox_orden0 == 1){
        if($('.d-box[data-orden="1"]',$pnActive).attr('data-audio')==undefined){
            $('.d-box[data-orden="1"]',$pnActive).addClass('nopreview');
        }
        return {"result" : true, "msje" : ""};
    } else if(cant_dbox_orden0 > 1){
        var audio_box1 = $('.d-box.left',$pnActive).attr('data-audio');
        var audio_box2 = $('.d-box.right',$pnActive).attr('data-audio');

        if(audio_box1!=undefined && audio_box2!=undefined){
            return {"result" : false, "msje" : MSJES_PHP.no_hay_orden_en_escena+" "+nroPage+ "."};
        } else if (audio_box1!=undefined && audio_box2==undefined){
            $('.d-box.right',$pnActive).addClass('nopreview');
            return {"result" : true, "msje" : ".left"};
        } else if(audio_box1==undefined && audio_box2!=undefined){
            $('.d-box.left',$pnActive).addClass('nopreview');
            return {"result" : true, "msje" : ".right"};
        } else if(audio_box1==undefined && audio_box2==undefined){
            return {"result" : false, "msje" : MSJES_PHP.no_hay_audio_en_escena+" "+nroPage+"." };
        }
    } else if(cant_dbox_orden0 < 1){
        return {"result" : false, "msje" : MSJES_PHP.debes_establecer_orden};
    }
};
var stopAudio = function(){
    $("#curaudiodialogo").attr('src', '').removeAttr('data-orden');
    $("#curaudiodialogo").trigger("stop");
    $('#curaudiodialogo').attr('currentTime', 0);
    $('#playdialogo').attr('data-estado', 'stopped');
    if( $('#playdialogo i').hasClass('fa-stop') ) {
        $('#playdialogo i').removeClass('fa-stop').addClass('fa-play');
    }
    if( $('.preview').is(':visible') ){
        $('.tpanedialog.active .d-box').show();
    }
};
var playAudio = function(){
    var $pnActive=$('.tpanedialog.active');
    var valid_dialog = validarDialogo($pnActive);
    if(valid_dialog.result){
        $('#playdialogo').attr('data-estado', 'playing');
        if( $('#playdialogo i').hasClass('fa-play') ){
            $('#playdialogo i').removeClass('fa-play').addClass('fa-stop');
        }

        $('.d-box').hide();
        var $dboxPrimero=$(".d-box"+valid_dialog.msje+"[data-orden='0']",$pnActive);
        $dboxPrimero.show();
        var audiosrc = $dboxPrimero.attr('data-audio');
        if(audiosrc!=undefined){
            $('#curaudiodialogo').attr('src',_sysUrlStatic_+'/media/audio/'+audiosrc);
            $('#curaudiodialogo').attr('data-orden',1);

            /* <!-- here was "handlerAudio_ended" code --> */
            $("#curaudiodialogo").trigger("play");
        } else {
            stopAudio();
            mostrar_notificacion(MSJES_PHP.attention,MSJES_PHP.no_hay_audio,'error');
        }
    } else {
        mostrar_notificacion(MSJES_PHP.attention, valid_dialog.msje ,'error');
        stopAudio();
    }
};

var renombrarPageDialogo = function($contenedorPaginacion){
    var i = 1;
    $contenedorPaginacion.find('a[data-toggle="pill"]').each(function() {
        $(this).text(i);
        i++;
    });
};

var handlerAudio_ended = function( $this ){
    var estado = $('#playdialogo').attr('data-estado');
    $this.trigger("stop");
    if(estado=='playing'){
        var $pnActive=$('.tpanedialog.active');
        var nextOrden=$this.attr('data-orden');
        var $dbox=$(".d-box[data-orden='"+nextOrden+"']",$pnActive);

        if( !$dbox.hasClass('nopreview') ){
            $dbox.show();

            if($dbox.length>0){

                var audiosrc = $dbox.attr('data-audio');
                if(audiosrc != undefined){


                    $this.attr('src',_sysUrlStatic_+'/media/audio/'+audiosrc);
                    $this.attr('data-orden',parseInt(nextOrden)+1);
                    $this.trigger("play");
                } else {
                    stopAudio();
                    mostrar_notificacion(MSJES_PHP.attention_text, MSJES_PHP.no_hay_audio,'error');
                }
            }else{
                var $nextPage = $('.dialogue-pagination>ul>li.active').next('li');
                var data_toggle= $nextPage.find('a').attr('data-toggle');
                if(data_toggle==='pill'){ 
                    $nextPage.find('a[data-toggle="pill"]').trigger('click');

                } else {
                    stopAudio();
                }
            }
        } else {
            var $nextPage = $('.dialogue-pagination>ul>li.active').next('li');
            var data_toggle= $nextPage.find('a').attr('data-toggle');
            if(data_toggle==='pill'){ 
                $nextPage.find('a[data-toggle="pill"]').trigger('click');

            } else {
                stopAudio();
            }
        }
    }
    if( estado=='stopped' ){
        stopAudio();
    }
};