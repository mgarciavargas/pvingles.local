(function($){
    $.fn.tplcompletar=function(ev){
        var objcont=$(this);
        funcionalidaddraganddrop(objcont);
        objcont.on('focusin',  '.tabhijo.active input.mce-object-input', function(e){
            var idpnl=$(this).closest('.panel.panelEjercicio').attr('id');
            var params = {
                id : '#'+idpnl,
                attention_text: MSJES_PHP.attention,
                obj :this,
            };
            var isDBY = esDBYself( $(this) );
            if(isDBY){ dby_handlerInputMCE_focusin(e, params); }else{handlerInputMCE_focusin(e, params); }
        }).on('focusout', '.tabhijo.active input.mce-object-input', function(e){
            e.preventDefault();
            var _this = this;
            if( !$(_this).hasClass('iswrite') ) {
                return false
            }
            evaluarRespuesta(_this);
        }).on('click', '.plantilla-completar ul.options-list>li>a', function(e){
            e.preventDefault();
            e.stopPropagation();
            var _this = this;
            var isDBY = esDBYself( $(_this));
            var fn = function(){
                var value = $(_this).text();
                var input = $(_this).parents('ul.options-list').siblings('input.open');
                input.val(value);
                cerrarFlotantes(input);
                evaluarRespuesta(input);
            };

            if(isDBY){
                var isCompleto = inputMCE_Completo( $(_this) );
                var condicion = (isDBY && !isCompleto);
                var buscar = 'input.mce-object-input',
                buscarCorrectos = '.inp-corregido[data-corregido="good"]';
                fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
            }else{
                var value = $(this).text();
                var input = $(this).parents('ul.options-list').siblings('input.open');
                input.val(value);
                cerrarFlotantes(input);
                evaluarRespuesta(input);
            }
        }).on('click', '.plantilla-completar div.helper-zone>a.btn-help', function(e){
            e.preventDefault();
            e.stopPropagation();
            var $tmplActiva = getTmplActiva();
            var $plantActiva = $(this).closest('.plantilla');
            var isDBY = esDBYself( $(this));
            if( $plantActiva.hasClass('tiempo-acabo') && isDBY){
                return true;
            }
            var $inputMCE = $(this).parent().siblings('input[data-mce-object="input"]');
            
            console.log( '$inputMCE=', $inputMCE);
            
            if( $(this).find('span.tip-word').length==0 ){
                $(this).append('<span class="tip-word"> : </span>');
            }
            var rspta= $(this).parent().siblings('input.open').attr('data-texto');
            var arr_rspta= rspta.split("");
            
            console.log( 'rspta=', rspta);
            console.log( 'arr_rspta=', arr_rspta);

            var i = $('span.tip-word').attr('data-pulsado');
            
            console.log('i=',i);
            
            if(i==''||i==undefined){
                i = 0;
            }
            i = parseInt(i);
            if( i < arr_rspta.length){
                $('span.tip-word').attr('data-pulsado',(i+1));
                var texto = $('span.tip-word').text();
                texto = texto.concat(arr_rspta[i]);
                $('span.tip-word').text(texto);
            }
            $inputMCE.focus();
        }).on('keypress', '.plantilla-completar input.mce-object-input', function(e){
            var $plantActiva = $(this).closest('.plantilla');
            var isDBY = esDBYself( $(this));
            if( $plantActiva.hasClass('tiempo-acabo') && isDBY){
                return true;
            }
            if(e.which == 13){
                e.preventDefault();
                if( $(this).parents('.wrapper').hasClass('input-flotantes') ){
                    $(this).parents('.input-flotantes.wrapper').find('.helper-zone').remove();
                    $(this).parents('.input-flotantes.wrapper').find('.options-list').remove();
                }
                $(this).removeClass('active open');
                if($(this).parents('.wrapper').hasClass('input-flotantes')){
                    $(this).parents('.input-flotantes.wrapper').children().unwrap();
                }
                var isCompleto = inputMCE_Completo( $(this) );
                var condicion = ( isDBY && isCompleto );
                var buscar = 'input.mce-object-input',
                buscarCorrectos = '.inp-corregido[data-corregido="good"]';
                var value = $(this).val();
                var rspta_crrta = $(this).attr('data-texto');
                _this=$(this);
                var fn = function(){
                    evaluarRespuesta(_this);
                };
                if(isDBY) fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
                if(rspta_crrta.toLowerCase().trim() === value.toLowerCase().trim()){
                    var valor = 'good';
                    agregarIcon(this, valor);
                    fn();
                }else{
                     var valor = 'bad';
                    agregarIcon(this, valor);
                    fn();
                }
            }
        }).on('keyup', '.plantilla-completar input.mce-object-input', function(e){
            var _this = this;
            var buscar = 'input.mce-object-input',
                buscarCorrectos = '.inp-corregido[data-corregido="good"]';
            var isDBY = esDBYself( $(_this) );
            var isCompleto = inputMCE_Completo( $(_this) );
            var condicion = ( isDBY && isCompleto );

            if(e.which != 13){
                var value = $(this).val();
                var rspta_crrta = $(this).attr('data-texto');
                if(value.length>0){
                    if(rspta_crrta.toLowerCase().trim() === value.toLowerCase().trim()){
                        if( $(this).parents('.wrapper').hasClass('input-flotantes') ){
                            $(this).parents('.input-flotantes.wrapper').find('.helper-zone').remove();
                            $(this).parents('.input-flotantes.wrapper').find('.options-list').remove();
                        }
                        $(this).removeClass('active open');
                        if($(this).parents('.wrapper').hasClass('input-flotantes')){
                            $(this).parents('.input-flotantes.wrapper').children().unwrap();
                        }
                        var fn = function(){
                           evaluarRespuesta(_this);
                        };
                        if(isDBY) fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
                        else{
                            var valor = 'good';
                            agregarIcon(this, valor);
                        }
                        var $next_input = $(this).parents('p').children('input.mce-object-input');
                        if( $next_input.length>0 ){
                            $next_input.trigger('focusin');
                        } else {
                            $next_input = $(this).parents('p').next('p').children('input.mce-object-input');
                            $next_input.trigger('focusin');
                        }
                    }
                }
            }
        }).on('click', '.plantilla-completar .panelAlternativas ul.alternativas-list li.alt-item a', function(e){
            e.preventDefault();
            e.stopPropagation();
            var _this = this;
            var isDBY = esDBYself($(_this));
            var tpl=$(this).closest('.tpl_plantilla');
            var pnlEjercicio=tpl.find('.panel.panelEjercicio');
            var idpnl=pnlEjercicio.attr('id');
            var idgui=tpl.attr('data-idgui');
            if(isDBY){
                var condicion = (isDBY && !existeAltMarcada($('#pnl_editalternatives'+idgui,objcont)));
                var buscar = 'input.mce-object-input',
                buscarCorrectos = '.inp-corregido[data-corregido="good"]';
                var fn = function(){
                    $('#pnl_editalternatives'+idgui+' ul.alternativas-list li.alt-item a',objcont).each(function() {
                        if( $(_this).parent().hasClass('wrapper') ){
                            $(_this).siblings('span.inner-icon').remove();
                            $(_this).unwrap();
                        }
                    });
                    evaluarRsptaAltern(_this, idgui,tpl);
                };
                fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
            }else{
                    var seleccionada = $(this).text();
                    $('ul.alternativas-list li.alt-item a').each(function() {
                        if( $(this).parent().hasClass('wrapper') ){
                            $(this).siblings('span.inner-icon').remove();
                            $(this).unwrap();
                        }
                    });
                    var $inpClicked = $('#'+idpnl).find('input.isclicked[data-mce-object="input"]');
                    var arr_opc_corr = [];
                    if($inpClicked.length>0){
                        $inpClicked.each(function() {
                            var opc_corr = $(this).attr('data-texto');
                            arr_opc_corr.push(opc_corr);
                        });
                    }
                    var rspta_crrta = arr_opc_corr.join(' - ');
                    if(rspta_crrta === seleccionada){
                        agregarIcon(this, 'good');
                        agregarRsptaInput($('#'+idpnl,objcont),seleccionada);
                    } else {
                        agregarIcon(this, 'bad');
                        agregarRsptaInput($('#'+idpnl,objcont),seleccionada);
                    }
            }
        }).on('click', '.plantilla-completar  ul.nav-tabs.ejercicios li a', function (e, esSgte=false) {
            var $li = $(this).parent();
            if( $li.hasClass('disabled') && !esSgte){
                e.preventDefault();
                e.stopPropagation();
            }if(esSgte){
                if( $li.prev('li').hasClass('disabled') ) $li.prev('li').removeClass('disabled');
                if( $li.hasClass('disabled') ) $li.removeClass('disabled');
            }
        }).on('click', '.plantilla-completar .tpl_plantilla', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var panelEjercicio=$('.tabhijo.active .tpl_plantilla').find('.panel.panelEjercicio');
            var idpnl=panelEjercicio.attr('id');
            var $input = $('#'+idpnl+' input.mce-object-input.open');
            cerrarFlotantes($input);
        });

        //verdadero y falso
        objcont.on('click', '.plantilla-verdad_falso   .list-premises a.btn.delete', function(e){
            e.preventDefault();
            e.stopPropagation();
            $(this).parents('.premise').remove();
        }).on('click','.plantilla-verdad_falso  a.btn.add-premise',function(e) {
            e.preventDefault();
            e.stopPropagation();
            var clone_from = $(this).attr('data-clone-from');
            var clone_to = $(this).attr('data-clone-to');
            var new_premise = $(clone_from).clone();
            var cant_premises = $(clone_to+' .premise').length;
            if(cant_premises>0) {
                var nameInput= $(clone_to+' .premise:nth-child('+cant_premises+') .options input').attr('name').split('-');
                var new_index = parseInt(nameInput[1]) + 1;
            } else {
                var new_index = 1;
            }
            new_index = Date.now();
            new_premise.find('.options input').each(function() {
                var new_name = $(this).attr('name') + new_index;
                $(this).attr('name', new_name ); /*renombrando grupo de opciones Radio*/
            });
            new_premise.find('input#opcPremise-').each(function() {
                var new_id = $(this).attr('id') + new_index;
                $(this).attr('id', new_id); /*renombrando input hidden opc-correcta*/
            });
            new_premise.find('input.valPremise.nopreview').each(function() {
                var new_data = $(this).attr('data-nopreview') + new_index;
                $(this).attr('data-nopreview', new_data); /*renombrando input hidden opc-correcta*/
            });

            $(clone_to).append('<div class="col-xs-12 premise pr-'+new_index+'">'+ new_premise.html() +'</div>');
            $('.istooltip').tooltip();
        }).on('change','.plantilla-verdad_falso  .list-premises .options input[type="radio"].radio-ctrl', function(e){
            e.stopPropagation();
            var $this=$(this);
            var isDBY = esDBYself($this);
            if(!isDBY){
                var name = $this.attr('name');
                var value = $('input[name="'+name+'"].radio-ctrl:checked',objcont).val();
                var plantilla=$(this).closest('.plantilla');
                if(plantilla.hasClass('editando'))
                    asignarRsptaEdicion( $this,objcont );
                else
                    evaluarRsptaVoF($this,objcont);

            }else{
                var $iconoCorregido = $this.parents('.options').siblings('.icon-zone').find('.icon-result>i.fa');
                if( $iconoCorregido.length==0 ){
                    if( $('.preview').is(':visible') ){ //si está en vista de edicion
                        asignarRsptaEdicion( $this,objcont);
                    } else {
                        var condicion = esDBYself($this);
                        var fn = function(){
                            evaluarRsptaVoF($this,objcont);
                        };
                        var buscar = '.premise',
                        buscarCorrectos = '*[data-corregido="good"]';
                        fnTiempoIntentosPuntaje( condicion, fn, buscar, buscarCorrectos );
                    }
                } else {
                    var $sibling = $this.parent().siblings('label').find('input[type="radio"]');
                    $this.removeProp('checked');
                    $sibling.prop('checked', true);
                }
            }
        }).on('mouseup', '.plantilla-verdad_falso    list-premises .options label', function(e){
            e.stopPropagation();
            var isDBY = esDBYself($(this));
            if(!isDBY) return false;
            if( !$('.preview').is(':visible') ){
                var $sibling = $(this).siblings('label').find('input[type="radio"]');
                if( $sibling.is(':checked') ){
                    mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.no_puedes_seleccionar, 'warning');
                }
            }
        });


        //Plantilla  fichas
        objcont.on('click', '.ejerc-fichas .ficha', function(e) {
            e.stopPropagation();
            var isDBY = esDBYself($(this));

            if( $(this).parents('.partes-1').length!=0 ){
                var seleccionado = $(this).attr('data-key');
                if(CLAVE_ACTIVA != seleccionado){
                    CLAVE_ACTIVA = seleccionado;
                    desmarcarIncorrectas( isDBY,objcont );
                    $('.partes-1 .ficha[data-key="'+CLAVE_ACTIVA+'"]').addClass('active');
                } else {
                    $(this).removeClass('active');
                    desmarcarIncorrectas( isDBY,objcont );
                    CLAVE_ACTIVA = null;
                }
            }

            if( $(this).parents('.partes-2').length!=0 ){
                e.stopPropagation();

                if(CLAVE_ACTIVA!=null){
                    if( !$(this).hasClass('corregido') ){
                        $(this).toggleClass('active');
                        if(isDBY){
                            var $this = $(this);
                            var condicion = isDBY;
                            var buscarTodos = '.partes-2 .ficha';
                            var buscarCorrectos = '.partes-2 .ficha.corregido.good';
                            var fn = function(){
                                corregirFicha($this);
                            };
                            fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
                        } else {
                            corregirFicha( $(this) );
                        }
                        $('.partes-1 .ficha.active').removeClass('active');
                        CLAVE_ACTIVA = null;
                    } else if(isDBY){
                        mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.no_puedes_seleccionar, 'warning');
                    }
                }
            }
        }).on('click', '.ejerc-fichas .ficha a', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var $plantilla=$(this).parents('.plantilla-fichas');
            var idgui=$plantilla.attr('data-idgui');
            var $audio = $('#audio-ejercicio'+idgui),
            src = $(this).attr('data-audio');
            $audio.attr('src', _sysUrlStatic_ +'/media/audio/'+src);
            $audio.trigger('play');
        });
    /**
    *** EDICION del ejerc Fichas :
    **/
    objcont.on('click','.plantilla-fichas > ._lista-fichas .selectmedia', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var txt= MSJES_PHP.select_upload;
            lastBtnClicked = this;
            selectedfile(e,this,txt,'activarBtn');//activarBtn() en 'editactividad.js'
        }).on('click', '.plantilla-fichas > ._lista-fichas .delete-ficha', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).parents('.ficha-edit').remove();
        }).on('click', '.plantilla-fichas > ._lista-fichas .delete-ficha-row', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).parents('.ficha-row').remove();
        }).on('click', '.plantilla-fichas > ._lista-fichas .add-ficha-row', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var cloneFrom = $(this).attr('data-clone-from');
            var cloneTo = $(this).attr('data-clone-to');
            var index = $(this).parents('.ficha-edit').attr('id').split('_')[1];
            var now = Date.now();
            var newRow = $(cloneFrom).clone();

            newRow.find('.renameClass').each(function() {
                var clase = $(this).data('clase');
                $(this).addClass(clase+index+'_'+now);
                $(this).removeClass('renameClass').removeAttr('data-clase');
            });

            newRow.find('.renameDataUrl').each(function() {
                var url = $(this).data('url');
                $(this).removeAttr('data-url').attr('data-url', url+index+'_'+now);
                $(this).removeClass('renameDataUrl');
            });

            newRow.removeClass('hidden').removeAttr('id');

            $(cloneTo).append(newRow);
            $("*[data-tooltip=\"tooltip\"]").tooltip();
        }).on('click', '.plantilla-fichas .add-ficha',function(e) {
            e.preventDefault();
            e.stopPropagation();
            var cloneFrom = $(this).attr('data-clone-from');
            var cloneTo = $(this).attr('data-clone-to');
            var newFicha = $(cloneFrom).clone();
            var now = Date.now();

            newFicha.find('.renameClass').each(function() {
                var clase = $(this).attr('data-clase');
                $(this).addClass(clase+now);
                $(this).removeClass('renameClass').removeAttr('data-clase');
            });

            newFicha.find('.renameDataUrl').each(function() {
                var url = $(this).attr('data-url');
                $(this).removeAttr('data-url').attr('data-url', url+now);
                $(this).removeClass('renameDataUrl');
            });

            newFicha.find('.add-ficha-row').attr('data-clone-to', '#f_'+now+' .part-2');

            newFicha.removeClass('hidden').removeAttr('id');
            newFicha.attr( 'id', newFicha.attr('data-clase')+now ).removeAttr('data-clase');

            $(cloneTo).append(newFicha);
            $("*[data-tooltip=\"tooltip\"]").tooltip();
        }).on('click','.plantilla-fichas .generar-fichas',function(e) {
            e.preventDefault();
            e.stopPropagation();
            var idCloneFrom = $(this).attr('data-clone-from');
            var idCloneTo = $(this).attr('data-clone-to');
            var $plantilla=$(this).parents('.plantilla-fichas');
            var idgui=$plantilla.attr('data-idgui');
            var resp = generarFichas(idCloneFrom, idCloneTo);
            if(resp){
                $(idCloneTo).show();
                $('#lista-fichas'+idgui).hide().addClass('hidden');
                $('#tmp_'+idgui+' .botones-creacion').hide().addClass('hidden');
                $('#tmp_'+idgui+' .botones-editar').show();
            } else {
                $(idCloneTo).hide();
                $('#lista-fichas'+idgui).show().removeClass('hidden');
                $('#tmp_'+idgui+' .botones-creacion').show().removeClass('hidden');
                $('#tmp_'+idgui+' .botones-editar').hide();
            }
        }).on('click','.plantilla-fichas .botones-editar .back-edit', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var $plantilla=$(this).parents('.plantilla-fichas');
            var idgui=$plantilla.attr('data-idgui');
            $('#ejerc-fichas'+idgui).hide();
            $('#lista-fichas'+idgui).show().removeClass('hidden');
            $('#tmp_'+idgui+' .botones-creacion').show().removeClass('hidden');
            $('#tmp_'+idgui+' .botones-editar').hide();
            $('#ejerc-fichas'+idgui).find('.partes-1').html('');
            $('#ejerc-fichas'+idgui).find('.partes-2').html('');
        });


    //Plantilla Ordenar Simple
    objcont.on('click', '.plantilla-ordenar .selectmedia', function(e) {
            e.preventDefault();
            var txt = MSJES_PHP.select_upload;
            lastBtnClicked = this;
            selectedfile(e,this,txt,'activarBtn');
        }).on('focusin','.plantilla-ordenar input.txt-words', function(e) {
            var string = $(this).val();
            if( string.split(' ').length > 1 ) { TIPO_DIVISION = 'palabra'; }
            else { TIPO_DIVISION = 'letra'; }
        }).on('keyup', '.plantilla-ordenar input.txt-words', function(e) {
            e.preventDefault();
            var string = $(this).val();
            if( string == '' ){ TIPO_DIVISION = 'letra' }
            if(e.keyCode == 32){  TIPO_DIVISION = 'palabra'; }
            if(e.keyCode == 8){
                if( string.split(' ').length <= 1 ){ TIPO_DIVISION = 'letra'; }
            }

            $contenedor = $(this).parents('.inputs-elem').find('.list-elem-part');

            dividirCadena(string, $contenedor);
        }).on('click', '.plantilla-ordenar .list-elem-part>div', function(e) {
            $(this).toggleClass('fixed');
        }).on('click', '.plantilla-ordenar  .generar-elem',function(e) {
            e.preventDefault();
            e.stopPropagation();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();

            var idCloneFrom = $(this).data('clone-from');
            var idCloneTo = $(this).data('clone-to');
            var xy = generarElementOrdenar(idCloneFrom, idCloneTo, idguiTmpl);
            $('#ejerc-ordenar'+idguiTmpl).show();
            $('#lista-elem-edit'+idguiTmpl).hide().addClass('hidden');
            $('#'+idTmpl+' .botones-editar').show();
            $('#'+idTmpl+' .botones-creacion').hide().addClass('hidden');
            initOrdenarSimple(idguiTmpl);
        }).on('click', '.plantilla-ordenar .back-edit', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();

            $('#ejerc-ordenar'+idguiTmpl).hide();
            $('#lista-elem-edit'+idguiTmpl).show().removeClass('hidden');
            $('#'+idTmpl+'  .botones-editar').hide();
            $('#'+idTmpl+'  .botones-creacion').show().removeClass('hidden');
            $('#ejerc-ordenar'+idguiTmpl).html('');
        }).on('click', '.plantilla-ordenar .ejerc-ordenar .drag>div', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            var $this = $(this);
            var isDBY = esDBYself( $this );
            var condicion = isDBY;
            var buscarTodos = '.element';
            var buscarCorrectos = '.element[data-corregido="good"]';
            var fn = function(){
                var value= $this.text();
                var idElem = $this.parents('.element').attr('id');
                var $primerVacio = $('#'+idElem,objcont).find('.drop>div:empty').first();
                $primerVacio.html('<div>'+value+'</div>')
                $this.remove();
                evaluarOrdenCorrecto( $('#'+idElem,objcont) , idguiTmpl);
            };
            if(isDBY){
                fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
            }else{
                fn();
            }
        }).on('click', '.plantilla-ordenar .ejerc-ordenar .drop>div.parte.blank>div', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            var $this= $(this);
            var isDBY = esDBYself( $this );
            var condicion = !isDBY;
            var buscarTodos = '.element';
            var buscarCorrectos = '.element[data-corregido="good"]';
            var fn = function(){
                var value= $this;
                var $parents = $this.parents('.element');
                $parents.find('.drag').append(value);
                evaluarOrdenCorrecto( $parents , idguiTmpl);
            };
            if(isDBY){
                fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
            }else{
                fn();
            }
        }).on('click', '.multimedia a[data-audio]', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();

            var $audio = $('#audio-ordenar'+idguiTmpl),
            src = $(this).attr('data-audio');
            $audio.attr('src', _sysUrlStatic_+'/media/audio/'+src);
            $audio.trigger('play');
        });

//Plantilla Ordenar Parrafo
       objcont.on('click', '.plantilla-ordenar  .delete-parrafo', function(e) {
            e.preventDefault();
            $(this).parents('.parrafo-edit').remove();
        }).on('click', '.plantilla-ordenar  .move-parrafo', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();

            var idElemEdit = $(this).parents('.parrafo-edit').attr('id');
            var $listaElemEdit = $('#lista-parrafo-edit'+idguiTmpl),
                $parrafoEdit = $('#'+idElemEdit),
                idTextArea = $parrafoEdit.find('textarea').attr('id');

            if( $listaElemEdit.children('.parrafo-edit').length>1 ){
                saveEditor( idTextArea );
                if( $(this).hasClass('up') && $parrafoEdit.prev().length!=0 ){
                    var $parrafoPrev = $parrafoEdit.prev();
                    $parrafoEdit.insertBefore($parrafoPrev);
                }

                if( $(this).hasClass('down') && $parrafoEdit.next().length!=0 ){
                    var $parrafoNext = $parrafoEdit.next();
                    $parrafoEdit.insertAfter($parrafoNext);
                }
                initEditor( idTextArea );
            }
        }).on('click', '.plantilla-ordenar  .list-parrafo-part>div', function(e) {
            $(this).toggleClass('fixed');
        }).on('click', '.plantilla-ordenar  .add-parrafo', function(e) {
            e.preventDefault();
            var cloneFrom = $(this).attr('data-clone-from');
            var cloneTo = $(this).attr('data-clone-to');
            var newFicha = $(cloneFrom).clone();
            var now = Date.now();

            newFicha.find('.renameId').each(function() {
                var id = $(this).data('id');
                $(this).attr('id', id+now);
                $(this).removeClass('renameId').removeAttr('data-id');
            });

            newFicha.find('.renameClass').each(function() {
                var clase = $(this).data('clase');
                $(this).addClass(clase+now);
                $(this).removeClass('renameClass').removeAttr('data-clase');
            });

            newFicha.find('.renameDataUrl').each(function() {
                var url = $(this).data('url');
                $(this).removeAttr('data-url').attr('data-url', url+now);
                $(this).removeClass('renameDataUrl');
            });

            var new_IdParrafo = newFicha.data('id')+now;
            newFicha.removeAttr('id');
            newFicha.attr( 'id', new_IdParrafo ).removeAttr('data-id');

            $(cloneTo).append(newFicha);
            $("*[data-tooltip=\"tooltip\"]").tooltip();

            initEditor( 'txt_parrafo_'+now );
            $('#'+new_IdParrafo).removeClass('hidden');
        }).on('click', '.plantilla-ordenar  .generar-parrafo', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();

            var idCloneFrom = $(this).data('clone-from');
            var idCloneTo = $(this).data('clone-to');
            var result = generarParrafo(idCloneFrom, idCloneTo, idguiTmpl);
            $('#ejerc-ordenar'+idguiTmpl).show();
            $('#lista-parrafo-edit'+idguiTmpl).hide().addClass('hidden');
            $('#'+idTmpl+'  .botones-editar').show();
            $('#'+idTmpl+'  .botones-creacion').hide().addClass('hidden');
            initOrdenarParrafos(idguiTmpl, "en_US");
        }).on('click', '.plantilla-ordenar  .back-edit-parr', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();

            $('#ejerc-ordenar'+idguiTmpl).hide();
            iniciarTodosEditores( $('#lista-parrafo-edit'+idguiTmpl) );
            $('#lista-parrafo-edit'+idguiTmpl).show().removeClass('hidden');
            $('#'+idTmpl+'  .botones-editar').hide();
            $('#'+idTmpl+'  .botones-creacion').show().removeClass('hidden');
            $('#lista-parrafo-edit'+idguiTmpl+'  input[type="checkbox"]').each(function() {
                var isChecked = $(this).attr('checked');
                if(isChecked!=undefined){
                    $(this).removeAttr('checked');
                    $(this).prop('checked', true);
                }
            });
        }).on('click', '.plantilla-ordenar .ejerc-ordenar .drag-parr>div', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            var $this= $(this);
            var isDBY = esDBYself( $this );
            var condicion = isDBY;
            var buscarTodos = '.drop-parr>div.filled';
            var buscarCorrectos = '.drop-parr>div.good';
            var fn = function(){
                var value= $this.html();
                var id = $this.attr('id');
                var $ejercOrdenar = $this.parents('#ejerc-ordenar'+idguiTmpl);
                var primerVacio = $ejercOrdenar.find('.drop-parr div:empty').first();
                primerVacio.html(value);
                primerVacio.addClass('filled').attr('id', id);
                $this.remove();
                evaluarOrdenParrafo( primerVacio, idguiTmpl);
            };
            if(isDBY){
                fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
            }else{
                fn();
            }
        }).on('click', '.plantilla-ordenar .ejerc-ordenar .drop-parr>div.filled', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-ordenar').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            var $this= $(this);
            var isDBY = esDBYself( $this );
            var condicion = !isDBY;
            var buscarTodos = '.drop-parr>div.filled';
            var buscarCorrectos = '.drop-parr>div.good';
            var fn = function(){
                if(!$this.hasClass('finish')){
                    var value= $this.html();
                    var id = $this.attr('id');
                    var $ejercOrdenar = $this.parents('#ejerc-ordenar'+idguiTmpl);
                    $ejercOrdenar.find('.drag-parr').append('<div id="'+id+'">'+value+'</div>');
                    $this.html('').removeClass('filled').removeAttr('id');
                    evaluarOrdenParrafo( $this, idguiTmpl);
                }
            };
            if(isDBY){
                fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
            }else{
                fn();
            }
        });

//Plantilla Imagen Etiquetada (con puntos):
        objcont.on('click', '.plantilla-img_puntos .botones-edicion .start-tag', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            $('#'+idTmpl+'  .mask-dots').addClass('start-tagging');
            $(this).hide();
            $(this).siblings('.btn').addClass('disabled');
            $('#'+idTmpl+'  .stop-tag').show().removeClass('disabled');
        }).on('click', '.plantilla-img_puntos .botones-edicion  .stop-tag', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            $('#'+idTmpl+' .mask-dots').removeClass('start-tagging');
            $(this).hide();
            $(this).siblings('.btn').removeClass('disabled');
            $('#'+idTmpl+' .start-tag').show().removeClass('disabled');
        }).on('click', '.plantilla-img_puntos .botones-edicion  .delete-tag', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            $('#'+idTmpl+' .mask-dots').addClass('delete-tagging');
            $(this).hide();
            $(this).siblings('.btn').addClass('disabled');
            $('#'+idTmpl+' .stop-del-tag').show().removeClass('disabled');
        }).on('click', '.plantilla-img_puntos .botones-edicion  .stop-del-tag', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            $('#'+idTmpl+' .mask-dots').removeClass('delete-tagging');
            $(this).hide();
            $(this).siblings('.btn').removeClass('disabled');
            $('#'+idTmpl+' .delete-tag').show().removeClass('disabled');
        }).on('click', '.plantilla-img_puntos .botones-edicion  .generate-tag', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            $(this).hide();
            $(this).siblings('.btn').hide();
            $('#'+idTmpl+' .back-edit-tag').show();
            generarTags($('#'+idTmpl+' .tag-alts-edit'), $('#'+idTmpl+' .tag-alts'));
            $('#'+idTmpl+' .tag-alts-edit').addClass('hidden').hide();
            $('#'+idTmpl+' .mask-dots .dot-container').each(function() {
                $(this).removeClass('edition');
                $(this).find('.dot-tag').attr('data-number', $(this).find('.dot-tag').text() );
            });
            $('#'+idTmpl+' .tag-alts').show();
            $('#'+idTmpl+' .mask-dots').addClass('playing');
            playEjercicio(idguiTmpl, $('#'+idTmpl+' .tag-alts'));
        }).on('click', '.plantilla-img_puntos .botones-edicion  .back-edit-tag', function(e) {
            e.preventDefault();
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');

            $(this).hide();
            $(this).siblings('.btn').show();
            $('.btn.stop-tag, .btn.stop-del-tag').hide();
            $('#'+idTmpl+' .tag-alts').hide();
            $('#'+idTmpl+' .mask-dots .dot-container').each(function() {
                $(this).addClass('edition');
                $(this).find('.dot-tag').text( $(this).find('.dot-tag').attr('data-number') );
                $(this).find('.dot-tag').removeAttr('data-number').removeClass('corregido').removeClass('good').removeClass('bad');
                $(this).removeClass('hover');
            });
            $('#'+idTmpl+'  .tag-alts-edit').removeClass('hidden').show();
            $('#'+idTmpl+'  .mask-dots').removeClass('playing');
        }).on('click', '.plantilla-img_puntos .contenedor-img .mask-dots.start-tagging', function(e) {
            var tpl=$(this).parents('.plantilla-img_puntos');
            var idTmpl = tpl.attr('id');
            var idguiTmpl = tpl.attr('data-idgui');
            $('.clone_punto'+idguiTmpl).attr('id','clone_punto'+idguiTmpl);
            var resp = crearPunto(e, '#clone_punto'+idguiTmpl, '#'+idTmpl+' .mask-dots', idguiTmpl);
            crearInput(resp.cant, resp.now, MSJES_PHP.write_tag, idguiTmpl);
        }).on('click', '.plantilla-img_puntos .contenedor-img .mask-dots.delete-tagging .dot', function(e) {
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            eliminarTag( $(this).parent(), $('#'+idTmpl+' .tag-alts-edit') );
        }).on('click', '.plantilla-img_puntos .contenedor-img .mask-dots .dot-container.edition>.dot', function(e) {
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            var indexId = $(this).parent().attr('id').split('_')[1];
            $('#'+idTmpl+'   #edit_tag_'+indexId+'   input').focus();
        }).on('click', '.plantilla-img_puntos .contenedor-img .mask-dots.playing .dot', function(e) {
            //e.stopPropagation();
            var idTmpl = $(this).closest('.plantilla-img_puntos').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();
            if( HELP[idguiTmpl] ){
                if( $(this).parent().hasClass('hover') ){
                    $(this).parent().removeClass('hover');
                    $('#'+idTmpl+' .tag-alts>.tag').removeClass('active');
                } else {
                    $('#'+idTmpl+' .mask-dots.playing .dot-container').removeClass('hover');
                    $(this).parent().addClass('hover');
                    $('#'+idTmpl+' .tag-alts>.tag').addClass('active');
                }
            }else {
                if( !$(this).siblings('.dot-tag').hasClass('corregido') ){
                    if( $(this).parent().hasClass('hover') ){
                        $(this).parent().removeClass('hover');
                        $('#'+idTmpl+' .tag-alts>.tag').removeClass('active');
                    } else {
                        $('#'+idTmpl+' .mask-dots.playing .dot-container').removeClass('hover');
                        $(this).parent().addClass('hover');
                        $('#'+idTmpl+' .tag-alts>.tag').addClass('active');
                    }
                }
            }
        }).on('click', '.plantilla-img_puntos .contenedor-img .mask-dots.playing', function(e) {
            /*var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            if( $(this).children().hasClass('hover') ){
                $(this).children().removeClass('hover');
                $('#'+idTmpl+' .tag-alts>.tag').removeClass('active');
            }*/
        }).on('focusin', '.plantilla-img_puntos .tag-alts-edit input[type="text"]', function(e) {
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            var indexId = $(this).parent().attr('id').split('_').pop();
            $('#'+idTmpl+' .mask-dots #dot_'+indexId).addClass('hover');
        }).on('focusout', '.plantilla-img_puntos .tag-alts-edit input[type="text"]', function(e) {
            e.stopPropagation();
            var idTmpl = $(this).parents('.plantilla-img_puntos').attr('id');
            $('#'+idTmpl+' .mask-dots .dot-container').removeClass('hover');
        }).on('click', '.plantilla-img_puntos .tag-alts .tag.active', function(e) {
            e.preventDefault();
            var $this = $(this);
            var idTmpl = $this.parents('.plantilla-img_puntos').attr('id');
            var idguiTmpl = idTmpl.split('_').pop();

            var textTag = $this.text();
            var isDBY = esDBYself( $this );
            var condicion = isDBY;
            var buscarTodos = '.dot';
            var buscarCorrectos = '.dot-tag.corregido.good';
            var fn = function(){
                $('#'+idTmpl+' .mask-dots .dot-container.hover .dot-tag').text(textTag);
                evaluarTag($this, $('#'+idTmpl+' .mask-dots .dot-container.hover'), HELP[idguiTmpl], idguiTmpl);
            }
            if(isDBY){
                fnTiempoIntentosPuntaje(condicion, fn, buscarTodos, buscarCorrectos);
            }else{
                fn();
            }

            /*$('#'+idTmpl+' .mask-dots.playing').trigger('click');*/
            //Desactivar todos los puntos:
            if( $('#'+idTmpl+' .mask-dots.playing').children().hasClass('hover') ){
                $('#'+idTmpl+' .mask-dots.playing').children().removeClass('hover');
                $('#'+idTmpl+' .tag-alts>.tag').removeClass('active');
            }
        });

        //plantilla dialogo
        objcont.on('click','#btn-AddPage a', function(e) {
            e.preventDefault();
            if( $(this).hasClass('disabled') ) return false;
            var contenedor = ".dialogue-pagination>ul.nav-pills";
            var idTab = "page_";
            var index_tab = $(contenedor+' li').length;
            var now = Date.now();
            var src_ultima_img = $('.dialogue-container #page_'+now+' .image>img' ).attr('src');
            var newTab= '<li><a href="#'+idTab+now+'" data-toggle="pill">'+index_tab+'</a></li>';
            $('#btn-AddPage').before(newTab);
            var met=$('.metod.active').attr('data-idmet');
            var idgui=$('.metod.active .aquicargaplantilla').find('#idgui').val();
            var htmlpanel=$('.dialogue-container .tpanedialog:last-child').clone(true,true);

            htmlpanel.find('.d-box.left').removeAttr('class').addClass('d-box left box1_'+met+'_'+now);
            htmlpanel.find('.d-box.left').removeAttr('data-audio');
            htmlpanel.find('.d-box.left .d-bubble').attr('id','dialogo-texto_1_'+now);
            htmlpanel.find('.d-box.left .d-bubble .addtext').text('Click here to Add text');
            htmlpanel.find('.d-box.left .d-bubble input.valin').attr('data-nopreview','box1_'+met+'_'+now);
            htmlpanel.find('.d-box.left .btnselectedfile[data-tipo="audio"]').attr('data-url','.box1_'+met+'_'+now);

            htmlpanel.find('.dialogue-image .btnselectedfile').attr('data-url','.img_1'+met+'_'+idgui+'_'+now);
            htmlpanel.find('.dialogue-image .image img').attr('id','img_1'+met+'_'+idgui+'_'+now);
            htmlpanel.find('.dialogue-image .image img').removeAttr('class').addClass('img-responsive img-thumbnail img-thu img_1'+met+'_'+idgui+'_'+now);

            htmlpanel.find('.d-box.right').removeAttr('class').addClass('d-box right box2_'+met+'_'+now);
            htmlpanel.find('.d-box.right').removeAttr('data-audio');
            htmlpanel.find('.d-box.right .d-bubble').attr('id','dialogo-texto_2_'+now);
            htmlpanel.find('.d-box.right .d-bubble .addtext').text('Click here to Add text');
            htmlpanel.find('.d-box.right .d-bubble input.valin').attr('data-nopreview','box2_'+met+'_'+now);
            htmlpanel.find('.d-box.right .btnselectedfile[data-tipo="audio"]').attr('data-url','.box2_'+met+'_'+now);

            var newPanel = '<div id="'+idTab+now+'" class="tpanedialog tab-pane fade">'+htmlpanel.html()+'</div>';
            $('.dialogue-container').append(newPanel);
            $(contenedor+' a[href="#'+idTab+now+'"]').tab('show');
            $('.istooltip').tooltip();
        }).on('click', '#btn-DelPage a', function(e) {
            e.preventDefault();
            $(this).parents('.dialogue-pagination').find('li.active').remove();
            $(this).parents('.plantilla-dialogo').find('.dialogue-container .tpanedialog.active').remove();
            renombrarPageDialogo( $(this).parents('.dialogue-pagination') );
            $('.dialogue-pagination>ul>li>a[data-toggle="pill"]:first').trigger('click');
        }).on('click', '.dialogue-pagination ul.nav-pills li a', function(e) {
             $('#btn-AddPage a').addClass('disabled');
        }).on('shown.bs.tab', '.dialogue-pagination ul.nav-pills li a', function(e) {
            $('#btn-AddPage a').removeClass('disabled');
            if( $(this).parents('ul').find('a[data-toggle="pill"]').length>1 && !$(this).parents('ul').find('li:first-child').hasClass('active') ){
                if( $('.preview').is(':visible') ){
                    $('.dialogue-pagination #btn-DelPage').show(); /*mostrar*/
                }
            } else {
                $('.dialogue-pagination #btn-DelPage').hide(); /*ocultar*/
            }
        }).on('click','#playdialogo',function(){
            var estado = $(this).attr('data-estado');
            if( estado=='stopped' ){
                playAudio();
            }
            if( estado=='playing' ){
                stopAudio();
            }
        }).on('click','#replaydialogo',function(){
            $('.dialogue-pagination>ul>li>a[data-toggle="pill"]:first').trigger('click');
            $('#playdialogo').trigger('click');
        }).on('.dialogue-pagination shown.bs.tab', 'ul>li>a', function(e){
            var estado = $('#playdialogo').attr('data-estado')
            if( estado=='playing' ){
              playAudio();
            }
            if( estado=='stopped' ){
              stopAudio();
            }
        }).on('change', '.plantilla-dialogo .dialogue-container .opt-nro-orden', function(e) {
            e.stopPropagation();
            var $box = $(this).parents('.dialogue-boxes');
            var value = $(this).val();
            if( value != -1 ){ $(this).parents('.d-box').attr('data-orden', value); }
            if( value == -1 ) {var otherValue = -1;}
            if( value == 0 ) {var otherValue = 1;}
            if( value == 1 ) {var otherValue = 0;}
            if( $box.hasClass('left') ){ var otherBox = 'right'; }
            if( $box.hasClass('right') ){ var otherBox = 'left'; }
            var $otherSelectTag = $box.siblings('.dialogue-boxes.'+otherBox).find('.opt-nro-orden');
            $otherSelectTag.val(otherValue);
            if( otherValue!=-1 ){ $otherSelectTag.parents('.d-box').attr('data-orden', otherValue); }
        }).on('click', '.opt-nro-orden, .d-box', function(e) { e.stopPropagation(); });


        //LOOK dialogo
        objcont.on('click','.plantilla_video #txtdialogocole .playvideo',function(){
            var time=$(this).attr('data-time');
            var idmet=_curmetod.attr('data-idmet');
            var idgui=$('input#idgui',_curejercicio).val();
            var $video = $('#vid222',_curejercicio);
            if($video.attr('src')!=''){
              $('#vid222').trigger('stop');
              $('#vid222')[0].currentTime=time;
              $('#vid222').trigger('play');
            }
        }).on('click','.plantilla_video .texto-video .addclone',function(){
            var de=$(this).attr('data-source');
            var adonde=$(this).attr('data-donde');
            var clon=$(de).clone();
            $(adonde).append('<tr data-time="0" class="playvideo">'+clon.html()+'</tr>');
        });

//plantilla look_video_texto edicion
        objcont.on("click",".plantilla_video #txtdialogocole .removedialog", function(){
            $(this).parents('tr').remove();
        }).on('click','.plantilla_video #txtdialogocole .addtextvideo', function(e){
            if( $('.preview').is(':visible') ){
              e.stopPropagation();
              var in_=$(this);
              var texto_inicial = $(this).text().trim();
              $(this).attr('data-initial_val', texto_inicial);
              var title=$(this).attr('title');
              in_.html('<input type="text" required="required" class="form-control" id="inputEditTexto" placeholder="'+title+'" value="'+ texto_inicial +'">');
              if($(this).hasClass("addtime")){
                $('input#inputEditTexto',in_).addClass('addtime').attr('placeholder','00:00');
                $('input#inputEditTexto',in_).mask('99:99');
              }
              $('input#inputEditTexto',in_).focus().select();
            }
        }).on('blur','.plantilla_video #txtdialogocole input#inputEditTexto',function(){
            var dataesc=$(this).attr('data-esc');
            var vini=$(this).parent().attr('data-initial_val');
            var vin=$(this).val();
            if(dataesc||vin=='') _intxt=vini;
            else _intxt=vin;
            $(this).removeAttr('data-esc');
            $(this).parent().attr('data-initial_val',_intxt);
            if($(this).hasClass("addtime")){
              var time=_intxt.split(":");
              var h=0,m=0,s=0;
              if(time.length==3){h=parseInt(time[0]);m=parseInt(time[1]);s=parseInt(time[2])}
              if(time.length==2){m=parseInt(time[0]);s=parseInt(time[1])}
              if(time.length==1){s=parseInt(time[0])}
                s=(h*3600)+(m*60)+s;
              var parent = $(this).parent().parent().parent().parent().attr('data-time',s);
            }
            $(this).parent().text(_intxt);
        }).on('keypress','.plantilla_video #txtdialogocole input#inputEditTexto', function(e){
            if(e.which == 13){
              e.preventDefault();
              $(this).trigger('blur');
            }
        }).on('keyup','.plantilla_video #txtdialogocole input#inputEditTexto', function(e){
            if(e.which == 27){
              $(this).attr('data-esc',"1");
              $(this).trigger('blur');
            }
        });
    };
}(jQuery));
