$(document).ready(function() {
    initSlider($('#pnl-cursos .slider-cursos'));

    $('#botones-menu .btn-inicio').click(function(e) {
      e.preventDefault();
      borrarSubMenu();
      insertarSubMenu( $(this) );
    });

    $('#botones-menu').on('click', '.btn_submenu button.close', function(e) {
      e.preventDefault();
      borrarSubMenu( $(this).closest('.btn_submenu') );
    });
});

var optionslike={
    //dots: true,
    infinite: false,
    //speed: 300,
    //adaptiveHeight: true
    navigation: false,
    slidesToScroll: 1,
    //centerPadding: '60px',
    slidesToShow: 5,
    responsive:[
      { breakpoint: 1360, settings: {slidesToShow: 5} },
      { breakpoint: 1200, settings: {slidesToShow: 5} },
      { breakpoint: 992, settings: {slidesToShow: 4 } },
      { breakpoint: 880, settings: {slidesToShow: 4 } },
      { breakpoint: 700, settings: {slidesToShow: 3 } },
      { breakpoint: 480, settings: {slidesToShow: 2 } },
      { breakpoint: 320, settings: {slidesToShow: 1 /*,arrows: false, centerPadding: '40px',*/} }       
  ]
};

var initSlider = function($objToSlide){
  if($objToSlide.length==0) return false;
  $objToSlide.slick(optionslike);
}


var insertarSubMenu = function( $btn ) {
  var selector_clonar = $btn.attr('href');
  var $cloned = $('#submenu_botones '+selector_clonar).clone();
  $btn.after($cloned);
};

var borrarSubMenu = function( $selector ){
  $selector = $selector ||  null;
  if($selector == null) {
    $('#botones-menu .btn_submenu').remove();
  } else {
    $('#botones-menu').find($selector).remove();
  }
};