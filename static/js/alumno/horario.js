$(function() {
  /*loadEvents();*/
  showTodaysDate();
  initializeCalendar();
  getCalendars();
  /*initializeRightCalendar();*/
  //initializeLeftCalendar();
  disableEnter();
  cargarHorario();
});

/* --------------------------initialize calendar-------------------------- */
var initializeCalendar = function() {
    var idioma = (_sysIdioma_=='ES')?'es':'en';
  $('#calendar').fullCalendar({
      editable: false,
      startEditable: false,
      eventLimit: true, // allow "more" link when too many events
      // create events
      events: events(),
      defaultView: 'agendaWeek',
      slotDuration: '00:30:00',
      slotLabelFormat:'h:mm a',
      minTime:'07:00',
      maxTime:'22:00',
      //forceEventDuration: true,
      //height: screen.height - 360,
      timezone: 'America/Lima',
      allDaySlot: false,
      locale: idioma,
    });
};

/*var initializeLeftCalendar = function() {
  var idioma = (_sysIdioma_=='ES')?'es':'en';
  $('#calendar').fullCalendar('option', {
      header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listWeek'
      },
      slotDuration: '00:30:00',
      navLinks: false,
      selectable: true,
      selectHelper: true,
      locale: idioma,
      select: function(start, end) {
          newEvent(start,end);
      },
      eventClick: function(calEvent, jsEvent, view) {
          console.log('editEvent');
          editEvent(calEvent);
      },
      dayClick: function(date) {
          cal1GoTo(date);
      },
  });
}*/

var cargarHorario=function(){
    console.log('_cargarHorario');
    $.ajax({
        url:  _sysUrlBase_+'/agenda/xHorario',
        type: 'POST',
        dataType: 'json',
        data:{
          'idcurso':$('#idcurso').val(),
        },
    }).done(function(resp) {
        if(resp.code=='ok'){
            $.each(resp.data, function(index, val) {
              var interval = moment(val.start);
              var intervalEnd = moment(val.end);
              if(val.start!=null && val.end!=null && val.start<=val.finCiclo){
                $cal.fullCalendar('renderEvent', val, true);
              }



              /*for(var i=1;i<=val.semanas;i++){
                var j=1;
                interval.add(j, 'week').format();
                intervalEnd.add(j, 'week').format();
                var newStart=interval.format();
                var newStartE=intervalEnd.format();
                  if(val.start!=null && val.end!=null && val.start<=val.finCiclo){
                    $cal.fullCalendar('renderEvent', val, true);
                    val.start='';
                    val.end='';
                  }
                  val.start=newStart;
                  val.end=newStartE;
              }*/

            });
        }
    }).fail(function(err) {
        console.log("error: ", err);
    });    
};
