$(function() {
  /*loadEvents();*/
  showTodaysDate();
  //initializeCalendar();
  getCalendars();
  /*initializeRightCalendar();*/
  initializeLeftCalendar();
  disableEnter();
    cargarEventos();
    cargarTareas();
    cargarSmartclass();
    //cargarHorario();
});



/* -------------------manage cal1 (left pane)------------------- */
var initializeLeftCalendar = function() {
  var idioma = (_sysIdioma_=='ES')?'es':'en';
  var vistDefecto = 'month';
  var btnsDerecha = 'month,agendaWeek,agendaDay,listWeek';
  if($('#hVistaListado').val()==='true'){
    vistDefecto = 'listWeek';
    btnsDerecha = ''
  }
  $('#calendar').fullCalendar( {
      header: {
          left: 'prev,next today',
          center: 'title',
          right: btnsDerecha
      },
      defaultView: vistDefecto,
      slotDuration: '00:5:00',
      minTime:'06:00',
      maxTime:'22:00',
      navLinks: false,
      allDaySlot: false,
      selectable: true,
      selectHelper: true,
      locale: idioma,
      select: function(start, end) {
          console.log('newEvent');
          newEvent(start,end);
      },
      eventClick: function(calEvent, jsEvent, view) {
          console.log('editEvent');
          editEvent(calEvent);
      },
      dayClick: function(date) {
          cal1GoTo(date);
      },
  });
}

var cargarEventos=function(){
    $.ajax({
        url:  _sysUrlBase_+'/agenda/buscarjson',
        type: 'POST',
        dataType: 'json',
    }).done(function(resp) {
        if(resp.code=='ok'){
            $.each(resp.data, function(index, val) {
                $cal.fullCalendar('renderEvent', val, true);
            });
        }
    }).fail(function(err) {
        console.log("error: ", err);
    });
};
var cargarTareas=function(){
    $.ajax({
        url:  _sysUrlBase_+'/tarea/xTareas',
        type: 'POST',
        dataType: 'json',
    }).done(function(resp) {
        if(resp.code=='ok'){
            $.each(resp.data, function(index, val) {
                $cal.fullCalendar('renderEvent', val, true);
            });
        }
    }).fail(function(err) {
        console.log("error: ", err);
    });
};
var cargarSmartclass=function(){
    $.ajax({
        url:  _sysUrlBase_+'/aulasvirtuales/xAula',
        type: 'POST',
        dataType: 'json',
    }).done(function(resp) {
        if(resp.code=='ok'){
            $.each(resp.data, function(index, val) {
                $cal.fullCalendar('renderEvent', val, true);
            });
        }
    }).fail(function(err) {
        console.log("error: ", err);
    });
};

