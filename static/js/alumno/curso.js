var getCantNotif = function(){
	var $menuCurso = $("#menu-curso");
	if( $menuCurso.length==0 ){ return false; }
	$.ajax({
		url: _sysUrlBase_+"/curso/getCantidadNotif",
		type: 'POST',
		dataType: 'json',
		data: {id: $('#hCursoId').val() },
	}).done(function(resp) {
		if(resp.code=="ok"){
			var data = resp.data;
			$.each(data, function(key, val) {
				if(val>0){
					var cant = (val < 10)?val:'+9';
					switch(key){
						case "tarea": $menuCurso.find(".btn-tarea .badge").removeClass('hidden').text(cant); break;
						case "examen": $menuCurso.find(".btn-examen .badge").removeClass('hidden').text(cant); break;
						case "aulavirtual": $menuCurso.find(".btn-aula .badge").removeClass('hidden').text(cant); break;
					}
				}
			});
		}
	}).fail(function(err) {
		console.log("error:", err);
	}).always(function() { });
};

$(document).ready(function() {
	getCantNotif();
});