//declaracion de variables
var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
var constraints = window.constraints = { audio: true, video: false};
var alumnoTryButton = document.getElementById("alumnoTry");
var lesson = document.getElementById("spoken");
var phrase = document.getElementById("nuevaPalabra");
var teacherMediaRecorder;
var alumnoMediaRecorder;
var chunks = [];
var blob;
var blobAlumno;
var chunksAlumno = [];
var tr =document.getElementById("teacherRecording");
var wavesurfer = WaveSurfer.create({
    //wavesurfer del profesor
    container: '#waveform',
    waveColor: 'violet',
    progressColor: 'purple',
});

var wavesurferA = WaveSurfer.create({
    //wavesurfer del Alumno
    container: '#waveformAlumno',
    waveColor: 'darkorange',
    progressColor: 'purple',
});

function deletefile(fi){
  $.ajax({
         url: '../../media/delete.php',
         data: {'file' : fi },
         success: function (response) {
            console.log("file deleted");
         },
         error: function () {
            // do something
         }
       });
}

function upload(){
    var data = new FormData();
    data.append('file',blob );
    $.ajax({
    type: "POST",
    url: "../../media/save.php",
    data: data,
    contentType: false,
    processData: false,
  }).done(function(o) {
      wavesurfer.load("../../media/"+String(o).substring(1, o.length-1));
      if(tr.value !=""){
        deletefile(tr.value);
      }
      tr.value = String(o).substring(1, o.length-1);
      console.log('saved');
    });
}

function uploadAlumno(){
  var data = new FormData();
  data.append('file',blob );
  $.ajax({
  type: "POST",
  url: "../../media/saveTemporal.php",
  data: data,
  contentType: false,
  processData: false,
}).done(function(o) {
    wavesurferA.load("../../media/temporal.wav");
    getPercentage(tr.value);
    console.log('saved');
  });
}

function getPercentage(fileName){
  document.getElementById('score').innerHTML = "we are processing your audio file";
  $.ajax({
  type: "GET",
  url: "../../../cgi-bin/index.py?f="+fileName,
  data:{},
  contentType: false,
  processData: false,
}).done(function(o) {
    if(o[0]>60){
      document.getElementById('score').innerHTML = o[0] +'% <i style="color:green;" class="fa fa-check" aria-hidden="true"></i>'
    }else{
      document.getElementById('score').innerHTML = o[0] +'% <i style="color:red;" class="fa fa-times" aria-hidden="true"></i>';
    }
    console.log('saved');
  }).fail(function(e){
    console.log(e);
  });
}

function handleSuccess(stream){
  teacherMediaRecorder = new MediaRecorder(stream);
  teacherMediaRecorder.onstop = function(e) {
    console.log("data available after MediaRecorder.stop() called.");
    blob = new Blob(chunks, { 'type' : 'audio/wav' });
    upload();
    console.log("recorder stopped");
  }

  teacherMediaRecorder.ondataavailable = function(e) {
    chunks.push(e.data);
  }
  teacherMediaRecorder.start();
}

function handleSuccessAlumno(stream) {
  teacherMediaRecorder = new MediaRecorder(stream);
  teacherMediaRecorder.onstop = function(e) {
    console.log("data available after MediaRecorder.stop() called.");
    blob = new Blob(chunks, { 'type' : 'audio/wav' });
    uploadAlumno();
    console.log("recorder stopped");
    }

  teacherMediaRecorder.ondataavailable = function(e) {
    chunks.push(e.data);
  }
  teacherMediaRecorder.start();
  window.stream = stream; // make variable available to browser console
}


function handleError(error) {
  console.log('navigator.getUserMedia error: ', error);
}

function recordTeacherVoice(){
  var b = document.getElementById("toggle");
  if(b.innerHTML=='Stop Recording <i class="fa fa-microphone" aria-hidden="true"></i>'){
    b.innerHTML='Start Recording <i class="fa fa-microphone" aria-hidden="true"></i>';

    teacherMediaRecorder.stop();
  }else{
    b.innerHTML='Stop Recording <i class="fa fa-microphone" aria-hidden="true"></i>';
    chunks= [];
    navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);
  }
}


//POCKETSPHINX

// These will be initialized later
var recognizer, recorder, callbackManager, audioContext, outputContainer;
// Only when both recorder and recognizer do we have a ready application
var isRecorderReady = isRecognizerReady = false;

// A convenience function to post a message to the recognizer and associate
// a callback to its response
function postRecognizerJob(message, callback){
  var msg = message || {};
  if (callbackManager) msg.callbackId = callbackManager.add(callback);
  if (recognizer) recognizer.postMessage(msg);
};
// This function initializes an instance of the recorder
// it posts a message right away and calls onReady when it
// is ready so that onmessage can be properly set
function spawnWorker(workerURL, onReady) {
    recognizer = new Worker(workerURL);
    recognizer.onmessage = function(event) {
      onReady(recognizer);
    };
    recognizer.postMessage({});
};

// To display the hypothesis sent by the recognizer
function updateHyp(hyp) {
  if (outputContainer) outputContainer.innerHTML = hyp;
};

// This updates the UI when the app might get ready
// Only when both recorder and recognizer are ready do we enable the buttons
function updateUI() {
  if (isRecorderReady && isRecognizerReady) alumnoTry.disabled = false;
};

// This is just a logging window where we display the status
function updateStatus(newStatus) {
  document.getElementById('current-status').innerHTML += "<br/>" + newStatus;
};

// A not-so-great recording indicator
function displayRecording(display) {
  if (display) document.getElementById('recording-indicator').innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
  else document.getElementById('recording-indicator').innerHTML = "";
};

// Callback function once the user authorises access to the microphone
// in it, we instanciate the recorder
function startUserMedia(stream) {
  var input = audioContext.createMediaStreamSource(stream);
  // Firefox hack https://support.mozilla.org/en-US/questions/984179
  window.firefox_audio_hack = input;
  var audioRecorderConfig = {errorCallback: function(x) {updateStatus("Error from recorder: " + x);}};
  recorder = new AudioRecorder(input, audioRecorderConfig);
  // If a recognizer is ready, we pass it to the recorder
  if (recognizer) recorder.consumers = [recognizer];
  isRecorderReady = true;
  updateUI();
  updateStatus("Audio recorder ready");
};

// This starts recording. We first need to get the id of the grammar to use
// important
var startRecording = function() {
  if(alumnoTryButton.innerHTML == 'stop recording'){
    teacherMediaRecorder.stop();
    alumnoTryButton.innerHTML = 'start recording';
    stopRecording();
  }else{
    document.getElementById("output").innerHTML="";
    chunks = [];
    navigator.mediaDevices.getUserMedia(constraints).then(handleSuccessAlumno).catch(handleError);
    alumnoTryButton.innerHTML = 'stop recording';
    var id = document.getElementById('grammars').value;
    if (recorder && recorder.start(id)) displayRecording(true);
  }

};

// Stops recording
var stopRecording = function() {
  recorder && recorder.stop();
  displayRecording(false);
};

// Called once the recognizer is ready
// We then add the grammars to the input select tag and update the UI
var recognizerReady = function() {
     updateGrammars();
     isRecognizerReady = true;
     updateUI();
     updateStatus("Recognizer ready");
};

// We get the grammars defined below and fill in the input select tag
var updateGrammars = function() {
  document.getElementById("grammars").innerHTML="";
  var selectTag = document.getElementById('grammars');
  for (var i = 0 ; i < grammarIds.length ; i++) {
      var newElt = document.createElement('option');
      newElt.value=grammarIds[i].id;
      newElt.innerHTML = grammarIds[i].title;
      selectTag.appendChild(newElt);
  }
};

// This adds a grammar from the grammars array
// We add them one by one and call it again as
// a callback.
// Once we are done adding all grammars, we can call
// recognizerReady()
var feedGrammar = function(g, index, id) {
  if (id && (grammarIds.length > 0)) grammarIds[0].id = id.id;
  if (index < g.length) {
    grammarIds.unshift({title: g[index].title});
    postRecognizerJob({command: 'addGrammar', data: g[index].g}, function(id) {feedGrammar(grammars, index + 1, {id:id});});
  }else{
    grammarIds.push({"id":0, "title": "Keyword spotting"});
    recognizerReady();
  }
};

// This adds words to the recognizer. When it calls back, we add grammars
var feedWords = function(words) {
    postRecognizerJob({command: 'addWords', data: words},
    function() {feedGrammar(grammars, 0);});
};

// This initializes the recognizer. When it calls back, we add words
var initRecognizer = function() {
    // You can pass parameters to the recognizer, such as : {command: 'initialize', data: [["-hmm", "my_model"], ["-fwdflat", "no"]]}
    postRecognizerJob({command: 'initialize', data: [["-kws", "kws.txt"], ["-dict","kws.dict"]]},
    function() { if (recorder) recorder.consumers = [recognizer];  feedWords(wordList);});
};

// When the page is loaded, we spawn a new recognizer worker and call getUserMedia to
// request access to the microphone

  outputContainer = document.getElementById("output");
  updateStatus("Initializing web audio and speech recognizer, waiting for approval to access the microphone");
  callbackManager = new CallbackManager();
  spawnWorker("../../static/js/audio/pocketsphinx/webapp/js/recognizer.js", function(worker) {
      worker.onmessage = function(e) {
          if(e.data.hasOwnProperty('id')) {
            var clb = callbackManager.get(e.data['id']);
            var data = {};
            if ( e.data.hasOwnProperty('data')) data = e.data.data;
            if(clb) clb(data);
          }
          if(e.data.hasOwnProperty('hyp')) {
            var newHyp = e.data.hyp;
            if (e.data.hasOwnProperty('final') &&  e.data.final) newHyp = newHyp;
            updateHyp(newHyp);
          }
          if(e.data.hasOwnProperty('status') && (e.data.status == "error")) {
            updateStatus("Error in " + e.data.command + " with code " + e.data.code);
          }
      };
      postRecognizerJob({command: 'lazyLoad', data: {folders:[],files:[["/", "kws.txt", "../kws.txt"],["/", "kws.dict", "../kws.dict"]]}}, initRecognizer);
  });

  // The following is to initialize Web Audio
  try {
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    window.URL = window.URL || window.webkitURL;
    audioContext = new AudioContext();
  } catch (e) {
    updateStatus("Error initializing Web Audio browser");
  }
  if (navigator.getUserMedia) navigator.getUserMedia({audio: true}, startUserMedia, function(e) {
                                  updateStatus("No live audio input in this browser");
                              });
  else updateStatus("No web audio support in this browser");

// Wiring JavaScript to the UI
var alumnoTry = document.getElementById('alumnoTry');
alumnoTry.disabled = true;
alumnoTry.onclick = startRecording;


 // This is the list of words that need to be added to the recognizer
 // This follows the CMU dictionary format
var wordList = [];
var grammars = [];
var grammarIds = [];
function agregarGramatica(){
    var wordList2 = [];
    var grammarNew = {};
    d = document.getElementById("nuevaPalabra").value;
    document.getElementById("cleanedD").value = d;
    document.getElementById("spoken").innerHTML = "<b> Teacher says: "+d+"</b>";
    var wo = d.split(" ");
    var temp = {};
    var t = [];
      $.ajax({
        type: "POST",
        url: '../../media/accentQuery.php',
        data: {data: wo},
        dataType: 'json',
        success: function(data)
        {
          spawnWorker.numStates=wo.length+1;
          grammarNew.start = 0;
          grammarNew.end = wo.length;
          for(var i = 0; i< wo.length; i++){
            temp = {};
            temp.from = i;
            temp.to = i+1;
            temp.word = wo[i].toUpperCase();
            t.push(temp);
            wordList2[i] = [];
            wordList2[i].push(wo[i].toUpperCase());
            wordList2[i].push(data[wo[i]].substring(0,data[wo[i]].length-2));
          }

          temp.from = wo.length-1;
          temp.to = wo.length;
          t.push(temp);
          grammarNew.transitions = t;         
          var grammarList = [{title: "new grammar", g: grammarNew}];
          postRecognizerJob({command: 'addWords', data: wordList2},
                   function() {feedGrammar(grammarList, 0);});
        }
      });
}

var inputSaved = document.getElementById("isSaved");

if(inputSaved.value != ""){
  wavesurfer.load("../../media/"+ document.getElementById("teacherRecording"));
  d = document.getElementById("cleanedD").value;
  var wo = d.split(" ");
  var temp = {};
  var t = [];
    $.ajax({
      type: "POST",
      url: '../../media/accentQuery.php',
      data: {data: wo},
      dataType: 'json',
      success: function(data)
      {
        grammarNew.numStates=wo.length+1;
        grammarNew.start = 0;
        grammarNew.end = wo.length;
        for(var i = 0; i< wo.length; i++){
          temp = {};
          temp.from = i;
          temp.to = i+1;
          temp.word = wo[i].toUpperCase();
          t.push(temp);
          wordList2[i] = [];
          wordList2[i].push(wo[i].toUpperCase());
          wordList2[i].push(data[wo[i]].substring(0,data[wo[i]].length-2));
        }

        temp.from = wo.length-1;
        temp.to = wo.length;
        t.push(temp);
        grammarNew.transitions = t;
        var grammarList = [{title: "new grammar", g: grammarNew}];
        postRecognizerJob({command: 'addWords', data: wordList2}, function() {feedGrammar(grammarList, 0);});
      }
    });
}
