'use strict';

var ARCHIVOS = null;

var actualizarListado = function() {
	if (!ARCHIVOS.length){
		$('#resultado_busqueda').html('<div class="text-center" style="font-size: 1.8em;"><i class="fa fa-ban fa-4x"></i><p>'+MSJES_PHP.there_are_no_files+'.</p></div>')
		return false;
	}
	var strFiles = '';
	$.each(ARCHIVOS, function(i, a) {
		strFiles += '<div class="col-sm-6 col-md-3"><div class="thumbnail">';
		strFiles += '<img src="'+a.imagen+'" alt="imagen" class="img-responsive" style="height:170px;">';
		strFiles += '<div class="caption">';
		strFiles += '<h3  style="height:52px; overflow:hidden;" title="'+a.nombre+'">'+a.nombre+'</h3>';
		//strFiles += '<p>...</p>';
		strFiles += '<p class="text-center"><a href="'+_sysUrlBase_+'/notas/notas/ver/?idarchivo='+a.idarchivo+'" class="btn btn-info" role="button">'+MSJES_PHP.view+'</a></p>';
		strFiles += '</div>';
		strFiles += '</div></div>';
	});
	$('#resultado_busqueda').html(strFiles);
	return true;
};

var cargarArchivos = function() {
	var idGAD = $('#filtros #opcGrupoAulaDetalle').val();
	$.ajax({
		url: _sysUrlBase_+'/calificaciones/jxGetArchivos',
		type: 'POST',
		dataType: 'json',
		data: {
			"iddocente": $('#hIdDocente').val(),
			"idgrupoauladetalle": idGAD,
		},
		beforeSend: function() {
			$('#resultado_busqueda').html('<div class="text-center" style="font-size: 1.8em;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><p>'+MSJES_PHP.loading+'</p></div>')
		}
	}).done(function(resp) {
		if(resp.code=="ok") {
			ARCHIVOS = resp.data;
			actualizarListado();
		} else {
			mostrar_notificacion('Error', resp.msj, 'error');
		}
	}).fail(function(err) {
		console.log("error! : ", err);
	}).always(function() {});
};

$(document).ready(function() {
	cargarMensajesPHP($("#mensajes_idioma"));

	$('#filtros #opcLocal').change(function(e) {
		var idLocal = $(this).val();
		$.ajax({
			url: _sysUrlBase_+'/acad_grupoauladetalle/jxMisAmbientes',
			type: 'GET',
			dataType: 'json',
			data: {"idlocal": idLocal, "estado":1},
			beforeSend: function() {
				$('#filtros #opcAmbiente').html('<option value="0">- '+MSJES_PHP.select_classroom+' -</option>');
				$('#filtros #opcGrupoAulaDetalle').html('<option value="">- '+MSJES_PHP.select_group+' -</option>');
			},
		}).done(function(resp) {
			if(resp.code=="ok") {
				var strOptions = '';
				$.each(resp.data, function(i, a) {
					strOptions += '<option value="'+a.idambiente+'">'+ a.numero +'</option>';
				});
				$('#filtros #opcAmbiente').append(strOptions);
			} else {
				mostrar_notificacion('Error', resp.msj, 'error');
			}
		}).fail(function(err) {
			console.log("error");
		}).always(function() { });
	});

	$('#filtros #opcAmbiente').change(function(e) {
		var idLocal = $('#filtros #opcLocal').val();
		var idAmbiente = $(this).val();
		var idDocente = $('#hIdDocente').val();
		$.ajax({
			url: _sysUrlBase_+'/acad_grupoauladetalle/buscarjson',
			type: 'GET',
			dataType: 'json',
			data: {
				"idlocal": idLocal, 
				"idambiente": idAmbiente, 
				"iddocente": idDocente, 
				"estado": 1
			},
			beforeSend: function() {
				$('#filtros #opcGrupoAulaDetalle').html('<option value="">- '+MSJES_PHP.select_group+' -</option>');
			},
		}).done(function(resp) {
			if(resp.code=="ok") {
				var strOptions = '';
				$.each(resp.data, function(i, gad) {
					strOptions += '<option value="'+gad.idgrupoauladetalle+'">'+ gad.strcurso + ' - ' + gad.strgrupoaula +'</option>';
				});
				$('#filtros #opcGrupoAulaDetalle').append(strOptions);
			} else {
				mostrar_notificacion('Error', resp.msj, 'error');
			}
		}).fail(function(err) {
			console.log("error");
		}).always(function() { });
	});

	$('#filtros #opcGrupoAulaDetalle').change(cargarArchivos);

	cargarArchivos();
});