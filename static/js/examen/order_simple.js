/**
* Ordenar Simple plugin
**/
var ANS= {}; 
var AYUDA_ORD= {};

var initOrdenarSimple = function(IDGUI, ayuda=true){
    ANS[IDGUI] = {};
    if(AYUDA_ORD[IDGUI]==undefined && ayuda!=undefined) { AYUDA_ORD[IDGUI] = ayuda; }
    
    var now = Date.now();
    $('#elem_0').attr('id', 'elem_'+now);
    $('a[data-url=".img_0"]').attr('data-url', '.img_'+now);
    $('img.img_0').removeClass('img_0').addClass('img_'+now);
    $('a[data-url=".audio_0"]').attr('data-url', '.audio_'+now);
    $('audio.audio_0').removeClass('audio_0').addClass('audio_'+now);

    if( !$('#lista-elem-edit'+IDGUI).is(':visible') || $('#lista-elem-edit'+IDGUI).length==0 ){
        $('#ejerc-ordenar'+IDGUI+' .element').each(function() {
            var resp = $(this).attr('data-resp');
            var id = $(this).attr('id');
            ANS[IDGUI][id] = resp;
            if( !$('.mask.elegirtpl').is(':visible') || $('.mask.elegirtpl').length==0 ) $(this).removeAttr('data-resp');
        });

        /*$('#tmp_'+IDGUI+' .elem-edit .selectmedia').each(function() {
            lastBtnClicked = this;
            activarBtn();
        });*/
    }
};

var evaluarFinal_simple = function($element , IDGUI){
    var id = $element.attr('id');
    var string = '' ;
    
    if( $element.find('.drop>div.parte').hasClass('letter') ){
        $element.find('.drop>div.parte').each(function() {
            string += $(this).text();
        });
        if( ANS[IDGUI][id]!=undefined ) var isMatch = ( ANS[IDGUI][id].toUpperCase() == string.toUpperCase() );
    }
    if( $element.find('.drop>div.parte').hasClass('word') ){
        $element.find('.drop>div.parte').each(function() {
            string += $(this).text()+' ';
        });
        if( ANS[IDGUI][id]!=undefined ) var isMatch = ( ANS[IDGUI][id] == string.trim() ) ; 
    }

    if( isMatch!=undefined ){
        if( isMatch ){
            $element.attr('data-corregido', 'good');
            //$element.find('.drop').removeClass('bad').addClass('good');
            $element.find('.drop .parte.blank').attr('data-corregido', 'good');  
        } else {
            $element.attr('data-corregido', 'bad');
            //$element.find('.drop').removeClass('good').addClass('bad');
            $element.find('.drop .parte.blank').attr('data-corregido','bad');
        }
    }

};

var evaluarUno_simple = function($element, IDGUI){
    var cantVacios = $element.find('.drop>div:empty').length;
    var id = $element.attr('id');
    var i=0;
    var rspta_completa='';
    if( ANS[IDGUI][id]!=undefined ) {
        rspta_completa = ANS[IDGUI][id];
        if( $element.find('.drop>.parte').hasClass('word') ) rspta_completa = rspta_completa.split(' ');
    }
    $element.find('.drop>.parte:empty').removeClass('bad').removeClass('good');
    $element.find('.drop>.parte').each(function() {
        var $this = $(this);
        if( $this.is(':empty') ){ return false; }
        if( !$this.hasClass('fixed') ){
            var parte = $(this).text();
            if( parte.toUpperCase()==rspta_completa[i].toUpperCase() ){
                $this.attr('data-corregido', 'good');
                //$this.removeClass('bad').addClass('good');
            }else{
                $this.attr('data-corregido', 'bad');
                //$this.removeClass('good').addClass('bad');
            }
        }

        i++;
    });

    //if( cantVacios==0 ){ evaluarFinal_simple($element, IDGUI); }

};

var evaluarOrdenCorrecto = function( $element, IDGUI ){
    var cantVacios = $element.find('.drop>div:empty').length;
    var isDBY = esDBYself( $element );
    var ayuda = AYUDA_ORD[IDGUI];
    if( !ayuda && cantVacios == 0 ) {
        evaluarFinal_simple($element, IDGUI);
    } else {
        $element.removeAttr('data-corregido');
        $element.find('.drop').removeClass('bad').removeClass('good');
    }
    if(ayuda){
        evaluarUno_simple($element, IDGUI);
    }

    //pausarTiempoOrdenar();
};

(function($){
    $.fn.examOrdenSimple = function(opciones){
        var opts = $.extend({}, $.fn.examOrdenSimple.defaults, opciones);
        return this.each(function() {
            var that = $(this);
            var _idgui = that.find('#idgui').val();
            var _isEditando = that.hasClass('editando');

            that.on('click', '.ejerc-ordenar .drag>div', function(e) { 
                if(_isEditando==true) return false;
                e.preventDefault();
                var $this = $(this);
                var value= $this.text();
                var idElem = $this.closest('.element').attr('id');
                var $primerVacio = $('#'+idElem, that).find('.drop>div:empty').first();
                $primerVacio.html('<div>'+value+'</div>')
                $this.remove();
                evaluarOrdenCorrecto( $('#'+idElem, that) , _idgui);
            })
            .on('click', '.ejerc-ordenar .drop>div.parte.blank>div', function(e) { 
                if(_isEditando==true) return false;
                e.preventDefault();
                var $this = $(this);
                var value= $this;
                var $parents = $this.closest('.element');
                $parents.find('.drag').append(value);
                evaluarOrdenCorrecto( $parents , _idgui);
            })
            .on('click', '.multimedia a[data-audio]', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var $audio = $('#audio-ordenar'+_idgui),
                src = $(this).attr('data-audio');
                $audio.attr('src', _sysUrlStatic_+'/media/audio/'+src);
                $audio.trigger('play');
            });

            initOrdenarSimple(_idgui);
        });
    }

    $.fn.examOrdenSimple.defaults = {};
}(jQuery));