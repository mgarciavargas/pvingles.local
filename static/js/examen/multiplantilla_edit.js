var funcionalidaddraganddrop=function(cont){
    $('.plantilla-completar .isdragable',cont).draggable({
        start:function(event,ui){
            sysdragstart(event,ui);
        },
        stop:function(event,iu){
           sysdragend(event,iu) 
        }
    });

    $('.plantilla-completar .isdrop',cont).droppable({
        drop:function(event,ui){
            sysdrop(event,ui,$(this));
            calcularBarraProgreso();
        }
    });
}