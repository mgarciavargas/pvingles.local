/**
* Etiquetado de Imagen plugin
**/
var ANS_TAG = {};
var HELP = {};

var playEjercicio = function(IDGUI, $tagAlts){
    $tagAlts.find('.tag').each(function() {
        var id=$(this).attr('id');
        var indexID = id.split('_')[id.split('_').length-1];
        var numero = $(this).attr('data-number');
        ANS_TAG[IDGUI][numero] = indexID;
        if( !$('.mask.elegirtpl').is(':visible') || $('.mask.elegirtpl').length==0 ) {
            $(this).removeAttr('data-number');
        }
    });
};

var initImageTagged = function($plantilla, IDGUI, ayuda=true){
    ANS_TAG[IDGUI] = {};
    HELP[IDGUI] = ayuda;    
    if( $('.botones-edicion', $plantilla).length>0 || $('.mask-dots', $plantilla).hasClass('playing') ){
        playEjercicio(IDGUI, $('.tag-alts', $plantilla));
    }
};

var evaluarTag = function($selectedTag, $dotContainerActive, IDGUI){
    var indexId_selecc = $selectedTag.attr('id').split('_').pop();
    var number_selecc = $dotContainerActive.attr('id').split('_').pop();
    var ayuda = HELP[IDGUI];
    $dotContainerActive.find('.dot-tag').addClass('corregido');
    if( indexId_selecc == number_selecc ){
        var valor = 'good';
    } else {
        var valor = 'bad';
    }
    $dotContainerActive.find('.dot-tag').attr('data-corregido', valor);

    if(!ayuda){
        $selectedTag.hide();
    }
};

(function($){
    $.fn.examTagImage = function(opciones){
        var opts = $.extend({}, $.fn.examTagImage.defaults, opciones);
        return this.each(function() {
            var that = $(this);
            var _idgui = that.find('#idgui').val();
            var _isEditando = that.hasClass('editando');

            that.on('click', '.contenedor-img .mask-dots.playing .dot', function(e) {
                //e.stopPropagation();
                var $plantilla = $(this).closest('.plantilla-img_puntos');
                if( HELP[_idgui] ){
                    if( $(this).parent().hasClass('hover') ){
                        $(this).parent().removeClass('hover');
                        $('.tag-alts>.tag', $plantilla).removeClass('active');
                    } else {
                        $('.mask-dots.playing .dot-container', $plantilla).removeClass('hover');
                        $(this).parent().addClass('hover');
                        $('.tag-alts>.tag', $plantilla).addClass('active');
                    }
                }else {
                    if( !$(this).siblings('.dot-tag').hasClass('corregido') ){
                        if( $(this).parent().hasClass('hover') ){
                            $(this).parent().removeClass('hover');
                            $('.tag-alts>.tag', $plantilla).removeClass('active');
                        } else {
                            $('.mask-dots.playing .dot-container', $plantilla).removeClass('hover');
                            $(this).parent().addClass('hover');
                            $('.tag-alts>.tag', $plantilla).addClass('active');
                        }
                    }
                }
            }).on('click', '.tag-alts .tag.active', function(e) {
                e.preventDefault();
                var $this = $(this);
                var $plantilla = $this.closest('.plantilla-img_puntos');
                var textTag = $this.text();

                $('.mask-dots .dot-container.hover .dot-tag', $plantilla).text(textTag);
                evaluarTag($this, $('.mask-dots .dot-container.hover', $plantilla), _idgui);
                
                if( $('.mask-dots.playing', $plantilla).children().hasClass('hover') ){
                    $('.mask-dots.playing', $plantilla).children().removeClass('hover');
                    $('.tag-alts>.tag', $plantilla).removeClass('active');
                }
            });

            initImageTagged(that, _idgui);
        });
    }

    $.fn.examTagImage.defaults = {};
}(jQuery));