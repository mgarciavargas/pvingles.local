$(document).ready(function(){
    var sideBars_Height = function(selectorSideBar){
        var height = ($(window).height())-60;
        $(selectorSideBar).css('height', height+'px');
    };
    var loadModalGuia = function(){
        var isChecked = localStorage.DoNotShowGuide;
        if (isChecked == undefined || isChecked=='false') isChecked =false;
        if( !isChecked ){
            $('#myModal').modal({
                keyboard: false, /*desactiva Cerrar Modal con ESC*/
            });
        }
    };

	var changeButtonIcon = function(idBtn, oldIcon, newIcon){
        $(idBtn+" i").removeClass(oldIcon);
        $(idBtn+" i").addClass(newIcon);
    };

    var showToolbar = function( value ){
        if( value ){
            $(".toolbar").addClass("toolbar-toggled");
            $(".page-content").addClass("page-content-toolbar");
        }else{
            $(".toolbar").removeClass("toolbar-toggled");
            $(".page-content").removeClass("page-content-toolbar");
        }
    };

    var showSidebarRight = function( value ){
        if( value ){
            $(".sidebar-right").addClass("sidebar-toogled");
            $(".page-content").addClass("page-content-toggled-right");

            changeButtonIcon("#getting-started", "fa-th-large", "fa-times");
        } else {
            $(".sidebar-right").removeClass("sidebar-toogled");
            $(".page-content").removeClass("page-content-toggled-right");

            changeButtonIcon("#getting-started", "fa-times", "fa-th-large");
        }
    };

    var showSidebarLeft = function( value ){
        if( value ){
            $(".sidebar-left").addClass("sidebar-toogled");
            $(".page-content").addClass("page-content-toggled-left");
        } else {
            $(".sidebar-left").removeClass("sidebar-toogled");
            $(".page-content").removeClass("page-content-toggled-left");
        }
    };

    var loadPHPSource = function( source , container ){
        var path = "";
        var ext = '.php';
        if(source != null){
            container.load( path+source+ext );
        }
        else{
            container.load( path+"_blank"+ext );
        }
    };

    $('#getting-started').click(function(e){
        e.preventDefault();
        showSidebarLeft(false);
        var isGettingStarted = $("#getting-started i").hasClass("fa-th-large");

        if( isGettingStarted ){
            showToolbar(true);
            changeButtonIcon("#getting-started", "fa-th-large", "fa-times");
        } else { 
            /*si isClose */
            showToolbar(false);
            showSidebarRight(false);
            if( $(".sidebar-right").hasClass('sidebar-toogled') ){
                loadPHPSource( null, $(".sidebar-right") );
            }
        }
    });
    
    //$(".slide-sidebar-right").click(function(e){
    $('body').on('click', '.slide-sidebar-right', function(e) {
        e.preventDefault();
        showSidebarRight(true);
        showToolbar(false);
        showSidebarLeft(false);
        var hasDataParams = $(this).attr("data-source");
        if( hasDataParams ){
            var source = $(this).data("source");
            loadPHPSource(source, $(".sidebar-right") );
        }
    });

    $(".slide-sidebar-left").click(function(e){
        e.preventDefault();
        showToolbar(false);
        showSidebarRight(false);
        var isOpen = $(".sidebar-left").hasClass("sidebar-toogled");
        if( !isOpen ){
            showSidebarLeft(true);
        } else {
            showSidebarLeft(false);
        }
    });
    $('#myModal').on('hidden.bs.modal', function (e) {
        /* Evento disparado al cerrar el modal #myModal*/
        var isChecked = $("#chkDejarDeMostrar").is(':checked');
        localStorage.DoNotShowGuide = isChecked;
    });

    $(window).resize(function(e) {
        sideBars_Height('#toolbar');
        sideBars_Height('#sidebar-left');
        sideBars_Height('#sidebar-right');
    });

   
    /* Inciando componentes */
    sideBars_Height('#toolbar');
    sideBars_Height('#sidebar-left');
    sideBars_Height('#sidebar-right');
    /*loadModalGuia();*/
});


/**
* Funcion general para llamar a Biblioeca
* Incializar asi: $(selectorBtn).biblioteca();
* @required: "selectedfile()" at "./static/tema/js/funciones.js"
**/
/*(function($){
    $.fn.biblioteca=function(){
        var that = $(this);
        that.on('click', function(ev) {
            ev.preventDefault();
            var txt='Select or Upload';
            selectedfile(ev,this,txt);
        });
    };
}(jQuery));*/