var speachonline=navigator.onLine?true:false;
speachonline=true;
var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
var constraints = window.constraints = { audio: true, video: false};
window.AudioContext=window.AudioContext || window.webkitAudioContext;
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
window.speechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.oSpeechRecognition || window.msSpeechRecognition;
var audiorecording = 0;
var audio_context,recorder,recognizeronline,recognizer,callbackManager,isurfer=0;
var _initaudiorecord=false;
var mediaRecorder;
var blobs=wave=chunks=wordList=grammars=grammars=grammarIds=[];
var wave=[];
var chunks=[];
var isRecorderReady = isRecognizerReady = false;
callbackManager = new CallbackManager();
var initofflinereconizer=false;
var initspeach=function(pnl){
	if(pnl=='')return;
	if(pnl.hasClass('editando')){
		previewspeach(false);
		$('#btns-guardar-actividad').show();		
	}else{
		$('#btns-guardar-actividad').hide();
        previewspeach(true);       
	};
}

var pausarTiempoSpeach = function(){
	var $tmplActiva = getTmplActiva();
	var $plantillaActiva = $tmplActiva.find('.plantilla-speach');
	$pnlalumno=$plantillaActiva.find('.pnl-speach.alumno');
  	if($pnlalumno.find('.btnAccionspeachdby').length>0){
	  $pnlalumno.find('.btnGrabarAudio').attr('disabled',true).addClass('disabled');
	  var elemsTotal = $plantillaActiva.find('.textohablado span').length-1;
	  var elemsCorregidas = $plantillaActiva.find('.textohablado span.speachtextook').length;
	  //console.log(elemsTotal,elemsCorregidas);
	  $('#panel-tiempo .info-show').trigger('oncropausar');
	  $tmplActiva.find('.plantilla').addClass('tiempo-pausado');
	  var $panelIntentos = $('#panel-intentos-dby');
  	  var intento_actual = $panelIntentos.find('.actual').text();
  	  var intentos_total = $panelIntentos.find('.total').text();
	  if(( elemsTotal === elemsCorregidas && elemsTotal>0)||intento_actual>=intentos_total){
		$tmplActiva.find('.save-progreso').show('fast');	    
	  }
	  
	}
};

var restextosimilar=function(idgui,pt,res,np,nok){
np=np||1;
nok=nok||0;	
id=Date.now();
var btn='<button class="btncalcular" id="btn'+id+'" style="display:none;"></button>';
	$('#'+idgui).find('.textohablado').html(res+' '+'<span class="speachtexttotal" total-palabras="'+np+'" total-ok="'+nok+'">'+pt.toFixed(0)+'%</span>'+btn);
	$('#btn'+id).trigger('click');
	pausarTiempoSpeach();
}

var textosimilar=function(idgui,s1,s2){
	var str1=s1||'';
	var str2=s2||'';
	var pt=0;
	var result='';
	str1=str1.toLowerCase().trim().replace(/\s+/gi,' ').replace(/  /,' ');
	str2=str2.toLowerCase().trim().replace(/\s+/gi,' ').replace(/  /,' ');
	//console.log('txt1 '+str1);
	//console.log('txt2 '+str2);

	str3=str1.replace(/[^a-zA-Z 0-9]+/gi,'').replace(/\s+/gi,' ');
	str4=str2.replace(/[^a-zA-Z 0-9]+/gi,'').replace(/\s+/gi,' ');

	str5=str3.replace(/\s/gi,'');
	str6=str4.replace(/\s/gi,'');
	if(str5===str6){
		restextosimilar(idgui,100,'<span class="speachtextook">'+str1+'</span>',1,1);
		return;
	}
	if(str1.length==0) return;
	if(str2.length==0) return;
	if(str3===str4){
		restextosimilar(idgui,100,'<span class="speachtextook">'+str1+'</span>',1,1);
		return;
	}
	var _rstr1=str1.split(" ");
	var rstr1=str3.split(" ");
	var rstr2=str4.split(" ");	
	n1=rstr1.length;
	n2=rstr2.length;
	n3=n1>n2?n1:n2;
	var rstmp=[];
	var iok=0;
	for(var i=0; i<rstr1.length; i++){
		if(rstr1[i]==rstr2[i]){
			rstmp[i]='<span class="speachtextook">'+_rstr1[i]+'</span>';
			iok++
		}else{//ierror++;
			rstmp[i]='<span class="speachtextobad">'+_rstr1[i]+'</span>';
		}
	}
	var ok=iok;
	iok=((iok*100)/n3);
	restextosimilar(idgui,iok,rstmp.join(' ').trim(),n3,ok);
}

function mostrartextospeach(tpl){
	var txt1=$('#'+tpl).find('.txtalu1').text().trim();
	var txt2=$('#'+tpl).find('.txtalu2').text().trim();	
	if(txt1.length>0&&txt2.length>0){
		textosimilar(tpl,txt1,txt2);
	}
}

function stoprecognizer(tpl)//stop it manually
{
	//console.log(recognizeronline);
	if(recognizeronline != null)
	{		
		if(speachonline){recognizeronline.stop();		
		setTimeout(mostrartextospeach(tpl),7000);	
		}
	}
}

function stopRecording(pnl){
	//console.log(pnl);
   /* recorder && recorder.stop();
    recorder.exportMP3(function(blob){
        var url = URL.createObjectURL(blob);
        crearonda(url,pnl);       
  	});
  	recorder.clear();*/
}

function postRecognizerJob(message, callback){
  var msg = message || {};
  if (callbackManager) msg.callbackId = callbackManager.add(callback);
  if (recognizer) recognizer.postMessage(msg);
}

function spawnWorker(workerURL, onReady) {
    recognizer = new Worker(workerURL);
    recognizer.onmessage = function(event) {
      onReady(recognizer);
    };
    recognizer.postMessage({});
}

function updateHyp(hyp) {
  //console.log(hyp)
}

function displayRecording(display) {
	//console.log(display);
}
var recognizerReady = function() {
     updateGrammars();
     isRecognizerReady = true; 
};

var updateGrammars = function() {
	//console.log('updateGrammars');
  //document.getElementById("grammars").innerHTML="";
  //var selectTag = document.getElementById('grammars');
  for (var i = 0 ; i < grammarIds.length ; i++) {
  	  //console.log(grammarIds[i].id,grammarIds[i].title);
      /*var newElt = document.createElement('option');
      newElt.value=grammarIds[i].id;
      newElt.innerHTML = grammarIds[i].title;
      selectTag.appendChild(newElt);*/
  }
};

var feedGrammar = function(g, index, id) {
  if (id && (grammarIds.length > 0)) grammarIds[0].id = id.id;
  if (index < g.length) {
    grammarIds.unshift({title: g[index].title});
    postRecognizerJob({command: 'addGrammar', data: g[index].g},
    function(id) {feedGrammar(grammars, index + 1, {id:id});});
  }else{
    // We are adding keyword spotting which has id 0
    grammarIds.push({"id":0, "title": "Keyword spotting"});
    recognizerReady();
  }
};
var feedWords = function(words) {
    postRecognizerJob({command: 'addWords', data: words},
    function(){/*console.log('f')*/})
    //feedGrammar(grammars, 0);});
};


var onda=function(id){
    if(wave[id]==undefined){
        wave[id] = Object.create(WaveSurfer); 
    }
    return wave[id];
}

function crearonda(url,donde,cargaini){
	var idpnl=donde.attr('id');
    cargaini=cargaini||false;
	isurfer++;
	var iddonde=Date.now();
    var wavesurfer=null;
    if($('#'+idpnl).find('.wavesurfer').length>0){
    	iddonde=$('#'+idpnl).find('.wavesurfer').attr('idtmp');        
        wavesurfer=onda(iddonde);
        wavesurfer.destroy();
    	$('#'+idpnl).find('.wavesurfer').remove();
        $('#'+idpnl).find('audio').remove();        
    }
    $('#'+idpnl).find('.grafico').append('<div class="wavesurfer" idtmp="'+iddonde+'" id="wave_'+iddonde+'" ></div>');
    wavesurfer=onda(iddonde);
	wavesurfer.init({
        container: document.querySelector('#wave_'+iddonde),
        waveColor: '#A8DBA8',
        progressColor: '#3B8686',
        backend: 'MediaElement',
    });
    var newurl='';   
    var index1_=url.indexOf('?',0);
    var tam_ =url.length;   
    if(index1_>10)url=url.substring(0,index1_);
    url=url.replace(_sysUrlBase_,'');
   // if(cargaini==true) newurl=url;
    newurl=_sysUrlBase_+'/'+url+'?tmp='+Date.now();
    wavesurfer.load(newurl);
    wavesurfer.on('ready', function(){
	  wavesurfer.drawer.container.style.display = '';
	  wavesurfer.drawBuffer();
	});
    $('#'+idpnl+' .btnPlaytexto').off('click');
    $('#'+idpnl+' .btnPlaytexto').on('click',function(ev){
    	$(this).addClass('inwave').attr('id','play_'+idpnl);
    	wavesurfer.playPause();
    });
}
var previewspeach=function(inpreview){
	$('.plantilla-speach .textohablado').html('');
	if(inpreview==true){
		$('.plantilla-speach .inpreview').removeClass('hidden');        
		$('.pnl-speach.editable').removeClass('editable').attr('editable',"true");
        $('.plantilla-speach .pnl-speach.inpreview .grafico').html('');
        $('.plantilla-speach .pnl-speach .txtalu2').text('');
        $('.plantilla-speach .pnl-speach.inpreview .btnPlaytexto').off('click');
        $('.plantilla-speach .pnl-speach.alumno .btnGrabarAudio').removeClass('disabled').removeAttr('disabled');
	}else{
		$('.plantilla-speach .inpreview').addClass('hidden');
		$('.pnl-speach[editable="true"]').addClass('editable').removeAttr('editable');
		$('.plantilla-speach .pnl-speach.inpreview .grafico').html('');	
	}
}

var speachpronunciar=function(pnl){	
	var txt=$(pnl).find('.edittexto_ch').text();
	pronunciar(txt);	
}

var speachuploadblob=function(miblob,pnl,tpl){
	var name=pnl.attr('id')||'';
	var data = new FormData();
  	data.append('filearchivo',miblob);
  	data.append('type',miblob.type);
  	data.append('name',name);
	$.ajax({
        url : _sysUrlBase_+"/biblioteca/subirblob",
        type: 'POST',
        data: data,
        contentType: false,
        processData: false,
        dataType :'json',
        success: function(res){        	
         if(res["code"]=='ok'){         	
         	crearonda(res.namelink,pnl);
         }
        },error: function(e) {
            
            //console.log('Error inesperado intententelo mas tarde',e);
        }
    });
}

var speachgrammar=function (d){
	var wo = d.split(" ");
	var temp = {};
	var t = [];
    $.ajax({
      type: "POST",
      url: '../../media/accentQuery.php',
      data: {data: wo},
      dataType: 'json',
      success: function(data)
      {
        //console.log(data);
        grammarNew.numStates=wo.length+1;
        grammarNew.start = 0;
        grammarNew.end = wo.length;
        for(var i = 0; i< wo.length; i++){
          temp = {};
          temp.from = i;
          temp.to = i+1;
          temp.word = wo[i].toUpperCase();
          t.push(temp);
          wordList2[i] = [];
          wordList2[i].push(wo[i].toUpperCase());
          wordList2[i].push(data[wo[i]].substring(0,data[wo[i]].length-2));
        }

        temp.from = wo.length-1;
        temp.to = wo.length;
        t.push(temp);
        grammarNew.transitions = t;
        var grammarList = [{title: "new grammar", g: grammarNew}];
        postRecognizerJob({command: 'addWords', data: wordList2}, function() {feedGrammar(grammarList, 0);});
      }
    });
}



var speachofflinefun=function(stream,pnl){

}
var initRecognizer = function() {
    postRecognizerJob({command: 'initialize'},
    function() { if (recorder) recorder.consumers = [recognizer]; speachgrammar('')});
};

var initworker=function(){
	spawnWorker('../../media/speach/webapp/js/recognizer.js', function(worker) {
	  worker.onmessage = function(e){
	      if(e.data.hasOwnProperty('id')){	      
	        var clb = callbackManager.get(e.data['id']);
	        var data = {};
	        if(e.data.hasOwnProperty('data')) data = e.data.data;
	        if(clb)clb(data);
	      }
	      if(e.data.hasOwnProperty('hyp')){
	        var newHyp = e.data.hyp;
	          console.log(newHyp);
	        if (e.data.hasOwnProperty('final') &&  e.data.final) newHyp = newHyp;
	        updateHyp(newHyp);
	      }
	      if(e.data.hasOwnProperty('status') && (e.data.status == "error")){
	      	console.log("Error in :", e.data.command, " with code :", e.data.code);
	      }
	  };	  
	   postRecognizerJob({command: 'lazyLoad', data: {folders: [], files: [["/", "kws.txt", "../kws.txt"], ["/", "kws.dict", "../kws.dict"]]}}, initRecognizer);
	  initofflinereconizer=true;	  
	});	
}

if(!speachonline) {	initworker();}
var streamtest=null;
var speachgrabaraudio=function(pnl,tpl){
	if(!speachonline&&!initofflinereconizer) {initworker(); speachgrabaraudio(pnl,tpl); return false;}
	chunks=[];
	navigator.mediaDevices.getUserMedia({audio:true,video:false}).then(function(stream){
        streamtest=stream;
		mediaRecorder = new MediaRecorder(stream);
		if(!recorder){
			context = new window.AudioContext;
			var mediaStreamSource = context.createMediaStreamSource(stream);
			window.firefox_audio_hack = mediaStreamSource;			
			var audioRecorderConfig = {errorCallback: function(x) {console.log('Error :',x)}};
  			recorder = new AudioRecorder(mediaStreamSource, audioRecorderConfig);  			
		}
		if (initofflinereconizer&&recognizer){
			recorder.consumers = [recognizer];
			isRecognizerReady=true;
		}

		mediaRecorder.onstop = function(e){
			blob = new Blob(chunks, { 'type' : 'audio/wav' });
    		speachuploadblob(blob,pnl,tpl); 
  		}
  		mediaRecorder.ondataavailable = function(e) { chunks.push(e.data);}
		mediaRecorder.start();
		if(speachonline) speachonlinefun(pnl,tpl);
		else {speachofflinefun(pnl,tpl);}
	}).catch(function(er){
		console.log(er)
	});
}

var speachonlinefun=function(pnl,tpl){
	if(window.speechRecognition == undefined)
	{
		speachonline=false;
		return speachofflinefun(pnl);		
	}
	audiorecording = audiorecording + 1;
	recognizeronline = new speechRecognition();
	recognizeronline.continuous = true;
	recognizeronline.lang = "en-US";
	recognizeronline.interimResults = true;
	recognizeronline.onstart = function(){ /*console.log('reconizer start');*/ }
	recognizeronline.onend = function(){ /*console.log('recong stop');*/		
		//recognizeronline = null;
		setTimeout(mostrartextospeach(tpl),7000);
	}
	/*console.log('before reconizer');*/
	recognizeronline.onresult = function(event){
		/*console.log('reconizer...');*/
		var text = '';
	    if(event.results &&  event.results.length )
	    {
	      for ( var i = event.resultIndex, len = event.results.length; i < len; ++i ){
	        text += event.results[i][0].transcript;
	      }
	      pnl.find('.edittexto_ch').text(text);
	    }
 		    
	}
	recognizeronline.start();
}

var speachofflinefun=function(pnl,tpl){
	recorder.start();
	isRecorderReady = true;	
}


$(document).ready(function(){
	$('body').on('click','.pnl-speach .btnGrabarAudio',function(ev){
		ev.preventDefault();
		var pnlspeach=$(this).closest('.pnl-speach');
		var plantillaidgui=$(this).closest('.plantilla-speach').attr('id');
		var btn=$(this).find('i');
		btn.toggleClass('color-green');
		if(btn.hasClass('color-green')){
			btn.addClass('animated infinite zoomIn');
			speachgrabaraudio(pnlspeach,plantillaidgui);
			$(this).siblings('.btnPlaytexto').addClass('disabled').attr('disabled',true);
			var span=$(this).find('span');
			$(this).attr('idioma1',span.text());
			$(this).find('span').text(span.attr('idioma2'));

			var DBYgrabar=$(this).siblings('.btnAccionspeachdby');
			if(DBYgrabar.length){
				$(this).siblings('.btnAccionspeachdby').addClass('active').trigger('click');
			}
		}else{
			$(this).siblings('.btnPlaytexto').removeClass('disabled').removeAttr('disabled');
			btn.removeClass('animated infinite zoomIn');
			$(this).find('span').text($(this).attr('idioma1'));
            if(streamtest) streamtest.getTracks()[0].stop();
			stoprecognizer(plantillaidgui);
			mediaRecorder.stop();
			var DBYgrabar=$(this).siblings('.btnAccionspeachdby');
			if(DBYgrabar.length){
				$(this).siblings('.btnAccionspeachdby').removeClass('active').trigger('click');
			}
		}
	}).on('click','.pnl-speach .btnWriteTexto',function(ev){
		ev.preventDefault();
		var pnlspeach=$(this).closest('.pnl-speach');
		if(pnlspeach.hasClass('editable')){
			pnlspeach.find('.edittexto_ch').trigger('click');
		}
	}).on('click','.pnl-speach .btnPlaytexto',function(ev){
		ev.preventDefault();
		if(!$(this).hasClass('inwave')){
			var pnlspeach=$(this).closest('.pnl-speach');
			speachpronunciar(pnlspeach);
		}
	})

	$('body').on('click','.pnl-speach.editable .edittexto_ch',function(ev){		
		ev.preventDefault();
		var como=$(this).attr('data-edittextocomo')||'input';		
		if($(this).find(como).length>0){
            $(this).find(como).focus();
            return;
        }
        var txt=$(this).text();
        var placeholder=$(this).attr('placeholder')||'';
		var txtold=$(this).attr('data-textoold')||'';
		var txtedit=null;
		if(como=='textarea'){
			txtedit='<textarea class="form-control" placeholder="'+placeholder+'">'+(txt!=txtold?txt:'')+'</textarea>';
		}else{
			txtedit='<input type="text" class="form-control" placeholder="'+placeholder+'" value="'+(txt!=txtold?txt:'')+'">';
		}
		$(this).html(txtedit);
        $(this).find(como).focus();
	}).on('focusout','.edittexto_ch input',function(ev){
		ev.preventDefault();
		var txt=$(this).val();
        $(this).closest('.edittexto_ch').html(txt);
	}).on('focusout','.edittexto_ch textarea',function(ev){
		ev.preventDefault();
		var txt=$(this).val();
        $(this).closest('.edittexto_ch').html(txt);
	})    
});

var rinittemplatealshow=function(){
	/*console.log('rinittemplatealshow()');*/
	var tabactivo=$('.tabhijo.active').find('.plantilla');
	tabactivo.each(function(index, el) {
		if($(el).hasClass('plantilla-speach')){
			var audiostmp= $(el).find('audio');
		    audiostmp.each(function(i, audiotag){
		        var url=$(audiotag).attr('src');        
		        var donde=$(audiotag).closest('.pnl-speach');
		        donde.find('.grafico').html('');
		        crearonda(url,donde,true);
		    }); 
		}
	});
}