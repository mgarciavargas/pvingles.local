//selecionar un media
//var _sysdominio=document.domain;
var addtext1=function(e,obj){
  var in_=$(obj);
  var texto_inicial = in_.text().trim();
  in_.attr('data-initial_val', texto_inicial);
  in_.html('<input type="text" required="required" class="_intxtadd form-control" value="'+ texto_inicial +'">');
  var cini=$('input',in_).parent().siblings('input.valin').attr('data-cini');
  if(cini==''||cini==undefined){
    $('input',in_).parent().siblings('input.valin').attr('data-cini',1);
    $('input',in_).parent().siblings('input.valin').attr('data-valueini',texto_inicial);
  }
  $('input',in_).focus().select();
}
var addtext1blur=function(e,obj){
	var in_=$(obj);
	var dataesc=in_.attr('data-esc'); 
  var vini=in_.parent().attr('data-initial_val');
  var vin=in_.val();
  var dvaliniin=in_.parent().siblings('input.valin').attr('data-valueini');
  if(dataesc) _intxt=vini;
  else _intxt=vin;
  if(_intxt==dvaliniin)vin='';
  if(vin)in_.parent().siblings('data-cini',2);
  if(_intxt=='')_intxt = dvaliniin;
  in_.removeAttr('data-esc');
  in_.parent().attr('data-initial_val',_intxt);
  in_.parent().siblings('input.valin').val(vin);       
  var vpara=in_.parent().attr('data-valpara');
  if(vpara!=''&&vpara!=undefined){
    var vdonde=in_.parent().attr('data-valen');
    if(vdonde!=''&&vdonde!=undefined) $(vpara).attr(vdonde,_intxt);
  }
  in_.parent().text(_intxt);
}

var activarTabsEjercicios = function(valor){
  var $contenTabs = $('#met-3 ul.nav-tabs.ejercicios');
  if(!valor){
    $contenTabs.find('li:first-child a').trigger('click');
    if( !$contenTabs.find('li').hasClass('disabled') ) { 
      $contenTabs.find('li').addClass('disabled');
    }
  }else{
    if( $contenTabs.find('li').hasClass('disabled') ) { 
      $contenTabs.find('li').removeClass('disabled');
    }
  }
};



/*  ***  */


var showvideoayudatemplate=function(show){
  var infotemplate=$('.metod.active  .tabhijo.active .aquicargaplantilla');
  var ventana=infotemplate.attr('data-tpl');
  var videohelp=infotemplate.attr('data-helpvideo')||'';
  var videohelptitle=infotemplate.attr('data-tpltitulo')||'aiv';
  var nameventanahelp='show_helvideo_'+ventana+'_'+(videohelptitle.replace(/\s/g,"_"));
  var showv=sessionStorage.getItem(nameventanahelp);

  if(videohelp!=''){
    htmlvideo='<div class="col-xs-12">'             
    +'<div class="embed-responsive embed-responsive-16by9 mascara_video">'
    +'<video controls="true" class="valvideo embed-responsive-item vid_11_58823ebedfa33" src="'+_sysUrlBase_+'/static/sysplantillas/'+videohelp+'.mp4"></video>'         
    +'</div>'
    +'</div>';
    htmlfooter=$('#footernoshow').html();
    if((showv==null||showv==undefined||showv=='si')||show==true){
      var modaldata={
        titulo:'Guide Video: ' + videohelptitle,
        htmltxt:htmlvideo,
        ventanaid:'vent-'+nameventanahelp,
        borrar:false,
        showfooter:true
      }
      var modal=sysmodal(modaldata);
      if(modal!=false){
        modal.find('.modal-footer').prepend(htmlfooter);
        modal.on('click','.nomostrarmodalventana',function(){
          if($(this).is(':checked'))
            sessionStorage.setItem(nameventanahelp,'no');
          else
            sessionStorage.setItem(nameventanahelp,'si');
        });
      }
    }
  }
};



//callback despues de biblioteca en: FICHAS, ORDENAR_SIMPLE
var lastBtnClicked = null;
function activarBtn(){
    var $btn = $(lastBtnClicked);
    var classElem = $btn.attr('data-url');
    if( $btn.data('tipo') == 'image' ){
        var src = $btn.siblings('img'+classElem).attr('src');
        if( src!='' && src!=undefined ){
            $btn.removeClass('btn-default').addClass('btn-success');
        } else {
            $btn.removeClass('btn-success').addClass('btn-default');
        }
    }
    if( $btn.data('tipo') == 'audio' ){
        var src = $btn.siblings('audio'+classElem).data('audio');
        if( src!='' && src!=undefined ){
            $btn.removeClass('btn-default').addClass('btn-orange');
        } else {
            $btn.removeClass('btn-orange').addClass('btn-default');
        }
    }
    lastBtnClicked = null;
}

//callback despues de biblioteca en: IMG_PUNTOS
function borrarTagsPuntos(){
  var $tmplActiva = getTmplActiva();
  var $plantillaActiva = $tmplActiva.find('.plantilla');
  var idTmpl = $plantillaActiva.attr('id');
  $('#'+idTmpl+'  .mask-dots').html('');
  $('#'+idTmpl+'  .tag-alts-edit').html('');
}

//funciones de inicio  en editar
var iniciarPlantilla = function(plantilla=''){
  if(plantilla!='' && plantilla=='multi') {
    addBtnsEdicionMCE();
    var tpl_alternatives=$('.metod.active .tabhijo.active .tpl_plantilla .tpl_alternatives');
    if($('.panel',tpl_alternatives).text().trim()=='') tpl_alternatives.hide(); /*si alternativa esta vacio lo oculta*/
    mostrarEditorMCE();
  }
  showvideoayudatemplate();
};

var sysdragstart=function(ev,_obj){ //isdragable
    obj=_obj.helper;  
    var d = new Date();     
    var id="idtmp"+d.getDate()+"_"+d.getMonth()+"_"+d.getFullYear()+''+d.getHours()+''+d.getMinutes()+''+d.getSeconds();
    $(obj).addClass('active').attr('id',id);    
}
var sysdragend=function(ev,_obj){ //isdragable
    obj=_obj.helper;    
    $(obj).closest('.tpl_plantilla').find('.isdragable').removeClass('active');
    $(obj).removeAttr('id').attr('style','position:relative');
    
}

var sysdrop=function(event,ui,obj){ //isdrop
    var _this = obj;     
    if($(_this).hasClass('_success_')){ return false;}
    var isDBY = esDBYself($(_this));
    var dragable=ui.draggable;
    texto=$(dragable).text().trim();
    if(isDBY){
        var isCompleto = inputMCE_Completo( $(_this) );
        var condicion = ( isDBY && !isCompleto );
        var buscar = 'input.mce-object-input',
        buscarCorrectos = '.inp-corregido[data-corregido="good"]';
        var fn = function(){
          $(_this).val(texto);
          $(_this).attr('value', texto);
          evaluarRespuesta(_this);
          if(texto==$(_this).attr('data-texto').trim()){
            $(_this).addClass('_success_');
            borrardraganddrog($(_this).closest('.panelEjercicio'),true,$(dragable));
          }else  borrardraganddrog($(_this).closest('.panel'),false,$(dragable));
        };
        fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
    }else{
        $(_this).val(texto);
        $(_this).attr('value', texto);
        evaluarRespuesta(_this);
        if(texto==$(_this).attr('data-texto').trim()){
          $(_this).addClass('_success_');
          borrardraganddrog($(_this).closest('.panelEjercicio'),true,$(dragable))
        }else  borrardraganddrog($(_this).closest('.panel'),false,$(dragable));
          
    }
}

var borrardraganddrog=function(pnl,corr,opt){
  let inputs=pnl.find('input.mce-object-input');
  if(inputs.length==1){
    if(corr==true){
      opt.addClass('hidden');
      opt.siblings('.isdragable').addClass('hidden');
    }else{
      opt.addClass('hidden');
    }
  }else{
    let alt=opt.closest('.panelAlternativas').find('.isdragable');
    if(corr==true){      
      $.each(alt,function(){
        if(!$(this).hasClass('hiddenok')){
          $(this).removeClass('hidden');
        }
      })
      opt.addClass('hiddenok hidden');     
    }else{
      $.each(alt,function(){
        if(!$(this).hasClass('hiddenok')){
          $(this).removeClass('hidden');
        }
      })
      opt.addClass('hidden');     
    }
  }
}