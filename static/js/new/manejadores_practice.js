var handlerInputMCE_focusin = function(e,params=false){
    var idPanel =  params.id;
    var _this =  params.obj;
    e.preventDefault();
    if(!$(_this).hasClass('active') ){
        $(idPanel).find('input.mce-object-input').removeClass('open active');
        removerTodosFlotantes(idPanel); 
    }
    if( !$(_this).hasClass('open')){
        $(_this).addClass('open active');
        var id = generarID(_this);
        prepararInput(_this, id);        
        if( $(_this).hasClass('isayuda') ){
            desplegarAyuda(_this, id);
        }
        if( $(_this).hasClass('ischoice') ){
            desplegarOpciones(_this, id);
        }
        if( !$(_this).hasClass('iswrite') ) {
            $(_this).attr('readonly', 'readonly');
        } else  $(_this).focus();
    }
};

var handlerBtnHelp_click = function(e){
    e.preventDefault();
    e.stopPropagation();
    var $inputMCE = $(this).parent().siblings('input[data-mce-object="input"]');
    if( $(this).find('span.tip-word').length==0 ){
        $(this).append('<span class="tip-word"> : </span>');
    }
    var rspta= $(this).parents('.wrapper').find('input.open').data('texto');
    //if(rspta!=undefined && rspta!='') {
        var arr_rspta= rspta.split("");
        var i = $('span.tip-word').attr('data-pulsado');
        if(i==''||i==undefined){
            i = 0;
        }
        i = parseInt(i);
        if( i < arr_rspta.length){
            $('span.tip-word').attr('data-pulsado',(i+1));
            var texto = $('span.tip-word').text();
            texto = texto.concat(arr_rspta[i]);
            $('span.tip-word').text(texto);
        }
    //}
    $inputMCE.focus();
};

var handlerInputMCE_focusOut = function(e, idPanel){
    $(this).off('focusout')
    /*if( e.data.focusOut){
        if( $(this).hasClass('iswrite') && $(this).val().length>0 ) {
            evaluarRespuesta($(this));
        }
        if( e.data.blur){
            cerrarFlotantes($(this));
        }
    }*/
    $(this).removeClass('tmp_activo');
};