
(function($){
    $.fn.iniciarVisor = function(opciones){
        var opts = $.extend({}, $.fn.iniciarVisor.defaults, opciones);
        return this.each(function() {
            var $that = $(this);
            //var $contenedor = $that.parent();
            /*var random = Math.floor(Math.random()*Math.pow(10,10));*/
            var now = Date.now();
            $that.attr('aria-contenedorventana', 'ventana_'+now);
            if (opts.btnGuardar==null) {
                var boton_guardar='<div class="col-xs-12">';
                boton_guardar+='<div class="col-xs-12 text-right">';
                boton_guardar+='<button class="btn btn-success guardar_progreso_ventana">';
                boton_guardar+=opts.texto.guardar;
                boton_guardar+='</button>';
                boton_guardar+='</div></div>';
                $that.after(boton_guardar);
                opts.btnGuardar = '.guardar_progreso_ventana';
                
                $(opts.btnGuardar).attr({
                    'id': 'guardar_progreso_ventana_'+now,
                    'data-key': now,
                });
                
                $('body').on('click', opts.btnGuardar, function(e) {
                    e.preventDefault();
                    if(opts.fnGuardarProgreso!=null){ opts.fnGuardarProgreso(); }
                    else{ throw 'Param fnGuardarProgreso is null.' }
                });
            }

            var init = function(){
                if(opts.fnAlIniciar!=null){ opts.fnAlIniciar(); }
            };

            init();
        });
    };

    $.fn.iniciarVisor.defaults = {'btnGuardar':null, 'texto':{}, 'fnGuardarProgreso': null, 'fnAlIniciar': null};
}(jQuery));