//selecionar un media
//var _sysdominio=document.domain;
var selectedfile=function(e,obj,txt,fcall){
	var tipo= $(obj).attr('data-tipo');
  var donde= $(obj).attr('data-url');
  var fcall='&fcall='+fcall;
  if(tipo==''||tipo==undefined)return false;
  var rutabiblioteca= _sysUrlBase_+'/biblioteca/?plt=modal&robj=afile&donde='+donde+'&type='+tipo+fcall;
  var data={
    titulo:txt+' - '+tipo,
    url:rutabiblioteca,
    ventanaid:'biblioteca-'+tipo,
    borrar:true
  }
  var modal=sysmodal(data);
}

var cerrarmodal=function(){   //cerrar ventana de seleccionar file
  $('.btncloseaddinfotxt').trigger('click');
  $('.addinfotitle').html('');
}

var addtext1=function(e,obj){
  var in_=$(obj);
  var texto_inicial = in_.text().trim();
  in_.attr('data-initial_val', texto_inicial);
  in_.html('<input type="text" required="required" class="_intxtadd form-control" value="'+ texto_inicial +'">');
  var cini=$('input',in_).parent().siblings('input.valin').attr('data-cini');
  if(cini==''||cini==undefined){
    $('input',in_).parent().siblings('input.valin').attr('data-cini',1);
    $('input',in_).parent().siblings('input.valin').attr('data-valueini',texto_inicial);
  }
  $('input',in_).focus().select();
}
var addtext1blur=function(e,obj){
	var in_=$(obj);
	var dataesc=in_.attr('data-esc');
  var vini=in_.parent().attr('data-initial_val');
  var vin=in_.val();
  var dvaliniin=in_.parent().siblings('input.valin').attr('data-valueini');
  if(dataesc) _intxt=vini;
  else _intxt=vin;
  if(_intxt==dvaliniin)vin='';
  if(vin)in_.parent().siblings('data-cini',2);
  if(_intxt=='')_intxt = dvaliniin;
  in_.removeAttr('data-esc');
  in_.parent().attr('data-initial_val',_intxt);
  in_.parent().siblings('input.valin').val(vin);
  var vpara=in_.parent().attr('data-valpara');
  if(vpara!=''&&vpara!=undefined){
    var vdonde=in_.parent().attr('data-valen');
    if(vdonde!=''&&vdonde!=undefined) $(vpara).attr(vdonde,_intxt);
  }
  in_.parent().text(_intxt);
}


var mostrarHabilidadesActivas = function(idPanelContenedor){
  var cant_input_hidden = $(idPanelContenedor + ' input[type="hidden"].selected-skills').length;
  if( cant_input_hidden == 1){
    var input_hidden_value = $(idPanelContenedor + ' input[type="hidden"].selected-skills').val();
  } else {
    var input_hidden_value = $(idPanelContenedor + ' .tab-content>.tab-pane.active input[type="hidden"].selected-skills').val();
  }

  if( input_hidden_value!='' && input_hidden_value!=undefined ){
    var arr_values = input_hidden_value.split('|');
    $('#actividad-body .widget-box div.habilidades .btn-skill').each(function() {
      $(this).removeClass('active');
      var id_skill = $(this).attr('data-id-skill');
      $.each(arr_values, function(index, elem) {
        $('#actividad-body .widget-box div.habilidades .btn-skill[data-id-skill="'+elem+'"]').addClass('active', {duration:200});
      });
    });
  }
};


var activarTabsEjercicios = function(valor){
  var $contenTabs = $('#met-3 ul.nav-tabs.ejercicios');
  if(!valor){
    $contenTabs.find('li:first-child a').trigger('click');
    if( !$contenTabs.find('li').hasClass('disabled') ) {
      $contenTabs.find('li').addClass('disabled');
    }
  }else{
    if( $contenTabs.find('li').hasClass('disabled') ) {
      $contenTabs.find('li').removeClass('disabled');
    }
  }
};



/*  ***  */
var mostrarEditorMCE  = function(){
  var tpl_plantilla=$('.metod.active .tabhijo.active .tpl_plantilla');
  tpl_plantilla.hide();
  var idgui=tpl_plantilla.attr('data-idgui');
  var html=tpl_plantilla.find('.panel.panelEjercicio').html();
  $('#txtarea'+idgui).html(html);
  $('#txtarea'+idgui).show();
  $('.metod.active .tabhijo.active .plantilla .btnedithtml').addClass('disabled');
  $('.metod.active .tabhijo.active .plantilla .btnsavehtmlsystem').removeClass('disabled');
  var info=$('.metod.active .tabhijo.active .aquicargaplantilla');
  var showtoolstiny=info.attr('data-tools')||'chingoinput';
  tinymce.init({
    relative_urls : false,
    convert_newlines_to_brs : true,
    menubar: false,
    statusbar: false,
    verify_html : false,
    content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
    selector: '#txtarea'+idgui,
    height: 250,
    paste_auto_cleanup_on_paste : true,
    paste_preprocess : function(pl, o) {
             var html='<div>'+o.content+'</div>';
            var txt =$(html).text();
            o.content = txt;
        },
    paste_postprocess : function(pl, o) {
            o.node.innerHTML = o.node.innerHTML;
        },
    plugins:[showtoolstiny+" chingosave textcolor paste" ],  // chingoinput chingoimage chingoaudio chingovideo
    toolbar: 'chingosave | undo redo chingodistribution | styleselect |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor | '+showtoolstiny //chingoinput chingoimage chingoaudio chingovideo
  });
};

var showvideoayudatemplate=function(show){
  var infotemplate=$('.metod.active  .tabhijo.active .aquicargaplantilla');
  var ventana=infotemplate.attr('data-tpl');
  var videohelp=infotemplate.attr('data-helpvideo')||'';
  var videohelptitle=infotemplate.attr('data-tpltitulo')||'aiv';
  var nameventanahelp='show_helvideo_'+ventana+'_'+(videohelptitle.replace(/\s/g,"_"));
  var showv=sessionStorage.getItem(nameventanahelp);

  if(videohelp!=''){
    htmlvideo='<div class="col-xs-12">'
    +'<div class="embed-responsive embed-responsive-16by9 mascara_video">'
    +'<video controls="true" class="valvideo embed-responsive-item vid_11_58823ebedfa33" src="'+_sysUrlBase_+'/static/sysplantillas/'+videohelp+'.mp4"></video>'
    +'</div>'
    +'</div>';
    htmlfooter=$('#footernoshow').html();
    if((showv==null||showv==undefined||showv=='si')||show==true){
      var modaldata={
        titulo:'Guide Video: ' + videohelptitle,
        htmltxt:htmlvideo,
        ventanaid:'vent-'+nameventanahelp,
        borrar:false,
        showfooter:true
      }
      var modal=sysmodal(modaldata);
      if(modal!=false){
        modal.find('.modal-footer').prepend(htmlfooter);
        modal.on('click','.nomostrarmodalventana',function(){
          if($(this).is(':checked'))
            sessionStorage.setItem(nameventanahelp,'no');
          else
            sessionStorage.setItem(nameventanahelp,'si');
        });
      }
    }
  }
};
var addBtnsEdicionMCE = function(){
  $('.metod.active .tabhijo.active .btn-toolbar').remove();
  var tpl_plantilla= $('.metod.active .tabhijo.active .tpl_plantilla');
  tpl_plantilla.parent().prepend($('#tpledittoolbar').html());
  $('.btn-toolbar',tpl_plantilla).attr('data-idgui',tpl_plantilla.attr('data-idgui')).addClass('nopreview');
};



$('.actividad-main-content')
  .on('click','.btndistribucion',function(ev){  //dividir paneles sin idguipara todos
    ev.preventDefault();
    var tpltxt=$('.metod.active .tabhijo.active .tpl_plantilla .discol1');
    var tplalter=$('.metod.active .tabhijo.active .tpl_plantilla .discol2');
    if(tpltxt.hasClass('col-md-12')){
      tpltxt.removeClass().addClass('col-md-6 col-sm-7 col-xs-12 discol1');
      tplalter.removeClass().addClass('col-md-6 col-sm-5 col-xs-12 discol2');
    }else{
      tpltxt.removeClass().addClass('col-md-12 col-sm-12 col-xs-12 discol1');
      tplalter.removeClass().addClass('col-md-12 col-sm-12 col-xs-12 discol2');
    }
  })
  .on('click','.btnedithtml',function(ev){
    ev.preventDefault();
    if($(this).hasClass('disabled')){
      return false;
    }else{
      mostrarEditorMCE();
    }
  })
  .on('click','.btnsavehtmlsystem',function(ev){
      if($(this).hasClass('disabled')) return false;
      if($('#info-avance-alumno').hasClass('editandoinput')){
         $('#setting-textbox .save-setting-textbox').trigger('click');
      }
      var tabhijo=$(this).closest('.aquicargaplantilla');
      var tpl_plantilla=$('.tpl_plantilla',tabhijo);
      var panelEjercicios=$('.panelEjercicio',tpl_plantilla);
      $('.plantilla .btnedithtml',tabhijo).removeClass('disabled');
      tinyMCE.triggerSave();
      var idgui=tpl_plantilla.attr('data-idgui');
      var txt=$('#txtarea'+idgui).val();
      panelEjercicios.html(txt);
      tpl_plantilla.show();

      tinymce.remove('#txtarea'+idgui);
      $('#txtarea'+idgui).hide();
      var inpdrag=panelEjercicios.find('input.isdrop[data-mce-object="input"]');
      var ninpdrag=inpdrag.length;
      var spanalt=[];
      inpdrag.each(function(){
        if(ninpdrag>1){ //draggable="true"  ondragend="sysdragend(this,event)"
          spanalt.push('<span class="isdragable" >'+$(this).attr('data-texto')+'</span>');
        }else{
          var opt=$(this).attr('data-options');
          if(opt!='' && opt!=undefined){
            spanalt.push('<span class="isdragable">'+$(this).attr('data-texto')+'</span>');
            opt = opt.split(',');
            $.each(opt,function(index,value){
              spanalt.push('<span class="isdragable">'+value+'</span>');
            });
          }
        }
      });
      spanalt.sort(function(){return Math.random() - 0.5});
      var inpClicked=panelEjercicios.find('input.isclicked[data-mce-object="input"]');

      if(inpClicked.length>0){
        desplegarAlternativas(idgui);
      } else {
         plnaleternativas= $('.panelAlternativas',tpl_plantilla);
         plnaleternativas.html(spanalt.toString());
         if($('span.isdragable',plnaleternativas).length>0)
          funcionalidaddraganddrop(tabhijo);
      }


      var $imgs = panelEjercicios.find('img[data-mce-object="image"]');
      if($imgs.length>0){
          $imgs.addClass('gray-scale');
      }
      $('.panelAlternativas',tpl_plantilla).parent().removeClass('hide').removeAttr('style');
      if($('.panelAlternativas',tpl_plantilla).text()=='')
        $('.panelAlternativas',tpl_plantilla).parent().addClass('hide');
      $(this).addClass('disabled');

  })
  .on('click','.vervideohelp',function(e){
    showvideoayudatemplate(true);
  });


var guardaractividaddetalle=function(params){
  var msjeAttention = params.msje.attention;
  var IDGUI = params.idgui;
  $('.preview').trigger( "click" );
  var formData = new FormData($("#frm-"+IDGUI)[0]);
  $.ajax({
    url: _sysUrlBase_+'/actividad/guardar', // cls.WebActividad
    type: "POST",
    data:  formData,
    contentType: false,
    dataType :'json',
    cache: false,
    processData:false,
    success: function(data)
    {
      $('.back').trigger( "click" );
      if(data.code==='ok'){
        mostrar_notificacion(msjeAttention, data.msj, 'success');
      }else{
        mostrar_notificacion(msjeAttention, data.msj, 'warning');
      }
      return false;
    },
    error: function(xhr,status,error){
      mostrar_notificacion(msjeAttention, status, 'warning');
      $('.back').trigger( "click" );
      return false;
    }
  });
};

//callback despues de biblioteca en: FICHAS, ORDENAR_SIMPLE
var lastBtnClicked = null;
function activarBtn(){
    var $btn = $(lastBtnClicked);
    var classElem = $btn.attr('data-url');
    if( $btn.data('tipo') == 'image' ){
        var src = $btn.siblings('img'+classElem).attr('src');
        if( src!='' && src!=undefined ){
            $btn.removeClass('btn-default').addClass('btn-success');
        } else {
            $btn.removeClass('btn-success').addClass('btn-default');
        }
    }
    if( $btn.data('tipo') == 'audio' ){
        var src = $btn.siblings('audio'+classElem).data('audio');
        if( src!='' && src!=undefined ){
            $btn.removeClass('btn-default').addClass('btn-orange');
        } else {
            $btn.removeClass('btn-orange').addClass('btn-default');
        }
    }
    lastBtnClicked = null;
}

//callback despues de biblioteca en: IMG_PUNTOS
function borrarTagsPuntos(){
  var $tmplActiva = getTmplActiva();
  var $plantillaActiva = $tmplActiva.find('.plantilla');
  var idTmpl = $plantillaActiva.attr('id');
  $('#'+idTmpl+'  .mask-dots').html('');
  $('#'+idTmpl+'  .tag-alts-edit').html('');
}

//funciones de inicio  en editar
var iniciarPlantilla = function(plantilla=''){
  if(plantilla!='' && plantilla=='multi') {
    addBtnsEdicionMCE();
    var tpl_alternatives=$('.metod.active .tabhijo.active .tpl_plantilla .tpl_alternatives');
    if($('.panel',tpl_alternatives).text().trim()=='') tpl_alternatives.hide(); /*si alternativa esta vacio lo oculta*/
    mostrarEditorMCE();
  }
  showvideoayudatemplate();
};

var sysdragstart=function(ev,_obj){ //isdragable
    obj=_obj.helper;
    var d = new Date();
    var id="idtmp"+d.getDate()+"_"+d.getMonth()+"_"+d.getFullYear()+''+d.getHours()+''+d.getMinutes()+''+d.getSeconds();
    $(obj).addClass('active').attr('id',id);
}
var sysdragend=function(ev,_obj){ //isdragable
    obj=_obj.helper;
    $(obj).closest('.tpl_plantilla').find('.isdragable').removeClass('active');
    $(obj).removeAttr('id').attr('style','position:relative');
}

var sysdrop=function(event,ui,obj){ //isdrop
    var _this = obj;
    var isDBY = esDBYself($(_this));
    var dragable=ui.draggable;
    texto=$(dragable).text().trim();
    if(isDBY){
        var isCompleto = inputMCE_Completo( $(_this) );
        var condicion = ( isDBY && !isCompleto );
        var buscar = 'input.mce-object-input',
        buscarCorrectos = '.inp-corregido[data-corregido="good"]';
        var fn = function(){
          $(_this).val(texto);
          evaluarRespuesta(_this);
        };
        fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
    }else{
        $(_this).val(texto);
        evaluarRespuesta(_this);
    }
}

var funcionalidaddraganddrop=function(cont){
    $('.plantilla-completar .isdragable',cont).draggable({
        start:function(event,ui){
            sysdragstart(event,ui);
        },
        stop:function(event,iu){
           sysdragend(event,iu)
        }
    });
    $('.plantilla-completar .isdrop',cont).droppable({
        drop:function(event,ui){
            sysdrop(event,ui,$(this));
        }
    });
}
