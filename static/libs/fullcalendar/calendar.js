

/*--------------------------calendar variables--------------------------*/
var getCalendars = function() {
  $cal = $('#calendar');
  $cal1 = $('#calendar1');
  $cal2 = $('#calendar2');
}

/* -------------------manage cal2 (right pane)------------------- 
var initializeRightCalendar = function()  {
  $cal2.fullCalendar('changeView', 'agendaDay');

  $cal2.fullCalendar('option', {
    slotEventOverlap: false,
    allDaySlot: false,
    header: {
      right: 'prev,next today'
    },
    selectable: true,
    selectHelper: true,
    select: function(start, end) {
        newEvent(start);
    },
    eventClick: function(calEvent, jsEvent, view) {
        editEvent(calEvent);
    },
  });
}
*/


/* -------------------moves right pane to date------------------- */
/*var cal2GoTo = function(date) {
  $cal2.fullCalendar('gotoDate', date);
}*/
var cal1GoTo = function(date) {
  $cal1.fullCalendar('gotoDate', date);
}
/*var loadEvents = function() {
  $.getScript("js/events.js", function(){
  });
}
*/

var newEvent = function(start,end) {
  console.log('start : ', start);
  console.log('end : ', end);
  var fecha=start.format('YYYY-MM-DD H:mm');
  var fechaFin=end.format('YYYY-MM-DD H:mm');
  //$('input#color').checked;
  $('input#title').val("");
  $("input#fecha").val(fecha);
  $("input#fechaFin").val(fechaFin);
  $('#newEvent').find('.modal-body').show();
  $('#newEvent').find('.modal-footer').show();
  $('#newEvent').find('.cargando').hide();
  $('#delete').addClass('hidden');
  $('#newEvent').modal('show');
  $('#submit').unbind();
  $('#submit').on('click', function() {
    var title = $('input#title').val();
    var horaInicial = $("input#fecha").val();
    var horaFinal = $('input#fechaFin').val();
    var color = $('input#colores:checked').val();
    if (title) {
      var eventData = {
        title: title,
        start: horaInicial,
        end: horaFinal,
        backgroundColor: color,
        borderColor: color
      };
      $.ajax({
        url: _sysUrlBase_+'/agenda/guardarAgenda',
        type: "POST",
        data: {
          'txtTitulo':eventData.title,
          'txtFecha_inicio':eventData.start,
          'txtFecha_final':eventData.end,
          'txtColor':eventData.backgroundColor,
        },
        dataType: 'json',
        beforeSend:function(){
          $('#newEvent').find('.modal-body').hide();
          $('#newEvent').find('.modal-footer').hide();
          $('#newEvent').find('.cargando').show();
        }
      }).done(function(resp) {
        if(resp.code == 'ok'){
          console.log(eventData);
          eventData.id = resp.newid;
          $cal.fullCalendar('renderEvent', eventData, true);
        }else{
          alert('Could not be saved. try again.'); 
        }
      }).fail(function(r) {
        console.log("error",r);
      }).always(function(){
        $('#newEvent').modal('hide');
      });
    }
    else {
      alert("Ingrese Título")
    }
  });
}

var editEvent = function(calEvent) {
  console.log(calEvent);
  $('input#idagenda').val(calEvent.id);
  $('input#title').val(calEvent.title);
  $('input#colores[value="'+calEvent.backgroundColor+'"]').prop('checked', 'true');
  $('input#fecha').val(calEvent.start._i);
  $('input#fechaFin').val(calEvent.end._i);
  $('#newEvent').find('.modal-body').show();
  $('#newEvent').find('.modal-footer').show();
  $('#newEvent').find('.cargando').hide();
  $('#delete').removeClass('hidden');
  $('#newEvent').modal('show');
  $('#submit').unbind();
  $('#delete').unbind();
  $('#submit').on('click', function() {
    var id = $('input#idagenda').val();
    var title = $('input#title').val();
    var horaInicial = $('input#fecha').val();
    var horaFinal = $('input#fechaFin').val();
    var color = $('input#colores:checked').val();
    $('#newEvent').modal('hide');

    if (title) {
      calEvent.title = title,
      calEvent.start = horaInicial,
      calEvent.end = horaFinal,
      calEvent.backgroundColor = color,
      calEvent.borderColor = color
     $.ajax({
       url: _sysUrlBase_+'/agenda/guardarAgenda',
       type: "POST",
       dataType: 'json',
       data: {
        'id':id,
        'txtTitulo':title,
        'txtFecha_inicio':horaInicial,
        'txtFecha_final':horaFinal,
        'txtColor':color,
       },
       beforeSend:function(){
          $('#newEvent').find('.modal-body').hide();
          $('#newEvent').find('.modal-footer').hide();
          $('#newEvent').find('.cargando').show();
       }
      }).done(function(rep) {
        if(rep.code == 'ok'){
            console.log(calEvent);
            calEvent.id = resp.new_id;
            $cal.fullCalendar('updateEvent', calEvent);
        }else{
            alert('Could not be update. try again.'); 
        }
      }).fail(function(r) {
        console.log("error",r);
      });
    } else {
      alert("Ingrese Título")
    }
  });
  $('#delete').on('click', function() {
    var id = $('input#idagenda').val();
    if (id){
      $.ajax({
        url: _sysUrlBase_+'/agenda/eliminar',
        type: "POST",
        dataType: 'json',
        data: { 'id':id, },
        /*beforeSend:function(){
          $('#newEvent').find('.modal-body').hide();
          $('#newEvent').find('.modal-footer').hide();
          $('#newEvent').find('.cargando').show();
        }*/
      }).done(function(rep) {
        if(rep.code == 'ok'){
            $('#newEvent').modal('hide');
            $cal.fullCalendar('removeEvents', [rep.id]);
        }else{
            alert('Could not be delete. try again.'); 
        }
      }).fail(function(r) {
        console.log("error",r);
      });
    //  $cal1.fullCalendar('removeEvents', [getCal1Id(calEvent._id)]);
      
    } else {
      $cal.fullCalendar('removeEvents', [calEvent._id]);
    }
    $('#editEvent').modal('hide');
  });
}

/* --------------------------load date in navbar-------------------------- */
var showTodaysDate = function() {
  n =  new Date();
  y = n.getFullYear();
  d = n.getDate();
  m = n.getMonth() + 1;
  $("#todaysDate").html("Today is " + d + "/" + m + "/" + y);
};

/* full calendar gives newly created given different ids in month/week view
    and day view. create/edit event in day (right) view, so correct for
    id change to update in month/week (left)
  */
var getCal1Id = function(cal1Id) {
  var num = cal1Id - 1;
  var id = num;
  return id;
}

var disableEnter = function() {
  $('body').bind("keypress", function(e) {
      if (e.keyCode == 13) {
          e.preventDefault();
          return false;
      }
  });
}
