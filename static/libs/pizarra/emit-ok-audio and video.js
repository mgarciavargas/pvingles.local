/*************************************   General   ******************************************************/
(function() {
    var params = {},
        r = /([^&=]+)=?([^&]*)/g;
    function d(s) {
        return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1)))
        params[d(match[1])] = d(match[2]);
    window.params = params;
})();




/***********************************************************************************************/

function showPage(page_no) {
        __PDFPAGE_RENDERING_IN_PROGRESS = 1;
        __PDFCURRENT_PAGE = page_no;

        // Disable Prev & Next buttons while page is being loaded
        //$("#pdf-next, #pdf-prev").attr('disabled', 'disabled');

        // While page is being rendered hide the canvas and show a loading message
        $("#micanvas").hide();
        //$("#page-loader").show();

        // Update current page in HTML
        $("#pdf-current-page").text(page_no);
        
        // Fetch the page
        __PDF_DOC.getPage(page_no).then(function(page) {// As the canvas is of a fixed width we need to set the scale of the viewport accordingly
            var wcanvas=$('#micanvaspdf').outerWidth();
            var scale_required = __CANVAS.width / page.getViewport(1).width;        
            var viewport = page.getViewport(1.5);// Get viewport of the page at required scale  
            __CANVAS.width = viewport.width;
            __CANVAS.height = viewport.height;
            canvasfabric.setWidth(viewport.width);
            canvasfabric.setHeight(viewport.height);
            var renderContext = {
                canvasContext: __CANVAS_CTX,
                viewport: viewport
            };
            
            // Render the page contents in the canvas
            page.render(renderContext).then(function() {
                __PDFPAGE_RENDERING_IN_PROGRESS = 0;

                var myImage = __CANVAS.toDataURL("image/png");
                fabric.Image.fromURL(myImage, function(img) {
                    canvasfabric.backgroundImage = img;
                    canvasfabric.backgroundImage.width = __CANVAS.width;
                    canvasfabric.backgroundImage.height =  __CANVAS.height;
                    canvasfabric.renderAll();
                });
                $("#micanvas").show();
                $(".pdffileok").show(); 
            });
        });
}



function showPDF(pdf_url) {
    // Show the pdf loader
    $("#pdf-loader").show();

    PDFJS.getDocument({ url: pdf_url }).then(function(pdf_doc) {
        __PDF_DOC = pdf_doc;
        __PDFTOTAL_PAGES = __PDF_DOC.numPages;
        $("#pdf-total-pages").text(__PDFTOTAL_PAGES);
        showPage(1);
    }).catch(function(error) {
             
    });
}


$(document).ready(function(){
function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}

function geturl() {
    var loc = window.location;
    var pathname = window.location.pathname;
    return loc+pathname;
}
function medidas(id){
    return {w:document.getElementById(id).offsetWidth,h:document.getElementById(id).offsetHeight}
}

/************************************************************************************************/

var infokey='chingo-sala-emit'; 
var connection = new RTCMultiConnection();
    var _detectRTC=connection.DetectRTC;
    connection.session = {audio: true, video: false, screen:false, data: true }; 
    connection.socketURL ='https://rtcmulticonnection.herokuapp.com:443/'; //'https://socket.skyrooms.io:443/';
    //connection.setCustomSocketHandler(SSEConnection);
    connection.socketMessageEvent = infokey;
    connection.direction = 'many-to-many';
    connection.sdpConstraints.mandatory = { OfferToReceiveAudio: true,OfferToReceiveVideo: true};
    connection.autoCloseEntireSession = false;    
    connection.maxParticipantsAllowed = 400;
    connection.leaveOnPageUnload = false;
    //connection.enableLogs = false;
    //connection._videosScreen = document.getElementById('videos-screen');
    connection._medias = document.getElementById('medias-container');
    connection._cameraemit = document.getElementById('container-webcams');

    var haymicro=false;    
    var haywebcam=false;
    var hayScreen=false;
    var urlBase=getAbsolutePath();
   
    var roomid=$('#infousersala ._sala').text();
    var userid=$('#infousersala ._user').text();
    var tmptypeusertxt=['U','P','M'];  // M=moderador , P=participante , U= usuario;
    var tmpidtype=$('#infousersala ._typeuser').text();//Math.round(Math.random() * (0 + 2) + 0);
    var typeuser=tmptypeusertxt[tmpidtype]; 
    var emiteruser=false;

/******************************************** funciones ****************************************/    
    var usersparticipante=$('#vparticipantes ul#usersparticipante');
    var userchatsms=$('#vchat select#userchatsms');

    function addparticipantes(jsonuser,soyyo){
        var userclone=$('#vparticipantes div#userclone').clone(true);
        userclone.find('#idtmp').attr('id',jsonuser.userid);        
        userclone.find('a.userstatus.mode'+jsonuser.typeuser).addClass('active');
        userclone.find('span.nombre').text(jsonuser.username);      
        var iclass=userclone.find('a.userstatus.active i').attr('class');
        userclone.find('a.miestatus i').removeAttr('class').addClass(iclass); 
        if(soyyo==true){            
            userclone.find('span.userchataccion').html('Yo');
            usersparticipante.prepend(userclone.html());
            if(typeuser=='M'){
                emiteruser=true;
            }
        }else{
            userclone.find('.lilinea').remove();
            usersparticipante.find('li.linea').before(userclone.html());
            userchatsms.append('<option value="'+jsonuser.userid+'"><span class="'+iclass+'"></span> '+jsonuser.username+'</option>');
        }
        if(typeuser!='M'){
            //usersparticipante.find('span.userchataccion .btn-group').hide(0);
            usersparticipante.find('a.miestatus').addClass('disabled');
            usersparticipante.find('a.miestatus span').hide(0);
        } 
        usersparticipante.find('#totaluser b').text(usersparticipante.find('li.liuser').length);
        connection.extra={username:userid,typeuser:typeuser};
        connection.updateExtraData();
    }

    function removeparticipante(userid){
        usersparticipante.find('li#'+userid).remove();
        userchatsms.find('option[value="'+userid+'"]').remove();
        usersparticipante.find('#totaluser b').text(usersparticipante.find('li.liuser').length);
    }

    function addmensajechat(data,user){
        var para=data.para;
        var currentTime = new Date();
        var time=currentTime.getHours()+':'+currentTime.getMinutes()+':'+currentTime.getSeconds();
        var txt='';
        if(user=='si'){
            txt='<div class="mensaje" id="'+connection.userid+'"><div class="pull-right text-left globoright"><b>Yo :</b><br>'+data.texto+'<br><div class="chattime">'+time+'</div></div><div class="clearfix"></div></div>';
        }else{
            _user=user.extra;
            if(para!='alluser'&&para!=connection.userid) return;
            var isprivate=para==connection.userid?'mensaje isprivate':'mensaje';
            txt='<div class="'+isprivate+'"  id="'+user.userid+'"><div class="pull-left text-left globoleft"><b>'+_user.username+' :</b><br>'+data.texto+'<br><div class="chattime">'+time+'</div></div><div class="clearfix"></div></div>';
        }
        var scrollHeight=$('.chatAll .mensajes')[0].scrollHeight;
        $('.chatAll .mensajes').append(txt).animate({scrollTop:scrollHeight}, 500);
    }

    function cambiartypeuser(data){
        var para=data.para;
        var mode=data.mode;
        var iclass=data.cls;
        if(para==roomid) return;
        if(para==connection.userid){
            if(mode!='M'){
                usersparticipante.find('span.userchataccion .btn-group').hide(0);
                usersparticipante.find('a.miestatus').addClass('disabled');
                usersparticipante.find('a.miestatus span').hide(0); 
                if(mode=='P')emiteruser=true;
                else emiteruser=false;
            }else{
                usersparticipante.find('span.userchataccion .btn-group').show(0);
                usersparticipante.find('a.miestatus').removeClass('disabled');
                usersparticipante.find('a.miestatus span').show(0);
                emiteruser=true;
            }           
            typeuser=data.mode;
        }else{
            if(mode!='U') emiteruser=false;
        }   
        var userachange=usersparticipante.find('li#'+para);
        userachange.find('.userstatus').removeClass('active');
        userachange.find('.miestatus i.fa').removeAttr('class').addClass(iclass);
        userachange.find('.userstatus.mode'+mode).addClass('active');       
    }

    function cambiarmicrouser(userremote,data){
        var para=data.para||'allusers';
        if(para=='allusers'){
            var audiouser=$('audio#'+userremote);
            if(data.microactive){
                if(audiouser) {
                    audiouser.prop("volume", 0.8);
                    audiouser.prop("muted",false);
                    audiouser.trigger('play');
                }
                $('#vparticipantes #usersparticipante li#'+userremote+' .btnmicro').addClass('active');
                $('#vparticipantes #usersparticipante li#'+userremote+' .btnmicro i').removeClass('fa-microphone-slash').addClass('fa-microphone bounceIn colorgreen');
                if(userremote==connection.userid){
                    $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone-slash').addClass('fa-microphone bounceIn colorgreen');
                    $('#menupersonalizado .menutools a.btnsharemicro').addClass('active');
                }
            }else{
                if(audiouser) audiouser.trigger('pause');
                $('#vparticipantes #usersparticipante li#'+userremote+' .btnmicro').removeClass('active');
                $('#vparticipantes #usersparticipante li#'+userremote+' .btnmicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
                if(userremote==connection.userid){
                    $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
                    $('#menupersonalizado .menutools a.btnsharemicro').removeClass('active');
                }
            }
        }
    }

    function cambiarmicroalluser(userremote,data){
        var obj=$('#vparticipantes #usersparticipante li.liuser');
        $.each(obj,function(i,v){           
            var id=$(v).attr('id');     
            if(id!=userremote){
                cambiarmicrouser(id,data)
            }           
        });
    }

    function alzarlamano(userremote,data){
        if(data.alzarlamano){
            if(typeuser!='U'){
                var btnmano=$('#vparticipantes #usersparticipante li#'+userremote+' .btnmano');
                btnmano.show();
                $('i',btnmano).removeClass('fa-hand-stop-o').addClass('fa-thumbs-up bounceIn colorgreen');              
            }
        }
    }
    function cancelaralzarmano(data){
        var btnmano=$('#vparticipantes #usersparticipante li#'+data.userremote+' .btnmano');
            btnmano.hide();     
        if(data.userremote==connection.userid){
            $('i',btnmano).removeClass('fa-thumbs-up bounceIn colorgreen').addClass('fa-hand-stop-o');
            $('#menupersonalizado a.btnalzarmano i').removeClass('fa-thumbs-up bounceIn colorgreen').addClass('fa-hand-stop-o');;
            $('#menupersonalizado .menutools a.btnalzarmano').removeClass('active');
        }
    }

    var camarasonline={}
    function shichcamara(userremote,data){      
        var videocam=  $('video#camera_'+userremote);
        if(data!=undefined){
            console.log(data);
            if(data.active==false){
                console.log(videocam);
                if(videocam.length>0){
                    camarasonline[userremote]=videocam;
                    videocam.remove();
                }
            }else{
                if(camarasonline[userremote]!=undefined){   
                    $('#container-webcams').append(camarasonline[userremote]);
                    videocam= $('video#camera_'+userremote);
                    setTimeout(function(){ videocam.trigger('play'); }, 3000);
                    var videos=$('#container-webcams').find('video');           
                    var total=100/videos.length;
                    if(videos.length>1&&total>19){
                        videos.css('width',total+'%');
                    }
                }
            }
        }
    }

    /*******************************funciones de pdf ********************************************/
    
    var sysencuestas={};
    function tomarencuesta(encuesta){
        var modal=$('#modalquestion').clone(true);
        modal.attr('id',encuesta.idencuesta);
        modal.find('.aquipregunta').text(encuesta.pregunta);
        var aquialternativas=modal.find('.aquialternativas');
        var alt_=encuesta.alternativas
        console.log(alt_);
        aquialternativas.html('');
        if(alt_!=undefined)
        $.each(alt_,function(i,v){
            var bt='<a href="#" data-value="'+i+'" class="btn btn-default btnresponder">'+i+'</a>';
            aquialternativas.append(bt);
        })
        modal.modal({backdrop: 'static', keyboard: false});
    }

    function responderencuesta(userremote,data){
        var id=data.id;
        var res=data.respuesta;
        if(sysencuestas[id]!=undefined){
            sysencuestas[id]['alternativas'][res]=sysencuestas[id]['alternativas'][res]+1;            
        }        
    }

    function mostraricono(userremote,data){
        var audiosound=$('#audiosonidos');
        var span='<span id="tmpicon'+userremote+'"><i class="'+data.icon+'"></i></span>';
        var li=$('#vparticipantes ul#usersparticipante li#'+userremote);
        $('.userchataccion',li).prepend(span);
        var elimino=false;
        if(data.sonido!=''){            
            audiosound.attr('src',_sysUrlStatic_+'/media/aulasvirtuales/audios/'+data.sonido);
            audiosound.trigger('play');   
            audiosound.on('ended',function(){                
               if(elimino==false)$('#tmpicon'+userremote,li).remove();
               elimino=true;
            });
        }
            if(elimino==false){
                setTimeout(function(){$('#tmpicon'+userremote,li).remove();},15000);
                elimino=true;
            }        
    }

    function addsharefile(userremote,data){
        var newfile=$('#vfiles #addfileshareclone').clone(true);
        newfile.find('.nombreuser').text(userremote);
        newfile.find('.nombrefile').text(data.namefile);
        newfile.find('.extension').text(data.extension);
        newfile.find('.link a').attr('href',_sysUrlStatic_+'/media/aulasvirtuales/'+data.namefile).attr('target','_blank');
        newfile.show('fast').removeAttr('id');
        $('#vfiles table').append(newfile);
        $('#vfiles').show();
    }

    function addsharevideo(userremote,data){
        $('#vvideos div.addvideoaqui').html('<video src="'+_sysUrlStatic_+'/media/aulasvirtuales/'+data.namefile+'" controls autoplay>');
        $('#vvideos').show();
    }
    function addscreenshot(userremote,data){
        $('#medias-container audio').hide();
        $('#medias-container video').remove();
        $img=$("#mediascrenshot");
        if($img.length>0)
            $img.attr('src',data.screen);
        else{
            var img='<img src="'+data.screen+'" width="100%" id="mediascrenshot">';        
            $('#medias-container').append(img);
        }    
        
        //connection._medias.appendChild(media);
        ventana=$('.showwindow[data-ventana="vemiting"]');
        if(ventana.hasClass('active')){
            $('#vemiting').show();
        }else{
            ventana.trigger('click');
        }
    }

    /******************************************** eventos sockets ********************************/
    connection.onopen = function(event){
        var extra=event.extra;
        var jsonuser={
            userid:event.userid,
            username:extra.username,
            typeuser:extra.typeuser
        }
        addparticipantes(jsonuser,false);
    }
    connection.onclose = function(event){
        var remoteuserid=event.userid;
        connection.disconnectWith(remoteuserid);
        removeparticipante(remoteuserid);
        $('audio#'+remoteuserid).trigger('pause').remove();
        $('video#camera_'+remoteuserid).trigger('pause').remove();
    }

    connection.onmessage=function(event){
        console.log(event);
        var data=event.data||'';
        var accion=data!=''?data.accion:'';
        var userremote=data.userremote||event.userid;       
        if(accion=='addmensajechat') addmensajechat(data,event);
        else if(accion=='cambiartypeuser') cambiartypeuser(data);
        else if(accion=='cambiarmicrouser') cambiarmicrouser(userremote,data);
        else if(accion=='cambiarmicroalluser') cambiarmicroalluser(event.userid,data);
        else if(accion=='alzarlamano') alzarlamano(userremote,data);
        else if(accion=='cancelaralzarmano') cancelaralzarmano(data);
        else if(accion=='shichcamara') shichcamara(event.userid,data);
        else if(accion=='tomarencuesta') tomarencuesta(data);
        else if(accion=='responderencuesta') responderencuesta(event.userid,data); 
        else if(accion=='mostraricono') mostraricono(userremote,data);
        else if(accion=='addsharefile') addsharefile(userremote,data); 
        else if(accion=='addsharevideo') addsharevideo(userremote,data);
        else if(accion=='addscreenshot') addscreenshot(userremote,data);
    }

    connection.onstream = function(event){
        var userremote=event.userid;        
        var _stream=event.stream;
        var tipouser=event.type;
        var media=event.mediaElement;    
        console.log(_stream);   
        if (_stream.isScreen){
            if(tipouser=='local') {
                $('.btnsharedesktop i').addClass('active bounceIn colorgreen');
            }
            if(tipouser=='remote'){
                $('#medias-container audio').hide();
                $('#medias-container video').remove();
                connection._medias.appendChild(media);
                setTimeout(function(){ media.play(); }, 5000); 

                ventana=$('.showwindow[data-ventana="vemiting"]');
                if(ventana.hasClass('active')){
                    $('#vemiting').show();
                }else{
                    ventana.trigger('click');
                }                       
            }
        }else if(_stream.isVideo){
            media.id='camera_'+userremote; 
            media.muted=true; 
            media.controls=false;
            connection._cameraemit.appendChild(media);
            camarasonline[userremote]=media;
            setTimeout(function() { media.play(); }, 5000);
            /******* adaptando video de camaras ***/
            var video=$('#container-webcams').find('video');
            var total=100/video.length;
            if(video.length>1&&total>19){
                video.css('width',total+'%');
            }
            if(tipouser=='remote'){
                ventana=$('.showwindow[data-ventana="vwebcam"]');
                if(ventana.hasClass('active')){
                    $('"vwebcam').show();
                }else{
                    ventana.trigger('click');
                }
            }
        }else if(_stream.isAudio){ 
                media.id=userremote;
                media.controls=true;
                media.muted=true        
            if(tipouser=='local'){
                microaudio.tengo='si';
                microaudio.media=media;
                return;
            }                   
            connection._medias.appendChild(media);
            setTimeout(function() { media.pause(); }, 5000);
        }
    }

    connection.onstreamended = function(event){
        if(event.stream.isScreen)
            if(event.type=="local"){                
                $('#menupersonalizado .menucompartir a.btnsharedesktop').removeClass('active');
                $('.btnsharedesktop i').removeClass('bounceIn colorgreen');
            }else{
                $('video#isScreen_'+event.userid).remove();             
            }
    }

    connection.onPeerStateChanged = function(state){
        if (state.iceConnectionState.search(/closed|failed/gi) !== -1) {
            Object.keys(connection.streamEvents).forEach(function(streamid) {
                var streamEvent = connection.streamEvents[streamid];
                if (streamEvent.userid === state.userid) {
                    connection.onstreamended(streamEvent);
                }
            });
        }
    }

    connection.getScreenConstraints = function(callback){
        getScreenConstraints(function(error, screen_constraints) {
            if (!error){
                screen_constraints = connection.modifyScreenConstraints(screen_constraints);
                callback(error, screen_constraints);
                return;
            }
            throw error;
        });
    }

    var lastSharedScreenShot = '';
    var _sysshareScreen=false;
    connection.sharePartOfScreen = function(element) {
        html2canvas(element, {
            onrendered: function(canvas){ 
                console.log(_sysshareScreen);
                if(_sysshareScreen==false) return;              
                var screenshot = canvas.toDataURL('image/png', 1);
                if(screenshot === lastSharedScreenShot){                    
                    connection.sharePartOfScreen(element);
                    return;
                }
                lastSharedScreenShot=screenshot;               
                var data={accion:'addscreenshot', screen:screenshot }
                connection.send(data);
                connection.sharePartOfScreen(element);                
            },
            grabMouse: true
        });
    };
    connection.stopPartOfScreenSharing=function(){
       _sysshareScreen=false;
    }

    /************************************** Eventos Botones ********************************************/
    var microaudio={tengo:'no',url:'',estado:'pausado'};
    var _detectRTC=connection.DetectRTC;

    $('.btnsharemicro').click(function(ev){
        _detectRTC.load(function(){
            if (_detectRTC.hasMicrophone === true) { // enable microphone 
                var btnsharemicro=$('#menupersonalizado .menutools a.btnsharemicro');
                btnsharemicro.toggleClass('active');
                var data={accion:'cambiarmicrouser',microactive:false};
                if(btnsharemicro.hasClass('active')) data.microactive=true;
                cambiarmicrouser(connection.userid,data);
                connection.send(data);
            }else{
                mostrar_notificacion('Informacion','No se Detecto Microfono, conecte y refresque la pagina','danger');
            }
        });
    });

    $('.btnalzarmano').click(function(ev){
        var btnalzarmano=$('#menupersonalizado .menutools a.btnalzarmano');
        var obj=$(this);
        var i=obj.find('i');
        btnalzarmano.toggleClass('active');
        if(btnalzarmano.hasClass('active')){
            if(i.length){
                i.removeClass('fa-hand-stop-o').addClass('fa-hand-pointer-o bounceIn colorgreen');
            }else{
                $('#menupersonalizado a.btnalzarmano i').removeClass('fa-hand-stop-o').addClass('fa-hand-pointer-o bounceIn colorgreen');
            }
            var data={accion:'alzarlamano',alzarlamano:true}
            connection.send(data);
        }else{
            var data={accion:'cancelaralzarmano',userremote:connection.userid}
            cancelaralzarmano({userremote:connection.userid});
            connection.send(data);
        }
    });

    $('#vparticipantes').on('click','#usersparticipante .btnmano',function(){
        $(this).hide();
        var userremote=$(this).closest('li').attr('id');
        var data={accion:'cancelaralzarmano',userremote:userremote}
        connection.send(data)
    });

    $('.btnshareventana').click(function(){
        var iobj=$('i',this);
        if(iobj.hasClass('fa-share')){
            iobj.removeClass('fa-share').addClass('fa-share-alt');
            $('#tmpscreenshot').removeAttr('id');          
            $(this).closest('.ventanas').find('.panel-body').attr('id','tmpscreenshot');
            if($('#tmpscreenshot').length>0){
                _sysshareScreen=true;
                connection.sharePartOfScreen(document.getElementById('tmpscreenshot'));
            }
        }else{
            iobj.removeClass('fa-share-alt').addClass('fa-share');
            connection.stopPartOfScreenSharing();
        }

    })


    function canceltransmision(media){
        Object.keys(connection.streamEvents).forEach(function(streamid){
            var streamEvent = connection.streamEvents[streamid];
            if(streamEvent.type=="local"){
                if(media=='video'&&streamEvent.stream.isVideo===1){
                    var videoTrack = streamEvent.stream.getVideoTracks()[0];
                    streamEvent.stream.removeTrack(videoTrack);                
                    $('.btnsharecamera i').removeClass('active bounceIn colorgreen');
                }
                else if(media=='screen'&&streamEvent.stream.isScreen==true){
                    var track = streamEvent.stream.getVideoTracks()[0];
                    streamEvent.stream.removeTrack(track);
                    if(track.stop) {
                        track.stop();
                        $('.btnsharedesktop i').removeClass('bounceIn colorgreen');
                    }                 
                }
            }
        });    
    }

    $('.btnsharecamera').click(function(ev){
        ev.preventDefault();
        var data={accion:'shichcamara',active:false};
        if (_detectRTC.hasWebcam == true){
            var btncamera=$('#menupersonalizado .menutools a.btnsharecamera');
            if(!btncamera.hasClass('active')){ // enable camera 
                $('#vwebcam').fadeIn(500);
                if(camarasonline[connection.userid]==undefined){
                    connection.session.video=true;
                    connection.mediaConstraints.video = true;
                    connection.addStream({video: true,audio:false});
                }
                data.active=true;
                btncamera.addClass('active');
                $('.btnsharecamera i').addClass('bounceIn colorgreen');             
                shichcamara(connection.userid,data);
                connection.send(data);
            }else{
                btncamera.removeClass('active');
                $('.btnsharecamera i').removeClass('bounceIn colorgreen');              
                var data={accion:'shichcamara',active:false};
                shichcamara(connection.userid,data);
                connection.send(data);
                canceltransmision('video')
                //$('#vwebcam').hide();
            }
        }else{
            $('#vwebcam').hide();
            $('.showwindow[data-ventana="vwebcam"]').addClass('active');
            mostrar_notificacion('Informacion','No se Detecto Camara conectada','danger');          
        }
    });


    $('.btnsharedesktop').click(function(ev){
        _detectRTC.load(function(){
            if(!_detectRTC.isScreenCapturingSupported){           
                mostrar_notificacion('Informacion','Su navegador no soporta captura de pantalla','danger');
                return false;
            }
            getChromeExtensionStatus(function(status){
                if(status==='installed-enabled'){
                    var btnsharepantalla=$('#menupersonalizado .menucompartir a.btnsharedesktop');              
                    btnsharepantalla.toggleClass('active');
                    if(btnsharepantalla.hasClass('active')){
                        connection.session.video=true;
                        connection.mediaConstraints.video = true;
                        connection.addStream({screen: true});                   
                    }else{
                        canceltransmision('screen');
                    }               
                }else{
                    mostrar_notificacion('Informacion','Extension no instalado en su navegador','danger');
                    return false; 
                }           
            });


        });
    });

    var elementToShare = document.getElementById('pantalla1');
    var canvas2d = document.createElement('canvas');
    var context = canvas2d.getContext('2d');
    canvas2d.width = elementToShare.clientWidth;
    canvas2d.height = elementToShare.clientHeight;
    canvas2d.style.top = 0;
    canvas2d.style.left = 0;
    canvas2d.style.zIndex = -1;
    var isRecordingStarted = false;
    var isStoppedRecording = false;
    var recorder = new RecordRTC(canvas2d, { type: 'canvas'});
    (document.body || document.documentElement).appendChild(canvas2d);

    function iniciargrabando(){
            if(!isRecordingStarted) {
                return setTimeout(iniciargrabando, 500);
            }
            html2canvas(elementToShare, {
                grabMouse: true,
                onrendered: function(canvas) {
                    context.clearRect(0, 0, canvas2d.width, canvas2d.height);
                    context.drawImage(canvas, 0, 0, canvas2d.width, canvas2d.height);
                    if(isStoppedRecording) {
                        return;
                    }
                    setTimeout(iniciargrabando, 1);
                }
            });
    }

   /* var videoElement = document.querySelector('video');
        function playVideo(callback) {
            function successCallback(stream) {
                videoElement.onloadedmetadata = function() {
                    callback();
                };
                videoElement.src = URL.createObjectURL(stream);
                videoElement.play();
            }
            function errorCallback(error) {
                console.error('get-user-media error', error);
                callback();
            }
            var _mediaConstraints = { video: true,audio:true };
            navigator.mediaDevices.getUserMedia(_mediaConstraints).then(successCallback).catch(errorCallback);
    }*/

    $('.btnsharegrabar').click(function(ev){
        var btn=$(this);
        var icon=$('i',btn);
        icon.toggleClass('active');
        if(icon.hasClass('active')){
            icon.addClass('zoomIn colored');            
            isStoppedRecording = false;
            isRecordingStarted = true;   
            recorder.startRecording();
            iniciargrabando();       
            //playVideo(function(){ recorder.startRecording(); });           
        }else{            
            icon.removeClass('zoomIn colored');
            isStoppedRecording = true;
            recorder.stopRecording(function(){
                var blob = recorder.getBlob();
                var link = document.createElement("a");
                var objurl = URL.createObjectURL(blob);
                link.target = '_blank';
                link.download = "videoclase.webm";
                link.href = objurl;
                link.click();
            });
        }
    });


    $('.btnsharepdfinboard').click(function(ev){
        var file=document.createElement('input');
        file.setAttribute("type", "file");
        file.setAttribute("accept", "application/pdf");
        file.click();
        $(file).change(function(){       
            if(['application/pdf'].indexOf($(this).get(0).files[0].type) == -1) {
                alert('Error : Not a PDF');
                return;
            }
            showPDF(window.URL.createObjectURL($(this).get(0).files[0]));
            $('#vpizarra').show();
            $('.showwindow[data-ventana="vpizarra"]').addClass('active');
            $('.btninpdf').show();
        });
    });

    $("#pdf-prev").on('click', function() {
        if(__PDFCURRENT_PAGE != 1)  showPage(--__PDFCURRENT_PAGE);
    });

    // Next page of the PDF
    $("#pdf-next").on('click', function() {
        if(__PDFCURRENT_PAGE != __PDFTOTAL_PAGES) showPage(++__PDFCURRENT_PAGE);
    });

    $('.btnsharevideo').click(function(){
        var file=document.createElement('input');
        file.setAttribute("type", "file");
        file.setAttribute("accept", "video/*");
         file.click();    
        $(file).change(function(){        
            _newfile=$(this).get(0).files[0];
            var formData = new FormData();
            formData.append("aula",'prueba');
            formData.append("filearchivo",_newfile);
            $.ajax({
              url: _sysUrlBase_ +'/aulavirtual/cargarmedia',
              type: "POST",
              data:  formData,
              contentType: false,
              processData: false,
              dataType :'json',
              cache: false,
              processData:false,
              xhr:function(){
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete = Math.floor((evt.loaded*100) / evt.total);            
                    $('#divprecargafile .progress-bar').width(percentComplete+'%');
                    $('#divprecargafile .progress-bar span').text(percentComplete+'%');
                  }
                }, false);
                xhr.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
                  }
                }, false);
                return xhr;
              },
              beforeSend: function(XMLHttpRequest){
                div=$('#divprecargafile');
                $('#divprecargafile').removeClass('progress-bar-danger progress-bar-success progress-bar-striped progress-bar-animated').addClass('progress-bar-striped progress-bar-animated');        
                $('#divprecargafile .progress-bar').width('0%');
                $('#divprecargafile .progress-bar span').text('0%'); 
                $('#divprecargafile').show('fast'); 
              },      
              success: function(data)
              { 
                if(data.code==='ok'){
                  $('#divprecargafile .progress-bar').width('100%');
                  $('#divprecargafile .progress-bar').html('Complete <span>100%</span>');
                  $('#divprecargafile').addClass('progress-bar-success').removeClass('progress-bar-animated');           
                  $('#divprecargafile').hide('fast');          
                  var midata={
                    accion:'addsharevideo',
                    namefile:data.namefile,
                    extension:data.extension
                  }
                  var userremote=connection.userid==roomid?connection.extra.username:connection.userid;
                  addsharevideo(userremote,midata);
                  connection.send(midata);
                }else{
                    $('#divprecargafile').addClass('progress-bar-warning progress-bar-animated');           
                    return false;
                }
              },
              error: function(e) 
              {
                  $('#divprecargafile').addClass('progress-bar-danger progress-bar-animated');
                  $('#divprecargafile .progress-bar').html('Error <span>-1%</span>');           
                  return false;
              },
              complete: function(xhr){
                 $('#divprecargafile .progress-bar').html('Complete <span>100%</span>'); 
                 $('#divprecargafile').addClass('progress-bar-success progress-bar-animated').hide('slow');
              }
            });
        });

    })


    $('.btnsharefile').click(function(){
        var file=document.createElement('input');
        file.setAttribute("type", "file");
        //file.setAttribute("accept", "image/*");
        file.click();    
        $(file).change(function(){        
            _newfile=$(this).get(0).files[0];
            var formData = new FormData();
            formData.append("aula",'prueba');
            formData.append("filearchivo",_newfile);
            $.ajax({
              url: _sysUrlBase_ +'/aulavirtual/cargarmedia',
              type: "POST",
              data:  formData,
              contentType: false,
              processData: false,
              dataType :'json',
              cache: false,
              processData:false,
              xhr:function(){
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete = Math.floor((evt.loaded*100) / evt.total);            
                    $('#divprecargafile .progress-bar').width(percentComplete+'%');
                    $('#divprecargafile .progress-bar span').text(percentComplete+'%');
                  }
                }, false);
                xhr.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
                  }
                }, false);
                return xhr;
              },
              beforeSend: function(XMLHttpRequest){
                div=$('#divprecargafile');
                $('#divprecargafile').removeClass('progress-bar-danger progress-bar-success progress-bar-striped progress-bar-animated').addClass('progress-bar-striped progress-bar-animated');        
                $('#divprecargafile .progress-bar').width('0%');
                $('#divprecargafile .progress-bar span').text('0%'); 
                $('#divprecargafile').show('fast'); 
              },      
              success: function(data)
              { 
                if(data.code==='ok'){
                  $('#divprecargafile .progress-bar').width('100%');
                  $('#divprecargafile .progress-bar').html('Complete <span>100%</span>');
                  $('#divprecargafile').addClass('progress-bar-success').removeClass('progress-bar-animated');           
                  $('#divprecargafile').hide('fast');          
                  var midata={
                    accion:'addsharefile',
                    namefile:data.namefile,
                    extension:data.extension
                  }
                  var userremote=connection.userid==roomid?connection.extra.username:connection.userid;
                  addsharefile(userremote,midata);
                  connection.send(midata);
                  $('#vfiles').show('fast');
                }else{
                    $('#divprecargafile').addClass('progress-bar-warning progress-bar-animated');           
                    return false;
                }
              },
              error: function(e) 
              {
                  $('#divprecargafile').addClass('progress-bar-danger progress-bar-animated');
                  $('#divprecargafile .progress-bar').html('Error <span>-1%</span>');           
                  return false;
              },
              complete: function(xhr){
                 $('#divprecargafile .progress-bar').html('Complete <span>100%</span>'); 
                 $('#divprecargafile').addClass('progress-bar-success progress-bar-animated').hide('slow');
              }
            });
        });
    });

$('#vchat #txtnewmensaje').keyup(function(e){
    var txt=$(this).val();
    if (e.which!=13) return;
    if (!txt.length) return;
    var data={
        accion:'addmensajechat',
        texto:txt,
        para:$('#vchat #userchatsms').val()
    }
    $(this).val('');
    addmensajechat(data,'si');
    connection.send(data);
});


$('#vparticipantes').on('click','.userstatus',function(){
    var li=$(this).closest('li.dropdown');
    var iduser=li.attr('id');
    var mode=$(this).attr('data-mode'); 
    if(iduser==roomid&&mode!='M') return;
    //$(this).closest('ul').find('.userstatus').removeClass('active');      
    var _clasi=$('i',this).attr('class');
    //li.find('.miestatus i.fa').removeAttr('class').addClass(_clasi);
    var data={accion:'cambiartypeuser', mode:mode,para:iduser,cls:_clasi}
    cambiartypeuser(data);
    connection.send(data);
}).on('click','.btnmicro',function(ev){
    ev.preventDefault();
    if(typeuser=='U') return false;
    var micro=$(this);
    var userremote=micro.closest('li.dropdown').attr('id');
    var data={accion:'cambiarmicrouser',microactive:false,userremote:userremote};               
    micro.toggleClass('active');
    if(micro.hasClass('active')) data.microactive=true;
    cambiarmicrouser(userremote,data);
    connection.send(data);
}).on('click','.btnmicroall',function(ev){
    ev.preventDefault();
    if(typeuser=='U') return false;
    var micro=$(this);
    //micro.toggleClass('active');
    var data={accion:'cambiarmicroalluser',microactive:false};  
    if(micro.hasClass('active')) {
        data.microactive=true;
        $('i',this).removeClass('fa-microphone-slash').addClass('fa-microphone');
    }else $('i',this).removeClass('fa-microphone').addClass('fa-microphone-slash');
    cambiarmicroalluser(connection.userid,data);
    connection.send(data);
});

$('#vencuesta').on('click','.btn_encuesta_enviar',function(){      
        if($(this).hasClass('disabled'))return false;
        $(this).addClass('disabled');
        var preg=$('#vencuesta #pregunta').val();
        if(preg=='') return false;
        var alt={};
        $('#vencuesta table tr.si input').each(function(){
            var alt_=$(this).val();
            if(alt_!=''){
                alt[alt_]=0;
            }
        });
        var idencuesta=userid+'_'+Date.now();
        sysencuestas[idencuesta]={accion:'tomarencuesta',idencuesta:idencuesta,pregunta:preg,alternativas:alt,idsala:1};
        connection.send(sysencuestas[idencuesta]);
        var addencuesta='<div class="encuesta" data-id="'+idencuesta+'">'+preg+'<br><a href="#" class="btnverresultadoencuesta">ver respuestas <i class="fa fa-eye"></i></a>';
            addencuesta+=' <a href="#" class="btneliminarencuesta">Eliminar <i class="fa fa-trash"></i></a></div><br>';//  <a href="#" class="">Guardar <i class="fa fa-save"></i></a>'
        $('#vencuesta .resultadoencuesta').append(addencuesta);
        $(this).removeClass('disabled');
}).on('click','.btnverresultadoencuesta',function(){
    var encuesta=$(this).closest('.encuesta').attr('data-id');
    console.log(encuesta);
    console.log(sysencuestas);
    if(sysencuestas[encuesta])
        var resencuesta=sysencuestas[encuesta];
        if(resencuesta){
            var alternativas=resencuesta.alternativas; 
            var modal='';
            if($('#modaltmp123').length) modal=$('#modaltmp123');
            else modal=$('#modalquestion').clone();
            modal.find('.modal-title').text('Grafico :')           
            modal.attr('id','modaltmp123');
            modal.find('.modal-body').html('<div><strong>'+resencuesta.pregunta+'</strong></div><div class="graficodemela_'+encuesta+'" ></div>');
            modal.find('.modal-footer').html('<a href="#" class="btncerrarmodal100">Cerrar</a>');
            modal.modal('show'); 
            modal.on('shown.bs.modal',function(){
                var series_=[];
                var labels_=[];
                $.each(alternativas,function(i,v){
                    labels_.push(i+' '+v);
                    series_.push(v);
                });
                modal.find('.modal-body').removeAttr('style');
                var data={ labels: labels_, series: series_};
                var opciones={ fullWidth: true,chartPadding: 2, showLabel: true, labelOffset: 10, startAngle: 0, };                
                var yourchart = new Chartist.Pie('.graficodemela_'+encuesta,data,opciones); 
                yourchart.update();  
            });
        }

}).on('click','.btneliminarencuesta',function(){
    $(this).closest('.encuesta').remove();
});
$('#pantalla1').on('click','#showiconos i.em',function(){    
    var data={};
    data.icon=$(this).attr('class');
    data.accion='mostraricono';
    data.titulo=$(this).attr('title');
    data.sonido=$(this).attr('data-audio');
    mostraricono(connection.userid,data)    
    connection.send(data);
});

$('body').on('click','a.btnresponder',function(ev){
    ev.preventDefault();
    var modal=$(this).closest('.modal');
    var encuesta={};
        encuesta.accion='responderencuesta';
        encuesta.id=modal.attr('id');
        encuesta.respuesta=$(this).attr('data-value');
        modal.modal('hide');
        connection.send(encuesta);

}).on('click','a.btncerrarmodal100',function(ev){
    var modal=$(this).closest('.modal');
    modal.modal('hide');
});

window.addEventListener('unload', function () {
    connection.close();
    connection.leave();
}, false);

function reCheckRoomPresence(){
    connection.userid=userid;
    connection.sessionid=roomid;
    connection.session.video=true;
    connection.mediaConstraints.video = false;
    //connection.session.video=true;
    //connection.mediaConstraints.video = false;
    connection.checkPresence(roomid, function(isRoomExists){
        if(isRoomExists){               
            connection.join(roomid,function(){
                var jsonuser={
                    userid:connection.userid,
                    username:connection.userid,
                    userfullname:$('#infousersala ._username').text(),
                    useremail:$('#infousersala ._useremail').text(),
                    typeuser:typeuser
                }                   
                addparticipantes(jsonuser,true);
            }); 
            return;            
        }else{              
            connection.open(roomid,function(){
                var jsonuser={
                    userid:connection.userid,
                    username:userid,
                    userfullname:$('#infousersala ._username').text(),
                    useremail:$('#infousersala ._useremail').text(),
                    typeuser:typeuser
                }                   
                addparticipantes(jsonuser,true);
                //mostrar_notificacion("Welcomme", "vienvenido", "info");
            }); 
            return;             
        }            
    });
}
reCheckRoomPresence();
});
function updatecolor(picker){
    id=picker.styleElement.id;  
    color = picker.toHEXString();
    var a=$('#'+id).closest('a');
    a.attr('data-color',color);
    a.find('i').css('color',color);
    if(a.hasClass('toolcolorfondocanvas')){
        colorcanvas=color;
        $('.herramienta-dibujo .toolcolorfondocanvas').trigger('click');       
    }else if(a.hasClass('toolcolorline')){
        colorborde=color; 
        $('.herramienta-dibujo .toolcolorline').trigger('click'); 
    }else if(a.hasClass('toolcolorfill')){
        colorfondo=color;
        $('.herramienta-dibujo .toolcolorfill').trigger('click'); 
    }   
}