/*************************************   General   ******************************************************/
(function() {
    var params = {},
        r = /([^&=]+)=?([^&]*)/g;
    function d(s) {
        return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1)))
        params[d(match[1])] = d(match[2]);
    window.params = params;
})();

$(document).ready(function(){
function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}

function geturl() {
    var loc = window.location;
    var pathname = window.location.pathname;
    return loc+pathname;
}
function medidas(id){
    return {w:document.getElementById(id).offsetWidth,h:document.getElementById(id).offsetHeight}
}
/****************************************     ***********************************************/

var infokey='chingo-sala-emit'; 
var connection = new RTCMultiConnection();
    var _detectRTC=connection.DetectRTC;
    connection.session = {audio: true, video: false, screen:false, data: true }; 
    connection.socketURL ='https://rtcmulticonnection.herokuapp.com:443/'; //'https://socket.skyrooms.io:443/';
    //connection.setCustomSocketHandler(SSEConnection);
    connection.socketMessageEvent = infokey;
    connection.direction = 'many-to-many';
    connection.sdpConstraints.mandatory = { OfferToReceiveAudio: true,OfferToReceiveVideo: true};
    connection.autoCloseEntireSession = false;    
    connection.maxParticipantsAllowed = 400;
    connection.leaveOnPageUnload = false;
    connection.enableLogs = false;
    //connection._videosScreen = document.getElementById('videos-screen');
    connection._medias = document.getElementById('medias-container');
    connection._cameraemit = document.getElementById('container-webcams');

    var haymicro=false;    
    var haywebcam=false;
    var hayScreen=false;
    var urlBase=getAbsolutePath();
   
    var roomid='lasa';
    var userid='abel'+connection.token();
    var tmptypeusertxt=['U','M','P'];  // M=moderador , P=participante , U= usuario;
    var tmpidtype=Math.round(Math.random() * (0 + 2) + 0);
    var typeuser=tmptypeusertxt[tmpidtype]; 
    var emiteruser=false;

    /******************************************** funciones ********************************/
    
    var usersparticipante=$('#vparticipantes ul#usersparticipante');
    var userchatsms=$('#vchat select#userchatsms');

    function addparticipantes(jsonuser,soyyo){
        var userclone=$('#vparticipantes div#userclone').clone(true);
        userclone.find('#idtmp').attr('id',jsonuser.userid);        
        userclone.find('a.userstatus.mode'+jsonuser.typeuser).addClass('active');
        userclone.find('span.nombre').text(jsonuser.username);      
        var iclass=userclone.find('a.userstatus.active i').attr('class');
        userclone.find('a.miestatus i').removeAttr('class').addClass(iclass); 
        if(soyyo==true){            
            userclone.find('span.userchataccion').html('Yo');
            usersparticipante.prepend(userclone.html());
            if(typeuser=='M'){
                emiteruser=true;
            }
        }else{
            userclone.find('.lilinea').remove();
            usersparticipante.find('li.linea').before(userclone.html());
            userchatsms.append('<option value="'+jsonuser.userid+'"><span class="'+iclass+'"></span> '+jsonuser.username+'</option>');
        }
        if(typeuser!='M'){
            //usersparticipante.find('span.userchataccion .btn-group').hide(0);
            usersparticipante.find('a.miestatus').addClass('disabled');
            usersparticipante.find('a.miestatus span').hide(0);
        } 
        usersparticipante.find('#totaluser b').text(usersparticipante.find('li.liuser').length);
        connection.extra={username:userid,typeuser:typeuser};
        connection.updateExtraData();
    }

    function removeparticipante(userid){
        usersparticipante.find('li#'+userid).remove();
        userchatsms.find('option[value="'+userid+'"]').remove();
        usersparticipante.find('#totaluser b').text(usersparticipante.find('li.liuser').length);
    }

    function addmensajechat(data,user){
        var para=data.para;
        var currentTime = new Date();
        var time=currentTime.getHours()+':'+currentTime.getMinutes()+':'+currentTime.getSeconds();
        var txt='';
        if(user=='si'){
            txt='<div class="mensaje" id="'+connection.userid+'"><div class="pull-right text-left globoright"><b>Yo :</b><br>'+data.texto+'<br><div class="chattime">'+time+'</div></div><div class="clearfix"></div></div>';
        }else{
            _user=user.extra;
            if(para!='alluser'&&para!=connection.userid) return;
            var isprivate=para==connection.userid?'mensaje isprivate':'mensaje';
            txt='<div class="'+isprivate+'"  id="'+user.userid+'"><div class="pull-left text-left globoleft"><b>'+_user.username+' :</b><br>'+data.texto+'<br><div class="chattime">'+time+'</div></div><div class="clearfix"></div></div>';
        }
        var scrollHeight=$('.chatAll .mensajes')[0].scrollHeight;
        $('.chatAll .mensajes').append(txt).animate({scrollTop:scrollHeight}, 500);
    }

    function cambiartypeuser(data){
        var para=data.para;
        var mode=data.mode;
        var iclass=data.cls;
        if(para==roomid) return;
        if(para==connection.userid){
            if(mode!='M'){
                usersparticipante.find('span.userchataccion .btn-group').hide(0);
                usersparticipante.find('a.miestatus').addClass('disabled');
                usersparticipante.find('a.miestatus span').hide(0); 
                if(mode=='P')emiteruser=true;
                else emiteruser=false;
            }else{
                usersparticipante.find('span.userchataccion .btn-group').show(0);
                usersparticipante.find('a.miestatus').removeClass('disabled');
                usersparticipante.find('a.miestatus span').show(0);
                emiteruser=true;
            }           
            typeuser=data.mode;
        }else{
            if(mode!='U') emiteruser=false;
        }   
        var userachange=usersparticipante.find('li#'+para);
        userachange.find('.userstatus').removeClass('active');
        userachange.find('.miestatus i.fa').removeAttr('class').addClass(iclass);
        userachange.find('.userstatus.mode'+mode).addClass('active');       
    }

    function cambiarmicrouser(userremote,data){
        var para=data.para||'allusers';
        if(para=='allusers'){
            var audiouser=$('audio#'+userremote);
            if(data.microactive){
                if(audiouser) {
                    audiouser.prop("volume", 0.8);
                    audiouser.prop("muted",false);
                    audiouser.trigger('play');
                }
                $('#vparticipantes #usersparticipante li#'+userremote+' .btnmicro').addClass('active');
                $('#vparticipantes #usersparticipante li#'+userremote+' .btnmicro i').removeClass('fa-microphone-slash').addClass('fa-microphone bounceIn colorgreen');
                if(userremote==connection.userid){
                    $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone-slash').addClass('fa-microphone bounceIn colorgreen');
                    $('#menupersonalizado .menutools a.btnsharemicro').addClass('active');
                }
            }else{
                if(audiouser) audiouser.trigger('pause');
                $('#vparticipantes #usersparticipante li#'+userremote+' .btnmicro').removeClass('active');
                $('#vparticipantes #usersparticipante li#'+userremote+' .btnmicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
                if(userremote==connection.userid){
                    $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
                    $('#menupersonalizado .menutools a.btnsharemicro').removeClass('active');
                }
            }
        }
    }

    function cambiarmicroalluser(userremote,data){
        var obj=$('#vparticipantes #usersparticipante li.liuser');
        $.each(obj,function(i,v){           
            var id=$(v).attr('id');     
            if(id!=userremote){
                cambiarmicrouser(id,data)
            }           
        });
    }

    function alzarlamano(userremote,data){
        if(data.alzarlamano){
            if(typeuser!='U'){
                var btnmano=$('#vparticipantes #usersparticipante li#'+userremote+' .btnmano');
                btnmano.show();
                $('i',btnmano).removeClass('fa-hand-stop-o').addClass('fa-thumbs-up bounceIn colorgreen');              
            }
        }
    }
    function cancelaralzarmano(data){
        var btnmano=$('#vparticipantes #usersparticipante li#'+data.userremote+' .btnmano');
            btnmano.hide();     
        if(data.userremote==connection.userid){
            $('i',btnmano).removeClass('fa-thumbs-up bounceIn colorgreen').addClass('fa-hand-stop-o');
            $('#menupersonalizado a.btnalzarmano i').removeClass('fa-thumbs-up bounceIn colorgreen').addClass('fa-hand-stop-o');;
            $('#menupersonalizado .menutools a.btnalzarmano').removeClass('active');
        }
    }

    var camarasonline={}
    function shichcamara(userremote,data){      
        var videocam=  $('video#camera_'+userremote);
        if(data!=undefined){
            console.log(data);
            if(data.active==false){
                console.log(videocam);
                if(videocam.length>0){
                    camarasonline[userremote]=videocam;
                    videocam.remove();
                }
            }else{

                if(camarasonline[userremote]!=undefined){   
                    $('#container-webcams').append(camarasonline[userremote]);
                    videocam= $('video#camera_'+userremote);
                    setTimeout(function(){ videocam.trigger('play'); }, 3000);
                    var videos=$('#container-webcams').find('video');           
                    var total=100/videos.length;
                    if(videos.length>1&&total>19){
                        videos.css('width',total+'%');
                    }
                }
            }
        }
    }

    /******************************************** eventos sockets ********************************/
    connection.onopen = function(event){
        var extra=event.extra;
        var jsonuser={
            userid:event.userid,
            username:extra.username,
            typeuser:extra.typeuser
        }
        addparticipantes(jsonuser,false);
    }
    connection.onclose = function(event){
        var remoteuserid=event.userid;
        connection.disconnectWith(remoteuserid);
        removeparticipante(remoteuserid);
        $('audio#'+remoteuserid).trigger('pause').remove();
        $('video#camera_'+remoteuserid).trigger('pause').remove();
    }

    connection.onmessage=function(event){       
        var data=event.data||'';
        var accion=data!=''?data.accion:'';
        var userremote=data.userremote||event.userid;       
        if(accion=='addmensajechat') addmensajechat(data,event);
        else if(accion=='cambiartypeuser') cambiartypeuser(data);
        else if(accion=='cambiarmicrouser') cambiarmicrouser(userremote,data);
        else if(accion=='cambiarmicroalluser') cambiarmicroalluser(event.userid,data);
        else if(accion=='alzarlamano') alzarlamano(userremote,data);
        else if(accion=='cancelaralzarmano') cancelaralzarmano(data);
        else if(accion=='shichcamara') shichcamara(event.userid,data);      
    }

    connection.onstream = function(event){
        var userremote=event.userid;        
        var _stream=event.stream;
        var tipouser=event.type;
        var media=event.mediaElement;    
        console.log(_stream);   
        if (_stream.isScreen){
            if(tipouser=='local') {
                $('.btnsharedesktop i').addClass('active bounceIn colorgreen');
            }

            if(tipouser=='remoto'){
                connection._medias.appendChild(media);
                setTimeout(function(){ media.pause(); }, 5000);
            }

        }else if(_stream.isVideo){
            media.id='camera_'+userremote; 
            media.muted=true; 
            media.controls=false;
            connection._cameraemit.appendChild(media);
            camarasonline[userremote]=media;
            setTimeout(function() { media.play(); }, 3000);
            /******* adaptando video de camaras ***/
            var video=$('#container-webcams').find('video');
            var total=100/video.length;
            if(video.length>1&&total>19){
                video.css('width',total+'%');
            }
        }else if(_stream.isAudio){ 
                media.id=userremote;
                media.controls=true;
                media.muted=true        
            if(tipouser=='local'){
                microaudio.tengo='si';
                microaudio.media=media;
                return;
            }                   
            connection._medias.appendChild(media);
            setTimeout(function() { media.pause(); }, 5000);
        }
    }

    connection.onstreamended = function(event){
        if(event.stream.isScreen)
            if(event.type=="local"){                
                $('#menupersonalizado .menucompartir a.btnsharedesktop').removeClass('active');
                $('.btnsharedesktop i').removeClass('bounceIn colorgreen');
            }else{
                $('video#isScreen_'+event.userid).remove();             
            }
    }

    connection.onPeerStateChanged = function(state){
        if (state.iceConnectionState.search(/closed|failed/gi) !== -1) {
            Object.keys(connection.streamEvents).forEach(function(streamid) {
                var streamEvent = connection.streamEvents[streamid];
                if (streamEvent.userid === state.userid) {
                    connection.onstreamended(streamEvent);
                }
            });
        }
    }

    connection.getScreenConstraints = function(callback){
        getScreenConstraints(function(error, screen_constraints) {
            if (!error){
                screen_constraints = connection.modifyScreenConstraints(screen_constraints);
                callback(error, screen_constraints);
                return;
            }
            throw error;
        });
    }
    /************************************** Eventos Botones ********************************************/
    var microaudio={tengo:'no',url:'',estado:'pausado'};
    var _detectRTC=connection.DetectRTC;

$('.btnsharemicro').click(function(ev){
    _detectRTC.load(function(){
        if (_detectRTC.hasMicrophone === true) { // enable microphone 
            var btnsharemicro=$('#menupersonalizado .menutools a.btnsharemicro');
            btnsharemicro.toggleClass('active');
            var data={accion:'cambiarmicrouser',microactive:false};
            if(btnsharemicro.hasClass('active')) data.microactive=true;
            cambiarmicrouser(connection.userid,data);
            connection.send(data);
        }else{
            mostrar_notificacion('Informacion','No se Detecto Microfono, conecte y refresque la pagina','danger');
        }
    });
});

$('.btnalzarmano').click(function(ev){
    var btnalzarmano=$('#menupersonalizado .menutools a.btnalzarmano');
    var obj=$(this);
    var i=obj.find('i');
    btnalzarmano.toggleClass('active');
    if(btnalzarmano.hasClass('active')){
        if(i.length){
            i.removeClass('fa-hand-stop-o').addClass('fa-hand-pointer-o bounceIn colorgreen');
        }else{
            $('#menupersonalizado a.btnalzarmano i').removeClass('fa-hand-stop-o').addClass('fa-hand-pointer-o bounceIn colorgreen');
        }
        var data={accion:'alzarlamano',alzarlamano:true}
        connection.send(data);
    }else{
        var data={accion:'cancelaralzarmano',userremote:connection.userid}
        cancelaralzarmano({userremote:connection.userid});
        connection.send(data);
    }
});

$('#vparticipantes').on('click','#usersparticipante .btnmano',function(){
    $(this).hide();
    var userremote=$(this).closest('li').attr('id');
    var data={accion:'cancelaralzarmano',userremote:userremote}
    connection.send(data)
});


function canceltransmision(media){
    Object.keys(connection.streamEvents).forEach(function(streamid){
        var streamEvent = connection.streamEvents[streamid];
        if(streamEvent.type=="local"){
            if(media=='video'&&streamEvent.stream.isVideo===1){
                var videoTrack = streamEvent.stream.getVideoTracks()[0];
                streamEvent.stream.removeTrack(videoTrack);
                $('.btnsharecamera i').removeClass('active bounceIn colorgreen');
            }
            else if(media=='screen'&&streamEvent.stream.isScreen==true){
                var track = streamEvent.stream.getVideoTracks()[0];
                streamEvent.stream.removeTrack(track);
                if(track.stop) {
                    track.stop();
                    $('.btnsharedesktop i').removeClass('bounceIn colorgreen');
                }                 
            }
        }
    });    
}



$('.btnsharecamera').click(function(ev){
    ev.preventDefault();
    var data={accion:'shichcamara',active:false};
    if (_detectRTC.hasWebcam == true){
        var btncamera=$('#menupersonalizado .menutools a.btnsharecamera');
        if(!btncamera.hasClass('active')){ // enable camera 
            $('#vwebcam').fadeIn(500);
            if(camarasonline[connection.userid]==undefined){
                connection.session.video=true;
                connection.mediaConstraints.video = true;
                connection.addStream({video: true,audio:false});
            }
            data.active=true;
            btncamera.addClass('active');
            $('.btnsharecamera i').addClass('bounceIn colorgreen');             
            shichcamara(connection.userid,data);
            connection.send(data);
        }else{
            btncamera.removeClass('active');
            $('.btnsharecamera i').removeClass('bounceIn colorgreen');              
            var data={accion:'shichcamara',active:false};
            shichcamara(connection.userid,data);
            connection.send(data);
            //$('#vwebcam').hide();
        }
    }else{
        $('#vwebcam').hide();
        mostrar_notificacion('Informacion','No se Detecto Camara conectada','danger');          
    }
});


$('.btnsharedesktop').click(function(ev){
    _detectRTC.load(function(){
        if(!_detectRTC.isScreenCapturingSupported){           
            mostrar_notificacion('Informacion','Su navegador no soporta captura de pantalla','danger');
            return false;
        }
        getChromeExtensionStatus(function(status){
            if(status==='installed-enabled'){
                var btnsharepantalla=$('#menupersonalizado .menucompartir a.btnsharedesktop');              
                btnsharepantalla.toggleClass('active');
                if(btnsharepantalla.hasClass('active')){
                    connection.session.video=true;
                    connection.mediaConstraints.video = true;
                    connection.addStream({screen: true});                   
                }else{
                    canceltransmision('screen');
                }               
            }else{
                mostrar_notificacion('Informacion','Extension no instalado en su navegador','danger');
                return false; 
            }           
        });


    });
});

$('.btnsharegrabar').click(function(ev){
    var btn=$(this);
    var icon=$('i',btn);
    icon.toggleClass('active');
    if(icon.hasClass('active')){
        icon.addClass('zoomIn colored');
    }else{
        icon.removeClass('zoomIn colored');
    }
})


/*function canceltransmisionscreen(media){
    Object.keys(connection.streamEvents).forEach(function(streamid){
        var streamEvent = connection.streamEvents[streamid];
        if(streamEvent.type=="local"){
            if(media=='screen'&&streamEvent.stream.isScreen==true){
                streamEvent.stream.getVideoTracks().forEach(function(track){
                    streamEvent.stream.removeTrack(track);
                    if(track.stop) {
                        track.stop();
                        $('.btnsharedesktop i').removeClass('active bounceIn colorgreen');
                    }
                }); 
            }
        }
    });    
}*/



$('.btnshare').click(function(){
    connection.send('hola a todos'); 
});

$('#vchat #txtnewmensaje').keyup(function(e){
    var txt=$(this).val();
    if (e.which!=13) return;
    if (!txt.length) return;
    var data={
        accion:'addmensajechat',
        texto:txt,
        para:$('#vchat #userchatsms').val()
    }
    $(this).val('');
    addmensajechat(data,'si');
    connection.send(data);
});


$('#vparticipantes').on('click','.userstatus',function(){
    var li=$(this).closest('li.dropdown');
    var iduser=li.attr('id');
    var mode=$(this).attr('data-mode'); 
    if(iduser==roomid&&mode!='M') return;
    //$(this).closest('ul').find('.userstatus').removeClass('active');      
    var _clasi=$('i',this).attr('class');
    //li.find('.miestatus i.fa').removeAttr('class').addClass(_clasi);
    var data={accion:'cambiartypeuser', mode:mode,para:iduser,cls:_clasi}
    cambiartypeuser(data);
    connection.send(data);
}).on('click','.btnmicro',function(ev){
    ev.preventDefault();
    if(typeuser=='U') return false;
    var micro=$(this);
    var userremote=micro.closest('li.dropdown').attr('id');
    var data={accion:'cambiarmicrouser',microactive:false,userremote:userremote};               
    micro.toggleClass('active');
    if(micro.hasClass('active')) data.microactive=true;
    cambiarmicrouser(userremote,data);
    connection.send(data);
}).on('click','.btnmicroall',function(ev){
    ev.preventDefault();
    if(typeuser=='U') return false;
    var micro=$(this);
    //micro.toggleClass('active');
    var data={accion:'cambiarmicroalluser',microactive:false};  
    if(micro.hasClass('active')) {
        data.microactive=true;
        $('i',this).removeClass('fa-microphone-slash').addClass('fa-microphone');
    }else $('i',this).removeClass('fa-microphone').addClass('fa-microphone-slash');
    cambiarmicroalluser(connection.userid,data);
    connection.send(data);
});

    window.addEventListener('unload', function () {
        connection.close();
        connection.leave();
    }, false);

    function reCheckRoomPresence(){
        connection.userid=userid;
        connection.sessionid=roomid;
        connection.session.video=true;
        connection.mediaConstraints.video = false;
        //connection.session.video=true;
        //connection.mediaConstraints.video = false;
        connection.checkPresence(roomid, function(isRoomExists){
            if(isRoomExists){               
                connection.join(roomid,function(){
                    var jsonuser={
                        userid:connection.userid,
                        username:connection.userid,
                        typeuser:typeuser
                    }                   
                    addparticipantes(jsonuser,true);
                }); 
                return;            
            }else{              
                connection.open(roomid,function(){
                    var jsonuser={
                        userid:connection.userid,
                        username:userid,
                        typeuser:typeuser
                    }                   
                    addparticipantes(jsonuser,true);
                    //mostrar_notificacion("Welcomme", "vienvenido", "info");
                }); 
                return;             
            }            
        });
    }
    reCheckRoomPresence();
 });