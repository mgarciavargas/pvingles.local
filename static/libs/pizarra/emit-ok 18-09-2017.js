/*************************************   General   ******************************************************/
(function() {
    var params = {},
        r = /([^&=]+)=?([^&]*)/g;
    function d(s) {
        return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1)))
        params[d(match[1])] = d(match[2]);
    window.params = params;
})();

/***********************************************************************************************/
function showPage(page_no) {
        __PDFPAGE_RENDERING_IN_PROGRESS = 1;
        __PDFCURRENT_PAGE = page_no;
        $("#micanvas").hide();
        $("#pdf-current-page").text(page_no);
        __PDF_DOC.getPage(page_no).then(function(page) {// As the canvas is of a fixed width we need to set the scale of the viewport accordingly
            var wcanvas=$('#micanvaspdf').outerWidth();
            var scale_required = __CANVAS.width / page.getViewport(1).width;        
            var viewport = page.getViewport(1.5);// Get viewport of the page at required scale  
            __CANVAS.width = viewport.width;
            __CANVAS.height = viewport.height;
            canvasfabric.setWidth(viewport.width);
            canvasfabric.setHeight(viewport.height);
            var renderContext = {
                canvasContext: __CANVAS_CTX,
                viewport: viewport
            };
            
            // Render the page contents in the canvas
            page.render(renderContext).then(function() {
                __PDFPAGE_RENDERING_IN_PROGRESS = 0;

                var myImage = __CANVAS.toDataURL("image/png");
                fabric.Image.fromURL(myImage, function(img) {
                    canvasfabric.backgroundImage = img;
                    canvasfabric.backgroundImage.width = __CANVAS.width;
                    canvasfabric.backgroundImage.height =  __CANVAS.height;
                    canvasfabric.renderAll();
                });
                $("#micanvas").show();
                $(".pdffileok").show(); 
            });
        });
}

function showPDF(pdf_url) {
    // Show the pdf loader
    $("#pdf-loader").show();
    PDFJS.getDocument({ url: pdf_url }).then(function(pdf_doc) {
        __PDF_DOC = pdf_doc;
        __PDFTOTAL_PAGES = __PDF_DOC.numPages;
        $("#pdf-total-pages").text(__PDFTOTAL_PAGES);
        showPage(1);
    }).catch(function(error) {            
    });
}


$(document).ready(function(){
function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}

function geturl() {
    var loc = window.location;
    var pathname = window.location.pathname;
    return loc+pathname;
}
function medidas(id){
    return {w:document.getElementById(id).offsetWidth,h:document.getElementById(id).offsetHeight}
}

/************************************************************************************************/
var infokey='chingo01';
var rtcconectado=false; 
var connection = new RTCMultiConnection();
 	var _detectRTC=connection.DetectRTC;
    //connection.dontCaptureUserMedia = true;
    connection.enableScalableBroadcast=true;
    connection.session = {audio:true, video: false, screen:false, data: true};
    connection.mediaConstraints = {video:false,audio:true};
    connection.sdpConstraints.mandatory = { OfferToReceiveAudio: true,OfferToReceiveVideo: true};
    //connection.setCustomSocketHandler(SSEConnection);
    connection.socketURL='https://abacoeducacion.org/';//'https://rtcmulticonnection.herokuapp.com:443/'; //'https://socket.skyrooms.io:443/';
    //connection.setCustomSocketHandler(SSEConnection);
    connection.socketMessageEvent = infokey;
    connection.socketCustomEvent = connection.channel;
    connection.direction = 'many-to-many';
    
    connection.autoCloseEntireSession = true;    
    connection.maxParticipantsAllowed = 4000;
    connection.leaveOnPageUnload = false;
    //connection.closeBeforeUnload = false;
    //window.onbeforeunlaod = function() {
      //  connection.close();
    //};


    connection.enableLogs = true;
    //connection._videosScreen = document.getElementById('videos-screen');
    connection._medias = document.getElementById('medias-container');
    connection._mediaaudios= document.getElementById('container-audios');
    connection._mediawebcams= document.getElementById('container-webcams');
    var haymicro=false;    
    var haywebcam=false;
    var hayScreen=false;
    var salirsala=false;
    var urlBase=getAbsolutePath();
   
    roomid=roomid||$('#infousersala ._sala').text();
    userid=userid||$('#infousersala ._user').text();
    var tmproomid=sessionStorage.getItem("roomid");
    if(roomid===tmproomid){
        permiso=JSON.parse(sessionStorage.permiso);
        tipouser=sessionStorage.getItem("tipouser");       
    }else{
        sessionStorage.clear();
    }
    var typeuser=$('#infousersala ._typeuser').text(); 
    var emiteruser=false;

/******************************************** funciones ****************************************/    
    var usersparticipante=$('#vparticipantes ul#usersparticipante');
    var userchatsms=$('#vchat select#userchatsms');
    var userspizarra=$('#vpizarra select#userspizarra');

    function addparticipantes(jsonuser,soyyo){
    	var userclone=$('#vparticipantes div#userclone').clone(true);
    	userclone.find('#idtmp').attr('id',jsonuser.userid);
    	userclone.find('a.userstatus.mode'+jsonuser.typeuser).addClass('active');
    	userclone.find('span.nombre').text(jsonuser.username);
    	var iclass=userclone.find('a.userstatus.active i').attr('class');
    	userclone.find('a.miestatus i').removeAttr('class').addClass(iclass);
    	if(soyyo==true){
    		userclone.find('span.userchataccion').html('Yo');
    		usersparticipante.prepend(userclone.html());
    		if(typeuser=='M'){
    			emiteruser=true;
    		}
            userspizarra.append('<option value="Yo">Yo</option>');
    	}else{
    		userclone.find('.lilinea').remove();
    		usersparticipante.find('li.linea').before(userclone.html());
    		userchatsms.append('<option value="'+jsonuser.userid+'"><span class="'+iclass+'"></span> '+jsonuser.username+'</option>');
            userspizarra.append('<option value="'+jsonuser.userid+'">'+jsonuser.username+'</option>');
    	}
		if(typeuser!='M'){    		
    		usersparticipante.find('a.miestatus').addClass('disabled');
    		usersparticipante.find('a.miestatus span').hide(0);
    	} 
    	usersparticipante.find('#totaluser b').text(usersparticipante.find('li.liuser').length);
    	connection.extra={username:userid,typeuser:typeuser};
    	connection.updateExtraData();
    }

    function removeparticipante(userid){
    	usersparticipante.find('li#'+userid).remove();
    	userchatsms.find('option[value="'+userid+'"]').remove();
        userspizarra.find('option[value="'+userid+'"]').remove();
    	usersparticipante.find('#totaluser b').text(usersparticipante.find('li.liuser').length);
    }

    function addmensajechat(data,user){
    	var para=data.para;
    	var currentTime = new Date();
    	var time=currentTime.getHours()+':'+currentTime.getMinutes()+':'+currentTime.getSeconds();
    	var txt='';
    	if(user=='si'){
    		txt='<div class="mensaje" id="'+connection.userid+'"><div class="pull-right text-left globoright"><b>Yo :</b><br>'+data.texto+'<br><div class="chattime">'+time+'</div></div><div class="clearfix"></div></div>';
    	}else{
    		_user=user.extra;
    		if(para!='alluser'&&para!=connection.userid) return;
    		var isprivate=para==connection.userid?'mensaje isprivate':'mensaje';
    		txt='<div class="'+isprivate+'"  id="'+user.userid+'"><div class="pull-left text-left globoleft"><b>'+_user.username+' :</b><br>'+data.texto+'<br><div class="chattime">'+time+'</div></div><div class="clearfix"></div></div>';
    	}
    	var scrollHeight=$('.chatAll .mensajes')[0].scrollHeight;
    	$('.chatAll .mensajes').append(txt).animate({scrollTop:scrollHeight}, 500);
    }

    function cambiartypeuser(data){
    	var para=data.para;
    	var mode=data.mode;
    	var iclass=data.cls;
    	if(para==roomid) return;
        if(para==connection.userid){
            tipouser=data.mode;
            resetprivilegios();
        }
    	var userachange=usersparticipante.find('li#'+para);
    	userachange.find('.userstatus').removeClass('active');
    	userachange.find('.miestatus i.fa').removeAttr('class').addClass(iclass);
    	userachange.find('.userstatus.mode'+mode).addClass('active');    	
    }

    function alzarlamano(userremote,data){
    	if(data.alzarlamano){
    		if(typeuser!='U'){
	    		var btnmano=$('#vparticipantes #usersparticipante li#'+userremote+' .btnmano');
	    		btnmano.show();
	    		$('i',btnmano).removeClass('fa-hand-stop-o').addClass('fa-thumbs-up bounceIn colorgreen');	    		
    		}
    	}
    }
    function cancelaralzarmano(data){
    	var btnmano=$('#vparticipantes #usersparticipante li#'+data.userremote+' .btnmano');
	    	btnmano.hide();	    
	    if(data.userremote==connection.userid){
	    	$('i',btnmano).removeClass('fa-thumbs-up bounceIn colorgreen').addClass('fa-hand-stop-o');
	    	$('#menupersonalizado a.btnalzarmano i').removeClass('fa-thumbs-up bounceIn colorgreen').addClass('fa-hand-stop-o');;
	    	$('#menupersonalizado .menutools a.btnalzarmano').removeClass('active');
	    }
    }

    var camarasonline={}
    
    
    function shichmicroprivado(userremote,data){
        var userde=userremote;
        var userpara=data.userremote;
        var audiouserde=$('audio#audio_'+userde);
        var audiouserpara=$('audio#audio_'+userpara);
        $('#vparticipantes #usersparticipante li .btnmicro.active').each(function(){
            $('i',this).removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
            var id=$(this).closest('li').attr('id');
            var audiotmp=$('audio#audio_'+id);
            if(audiotmp.length){
               audiotmp.prop('muted',true);
               audiotmp.trigger('pause');
            }
            $('#vparticipantes #usersparticipante li#'+id+' .btnmicroprivado').removeClass('active');
            $('#vparticipantes #usersparticipante li#'+id+' .btnmicroprivado i').addClass('fa-phone').removeClass('fa-volume-control-phone bounceIn colorgreen');id
        });      
        if(connection.userid==userpara){
            if(audiouserde.length && data.active==true){
                audiouserde.prop('muted',false); 
                audiouserde.trigger('play');                
                $('#vparticipantes #usersparticipante li#'+userde+' .btnmicroprivado').addClass('active');
                $('#vparticipantes #usersparticipante li#'+userde+' .btnmicroprivado i').removeClass('fa-phone').addClass('fa-volume-control-phone bounceIn colorgreen');
                $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
                $('#menupersonalizado .menutools a.btnsharemicro').removeClass('active');
            }else{
                audiouserde.prop('muted',true);
                audiouserde.trigger('pause');
                $('#vparticipantes #usersparticipante li#'+userde+' .btnmicroprivado').removeClass('active');
                $('#vparticipantes #usersparticipante li#'+userde+' .btnmicroprivado i').addClass('fa-phone').removeClass('fa-volume-control-phone bounceIn colorgreen');                
            }
        }else if(connection.userid==userde){
            if(audiouserpara.length && data.active==true){
                audiouserpara.prop('muted',false); 
                audiouserpara.trigger('play');
                $('#vparticipantes #usersparticipante li#'+userpara+' .btnmicroprivado').addClass('active');
                $('#vparticipantes #usersparticipante li#'+userpara+' .btnmicroprivado i').removeClass('fa-phone').addClass('fa-volume-control-phone bounceIn colorgreen');
                $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
                $('#menupersonalizado .menutools a.btnsharemicro').removeClass('active');
            }else{
                audiouserpara.prop('muted',true);
                audiouserpara.trigger('pause');                
                $('#vparticipantes #usersparticipante li#'+userpara+' .btnmicroprivado').removeClass('active');
                $('#vparticipantes #usersparticipante li#'+userpara+' .btnmicroprivado i').addClass('fa-phone').removeClass('fa-volume-control-phone bounceIn colorgreen');
            }
        }else{
            $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
            $('#menupersonalizado .menutools a.btnsharemicro').removeClass('active');                           
        }
    }

    /*******************************funciones de pdf ********************************************/
    
    var sysencuestas={};
    function tomarencuesta(encuesta){
        var modal=$('#modalquestion').clone(true);
        modal.attr('id',encuesta.idencuesta);
        modal.find('.aquipregunta').text(encuesta.pregunta);
        var aquialternativas=modal.find('.aquialternativas');
        var alt_=encuesta.alternativas
        aquialternativas.html('');
        if(alt_!=undefined)
        $.each(alt_,function(i,v){
            var bt='<a href="#" data-value="'+i+'" class="btn btn-default btnresponder">'+i+'</a>';
            aquialternativas.append(bt);
        })
        modal.modal({backdrop: 'static', keyboard: false});
    }

    function responderencuesta(userremote,data){
        var id=data.id;
        var res=data.respuesta;
        if(sysencuestas[id]!=undefined){
            sysencuestas[id]['alternativas'][res]=sysencuestas[id]['alternativas'][res]+1;            
        }        
    }

    function mostraricono(userremote,data){
        var audiosound=$('#audiosonidos');       
        var span='<span id="icon'+userremote+'"><i class="'+data.icon+'"></i></span>';
        var li=$('#vparticipantes ul#usersparticipante li#'+userremote);
        var spanicon=$('#icon'+userremote,li);
        if(spanicon.length<1){
            $('.userchataccion',li).prepend(span);    
        }else{
        audiosound.trigger('pause');            
            $('i',spanicon).removeAttr('class').addClass(data.icon);
        }
        var elimino=false;
        if(data.sonido!=''){
            var idtmp=Date.now();
            var audiotmp='<audio id="tmpicon'+idtmp+'" src="'+_sysUrlStatic_+'/media/aulasvirtuales/audios/'+data.sonido+'" autoplay style="display:none;"></audio>'
            $('body').append(audiotmp);
            audiosound.onended =function(){
              $('audio#tmpicon'+idtmp).remove();
               if(elimino==false)$('#icon'+userremote,li).remove();
               elimino=true;
            };
        }
            if(elimino==false){
                setTimeout(function(){$('#icon'+userremote,li).remove();},10000);
                elimino=true;
            }        
    }

    function addsharefile(userremote,data){
        var newfile=$('#vfiles #addfileshareclone').clone(true);
        newfile.find('.nombreuser').text(userremote);
        newfile.find('.nombrefile').text(data.namefile);
        newfile.find('.extension').text(data.extension);
        newfile.find('.link a').attr('href',_sysUrlStatic_+'/media/aulasvirtuales/'+data.namefile).attr('target','_blank');
        newfile.show('fast').removeAttr('id');
        $('#vfiles table').append(newfile);
        $('#vfiles').show();
    }

    function addsharevideo(userremote,data){
        $('#vvideos div.addvideoaqui').html('<video src="'+_sysUrlStatic_+'/media/aulasvirtuales/'+data.namefile+'" controls autoplay>');
        $('#vvideos').show();
    }

    function addscreenshot(userremote,data){
        $('#medias-container audio').hide();
        $('#medias-container video').remove();
        $img=$("#mediascrenshot");
        if($img.length>0)
            $img.attr('src',data.screen);
        else{
            var img='<img src="'+data.screen+'" width="100%" id="mediascrenshot">';        
            $('#medias-container').append(img);
        } 
        //connection._medias.appendChild(media);
        ventana=$('.showwindow[data-ventana="vemiting"]');
        if(ventana.hasClass('active')){
            $('#vemiting').show();
        }else{
            ventana.trigger('click');
        }
    }
    function _cambiarmicrouser(estado,para){       
        if(para===connection.userid){
            // console.log(para+'='+connection.userid+' ------ '+estado);        
            if(estado==='nopermitir'){               
                permiso.micro=false;
                permiso.webcam=false;
                resetmedios();
            }else if(estado==='permitir'){                
                permiso.micro=true;
                resetmedios();
            }else if(estado==='inactive'){                
                $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone bounceIn colorgreen').addClass('fa-microphone-slash');
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone-slash iconinactive animated infinite');
            }else if(estado==='active'){               
                $('#menupersonalizado a.btnsharemicro i').removeClass('fa-microphone-slash').addClass('fa-microphone bounceIn colorgreen');
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone bounceIn colorgreen animated infinite');
            }            
        }else{
            var audiouser=$('audio#audio_'+para);
            if(estado==='nopermitir'){
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone-slash  animated infinite');
            }else if(estado==='permitir'){
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone animated infinite');
            }else if(estado==='inactive'){
                if(audiouser.length) {audiouser.prop('muted',true);  audiouser.trigger('pause');}
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone-slash iconinactive animated infinite');
            }else if(estado==='active'){
                if(audiouser.length) {audiouser.prop('muted',false);  audiouser.trigger('play');}
                $('#vparticipantes #usersparticipante li#'+para+' .btn-micro i.vericon').removeAttr('class').addClass('vericon fa fa-microphone bounceIn colorgreen animated infinite');
            }
        }
    }
    function _cambiarmicro(userremote,data){
        var estado=data.estado;
        var para=userremote;
        if(para==='alluser'){
            var obj=$('#vparticipantes #usersparticipante li.liuser');
            $.each(obj,function(i,v){
                var id=$(v).attr('id');
                _cambiarmicrouser(estado,id);          
            });
            var cls='';
            if(estado==='active') cls='fa fa-microphone colorgreen';
            else if(estado==='inactive') cls='fa fa-microphone-slash iconinactive';
            else if(estado==='permitir')  cls='fa fa-microphone';
            else if(estado==='nopermitir')  cls='fa fa-microphone-slash';
            $('#vparticipantes #usersparticipante .alluser .btn-micro i.vericon').removeAttr('class').addClass('vericon '+cls+' animated infinite');            
        }else{
            _cambiarmicrouser(estado,para);
        }        
    }

    function _stopscreen(userremote){
        if(userremote===connection.userid){
            if(mediosemit.screen)mediosemit.screen.stop();
        }
    }

    function _stopcamera(userremote){
        if(userremote===connection.userid){
            if(mediosemit.camera)mediosemit.camera.stop();
            connection.renegotiate();
        }
        var cameraremote=$('#container-webcams').find('video#camera_'+userremote);
        if(cameraremote.length>0){
            cameraremote.remove();
            var videos=$('#container-webcams').find('video');
            var totalvideos=100/(videos.length+1);
            if(totalvideos<19)totalvideos=19;
            $.each(videos,function(i, o){                   
                $(this).css('width',totalvideos+'%');
            });
        }
    }
    
	/******************************************** eventos sockets ********************************/
    connection.onopen = function(event){
    	var extra=event.extra;
	   	var jsonuser={
    		userid:event.userid,
	        username:extra.username,
	        typeuser:extra.typeuser
    	}
    	addparticipantes(jsonuser,false);
	}
	connection.onclose = function(event){
        //console.log(event);
        if(connection.userid==event.userid){
            if(salirsala==false);
             connection.rejoin(connection.connectionDescription);
        }
		var remoteuserid=event.userid;
    	connection.disconnectWith(remoteuserid);
    	removeparticipante(remoteuserid);
    	$('audio#audio_'+remoteuserid).trigger('pause').remove();
    	$('video#camera_'+remoteuserid).remove();
	}


    connection.onMediaError = function(error){console.log(JSON.stringify(error))};

   	connection.onmessage=function(event){
        //console.log(event);
		var data=event.data||'';
		var accion=data!=''?data.accion:'';
		var userremote=data.userremote||event.userid;		
		if(accion=='addmensajechat') addmensajechat(data,event);
		else if(accion=='cambiartypeuser') cambiartypeuser(data);
		//else if(accion=='cambiarmicrouser') cambiarmicrouser(userremote,data);
		//else if(accion=='cambiarmicroalluser') cambiarmicroalluser(event.userid,data);
		else if(accion=='alzarlamano') alzarlamano(userremote,data);
		else if(accion=='cancelaralzarmano') cancelaralzarmano(data);
		
        //else if(accion=='shichmicro') shichmicro(event.userid,data);
        else if(accion=='shichmicroprivado') shichmicroprivado(event.userid,data);
        else if(accion=='tomarencuesta') tomarencuesta(data);
        else if(accion=='responderencuesta') responderencuesta(event.userid,data); 
        else if(accion=='mostraricono') mostraricono(userremote,data);
        else if(accion=='addsharefile') addsharefile(userremote,data); 
        else if(accion=='addsharevideo') addsharevideo(userremote,data);
        else if(accion=='addscreenshot') addscreenshot(userremote,data);
        else if(accion=='cambiarmicro') _cambiarmicro(userremote,data);
        else if(accion=='stopscreen') _stopscreen(userremote);
        else if(accion=='stopcamera') _stopcamera(userremote);
       // else if(accion=='shichcamara') _shichcamara(event.userid,data);
	}
    var mediosemit={};
	connection.onstream = function(event){
		var userremote=event.userid;
    	var _stream=event.stream;
    	var _tipouser=event.type;    	
    	if (_stream.isScreen){
    		if(_tipouser=='local') {
    			$('.btnsharedesktop i').addClass('active bounceIn colorgreen');   
                mediosemit.screen=_stream;
                if(mediosemit.screenremote){                   
                    mediosemit.screenremote.stop();
                    var data={accion:'stopscreen',userremote:mediosemit.screenremoteuserid};
                    connection.send(data);
                }
                $('.infosmartclass').show();
                $('#medias-container').html('').hide();
    		}else if(_tipouser=='remote'){                
                try {
                    $('.infosmartclass').hide();
                    //$('#medias-container').html('').show();
                    _sysshareScreen=false;        			
        			$('#medias-container').html('<video id="remotescreenuser" width="100%" src="'+URL.createObjectURL(_stream)+'" autoplay></video>').show();
                    mediosemit.screenremote=_stream;
                    mediosemit.screenremoteuserid=userremote;
                    ventana=$('.showwindow[data-ventana="vinformation"]');
                    if(ventana.hasClass('active')){
                        $('#vinformation').show();
                    }else{
                        ventana.trigger('click');
                    }                    
                }catch(err){console.log(err)}
    		}
    	}else if(_stream.isVideo){
            try {
                var videos=$('#container-webcams').find('video');
                var totalvideos=100/(videos.length+1);
                if(totalvideos<19)totalvideos=19;
                var htmlvideo='<video id="camera_'+userremote+'" autoplay muted style="width:'+totalvideos+'%"> <source src="'+URL.createObjectURL(_stream)+'" ></video>';
                $.each(videos,function(i, o){                   
                    $(this).css('width',totalvideos+'%');
                });
                if(_tipouser=='local'){
                    if(mediosemit.camera){
                        //mediosemit.camera.stop();
                        var data={accion:'stopcamera',userremote:userremote};
                        connection.send(data);
                    }
                    mediosemit.camera=_stream;
                }else if(_tipouser=='remote'){
                    var cameraremote=$('#container-webcams').find('video#camera_'+userremote);
                    if(cameraremote.length>0){
                        cameraremote.remove();
                    }
                } 
                $('#container-webcams').append(htmlvideo);
                ventana=$('.showwindow[data-ventana="vwebcam"]');
                if(ventana.hasClass('active')){
                    $('#vwebcam').show();
                }else{
                    ventana.trigger('click');
                }
            }catch(err){console.log(err)}
    	}else if(_stream.isAudio){
                var media=event.mediaElement;
                media.muted=true;
                media.id='audio_'+userremote;
    		    media.controls=true;
            if(_tipouser=='remote'){ 
                if($('#audio_'+userremote).length>0)$('#audio_'+userremote).remove();
                connection._mediaaudios.appendChild(media);
                setTimeout(function() { media.play(); }, 5000);
            }else{                               
                mediosemit.micro=_stream;
            }
    	}
       // connection.renegotiate();
	}

	connection.onstreamended = function(event){
		if(event.stream.isScreen){
			if(event.type=="local"){
			 	$('#menupersonalizado .menucompartir a.btnsharedesktop').removeClass('active');
	            $('.btnsharedesktop i').removeClass('bounceIn colorgreen');
	        }else{
	        	$('video#isScreen_'+event.userid).remove();
	        }
        }else if(event.stream.isVideo){
            var cameraremote=$('#container-webcams').find('video#camera_'+event.userid);
            if(cameraremote.length>0){
                cameraremote.remove();
            }
            var videos=$('#container-webcams').find('video');
            var totalvideos=100/(videos.length+1);
            if(totalvideos<19)totalvideos=19;
            $.each(videos,function(i, o){                   
                $(this).css('width',totalvideos+'%');
            });

        }
       // connection.renegotiate();
	}

	connection.onPeerStateChanged = function(state){
        console.log(state);
		if (state.iceConnectionState.search(/closed|failed/gi) !== -1) {
	        Object.keys(connection.streamEvents).forEach(function(streamid) {
	            var streamEvent = connection.streamEvents[streamid];
	            if (streamEvent.userid === state.userid) {
	                connection.onstreamended(streamEvent);
	            }
	        });
    	}
	}

	connection.getScreenConstraints = function(callback){
		getScreenConstraints(function(error, screen_constraints) {
	        if (!error){
	            screen_constraints = connection.modifyScreenConstraints(screen_constraints);
	            callback(error, screen_constraints);
	            return;
	        }
	        console.log(error);
    	});
	}

    var lastSharedScreenShot = '';
    var _sysshareScreen=false;
    connection.sharePartOfScreen = function(element) {
        html2canvas(element, {
            onrendered: function(canvas){                
                if(_sysshareScreen==false) return;              
                var screenshot = canvas.toDataURL('image/png', 1);
                if(screenshot === lastSharedScreenShot){                    
                    connection.sharePartOfScreen(element);
                    return;
                }
                lastSharedScreenShot=screenshot;               
                var data={accion:'addscreenshot', screen:screenshot }
                connection.send(data);
                connection.sharePartOfScreen(element);                
            },
            grabMouse: true
        });
    };
    connection.stopPartOfScreenSharing=function(){
       _sysshareScreen=false;
    }

	/************************************** Eventos Botones ********************************************/
	var microaudio={tengo:'no',url:'',estado:'pausado'};
	var _detectRTC=connection.DetectRTC;

    $('.btnalzarmano').click(function(ev){
    	var btnalzarmano=$('#menupersonalizado .menutools a.btnalzarmano');
    	var obj=$(this);
        var i=obj.find('i');
        btnalzarmano.toggleClass('active');
        if(btnalzarmano.hasClass('active')){
    	    if(i.length){
    	    	i.removeClass('fa-hand-stop-o').addClass('fa-hand-pointer-o bounceIn colorgreen');
    	    }else{
    	    	$('#menupersonalizado a.btnalzarmano i').removeClass('fa-hand-stop-o').addClass('fa-hand-pointer-o bounceIn colorgreen');
    	    }
    		var data={accion:'alzarlamano',alzarlamano:true}
    		connection.send(data);
    	}else{
    	    var data={accion:'cancelaralzarmano',userremote:connection.userid}
    	    cancelaralzarmano({userremote:connection.userid});
    		connection.send(data);
    	}
    });

    $('#vparticipantes').on('click','#usersparticipante .btnmano',function(){
    	$(this).hide();
    	var userremote=$(this).closest('li').attr('id');
    	var data={accion:'cancelaralzarmano',userremote:userremote}
    	connection.send(data);
    }).on('click','.btnmicroactive',function(ev){
        ev.preventDefault();
        var tipouser=$(this).closest('.sysmenu');
        if(tipouser.hasClass('alluser')){
            var userremote='alluser';
        }else if(tipouser.hasClass('oneuser')){           
            var userremote=$(this).closest('li.liuser').attr('id');            
        }       
        var data={accion:'cambiarmicro',estado:'active',userremote:userremote};
        _cambiarmicro(userremote,data);
        connection.send(data);
    }).on('click','.btnmicroinactive',function(ev){
        ev.preventDefault();
        var tipouser=$(this).closest('.sysmenu');
        if(tipouser.hasClass('alluser')){
            var userremote='alluser';
        }else if(tipouser.hasClass('oneuser')){
            var userremote=$(this).closest('li.liuser').attr('id');
        }       
        var data={accion:'cambiarmicro',estado:'inactive',userremote:userremote};
        _cambiarmicro(userremote,data);
        connection.send(data);
    }).on('click','.btnmicropermitir',function(ev){
        ev.preventDefault();
        var tipouser=$(this).closest('.sysmenu');
        if(tipouser.hasClass('alluser')){
            var userremote='alluser';
        }else if(tipouser.hasClass('oneuser')){
            var userremote=$(this).closest('li.liuser').attr('id');
        }     
        var data={accion:'cambiarmicro',estado:'permitir',userremote:userremote};
        _cambiarmicro(userremote,data);
        connection.send(data);
    }).on('click','.btnmicronopermitir',function(ev){
        ev.preventDefault();
        var tipouser=$(this).closest('.sysmenu');
        if(tipouser.hasClass('alluser')){
            var userremote='alluser';
        }else if(tipouser.hasClass('oneuser')){
            var userremote=$(this).closest('li.liuser').attr('id');
        }       
        var data={accion:'cambiarmicro',estado:'nopermitir',userremote:userremote};
        _cambiarmicro(userremote,data);
        connection.send(data);
    }).on('click','.userstatus',function(){
        var li=$(this).closest('li.dropdown');
        var iduser=li.attr('id');
        var mode=$(this).attr('data-mode'); 
        if(iduser==roomid) return;        
        var _clasi=$('i',this).attr('class');        
        var data={accion:'cambiartypeuser', mode:mode,para:iduser,cls:_clasi}
        cambiartypeuser(data);
        connection.send(data);
    }).on('click','.btnmicroprivado',function(){        
        var userremote=$(this).closest('li').attr('id');
        var data={accion:'shichmicroprivado',userremote:userremote,active:true};
        if($(this).hasClass('active')){
            data.active=false;
        }
        connection.send(data);
        shichmicroprivado(connection.userid,data);
    });

    $('.btnshareventana').click(function(){
        var iobj=$('i',this);
        if(iobj.hasClass('fa-share')){
            iobj.removeClass('fa-share').addClass('fa-share-alt');
            $('#tmpscreenshot').removeAttr('id');          
            $(this).closest('.ventanas').find('.panel-body').attr('id','tmpscreenshot');
            if($('#tmpscreenshot').length>0){
                _sysshareScreen=true;
                connection.sharePartOfScreen(document.getElementById('tmpscreenshot'));
            }
        }else{
            iobj.removeClass('fa-share-alt').addClass('fa-share');
            connection.stopPartOfScreenSharing();
        }
    })

    $('.btnsharemicro').click(function(ev){
        ev.preventDefault(); 
         //_detectRTC.load(function(){
            if (_detectRTC.hasMicrophone === true) { // enable microphone 
                var btnmicro=$('#menupersonalizado .menutools a.btnsharemicro');
                if(!btnmicro.hasClass('active')){ // enable micro  
                    btnmicro.addClass('active'); 
                    var data={accion:'cambiarmicro',estado:'active',userremote:connection.userid};
                    _cambiarmicro(connection.userid,data);
                    connection.send(data);
                }else{  
                    btnmicro.removeClass('active'); 
                    var data={accion:'cambiarmicro',estado:'inactive',userremote:connection.userid};
                    _cambiarmicro(connection.userid,data);
                    connection.send(data);
                }
            }else{
                mostrar_notificacion('Informacion','No se Detecto Microfono, conecte y refresque la pagina','danger');
            }
        //});
    });


    $('.btnsharecamera').click(function(ev){
    	ev.preventDefault();    	
        if (_detectRTC.hasWebcam == true){
        	var btncamera=$('#menupersonalizado .menutools a.btnsharecamera');
            if(!btncamera.hasClass('active')){ // enable camera 
            	$('#vwebcam').fadeIn(500);                 
    			connection.mediaConstraints = {video:true,audio:true};
    			connection.addStream({video: true,audio:false,oneway: true});                
            	btncamera.addClass('active');
                $('.btnsharecamera i').addClass('colorgreen');
            }else{
            	btncamera.removeClass('active');
    			$('.btnsharecamera i').removeClass('active colorgreen'); 
                var data={accion:'stopcamera',userremote:connection.userid};
                connection.send(data);                
            }
        }else{
        	$('#vwebcam').hide();
        	$('.showwindow[data-ventana="vwebcam"]').addClass('active');
        	mostrar_notificacion('Informacion','No se Detecto Camara conectada','danger');        	
        }
    });

    $('.btnsharedesktop').click(function(ev){
    	_detectRTC.load(function(){
    		if(!_detectRTC.isScreenCapturingSupported){           
                mostrar_notificacion('Informacion','Su navegador no soporta captura de pantalla','danger');
                return false;
            }
            getChromeExtensionStatus(function(status){
                if(status==='installed-enabled'){
                	var btnsharepantalla=$('#menupersonalizado .menucompartir a.btnsharedesktop');
                	btnsharepantalla.toggleClass('active');
    	        	if(btnsharepantalla.hasClass('active')){
    	        		connection.session.video=true;
                    	connection.mediaConstraints.video = true;
                    	connection.addStream({audio:false,screen: true,oneway: true});
    	        	}else{
    	        		 if(mediosemit.screen)mediosemit.screen.stop();
    	        	}
                }else{
                    mostrar_notificacion('Informacion','Extension no instalado en su navegador','danger');
                	return false; 
                }           
            });


    	});
    });
  
    function subirfile(file,callback){
         var formData = new FormData();
            formData.append("aula",$('#infousersala ._salaid').text());
            formData.append("filearchivo",file);
            $.ajax({
              url: _sysUrlBase_ +'/aulavirtual/cargarmedia',
              type: "POST",
              data:  formData,
              contentType: false,
              processData: false,
              dataType :'json',
              cache: false,
              processData:false,
              xhr:function(){
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt){
                      if (evt.lengthComputable) {
                        var percentComplete = Math.floor((evt.loaded*100) / evt.total);            
                        $('#divprecargafile .progress-bar').width(percentComplete+'%');
                        $('#divprecargafile .progress-bar span').text(percentComplete+'%');
                      }
                    }, false);
                    xhr.addEventListener("progress", function(evt){
                      if (evt.lengthComputable) {
                        var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
                      }
                    }, false);
                    return xhr;
              },
              beforeSend: function(XMLHttpRequest){
                    div=$('#divprecargafile');
                    $('#divprecargafile').removeClass('progress-bar-danger progress-bar-success progress-bar-striped progress-bar-animated').addClass('progress-bar-striped progress-bar-animated');        
                    $('#divprecargafile .progress-bar').width('0%');
                    $('#divprecargafile .progress-bar span').text('0%'); 
                    $('#divprecargafile').show('fast'); 
              },      
              success: function(data)
              { 
                if(data.code==='ok'){
                  $('#divprecargafile .progress-bar').width('100%');
                  $('#divprecargafile .progress-bar').html('Complete <span>100%</span>');
                  $('#divprecargafile').addClass('progress-bar-success').removeClass('progress-bar-animated');           
                  $('#divprecargafile').hide('fast'); 
                  if(callback) callback(data);  
                  
                }else{
                    $('#divprecargafile').addClass('progress-bar-warning progress-bar-animated');           
                    return false;
                }
              },
              error: function(e) 
              {
                  $('#divprecargafile').addClass('progress-bar-danger progress-bar-animated');
                  $('#divprecargafile .progress-bar').html('Error <span>-1%</span>');           
                  return false;
              },
              complete: function(xhr){
                 $('#divprecargafile .progress-bar').html('Complete <span>100%</span>'); 
                 $('#divprecargafile').addClass('progress-bar-success progress-bar-animated').hide('slow');
              }
            });
    }

    var recorder = new MRecordRTC();
    recorder.mediaType = {
       audio: true, // or StereoAudioRecorder
       video: true, // or WhammyRecorder
    };

    var recorderstart=false;
    var recorderstream=null;
    var URL=window.URL;
    $('.btnsharegrabar').click(function(ev){
    	var btn=$(this);
    	var icon=$('i',btn);
    	icon.toggleClass('active');
    	if(icon.hasClass('active')){
    		icon.addClass('zoomIn colored');            
            recorderstart = true;
            getScreenId(function (error, sourceId, screen_constraints) {
              navigator.getUserMedia = navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
              navigator.getUserMedia(screen_constraints, function (stream) {
                navigator.getUserMedia({audio: true}, function (audioStream) {
                  stream.addTrack(audioStream.getAudioTracks()[0]);                 
                  recorder.addStream(stream);
                  recorder.startRecording();
                  recorderstream=stream;
                }, function (error) {
                  console.log(error);
                });
              }, function (error) {
                console.log(error);
              });
            });
    	}else{            
    		icon.removeClass('zoomIn colored');
            recorderstart = false;            
            recorder.stopRecording(function(){
                var blob = recorder.getBlob();
                var name=$('#infousersala ._salaid').text()+"_video";
                var file = new File([blob.video], name+'.webm', { type: 'video/webm' });
                recorder.save(blob);
                recorder.writeToDisk(blob);              
                recorderstream.stop();
            });
    	}
    });


    $('.btnsharepdfinboard').click(function(ev){
        var file=document.createElement('input');
        file.setAttribute("type", "file");
        file.setAttribute("accept", "application/pdf");
        file.click();
        $(file).change(function(){       
            if(['application/pdf'].indexOf($(this).get(0).files[0].type) == -1) {
                alert('Error : Not a PDF');
                return;
            }
            showPDF(window.URL.createObjectURL($(this).get(0).files[0]));
            $('#vpizarra').show();
            $('.showwindow[data-ventana="vpizarra"]').addClass('active');
            $('.btninpdf').show();
        });
    });

    $("#pdf-prev").on('click', function() {
    	if(__PDFCURRENT_PAGE != 1)	showPage(--__PDFCURRENT_PAGE);
    });

    // Next page of the PDF
    $("#pdf-next").on('click', function() {
    	if(__PDFCURRENT_PAGE != __PDFTOTAL_PAGES) showPage(++__PDFCURRENT_PAGE);
    });
   

    $('.btnsharevideo').click(function(){
        var file=document.createElement('input');
        file.setAttribute("type", "file");
        file.setAttribute("accept", "video/*");
        file.click();    
        $(file).change(function(){        
            _newfile=$(this).get(0).files[0];
            subirfile(_newfile,function(data){
                var midata={
                    accion:'addsharevideo',
                    namefile:data.namefile,
                    extension:data.extension
                  }
                  var userremote=connection.userid==roomid?connection.extra.username:connection.userid;
                  addsharevideo(userremote,midata);
                  connection.send(midata);
            });
        });
    })

    $('.btnsharefile').click(function(){
    	var file=document.createElement('input');
        file.setAttribute("type", "file");
        //file.setAttribute("accept", "image/*");
        file.click();    
        $(file).change(function(){        
            _newfile=$(this).get(0).files[0];
            subirfile(_newfile,function(data){
                var midata={
                    accion:'addsharefile',
                    namefile:data.namefile,
                    extension:data.extension
                  }
                  var userremote=connection.userid==roomid?connection.extra.username:connection.userid;
                  addsharefile(userremote,midata);
                  connection.send(midata);
                  $('#vfiles').show('fast');
            });           
        });
    });

    $('#vchat #txtnewmensaje').keyup(function(e){
    	var txt=$(this).val();
    	if (e.which!=13) return;
    	if (!txt.length) return;
    	var data={
    		accion:'addmensajechat',
    		texto:txt,
    		para:$('#vchat #userchatsms').val()
    	}
    	$(this).val('');
    	addmensajechat(data,'si');
    	connection.send(data);
    });

    $('#vencuesta').on('click','.btn_encuesta_enviar',function(){      
            if($(this).hasClass('disabled'))return false;
            $(this).addClass('disabled');
            var preg=$('#vencuesta #pregunta').val();
            if(preg=='') return false;
            var alt={};
            $('#vencuesta table tr.si input').each(function(){
                var alt_=$(this).val();
                if(alt_!=''){
                    alt[alt_]=0;
                }
            });
            var idencuesta=userid+'_'+Date.now();
            sysencuestas[idencuesta]={accion:'tomarencuesta',idencuesta:idencuesta,pregunta:preg,alternativas:alt,idsala:1};
            connection.send(sysencuestas[idencuesta]);
            var addencuesta='<div class="encuesta" data-id="'+idencuesta+'">'+preg+'<br><a href="#" class="btnverresultadoencuesta">ver respuestas <i class="fa fa-eye"></i></a>';
                addencuesta+=' <a href="#" class="btneliminarencuesta">Eliminar <i class="fa fa-trash"></i></a></div><br>';//  <a href="#" class="">Guardar <i class="fa fa-save"></i></a>'
            $('#vencuesta .resultadoencuesta').append(addencuesta);
            $(this).removeClass('disabled');
    }).on('click','.btnverresultadoencuesta',function(){
        var encuesta=$(this).closest('.encuesta').attr('data-id');
        if(sysencuestas[encuesta])
            var resencuesta=sysencuestas[encuesta];
            if(resencuesta){
                var alternativas=resencuesta.alternativas; 
                var modal='';
                if($('#modaltmp123').length) modal=$('#modaltmp123');
                else modal=$('#modalquestion').clone();
                modal.find('.modal-title').text('Graficos :');
                modal.find('.modal-dialog').removeClass('modal-sm');
                modal.attr('id','modaltmp123');
                modal.find('.modal-body').html('<div><strong>'+resencuesta.pregunta+'</strong></div><div class="graficoPie_'+encuesta+' col-md-6" ></div><div class="col-md-6 graficoBarra_'+encuesta+'" ></div>');
                modal.find('.modal-footer').html('<a href="#" class="btncerrarmodal100">Cerrar</a>');
                modal.modal('show'); 
                modal.on('shown.bs.modal',function(){
                    var series_=[];
                    var labels_=[];
                    var labels2_=[];
                    $.each(alternativas,function(i,v){
                        labels_.push(i+' '+v);
                        labels2_.push(i);
                        series_.push(v);
                    });
                    modal.find('.modal-body').removeAttr('style');
                    var data={ labels: labels_, series: series_};
                    var opciones={ fullWidth: true,chartPadding: 2, showLabel: true, labelOffset: 10, startAngle: 0, };                
                    var chartpie = new Chartist.Pie('.graficoPie_'+encuesta,data,opciones); 
                    chartpie.update();                     
                    var databar={ labels: labels2_, series: [series_]};
                    var chartbar = new Chartist.Bar('.graficoBarra_'+encuesta,databar,{seriesBarDistance: 10}); 
                    chartbar.update();  
                });
            }
    }).on('click','.btneliminarencuesta',function(){
        $(this).closest('.encuesta').remove();
    });
    $('#pantalla1').on('click','#showiconos i.em',function(){    
        var data={};
        data.icon=$(this).attr('class');
        data.accion='mostraricono';
        data.titulo=$(this).attr('title');
        data.sonido=$(this).attr('data-audio');
        mostraricono(connection.userid,data)
        connection.send(data);
    });

    $('.btnlokedroom').click(function(){
        var i = $('i',this);
        if(i.hasClass('fa-lock')) i.removeClass('fa-lock').addClass('fa-unlock');
        else  i.removeClass('fa-unlock').addClass('fa-lock');
    })

    $('.btnsaliraula').click(function(){  
        salirsala=true;
        connection.leave();
        connection.close();
        sessionStorage.clear();
        window.location.href=(_sysUrlBase_);
    }),

    $('body').on('click','a.btnresponder',function(ev){
        ev.preventDefault();
        var modal=$(this).closest('.modal');
        var encuesta={};
            encuesta.accion='responderencuesta';
            encuesta.id=modal.attr('id');
            encuesta.respuesta=$(this).attr('data-value');
            modal.modal('hide');
            connection.send(encuesta);
    }).on('click','a.btncerrarmodal100',function(ev){
        var modal=$(this).closest('.modal');
        modal.modal('hide');
    });

    function opensala(){
        connection.userid=userid;
        connection.open(roomid,function(){           
            var jsonuser={
                userid:connection.userid,
                username:userid,
                userfullname:$('#infousersala ._username').text(),
                useremail:$('#infousersala ._useremail').text(),
                typeuser:tipouser
            }
            console.log('open sala:'+roomid+' , userid:'+connection.userid);
            addparticipantes(jsonuser,true); 
            rtcconectado=true;           
        });
        permiso.emitiendo=true;
        permiso.micro=true;
        permiso.webcam=true;
        permiso.sharedesktop=true;
        permiso.lookedroom=true;
        connection.renegotiate();
        resetprivilegios();
    }

    var esperandomoderador=false;
    function joinsala(){
        resetprivilegios();
        connection.userid=connection.token();
        connection.checkPresence(roomid, function(isRoomExists){
            connection.sessionid=roomid;
            var jsonuser={
                userid:connection.userid,
                username:userid,
                userfullname:$('#infousersala ._username').text(),
                useremail:$('#infousersala ._useremail').text(),
                typeuser:tipouser
            }
            $('#usersparticipante .liuser').remove();
            $('#usersparticipante .lilinea').remove();
            addparticipantes(jsonuser,true);
            if(isRoomExists){                
                connection.connectionDescription=connection.join(roomid,function(){
                    console.log('join sala: '+roomid+'userid: '+connection.userid);
                    rtcconectado=true;
                });
                connection.renegotiate();               
                if(esperandomoderador){
                    //console.log('quepasa');
                    connection.rejoin(connection.connectionDescription);                   
                    //var _idaula=$('#infousersala ._salaid').text();
                    //var user='&user='+$('#infousersala ._user').text();
                    //var email='&email='+$('#infousersala ._useremail').text();
                    //var tipo='&tipo='+tipouser;
                    //window.location.href=_sysUrlBase_+'/aulavirtual/ver/?id='+_idaula+email+user+tipo;
                }
                return;            
            }            
            esperandomoderador=true;
            console.log('esperando moderador');
            setTimeout(joinsala,3000);          
        });        
    }

    function inisala(){
        connection.sessionid=roomid;
        connection.session={data:true,audio:true,video:false,screen:false};
        if(_initsala==true) opensala();
        else joinsala();
    }
    inisala(); 
    $(window).on("beforeunload",function(){
       try {
            connection.getAllParticipants().forEach(function(pid) {
                connection.disconnectWith(pid);
            });
            connection.attachStreams.forEach(function(localStream) {
                localStream.stop();
            });
            if(rtcconectado){              
                connection.closeEntireSession();
                connection.leave();
            }
        }catch(err){}   
    });
});
    
function updatecolor(picker){
    id=picker.styleElement.id;  
    color = picker.toHEXString();
    var a=$('#'+id).closest('a');
    a.attr('data-color',color);
    a.find('i').css('color',color);
    if(a.hasClass('toolcolorfondocanvas')){
        colorcanvas=color;
        $('.herramienta-dibujo .toolcolorfondocanvas').trigger('click');       
    }else if(a.hasClass('toolcolorline')){
        colorborde=color; 
        $('.herramienta-dibujo .toolcolorline').trigger('click'); 
    }else if(a.hasClass('toolcolorfill')){
        colorfondo=color;
        $('.herramienta-dibujo .toolcolorfill').trigger('click'); 
    }   
}
