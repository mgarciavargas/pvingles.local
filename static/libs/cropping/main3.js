;
$(function () {

  var _img_=$('.img-container > img');

  $.each(_img_, function( index, __img ) {
    var alto=$(__img).attr("data-alto")||200;
    var ancho=$(__img).attr("data-ancho")||200;
    $(__img).cropper({
        aspectRatio: ancho / alto,
        strict:true,
        autoCropArea: 1, // Center 60%
        multiple: false,
        dragCrop: false,
        dashed: false,
        movable: false,
        resizable: false,
        guides:false,
        preview: '.img-preview',
        built: function () {
          var data = $(this).cropper('getCropBoxData');
          $(this).cropper('setCropBoxData',data);
        }
      });
    });

  $('.btncropper').click(function(){
   var img__=$(this).attr('data-image');
   var met__=$(this).attr('data-method');
   var opt__=$(this).attr('data-option');
     $("img[alt='"+img__+"']").cropper(met__, opt__);
  });

    var URL = window.URL || window.webkitURL;
    var blobURL;
    if (URL) {
      $('.btncropper-upload').change(function(){
        var _img__ = $(this).attr('alt');
        var files  = $(this).prop('files');       
        var file;
        if (files && files.length) {
          file = files[0];
          if (/^image\/\w+$/.test(file.type)) {
            blobURL = URL.createObjectURL(file);
            $img=$("img[alt='"+_img__+"']");
            $img.one('built.cropper', function (){ URL.revokeObjectURL(blobURL);
            }).cropper('replace', blobURL);

            $('.btncropper-upload').val('');
          } else {
           alert("no es imagen");
          }
        }
      });
    } 
});

