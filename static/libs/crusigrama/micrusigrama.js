var cargarcrucigrama = function(idgui,datos){
    if(datos==undefined)return 0;
    datos=datos[0];   
    if(datos['palabras']==undefined)datos['palabras']=["abel",'chingo','genio'];
    if(datos['frase']==undefined)datos['frase']=["como te llamas",'cual es tu apellido','eres un gran'];
    var words = datos['palabras'];
    var clues = datos['frase'];

    // Create crossword object with the words and clues
    var cw = new Crossword(words, clues);

    // create the crossword grid (try to make it have a 1:1 width to height ratio in 10 tries)
    var tries = 10; 
    var grid = cw.getSquareGrid(tries);

    // report a problem with the words in the crossword
    if(grid == null){
        var bad_words = cw.getBadWords();
        var str = [];
        for(var i = 0; i < bad_words.length; i++){
            str.push(bad_words[i].word);
        }
        alert("Shoot! A grid could not be created with these words:\n" + str.join("\n"));
        return;
    }

    // turn the crossword grid into HTML
    var show_answers = true;
    document.getElementById("crossword"+idgui).innerHTML = CrosswordUtils.toHtml(grid, show_answers);

    // make a nice legend for the clues
    var legend = cw.getLegend(grid);
    addLegendToPage(legend,idgui); // agrega los textos de referncias...
};

function addLegendToPage(groups,idgui){    
    for(var k in groups){
        var html = [];
        var ori=(k=="down")?'col':'row';
        for(var i = 0; i < groups[k].length; i++){
            var txt=groups[k][i]['word'];
            html.push("<li class='crusigramalista' data-valn='"+groups[k][i]['position']+"' data-orient='"+ori+"' data-val='"+txt.toUpperCase()+"' ><strong>" + groups[k][i]['position'] + ".</strong> " + groups[k][i]['clue'] + "</li>");
        }
        document.getElementById(k+idgui).innerHTML = html.join("\n");
    }
}