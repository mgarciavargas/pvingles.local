/*
Autor: abel chingo tello
require jquery
*/
$.fn.mitimer=function(eve){
    this.each(function(i,v){      
        var obj=$(this);
        var timercronometro;
        var curtime={};
        var formato=function(){
            var time=obj.text()||'00:00:00';
            var text = time.split(':');
            if (text.length == 1) {
                curtime.seg=parseInt(text[0]);
            }else if (text.length == 2) {
                curtime.min=parseInt(text[0]);
                curtime.seg=parseInt(text[1]);               
            }else if (text.length == 3) {
                curtime.hor=parseInt(text[0]);
                curtime.min=parseInt(text[1]);
                curtime.seg=parseInt(text[2]);              
            }else{
                curtime.hor=0;
                curtime.min=0;
                curtime.seg=0;
            }
        }
        var formato2=function(){
            var hor='00', min='00',seg='00';
            if(curtime.hor>9) hor=curtime.hor; else hor='0'+curtime.hor;
            if(curtime.min>9) min=curtime.min; else min='0'+curtime.min;
            if(curtime.seg>9) seg=curtime.seg; else seg='0'+curtime.seg;
            obj.text(hor+':'+min+':'+seg);            
        }
        var iniciar=function(){
            iniciado=true;
            formato();
            timercronometro=setInterval(function(){
                curtime.seg++;
                if (curtime.seg == 60){
                    curtime.seg=0;
                    curtime.min++;                    
                }
                if (curtime.min == 60){
                    curtime.min=0;
                    curtime.hor++;
                }
               formato2();
            }, 1000);
        }
        obj.on('oncropausar',function(ev,fcall1){           
                clearInterval(timercronometro);                
        });        

        obj.on('oncroiniciar',function(ev){
            iniciar();
        });

        obj.on('oncroreiniciar',function(ev,txt){
            obj.text('txt');
            iniciar();
        });
        if(eve=='oncroiniciar'){
            obj.trigger('oncroiniciar');
        }        
    });
    return this;
}