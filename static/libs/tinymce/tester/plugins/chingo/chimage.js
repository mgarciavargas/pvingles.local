
tinymce.PluginManager.add('chingoimage', function(editor, url){ 
    function showDialog(){
         var rutabase=tinymce.baseURL+'/../../../../';
        var win, width, height, data={}, dom=editor.dom,element=editor.selection;
        var frmItems=[{name: 'url',  type: 'textbox', size: 40, autofocus: true, label: 'Url'}]
        win = editor.windowManager.open({
            title: 'Selected image',
            url: rutabase+'biblioteca/?plt=tinymce&type=image',
            width: 400,
            height: 600
        },{
            editor:editor
        });
    }
    editor.addButton('chingoimage', {
        tooltip: 'Insert/edit image',
        icon: 'fa fa-photo',
        onclick: showDialog,
        stateSelector: ['img[data-mce-object=image]']
    });
    editor.addMenuItem('chingoimage', {
        icon: 'fa fa-photo',
        text: 'Insert/edit image',
        onclick: showDialog,
        context: 'insert',
        prependToContext: true
    });
    editor.addCommand('mceImage', showDialog);
    this.showDialog = showDialog;
});