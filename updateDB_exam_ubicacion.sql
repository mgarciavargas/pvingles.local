-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-03-2018 a las 16:42:31
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles.local`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_ubicacion`
--

CREATE TABLE `examen_ubicacion` (
  `idexamen_ubicacion` int(11) NOT NULL,
  `idrecurso` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `tipo` varchar(4) COLLATE utf8_unicode_ci NOT NULL COMMENT 'exam. ubicación de: [N]ivel, [U]nidad',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `rango_min` float DEFAULT NULL COMMENT 'rango mínimo para poder darlo',
  `idexam_prerequisito` int(11) DEFAULT '0' COMMENT 'idexamen_ubicacion que necesita aprobar primero'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `examen_ubicacion`
--

INSERT INTO `examen_ubicacion` (`idexamen_ubicacion`, `idrecurso`, `idcurso`, `tipo`, `activo`, `rango_min`, `idexam_prerequisito`) VALUES
(1, 236, 1, 'N', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_ubicacion_alumno`
--

CREATE TABLE `examen_ubicacion_alumno` (
  `idexam_alumno` int(11) NOT NULL,
  `idexamen` int(11) NOT NULL COMMENT 'idexamen_ubicacion',
  `idalumno` int(11) NOT NULL,
  `estado` varchar(4) COLLATE utf8_spanish_ci NOT NULL COMMENT '[O]mitido ; [T]omado',
  `resultado` text COLLATE utf8_spanish_ci,
  `fecha` timestamp NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `examen_ubicacion_alumno`
--

INSERT INTO `examen_ubicacion_alumno` (`idexam_alumno`, `idexamen`, `idalumno`, `estado`, `resultado`, `fecha`) VALUES
(1, 1, 72042592, 'T', '{"idexamen":"236","idalumno":"eder.figueroa","puntaje":"75","tiempoduracion":"00:01:57","fecha":"2018-03-06 18:08:12","intento":"1","calificacion_min":"0","calificacion_max":"{\\"escala 1\\":\\"80\\",\\"escala 2\\":\\"60\\",\\"escala 3\\":\\"40\\",\\"escala 4\\":\\"20\\",\\"escala 5\\":\\"0\\"}","calificacion_unidad":""}', '2018-03-06 23:09:04');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `examen_ubicacion`
--
ALTER TABLE `examen_ubicacion`
  ADD PRIMARY KEY (`idexamen_ubicacion`);

--
-- Indices de la tabla `examen_ubicacion_alumno`
--
ALTER TABLE `examen_ubicacion_alumno`
  ADD PRIMARY KEY (`idexam_alumno`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
