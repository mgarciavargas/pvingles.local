INSERT INTO `ubigeo` (`id_ubigeo`, `pais`, `departamento`, `provincia`, `distrito`, `ciudad`) VALUES ('000000', 'PE', '00', '00', '00', 'Perú');
ALTER TABLE `persona_apoderado` ADD PRIMARY KEY(`idapoderado`);
INSERT INTO `general` (`idgeneral`, `codigo`, `nombre`, `tipo_tabla`, `mostrar`) VALUES ('43', '1', 'congratulations', 'tiporecord', '1'), ('44', '2', 'Banns', 'tiporecord', '1');


CREATE TABLE `persona_record` (
  `idrecord` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `tiporecord` int(11) NOT NULL,
  `titulo` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `archivo` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecharegistro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mostrar` tinyint(4) NOT NULL,
  `idusuario` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


ALTER TABLE `persona_record`
  ADD PRIMARY KEY (`idrecord`);


  ALTER TABLE `acad_cursodetalle` ADD `color` VARCHAR(15) NULL AFTER `idpadre`;
  ALTER TABLE `acad_cursodetalle` ADD `esfinal` TINYINT NULL DEFAULT '0' AFTER `color`;
  ALTER TABLE `niveles` ADD `descripcion` TEXT NULL AFTER `imagen`;