<?php
defined('RUTA_BASE') or die();

$texts = array();
////////////////////---------------------A---------------------------------
$texts['all rights reserved']='tous les droits sont réservés';
$texts['adjustment']='le réglage';
$texts['add']='ajouter';
$texts['active']='actif';
$texts['aa']='';
////////////////////---------------------B---------------------------------
$texts['banners']='bannières';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------C---------------------------------
$texts['customers']='les clients';
$texts['content']='contenu';
$texts['currency']='device';
$texts['change password']='changer le mot de passe';
$texts['change language']='changer de langue';
$texts['cancel']='Annuler';
$texts['customer']='client';
$texts['combination of email and password incorrect']='Combinaison de courriel et mot de passe incorrect';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------D---------------------------------
$texts['did you forget your password?']="Nous avons eu quelqu'un oublie le mot de passe";
$texts['developer']='révélateur';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------E---------------------------------
$texts['exit']='Sortie';
$texts['email']='email';
$texts['e-mail']='email';
$texts['error']='Erreur';
////////////////////---------------------F---------------------------------
$texts['flag']='drapeau';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------G---------------------------------
$texts['generator']='Générateur';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------H---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------I---------------------------------
$texts['inactive']='Inactif';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------J---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------K---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------L---------------------------------
$texts['log in'] = 'connectez-vous';
$texts['license']='Licence';
$texts['language']='la langue';
$texts['list']='listage';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------M---------------------------------
$texts['media library']='Médiathèque';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------N---------------------------------
$texts['name']='Prénom';
$texts['new']='Nouveau';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------O---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------P---------------------------------
$texts['password'] = 'mot de passe';
$texts['pages']='pages';
$texts['page']='page';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------Q---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------R---------------------------------
$texts['recover password']='Mot de passe perdu';
$texts['reserves']='Réserves';
$texts['reserve list']='liste de réserve';
$texts['return to login']='revenir à vous identifier';
$texts['role']='rôle';
$texts['aa']='';
////////////////////---------------------S---------------------------------
$texts['sales']='Ventes';
$texts['settings']='réglage';
$texts['services']='les services';
$texts['save']='registrer';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------T---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------U---------------------------------
$texts['user'] = 'Utilisateur';
$texts['users and privileges']='utilisateurs et des privilèges';
$texts['users']='utilisateurs';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------V---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------W---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------X---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------Y---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
////////////////////---------------------Z---------------------------------
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
$texts['aa']='';
