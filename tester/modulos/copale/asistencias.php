<?php
session_start();
date_default_timezone_set('America/Lima');
if($_SESSION['copaleusuario']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
$copaleusu=$_SESSION['copaleusuario'];
	$datoscopale=$obj->select_assoc("select * from copale_usuario where idcopale_usuario=$copaleusu");
$dist=$datoscopale[0]['idDist'];
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>

<script type="text/javascript">
function imprSelec(muestra,cantidad)
{ 
	var ficha=document.getElementById(muestra);
	var ventimp=window.open(' ','popimpr');
	ventimp.document.write(ficha.innerHTML);
	ventimp.document.close();
	ventimp.print();
	ventimp.close();

}
</script>


	<link type="text/css" href="calendario/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="calendario/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.core.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker-es.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker.js"></script>
		<link type="text/css" href="calendario/demos.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function() {
				$("#desde").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});

			$(function() {
				$("#hasta").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true ,//Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});


		</script>


</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>
	
	<div id="loadcontent" >

    <div class="btnmore">
    	<input type="button" value="Registrar" class="btag" onclick="select_item('reg_asistencia.php');">
    </div>

	<form action="" method="post">




	<table align="center" style="width:410px;">
	<tr>
		<th colspan="7">Asistencias</th>
	</tr>

   <tr>
		<td>Institucion Educativa</td>
		<td>
			<select name="instituciones" id="instituciones" onchange="selects_alumn_dats('periodo','periodoe',this.value,document.getElementById('instituciones').value,'')" >
				<option value="">.:Seleccionar</option>
				<?php
				$listaie=$obj->select_assoc("select * from institucion where idDist=$dist");
				for($i=0;$i<count($listaie);$i++)
				{
					if($_POST['instituciones']==$listaie[$i]['id_institucion'])
						{
							$sie="selected=''";
						}
						else
						{
							$sie="";
						}
					?>
					<option <?php echo $sie?> value="<?php echo $listaie[$i]['id_institucion']?>"><?php echo $listaie[$i]['razon_social']?></option>
					<?php
				}
				?>
			</select>
		</td>

   	<td>Periodo Escolar</td>
   	<td id="periodoe">
   	<?php
if($_POST['periodo']!="")
{
?>
<select name="periodo" id="periodo" onchange="selects_alumn_dats('grado','gradoe',this.value,'','')">
				<option value="">Seleccionar</option>
				<?php
				$iep=$_POST['instituciones'];
				$sqld=$obj->select_assoc("select * from periodo_escolar where idinstitucion=$iep");
				for($i=0;$i<count($sqld);$i++)
				{
					if($_POST['periodo']==$sqld[$i]['idperiodo'])
					{
					$sl="selected=''";
					}
					else
					{
						$sl="";
					}
					?>
					<option <?php echo $sl;?> value="<?php echo $sqld[$i]['idperiodo']?>"><?php echo $sqld[$i]['periodo']?></option>
					<?php
				}
				?>
			</select>
<?php
}else{
   	?>
   	<select disabled="">
   		<option value="">Seleccionar</option>
   	</select>
   	<?php
}
   	?>
   	</td>
   </tr>

	<tr>
		<td>Seleccione el Grado</td>
		<td id="gradoe">
		<?php
if($_POST['grado']!="")
{
   	$nivelie=$obj->select_assoc("select idnivel from institucion where id_institucion=$iep");
   	$codnv=$nivelie[0]['idnivel'];
   	$sgrado=$obj->select_assoc("select * from grado_nivel where  idnivel=$codnv");
		?>
   <select name="grado" onchange="selects_ubg('secc',this.value,'seccid')">
   	<option value="0">Seleccionar</option>
   	<?php

   	for($i=0;$i<count($sgrado);$i++)
   	{

   		if($_POST['grado']==$sgrado[$i]['idgrado'])
   		{
$sl="selected=''";
   		}
   		else
   		{
   			$sl="";
   		}
?>
<option <?php echo $sl?> value="<?php echo $sgrado[$i]['idgrado']?>"><?php echo $sgrado[$i]['grado']?></option>
<?php
   	}
   	?>
   </select>
   <?php
}
	else
	{
		?>
   <select disabled=""><option>Seleccionar</option></select>
   <?php
	}

   ?>
		</td>


		<td>Seleccione la Seccion</td>
		<td id="seccid">
		<?php
if($_POST['grado']!=""){
	$pgrd=$_POST['grado'];
	$periodoe=$_POST['periodo'];
$sqlprosec=$obj->select_assoc("select * from grado_seccion where idgrado=$pgrd and idperiodo=$periodoe");
if(count($sqlprosec)==0){
echo '<select disabled=""><option>Seleccionar</option></select>';
}
else
{
?>
<select name="seccion" onchange="carga_fechas_asis(this.value);selects_ub('areas',this.value,'lstareas');selects_alumn_dats('area','lstareas',this.value,document.getElementById('instituciones').value,document.getElementById('periodo').value)" >
	<option value="0">Seleccionar</option>
	<?php
for($i=0;$i<count($sqlprosec);$i++){
	if($_POST['seccion']==$sqlprosec[$i]['idgseccion'])
	{
$sls="selected=''";
	}
	else
	{
$sls="";
	}
	?>
	<option <?php echo $sls?> value="<?php echo $sqlprosec[$i]['idgseccion']?>"><?php echo $sqlprosec[$i]['seccion']?></option>
	<?php
	}?>
</select>
<?php
}
}	else
{
	?>
<select disabled=""><option>Seleccionar</option></select>
	<?php
}	
?>
		</td>
	</tr>
	</table>
	<?php
if($_POST['seccion']!="")
{
$dis="block";
}
else
{
	$dis="none";
}
	?>
	<table align="center" style="width:409px;margin-top:0px;border-top:0px none #fff;display:<?php echo $dis?>;" id="addic">
		<tr>
			<td align="center" width="80">Curso</td>
			<td id="lstareas">
			<?php
if($_POST['area']!="")
{
	$gsc=$_POST['seccion'];
	$pdo=$_POST['periodo'];

	$sqlproarea=$obj->select_assoc("select ac.idinstitucion,ac.idperiodo,ac.idasignarcurso,m.idmgn,m.area,n.des_nivel,s.idgseccion,g.grado,s.seccion,p.nombres,p.ap_paterno,p.ap_materno,ac.l,ac.m,ac.mi,ac.j,ac.v,ac.s,ac.d,ac.estado from asignar_curso ac inner join malla_grado_nivel m on ac.idmgn=m.idmgn inner join nivel n on ac.idnivel=n.idnivel inner join grado_seccion s on ac.idgseccion=s.idgseccion inner join grado_nivel g on s.idgrado=g.idgrado inner join personal p on ac.iddocente=p.idpersonal where s.idgseccion=$gsc and ac.idinstitucion=$iep and ac.idperiodo=$pdo");
	?>
<select name="area" style="width:120px;font-size:12px;">
	<option value="">Seleccionar</option>
	<?php
for($i=0;$i<count($sqlproarea);$i++){
	if($_POST['area']==$sqlproarea[$i]['idmgn'])
	{
$slmgn="selected=''";
	}
	else
	{
$slmgn="";
	}
	?>
	<option <?php echo $slmgn?> value="<?php echo $sqlproarea[$i]['idmgn']?>"><?php echo $sqlproarea[$i]['area']?></option>
	<?php
	}?>
</select>
<?php
}
else
{
			?>
<select ><option>Seleccionar</option></select>
<?php
}
?>
			</td>
			<td   align="center">Fecha</td>
			<td ><input type="text" name="fecha" id="hasta" class="txtc" value="<?php echo $_POST['fecha']?>" ></td>

			
		</tr>
		<tr>
			<td colspan="4" align="center"><input type="submit" name="buscar" value="Buscar" class="btgenerate" onclick="this.form.action='asistencias.php'"></td>
		</tr>
	</table>

	

	<?php
if(count($sqlprosec)==0)
{
?>
<script type="text/javascript">
document.getElementById('addic').style.display="none";
</script>
<?php
}
	?>
	</form>

	
		<?php
      $seccion=$_POST['seccion'];
      $curso=$_POST['area'];
      $fecha=$_POST['fecha'];
      if($_POST['buscar']=="Buscar"){

if($fecha>date('Y-m-d'))
{
echo "<div style='margin:0 auto;color:red;width:400px;text-align:center;font-size:14px;'>No Puede Consultar  Asistencia de una Fecha Futura</div>";
}
else
{

      if($seccion!="" & $fecha!="")
      {
      	if($curso=="")
      	{
 
	$sqlconsulta=$obj->select_assoc("select * from asignar_curso where idgseccion=$seccion");
      	}
      	else
      	{
      		$sqlconsulta=$obj->select_assoc("select * from asignar_curso where idgseccion=$seccion and idmgn=$curso ");
      	}
      
      	$consultardia=$obj->otener_dia($fecha);
      	if($sqlconsulta[0]['l']=="l" & $consultardia=="Lunes" || $sqlconsulta[0]['m']=="m" & $consultardia=="Martes" || $sqlconsulta[0]['mi']=="m" & $consultardia=="Miercoles" || $sqlconsulta[0]['j']=="j" & $consultardia=="Jueves" || $sqlconsulta[0]['v']=="v" & $consultardia=="Viernes" || $sqlconsulta[0]['s']=="s" & $consultardia=="Sabado" || $sqlconsulta[0]['d']=="d" & $consultardia=="Domingo")
      	{

// consultar asistencias
      	if($curso=="")
      	{
 
      			$asistencias=$obj->select_assoc("select a.idasistencia,a.fecha,al.idalumno,al.nombres,al.ap_paterno,al.ap_materno,ac.idasignarcurso,m.idmgn,m.area,gs.idgseccion,g.grado,gs.seccion,p.idpersonal,p.nombres,p.ap_paterno,p.ap_materno,a.estado_asistencia,ac.l,ac.m,ac.mi,ac.j,ac.v,ac.s,ac.d from asistencia_alumno a inner join alumno al on a.idalumno=al.idalumno inner join asignar_curso ac on a.idasigna_curso=ac.idasignarcurso inner join malla_grado_nivel m on ac.idmgn=m.idmgn inner join grado_seccion gs on ac.idgseccion=gs.idgseccion
inner join grado_nivel g on gs.idgrado=g.idgrado inner join personal p on ac.iddocente=p.idpersonal
where a.fecha='$fecha' and gs.idgseccion=$seccion");
      	}
      	else
      	{
      			$asistencias=$obj->select_assoc("select a.idasistencia,a.fecha,al.idalumno,al.nombres,al.ap_paterno,al.ap_materno,ac.idasignarcurso,m.idmgn,m.area,gs.idgseccion,g.grado,gs.seccion,p.idpersonal,p.nombres,p.ap_paterno,p.ap_materno,a.estado_asistencia,ac.l,ac.m,ac.mi,ac.j,ac.v,ac.s,ac.d from asistencia_alumno a inner join alumno al on a.idalumno=al.idalumno inner join asignar_curso ac on a.idasigna_curso=ac.idasignarcurso inner join malla_grado_nivel m on ac.idmgn=m.idmgn inner join grado_seccion gs on ac.idgseccion=gs.idgseccion
inner join grado_nivel g on gs.idgrado=g.idgrado inner join personal p on ac.iddocente=p.idpersonal
where a.fecha='$fecha' and m.idmgn=$curso and gs.idgseccion=$seccion");
      	}
      		
      		if(count($asistencias)>0)
      		{
?>

<br><br><br>
	
	<div class="controlsplus">
		<div class="plusct">
			<img src="images/icono_imprimir.jpg" style="width:100%:height:100%;" alt="Imprimir" title="Imprimir" onclick="imprSelec('results','<?php echo $calm?>');">
		</div>
		<div class="plusct">
			<img src="images/export_excel.png" style="width:100%:height:100%;" alt="Exportar a Excel" title="Exportar a Excel">
		</div>
	</div>
	<hr>
<div id="results" >


		<?php
		 
	

		      	if($curso=="")
      	{
      		$cursoslst=$obj->select_assoc("select  gs.idinstitucion,gs.idperiodo,gs.idgseccion,g.grado, gs.seccion,ac.idasignarcurso,d.idpersonal,concat(d.nombres,' ',d.ap_paterno,' ',d.ap_materno) as docente,mc.idmgn,mc.area from grado_seccion gs  inner join grado_nivel g on gs.idgrado=g.idgrado inner join asignar_curso ac on gs.idgseccion=ac.idgseccion inner join personal d on ac.iddocente=d.idpersonal inner join malla_grado_nivel mc on ac.idmgn=mc.idmgn where gs.idgseccion=$seccion and gs.idinstitucion=$iep and gs.idperiodo=$periodoe");
 $more="yes";
      			$listaalumnado="select m.idmatricula,m.idperiodo,m.idinstitucion,a.idalumno,concat(a.nombres,' ',a.ap_paterno,' ',a.ap_materno) as alumno,gs.idgseccion,g.grado,
gs.seccion,ac.idasignarcurso,d.idpersonal,concat(d.nombres,' ',d.ap_paterno,' ',d.ap_materno) as docente,mc.idmgn,mc.area
from matricula m inner join alumno a on m.idalumno=a.idalumno
inner join grado_seccion gs on m.idgradoseccion=gs.idgseccion inner join grado_nivel g  
on gs.idgrado=g.idgrado inner join asignar_curso ac on m.idgradoseccion=ac.idgseccion
inner join personal d on ac.iddocente=d.idpersonal inner join malla_grado_nivel mc on
ac.idmgn=mc.idmgn where gs.idgseccion=$seccion and m.idinstitucion=$iep and m.idperiodo=$periodoe";
      	}
      	else
      	{
      		$more="no";
      			$listaalumnado=$obj->select_assoc("select m.idmatricula,m.idperiodo,m.idinstitucion,a.idalumno,concat(a.nombres,' ',a.ap_paterno,' ',a.ap_materno) as alumno,gs.idgseccion,g.grado,
gs.seccion,ac.idasignarcurso,d.idpersonal,concat(d.nombres,' ',d.ap_paterno,' ',d.ap_materno) as docente,mc.idmgn,mc.area
from matricula m inner join alumno a on m.idalumno=a.idalumno
inner join grado_seccion gs on m.idgradoseccion=gs.idgseccion inner join grado_nivel g  
on gs.idgrado=g.idgrado inner join asignar_curso ac on m.idgradoseccion=ac.idgseccion
inner join personal d on ac.iddocente=d.idpersonal inner join malla_grado_nivel mc on
ac.idmgn=mc.idmgn where gs.idgseccion=$seccion and mc.idmgn=$curso and m.idinstitucion=$iep and m.idperiodo=$periodoe");
      	}

		
		?>

	<?php
if($more=="yes")
{

?>

		<?php
		$listaalumnadooddd=$obj->select_assoc($listaalumnado);
 
if(count($listaalumnadooddd) > 0){
	?>
	<div style="margin:0 auto;width:700px;line-height:25px;color:#333;font-size:14px;border:1px solid #333;border-top:0px none #fff;border-left:0px none #fff;border-right:0px none #fff;">
		<b>Institucion Educativa:</b>
		<?php

$nomie=$obj->select_assoc("select id_institucion,razon_social from institucion where id_institucion=".$listaalumnadooddd[0]['idinstitucion']."");
echo $nomie[0]['razon_social'];
		?>
	</div>
<?php
}
?>

<?php
	for($i=0;$i<count($cursoslst);$i++)
	{
$mgnl=$cursoslst[$i]['idmgn'];
			 $lstalumns=$listaalumnado." and mc.idmgn=$mgnl";
			$listaalumnadoo=$obj->select_assoc($lstalumns);
?>




<table align="center" width="600" style="border-top:0px none #fff;border-left:0px none #fff;border-right:0px none #fff;">
			<tr>
			<td style="font-size:14px;"><b>Docente:</b></td>
				<td  style="font-size:13px;"><?php echo $cursoslst[$i]['docente']?></td>
				<td style="font-size:14px;"><b>Curso:</b></td>
				<td style="font-size:13px;"align="center"  >
					<?php echo $cursoslst[$i]['area']?>
				</td>
			</tr>
		</table>

<table align="center" cellpadding="0" cellspacing="0">

		<tr >
			 
			<th style="width:80px;">Fecha</th>
			<th style="width:160px;">Alumno</th>
			<th style="width:50px;">Grupo</th>
			
			<th style="width:70px;">Asistencia</th>
		 
		</tr>




			<?php
			
for($ii=0;$ii<count($listaalumnadoo);$ii++)
{
	$alm=$listaalumnadoo[$ii]['idmgn'];
	$asc=$listaalumnadoo[$ii]['idasignarcurso'];
	$frc=$_POST['fecha'];
	$verificaasistencia="select * from asistencia_alumno";
	
	$setomoasis=$obj->select_assoc($verificaasistencia." where idalumno=$alm and idasigna_curso=$asc and fecha='$frc' ");
	if(count($setomoasis)==1)
	{
		if($setomoasis[0]['estado_asistencia']=="A")
		{
$asistio="<label style='color:green'>Asistio</label>";
		}
		if($setomoasis[0]['estado_asistencia']=="T")
		{
$asistio="<label style='color:orange'>Tardanza</label>";
		}
		if($setomoasis[0]['estado_asistencia']=="F")
		{
$asistio="<label style='color:red'>Falto</label>";
		}
		if($setomoasis[0]['estado_asistencia']=="FJ")
		{
$asistio="<label style='color:brown'>Falta Justificada</label>";
		}
	}
	else
	{
		$asistio="No Registrada";
	}

			?>
			<tr class="listado">
			 
			<td align="center" style="">
			<?php echo $_POST['fecha']?>
			</td>
			<td align="center" style=""><?php echo $listaalumnadoo[$ii]['alumno'];?></td>
			<td align="center" style=""><?php echo $listaalumnadoo[$ii]['grado']." - ".$listaalumnadoo[$ii]['seccion']?></td>
			
			<td align="center" style=""><?php echo $asistio ?></td>
		
			 
		</tr>
<?php
}
?>

	</table>


<?php
	}
}
else
{
	?>

	<?php
		 
 
if(count($listaalumnado) > 0){
	?>
	<div style="margin:0 auto;width:700px;line-height:25px;color:#333;font-size:14px;border:1px solid #333;border-top:0px none #fff;border-left:0px none #fff;border-right:0px none #fff;">
		<b>Institucion Educativa:</b>
		<?php

$nomie=$obj->select_assoc("select id_institucion,razon_social from institucion where id_institucion=".$listaalumnado[0]['idinstitucion']."");
echo $nomie[0]['razon_social'];
		?>
	</div>
<?php
}
?>

	<table align="center" width="600" style="border-top:0px none #fff;border-left:0px none #fff;border-right:0px none #fff;">
			<tr>
			<td style="font-size:14px;"><b>Docente:</b></td>
				<td  style="font-size:13px;"><?php echo $listaalumnado[0]['docente']?></td>
				<td style="font-size:14px;"><b>Curso:</b></td>
				<td style="font-size:13px;"align="center"  >
					<?php echo $listaalumnado[0]['area']?>
				</td>
			</tr>
		</table>

<table align="center" cellpadding="0" cellspacing="0">
		<tr >
			 
			<th style="width:80px;">Fecha</th>
			<th style="width:160px;">Alumno</th>
			<th style="width:50px;">Grupo</th>
			
			<th style="width:70px;">Asistencia</th>
		 
		</tr>




			<?php
for($i=0;$i<count($listaalumnado);$i++)
{
	$alm=$listaalumnado[$i]['idalumno'];
	$asc=$listaalumnado[$i]['idasignarcurso'];
	$frc=$_POST['fecha'];
	$verificaasistencia="select * from asistencia_alumno";
$verificaasistencia." where idalumno=$alm and idasigna_curso=$asc and fecha='$frc'";
	$setomoasis=$obj->select_assoc($verificaasistencia." where idalumno=$alm and idasigna_curso=$asc and fecha='$frc'");
	if(count($setomoasis)==1)
	{
		if($setomoasis[0]['estado_asistencia']=="A")
		{
$asistio="<label style='color:green'>Asistio</label>";
		}
		if($setomoasis[0]['estado_asistencia']=="T")
		{
$asistio="<label style='color:orange'>Tardanza</label>";
		}
		if($setomoasis[0]['estado_asistencia']=="F")
		{
$asistio="<label style='color:red'>Falto</label>";
		}
		if($setomoasis[0]['estado_asistencia']=="FJ")
		{
$asistio="<label style='color:brown'>Falta Justificada</label>";
		}
	}
	else
	{
		$asistio="No Registrada";
	}

			?>
			<tr class="listado">
			 
			<td style="">
			<?php echo $_POST['fecha']?>
			</td>
			<td style=""><?php echo $listaalumnado[$i]['alumno'];?></td>
			<td style=""><?php echo $listaalumnado[$i]['grado']." - ".$listaalumnado[$i]['seccion']?></td>
			
			<td style=""><?php echo $asistio ?></td>
		
			 
		</tr>
<?php
}
?>

	</table>


	<?php
}

	?>



<?php
      		}
      		else
      		{
?>
<center><input type="button" value="Registrar Asistencia" class="btsa" onclick="select_item('reg_asistencia.php');" style="margin-top:10px;"></center>
<?php
      		}

      	}
      	
      		else
      	{
      		echo "<div style='margin:0 auto;color:red;width:400px;text-align:center;font-size:14px;'>Los Dias ".$consultardia." Esta Seccion no Lleva este Curso</div>";
      	}


      	
         
      	}

      	}

       }

       else 
      {
     
       }

       
 
	
		?>


		</div>
	
	</div>

</div>

</body>
</html>

<?php
}
?>
