<?php
session_start();
if($_SESSION['copaleusuario']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
include_once('function/function_all.php');
$obj= new cl_all_functions();
	$copaleusu=$_SESSION['copaleusuario'];
	$datoscopale=$obj->select_assoc("select * from copale_usuario where idcopale_usuario=$copaleusu");
$dist=$datoscopale[0]['idDist'];
$codpersonal=$_GET['codpersonal'];
if($codpersonal=="")
{
echo '<meta http-equiv="refresh" content="0; url=docentes.php">';
}else
{
	if(!is_numeric($codpersonal))
	{
echo '<meta http-equiv="refresh" content="0; url=docentes.php">';
	}
	else
	{
		$existe=$obj->select_rows("select idpersonal from personal where idpersonal=$codpersonal");
		if($existe==0)
		{
echo '<meta http-equiv="refresh" content="0; url=docentes.php">';
		}
		else
		{
			$datos=$obj->select_assoc("select d.idinstitucion,d.direccion,d.idpersonal,d.nombres,d.ap_paterno,d.ap_materno,t.tipopersonal,d.dni_num,d.correo,d.telefono,d.sexo,d.id_Depa,d.id_Prov,d.id_Dist,d.f_naci,d.foto,d.usuario,d.password,d.descripcion from personal d inner join tipo_personal t on d.idtipopersonal=t.idtipopersonal where d.idpersonal=$codpersonal");
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>


<link type="text/css" href="calendario/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="calendario/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.core.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker-es.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker.js"></script>
		<link type="text/css" href="calendario/demos.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function() {
				$("#fnaci").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});

	


		</script>

</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_personal.php" method="post" enctype="multipart/form-data" autocomplete="off">
		<table align="center">
<tr>
	<th colspan="4">Actualizar Datos del Personal</th>
</tr>

<tr>
		<td class="headertd">Institucion Educativa</td>
	<td >
		<?php
				$cole=$datos[0]['idinstitucion'];
				$listaie=$obj->select_assoc("select * from institucion where id_institucion=$cole");
		?>
		<input type="text" name="cole" disabled="" value="<?php echo $listaie[0]['razon_social']?>">
					<input type="hidden" name="instituciones"  value="<?php echo $listaie[0]['id_institucion']?>">
	</td>
	<td class="headertd">Tipo Personal</td>
	<td  >
	<?php
$docentetp=$obj->select_assoc("select * from tipo_personal where tipopersonal not like 'Docente%' order by tipopersonal ASC");
	?>
    <select name="tpd">
    <?php
    for($i=0;$i<count($docentetp);$i++){
    	if($docentetp[$i]['idtipopersonal']==$datos[0]['idtipopersonal'])
    	{
       $sl="selected=''";
    	}else{
    		$sl="";
    	}
    ?>
    <option <?php echo $sl;?> value="<?php echo $docentetp[$i]['idtipopersonal']?>"><?php echo $docentetp[$i]['tipopersonal']?></option>
    <?php
    }
    ?>
    </select>
	</td>
</tr>

<tr>
	<td class="headertd">Apellido Paterno</td>
	<td><input type="text" name="app" class="txtl" value="<?php echo $datos[0]['ap_paterno']?>"></td>
	<td class="headertd">Apellido Materno</td>
	<td><input type="text" name="apm" class="txtl" value="<?php echo $datos[0]['ap_materno']?>"></td>
</tr>
<tr>
	<td class="headertd">Nombres</td>
	<td><input type="text" name="nom" class="txtl" value="<?php echo $datos[0]['nombres']?>"></td>
	<td class="headertd">Fecha Nacimiento</td>
	<td><input type="text" name="fnaci" id="fnaci" class="txtm" value="<?php echo $datos[0]['f_naci']?>"></td>
</tr>
<tr>
	<td class="headertd">Sexo</td>
	<td>
		<select name="sex">
		    <?php
    if($datos[0]['sexo']=="m")
    {
    	$m="selected=''";
    }
    else if($datos[0]['sexo']=="f")
    {
    	$f="selected=''";
    }
		    ?>
			<option value="0">.:Seleccionar</option>
			<option <?php echo $m?> value="m">Masculino</option>
			<option <?php echo $f?> value="f">Femenino</option>
		</select>
	</td>
	<td class="headertd">Num. DNI</td>
	<td><input type="text" name="dni" id="dni" maxlength="8" class="txtm" value="<?php echo $datos[0]['dni_num']?>"></td>
</tr>

<tr>
	<td class="headertd" colspan="4">
	<div class="slub">
	Departamento 
	<?php
	$dep=$obj->select_assoc("select * from ubdepartamento where departamento='Piura'");
	?>
	
	<select name="depa" class="ubs" onchange="selects_ub('depa',this.value,'dprov');disableselect();">
		<option value="">.:Seleccionar</option>
		<?php
		for($i=0;$i<count($dep);$i++){
			if($dep[$i]['idDepa']==$datos[0]['id_Depa'])
			{
                $s="selected=''";
			}
			else
			{
				$s="";
			}
		?>
		<option <?php echo $s?> value="<?php echo $dep[$i]['idDepa']?>"><?php echo $dep[$i]['departamento']?></option>
		<?php
		}
		?>
	</select></div>
	 
	 <div class="slub" id="dprov"> 
	Provincia  
	 <?php
	  $ppv=$datos[0]['id_Depa'];
	  ?>
	<select name="prov" class="ubs" onchange="selects_ub('prov',this.value,'ddist')">
		<option value="">.:Seleccionar</option>
		<?php

		$provs=$obj->select_assoc("select * from ubprovincia where idDepa=$ppv");
		for($i=0;$i<count($provs);$i++){
			if($provs[$i]['idProv']==$datos[0]['id_Prov'])
			{
                $s="selected=''";
			}
			else
			{
				$s="";
			}
		?>
		<option <?php echo $s?> value="<?php echo $provs[$i]['idProv']?>"><?php echo $provs[$i]['provincia']?></option>
		<?php
		}
		?>
	</select></div>
	
	<div class="slub" id="ddist">  
	Distrito   
	
	<select name="dist" class="ubs" >
		<option value="">.:Seleccionar</option>
				<?php
$ddt=$datos[0]['id_Prov'];
		$dists=$obj->select_assoc("select * from ubdistrito where idProv=$ddt");
		for($i=0;$i<count($dists);$i++){
			if($dists[$i]['idDist']==$datos[0]['id_Dist'])
			{
                $s="selected=''";
			}
			else
			{
				$s="";
			}
		?>
		<option <?php echo $s?> value="<?php echo $dists[$i]['idDist']?>"><?php echo $dists[$i]['distrito']?></option>
		<?php
		}
		?>
	</select></div>
	</td>
</tr>


<tr>
	<td class="headertd">Direccion</td>
	<td><input type="text" name="dir"  class="txtl" value="<?php echo $datos[0]['direccion']?>"></td>
	<td class="headertd">Email</td>
	<td><input type="text" name="mail"  class="txtl" value="<?php echo $datos[0]['correo']?>"></td>
</tr>

<tr>
	<td class="headertd">Usuario</td>
	<td><input type="text" name="usu"  class="txtm" value="<?php echo $datos[0]['usuario']?>"></td>
	<td class="headertd">Clave</td>
	<td><input type="password" name="pass" id="pass"  class="txtm" value="<?php echo $datos[0]['password']?>">
        <input type="button" value="Generar" class="btgenerate" onclick="generapass()">
	</td>
</tr>


<tr>
	<td class="headertd">Telefono</td>
	<td><input type="text" name="tel"  class="txtm" value="<?php echo $datos[0]['telefono']?>">
<input type="hidden" name="docenteident" value="<?php echo $datos[0]['idpersonal']?>">
	</td>
    <td class="headertd"></td>
	<td></td>
</tr>
<tr>
<td class="headertd">Foto</td>
	<td>
<input type="file" class="filefoto"  id="foto" name="foto"  title="Clic para agregar una Foto">
<input type="hidden" name="fant" value="<?php echo $datos[0]['foto']?>">
	<div class="ftbox" title="Clic para agregar una Foto" id="lfoto">	
	<?php
	if($datos[0]['foto']!="")
	{?>
		<img src="../institucion/personal/thumbs/<?php echo $datos[0]['foto']?>" style='width:100px;height:100px;'>
   <?php
}
	?>
		</div>
		
   <output id="lists"></output>
		<script>
              function archivo(evt) {
                  var files = evt.target.files; // FileList object
             
                  // Obtenemos la imagen del campo "file".
                  for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }
             
                    var reader = new FileReader();
             
                    reader.onload = (function(theFile) {
                        return function(e) {
                          // Insertamos la imagen
                         document.getElementById("lfoto").innerHTML = ['<img class="thumb" style="width:100px;height:100px;" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                        };
                    })(f);
             
                    reader.readAsDataURL(f);
                  }
              }
             
              document.getElementById('foto').addEventListener('change', archivo, false);
      </script>

	</td>
			
	<td class="headertd"></td>
	<td></td>
	
</tr>


<tr>
	<td class="headertd">
	Observaci&oacute;n
	</td>
	<td colspan="3">
		<textarea name="obs" >
			<?php echo trim($datos[0]['descripcion'])?>
		</textarea>
	</td>
</tr>


<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Actualizar" name="personal">
		<input class="btr" type="reset" value="Cancelar" style="display:none;">
		<input class="btb" type="button" value="salir" onclick="select_item('personal.php')">
	</td>
</tr>

</table>
</form>

	</div>
</div>

</body>
</html>

<?php
}
}
}
}
?>
