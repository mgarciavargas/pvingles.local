<?php
session_start();
date_default_timezone_set('America/Lima');
if($_SESSION['copaleusuario']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
		$copaleusu=$_SESSION['copaleusuario'];
	$datoscopale=$obj->select_assoc("select * from copale_usuario where idcopale_usuario=$copaleusu");
$dist=$datoscopale[0]['idDist'];
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>

	<link type="text/css" href="calendario/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="calendario/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.core.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker-es.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker.js"></script>
		<link type="text/css" href="calendario/demos.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function() {
				$("#fecha").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});

	


		</script>


</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_pagos.php" method="post" enctype="multipart/form-data">
		<table align="center" cellspacing="0" cellpadding="0">
<tr>
	<th colspan="4">Registrar Pago de Servicio</th>
</tr>

<tr>
	<td  class="headertd">I. Educativa</td>
	<td>
<select name="instituciones" onchange="selects_alumn_dats('apafas','lstapafas',this.value,'','');">
				<option value="">.:Seleccionar</option>
				<?php
				$listaie=$obj->select_assoc("select * from institucion where idDist=$dist");
				for($i=0;$i<count($listaie);$i++)
				{
					if($_POST['instituciones']==$listaie[$i]['id_institucion'])
						{
							$sie="selected=''";
						}
						else
						{
							$sie="";
						}
					?>
					<option <?php echo $sie?> value="<?php echo $listaie[$i]['id_institucion']?>"><?php echo $listaie[$i]['razon_social']?></option>
					<?php
				}
				?>
			</select>
	</td>
</tr>

<tr>
	<td class="headertd">Fecha</td>
	<td  ><input type="text" name="fecha" id="fecha" value="<?php echo date('Y-m-d')?>" class="txtc"></td>
</tr>
<tr>
	<td class="headertd">Concepto</td>
	<td  ><input type="text" name="concepto" class="txtl"></td>
</tr>
<tr>
	 
	<td class="headertd">Monto</td>
	<td><input type="text" name="monto" class="txtc"></td>
</tr>
<tr>
	<td class="headertd">APAFA</td>
	<td id="lstapafas"><select disabled=""><option>Seleccionar</option></select></td>	 
</tr>

<tr>
	<td class="headertd">Observacion</td>
	<td ><textarea name="obs" style="width:200px;height:60px;"></textarea></td>
</tr>



<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Grabar" name="pagos">
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('pagos.php')">
	</td>
</tr>

</table>



</form>

	</div>
</div>

</body>
</html>


<?php
}
?>