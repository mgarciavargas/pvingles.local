<?php
session_start();
if($_SESSION['copaleusuario']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
$copaleusu=$_SESSION['copaleusuario'];
	$datoscopale=$obj->select_assoc("select * from copale_usuario where idcopale_usuario=$copaleusu");
$dist=$datoscopale[0]['idDist'];
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>

<link type="text/css" href="calendario/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="calendario/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.core.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker-es.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker.js"></script>
		<link type="text/css" href="calendario/demos.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function() {
				$("#fnaci").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});

	


		</script>

</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_personal.php" method="post" enctype="multipart/form-data" autocomplete="off">
		<table align="center">
<tr>
	<th colspan="4">Registro de Personal</th>
</tr>

<tr>

<td class="headertd">Institucion Educativa</td>
		<td>
			<select name="instituciones"  >
				<option value="">.:Seleccionar</option>
				<?php
				$listaie=$obj->select_assoc("select * from institucion where idDist=$dist");
				for($i=0;$i<count($listaie);$i++)
				{
					if($_POST['instituciones']==$listaie[$i]['id_institucion'])
						{
							$sie="selected=''";
						}
						else
						{
							$sie="";
						}
					?>
					<option <?php echo $sie?> value="<?php echo $listaie[$i]['id_institucion']?>"><?php echo $listaie[$i]['razon_social']?></option>
					<?php
				}
				?>
			</select>
		</td>


	<td class="headertd">Tipo Personal</td>
	<td >
	<?php
$docentetp=$obj->select_assoc("select * from tipo_personal where tipopersonal not like 'Docente%' order by tipopersonal ASC");
	?>
    <select name="tpd">
    <?php
    for($i=0;$i<count($docentetp);$i++){
    ?>
    <option value="<?php echo $docentetp[$i]['idtipopersonal']?>"><?php echo $docentetp[$i]['tipopersonal']?></option>
    <?php
    }
    ?>
    </select>
	</td>
</tr>

<tr>
	<td class="headertd">Apellido Paterno</td>
	<td><input type="text" name="app" class="txtl"></td>
	<td class="headertd">Apellido Materno</td>
	<td><input type="text" name="apm" class="txtl"></td>
</tr>
<tr>
	<td class="headertd">Nombres</td>
	<td><input type="text" name="nom" class="txtl"></td>
	<td class="headertd">Fecha Nacimiento</td>
	<td><input type="text" name="fnaci" id="fnaci" class="txtm"></td>
</tr>
<tr>
	<td class="headertd">Sexo</td>
	<td>
		<select name="sex">
			<option value="0">.:Seleccionar</option>
			<option value="m">Masculino</option>
			<option value="f">Femenino</option>
		</select>
	</td>
	<td class="headertd">Num. DNI</td>
	<td><input type="text" name="dni" id="dni" maxlength="8" class="txtm"></td>
</tr>

<tr>
	<td class="headertd" colspan="4">
	<div class="slub">
	Departamento 
	<?php
	$dep=$obj->select_assoc("select * from ubdepartamento where departamento='Piura'");
	?>
	
	<select name="depa" class="ubs" onchange="selects_ub('depa',this.value,'dprov');disableselect();">
		 
		<?php
		for($i=0;$i<count($dep);$i++){
		?>
		<option value="<?php echo $dep[$i]['idDepa']?>"><?php echo $dep[$i]['departamento']?></option>
		<?php
		}
		?>
	</select></div>
	 
	 <div class="slub" id="dprov"> 
	Provincia  
	 
	
	 <?php
	$pv=$dep[0]['idDepa'];
	$prov=$obj->select_assoc("select * from ubprovincia where idDepa=$pv");
	?>
	<select name="pv" onchange="distritop(this.value,'ddist')">
		<option vaue="0">Seleccionar</option>
		<?php
for($i=0;$i<count($prov);$i++){
		?>
		<option value="<?php echo $prov[$i]['idProv']?>"><?php echo $prov[$i]['provincia']?></option>
		<?php
		}
		?>
	</select>

	</div>
	
	<div class="slub" id="ddist">  
	Distrito   
	
	<select name="dist" class="ubs" disabled="">
		<option value="">.:Seleccionar</option>
	</select></div>
	</td>
</tr>



<tr>
	<td class="headertd">Direccion</td>
	<td><input type="text" name="dir"  class="txtl"></td>
	<td class="headertd">Email</td>
	<td><input type="text" name="mail"  class="txtl"></td>
</tr>

<tr>
	<td class="headertd">Usuario</td>
	<td><input type="text" name="usu"  class="txtm"></td>
	<td class="headertd">Clave</td>
	<td><input type="password" name="pass" id="pass"  class="txtm">
        <input type="button" value="Generar" class="btgenerate" onclick="generapass()">
	</td>
</tr>


<tr>
	<td class="headertd">Telefono</td>
	<td><input type="text" name="tel"  class="txtm"></td>
    <td class="headertd"></td>
	<td></td>
</tr>
<tr>
<td class="headertd">Foto</td>
	<td>
<input type="file" class="filefoto"  id="foto" name="foto"  title="Clic para agregar una Foto">
	<div class="ftbox" title="Clic para agregar una Foto" id="lfoto">	
		</div>
		
   <output id="lists"></output>
		<script>
              function archivo(evt) {
                  var files = evt.target.files; // FileList object
             
                  // Obtenemos la imagen del campo "file".
                  for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }
             
                    var reader = new FileReader();
             
                    reader.onload = (function(theFile) {
                        return function(e) {
                          // Insertamos la imagen
                         document.getElementById("lfoto").innerHTML = ['<img class="thumb" style="width:100px;height:100px;" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                        };
                    })(f);
             
                    reader.readAsDataURL(f);
                  }
              }
             
              document.getElementById('foto').addEventListener('change', archivo, false);
      </script>

	</td>
			
	<td class="headertd"></td>
	<td></td>
	
</tr>


<tr>
	<td class="headertd">
	Observaci&oacute;n
	</td>
	<td colspan="3">
		<textarea name="obs">
			
		</textarea>
	</td>
</tr>


<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Grabar" name="personal">
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('personal.php')">
	</td>
</tr>

</table>
</form>

	</div>
</div>

</body>
</html>

<?php
};
?>
