<?php
session_start();
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
		include_once('function/function_all.php');
$obj= new cl_all_functions();
	$institucion=$_SESSION['institucion'];
	?>

<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script src="script/function_select.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
   $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '<Ant',
 nextText: 'Sig>',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
 weekHeader: 'Sm',
 dateFormat: 'dd/mm/yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);
  $(function() {
    $( "#fini" ).datepicker({
  dateFormat: 'yy-mm-dd' 
});
    $( "#ffin" ).datepicker({
  dateFormat: 'yy-mm-dd' 
});

  });
  </script>

</head>
<body>
<div id="boxlist">
</div>
<div id="bloqp"></div>
<?php include('perfil.php');?>



<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_escalafones.php" method="post" enctype="multipart/form-data">
		<table align="center">
<tr>
	<th colspan="4">Agregar Escalfon Docente</th>
</tr>
<tr>
	<td class="headertd">Docente</td>
	<td colspan="3"><input type="text" name="docente" id="docente" class="txtl" disabled="">
	<input type="button" value="Seleccionar Docente" class="bselect" onclick="lstdoc('boxlist','docentes')">
	<input type="hidden" name="docenteid" id="docenteid">
	</td>
</tr>
<tr>
	<td class="headertd">Titulo</td>
	<td ><input type="text" name="titulo" class="txtl"></td>
	<td class="headertd">Instituci&oacute;n</td>
	<td ><input type="text" name="institucion" class="txtl"></td>
</tr>

<tr>
	<td class="headertd">Fecha Inicio</td>
	<td><input type="text" name="fini" id="fini" class="txtm"></td>
	<td class="headertd">Fecha Fin</td>
	<td><input type="text" name="ffin" id="ffin" class="txtm"></td>
</tr>

<tr>
	<td class="headertd" colspan="2"><input  onclick="estudiando('ffin')" type="checkbox" name="estudiando">Actualmente Especializandose</td>
	<td class="headertd">Pais</td>
	<td>
	<?php
	$sqlpais=$obj->select_assoc("select * from paises order by Pais ASC");
	?>
	<select style="width:100px;" name="pais">
	<?php
	for($i=0;$i<count($sqlpais);$i++){
		if($sqlpais[$i]['Pais']=="Peru")
		{
			$sl="selected=''";
		}
		else
		{
			$sl="";
		}
	?>
	<option <?php echo $sl?> value="<?php echo $sqlpais[$i]['Pais']?>"><?php echo $sqlpais[$i]['Pais']?></option>
	<?php
}
	?>
	</select>
	</td>
</tr>



<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Grabar" name="escalafon">
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('academico.php')">
	</td>
</tr>

</table>
</form>

	</div>
</div>

</body>
</html>


<?php
}
?>