<?php
session_start();
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
		include_once('function/function_all.php');
$obj= new cl_all_functions();
	$institucion=$_SESSION['institucion'];
	?>

<!DOCTYPE html>
<html>
<head>
<title></title>
 
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>

 

</head>
<body>
<div id="boxlist">
</div>
<div id="bloqp"></div>
<?php include('perfil.php');?>



<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_matricula.php" method="post" enctype="multipart/form-data">
		<table align="center">
<tr>
	<th colspan="4">Registrar Matr&iacute;cula</th>
</tr>

<tr>
	<td class="headertd">Periodo</td>
	<td colspan="3">
		<select name="periodo" id="periodo">
			<?php
$pesc=$obj->select_assoc("select * from periodo_escolar where idinstitucion=$institucion and estado='a' ");
			?>
			<option value="<?php echo $pesc[0]['idperiodo']?>"><?php echo $pesc[0]['periodo']?></option>
	</select>
</td>
</tr>

<tr>
	<td class="headertd">Alumno</td>
	<td colspan="3"><input type="text" name="alumno" id="alumno" class="txtl" disabled="">
	<input type="button" value="Seleccionar Alumno" class="bselect" onclick="block_page_blank('boxalumno.php','alumnos','500','300')">
	<input type="hidden" name="codalumn" id="codalumn">
	</td>
</tr>
<tr>
	<td class="headertd">Situaci&oacute;n</td>
	<td colspan="3" >
		<select name="st">
			<option value="Reciente">Nuevo</option>
			<option value="Promovido">Promovido</option>
			<option value="Repetir">Repetir</option>
		</select>
	</td>
</tr>

<tr>
	<td class="headertd">Grado</td>
	<td>
		<?php
   	$nivelie=$obj->select_assoc("select idnivel from institucion where id_institucion=$institucion");
   	$codnv=$nivelie[0]['idnivel'];
   	$sgrado=$obj->select_assoc("select * from grado_nivel where  idnivel=$codnv");
		?>
   <select name="grado" onchange="selects_ubg('secc',this.value,'seccid')">
   	<option value="">Seleccionar</option>
   	<?php

   	for($i=0;$i<count($sgrado);$i++)
   	{
?>
<option value="<?php echo $sgrado[$i]['idgrado']?>"><?php echo $sgrado[$i]['grado']?></option>
<?php
   	}
   	?>
   </select>
	</td>
	<td class="headertd">Secci&oacute;n</td>
	<td id="seccid">
	<select disabled="">
	<option>Seleccionar</option></select></td>
</tr>


<tr>
	<td class="headertd">Procedencia</td>
	<td colspan="3">
		<select name="procede">
		    <option value="nuevo">Nuevo</option>
			<option value="misma">Misma I.E</option>
			<option value="traslado">Traslado</option>
		</select>
	</td>
	
</tr>

<tr>
	<td class="headertd">Obervaci&oacute;n</td>
	<td colspan="3">
		<textarea name="obs"></textarea>
	</td>
	
</tr>

<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Grabar" name="matricula">
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('matriculas.php')">
	</td>
</tr>

</table>
</form>

	</div>
</div>

</body>
</html>


<?php
}
?>