<?php
session_start();
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
	$institucion=$_SESSION['institucion'];
	$amb=$_GET['ambiente'];
	if($amb!="" & is_numeric($amb))
	{
$datos=$obj->select_assoc("select * from ambiente where idinstitucion=$institucion and idambiente=$amb");
	}
	else
	{
		echo '<meta http-equiv="refresh" content="0; url=ambientes.php">';
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>
</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_ambientes.php" method="post" enctype="multipart/form-data">
		<table align="center">
<tr>
	<th colspan="4">Actualizar Ambiente</th>
</tr>
<tr>
	<td class="headertd">Tipo Ambiente</td>
	<td>			
		<?php
$tipos=$obj->select_assoc("select * from tipo_ambiente");
				?>
		<select name="tipoa">
					<option>.:Seleccionar</option>
					<?php
					
					for($i=0;$i<count($tipos);$i++){
						if($tipos[$i]['idtipoambiente']==$datos[0]['idtipoambiente'])
						{
							$sl="selected=''";
						}
						else
						{
							$sl="";
						}
					?>
					<option <?php echo $sl?> value="<?php echo $tipos[$i]['idtipoambiente']?>" <?php echo $st?> > <?php echo $tipos[$i]['tipo']?></option>
					<?php }?>
				</select>
	</td>
	<td class="headertd">Descripci&oacute;n</td>
	<td><input type="text" name="des" class="txtl" value="<?php echo $datos[0]['ambientedescripcion']?>"></td>
</tr>
<tr>
	<td class="headertd">Ubicaci&oacute;n</td>
	<td><input type="text" name="ubik" class="txtl" value="<?php echo $datos[0]['ubicacion']?>"></td>
	<td class="headertd">Capacidad</td>
	<td><input type="text" name="capacidad" class="txtc" value="<?php echo $datos[0]['capacidad']?>"></td>
</tr>
<tr>
	<td class="headertd">Area m2</td>
	<td><input type="text" name="area" class="txtc" value="<?php echo $datos[0]['metrosc']?>"></td>
	<td class="headertd">Estado Ambiente</td>
	<td>
		<select name="estado">
			<?php
			if($datos[0]['estadoamb']=="Bueno")
			{
				$b="selected=''";
			}
			if($datos[0]['estadoamb']=="Regular")
			{
				$r="selected=''";
			}
			if($datos[0]['estadoamb']=="Malo")
			{
				$m="selected=''";
			}
			?>
			<option value="">--Seleccionar</option>
			<option <?php echo $b?> value="Bueno">Bueno</option>
			<option <?php echo $r?> value="Regular">Regular</option>
			<option <?php echo $m?> value="Malo">Malo</option>
		</select>
	</td>
</tr>

<tr>
	<td class="headertd">Observacion</td>
	<td colspan="3"><textarea name="obs"><?php echo $datos[0]['observacion']?></textarea>
<input type="hidden" name="ambt" value="<?php echo $_GET['ambiente']?>">
	</td>
</tr>



<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Actualizar" name="ambientes">
 
		<input class="btb" type="button" value="salir" onclick="select_item('ambientes.php')">
	</td>
</tr>

</table>
</form>

	</div>
</div>

</body>
</html>


<?php
}
?>