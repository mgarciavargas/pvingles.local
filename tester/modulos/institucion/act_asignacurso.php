<?php
session_start();
date_default_timezone_set('America/Lima');
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
	$institucion=$_SESSION['institucion'];

if(isset($_GET['asigna']))
{
	$ac=$_GET['asigna'];
	$datos=$obj->select_assoc("select * from asignar_curso where idasignarcurso=$ac");
	$pd=$datos[0]['idperiodo'];
	$dct=$datos[0]['iddocente'];
	$cs=$datos[0]['idmgn'];
$periodo=$obj->select_assoc("select * from periodo_escolar where idperiodo=$pd");
$docente=$obj->select_assoc("select * from personal where idpersonal=$dct");
$curso=$obj->select_assoc("select * from malla_grado_nivel where idmgn=$cs");
}
else
{
	echo '<meta http-equiv="refresh" content="0; url=reg_asignacurso.php">';
}


?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>
</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_asignar.php" method="post" enctype="multipart/form-data">
		<table align="center" cellspacing="0" cellpadding="0">
<tr>
	<th colspan="4">Asignar Curso</th>
</tr>
<tr>
	<td class="headertd">Periodo</td>
	<td><input type="text" name="curso" class="txtc"   value="<?php echo $periodo[0]['periodo']?>" readonly="">
<input type="hidden" name="codp" value="<?php echo $periodo[0]['idperiodo'];?>">

	</td>
</tr>
<tr>
	<td class="headertd">Curso</td>
	<td><input type="text" name="curso" class="txtl" value="<?php echo $curso[0]['area']?>" readonly="">
<input type="hidden" name="gseccion" value="<?php echo $datos[0]['idgseccion'];?>">
<input type="hidden" name="codcurso" value="<?php echo $datos[0]['idmgn'];?>">
<input type="hidden" name="nivel" value="<?php echo $datos[0]['idnivel'];?>">
	</td>
</tr>

<tr>
	<td class="headertd">Docente</td>
	<td>
<input type="text" name="tt" id="tt" class="txtl" readonly="" value="<?php echo $docente[0]['nombres']." ".$docente[0]['ap_paterno']." ".$docente[0]['ap_materno']?>" >
		<input type="hidden" name="codtt" id="codtt" class="txtl" value="<?php echo $dct?>" >
<input type="button" value="..." onclick="block_page_blank('boxdocente.php','Docetes',500,300)">
	</td>
</tr>


<tr>
	<td class="headertd">Num. Horas</td>
	<td><input type="text" name="nhoras" id="horas" class="txtc"  value="<?php echo $datos[0]['duracion']?>" onblur="calcularduracion('hfin');"></td>
</tr>

<tr>
	<td class="headertd">Hora Inicio</td>
	<td><input type="text" name="hini" id="hini" class="txtc" value="<?php echo $datos[0]['hini']?>" onblur="calcularduracion('hfin');"></td>
</tr>

<tr>
	<td class="headertd">Hora Fin</td>
	<td><input type="text" name="hfin" id="hfin" class="txtc"  value="<?php echo $datos[0]['hfin']?>" onclick="calcularduracion('hfin');" ></td>
</tr>

<tr>
	<td class="headertd" colspan="2">Seleccionar Dias</td>
</tr>
<tr>
	<td colspan="2">
		<?php 
		if($datos[0]['l']=="l")
		{
			$l="checked=''";
		}
		if($datos[0]['m']=="m")
		{
			$m="checked=''";
		}
		if($datos[0]['mi']=="mi")
		{
			$mi="checked=''";
		}
		if($datos[0]['j']=="j")
		{
			$j="checked=''";
		}
		if($datos[0]['v']=="v")
		{
			$v="checked=''";
		}
		if($datos[0]['s']=="s")
		{
			$s="checked=''";
		}
		if($datos[0]['d']=="d")
		{
			$d="checked=''";
		}

		?>
    Lunes <input type="checkbox" name="dial" value="l" <?php echo $l?>>
    Martes <input type="checkbox" name="diam" value="m" <?php echo $m?>>
    Miercoles <input type="checkbox" name="diami" value="mi" <?php echo $mi?>>
    Jueves <input type="checkbox" name="diaj" value="j" <?php echo $j?>>
    Viernes <input type="checkbox" name="diav" value="v" <?php echo $v?>>
    Sabado <input type="checkbox" name="dias"  value="s" <?php echo $s?>>
    Domingo <input type="checkbox" name="diad" value="d" <?php echo $d?>>
	</td>
</tr>




<tr>
	<td class="headertd" colspan="4">
		<input type="hidden" name="codasigna" value="<?php echo $_GET['asigna']?>">
		<input class="bts" type="submit" value="Actualizar" name="asignar">
		 
		<input class="btr" type="submit" value="Liberar" name="asignar">
		<input class="btb" type="button" value="salir" onclick="select_item('asignar_curso.php')">
	</td>
</tr>

</table>



</form>

	</div>
</div>

</body>
</html>


<?php
}
?>