<?php
session_start();
date_default_timezone_set('America/Lima');
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
		$institucion=$_SESSION['institucion'];
 
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>

	<link type="text/css" href="calendario/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="calendario/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.core.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker-es.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker.js"></script>
		<link type="text/css" href="calendario/demos.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function() {
				$("#fecha").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});

	


		</script>


</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_pedidos.php" method="post" enctype="multipart/form-data">
		<table align="center" cellspacing="0" cellpadding="0">
<tr>
	<th colspan="4">Registrar Pedido</th>
</tr>

 

<tr>
	<td class="headertd">Fecha</td>
	<td  ><input type="text" name="fecha" id="fecha" value="<?php echo date('Y-m-d')?>" class="txtc"></td>
</tr>
<tr>
	<td class="headertd">Pedido</td>
	<td ><input type="text" name="dpedido" class="txtl"></td>
</tr>
<tr>
	 
	<td class="headertd">Cantidad</td>
	<td><input type="text" name="cantidad" class="txtc"></td>
</tr>
<tr>
	<td class="headertd">Ugel</td>
	<td id="lstugel">
		<?php
$ugie=$obj->select_assoc("select * from institucion where id_institucion=$institucion");
$cou=$ugie[0]['idugel'];
$ugeld=$obj->select_assoc("select * from ugel where idugel=$cou");
		?>
		<input type="text" readonly="" name="nomugel" value="<?php echo $ugeld[0]['nombre']?>" class="txtm">
		<input type="hidden" name="ugel" value="<?php echo $ugeld[0]['idugel']?>"  >
	</td>	 
</tr>

<tr>
	<td class="headertd">Descripcion</td>
	<td ><textarea name="obs" style="width:200px;height:60px;"></textarea></td>
</tr>



<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Grabar" name="pedido">
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('pagos.php')">
	</td>
</tr>

</table>



</form>

	</div>
</div>

</body>
</html>


<?php
}
?>