<?php
session_start();
date_default_timezone_set('America/Lima');
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
	$institucion=$_SESSION['institucion'];
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>

	<link type="text/css" href="calendario/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="calendario/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.core.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker-es.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker.js"></script>
		<link type="text/css" href="calendario/demos.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function() {
				$("#fecha").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});

	


		</script>


</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_pagos.php" method="post" enctype="multipart/form-data">
		<table align="center" cellspacing="0" cellpadding="0">
<tr>
	<th colspan="4">Registrar Pago de Servicio</th>
</tr>


<tr>
	<td class="headertd">Fecha</td>
	<td  ><input type="text" name="fecha" id="fecha" value="<?php echo date('Y-m-d')?>" class="txtc"></td>
</tr>
<tr>
	<td class="headertd">Concepto</td>
	<td  ><input type="text" name="concepto" class="txtl"></td>
</tr>
<tr>
	 
	<td class="headertd">Monto</td>
	<td><input type="text" name="monto" class="txtc"></td>
</tr>
<tr>
	<td class="headertd">APAFA</td>
	<td id="lstapafas">
<?php
$apafas=$obj->select_assoc("select * from apafa where idinstitucion=$institucion");
?>
<select name="apafa">
	<option value="">Seleccionar</option>
	<?php
for($i=0;$i<count($apafas);$i++)
{
	$periodoap=$apafas[$i]['idperiodo'];
	$periodo=$obj->select_assoc("select * from periodo_escolar where idperiodo=$periodoap");
	?>
	<option value="<?php echo $apafas[$i]['idapafa']?>">Apafa<?php echo " ".$periodo[0]['periodo']?></option>
	<?php
}
	?>
</select>
	</td>	 
</tr>

<tr>
	<td class="headertd">Observacion</td>
	<td ><textarea name="obs" style="width:200px;height:60px;"></textarea></td>
</tr>



<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Grabar" name="pagos">
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('pagos.php')">
	</td>
</tr>


</table>



</form>

	</div>
</div>

</body>
</html>


<?php
}
?>