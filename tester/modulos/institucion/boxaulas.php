<?php
session_start();
include_once('function/function_all.php');
$obj= new cl_all_functions();
$nom=$_POST['nom'];
$institucion=$_SESSION['institucion'];
?>
<!DOCTYPE html>
<html>
<head>
<TITLE>Listado de Aulas</TITLE>
	<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
	<script type="text/javascript">
function cargar_ambiente(ntutor,codtt)
{
  window.opener.document.getElementById('aula').value=ntutor
  window.opener.document.getElementById('caula').value=codtt;
  window.close();
}
	</script>
</head>
<body style="margin:0px;">
<div style="width:450px;line-height:30px;color:#042D59;text-align:center;font-family:Arial;font-weight:bold;">Listado de Aulas</div>
	<table align="center" cellpadding="0" cellspacing="0" width="420">
		<tr>
			<th style="width:60px;">N.</th>
			<th style="width:200px;">Aula</th>
			
		</tr>
		<?php 
		$lista=$obj->select_assoc("select a.idambiente,t.tipo,a.ambientedescripcion from ambiente a inner join tipo_ambiente t on a.idtipoambiente=t.idtipoambiente where a.idinstitucion=$institucion and t.tipo like 'Aula%'");
		for($i=0;$i<count($lista);$i++){
		?>
		<tr style="cursor:pointer;" class="listado" onclick="cargar_ambiente('<?php echo $lista[$i]['ambientedescripcion']?>','<?php echo $lista[$i]['idambiente']?>')">
		<td><?php echo $i+1?></td>
		<td><?php echo $lista[$i]['tipo']?></td>
		</tr>
		<?php 
		}?>
	</table>
</body>
</html>
<?php

?>