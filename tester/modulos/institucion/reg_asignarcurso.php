<?php
session_start();
date_default_timezone_set('America/Lima');
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
	$institucion=$_SESSION['institucion'];

	if($_GET['curso']=="" || !(is_numeric($_GET['curso'])))
	{
echo '<meta http-equiv="refresh" content="0; url=asignar_curso.php">';
	}
	else
	{
		$datosie=$obj->select_assoc("select idnivel from institucion where id_institucion=$institucion");
$nvie=$datosie[0]['idnivel'];
$cso=$_GET['curso'];
 
		$cursodatos=$obj->select_assoc("select * from malla_grado_nivel where idmgn=$cso and idnivel=$nvie");
	}
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>
</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_asignar.php" method="post" enctype="multipart/form-data">
		<table align="center" cellspacing="0" cellpadding="0">
<tr>
	<th colspan="4">Asignar Curso</th>
</tr>
<tr>
	<td class="headertd">Periodo</td>
	<?php
	$pd=$_GET['periodo'];
$lper=$obj->select_assoc("select * from periodo_escolar where idperiodo=$pd");
	?>
	<td><input type="text" name="curso" class="txtc"   value="<?php echo $lper[0]['periodo']?>" readonly="">
<input type="hidden" name="codp" value="<?php echo $_GET['periodo'];?>">

	</td>
</tr>
<tr>
	<td class="headertd">Curso</td>
	<td><input type="text" name="curso" class="txtl" value="<?php echo $cursodatos[0]['area']?>" readonly="">
<input type="hidden" name="gseccion" value="<?php echo $_GET['gseccion'];?>">
<input type="hidden" name="codcurso" value="<?php echo $_GET['curso'];?>">
<input type="hidden" name="nivel" value="<?php echo $nvie?>">
	</td>
</tr>

<tr>
	<td class="headertd">Docente</td>
	<td>

<input type="text" name="tt" id="tt" class="txtl" readonly="" >
		<input type="hidden" name="codtt" id="codtt" class="txtl"  >
<input type="button" value="..." onclick="block_page_blank('boxdocente.php','Docetes',500,300)">
	</td>
</tr>


<tr>
	<td class="headertd">Num. Horas</td>
	<td><input type="text" name="nhoras" id="horas" class="txtc"  onblur="calcularduracion('hfin');"></td>
</tr>

<tr>
	<td class="headertd">Hora Inicio</td>
	<td><input type="text" name="hini" id="hini" class="txtc" onblur="calcularduracion('hfin');"></td>
</tr>

<tr>
	<td class="headertd">Hora Fin</td>
	<td><input type="text" name="hfin" id="hfin" class="txtc"  onclick="calcularduracion('hfin');" ></td>
</tr>

<tr>
	<td class="headertd" colspan="2">Seleccionar Dias</td>
</tr>
<tr>
	<td colspan="2">
    Lunes <input type="checkbox" name="dial" value="l">
    Martes <input type="checkbox" name="diam" value="m">
    Miercoles <input type="checkbox" name="diami" value="mi">
    Jueves <input type="checkbox" name="diaj" value="j">
    Viernes <input type="checkbox" name="diav" value="v">
    Sabado <input type="checkbox" name="dias"  value="s">
    Domingo <input type="checkbox" name="diad" value="d">
	</td>
</tr>




<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Grabar" name="asignar">
		 
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('asignar_curso.php')">
	</td>
</tr>

</table>



</form>

	</div>
</div>

</body>
</html>


<?php
}
?>