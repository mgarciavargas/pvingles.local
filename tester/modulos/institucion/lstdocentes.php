<?php
session_start();
if($_POST['envio']=="docentes"){
include_once('function/function_all.php');
$obj= new cl_all_functions();
?>
<!DOCTYPE html>
<html>
<head>
	<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
</head>
<body style="margin:0px;">
   <div class="cabtb">Listado de Docentes
   <div class="closelight" alt="Cerrar" title="Cerrar" onclick="xlight()">x</div>
   </div>
	<table align="center" cellpadding="0" cellspacing="0" width="480">
		<tr>
			<th>N.</th>
			<th>Nombres</th>
			<th>Tipo</th>
			<th>Activo</th>
		</tr>
		<?php 
		$lista=$obj->select_assoc("select p.idpersonal,p.idtipopersonal,concat(p.nombres,' ',p.ap_paterno,' ',p.ap_materno) as nombres,p.estado,t.tipopersonal from personal p inner join tipo_personal t on p.idtipopersonal=t.idtipopersonal where t.tipopersonal like 'Docente%' ");
		for($i=0;$i<count($lista);$i++){
		?>
		<tr style="cursor:pointer;" class="listado" onclick="cargar_escalafon('<?php echo $lista[$i]['nombres']?>','<?php echo $lista[$i]['idpersonal']?>')">
		<td><?php echo $i+1?></td>
		<td><?php echo $lista[$i]['nombres']?></td>
		<td><?php echo $lista[$i]['tipopersonal']?></td>
		<td><?php 
		if($lista[$i]['estado']==1)
		{
			?>
			<img src="images/si.png">
			<?php
		}else
		{
			?>
			<img src="images/no.png">
			<?php
		}
		?></td>
		</tr>
		<?php 
		}?>
	</table>
</body>
</html>
<?php
}
?>