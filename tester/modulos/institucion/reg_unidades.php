<?php
session_start();
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
include_once('function/function_all.php');
$obj= new cl_all_functions();
$institucion=$_SESSION['institucion'];
$sqlc=$obj->select_assoc("select * from institucion where id_institucion=$institucion");
$nivel=$sqlc[0]['idnivel'];
$pdo=$_GET['periodo'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Listado de Unidades</title>
	<meta charset="utf-8">
	<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">


<link type="text/css" href="calendario/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="calendario/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.core.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker-es.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker.js"></script>
		<link type="text/css" href="calendario/demos.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function() {
				$("#fini").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
				$("#ffin").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});

	


		</script>


</head>
<body >
<form action="actions_unidades.php" method="post">
<table align="center" style="background-color:#fff;">
<tr>
	<th colspan="2">Registrar Unidad</th>
	<input type="hidden" name="periodo" value="<?php echo $pdo?>">
</tr>
<tr>
	<td>Unidad</td>
	<td><input type="text" class="txtc" name="uni"></td>
</tr>
<tr>
	<td>Fecha Inicio</td>
	<td><input type="text" class="txtm" name="fini" id="fini"></td>
</tr>
<tr>
	<td>Fecha Fin</td>
	<td><input type="text" class="txtm" name="ffin" id="ffin"></td>
</tr>
<tr>
	<td colspan="2">
		<input type="submit" value="Grabar" name="unidad" class="bts">
		<input type="reset" value="Cancelar" class="btr">
	</td>
</tr>
</table>
</form>
</body>
</html>