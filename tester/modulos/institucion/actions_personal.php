<?php
session_start();
date_default_timezone_set('America/Lima');
$opcion=$_POST['personal'];
if($opcion!="")
{
include_once('function/function_all.php');
$obj= new cl_all_functions();
if($opcion=="Grabar")
{
$tpd=$_POST['tpd'];
$app=strtoupper($_POST['app']);
$apm=strtoupper($_POST['apm']);
$nom=$_POST['nom'];
$foto=$_FILES['foto']['name'];
$fnaci=$_POST['fnaci'];
$sex=$_POST['sex'];
$dni=$_POST['dni'];
$depa=$_POST['depa'];
$prov=$_POST['prov'];
$dist=$_POST['dist'];
$dir=$_POST['dir'];
$mail=$_POST['mail'];
$usu=$_POST['usu'];
$pass=$_POST['pass'];
$tel=$_POST['tel'];
$est=1;
$freg=date('Y-m-d');
$obs=$_POST['obs'];
$institucion=$_SESSION['institucion'];
if($app!="" & $apm!="" & $nom!="" & $dni!="" & $dir!="" & $tpd!="" )
{
	if(is_uploaded_file($_FILES['foto']['tmp_name']))
	{
		$cpt="personal";
		$tipo=$_FILES['foto']['type'];
		@copy($_FILES['foto']['tmp_name'],"personal/".$foto);
		if($tipo=="image/png")
		{
			$ext=".png";
		}
		if($tipo=="image/jpeg")
		{
			$ext=".jpg";
		}
		if($tipo=="image/gif")
		{
			$ext=".gif";
		}
		$old="personal/".$foto;
		$new="personal/".$app."-".$apm."-".$nom.$ext;
		$factual=$app."-".$apm."-".$nom.$ext;
		rename($old,$new);

		if($tipo=="image/png")
		{
   $creathumb=$obj->transforma_png($factual,$cpt);
		}
		if($tipo=="image/jpeg")
		{
   $creathumb=$obj->transforma_jpg($factual,$cpt);
		}
		if($tipo=="image/gif")
		{
$creathumb=$obj->transforma_gif($factual,$cpt);
		}

		$agrega=$obj->querys("insert into personal values (null,$institucion,$tpd,'$nom','$app','$apm','$dni','$mail','$tel','$sex',$depa,$prov,$dist,'$dir','$fnaci','$factual','$usu','$pass','$obs','$freg','$est')");
		if($agrega==1)
		{
			unlink($new);
			$_SESSION['mokpersonal']="Registrado Correctamente";
			echo '<meta http-equiv="refresh" content="0; url=personal.php">';

		}
		else
		{
			$_SESSION['merrorpersonal']="Error no se ha podido Grabar los Datos ";
			echo '<meta http-equiv="refresh" content="0; url=personal.php">';
		}



	}
}
else
{
	$_SESSION['merrorpersonal']="Error no se ha podido Grabar los Datos ";
			echo '<meta http-equiv="refresh" content="0; url=personal.php">';
}
}



if($opcion=="Actualizar")
{
$docente=$_POST['docenteident'];
$tpd=$_POST['tpd'];
$app=strtoupper($_POST['app']);
$apm=strtoupper($_POST['apm']);
$nom=$_POST['nom'];
$foto=$_FILES['foto']['name'];
$fant=$_POST['fant'];
$fnaci=$_POST['fnaci'];
$sex=$_POST['sex'];
$dni=$_POST['dni'];
$depa=$_POST['depa'];
$prov=$_POST['prov'];
$dist=$_POST['dist'];
echo $dir=$_POST['dir'];
$mail=$_POST['mail'];
$usu=$_POST['usu'];
$pass=$_POST['pass'];
$tel=$_POST['tel'];
$est=1;
$freg=date('Y-m-d');
$obs=$_POST['obs'];
$institucion=$_SESSION['institucion'];
if($docente!="" & $app!="" & $apm!="" & $nom!="" & $dni!="" &  $tpd!="" )
{

	if($foto==""){
		
        $actualizar=$obj->querys("update personal set direccion='$dir',idtipopersonal=$tpd,nombres='$nom',ap_paterno='$app',ap_materno='$apm',dni_num='$dni',correo='$mail',telefono='$tel',sexo='$sex',id_Depa=$depa,id_Prov=$prov,id_Dist=$dist,f_naci='$fnaci',fregistro='$freg',usuario='$usu',password='$pass',descripcion='$obs' where idpersonal=$docente");
		if($actualizar==1)
		{
			
			$_SESSION['mokpersonal']="Datos Actualizados Correctamente";
			echo '<meta http-equiv="refresh" content="0; url=personal.php">';

		}
		else
		{
			$_SESSION['merrorpersonal']="Error no se ha podido Actualizar los Datos";
	echo '<meta http-equiv="refresh" content="0; url=personal.php">';
		}
	}else{
	if(is_uploaded_file($_FILES['foto']['tmp_name']))
	{
		unlink("personal/thumbs/".$fant);
		$cpt="personal";
		$tipo=$_FILES['foto']['type'];
		@copy($_FILES['foto']['tmp_name'],"personal/".$foto);
		if($tipo=="image/png")
		{
			$ext=".png";
		}
		if($tipo=="image/jpeg")
		{
			$ext=".jpg";
		}
		if($tipo=="image/gif")
		{
			$ext=".gif";
		}
		$old="personal/".$foto;
		$new="personal/".$app."-".$apm."-".$nom.$ext;
		$factual=$app."-".$apm."-".$nom.$ext;
		rename($old,$new);

		if($tipo=="image/png")
		{
   $creathumb=$obj->transforma_png($factual,$cpt);
		}
		if($tipo=="image/jpeg")
		{
   $creathumb=$obj->transforma_jpg($factual,$cpt);
		}
		if($tipo=="image/gif")
		{
$creathumb=$obj->transforma_gif($factual,$cpt);
		}

		$actualizar=$obj->querys("update personal set direccion='$dir',idtipopersonal=$tpd,nombres='$nom',ap_paterno='$app',ap_materno='$apm',foto='$factual',dni_num='$dni',correo='$mail',telefono='$tel',sexo='$sex',id_Depa=$depa,id_Prov=$prov,id_Dist=$dist,f_naci='$fnaci',fregistro='$freg',usuario='$usu',password='$pass',descripcion='$obs' where idpersonal=$docente");
		if($actualizar==1)
		{
			unlink($new);
			$_SESSION['mokpersonal']="Datos Actualizados Correctamente";
			echo '<meta http-equiv="refresh" content="0; url=personal.php">';

		}
		else
		{
			$_SESSION['merrorpersonal']="Error no se ha podido Actualizar los Datos";
			echo '<meta http-equiv="refresh" content="0; url=personal.php">';
		}



	}
}
}
else
{
$_SESSION['merrorpersonal']="Error no se ha podido Actualizar los Datos";
echo '<meta http-equiv="refresh" content="0; url=personal.php">';
}
}




}


?>