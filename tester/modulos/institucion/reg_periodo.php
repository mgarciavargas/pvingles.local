<?php
session_start();
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
date_default_timezone_set('America/Lima');
	$institucion=$_SESSION['institucion'];
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset="utf-8">
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>

<link type="text/css" href="calendario/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="calendario/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.core.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker-es.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker.js"></script>
		<link type="text/css" href="calendario/demos.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function() {
				$("#fini").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
				$("#ffin").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});

	


		</script>


</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_periodo.php" method="post" enctype="multipart/form-data">
		<table align="center">
<tr>
	<th colspan="4">Registro de Periodo Escolar</th>
</tr>
<tr>
	<td class="headertd">Año</td>
	<td>			
		<input type="text" class="txtc" name="periodoesc" value="<?php echo date('Y')?>" >
	</td>
	<td class="headertd">Descripci&oacute;n</td>
	<td><input type="text" name="des" class="txtl"></td>
</tr>
<tr>
	<td class="headertd">Fecha de Inicio</td>
	<td><input type="text" name="fini" id="fini" class="txtm"></td>
	<td class="headertd">Fecha Fin</td>
	<td><input type="text" name="ffin" id="ffin" class="txtm"></td>
</tr>
 

 


<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Grabar" name="periodo">
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('periodo_escolar.php')">
	</td>
</tr>

</table>
</form>

	</div>
</div>

</body>
</html>


<?php
}
?>