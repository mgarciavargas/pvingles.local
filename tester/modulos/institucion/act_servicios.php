<?php
session_start();
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
	$institucion=$_SESSION['institucion'];
	if($_GET['servicio']=="" || !(is_numeric($_GET['servicio'])))
	{
echo '<meta http-equiv="refresh" content="0; url=servicios.php">';
	}
	else
	{
		$servi=$_GET['servicio'];
		//echo "select * from servicio where idservicio=$servicio and idinstitucion=$institucion";
		$datos=$obj->select_assoc("select * from servicio where idservicio=$servicio and idinstitucion=$institucion");
		if(count($datos)==0)
		{
echo '<meta http-equiv="refresh" content="0; url=servicios.php">';
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>
</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_servicios.php" method="post" enctype="multipart/form-data">
		<table align="center" cellspacing="0" cellpadding="0">
<tr>
	<th colspan="4">Actualizar Servicios</th>
</tr>
<tr>
	<td class="headertd">Tipo Ambiente</td>
	<td colspan="3">			
<select name="serv">
<option>Seleccionar</option>
<?php
$sv=$datos[0]['servicio'];
if($sv=="Agua")
{
$ag="selected=''";
}
else if($sv=="Luz")
{
$lz="selected=''";
}
else if($sv=="Internet")
{
$it="selected=''";
}
else if($sv=="Telefono")
{
$tf="selected=''";
}
else if($sv=="Otro")
{
$ot="selected=''";
}
?>
	<option value="Agua" <?php echo $ag?> >Agua</option>
	<option value="Luz" <?php echo $lz?> >Luz</option>
	<option value="Internet" <?php echo $it?> >Internet</option>
	<option value="Telefono" <?php echo $tf?> >Telefono</option>
	<option value="Otro" <?php echo $ot?> >Otro</option>
</select>
<input type="hidden" name="servid" value="<?php echo $datos[0]['idservicio']?>">
	</td>
</tr>
<tr>
	<td class="headertd">Descripci&oacute;n</td>
	<td colspan="3"><input type="text" name="desc" class="txtl" value="<?php echo $datos[0]['descripcion']?>"></td>
	
</tr>
<tr>
	<td class="headertd">Proveedor</td>
	<td colspan="3"><input type="text" name="prov" class="txtl" value="<?php echo $datos[0]['proveedor']?>"></td>
	
</tr>

<tr>
	<td class="headertd">Monto</td>
	<td colspan="3"><input type="text" name="monto" class="txtm" value="<?php echo $datos[0]['monto_servicio']?>"></td>
</tr>



<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Actualizar" name="servicios">
		<input class="btb" type="button" value="salir" onclick="select_item('servicios.php')">
	</td>
</tr>

</table>



</form>

	</div>
</div>

</body>
</html>


<?php
}
?>