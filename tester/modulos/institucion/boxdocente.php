<?php
session_start();
include_once('function/function_all.php');
$obj= new cl_all_functions();
$nom=$_POST['nom'];
$institucion=$_SESSION['institucion'];
if($nom!="")
{
$add=" and concat(p.nombres,'',p.ap_paterno,'',p.ap_materno) like '%$nom%' ";
}
?>
<!DOCTYPE html>
<html>
<head>
<TITLE>Listado de Docentes</TITLE>
	<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
	<script type="text/javascript">
function cargar_tutor(ntutor,codtt)
{
  window.opener.document.getElementById('tt').value=ntutor
  window.opener.document.getElementById('codtt').value=codtt;
  window.close();
}
	</script>
</head>
<body style="margin:0px;">
<form action="" method="post">
    <table cellpadding="0" cellspacing="0" align="center">
    	<tr>
    		<td>Ingrese Nombres</td>
    		<td><input type="text" name="nom" class="txtl" value="<?php echo $nom?>"></td>
    		<td><input type="submit" value="Buscar" name="buscad"></td>
    	</tr>
    </table>
    </form>
	<table align="center" cellpadding="0" cellspacing="0" width="480">
		<tr>
			<th>N.</th>
			<th>Nombres</th>
			<th>Tipo</th>
			<th>Activo</th>
		</tr>
		<?php 
		$lista=$obj->select_assoc("select p.idpersonal,p.idtipopersonal,concat(p.nombres,' ',p.ap_paterno,' ',p.ap_materno) as nombres,p.estado,t.tipopersonal from personal p inner join tipo_personal t on p.idtipopersonal=t.idtipopersonal where p.idinstitucion=$institucion and t.tipopersonal like 'Docente%'".$add);
		for($i=0;$i<count($lista);$i++){
		?>
		<tr style="cursor:pointer;" class="listado" onclick="cargar_tutor('<?php echo $lista[$i]['nombres']?>','<?php echo $lista[$i]['idpersonal']?>')">
		<td><?php echo $i+1?></td>
		<td><?php echo $lista[$i]['nombres']?></td>
		<td><?php echo $lista[$i]['tipopersonal']?></td>
		<td><?php 
		if($lista[$i]['estado']==1)
		{
			?>
			<img src="images/si.png">
			<?php
		}else
		{
			?>
			<img src="images/no.png">
			<?php
		}
		?></td>
		</tr>
		<?php 
		}?>
	</table>
</body>
</html>
<?php

?>