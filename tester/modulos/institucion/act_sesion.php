<?php
session_start();
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
include_once('function/function_all.php');
$obj= new cl_all_functions();
$institucion=$_SESSION['institucion'];
$lcole=$obj->select_assoc("select * from institucion where id_institucion=$institucion");
$nivelie=$lcole[0]['idnivel'];
$pd=$_GET['pd'];
if(isset($_GET['sesion']))
{
	if($_GET['sesion']!="" & is_numeric($_GET['sesion']))
	{
		$ss=$_GET['sesion'];
 $sss=$obj->select_assoc("select * from sesion where idsesion=$ss");
 $idc=$sss[0]['idmgn'];
 $curso=$obj->select_assoc("select * from malla_grado_nivel where idmgn=$idc");
$iduni=$sss[0]['idunidad'];
$g=$sss[0]['idgrado'];
$grad=$obj->select_assoc("select * from grado_nivel where idgrado=$g");
	}
	else
	{
		echo '<meta http-equiv="refresh" content="0; url=sesiones.php">';
	}
}
else
{
	echo '<meta http-equiv="refresh" content="0; url=sesiones.php">';
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Sesiones</title>
	<meta charset="utf-8">
	<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>
</head>
<body >
<form action="actions_sesiones.php" method="post" enctype="multipart/form-data">
<table align="center" style="background-color:#fff;width:400px;">
	<tr>
			<th colspan="2">Actualizar Datos de la Sesion</th>
	</tr>
	<tr>
		<td align="center">Curso</td>
		<td>
			<input type="text" name="nc"  class="txtl" value="<?php echo $curso[0]['area']?>" readonly="">
			<input type="hidden" name="cursocod" value="<?php echo $idc?>">
			<input type="hidden" name="pd" value="<?php echo $pd?>">
			<input type="hidden" name="codses" value="<?php echo $ss?>">
		</td>
	</tr>

	<tr>
		<td align="center">Unidad</td>
		<td>
		<select name="unidad">
				<option value="">Seleccionar</option>
				<?php
				$unid=$obj->select_assoc("select * from unidad where idinstitucion=$institucion and idnivel=$nivelie and idperiodo=$pd");
				for ($i=0; $i <count($unid) ; $i++) { 
					if($iduni==$unid[$i]['idunidad'])
			{
				$slu="selected=''";
			}
			else
			{
				$slu="";
			}
					echo '<option '.$slu.' value="'.$unid[$i]['idunidad'].'">Unidad'.$unid[$i]['numunidad'].'</option>';
				}
				?>
			</select>
		</td>
	</tr>

<tr>
	<td align="center">Grado</td>
	<td>
		<input type="text" name="gd" class="txtc" value="<?php echo $grad[0]['grado']?>" readonly="">
		<input type="hidden" name="grado" value="<?php echo $grad[0]['idgrado']?>">
	</td>
</tr>

<tr>
	<td align="center">Titulo</td>
	<td>
		<input type="text" name="titulo" class="txtl" value="<?php echo $sss[0]['titulo'];?>">
	</td>
</tr>

<tr>
	<td align="center">Archivo</td>
	<td>
		<?php
if($sss[0]['archivo']!="")
{
?>
<label style="color:#2264FF;"><b>Archivo Actual</b>:<?php echo "  ".$sss[0]['archivo']?></label>
<input type="text" name="ftbefore" value="<?php echo $sss[0]['archivo']?>">
<?php
}
		?>
		<input type="file" name="archivo" style="width:220px;border:1px dotted green;">
	</td>
</tr>

<tr>
	<td align="center" colspan="2">
<input type="submit" value="Actualizar" class="bts" name="sesion">
<input type="button" value="Cancelar" onclick="window.location='sesiones.php?periodo=<?php echo $pd?>&grado=<?php echo $g?>'" class="btr">
	</td>
	
</tr>

</table>
</form>
</body>
</html>