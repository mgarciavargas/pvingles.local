<?php
session_start();
if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
include_once('function/function_all.php');
$obj= new cl_all_functions();
$institucion=$_SESSION['institucion'];
$lcole=$obj->select_assoc("select * from institucion where id_institucion=$institucion");
$nivelie=$lcole[0]['idnivel'];
$lareas=$obj->select_assoc("select * from malla_grado_nivel where idnivel=$nivelie and idmalla=1");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Listado de Cursos</title>
	<meta charset="utf-8">
	<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>
</head>
<body >
<table style="background-color:#fff;" align="center" width="400">
<tr>
	<th colspan="3">Lista de Cursos</th>
</tr>
<tr>
	<th align="center" width="70">Nº</th>
	<th align="center" width="300">Curso</th>
	<th>Sesiones</th>
</tr>
<?php
for($i=0;$i<count($lareas);$i++){
?>
<tr>
<td align="center"><?php echo $i+1?></td>
<td align="center"><?php echo utf8_encode($lareas[$i]['area'])?></td>
<td>
<a href="sesiones.php?grado=<?php echo $_GET['grado']?>" >Sesiones</a>
</td>
</tr>
<?php
}
?>
</table>
</body>
</html>
