<?php
session_start();

if($_SESSION['personal']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
include_once('function/function_all.php');
$obj= new cl_all_functions();
$institucion=$_SESSION['institucion'];
}

if($_GET['alumno']!="" || is_numeric($_GET['alumno']))
{
$codal=$_GET['alumno'];
$datos=$obj->select_assoc("select * from alumno where idalumno=$codal");
}
else
{
	echo '<meta http-equiv="refresh" content="0; url=alumnos.php">';
}

?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>


	<link type="text/css" href="calendario/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="calendario/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.core.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker-es.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker.js"></script>
		<link type="text/css" href="calendario/demos.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function() {
				$("#fnaci").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});

	


		</script>


</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">

 

		<form action="actions_alumnos.php" method="post" enctype="multipart/form-data">
		<table align="center" cellspacing="0" cellpadding="0">
<tr>
	<th colspan="4">Actualizar Datos  de Alumno</th>
</tr>
<tr>
	<td class="headertd">Apellido Paterno</td>
	<td>
	<input type="text" name="app" class="txtl" value="<?php echo $datos[0]['ap_paterno']?>">
	<input type="hidden" name="cal" value="<?php echo $datos[0]['idalumno']?>">
	</td>
	<td class="headertd">Apellido Materno</td>
	<td><input type="text" name="apm" class="txtl" value="<?php echo $datos[0]['ap_materno']?>"></td>
</tr>
<tr>
	<td class="headertd">Nombres</td>
	<td><input type="text" name="nom" class="txtl" value="<?php echo $datos[0]['nombres']?>"></td>
	<td class="headertd">Fecha Nacimiento</td>
	<td><input type="text" name="fnaci" id="fnaci" class="txtm" value="<?php echo $datos[0]['fecha_naci']?>"></td>
</tr>
<tr>
	<td class="headertd">Sexo</td>
	<td>
		<select name="sex">
		<?php
if($datos[0]['sexo']=="m")
{
$sm="selected=''";
}
else if($datos[0]['sexo']=="f")
{
$sf="selected=''";
}
 
		?>
			<option <?php echo $sm?> value="m">Masculino</option>
			<option <?php echo $sf?> value="f">Femenino</option>
		</select>
	</td>
	<td class="headertd">Num. DNI</td>
	<td><input type="text" name="dni" maxlength="8" class="txtm" value="<?php echo $datos[0]['num_dni']?>"></td>
</tr>
<tr>
	<td class="headertd">Direccion</td>
	<td><input type="text" name="dir"  class="txtl" value="<?php echo $datos[0]['direccion']?>"></td>
	<td class="headertd">Email</td>
	<td><input type="text" name="mail"  class="txtl" value="<?php echo $datos[0]['email']?>"></td>
</tr>
<tr>
	<td class="headertd">Telefono</td>
	<td><input type="text" name="tel"  class="txtl" value="<?php echo $datos[0]['telefono']?>"></td>
    <td class="headertd"></td>
	<td></td>
</tr>
<tr>
<td class="headertd">Foto</td>
	<td>
	<input type="hidden" name="fant" value="<?php echo $datos[0]['foto']?>">
<input type="file" class="filefoto"  id="foto" name="foto"  title="Clic para agregar una Foto">
	<div class="ftbox" title="Clic para agregar una Foto" id="lfoto">	

	<?php
	if($datos[0]['foto']!="")
	{?>
		<img src="alumnos/thumbs/<?php echo $datos[0]['foto']?>" style='width:100px;height:100px;'>
   <?php
}
	?>

		</div>
		
   <output id="lists"></output>
		<script>
              function archivo(evt) {
                  var files = evt.target.files; // FileList object
             
                  // Obtenemos la imagen del campo "file".
                  for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }
             
                    var reader = new FileReader();
             
                    reader.onload = (function(theFile) {
                        return function(e) {
                          // Insertamos la imagen
                         document.getElementById("lfoto").innerHTML = ['<img class="thumb" style="width:100px;height:100px;" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                        };
                    })(f);
             
                    reader.readAsDataURL(f);
                  }
              }
             
              document.getElementById('foto').addEventListener('change', archivo, false);
      </script>

	</td>
			
	<td class="headertd"></td>
	<td></td>
	
</tr>

<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Actualizar" name="alumno">
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('alumnos.php')">
	</td>
</tr>

</table>
</form>
 

	</div>
</div>

</body>
</html>
 

