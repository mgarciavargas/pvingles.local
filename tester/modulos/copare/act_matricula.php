<?php
session_start();
if($_SESSION['copareusuario']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
		include_once('function/function_all.php');
   $obj= new cl_all_functions();
	$copareusu=$_SESSION['copareusuario'];

	if($_GET['matricula']=="" || !(is_numeric($_GET['matricula'])))
	{
		echo '<meta http-equiv="refresh" content="0; url=matriculas.php">';
	}
	else
	{
		$mtl=$_GET['matricula'];
     $verifik=$obj->select_assoc("select * from matricula where idmatricula=$mtl");
     $institucion=$verifik[0]['idinstitucion'];
     $col=$obj->select_assoc("select id_institucion,razon_social from institucion where id_institucion=$institucion");
     if(count($verifik)==0)
     {
echo '<meta http-equiv="refresh" content="0; url=matriculas.php">';
     }
     else
     {

     }
	}
	?>

<!DOCTYPE html>
<html>
<head>
<title></title>
 
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script src="script/function_select.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
 

</head>
<body>
<div id="boxlist">
</div>
<div id="bloqp"></div>
<?php include('perfil.php');?>



<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="actions_matricula.php" method="post" enctype="multipart/form-data">
		<table align="center">
<tr>
	<th colspan="4">Actualizar Matr&iacute;cula</th>
</tr>

<tr>
<td class="headertd">Institucion Educativa</td>
<td colspan="">
<input type="text" readonly="" value="<?php echo $col[0]['razon_social']?>">
<input type="hidden" name="instituciones" value="<?php echo $institucion?>">
</td>
</tr>

<tr>
	<td class="headertd">Periodo</td>
	<td colspan="3">
		<select name="periodo" readonly="">
			<?php
$pesc=$obj->select_assoc("select * from periodo_escolar where idinstitucion=$institucion and estado='a' ");
			?>
			<option value="<?php echo $pesc[0]['idperiodo']?>"><?php echo $pesc[0]['periodo']?></option>
	</select>
</td>
</tr>

<tr>
	<td class="headertd">Alumno</td>
	<td colspan="3">
<?php
$cal=$verifik[0]['idalumno'];

$alumno=$obj->select_assoc("select idalumno,nombres,ap_paterno,ap_materno from alumno where idalumno=$cal");
?>
	<input type="text" name="alumno" id="alumno" class="txtl" disabled="" value="<?php echo $alumno[0]['nombres'].' '.$alumno[0]['ap_paterno'].' '.$alumno[0]['ap_materno']?>">
	 
	<input type="hidden" name="codalumn" id="codalumn" value="<?php echo $alumno[0]['idalumno']?>">
	</td>
</tr>
<tr>
	<td class="headertd">Situaci&oacute;n</td>
	<td colspan="3" >
	<?php
if($verifik[0]['situacion']=="Reciente")
{
	$rc="selected=''";
}
if($verifik[0]['situacion']=="Promovido")
{
	$pm="selected=''";
}
if($verifik[0]['situacion']=="Repetir")
{
	$rp="selected=''";
}
	?>
		<select name="st">
			<option <?php echo $rc?> value="Reciente">Nuevo</option>
			<option <?php echo $pm?> value="Promovido">Promovido</option>
			<option <?php echo $rp?> value="Repetir">Repetir</option>
		</select>
	</td>
</tr>

<tr>
	<td class="headertd">Grado</td>
	<td>
		<?php

   	$nivelie=$obj->select_assoc("select idnivel from institucion where id_institucion=$institucion");
   	$codnv=$nivelie[0]['idnivel'];
   	$gscc=$verifik[0]['idgradoseccion'];
   	$obtenergrado=$obj->select_assoc("select * from grado_seccion where idgseccion=$gscc");
   	$sgrado=$obj->select_assoc("select * from grado_nivel where  idnivel=$codnv");
		?>
   <select name="grado" onchange="selects_ub('secc',this.value,'seccid')">
   	<option value="">Seleccionar</option>
   	<?php

   	for($i=0;$i<count($sgrado);$i++)
   	{
   		if($sgrado[$i]['idgrado']==$obtenergrado[0]['idgrado'])
   		{
$slg="selected=''";
   		}
   		else
   		{
   			$slg="";
   		}
?>
<option <?php echo $slg?> value="<?php echo $sgrado[$i]['idgrado']?>"><?php echo $sgrado[$i]['grado']?></option>
<?php
   	}
   	?>
   </select>
	</td>
	<td class="headertd">Secci&oacute;n</td>
	<td id="seccid">
<select name="seccion">
	<option value="">Seleccionar</option>
	<?php
	$cgrd=$obtenergrado[0]['idgrado'];
$consultasc=$obj->select_assoc("select * from grado_seccion where idgrado=$cgrd");
for($i=0;$i<count($consultasc);$i++)
{
	if($consultasc[$i]['idgseccion']==$obtenergrado[0]['idgseccion'])
	{
$sls="selected=''";
	}
	else
	{
		$sls="";
	}
?>
<option <?php echo $sls?> value="<?php echo $consultasc[$i]['idgseccion']?>"><?php echo $consultasc[$i]['seccion']?></option>
<?php
}

	?>
</select>
	</td>
</tr>

<tr>
	<td class="headertd">Procedencia</td>
	<td colspan="3">
		<select name="procede">
		<?php
if($verifik[0]['procedencia']=="misma")
{
$msm="selected=''";
}
if($verifik[0]['procedencia']=="traslado")
{
$trl="selected=''";	
}
		?>
			<option <?php echo $msm?> value="misma">Misma I.E</option>
			<option <?php echo $trl?> value="traslado">Traslado</option>
		</select>
	</td>
	
</tr>

<tr>
	<td class="headertd">Obervaci&oacute;n</td>
	<td colspan="3">
		<textarea name="obs"><?php echo $verifik[0]['observacion']?></textarea>
	</td>
	
</tr>

<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Actualizar" name="matricula">
		<input type="hidden" value="<?php echo $verifik[0]['idmatricula']?>" name="codmatricula">
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('matriculas.php')">
	</td>
</tr>

</table>
</form>

	</div>
</div>

</body>
</html>


<?php
}
?>