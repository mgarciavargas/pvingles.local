<?php
session_start();
date_default_timezone_set('America/Lima');
if($_SESSION['copareusuario']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
		$copareusu=$_SESSION['copareusuario'];
 
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>

	<link type="text/css" href="calendario/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="calendario/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.core.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker-es.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker.js"></script>
		<link type="text/css" href="calendario/demos.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function() {
				$("#fecha").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});

	


		</script>


</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="" name="fload" method="post" enctype="multipart/form-data">
		<table align="center" cellspacing="0" cellpadding="0">
<tr>
	<th colspan="4">Registrar Pago </th>
</tr>

<tr>
	<td class="headertd">Institucion Educativa</td>
		<td>
			<input type="text" name="ncole" id="ncole" class="txtl"  onchange="alert(this.value);" readonly="" value="<?php echo $_POST['ncole']?>">
			<input type="hidden" name="instituciones" id="instituciones" value="<?php echo $_POST['instituciones']?>" >
			<input type="button" name="btng" value="..." onchange="alert(this.value);" onclick="block_page_blank('load_instituciones.php','Instituciones Educativas',750,450)">
		</td>
</tr>

<tr>
	<td class="headertd">Fecha</td>
	<td  ><input type="text" name="fecha" id="fecha" value="<?php echo date('Y-m-d')?>" class="txtc"></td>
</tr>
<tr>
	<td class="headertd">Concepto</td>
	<td  ><input type="text" name="concepto" class="txtl"></td>
</tr>
<tr>
	 
	<td class="headertd">Monto</td>
	<td><input type="text" name="monto" class="txtc"></td>
</tr>
<tr>
	<td class="headertd">APAFA</td>
	<td id="lstapafas">
<?php
if($_POST['instituciones']!="")
{
	$iev=$_POST['instituciones'];
	$sqld=$obj->select_assoc("select * from apafa where idinstitucion=$iev and estado=1");
	$periodoapafa=$obj->select_assoc("select * from periodo_escolar where idinstitucion=".$sqld[0]['idinstitucion']." and estado='a'");
 
?>
<select name="apafa" id="apafa" >
				 
				<?php
				
				for($i=0;$i<count($sqld);$i++)
				{
					if($_POST['apafa']==$sqld[$i]['idapafa'])
					{
					$sl="selected=''";
					}
					else
					{
						$sl="";
					}
					?>
					<option <?php echo $sl;?> value="<?php echo $sqld[$i]['idapafa']?>">Apafa<?php echo " ".$periodoapafa[0]['periodo']?></option>
					<?php
				}
				?>
			</select>
<?php
}else
{
	?>
<select disabled="">
<option>Seleccionar</option>
</select>
	<?php
}
?>
	</td>	 
</tr>

<tr>
	<td class="headertd">Observacion</td>
	<td ><textarea name="obs" style="width:200px;height:60px;"></textarea></td>
</tr>



<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Grabar" name="pagos" onclick="this.form.action='actions_pagos.php'">
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('pagos.php')">
	</td>
</tr>

</table>



</form>

	</div>
</div>

</body>
</html>


<?php
}
?>