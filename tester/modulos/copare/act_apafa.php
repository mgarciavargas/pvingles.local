<?php
session_start();
if($_SESSION['copareusuario']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
	$copareusu=$_SESSION['copareusuario'];
 

if($_GET['apafa']!="" & is_numeric($_GET['apafa']))
{
	$ap=$_GET['apafa'];
$datos=$obj->select_assoc("select * from apafa where idapafa=$ap");
$codie=$datos[0]['idinstitucion'];
$institucion=$obj->select_assoc("select id_institucion,razon_social from institucion where id_institucion=$codie");
$nomie=$institucion[0]['razon_social'];

$codp=$datos[0]['nom_presidente'];
$codvp=$datos[0]['nom_vpresidente'];
$cods=$datos[0]['nom_secretario'];
$codt=$datos[0]['nom_tesorero'];

$tp=$datos[0]['tpresidente'];
$tvp=$datos[0]['tvpresidente'];
$ts=$datos[0]['tsecretario'];
$tt=$datos[0]['ttesorero'];

$presidente=$obj->select_assoc("select * from padres where idpadres=$codp");
$vpresidente=$obj->select_assoc("select * from padres where idpadres=$codvp");
$secretario=$obj->select_assoc("select * from padres where idpadres=$cods");
$tesorero=$obj->select_assoc("select * from padres where idpadres=$codt");

if($datos[0]['tpresidente']=="padre")
{
$nomp=$presidente[0]['nompadre']." ".$presidente[0]['ap_paterno_p']." ".$presidente[0]['ap_materno_p'];
}
else
{
$nomp=$presidente[0]['nommadre']." ".$presidente[0]['ap_paterno_m']." ".$presidente[0]['ap_materno_m'];
}

if($datos[0]['tvpresidente']=="padre")
{
$nomvp=$vpresidente[0]['nompadre']." ".$vpresidente[0]['ap_paterno_p']." ".$vpresidente[0]['ap_materno_p'];
}
else
{
$nomvp=$vpresidente[0]['nommadre']." ".$vpresidente[0]['ap_paterno_m']." ".$vpresidente[0]['ap_materno_m'];
}

if($datos[0]['tsecretario']=="padre")
{
$noms=$secretario[0]['nompadre']." ".$secretario[0]['ap_paterno_p']." ".$secretario[0]['ap_materno_p'];
}
else
{
$noms=$secretario[0]['nommadre']." ".$secretario[0]['ap_paterno_m']." ".$secretario[0]['ap_materno_m'];
}

if($datos[0]['ttesorero']=="padre")
{
$nomt=$tesorero[0]['nompadre']." ".$tesorero[0]['ap_paterno_p']." ".$tesorero[0]['ap_materno_p'];
}
else
{
$nomt=$tesorero[0]['nommadre']." ".$tesorero[0]['ap_paterno_m']." ".$tesorero[0]['ap_materno_m'];
}



}
else
{
echo '<meta http-equiv="refresh" content="0; url=apafa.php">';
}
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>
</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">
		<form action="" name="fload" method="post" enctype="multipart/form-data">
		<table align="center" cellspacing="0" cellpadding="0">
<tr>
	<th colspan="8">Actualizar Datos de la Apafa</th>
</tr>
<tr>
	<td  class="headertd">I. Educativa</td>
	<td>
		<input type="hidden" name="instituciones" value="<?php echo $codie?>">
<input type="text" name="nie" class="txtm" value="<?php echo $nomie?>">
<input type="hidden" name="codapafa" value="<?php echo $datos[0]['idapafa']?>">
	</td>


	<td class="headertd">Periodo</td>
	<td  >		
	<?php

$sqld=$obj->select_assoc("select * from periodo_escolar where idinstitucion=$codie");

	?>
	<select name="periodo" id="periodo">
				<option value="">Seleccionar</option>
				<?php
				
				for($i=0;$i<count($sqld);$i++)
				{
					if($datos[0]['idperiodo']==$sqld[$i]['idperiodo'])
					{
					$sl="selected=''";
					}
					else
					{
						$sl="";
					}
					?>
					<option <?php echo $sl;?> value="<?php echo $sqld[$i]['idperiodo']?>"><?php echo $sqld[$i]['periodo']?></option>
					<?php
				}
				?>
			</select>
<?php 

?>

	</td>


</tr>
<tr>
	<td class="headertd">Nom. Presidente</td>
	<td>
   <input type="hidden" id="cpresidente" name="cpresidente" value="<?php echo $codp?>">
   <input type="hidden" id="tpresidente" name="tpresidente" value="<?php echo $tp?>">
		<input readonly="readonly" type="text" name="npresidente" id="npresidente"  class="txtl" style="width:200px" value="<?php echo $nomp?>">
		<input type="button" value="..." onclick="block_page_blank('padres_de_familia.php?institucion=<?php echo $codie?>&opcion=presidente','Padres de Familia',700,450)"></td>
	
	<td class="headertd">Nom. Vice Presidente</td>
	<td>
<input type="hidden" id="cvpresidente" name="cvpresidente" value="<?php echo $codvp?>">
   <input type="hidden" id="tvpresidente" name="tvpresidente" value="<?php echo $tvp?>">
		<input readonly="readonly" type="text" name="nvp" class="txtl" style="width:200px" value="<?php echo $nomvp?>">
		<input type="button" value="..." onclick="block_page_blank('padres_de_familia.php?institucion=<?php echo $codie?>&opcion=vpresidente','Padres de Familia',700,450)">
	</td>
</tr>
<tr>
	<td class="headertd">Nom. Secretaria</td>
	<td>
		<input type="hidden" id="csecretario" name="csecretario" value="<?php echo $cods?>">
   <input type="hidden" id="tsecretario" name="tsecretario" value="<?php echo $ts?>">
		<input readonly="readonly" type="text" name="nsecretario" class="txtl" style="width:200px" value="<?php echo $noms?>">
		<input type="button" value="..." onclick="block_page_blank('padres_de_familia.php?institucion=<?php echo $codie?>&opcion=secretario','Padres de Familia',700,450)">
	</td>


	<td class="headertd">Nom. Tesorero</td>
	<td>
		<input type="hidden" id="ctesorero" name="ctesorero" value="<?php echo $codt?>">
   <input type="hidden" id="ttesorero" name="ttesorero" value="<?php echo $tt?>">
		<input readonly="readonly" type="text" name="ntesorero" class="txtl" style="width:200px" value="<?php echo $nomt?>">
		<input type="button" value="..." onclick="block_page_blank('padres_de_familia.php?institucion=<?php echo $codie?>&opcion=tesorero','Padres de Familia',700,450)">
	</td>
</tr>

 


<tr>
	<td class="headertd" colspan="8">
		<input class="bts" type="submit" value="Actualizar" name="apafa" onclick="this.form.action='actions_apafa.php'">
		<input class="btb" type="button" value="salir" onclick="select_item('apafa.php')">
	</td>
</tr>

</table>



</form>

	</div>
</div>

</body>
</html>


<?php
}
?>