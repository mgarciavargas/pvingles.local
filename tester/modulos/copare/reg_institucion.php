<?php
session_start();
if($_SESSION['copareusuario']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
$copareusu=$_SESSION['copareusuario'];
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>

<script type="text/javascript">
function imprSelec(muestra)
{
	document.getElementById('tacc').style.display="none";
	document.getElementById('controlacc').style.display="none";
	document.getElementById('tacc').style.display="none";
	document.getElementById('controlacc').style.display="none";
	var ficha=document.getElementById(muestra);
	var ventimp=window.open(' ','popimpr');
	ventimp.document.write(ficha.innerHTML);
	ventimp.document.close();
	ventimp.print();
	ventimp.close();
	document.getElementById('tacc').style.display="block";
	document.getElementById('controlacc').style.display="block";
	

}
</script>


</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>


<form action="actions_instituciones.php" method="post" enctype="multipart/form-data" autocomplete="off">
		<table align="center">
<tr>
	<th colspan="4">Registrar Datos de la Instituci&oacute;n</th>
	<input type="hidden" name="codg" value="<?php echo  $_SESSION['codgenerado'] ?>">
</tr>


<tr>
	<td class="headertd" >Instituci&oacute;n Educativa</td>
	<td ><input type="text" name="ie" class="txtl"></td>
	<td>Codigo Modular</td>
	 <td><input type="text" class="txtm" name="cmodular" ></td>
	 
</tr>
<tr>
	<td class="headertd">Nivel</td>
	<td ><?php 
	$nl=$obj->select_assoc("select * from nivel");
	?>
	<select name="nv">
		<option value="">Seleccionar</option>
		<?php
for($i=0;$i<count($nl);$i++)
{ 
		?>
		<option value="<?php echo $nl[$i]['idnivel']?>"><?php echo $nl[$i]['des_nivel']?></option>
		<?php 
}
		?>
	</select>
    </td>

    <td class="headertd">Tipo Instituci&oacute;n Educativa</td>
	<td >
	<select name="tp">
		<option value="">Seleccionar</option>
		<option value="estatal">Estatal</option>
		<option value="privada">Privada</option>
	</select>
    </td>
	
</tr>
<tr>
	<td class="headertd">Fecha Fundaci&oacute;n</td>
	<td coslpan="3">
		<input type="text" class="textc"  id="fap" name="fap">
	</td>

	<td>UGEL</td>
	<td>
	<select name="ugel">		
	<option>Seleccionar</option>
    <?php
    $lugel=$obj->select_assoc("select * from ugel");
    for($i=0;$i<count($lugel);$i++)
    {
    	?>
<option  value="<?php echo $lugel[$i]['idugel']?>"><?php echo $lugel[$i]['nombre']?></option>
    	<?php
    }
    ?>
	</select>
	</td>

</tr>

<tr>
	<td class="headertd" colspan="4">
	<div class="slub">
	Departamento 
	<?php
	$dep=$obj->select_assoc("select * from ubdepartamento where departamento='PIURA' ");
	?>
	<select name="dp">
		<option value="<?php echo $dep[0]['idDepa']?>"><?php echo $dep[0]['departamento']?></option>
	</select>
	</div>
	 
	 <div class="slub" id="dprov"> 
	Provincia  
	 <?php
	$pv=$dep[0]['idDepa'];
	$prov=$obj->select_assoc("select * from ubprovincia where idDepa=$pv");
	?>
	<select name="pv" onchange="distritop(this.value,'ddist')">
		<option vaue="0">Seleccionar</option>
		<?php
for($i=0;$i<count($prov);$i++){
		?>
		<option value="<?php echo $prov[$i]['idProv']?>"><?php echo $prov[$i]['provincia']?></option>
		<?php
		}
		?>
	</select>
	 </div>
	
	<div class="slub" id="ddist">  
	Distrito   
<select disabled=""><option>Seleccionar</option></select>
	</div>
	</td>
</tr>


<tr>
	<td class="headertd">Direccion</td>
	<td><input type="text" name="dir"  class="txtl" ></td>
	<td class="headertd">Email</td>
	<td><input type="text" name="mail"  class="txtl"  >
	</td>
</tr>

<tr>
	<td class="headertd">Telefono</td>
	<td><input type="text" name="tel"  class="txtm"  ></td>
	<td class="headertd">Web</td>
	<td><input type="text" name="web" id="pass"  class="txtl" >
        
	</td>
</tr>


<tr>
<td class="headertd">Foto</td>
	<td>
<input type="file" class="filefoto"  id="foto" name="foto"  title="Clic para agregar una Foto">
	<div class="ftbox" title="Clic para agregar una Foto" id="lfoto">

 

		</div>
		
   <output id="lists"></output>
		<script>
              function archivo(evt) {
                  var files = evt.target.files; // FileList object
             
                  // Obtenemos la imagen del campo "file".
                  for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }
             
                    var reader = new FileReader();
             
                    reader.onload = (function(theFile) {
                        return function(e) {
                          // Insertamos la imagen
                         document.getElementById("lfoto").innerHTML = ['<img class="thumb" style="width:100px;height:100px;" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                        };
                    })(f);
             
                    reader.readAsDataURL(f);
                  }
              }
             
              document.getElementById('foto').addEventListener('change', archivo, false);
      </script>

	</td>
			
	<td class="headertd"></td>
	<td></td>
	
</tr>


<tr>
	<td class="headertd">
	Observaci&oacute;n
	</td>
	<td colspan="3">
		<textarea name="des">
			
		</textarea>
	</td>
</tr>


<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Grabar" name="institucion">
		
		<input class="btb" type="button" value="salir" onclick="select_item('instituciones.php')">
	</td>
</tr>

</table>
</form>


</div>

</body>
</html>

<?php
};
?>
