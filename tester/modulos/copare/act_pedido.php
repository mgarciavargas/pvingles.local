<?php
session_start();
date_default_timezone_set('America/Lima');
if($_SESSION['copareusuario']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}
else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
		$copareusu=$_SESSION['copareusuario'];
 $cpd=$_GET['pedido'];

if($_GET['pedido']!="" & is_numeric($_GET['pedido']))
	{
		$gp=$_GET['pedido'];
$datos=$obj->select_assoc("select * from pedido where idpedido=$gp");
$codcole=$datos[0]['idinstitucion'];
$cole=$obj->select_assoc("select id_institucion,razon_social,idugel from institucion where id_institucion=$codcole");
$cug=$cole[0]['idugel'];
$ugeld=$obj->select_assoc("select * from ugel where idugel=$cug");
	}
	else
	{
		echo '<meta http-equiv="refresh" content="0; url=pedidos.php">';
	}

?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>

	<link type="text/css" href="calendario/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="calendario/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.core.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker-es.js"></script>
		<script type="text/javascript" src="calendario/jquery.ui.datepicker.js"></script>
		<link type="text/css" href="calendario/demos.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function() {
				$("#fecha").datepicker({
					showOn: 'both', //Parametro para que se vea el icono del calendario
					buttonImageOnly: true, //Indicamos si queremos que solo se vea el icono y no el botón
					buttonImage: 'calendario/calendar.gif', //Indicamos el icono del botón
					firstDay: 1, //El primer día será el 1
					changeMonth: true, //Si se pueden cambiar los meses
					changeYear: true, //Si se pueden cambiar los años
					dateFormat: 'yy-mm-dd' 
				});
			});

	


		</script>

		<script type="text/javascript">
function imprSelec(muestra)
{
	var ficha=document.getElementById(muestra);
	var ventimp=window.open(' ','popimpr');
	ventimp.document.write(ficha.innerHTML);
	ventimp.document.close();
	ventimp.print();
	ventimp.close();
}
</script>


</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>

	<div id="loadcontent">


 <?php
if($_SESSION['mokpedido']!=""){
?>
    <div id="mensaje" style="font-size:11px;text-transform: uppercase;text-align:center;width:350px;height:30px;line-height:30px;background-color:#84F155;color:#41651F;border:1px dotted #41651F;position:absolute;left:50%;margin-left:-80px;margin-top:-20px;border-radius:4px;">
    	<?php
echo $_SESSION['mokpedido'];
unset($_SESSION['mokpedido']);
    	?>
    </div>
<?php
}
?>
<?php
if($_SESSION['merrorpedido']!=""){
?>
    <div id="mensaje" style="font-size:11px;text-transform: uppercase;text-align:center;width:350px;height:30px;line-height:30px;background-color:#D98D7C;color:#8C271E;border:1px dotted #8C271E;position:absolute;left:50%;margin-left:-80px;margin-top:-20px;border-radius:4px;">
    	<?php
echo $_SESSION['merrorpedido'];
unset($_SESSION['merrorpedido']);
    	?>
    </div>
<?php
}
?>



		<form action="actions_pedidos.php" method="post" enctype="multipart/form-data">

		<table align="center" cellspacing="0" cellpadding="0">
<tr>
	<th colspan="4">Atender Pedido</th>
</tr>

<tr>
	<td  class="headertd">I. Educativa</td>
	<td>
<input type="hidden" name="instituciones" value="<?php echo $codcole?>">
<input type="hidden" name="codpedido" value="<?php echo $datos[0]['idpedido']?>">
<input type="text" name="nminstituciones" value="<?php echo $cole[0]['razon_social']?>" readonly="">
	</td>
</tr>

<tr>
	<td class="headertd">Fecha</td>
	<td  ><input type="text" name="fecha" id="fecha" value="<?php echo $datos[0]['fecha_pedido']?>" class="txtc"></td>
</tr>
<tr>
	<td class="headertd">Pedido</td>
	<td  ><input type="text" name="dpedido" class="txtl" value="<?php echo $datos[0]['pedido_descripcion']?>"></td>
</tr>
<tr>
	 
	<td class="headertd">Cantidad</td>
	<td><input type="text" name="cantidad" class="txtc" value="<?php echo $datos[0]['cantidad']?>"></td>
</tr>
<tr>
	<td class="headertd">Ugel</td>
	<td id="lstugel">
<input type="hidden" name="ugel" value="<?php echo $cug?>">
<input type="text" name="nug" value="<?php echo $ugeld[0]['nombre']?>">
	</td>	 
</tr>

<tr>
	<td class="headertd">Descripcion</td>
	<td ><textarea name="obs" style="width:200px;height:60px;"><?php echo $datos[0]['observacion']?></textarea></td>
</tr>

<tr>
<td colspan="2" style="background-color:#0483BE;height:1px;"></td>
</tr>

<tr>
	<td align="center" class="headertd">Acci&oacute;n</td>
	<td>
	<select name="accion">
		<option value="">Seleccionar</option>
		<option value="archivado">Archivado</option>
		<option value="pendiente">Pendiente</option>
		<option value="otorgado">Otorgado</option>
		<option value="negado">Negado</option>
	</select>
	</td>
</tr>

<tr>
	<td align="center" class="headertd">Observaci&oacute;n</td>
	<td>
	<textarea name="observ" style="width:250px;height:80px;"></textarea>
	</td>
</tr>

<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Atender" name="pedido">
		<input class="btr" type="reset" value="Cancelar" >
		<input class="btb" type="button" value="salir" onclick="select_item('pedidos.php')">
	</td>
</tr>

</table>

        <div class="plusct" style="margin-top:-30px;">
			<img src="images/icono_imprimir.jpg" style="width:100%:height:100%;" alt="Imprimir" title="Imprimir" onclick="imprSelec('results');">
		</div>
<hr>
<div id="results">
<table align="center" cellpadding="0" cellspacing="0" style="font-size:13px;">
<tr>
	<td colspan="3" style="border-bottom:1px solid #ccc;">Instituci&oacute;n Educat&iacute;va:<?php echo $cole[0]['razon_social']?></td>
</tr>
	<tr>
		<th align="center" width="60">Acci&oacute;n</th>
		<th align="center" width="100">Fecha</th>
		<th align="center" width="300">Observaci&oacute;n</th>
	</tr>
<?php
$accionesp=$obj->select_assoc("select * from seguimiento_pedido where idpedido=$cpd order by fecha DESC");
for($i=0;$i<count($accionesp);$i++){
?>
	<tr>
		<td align="center"><?php echo $accionesp[$i]['descripcion']?></td>
		<td align="center"><?php echo $accionesp[$i]['fecha']?></td>
		<td align="center"><?php echo $accionesp[$i]['observacion']?></td>
	</tr>
<?php
}
?>
</table>
</div>

</form>

	</div>
</div>
<script type="text/javascript">
	setTimeout(function(){desaparece('mensaje')},2000)
</script>
</body>
</html>


<?php
}
?>