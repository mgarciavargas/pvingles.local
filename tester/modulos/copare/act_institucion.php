<?php
session_start();
if($_SESSION['copareusuario']=="")
{
echo '<meta http-equiv="refresh" content="0; url=index.php">';
}else
{
	include_once('function/function_all.php');
$obj= new cl_all_functions();
if($_GET['institucion']=="" || !(is_numeric($_GET['institucion'])))
{
echo '<meta http-equiv="refresh" content="0; url=instituciones.php">';
}
$copareusu=$_SESSION['copareusuario'];
$institucion=$_GET['institucion'];
$sql=$obj->select_assoc("select * from institucion where id_institucion=$institucion");

?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="css/tables.css" type="text/css" rel="stylesheet" media="all">
<script type="text/javascript" src="script/script_functions.js"></script>

<script type="text/javascript">
function imprSelec(muestra)
{
	document.getElementById('tacc').style.display="none";
	document.getElementById('controlacc').style.display="none";
	document.getElementById('tacc').style.display="none";
	document.getElementById('controlacc').style.display="none";
	var ficha=document.getElementById(muestra);
	var ventimp=window.open(' ','popimpr');
	ventimp.document.write(ficha.innerHTML);
	ventimp.document.close();
	ventimp.print();
	ventimp.close();
	document.getElementById('tacc').style.display="block";
	document.getElementById('controlacc').style.display="block";
	

}
</script>


</head>
<body>
<?php include('perfil.php');?>

<div class="content">
	<?php include('menu.php');?>


		<form action="actions_instituciones.php" method="post" enctype="multipart/form-data" autocomplete="off">
		<table align="center" >
<tr>
	<th colspan="4">Datos de la Instituci&oacute;n</th>
</tr>


<tr>
	<td class="headertd" >Instituci&oacute;n Educativa</td>
	<td colspan="">
		<input type="text" name="rs" class="txtl" value="<?php echo $sql[0]['razon_social']?>">
<input type="hidden" name="institucionedu" value="<?php echo $sql[0]['id_institucion']?>">
	</td>
	<td>Codigo Modular</td>
	 <td><input type="text" class="txtm" name="cmodular" value="<?php echo $sql[0]['codmodular']?>"></td>
</tr>
<tr>
	<td class="headertd">Director(a)</td>
	<td>
		<?php

		$direc=$obj->select_assoc("select p.idpersonal,p.nombres,p.ap_paterno,p.ap_materno,t.tipopersonal from personal p inner join tipo_personal t on p.idtipopersonal=t.idtipopersonal where idinstitucion=$institucion and t.tipopersonal like 'Director%' ");
		echo $direc[0]['nombres']." ".$direc[0]['ap_paterno']." ".$direc[0]['ap_materno'];
		?>
	</td>

	<td class="headertd">Nivel</td>
	<td><?php 
	$nivel=$sql[0]['idnivel'];
	$nl=$obj->select_assoc("select * from nivel where idnivel=$nivel");
	echo $nl[0]['des_nivel']?></td>
	
</tr>
<tr>
	<td class="headertd">Fecha Fundaci&oacute;n</td>
	<td>
		<input type="text" class="textc" value="<?php echo $sql[0]['fecha_apertura']?>" id="fap" name="fap">
	</td>
	<td>UGEL</td>
	<td>
	<select name="ugel">		
	<option>Seleccionar</option>
    <?php
    $lugel=$obj->select_assoc("select * from ugel");
    for($i=0;$i<count($lugel);$i++)
    {
    	if($sql[0]['idugel']==$lugel[$i]['idugel'])
    	{
    		$slu="selected=''";
    	}
    	else
    	{
    		$slu="";
    	}
    	?>
<option <?php echo $slu?> value="<?php echo $lugel[$i]['idugel']?>"><?php echo $lugel[$i]['nombre']?></option>
    	<?php
    }
    ?>
	</select>
	</td>

</tr>

<tr>
	<td class="headertd" colspan="4">
	<div class="slub">
	Departamento 
	<?php
	$dep=$obj->select_assoc("select * from ubdepartamento");
	?>
	
	<select name="depa" class="ubs" onchange="selects_ub('depa',this.value,'dprov');disableselect();">
		<option value="">.:Seleccionar</option>
		<?php
		for($i=0;$i<count($dep);$i++){
			if($sql[0]['idDepa']==$dep[$i]['idDepa'])
			{
				$lsdp="selected=''";
			}
			else
			{
				$lsdp="";
			}
		?>
		<option <?php echo $lsdp;?> value="<?php echo $dep[$i]['idDepa']?>"><?php echo $dep[$i]['departamento']?></option>
		<?php
		}
		?>
	</select></div>
	 
	 <div class="slub" id="dprov"> 
	Provincia  
	 <?php
if($sql[0]['idProv']==""){
	 ?>
	<select name="prov" class="ubs" disabled="">
		<option value="">.:Seleccionar</option>
	</select>
<?php
}
else
{
	$dpc=$sql[0]['idDepa'];
	$sqlpv=$obj->select_assoc("select * from ubprovincia where idDepa=$dpc");

	?>
	<select name="prov"  class="ubs">
	<option value="">.:Seleccionar</option>
	<?php
	for($o=0;$o<count($sqlpv);$o++)
	{
		if($sql[0]['idProv']==$sqlpv[$o]['idProv'])
		{
			$slpv="selected=''";
		}
		else
		{
			$slpv="";
		}
?>
<option <?php echo $slpv?> value="<?php echo $sqlpv[$o]['idProv']?>" >
<?php echo $sqlpv[$o]['provincia']?>
</option>
<?php
}
?>
</select>
<?php
}
?>
	</div>
	
	<div class="slub" id="ddist">  
	Distrito   
	<?php
if($sql[0]['idDist']==""){
	?>
	<select name="dist" class="ubs" disabled="">
		<option value="">.:Seleccionar</option>
	</select>
<?php
}
else
{
	$pvcd=$sql[0]['idProv'];
	$sqldis=$obj->select_assoc("select * from ubdistrito where idProv=$pvcd");
?>
<select name="dist"  class="ubs">
	<option value="">.:Seleccionar</option>
	<?php
	for($a=0;$a<count($sqldis);$a++){
		if($sqldis[$a]['idDist']==$sql[0]['idDist'])
		{
			$sldt="selected=''";
		}
		else
		{
			$sldt="";
		}
	?>
	<option <?php echo $sldt?> value="<?php echo $sqldis[$a]['idDist']?>"><?php echo $sqldis[$a]['distrito']?></option>
	<?php
	}
	?>
</select>
<?php
}
?>

	</div>
	</td>
</tr>


<tr>
	<td class="headertd">Direccion</td>
	<td><input type="text" name="dir"  class="txtl" value="<?php echo $sql[0]['direccion']?>"></td>
	<td class="headertd">Email</td>
	<td><input type="text" name="mail"  class="txtl"  value="<?php echo $sql[0]['email']?>">
<input type="hidden" name="fac" value="<?php echo $sql[0]['insignia']?>">
<input type="hidden" name="ninst" value="<?php echo $sql[0]['razon_social']?>">
	</td>
</tr>

<tr>
	<td class="headertd">Telefono</td>
	<td><input type="text" name="tel"  class="txtm"  value="<?php echo $sql[0]['telefono']?>"></td>
	<td class="headertd">Web</td>
	<td><input type="text" name="web" id="pass"  class="txtl"  value="<?php echo $sql[0]['web']?>">
        
	</td>
</tr>


<tr>
<td class="headertd">Foto</td>
	<td>
<input type="file" class="filefoto"  id="foto" name="foto"  title="Clic para agregar una Foto">
	<div class="ftbox" title="Clic para agregar una Foto" id="lfoto">
	 <?php 
	 if($sql[0]['insignia']=="")
	 {

	 }
	 else
	 {
	 	?>
<img src="../institucion/instituciones/thumbs/<?php echo $sql[0]['insignia']?>" style="width:100px;height:100px;">
	 	<?php
	 }
	 ?>
		</div>
		
   <output id="lists"></output>
		<script>
              function archivo(evt) {
                  var files = evt.target.files; // FileList object
             
                  // Obtenemos la imagen del campo "file".
                  for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }
             
                    var reader = new FileReader();
             
                    reader.onload = (function(theFile) {
                        return function(e) {
                          // Insertamos la imagen
                         document.getElementById("lfoto").innerHTML = ['<img class="thumb" style="width:100px;height:100px;" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                        };
                    })(f);
             
                    reader.readAsDataURL(f);
                  }
              }
             
              document.getElementById('foto').addEventListener('change', archivo, false);
      </script>

	</td>
			
	<td class="headertd"></td>
	<td></td>
	
</tr>


<tr>
	<td class="headertd">
	Observaci&oacute;n
	</td>
	<td colspan="3">
		<textarea name="des">
			<?php echo $sql[0]['descripcion']?>
		</textarea>
	</td>
</tr>


<tr>
	<td class="headertd" colspan="4">
		<input class="bts" type="submit" value="Actualizar" name="institucion">
		
		<input class="btb" type="button" value="salir" onclick="select_item('instituciones.php')">
	</td>
</tr>

</table>
</form>


</div>

</body>
</html>

<?php
};
?>
