'use strict';
// Create an instance
var wavesurfer = Object.create(WaveSurfer);

// Init & load audio file
document.addEventListener('DOMContentLoaded', function(){
    // Init
    wavesurfer.init({
        container: document.querySelector('#waveform'),
        waveColor: '#A8DBA8',
        progressColor: '#3B8686',
        backend: 'MediaElement'
    });

    // Load audio from URL
    wavesurfer.load('1.mp3');
    document.querySelector('#xyz').addEventListener('click', wavesurfer.playPause.bind(wavesurfer));
    //document.querySelector('#xyz2').addEventListener('pause', wavesurfer.pause.bind(wavesurfer));
});