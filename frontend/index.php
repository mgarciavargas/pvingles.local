<?php
/**
 * @autor       Abel Chingo Tello
 * @fecha       08/09/2016
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */
$host= !empty($_SERVER["HTTP_HOST"])?$_SERVER["HTTP_HOST"]:'abacoeducacion.org';
$url= @$_SERVER["REQUEST_URI"];
if (!$_SERVER['HTTPS']){header('Location: https:'.$host.$url); exit();}
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR . 'ini_app.php');
$aplicacion->iniciar();

@session_start();
$urlredir=(!empty($_SERVER['HTTPS'])?'https://':'http://').$host.$url;
$_SESSION["urlredirok"]=$urlredir;
if(!empty($_REQUEST["idioma"])){	
	$documento = &JrInstancia::getDocumento();	
	$idioma=$_REQUEST["idioma"];
	$documento->setIdioma($idioma);
}

$aplicacion->enrutar(JrPeticion::getPeticion(0), JrPeticion::getPeticion(1));
$aplicacion->despachar();
echo $aplicacion->presentar();

function mostar__($rec, $ax){
	global $aplicacion;	
	$aplicacion->iniciar();
	$aplicacion->enrutar($rec, $ax);
	echo $aplicacion->presentar();
}