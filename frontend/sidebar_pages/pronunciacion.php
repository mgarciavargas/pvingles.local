<?php
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(__FILE__)).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
?>
<style type="text/css">  
  .form-control, .input-group-addon {
    border: 1px solid #4683af;
    margin: 0px;
  }
  .cajaselect {
    overflow: hidden;
    width: 100%;
    position: relative;
    border-radius: 5px;
  }
  
  .cajaselect::after {
    font-family: FontAwesome;
    font-size: 1.3em;
    content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #0070a0;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: #FFF;
    border-radius: 0px 0.3ex 0.3ex 0px;
  }
</style>
<div class="row" >
    <div class="col-xs-12">
        <h1 class="text-center">
            <?php echo JrTexto::_('How is it pronounced?');?>
            <!--<small>
                <i class="fa fa-angle-double-right"></i> Subtext for header
            </small>-->
        </h1>
    </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Volume")); ?>:</label>
                <input type="range" id="voicevolume" min="0" max="1" value="0.7" step="0.1" />
            </div>
          </div>
          <div  class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Speed")); ?>:</label>
                <input type="range" id="voiceSpeed" min="0.1" max="2" value="1" step="0.1" />
            </div>
          </div>
          <div  class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Pitch")); ?>:</label>
                <input type="range" id="voicepitch" min="0" max="2" value="1" step="0.1" />
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Voice"))?> :</label>
              <div class="cajaselect">
                <select name="voicepronunciation" id="voicepronunciation" class="speachvoives form-control">                  
                </select>
              </div>
            </div>            
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Write text"))?> :</label>
              <textarea id="texto" placeholder="Write text here" style="resize:none;" class="form-control" rows="3"></textarea>              
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button style="margin-top:5vh;" id="get_pronunciacion" onclick="speak()" type="button" class="btn btn-default center-block">Listen to the pronunciation</button>
          </div>
        </div> 
      </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    __cargarvoces($('select#voicepronunciation'));   
  });
</script>