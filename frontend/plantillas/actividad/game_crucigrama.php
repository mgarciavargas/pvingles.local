<?php 
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta.'ini_app.php');
    defined('RUTA_BASE') or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $met=empty($_GET["met"])?1:$_GET["met"];
    $orden=empty($_GET["ord"])?1:$_GET["ord"];
    $nivel=empty($_GET["nivel"])?1:$_GET["nivel"];
    $unidad=empty($_GET["unidad"])?1:$_GET["unidad"];
    $actividad=empty($_GET["actividad"])?1:$_GET["actividad"];
    $idgame=empty($_GET["idgame"])?null:$_GET["idgame"];
    $idgui = uniqid();
?>
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="V">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="D">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<div class="plantilla plantilla-crucigrama" data-idgui="<?php echo $idgui ?>">
  <div class="row">
    <div class="col-xs-12 pull-right text-right btns-zone nopreview" style="display: none; z-index: 9">
        <a class="btn btn-primary save-crossword">
            <i class="fa fa-floppy-o"></i> 
            <?php echo JrTexto::_('save').' '.JrTexto::_('crossword') ?>
        </a>
    </div>
    <div class="col-md-12">
      <div id="crossword<?php echo $idgui; ?>"></div>
      <div class="col-xs-12 col-sm-12 col-md-12 ">
              <table class="nopreview table table-striped table-responsive" id="alternacru<?php echo $idgui; ?>">               
                <tbody >
                  <tr>
                    <td><div class="addtextvideo txtquestion<?php echo $idgui; ?>"><?php echo JrTexto::_('Add Question or reference'); ?></div></td>
                    <td class="addtextvideo nopreview txtresponse<?php echo $idgui; ?>"><?php echo JrTexto::_('Response'); ?></td>
                    <td class="nopreview"><span class="removedialog tooltip" title="<?php echo JrTexto::_('Remove question'); ?>"><i class="fa fa-trash"></i> </span></td>
                  </tr>
                  <tr>
                    <td><div class="addtextvideo txtquestion<?php echo $idgui; ?>"><?php echo JrTexto::_('Add Question or reference'); ?></div></td>
                    <td class="addtextvideo nopreview txtresponse<?php echo $idgui; ?>"><?php echo JrTexto::_('Response'); ?></td>
                    <td class="nopreview  "><span class="removedialog tooltip" title="<?php echo JrTexto::_('Remove question'); ?>"><i class="fa fa-trash"></i> </span></td>
                  </tr>
                  <tr class="hidden" id="addpregunta<?php echo $idgui; ?>">
                    <td><div class="addtextvideo txtquestion<?php echo $idgui; ?>"><?php echo JrTexto::_('Add Question or reference'); ?></div></td>
                    <td class="addtextvideo nopreview txtresponse<?php echo $idgui; ?>"><?php echo JrTexto::_('Response'); ?></td>
                    <td class="nopreview"><span class="removedialog tooltip" title="<?php echo JrTexto::_('Remove question'); ?>"><i class="fa fa-trash"></i> </span></td>
                  </tr>                                
                </tbody>              
              </table>
              <div class="text-center nopreview">
                <span class="btn btn-xs btn-primary addclone<?php echo $idgui; ?> nopreview tooltip" data-source="#addpregunta<?php echo $idgui; ?>" data-donde="#alternacru<?php echo $idgui; ?>" title="<?php echo JrTexto::_('Add more Question or reference') ?>" >
                  <i class="fa fa-plus-square"></i>
                </span> 
                <span class="btn btn-xs btn-primary btngeneratecrusigrama nopreview tooltip" data-donde="#txtdialogocole" title="<?php echo JrTexto::_('Generate crossword') ?>" >
                  <i class="fa fa-gamepad"></i>
                </span>
              </div>
              <table class="table table-responsive" id="tbopcion<?php echo $idgui; ?>">
                  <thead>
                      <tr>
                          <th class="text-center"><?php echo JrTexto::_('Horizontal');?></th>
                          <th class="text-center"><?php echo JrTexto::_('Vertical'); ?></th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td><ul id="across<?php echo $idgui; ?>"></ul></td>
                          <td><ul id="down<?php echo $idgui; ?>"></ul></td>
                      </tr>
                  </tbody>
              </table>
            </div>      
    </div>
  </div>
</div> 

<script type="text/javascript">
$('.tooltip').tooltip();
$(document).ready(function(){
  $('#alternacru<?php echo $idgui; ?>').on('click','.removedialog',function(){
    $(this).parent().parent('tr').remove();
  });

  $('.addclone<?php echo $idgui; ?>').click(function(){
    var de=$(this).attr('data-source');
    var adonde=$(this).attr('data-donde');
    var clon=$(de).clone();
    $(adonde).append('<tr>'+clon.html()+'</tr>');
    $('.tooltip').tooltip();  
  });

  $('.btngeneratecrusigrama').click(function(){
      var question=$('.txtquestion<?php echo $idgui; ?>');
      var response=$('.txtresponse<?php echo $idgui; ?>');
      var datosp=[], datosf=[];
      response.each(function(){
        if(!$(this).parent().hasClass('hidden')){
          txt=$(this).text();
          datosp.push(txt.trim());
        }
      });
      question.each(function(){
       if(!$(this).parent().parent().hasClass('hidden')){
          txt=$(this).text();
          datosf.push(txt.trim());
        }
      });      
      var datos=[{palabras:datosp,frase:datosf}];
      $('#tbopcion<?php echo $idgui; ?>').removeClass('hidden');
      cargarcrucigrama('<?php echo $idgui; ?>',datos);
      $('.btns-zone').show('fast');
  });

  $('#alternacru<?php echo $idgui; ?>')
      .on('click','.addtextvideo', function(e){
       // if( $('.preview').is(':visible') ){ //si btn.preview es Visible:estamos en edicion
          var in_=$(this);
          var texto_inicial = $(this).text().trim();
          $(this).attr('data-initial_val', texto_inicial);
          in_.html('<input type="text" required="required" class="form-control" id="inputEditTexto" value="'+ texto_inicial +'">');
          if($(this).hasClass("addtime")){
            $('input#inputEditTexto',in_).addClass('addtime');
          }
          $('input#inputEditTexto',in_).focus().select();
        //}
      })
      .on('blur','input#inputEditTexto',function(){
        var dataesc=$(this).attr('data-esc');
        var vini=$(this).parent().attr('data-initial_val');
        var vin=$(this).val();
        if(dataesc) _intxt=vini;
        else _intxt=vin;
        $(this).removeAttr('data-esc');
        $(this).parent().attr('data-initial_val',_intxt); 
        if($(this).hasClass("addtime")){   
          var time=_intxt.split(":");
          var h=0,m=0,s=0;
          if(time.length==3){h=parseInt(time[0]);m=parseInt(time[1]);s=parseInt(time[2])}
          if(time.length==2){m=parseInt(time[0]);s=parseInt(time[1])}
          if(time.length==1){s=parseInt(time[0])}
          s=(h*3600)+(m*60)+s;
          var parent = $(this).parent().parent().parent().parent().attr('data-time',s);
        }
        $(this).parent().text(_intxt);
      })
      .on('keypress','input#inputEditTexto', function(e){      
        if(e.which == 13){ //Enter pressed
          e.preventDefault();          
          $(this).trigger('blur');
        } 
      }).on('keyup','input#inputEditTexto', function(e){      
        if(e.which == 27){ //Enter esc
          $(this).attr('data-esc',"1");
          $(this).trigger('blur');
        }
      });

  $('#crossword<?php echo $idgui; ?>').on('keydown','input.tvalcrusigrama',function(ev){
    if($(this).hasClass('ok')) return false;
    $(this).val('');
  });

  $('#crossword<?php echo $idgui; ?>').on('keyup','input.tvalcrusigrama',function(){
    val =$(this).data('val');
    val2=$(this).val();
    var tvalrow=['--'];
    $('#across<?php echo $idgui; ?>').find('li[data-orient="row"]').each(function(){
      var rval=$(this).data('val');
      tvalrow.push(rval.toUpperCase());
    });
    var tvalcol=new Array();
    $('#down<?php echo $idgui; ?>').find('li[data-orient="col"]').each(function(){
      var rval=$(this).data('val');
      tvalcol.push(rval.toUpperCase());
    });
    if(val.trim().toUpperCase()==val2.trim().toUpperCase()){
      $(this).addClass('ok').removeClass('error').attr('readonly','true');
      var r=$(this).data('row');
      var c=$(this).data('col');
      var todosrow=$('#crossword<?php echo $idgui; ?>').find('input.tvalcrusigrama.ok[data-row="'+r+'"]');
      var palabra=new Array();
      if(todosrow.length>1){
        $.each(todosrow,function(){
          var c=$(this).data('col');
          var v_=$(this).data('val');
          palabra[c]=v_;
        });
        txt=palabra.join("").toUpperCase();
        if(tvalrow.indexOf(txt)>-1){
          $('#across<?php echo $idgui; ?>').find('li[data-val="'+txt+'"]').css({color:'green','text-decoration':'line-through'});
        }
      }
      //var palabra=[];
      var todoscol=$('#crossword<?php echo $idgui; ?>').find('input.tvalcrusigrama.ok[data-col="'+c+'"]');
      var palabra=new Array();
      if(todoscol.length>1){
        $.each(todoscol,function(){         
          var c=$(this).data('row');
          var v_=$(this).data('val');
          palabra[c]=v_;
        });
        txt=palabra.join("").toUpperCase();
        if(tvalcol.indexOf(txt)>-1){
          $('#down<?php echo $idgui; ?>').find('li[data-val="'+txt+'"]').css({color:'green','text-decoration':'line-through'});
        }
      }      
    }else
      $(this).addClass('error').removeClass('ok');
      
  });

  $('.save-crossword').click(function(e) {
    e.preventDefault();
    msjes = {
        'attention' : '<?php echo JrTexto::_("Attention");?>',
        'guardado_correcto' : '<?php echo JrTexto::_("Game").' '.JrTexto::_("saved successfully");?>'
    };
    var data={
      idgame:$('#idgame').val(),
      nivel:'<?php echo $nivel;?>',
      unidad:'<?php echo $unidad;?>',
      actividad:'<?php echo $actividad;?>',
      texto:$('.games-main').html(),
      titulo:$('.titulo-game').val(),
      descripcion:$('.descripcion-game').val()
    }
    var idgame=saveGame(data,msjes);
    $('#idgame').val(idgame);
  });

/* 
* esta funcion esta en 'tool_games.js' y se 
* debe iniciar usando el plugin iniciarJuego() en 'plugin_games.js'
*/
  /*var iniciarCrucigrama = function(){
    var valRol = $('#hRol').val();
    if( valRol!='' && valRol!=undefined ){
      if(valRol != 1){
        $('.plantilla-crucigrama').find('.nopreview').remove();
      }
    }
  };

  iniciarCrucigrama();*/

});

</script>