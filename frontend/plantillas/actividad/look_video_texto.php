<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(dirname(__FILE__))).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
$documento =&JrInstancia::getDocumento();
$met=empty($_GET["met"])?exit(0):$_GET["met"];
$orden=empty($_GET["ord"])?1:$_GET["ord"];
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
?>
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Look">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Video_texto">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<input type="hidden" id="video<?php echo $idgui ?>" name="url[<?php echo $met ?>][<?php echo $orden ?>]" value="">
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_video.css">
<div class="plantilla plantilla_video" data-idgui="<?php echo $idgui ?>">
  <div class="tmp_video v<?php echo $idgui ?>">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-9 video-zone">
        <div class="video-loader"> 
          <div class="btnselectedfile btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a video from our multimedia library or upload your own video')) ?>" data-tipo="video" data-url=".vid_2<?php echo $met."_".$idgui ?>">
            <i class="fa fa-video-camera"></i> 
            <?php echo ucfirst(JrTexto::_('select')) ?> video
          </div>
        </div>
        <div class="videoseleccionado"></div>
        <div class="embed-responsive embed-responsive-16by9 mascara_video">
          <video id="vid222" controls="true" class="valvideo embed-responsive-item vid_2<?php echo $met."_".$idgui ?>" src="" style="display: none"></video>
          <img src="<?php echo $documento->getUrlStatic(); ?>/media/web/novideo.png" class="img-responsive img-thumbnail embed-responsive-item nopreview">
        </div>
      </div>

      <div class="col-xs-12 col-sm-4 col-md-3 texto-video">
        <table class="table table-striped" id="txtdialogocole">
          <thead>
            <tr>
              <th colspan="4" class="addtextvideo" title="<?php echo JrTexto::_('Add title'); ?>" data-initial_val="<?php echo JrTexto::_('Click here to add title') ?>"><?php echo JrTexto::_('Click here to add title') ?></th>
            </tr>
          </thead>
          <tbody >
            <tr data-time="0" class="playvideo">
              <td><div class="addtextvideo" title="<?php echo JrTexto::_('person').' '.JrTexto::_('or').' '.JrTexto::_('character'); ?>" data-initial_val="<?php echo JrTexto::_('person'); ?>"><?php echo JrTexto::_('person'); ?></div>
                <div class="nopreview time-input">
                  <span class="addtextvideo addtime" data-placement="right" data-initial_val="00:00" title="<?php echo ucfirst(JrTexto::_('time in seconds when this text will start')); ?>">00:00</span>
                </div>
              </td>
              <td style="width:5px;">:</td><td class="addtextvideo" title="<?php echo JrTexto::_('message').' '.JrTexto::_('or').' '.JrTexto::_('text'); ?>" ><?php echo JrTexto::_('message').' '.JrTexto::_('or').' '.JrTexto::_('text'); ?></td>
              <td class="nopreview"><span class="removedialog " title="<?php echo JrTexto::_('Remove dialog'); ?>"><i class="fa fa-trash"></i> </span></td>
            </tr>
            <tr data-time="0" class="playvideo">
              <td><div class="addtextvideo" title="<?php echo JrTexto::_('Add autor'); ?>" data-initial_val="<?php echo JrTexto::_('person'); ?>"><?php echo JrTexto::_('person'); ?></div>
                <div class="nopreview time-input">
                  <span class="addtextvideo addtime" data-placement="right" data-initial_val="00:00" title="<?php echo ucfirst(JrTexto::_('time in seconds when this text will start')); ?>">00:00</span>
                </div>
                <td>:</td><td class="addtextvideo" title="<?php echo JrTexto::_('Message or text'); ?>" data-initial_val="<?php echo JrTexto::_('Message or text'); ?>" ><?php echo JrTexto::_('Message or text'); ?></td>
                <td class="nopreview"><span class="removedialog " title="<?php echo JrTexto::_('Remove dialog'); ?>"><i class="fa fa-trash"></i> </span></td>
            </tr>
            <tr class="nopreview hidden playvideo" id="addautor" data-time="0" >
                <td><div class="addtextvideo" title="<?php echo JrTexto::_('Add autor'); ?>" data-initial_val="<?php echo JrTexto::_('Add autor'); ?>"><?php echo JrTexto::_('Autor'); ?></div>
                  <div class="nopreview time-input">
                    <span class="addtextvideo addtime" data-placement="right" data-initial_val="00:00" title="<?php echo JrTexto::_('time in seconds when this text will start'); ?>">00:00</span>
                  </div>
                  <td>:</td><td class="addtextvideo" title="<?php echo JrTexto::_('Message or text'); ?>" data-initial_val="<?php echo JrTexto::_('Message or text'); ?>" ><?php echo JrTexto::_('Message or text'); ?></td>
                  <td class="nopreview"><span class="removedialog " title="<?php echo JrTexto::_('Remove dialog'); ?>"><i class="fa fa-trash"></i> </span></td>
            </tr>                
          </tbody>              
        </table>
        
        <div class="btn btn-xs btn-primary addclone  nopreview " data-source="#addautor" data-donde="#txtdialogocole" title="<?php echo JrTexto::_('Add dialog') ?>" >
          <i class="fa fa-plus-square"></i>  <?php echo JrTexto::_('Add dialog') ?> 
        </div>

        <div class="texto tmp_video_texto<?php echo $idgui ?>" style="display:none;"> </div>
      </div>
    </div>
  </div>
  <textarea style="display:none;" class="tmp_video_texto<?php echo $idgui ?>" name="texto[<?php echo $met ?>]"> </textarea>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('video#vid222.vid_2<?php echo $met."_".$idgui ?>')
    .on('timeupdate',function(){
          var obj=this;
          var curtime=Math.floor(obj.currentTime);
          var txtdialogocole=$(obj).closest('.plantilla_video').find('#txtdialogocole');
          var tr= txtdialogocole.find('tr[data-time='+curtime+']');
          if(tr.length){
           txtdialogocole.find('tr').removeClass('inlectura');
            $(tr).addClass('inlectura');    
          }
    })
    .on("ended",function(){      
        var obj=this;
        var txtdialogocole=$(obj).closest('.plantilla_video').find('#txtdialogocole');  
        txtdialogocole.find('tr').removeClass('inlectura');
    });
  });
</script>