<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(dirname(__FILE__))).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
$documento =&JrInstancia::getDocumento();
$met=empty($_GET["met"])?exit(0):$_GET["met"];
$orden=empty($_GET["ord"])?1:$_GET["ord"];
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
?>
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_ordenar.css">

<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="DBY">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Ordenar_parrafos">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">

<div class="plantilla plantilla-ordenar ord_parrafo" id="tmp_<?php echo $idgui; ?>" data-tipo-tmp="ordenar_parrafo" data-idgui="<?php echo $idgui ?>">
    <div class="row nopreview" id="lista-parrafo-edit<?php echo $idgui; ?>">
        <div class="col-xs-12 parrafo-edit" id="parrafo_0">
            <div class="col-xs-12 col-sm-10 col-lg-11 zona-edicion">
                <div class="col-xs-12 col-sm-11 inputs-parrafo">
                    <textarea name="" id="txt_parrafo_0"></textarea>
                    <div class="row check-fixed">
                        <label class="col-xs-8 col-sm-4 col-md-3"><?php echo ucfirst(JrTexto::_('fix')).' '.JrTexto::_('this paragraph'); ?>: </label>
                        <div class="col-xs-4 col-sm-8 col-md-9">
                            <input type="checkbox" class="checkbox-ctrl" name="chkFixed">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-1 btns-parrafo text-center">
                    <a href="#" class="btn btn-danger delete-parrafo" data-tooltip="tooltip" title="<?php echo JrTexto::_('discard').' '.JrTexto::_(''); ?>"><i class="fa fa-trash-o"></i></a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2 col-lg-1 zona-btns-mover">
                <a href="#" class="col-xs-6 col-sm-12 btn btn-default move-parrafo up" data-tooltip="tooltip" data-placement="top" title="<?php echo ucfirst(JrTexto::_('move up')); ?>"><i class="fa fa-chevron-up"></i></a>

                <a href="#" class="col-xs-6 col-sm-12 btn btn-default move-parrafo down" data-tooltip="tooltip" data-placement="bottom" title="<?php echo ucfirst(JrTexto::_('move down')); ?>"><i class="fa fa-chevron-down"></i></a>
            </div>
        </div>
    </div>
    <div class="row nopreview botones">
        <div class="col-xs-12 text-center botones-creacion">
            <a href="#" class="btn btn-primary add-parrafo" data-clone-from="#tmp_<?php echo $idgui; ?> #clone_parrafo" data-clone-to="#lista-parrafo-edit<?php echo $idgui ?>"><i class="fa fa-plus-square"></i> <?php echo JrTexto::_('add').' '.JrTexto::_('paragraph'); ?></a>
            <a href="#" class="btn btn-success generar-parrafo" data-clone-from="" data-clone-to="#ejerc-ordenar<?php echo $idgui; ?>"><i class="fa fa-rocket"></i> <?php echo JrTexto::_('finish').' '.JrTexto::_('and').' '.JrTexto::_('generate').' '.JrTexto::_(''); ?></a>
        </div>
        <div class="col-xs-12 text-center botones-editar" style="display: none;">
            <a href="#" class="btn btn-success back-edit-parr"><i class="fa fa-pencil"></i> <?php echo JrTexto::_('back').' '.JrTexto::_('and').' '.JrTexto::_('edit'); ?></a>
        </div>

        <div class="hidden col-xs-12 parrafo-edit" data-id="parrafo_" id="clone_parrafo">
            <div class="col-xs-12 col-sm-10 col-lg-11 zona-edicion">
                <div class="col-xs-12 col-sm-11 inputs-parrafo">
                    <textarea class="renameId" data-id="txt_parrafo_" id=""></textarea>
                    <div class="row">
                        <label class="col-xs-6 col-sm-4 col-md-3"><?php echo ucfirst(JrTexto::_('fix')).' '.JrTexto::_('this paragraph'); ?>: </label>
                        <div class="col-xs-6 col-sm-8 col-md-9">
                            <input type="checkbox" class="checkbox-ctrl" name="chkFixed">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-1 btns-parrafo text-center">
                    <a href="#" class="btn btn-danger delete-parrafo" data-tooltip="tooltip" title="<?php echo JrTexto::_('discard').' '.JrTexto::_(''); ?>"><i class="fa fa-trash-o"></i></a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2 col-lg-1 zona-btns-mover">
                <a href="#" class="col-xs-6 col-sm-12 btn btn-default move-parrafo up" data-tooltip="tooltip" data-placement="top" title="<?php echo ucfirst(JrTexto::_('move up')); ?>"><i class="fa fa-chevron-up"></i></a>

                <a href="#" class="col-xs-6 col-sm-12 btn btn-default move-parrafo down" data-tooltip="tooltip" data-placement="bottom" title="<?php echo ucfirst(JrTexto::_('move down')); ?>"><i class="fa fa-chevron-down"></i></a>
            </div>
        </div>
    </div>
    <div class="row ejerc-ordenar tpl_plantilla" id="ejerc-ordenar<?php echo $idgui; ?>" style="display: none;">
        <div class="col-xs-12 col-sm-6 drop-parr">
            
        </div>
        <div class="col-xs-12 col-sm-6 drag-parr">
            
        </div>
    </div>
    
    <audio src="" class="hidden" id="audio-ordenar<?php echo $idgui; ?>" style="display: none;"></audio>
</div>