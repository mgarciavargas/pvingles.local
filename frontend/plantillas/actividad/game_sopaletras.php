<?php
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta."ini_app.php");
    defined("RUTA_BASE") or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $nivel=empty($_GET["nivel"])?1:$_GET["nivel"];
    $unidad=empty($_GET["unidad"])?1:$_GET["unidad"];
    $actividad=empty($_GET["actividad"])?1:$_GET["actividad"];
    $idgame=empty($_GET["idgame"])?null:$_GET["idgame"];
    $idgui = uniqid();
?>
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_sopaletras.css">
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<div class="plantilla plantilla-sopaletras" data-idgui="<?php echo $idgui ?>">
    <div class="row nopreview" id="pnl-edit">
        <div class="col-xs-12 btns-zone">
            <a class="btn btn-info add-word-game" data-clonefrom="#to-clone" data-cloneto="#list-words-game">
                <i class="fa fa-plus-circle"></i>
                <?php echo JrTexto::_("add")." ".JrTexto::_("word") ?>
            </a>
            <div class="col-xs-12 col-sm-6 col-md-4 item-word-game hidden" id="to-clone">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control word-game" placeholder="<?php echo JrTexto::_("write a word") ?>" maxlength="18">
                    <span class="glyphicon glyphicon-trash form-control-feedback" title="<?php echo JrTexto::_("remove")." ".JrTexto::_("word"); ?>"></span>
                </div>
            </div>
        </div>

        <div class="col-xs-12" id="list-words-game">
            <div class="col-xs-12 col-sm-6 col-md-4 item-word-game">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control word-game" placeholder="<?php echo JrTexto::_("write a word") ?>" maxlength="18" autofocus>
                    <span class="glyphicon glyphicon-trash form-control-feedback" data-tooltip="tooltip" title="<?php echo JrTexto::_("remove")." ".JrTexto::_("word"); ?>"></span>
                </div>
            </div>

        </div>

        <div class="col-xs-12 text-right">
            <a class="btn btn-primary create-word-search">
                <i class="fa fa-gamepad"></i>
                <?php echo JrTexto::_("create")." ".JrTexto::_("word search") ?>
            </a>
        </div>
    </div>

    <div class="row" id="findword-game" style="display: none;">
        <div class="col-xs-12 pull-right text-right btns-zone nopreview">
            <a class="btn btn-success edit-wordfind">
                <i class="fa fa-pencil-square-o"></i>
                <?php echo ucfirst(JrTexto::_("edit"))." ".JrTexto::_("word search"); ?>
            </a>
            <a class="btn btn-primary save-wordfind">
                <i class="fa fa-floppy-o"></i>
                <?php echo ucfirst(JrTexto::_("save"))." ".JrTexto::_("word search") ?>
            </a>
        </div>

        <div class="col-xs-12" style="text-align: center;">
            <div id="puzzle"></div>
        </div>
        <div class="col-xs-12" >
            <div id="words"></div>
        </div>
    </div>
</div>

<script>

$("*[data-tooltip=\"tooltip\"]").tooltip();



$(document).ready(function() {
    var inputsValidos = function(){
    var rspta = true;
    var $contenInputs = $("#list-words-game");
    $contenInputs.find("input.word-game").each(function() {
        var valor = $(this).val();
        if(valor.length<=1){
            $(this).parents(".form-group").addClass("has-error");
        } else {
            $(this).parents(".form-group").removeClass("has-error");
        }
    });
    $contenInputs.find("input.word-game").each(function() {
        var valor = $(this).val();
        if(valor.length<=1){
            rspta = false;
            return false;
        }
    });
    return rspta;
};

var obtenerPalabras = function(){
    var palabras = [];

    var $contenInputs = $("#list-words-game");
    $contenInputs.find("input.word-game").each(function() {
        var valor = $(this).val();
        $(this).attr("value", valor);
        palabras.push(valor);
    });

    return palabras;
};

$("#list-words-game")
    .on("keydown", "input.word-game", function(e){
        if (e.which === 32)
            return false;
        if( $(this).parents(".form-group").hasClass("has-error") ) inputsValidos();
    })
    .on("change", "input.word-game", function(e){
        this.value = this.value.replace(/\s/g, "");
    })
    .on("click", "span.form-control-feedback", function(e) {
        e.preventDefault();
        $(this).parents(".item-word-game").remove();
    });

$(".btn.add-word-game").click(function(e) {
    e.preventDefault();
    var id_cloneFrom = $(this).data("clonefrom");
    var id_cloneTo = $(this).data("cloneto");
    var now = Date.now();
    var newWord_html = $(id_cloneFrom).clone(true, true);

    newWord_html.removeClass("hidden");
    newWord_html.removeAttr("id");
    newWord_html.find("input.word-game").attr("id", "input-"+now);
    newWord_html.find("span.form-control-feedback").attr("data-tooltip", "tooltip");
    $(id_cloneTo).append(newWord_html);

    $("#input-"+now).focus();
    $("*[data-tooltip=\"tooltip\"]").tooltip();
});

$(".btn.create-word-search").click(function(e) {
    e.preventDefault();
    if( inputsValidos() ){
        var idPuzzle = "#puzzle";
        var idWords = "#words";
        var words = obtenerPalabras();
        var gamePuzzle = wordfindgame.create(words, idPuzzle, idWords);

        $("#pnl-edit").hide();
        $("#findword-game").show("fast");
    } else {
        $("#list-words-game").find(".has-error").first().find("input.word-game").focus();
        mostrar_notificacion("<?php echo JrTexto::_("Attention");?>","<?php echo JrTexto::_("words must have more than one letter");?>", "warning");
    }
    $('div[role="log"]').remove();
});

$(".edit-wordfind").click(function(e) {
    e.preventDefault();

    $("#pnl-edit").show("fast");
    $("#findword-game").hide();

    $("#puzzle").html("");
    $("#words").html("");
});

$(".btn.save-wordfind").click(function(e) {
    e.preventDefault();
    msjes = {
        'attention' : '<?php echo JrTexto::_("Attention");?>',
        'guardado_correcto' : '<?php echo JrTexto::_("Game").' '.JrTexto::_("saved successfully");?>'
    }
     var data={
      idgame:$('#idgame').val(),
      nivel:'<?php echo $nivel;?>',
      unidad:'<?php echo $unidad;?>',
      actividad:'<?php echo $actividad;?>',
      texto:$('.games-main').html(),
      titulo:$('.titulo-game').val(),
      descripcion:$('.descripcion-game').val()
    }
    var idgame=saveGame(data,msjes);
    $('#idgame').val(idgame);
});


/*
* esta funcion esta en 'tool_games.js' y se
* debe iniciar usando el plugin iniciarJuego() en 'plugin_games.js'
*/
/*var iniciarSopaLetras = function(){
    var arrWords=[];
    if( $('#words').html()!='' ){
        $('#words').find('li.word').each(function() {
            var word = $(this).text();
            arrWords.push(word);
        });

        var gamePuzzle = wordfindgame.create(arrWords, '#puzzle', '#words');
    }

    var valRol = $('#hRol').val();
    if( valRol!='' && valRol!=undefined ){
        if(valRol != 1){
            $('.plantilla-sopaletras').find('.nopreview').remove();
        }
    }
};
    iniciarSopaLetras();*/
});

</script>
