<?php
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta."ini_app.php");
    defined("RUTA_BASE") or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $met=empty($_GET["met"])?exit(0):$_GET["met"];
    $orden=empty($_GET["ord"])?1:$_GET["ord"];
    $idgui = uniqid();
    $usuarioAct = NegSesion::getUsuario();
    $rolActivo=$usuarioAct["rol"];
?>
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Practice">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Speach">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<div class="plantilla plantilla-speach" id="tmp_<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>" data-tipo-tmp="speach">
  <div class="row">
    <div class="col-xs-12 col-md-12 col-md-12 text-center">
      <div class="pnl-speach editable docente" id="pnl1<?php echo $idgui; ?>">
        <div>
          <a class="btn btn-sm btn-danger btnGrabarAudio nopreview"> <i class="fa fa-microphone"></i> <span></span></a> 
          <a class="btn btn-sm btn-danger btnWriteTexto nopreview"> <i class="fa fa-text-width"></i> <span></span></a>
          <a class="btn btn-sm btn-primary btnPlaytexto inpreview hidden"> <i class="fa fa-play"></i> <span></span></a>
        </div>
        <div class="form-group" idioma="EN">
          <div class="text-center">
            <span class="edittexto_ch ingles txtalu1"></span>
            <i class="pronunciar icon1 fa fa-volume-up hidden"></i>
          </div> 
        </div>
        <div class="grafico"></div>       
      </div>
      <br>
      <hr>
      <div class="textohablado" id="txtspeach<?php echo $idgui; ?>"></div>
      <br>
      <div class="pnl-speach alumno inpreview hidden" id="pnl2<?php echo $idgui; ?>">
        <div class="grafico"></div>
        <div class="form-group" idioma="EN">
          <div class="text-center">
            <span class="edittexto_ch ingles txtalu2"></span>
            <i class="pronunciar icon1 fa fa-volume-up hidden"></i>
          </div> 
        </div>
        <div> 
          <a class="btn btn-sm btn-danger btnGrabarAudio"> <i class="fa fa-microphone"></i> <span></span></a> 
          <a class="btn btn-sm btn-primary btnPlaytexto"> <i class="fa fa-play"></i> <span></span></a>
        </div>        
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#tmp_<?php echo $idgui; ?> .btnGrabarAudio span').text(MSJES_PHP.grabar_audio).attr('title',MSJES_PHP.grabar_audio) .attr('idioma2',MSJES_PHP.detener_audio);
    $('#tmp_<?php echo $idgui; ?> .btnWriteTexto span').text(MSJES_PHP.escribir_texto).attr('title',MSJES_PHP.escribir_texto);
    $('#tmp_<?php echo $idgui; ?> .btnPlaytexto span').text(MSJES_PHP.reproducir_audio).attr('title',MSJES_PHP.reproducir_audio);    
  });
</script>