<?php defined('_BOOT_') or die(''); 
if (isset($_GET['idcurso'])){ $idCurso = $_GET['idcurso']; }
else if (isset($_GET['id'])) { $idCurso = $_GET['id']; }
else{ $idCurso = 0; }
$ruta_completa=$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"];
$rutaBase = str_replace(array('http://', 'https://'), '', $documento->getUrlBase());
$ruta_sin_UrlBase = str_replace($rutaBase,'',$ruta_completa);
$ruta_sin_UrlBase = explode('?', $ruta_sin_UrlBase)[0];
$verion=!empty(_version_)?_version_:'1.0';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/hover.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/pnotify.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/libs/datepicker/datetimepicker.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo $documento->getUrlTema()?>/css/inicio.css?vs=<?php echo $verion; ?>" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/css/colores_flat.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/alumno/general.css?vs=<?php echo $verion; ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery.min.js"></script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/pnotify.min.js"></script>
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/funciones.js?vs=<?php echo $verion; ?>"></script>
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/js/inicio.js?vs=<?php echo $verion; ?>"></script>
    <script> var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>'; 
            var sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
            var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';
            var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';</script>
    <jrdoc:incluir tipo="cabecera" />
     <script src="<?php echo $documento->getUrlStatic()?>/js/pronunciacion.js?vs=<?php echo $verion; ?>"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/bootstrap.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/js/alumno/alumno.js?vs=<?php echo $verion; ?>"></script>
</head>
<body>
<jrdoc:incluir tipo="modulo" nombre="preload" /></jrdoc:incluir>
<jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
<jrdoc:incluir tipo="modulo" nombre="toolbar" /></jrdoc:incluir>
<jrdoc:incluir tipo="modulo" nombre="sidebar_right" /></jrdoc:incluir>

<div class="container">
    <input type="hidden" id="hCursoId" name="hCursoId" value="<?php echo @$_GET['id']; ?>">
    <div class="row">
        <!--div class="col-xs-12 col-sm-2 col-md-1" id="menu-curso">
            <a href="<?php echo $documento->getUrlBase().'/curso/?id='.$idCurso; ?>" class="btn-curso btn btn-orange <?php if($ruta_sin_UrlBase=='/curso/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Course'); ?>">
                <i class="glyphicon glyphicon-book fa-2x"></i>
                <span class="color-orange-dark border-orange-dark badge hidden">0</span>
            </a>

            <a href="<?php echo $documento->getUrlBase().'/curso/tarea/?id='.$idCurso; ?>" class="btn-tarea btn btn-aqua <?php if($ruta_sin_UrlBase=='/curso/tarea/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Smartask'); ?>">
                <i class="fa fa-briefcase fa-2x"></i>
                <span class="color-aqua-dark border-aqua-dark badge hidden">0</span>
            </a>

            <a href="<?php echo $documento->getUrlBase().'/docente/panelcontrol/?idcurso='.$idCurso; ?>" class="btn-seguimiento btn btn-green <?php if($ruta_sin_UrlBase=='/docente/panelcontrol/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Smartracking'); ?>">
                <i class="fa fa-tachometer fa-2x"></i>
                <span class="color-green-dark border-green-dark badge hidden">0</span>
            </a>

            <a href="<?php echo $documento->getUrlBase().'/curso/examenes/?id='.$idCurso; ?>" class="btn-examen btn btn-purple <?php if($ruta_sin_UrlBase=='/curso/examenes/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Smartquiz'); ?>"
                ><i class="fa fa-list fa-2x"></i>
                <span class="color-purple-dark border-purple-dark badge hidden">0</span>
            </a>

            <a href="https://abacoeducacion.org/web/smartclass/aulavirtual/requisitos/?id=7&5a56b6054a825720180110&idioma=ES<?php //echo $documento->getUrlBase().'/smartclass/?id='.$idCurso; ?>" class="btn-aula btn btn-red <?php if($ruta_sin_UrlBase=='/smartclass/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Smartclass'); ?>">
                <i class="fa fa-university fa-2x"></i>
                <span class="color-red-dark border-red-dark badge hidden">0</span>
            </a>

            <a href="<?php echo $documento->getUrlBase().'/curso/academico/?id='.$idCurso; ?>" class="btn-academico btn btn-yellow <?php if($ruta_sin_UrlBase=='/curso/academico/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Academic'); ?>">
                <i class="fa fa-graduation-cap fa-2x"></i>
                <span class="color-yellow-dark border-yellow-dark badge hidden">0</span>
            </a>
        </div-->
        <div class="col-xs-12" id="curso-contenido___">
            <jrdoc:incluir tipo="mensaje" />
            <jrdoc:incluir tipo="recurso" />
        </div>
    </div>
</div>

<jrdoc:incluir tipo="modulo" nombre="footer" posicion="man"/></jrdoc:incluir>

<?php
$configs = JrConfiguracion::get_();
require_once(RUTA_SITIO . 'ServerxAjax.php');
echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
?>
<script src="<?php echo $documento->getUrlStatic()?>/libs/moment/moment.js"></script>
<script src="<?php echo $documento->getUrlStatic()?>/libs/datepicker/datetimepicker.js"></script>
<jrdoc:incluir tipo="docsJs" />
<jrdoc:incluir tipo="docsCss" />
</body>
</html>