<?php defined('_BOOT_') or die(''); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/bootstrap.min.css" rel="stylesheet">
    <!--link href="<?php //echo $documento->getUrlStatic()?>/tema/css/bootstrap-datetimepicker.css" rel="stylesheet"-->

    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/hover.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/pnotify.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="<?php echo $documento->getUrlTema()?>/css/inicio.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/css/colores_inicio.css" rel="stylesheet">
    <jrdoc:incluir tipo="docsCss" />
    <link href="<?php echo $documento->getUrlSitio()?>/plantillas/tema1/aulavirtual/css/general.css" rel="stylesheet">    
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery.min.js"></script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/pnotify.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/libs/moment/moment.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/funciones.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/js/inicio.js"></script>
    <script> var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>'; 
            var sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
            var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';</script>
    <jrdoc:incluir tipo="cabecera" />

    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/bootstrap.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery-ui.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/libs/pizarra/jscolor.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/libs/pizarra/fabric1.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/libs/pizarra/pdf.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/libs/pizarra/pdf.worker.js"></script>   

</head>
<body>
<jrdoc:incluir tipo="modulo" nombre="preload" /></jrdoc:incluir>
<jrdoc:incluir tipo="modulo" nombre="aulavirtual" posicion="aulavirtual" /></jrdoc:incluir>
<div class="">
    <div class="menu-right pull-right">
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <jrdoc:incluir tipo="mensaje" />
                <jrdoc:incluir tipo="recurso" />
            </div>
        </div>
    </div>
</div>

<!--jrdoc:incluir tipo="modulo" nombre="footer" posicion="man"/></jrdoc:incluir-->
<?php
$configs = JrConfiguracion::get_();
require_once(RUTA_SITIO . 'ServerxAjax.php');
echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
?>
<script src="<?php echo $documento->getUrlStatic()?>/libs/moment/moment.js"></script>
<!--script src="<?php //echo $documento->getUrlStatic()?>/libs/datepicker/datetimepicker.js"></script-->
<jrdoc:incluir tipo="docsJs" />

</body>
</html>