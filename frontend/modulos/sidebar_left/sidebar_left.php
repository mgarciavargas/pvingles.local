<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('jrAdwen::JrModulo', RUTA_LIBS, 'jrAdwen::');
JrCargador::clase('modulos::sidebar_left::SidebarLeft', RUTA_SITIO, 'modulos::sidebar_left');
$oMod = new SidebarLeft;
echo $oMod->mostrar();