<footer class="row hidden" style="width: 100%;bottom: 0px;">
    <div class="col-sm-7 capitalize">
        &copy; <?=date('Y');?> AOL Ltd. <?php echo JrTexto::_('all rights reserved');?>
    </div>
    <div class="col-sm-5">
        <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/logo_minedu.png" class="img-responsive" alt="Logo MINEDU" >
    </div>
</footer>
<!-- Modal Box -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo JrTexto::_('User Guide');?></h4>
            </div>
            <div class="modal-body">
                <!--div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/dlJshzOv2cw" frameborder="0" allowfullscreen></iframe>
                </div>

                <label style="margin-top: 1em; width: 100%;">
                    <input type="checkbox" name="chkDejarDeMostrar" value="1" id="chkDejarDeMostrar">   
                    <?php //echo JrTexto::_('Do not show this again');?>.
                </label-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close"><?php echo JrTexto::_('close');?></button>
            </div>
        </div>
    </div>
</div>