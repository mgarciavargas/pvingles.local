<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('jrAdwen::JrModulo', RUTA_LIBS, 'jrAdwen::');
JrCargador::clase('modulos::top_bib::Top_bib', RUTA_SITIO, 'modulos::top_bib');
$oMod = new Top;
echo $oMod->mostrar(@$atributos['posicion']);