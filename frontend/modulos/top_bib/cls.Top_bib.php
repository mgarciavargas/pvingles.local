<?php
/**
 * @autor		: Abel Chingo Tello . ACHT
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRoles', RUTA_BASE, 'sys_negocio');
class Top extends JrModulo
{
	protected $oNegConfig;
	protected $oNegRoles;
	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->oNegRoles =new NegRoles();
		$this->modulo = 'top_bib';
	}
	
	public function mostrar($html=null)
	{
		try {
			$this->usuarioAct = NegSesion::getUsuario();
			if(empty($html)){
				$this->esquema = 'top_bib';
			}else $this->esquema = $html;

			
			
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}