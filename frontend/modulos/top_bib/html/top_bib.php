<header>
    <div class="container-fluid">
        <div class="navbar-header navbar-static-top"> 
            <button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button"> 
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
            </button> 
            <a href="<?php echo $this->documento->getUrlSitio() ?>" class="navbar-brand" id="logo">
                <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/logo.png" class="hvr-wobble-skew img-responsive hidden" alt="logo">
            </a>                        
        </div>       
        <nav class="collapse navbar-collapse" id="bs-navbar"> 
            <ul class="nav navbar-nav navbar-right menutop1"> 
               
                <li class="dropdown"> 
                    <ul class="dropdown-menu">
                        <li class="dropdown-header">
                            <a href="#" class="capitalize btn slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/nueva_actividad">
                                <i class="fa fa-thumb-tack"></i>
                                <?php echo JrTexto::_('New activity'); ?>
                            </a>
                        </li>
                        <li class="dropdown-content">
                            <ul>
                            <?php for($i=1; $i<=2; $i++): ?>
                                <li>
                                    <a href="#" class="activity">
                                        <span>Activity <?=$i; ?>.</span>
                                    </a>
                                </li>
                            <?php endfor; ?>
                            </ul>
                        </li>
                        <li class="dropdown-footer">
                            <a href="#" class="capitalize btn slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/ver_actividades">
                                <?php echo JrTexto::_('see more'); ?>
                                <i class="fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li> 
                
                <li class="dropdown" id="account" style="width: 100%;"> 
                    <a href="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user-circle"></i>
                    </a>

                    <div class="user-menu dropdown-menu">
                        <div class="account-info">
                            <a  class="user-photo">
                                <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/user_avatar.jpg" alt="user" class="img-responsive">
                            </a>
                            <div class="data-user">
                                <div class="user-name"><?php echo $this->usuarioAct['nombre_full'] ?></div>
                                <div class="user-email"><?php echo $this->usuarioAct['email'] ?></div>
                                <?php   
                                if($this->usuarioAct["rol_original"]!='Alumno'){
                                        $rol_=$this->usuarioAct["rol"];
                                        $rolo_=$this->usuarioAct["rol_original"];  
                                        $rol=$this->oNegRoles->buscar(array('idrol'=>$rolo_));
                                            if(!empty($rol[0])){
                                                $rol=$rol[0];
                                                $verrol=$rol_==$rolo_?ucfirst($rol["rol"]):'Alumno';
                                                $verme=$rol_==$rolo_?'Alumno':ucfirst($rol["rol"]);
                                                if(NegSesion::tiene_acceso('seralumno', 'list')||$rolo_!='Alumno'){
                                                  $verrol.=' <small class="changerol">('.JrTexto::_('Watch me as a')." ".$verme.')</small>';
                                                }
                                            }
                                        }else{ $verrol=$this->usuarioAct["rol"];}
                                    ?>
                                <div class="user-email">
                                 <?php echo $verrol; ?></div>

                                <button class="btn btn-blue capitalize slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/mi_perfil">
                                    <?php echo JrTexto::_('my profile'); ?>
                                </button>
                            </div>
                        </div>
                        <div class="action-buttons">
                            <div>
                                <a href="#" class="btn btn-default capitalize"><?php echo JrTexto::_('change password'); ?></a>
                            </div>
                            <div>
                                <a href="<?php echo $this->documento->getUrlSitio() ?>/sesion/salir" class="btn btn-default capitalize"><?php echo JrTexto::_('log out'); ?></a>
                            </div>
                        </div>
                    </div> 
                </li>
                
            </ul>
        </nav> 
    </div>
</header>