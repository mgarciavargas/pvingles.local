<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-11-2017 
 * @copyright	Copyright (C) 11-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBitacora_smartbook', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno_logro', RUTA_BASE, 'sys_negocio');
class WebBitacora_smartbook extends JrWeb
{
	private $oNegBitacora_alumno_smartbook;
	private $oNegBitacora_smartbook;
	private $oNegAlumno_logro;
	private $oNegAcad_cursodetalle;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBitacora_smartbook = new NegBitacora_smartbook;
		$this->oNegBitac_alum_smbook = new NegBitacora_alumno_smartbook;
	}

	public function defecto(){
		return $this->listado();
	}

/*
	public function listado()
	{
		try{
			global $aplicacion;			
			#if(!NegSesion::tiene_acceso('Bitacora_smartbook', 'list')) {
			#	throw new Exception(JrTexto::_('Restricted access').'!!');
			#}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idbitacora"])&&@$_REQUEST["idbitacora"]!='')$filtros["idbitacora"]=$_REQUEST["idbitacora"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idsesion"])&&@$_REQUEST["idsesion"]!='')$filtros["idsesion"]=$_REQUEST["idsesion"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["pestania"])&&@$_REQUEST["pestania"]!='')$filtros["pestania"]=$_REQUEST["pestania"];
			if(isset($_REQUEST["fechahora"])&&@$_REQUEST["fechahora"]!='')$filtros["fechahora"]=$_REQUEST["fechahora"];
			if(isset($_REQUEST["progreso"])&&@$_REQUEST["progreso"]!='')$filtros["progreso"]=$_REQUEST["progreso"];
			if(isset($_REQUEST["otros_datos"])&&@$_REQUEST["otros_datos"]!='')$filtros["otros_datos"]=$_REQUEST["otros_datos"];
			
			$this->datos=$this->oNegBitacora_smartbook->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bitacora_smartbook'), true);
			$this->esquema = 'bitacora_smartbook-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			#if(!NegSesion::tiene_acceso('Bitacora_smartbook', 'add')) {
			#	throw new Exception(JrTexto::_('Restricted access').'!!');
			#}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Bitacora_smartbook').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			#if(!NegSesion::tiene_acceso('Bitacora_smartbook', 'edit')) {
			#	throw new Exception(JrTexto::_('Restricted access').'!!');
			#}
			$this->frmaccion='Editar';
			$this->oNegBitacora_smartbook->idbitacora = @$_GET['id'];
			$this->datos = $this->oNegBitacora_smartbook->dataBitacora_smartbook;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bitacora_smartbook').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bitacora_smartbook-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bitacora_smartbook', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}*/
			$filtros=array();
			if(isset($_REQUEST["idbitacora"])&&@$_REQUEST["idbitacora"]!='')$filtros["idbitacora"]=$_REQUEST["idbitacora"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idsesion"])&&@$_REQUEST["idsesion"]!='')$filtros["idsesion"]=$_REQUEST["idsesion"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["pestania"])&&@$_REQUEST["pestania"]!='')$filtros["pestania"]=$_REQUEST["pestania"];
			if(isset($_REQUEST["fechahora"])&&@$_REQUEST["fechahora"]!='')$filtros["fechahora"]=$_REQUEST["fechahora"];
			if(isset($_REQUEST["progreso"])&&@$_REQUEST["progreso"]!='')$filtros["progreso"]=$_REQUEST["progreso"];
			if(isset($_REQUEST["otros_datos"])&&@$_REQUEST["otros_datos"]!='')$filtros["otros_datos"]=$_REQUEST["otros_datos"];
						
			$this->datos=$this->oNegBitacora_smartbook->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarBitacora_smartbook(){
		$this->documento->plantilla = 'returnjson';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            //if(!empty(@$pkIdbitacora)) {
            if(!empty(@$_REQUEST['Idbitacora'])) {
				$this->oNegBitacora_smartbook->idbitacora = $_REQUEST['Idbitacora']; //$frm['pkIdbitacora'];
				$accion='_edit';
			}

        	global $aplicacion;         
        	$usuarioAct = NegSesion::getUsuario();
        	@extract($_POST);

			/*$this->oNegBitacora_smartbook->idcurso=@$txtIdcurso;
			$this->oNegBitacora_smartbook->idsesion=@$txtIdsesion;
			$this->oNegBitacora_smartbook->idusuario=@$usuarioAct['dni'];//@$txtIdusuario;*/
			$this->oNegBitacora_smartbook->idbitacora_alum_smartbook=@$Idbitacora_smartbook;
			$this->oNegBitacora_smartbook->pestania=@$txtPestania;
			$this->oNegBitacora_smartbook->total_pestanias=@$txtTotal_Pestanias;
			$this->oNegBitacora_smartbook->fechahora=date('Y-m-d H:i:s');
			$this->oNegBitacora_smartbook->progreso=@$txtProgreso;
			$this->oNegBitacora_smartbook->otros_datos= @str_replace($this->documento->getUrlBase(), '__xRUTABASEx__', @$txtOtros_datos);
					
            if($accion=='_add') {
            	$res=$this->oNegBitacora_smartbook->agregar();
            	$texto_msje = JrTexto::_('saved successfully');
            }else{
            	$res=$this->oNegBitacora_smartbook->editar();
            	$texto_msje = JrTexto::_('update successfully');
            }
            $this->oNegBitac_alum_smbook->setCampo($Idbitacora_smartbook, 'regfecha', @date('Y-m-d H:i:s'));

            $progreso = $this->oNegBitacora_smartbook->getProgresoPromedio(array("idbitacora_alum_smartbook"=>$Idbitacora_smartbook ));

            $flagMostrado = $this->setAlumnoLogro( (int)@$txtIdlogro, @$txtIdsesion, @$txtIdcurso, $progreso);
            
            echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Bitacora_smartbook')).' '.$texto_msje,'newid'=>$res, 'progreso'=>$progreso,'bandera'=>$flagMostrado)); 
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage() ));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveBitacora_smartbook(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdbitacora'])) {
					$this->oNegBitacora_smartbook->idbitacora = $frm['pkIdbitacora'];
				}
				
				$this->oNegBitacora_smartbook->idcurso=@$frm["txtIdcurso"];
				$this->oNegBitacora_smartbook->idsesion=@$frm["txtIdsesion"];
				$this->oNegBitacora_smartbook->idusuario=@$frm["txtIdusuario"];
				$this->oNegBitacora_smartbook->pestania=@$frm["txtPestania"];
				$this->oNegBitacora_smartbook->fechahora=@$frm["txtFechahora"];
				$this->oNegBitacora_smartbook->progreso=@$frm["txtProgreso"];
				$this->oNegBitacora_smartbook->otros_datos=@$frm["txtOtros_datos"];

				if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegBitacora_smartbook->agregar();
				}else{
					$res=$this->oNegBitacora_smartbook->editar();
				}
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegBitacora_smartbook->idbitacora);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDBitacora_smartbook(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBitacora_smartbook->__set('idbitacora', $pk);
				$this->datos = $this->oNegBitacora_smartbook->dataBitacora_smartbook;
				$res=$this->oNegBitacora_smartbook->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBitacora_smartbook->__set('idbitacora', $pk);
				$res=$this->oNegBitacora_smartbook->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegBitacora_smartbook->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}


	// ========================== Funciones xajax ========================== //
	private function setAlumnoLogro($idLogro = 0, $idSesion = 0 , $idCurso = 0, $progreso=0.0)
	{
		try {
			JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
			JrCargador::clase('sys_negocio::NegAlumno_logro', RUTA_BASE, 'sys_negocio');
			$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
			$this->oNegAlumno_logro = new NegAlumno_logro;

        	$usuarioAct = NegSesion::getUsuario();
			$idalumno_logro = $flagMostrado = null;
			if($idLogro>0) {
	            $curso_det=$this->oNegAcad_cursodetalle->buscar(array( 'idrecurso'=>$idSesion, 'idcurso'=>$idCurso ));
	            $alumno_logro = $this->oNegAlumno_logro->buscar(array( 'id_alumno'=>$usuarioAct['dni'], 'id_logro'=>$idLogro, 'idrecurso'=>$idSesion, 'tiporecurso'=>$curso_det[0]['tiporecurso'] ));

	            if(!empty($alumno_logro)){
	            	$flagMostrado = $alumno_logro[0]['bandera'];
	            }else if($progreso>=100){
	            	$flagMostrado = 0;
	            	$this->oNegAlumno_logro->id_alumno=$usuarioAct['dni'];
	            	$this->oNegAlumno_logro->idrecurso=@$idSesion;
	            	$this->oNegAlumno_logro->tiporecurso=$curso_det[0]['tiporecurso'];
	            	$this->oNegAlumno_logro->id_logro=$curso_det[0]['idlogro'];
	            	$this->oNegAlumno_logro->bandera=$flagMostrado;
	            	$idalumno_logro = $this->oNegAlumno_logro->agregar();
	            }
            }

            return $flagMostrado;
		} catch (Exception $e) {
			throw new Exception(JrTexto::_($e->getMessage()));
		}
	}
}