<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		02-01-2018 
 * @copyright	Copyright (C) 02-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona_apoderado', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');

class WebPersona_apoderado extends JrWeb
{
	private $oNegPersona_apoderado;
	private $oNegPersonal;
	private $oNegGeneral;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona_apoderado = new NegPersona_apoderado;
		$this->oNegPersonal = new NegPersonal;
	$this->oNegGeneral = new NegGeneral;
		
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idapoderado"])&&@$_REQUEST["idapoderado"]!='')$filtros["idapoderado"]=$_REQUEST["idapoderado"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["ape_paterno"]!='')$filtros["ape_paterno"]=$_REQUEST["ape_paterno"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["ape_materno"]!='')$filtros["ape_materno"]=$_REQUEST["ape_materno"];
			if(isset($_REQUEST["correo"])&&@$_REQUEST["correo"]!='')$filtros["correo"]=$_REQUEST["correo"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["tipodoc"])&&@$_REQUEST["tipodoc"]!='')$filtros["tipodoc"]=$_REQUEST["tipodoc"];
			if(isset($_REQUEST["ndoc"])&&@$_REQUEST["ndoc"]!='')$filtros["ndoc"]=$_REQUEST["ndoc"];
			if(isset($_REQUEST["parentesco"])&&@$_REQUEST["parentesco"]!='')$filtros["parentesco"]=$_REQUEST["parentesco"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			
			$this->datos=$this->oNegPersona_apoderado->buscar($filtros);
			//$this->fkidpersona=$this->oNegPersonal->buscar();
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));			
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->fkparentesco=$this->oNegGeneral->buscar(array('tipo_tabla'=>'parentesco','mostrar'=>1));
			$this->fktipodoc=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipodocidentidad','mostrar'=>1));
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Persona_apoderado'), true);
			$this->esquema = 'persona_apoderado-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Persona_apoderado').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			$this->frmaccion='Editar';
			$this->oNegPersona_apoderado->idapoderado = @$_GET['id'];
			$this->datos = $this->oNegPersona_apoderado->dataPersona_apoderado;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Persona_apoderado').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->fkidpersona=$this->oNegPersonal->buscar();
			$this->fksexo=$this->oNegGeneral->buscar();
			$this->fktipodoc=$this->oNegGeneral->buscar();
			$this->fkparentesco=$this->oNegGeneral->buscar();
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'persona_apoderado-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Persona_apoderado', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}*/
			$filtros=array();
			if(isset($_REQUEST["idapoderado"])&&@$_REQUEST["idapoderado"]!='')$filtros["idapoderado"]=$_REQUEST["idapoderado"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["ape_paterno"]!='')$filtros["ape_paterno"]=$_REQUEST["ape_paterno"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["ape_materno"]!='')$filtros["ape_materno"]=$_REQUEST["ape_materno"];
			if(isset($_REQUEST["correo"])&&@$_REQUEST["correo"]!='')$filtros["correo"]=$_REQUEST["correo"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["tipodoc"])&&@$_REQUEST["tipodoc"]!='')$filtros["tipodoc"]=$_REQUEST["tipodoc"];
			if(isset($_REQUEST["ndoc"])&&@$_REQUEST["ndoc"]!='')$filtros["ndoc"]=$_REQUEST["ndoc"];
			if(isset($_REQUEST["parentesco"])&&@$_REQUEST["parentesco"]!='')$filtros["parentesco"]=$_REQUEST["parentesco"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
						
			$this->datos=$this->oNegPersona_apoderado->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarPersona_apoderado(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);
            }

            @extract($_POST);           
            $accion='_add';
            if(!empty(@$idapoderado)) {
				$this->oNegPersona_apoderado->idapoderado = $idapoderado;
				$accion='_edit';
			}
        	         
        	$usuarioAct = NegSesion::getUsuario(); 
			$this->oNegPersona_apoderado->idpersona=@$idpersona;
			$this->oNegPersona_apoderado->nombre=@$nombre;
			$this->oNegPersona_apoderado->ape_paterno=@$ape_paterno;
			$this->oNegPersona_apoderado->ape_materno=@$ape_materno;
			$this->oNegPersona_apoderado->correo=@$correo;
			$this->oNegPersona_apoderado->sexo=@$sexo;
			$this->oNegPersona_apoderado->tipodoc=@$tipodoc;
			$this->oNegPersona_apoderado->ndoc=@$ndoc;
			$this->oNegPersona_apoderado->parentesco=@$parentesco;
			$this->oNegPersona_apoderado->telefono=@$telefono;
			$this->oNegPersona_apoderado->celular=@$celular;
			$this->oNegPersona_apoderado->mostrar=!empty($mostrar)?$mostrar:1;
					
            if($accion=='_add') {
            	$res=$this->oNegPersona_apoderado->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Persona_apoderado')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPersona_apoderado->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Persona_apoderado')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSavePersona_apoderado(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdapoderado'])) {
					$this->oNegPersona_apoderado->idapoderado = $frm['pkIdapoderado'];
				}
				
				$this->oNegPersona_apoderado->idpersona=@$frm["txtIdpersona"];
				$this->oNegPersona_apoderado->nombre=@$frm["txtNombre"];
				$this->oNegPersona_apoderado->ape_paterno=@$frm["txtApe_paterno"];
				$this->oNegPersona_apoderado->ape_materno=@$frm["txtApe_materno"];
				$this->oNegPersona_apoderado->correo=@$frm["txtCorreo"];
				$this->oNegPersona_apoderado->sexo=@$frm["txtSexo"];
				$this->oNegPersona_apoderado->tipodoc=@$frm["txtTipodoc"];
				$this->oNegPersona_apoderado->ndoc=@$frm["txtNdoc"];
				$this->oNegPersona_apoderado->parentesco=@$frm["txtParentesco"];
				$this->oNegPersona_apoderado->telefono=@$frm["txtTelefono"];
				$this->oNegPersona_apoderado->celular=@$frm["txtCelular"];
				$this->oNegPersona_apoderado->mostrar=@$frm["txtMostrar"];
				
			   	if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegPersona_apoderado->agregar();
				}else{
				    $res=$this->oNegPersona_apoderado->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegPersona_apoderado->idapoderado);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDPersona_apoderado(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersona_apoderado->__set('idapoderado', $pk);
				$this->datos = $this->oNegPersona_apoderado->dataPersona_apoderado;
				$res=$this->oNegPersona_apoderado->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersona_apoderado->__set('idapoderado', $pk);
				$res=$this->oNegPersona_apoderado->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegPersona_apoderado->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}