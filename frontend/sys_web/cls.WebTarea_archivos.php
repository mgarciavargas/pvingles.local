<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-05-2017 
 * @copyright	Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTarea_archivos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_respuesta', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_detalle', RUTA_BASE, 'sys_negocio');
class WebTarea_archivos extends JrWeb
{
	private $oNegTarea_archivos;
	private $oNegTarea;
	private $oNegTarea_asignacion_alumno;
	private $oNegTarea_asignacion;
	private $oNegTarea_respuesta;
	private $oNegActividad_detalle;

	public function __construct()
	{
		parent::__construct();	
		$this->oNegTarea = new NegTarea;	
		$this->oNegTarea_archivos = new NegTarea_archivos;
		$this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;
		$this->oNegTarea_asignacion = new NegTarea_asignacion;
		$this->oNegTarea_respuesta = new NegTarea_respuesta;
		$this->oNegActividad_detalle = new NegActividad_detalle;
	}

	public function defecto(){
		//return $this->listado();
	}

	public function visor()
	{
		try {
			global $aplicacion;	
			if(empty($_GET['idarchivo'])){
				throw new Exception("Homework parameters are missing.");
			}

            /*librerias*/
             $this->documento->script('jquery-ui.min', '/tema/js/');
             $this->documento->script('jquery.md5', '/tema/js/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/'); 
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('jquery-confirm.min', '/libs/alert/');
            
             $this->documento->script('cronometro', '/libs/chingo/');
			 $this->documento->script('editactividad', '/js/new/');
             $this->documento->script('actividad_completar', '/js/new/');
             $this->documento->script('actividad_ordenar', '/js/new/');
             $this->documento->script('actividad_imgpuntos', '/js/new/');
             $this->documento->script('actividad_verdad_falso', '/js/new/');
             $this->documento->script('actividad_fichas', '/js/new/');
             $this->documento->script('actividad_dialogo', '/js/new/');
             $this->documento->script('manejadores_dby', '/js/new/');
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('jquery.md5', '/tema/js/');            
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('completar', '/js/new/');

             $this->documento->script('tools_games', '/js/');
             $this->documento->script('plugin_visor', '/js/');
             $this->documento->stylesheet('estilo', '/libs/crusigrama/');
             $this->documento->script('crossword', '/libs/crusigrama/');
             $this->documento->script('micrusigrama', '/libs/crusigrama/');
             $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
             $this->documento->script('wordfind', '/libs/sopaletras/js/');
             $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
             $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
             
             $this->documento->script('wavesurfer.min', '/libs/audiorecord/');
             $this->documento->script('audioRecord', '/libs/audiorecord/');
             $this->documento->script('recorderWorker', '/libs/audiorecord/');

            $usuarioAct = NegSesion::getUsuario();
			$this->idtarea_archivos= @$_GET['idarchivo'];
			$this->idtarea_asignacion_alumno= @$_GET['idasig_alum'];
			$this->idalumno=(!empty(@$_GET['idalumno']))? @$_GET['idalumno']:$usuarioAct['dni'];
			$this->breadcrumb=JrTexto::_('Viewer');
			$this->archivo= $this->tarea= $this->respuesta= $this->html= array();
			$this->asignacion_alumno= $this->asignacion= array();

			if( !empty($this->idtarea_asignacion_alumno) ){
				$filtros = array('iddetalle'=> $this->idtarea_asignacion_alumno);
				if($usuarioAct['rol']=="Alumno"){
					$filtros['idalumno'] = $this->idalumno;
				}
				$this->asignacion_alumno= $this->oNegTarea_asignacion_alumno->buscar($filtros);

				if(!empty($this->asignacion_alumno)){
					$this->asignacion_alumno= $this->asignacion_alumno[0];
					$this->asignacion= $this->oNegTarea_asignacion->buscar([
						'idtarea_asignacion'=>$this->asignacion_alumno['idtarea_asignacion'], 
						/*'fechahora_mayorigual'=> date('Y-m-d H:i:s'), */
						'eliminado'=> 0 ]);
				}else{ throw new Exception(JrTexto::_("This homework attachment is not assigned to you").'.'); }

				if(!empty($this->asignacion)){
					$this->asignacion= $this->asignacion[0];
					$this->tarea= $this->oNegTarea->buscar([
						'idtarea'=> $this->asignacion['idtarea'], 
						'eliminado'=> 0 ]);
				}else{ throw new Exception(JrTexto::_("This homework attachment is not assigned to you")); }

				if(!empty($this->tarea)){ 
					$this->tarea= $this->tarea[0]; 
					$this->respuesta= $this->oNegTarea_respuesta->buscarConArchivos(['idtarea_asignacion_alumno'=> $this->idtarea_asignacion_alumno] , ['idtarea_archivos_padre'=> $this->idtarea_archivos]);
				}else{ throw new Exception(JrTexto::_("Homework file not found")); }

				if(!empty($this->respuesta)){ 
					$this->respuesta= $this->respuesta[0]; 
					if(!empty($this->respuesta['respuesta_archivos'])){
						$this->archivo=$this->respuesta['respuesta_archivos'];
					}else{
						$this->archivo= $this->oNegTarea_archivos->buscar([
							'idtarea_archivos'=> $this->idtarea_archivos, 
							'tablapadre'=> 'T']);
					}
				}else{ throw new Exception(JrTexto::_("You cannot answer this attachment file")); }

				if(!empty($this->archivo)){ 
					$this->archivo= $this->archivo[0]; 
					$arrTexto = json_decode($this->archivo['texto'], true);
					foreach ($arrTexto as $elem) {
						$html_elem=array();
						
						if($this->archivo['tipo']=='A'){
							if($this->archivo['tablapadre']=='T'){
								$datos=$this->oNegActividad_detalle->buscar(['iddetalle'=>$elem]);
								$html_elem['id']=$datos[0]['iddetalle'];
								$html_elem['texto']=$datos[0]['texto'];
							}elseif($this->archivo['tablapadre']=='R'){
								$datos=$this->oNegActividad_detalle->buscar(['iddetalle'=>$elem['id'] ]);
								$html_elem['id']=$elem['id'];
								$html_elem['texto']=$elem['html'];
							}
							$html_elem['titulo']= $datos[0]['titulo'];
							$html_elem['descripcion']= $datos[0]['descripcion'];

						}elseif($this->archivo['tipo']=='J' || $this->archivo['tipo']=='E'){
							if($this->archivo['tablapadre']=='T'){
								$html_elem['id']=$elem;
							}elseif($this->archivo['tablapadre']=='R'){
								$html_elem['id']=$elem['id'];
							}
						}
						array_push($this->html, $html_elem);
					}
				}else{ throw new Exception(JrTexto::_("This attachment file does not belong to the homework anymore")); }
			}else{
				$this->archivo= $this->oNegTarea_archivos->buscar([
					'idtarea_archivos'=> $this->idtarea_archivos, 
					/*'tablapadre'=> 'T'*/]);

				if(!empty($this->archivo)){ 
					$this->archivo= $this->archivo[0]; 
					$this->tarea= $this->oNegTarea->buscar([
						'idtarea'=> $this->archivo['idpadre'], 
						'iddocente'=> $usuarioAct['dni'], 
						'eliminado'=> 0 ]); 
				}else{ throw new Exception(JrTexto::_("Homework attachment not found")); }

				if(!empty($this->tarea)){ $this->tarea= $this->tarea[0]; }
				else{ throw new Exception(JrTexto::_("This attachment file does not belong to the homework")); }

				$arrTexto = json_decode($this->archivo['texto'],true);
				foreach ($arrTexto as $elem) {
					$html_elem=[];
					if($this->archivo['tipo']=='A'){
						$act_det=$this->oNegActividad_detalle->buscar(['iddetalle'=>$elem]);
						$html_elem['id']=$act_det[0]['iddetalle'];
						$html_elem['titulo']=$act_det[0]['titulo'];
						$html_elem['descripcion']=$act_det[0]['descripcion'];
						$html_elem['texto']=$act_det[0]['texto'];
					}elseif($this->archivo['tipo']=='J' || $this->archivo['tipo']=='E'){
						if($this->archivo['tablapadre']=='T' || $this->archivo['tablapadre']==null){
							$html_elem['id']=$elem;
						}elseif($this->archivo['tablapadre']=='R'){
							$html_elem['id']=$elem['id'];
						}
					}
					array_push($this->html, $html_elem);
				}
			}
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tarea Visor'), true);
			$this->esquema = 'tarea/visor_html';			
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
/*
	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Tarea_archivos', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegTarea_archivos->buscar();

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tarea_archivos'), true);
			$this->esquema = 'tarea_archivos-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Tarea_archivos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Tarea_archivos').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Tarea_archivos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegTarea_archivos->idtarea_archivos = @$_GET['id'];
			$this->datos = $this->oNegTarea_archivos->dataTarea_archivos;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Tarea_archivos').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegTarea_archivos->idtarea_archivos = @$_GET['id'];
			$this->datos = $this->oNegTarea_archivos->dataTarea_archivos;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tarea_archivos').' /'.JrTexto::_('see'), true);
			$this->esquema = 'tarea_archivos-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'tarea_archivos-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/
	public function subirarchivo(){
		$this->documento->plantilla = 'returnjson';
		try{
		
			global $aplicacion;	
			if(empty($_POST["tipo"])||empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit();
			}
			$folder = 'tarea_archivos';
			$file=$_FILES["filearchivo"];

			$ext=@strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
			$intipo='';
			$tipo=@$_POST["tipo"];
			if($tipo=='D'){
				$intipo=array('doc','docx','ppt','pptx','xls','xlsx','txt','pdf', 'xml', 'html', 'mpp');
				$carpeta_tipo = 'documento';
			}elseif($tipo=='V'){
				$intipo=array('mp4', 'avi', 'mpeg', 'mkv');
				$carpeta_tipo = 'video';
			}elseif($tipo=='U'){
				$intipo=array('mp3', 'wav', 'aac', 'flac');
				$carpeta_tipo = 'audio';
			}elseif($tipo=='G'){
				$ext = !empty($ext)?$ext:'mp3';
				$file['name'] = @$_POST['nombre_file'].'.'.$ext;
				$intipo=array('mp3', 'aac', 'wav' );
				$carpeta_tipo = 'grabacionvoz';
			}
			if(!in_array($ext, $intipo)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
				exit(0);
			}
			$newname=date('YmdHis').'_'.$file['name'];
			$directorio = RUTA_BASE . 'static' . SD . $folder . SD . $carpeta_tipo ;
			@mkdir($directorio, '0777', true);
			if(move_uploaded_file($file["tmp_name"], $directorio.SD.$newname)){
		   		$this->oNegTarea_archivos->__set('tablapadre', @$_POST["tablapadre"]);
		   		$this->oNegTarea_archivos->__set('idpadre', @$_POST["idpadre"]);
		   		$this->oNegTarea_archivos->__set('nombre', $file["name"]);
				$this->oNegTarea_archivos->__set('ruta', str_replace(RUTA_BASE, '__xRUTABASEx__'.SD, $directorio.SD.$newname));
				$this->oNegTarea_archivos->__set('tipo', $tipo);
				$res=$this->oNegTarea_archivos->agregar();
				echo json_encode(array(
					'code'=>'ok', 
					'msj'=>JrTexto::_('File uploaded successfully'), 
					'idtarea_archivos'=>$res, 
					'ruta'=>$this->documento->getUrlStatic().'/'.$folder.'/'.$carpeta_tipo.'/'.$newname, 
					'nombre'=>$file["name"],
					'tipo'=>$tipo,
				));
				exit(0);
			}
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error tryin to upload the file').'. '.$e->getMessage()));
		}
	}

	public function guardarTarea_archivos()
	{
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			if(empty($_POST)){
				throw new Exception(JrTexto::_("There is no data to register"));
			}
			if($_POST['txtTipo']=='L'){
				$ruta = trim(@$_POST["txtRuta"]);
				if(empty($_POST["txtNombre"])) $_POST['txtNombre'] = $ruta;
				if(strpos($ruta, 'http://')<0 && strpos($ruta, 'https://')<0){ $ruta='http://'.$ruta; }
			}else if($_POST['txtTipo']=='A' || $_POST['txtTipo']=='J' || $_POST['txtTipo']=='E'){
				$ruta = str_replace($this->documento->getUrlBase(), '__xRUTABASEx__', @$_POST["txtRuta"]);
			}

			if(!empty(@$_POST["txtIdtarea_archivos"])) {
				$this->oNegTarea_archivos->idtarea_archivos = @$_POST["txtIdtarea_archivos"];
			}
			$texto=@trim(str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',@$_POST['txtTexto']));
			$this->oNegTarea_archivos->__set('nombre',@$_POST["txtNombre"]);
			$this->oNegTarea_archivos->__set('ruta',$ruta);
			$this->oNegTarea_archivos->__set('tipo',@$_POST['txtTipo']);
			$this->oNegTarea_archivos->__set('texto',@$texto);
			$this->oNegTarea_archivos->__set('tablapadre',@$_POST['txtTablapadre']);
			$this->oNegTarea_archivos->__set('idpadre',@$_POST['txtIdpadre']);
			$this->oNegTarea_archivos->__set('idtarea_archivos_padre',@$_POST['txtIdtarea_archivos_padre']);
			$this->oNegTarea_archivos->__set('puntaje',@$_POST['txtPuntaje']);
			if(empty(@$_POST["txtIdtarea_archivos"])) {
				$res=$this->oNegTarea_archivos->agregar();
			}else{
				$res=$this->oNegTarea_archivos->editar();
			}
			if($_POST['txtTipo']=='A' || $_POST['txtTipo']=='J' || $_POST['txtTipo']=='E'){ $ruta= $ruta.$res; }
			echo json_encode(array(
				'code'=>'ok', 
				'msj'=>JrTexto::_('Attachment saved'), 
				'idtarea_archivos'=>$res, 
				'ruta'=>str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $ruta), 
				'nombre'=>@$_POST["txtNombre"],
				'tipo'=>@$_POST['txtTipo'],
			));
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error tryin to Insert attach').'. '.$e->getMessage()));
		}
	}

	public function xEliminar()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;	
			if(empty($_POST['idtarea_archivos'])){
				throw new Exception(JrTexto::_("There is no data to delete"));
			}
			$this->oNegTarea_archivos->idtarea_archivos=@$_POST['idtarea_archivos'];
			$tipo = $this->oNegTarea_archivos->tipo;
			$ruta = str_replace('__xRUTABASEx__'.SD, RUTA_BASE, $this->oNegTarea_archivos->ruta);
			$res = $this->oNegTarea_archivos->eliminar();
			if(empty($res)){
				throw new Exception(JrTexto::_("The record could not be deleted"));
			}else{
				if($tipo=='D' || $tipo=='V' || $tipo=='G' || $tipo=='U'){
					unlink($ruta);
				}
			}
			echo json_encode(array('code'=>'ok', 'data'=>$res, 'msj'=>JrTexto::_('Attachment deleted')));
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error tryin to Delete attach').'. '.$e->getMessage()));
		}
	}
/*
	// ========================== Funciones xajax ========================== //
	public function xSaveTarea_archivos(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdtarea_archivos'])) {
					$this->oNegTarea_archivos->idtarea_archivos = $frm['pkIdtarea_archivos'];
				}
				
				$this->oNegTarea_archivos->__set('tablapadre',@$frm["txtTablapadre"]);
				$this->oNegTarea_archivos->__set('idpadre',@$frm["txtIdpadre"]);
				$this->oNegTarea_archivos->__set('nombre',@$frm["txtNombre"]);
				$this->oNegTarea_archivos->__set('ruta',@$frm["txtRuta"]);
				$this->oNegTarea_archivos->__set('tipo',@$frm["txtTipo"]);
				$this->oNegTarea_archivos->__set('puntaje',@$frm["txtPuntaje"]);
				$this->oNegTarea_archivos->__set('habilidad',@$frm["txtHabilidad"]);
				$this->oNegTarea_archivos->__set('texto',@$frm["txtTexto"]);
				
			   if(@$frm["accion"]=="Nuevo"){
				    $res=$this->oNegTarea_archivos->agregar();
				}else{
				    $res=$this->oNegTarea_archivos->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegTarea_archivos->idtarea_archivos);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDTarea_archivos(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTarea_archivos->__set('idtarea_archivos', $pk);
				$this->datos = $this->oNegTarea_archivos->dataTarea_archivos;
				$res=$this->oNegTarea_archivos->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTarea_archivos->__set('idtarea_archivos', $pk);
				$res=$this->oNegTarea_archivos->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
*/
}