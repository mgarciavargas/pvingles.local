<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAgenda', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno_logro', RUTA_BASE, 'sys_negocio');
class WebDefecto extends JrWeb
{	
	private $oNegNiveles;
	private $oNegResources;
    protected $oNegMetodologia;
    protected $oNegAlumno_logro;

	public function __construct()
	{
		parent::__construct();
		$this->oNegNiveles = new NegNiveles;
		$this->oNegResources = new NegResources;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegMatricula = new NegAcad_matricula;
        $this->oNegAgenda = new NegAgenda;
        $this->oNegAlumno_logro = new NegAlumno_logro;
        $this->usuarioAct = NegSesion::getUsuario();
	}
	
	public function defecto()
	{
		try {
			global $aplicacion;
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->setTitulo(JrTexto::_('Inicio'), true);
			$rol=$this->usuarioAct["rol"];
			if(strtolower($rol)=="alumno"){

				$this->cursosMatric = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$this->usuarioAct['dni'], 'estado'=>1));

				/* Posibles vistas al antojo del Jefe: */
				#$this->vistaConLogrosYAgenda();
				$this->vistaConHabilidades();
				#$this->vistaDiplomadoTICs();
			}else{
				$this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
				$this->documento->script('circle', '/libs/graficos/progressbar/');
				$this->niveles=$this->oNegNiveles->buscarNiveles(array('tipo'=>'N'));

	            $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
	            $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

	            $usuarioAct = NegSesion::getUsuario();
	           	$this->iddoc=$usuarioAct["dni"];
	           	$this->recursos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'tipo'=>'P','idpersonal'=>$this->iddoc));

	           	$this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
				$this->esquema = 'docente/inicio';
				$this->documento->plantilla = 'inicio';
			}
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function vistaConLogrosYAgenda()
	{
		try {
			$this->documento->script('fullcalendar', '/libs/fullcalendar/');
            $this->documento->script('calendar', '/libs/fullcalendar/');
            $this->documento->script('events', '/libs/fullcalendar/');
        	$this->documento->script('moment.min', '/libs/fullcalendar/');
        	$this->documento->stylesheet('fullcalendar', '/libs/fullcalendar/');
        	$this->documento->script('agenda', '/js/alumno/');
			$this->documento->script('inicio', '/js/alumno/');
			$this->agenda_listado=true;
			
			$this->agenda = array(
				'hoy' => $this->oNegAgenda->buscar(array('fecha_inicio_mayorigual'=>date('Y-m-d', strtotime("now")), 'fecha_inicio_menor'=>date('Y-m-d', strtotime("+1 day")), 'alumno_id'=>$this->usuarioAct['dni'] )),
				'maniana' => $this->oNegAgenda->buscar(array('fecha_inicio_mayorigual'=>date('Y-m-d', strtotime("+1 day")), 'fecha_inicio_menor'=>date('Y-m-d', strtotime("+2 day")), 'alumno_id'=>$this->usuarioAct['dni'] )),
			);
			$this->logros= $this->oNegAlumno_logro->buscar(array('id_alumno'=>$this->usuarioAct['dni']));
			
			$this->esquema = 'alumno/inicio';
			$this->documento->plantilla = 'alumno/general';
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function vistaConHabilidades()
	{
		try {
			$this->documento->script('inicio_new', '/js/alumno/');

			$this->esquema = 'alumno/inicio_new';
			$this->documento->plantilla = 'alumno/general_new';
		} catch (Exception $e) {
			throw new Exception( JrTexto::_($e->getMessage()) );
		}
	}
	
	private function vistaDiplomadoTICs() /* obedece a BD de Ingenio&Talento */
	{
		try {
			#Cargador de clases e Instanciación
			 JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE, 'sys_negocio');
        	 $this->oNegAcad_categorias = new NegAcad_categorias;
        	 
        	$search = $this->oNegAcad_categorias->buscarxIDs(array('idcategoria'=>array(14, 13, 16, 5) ));

        	$this->categorias = array();
        	foreach ($search as $elem) {
        		if($elem["idcategoria"]==14) {
        			$this->categorias[0] = $elem;
        		} else if($elem["idcategoria"]==13) {
        			$this->categorias[1] = $elem;
        		}else if($elem["idcategoria"]==16) {
        			$this->categorias[2] = $elem;
        		}else if($elem["idcategoria"]==5) {
        			$this->categorias[3] = $elem;
        		}
        	}
			ksort($this->categorias);

			$this->esquema = 'alumno/inicio_diplomado';
			$this->documento->plantilla = 'alumno/diplomado';
		} catch (Exception $e) {
			throw new Exception( JrTexto::_($e->getMessage()) );
			
		}
	}

}