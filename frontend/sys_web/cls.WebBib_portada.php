<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-07-2017 
 * @copyright	Copyright (C) 31-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_portada', RUTA_BASE, 'sys_negocio');
class WebBib_portada extends JrWeb
{
	private $oNegBib_portada;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBib_portada = new NegBib_portada;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_portada', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegBib_portada->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_portada'), true);
			$this->esquema = 'bib_portada-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function listadojson()
	{
		try{
			global $aplicacion;
			$datos=$this->oNegBib_portada->buscar(array('estado'=>1));
			if(!empty($datos)) $datos=$datos[0];
			$this->documento->plantilla = 'blanco';
			echo json_encode(array('code'=>'ok','data'=>$datos));
		 	exit(0);		
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_portada', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Bib_portada').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Bib_portada', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegBib_portada->id_portada = @$_GET['id'];
			$this->datos = $this->oNegBib_portada->dataBib_portada;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bib_portada').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegBib_portada->id_portada = @$_GET['id'];
			$this->datos = $this->oNegBib_portada->dataBib_portada;
						
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/cropper.min.js");
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/main3.js");
							$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_portada').' /'.JrTexto::_('see'), true);
			$this->esquema = 'bib_portada-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bib_portada-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function subirarchivo(){
			$this->documento->plantilla = 'returnjson';
			try{
				global $aplicacion;	
				if(empty($_POST["tipo"])||empty($_FILES["filearchivo"])){
					echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
					exit();
				}
				$file=$_FILES["filearchivo"];

				$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
				$intipo='';
				$tipo=@$_POST["tipo"];
				if($tipo=='image')	$intipo=array('jpg','jpeg','png','gif');
				if(!in_array($ext, $intipo)){
					echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
					exit(0);
				}
				$newname=date('Ymdhis').'_'.$file['name'];
				$dir_media = RUTA_BASE . 'static' . SD . 'libreria' . SD . $tipo ;
				@mkdir($dir_media = RUTA_BASE . 'static' . SD . 'libreria' . SD . $tipo,'0777',true);
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newname)){
					echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File Upload success'),'namelink'=>$newname,'nombre'=>$file["name"]));
					exit(0);
				}
			}catch(Exception $e){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function').'. '.$e->getMessage()));
			}
		}
	// ========================== Funciones xajax ========================== //
	public function xSaveBib_portada(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId_portada'])) {
					$this->oNegBib_portada->id_portada = $frm['pkId_portada'];
				}
				JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
				$this->oNegBib_portada->__set('estado',@$frm["txtEstado"]);
				$this->oNegBib_portada->__set('foto',@$frm["hImgPortada"]);
					
				   if(@$frm["accion"]=="Nuevo"){
						$res=$this->oNegBib_portada->agregar();
					}else{
					
						$archivo=basename($frm["txtFoto_old"]);
						if(!empty($frm["txtFoto"])) $txtFoto=NegTools::subirImagen("foto_",@$frm["txtFoto"], "bib_portada",false,100,100 );
						$this->oNegBib_portada->__set('foto',@$txtFoto);
						@unlink(RUTA_SITIO . SD ."static/media/bib_portada/".$archivo);
										    $res=$this->oNegBib_portada->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegBib_portada->id_portada);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDBib_portada(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_portada->__set('id_portada', $pk);
				$this->datos = $this->oNegBib_portada->dataBib_portada;
				$res=$this->oNegBib_portada->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_portada->__set('id_portada', $pk);
				$res=$this->oNegBib_portada->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegBib_portada->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
	     
}