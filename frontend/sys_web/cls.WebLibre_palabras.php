<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-04-2018 
 * @copyright	Copyright (C) 17-04-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegLibre_palabras', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLibre_tema', RUTA_BASE, 'sys_negocio');
class WebLibre_palabras extends JrWeb
{
	private $oNegLibre_palabras;
	private $oNegLibre_tema;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegLibre_palabras = new NegLibre_palabras;
		$this->oNegLibre_tema = new NegLibre_tema;

		$this->usuarioAct = NegSesion::getUsuario();
			
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Libre_palabras', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idpalabra"])&&@$_REQUEST["idpalabra"]!='')$filtros["idpalabra"]=$_REQUEST["idpalabra"];
			if(isset($_REQUEST["idtema"])&&@$_REQUEST["idtema"]!='')$filtros["idtema"]=$_REQUEST["idtema"];
			if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros["palabra"]=$_REQUEST["palabra"];
			if(isset($_REQUEST["audio"])&&@$_REQUEST["audio"]!='')$filtros["audio"]=$_REQUEST["audio"];
			if(isset($_REQUEST["idagrupacion"])&&@$_REQUEST["idagrupacion"]!='')$filtros["idagrupacion"]=$_REQUEST["idagrupacion"];
			
			$this->datos=$this->oNegLibre_palabras->buscar($filtros);
			$this->fkidtema=$this->oNegLibre_tema->buscar();
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Libre_palabras'), true);
			$this->esquema = 'libre_tema/libre_palabras-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Libre_palabras', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}

			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Palabras').' /'.JrTexto::_('New'), true);
			$this->idtema = !empty(@$_REQUEST['idtema']) ? $_REQUEST['idtema'] : 0;
			if($this->idtema===0) {
				return $this->form();
			} else {
				return $this->formXtipo();
			}
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Libre_palabras', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegLibre_palabras->idpalabra = @$_GET['id'];
			$this->datos = $this->oNegLibre_palabras->dataLibre_palabras;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Libre_palabras').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			$this->temas=$this->oNegLibre_tema->buscar();
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'libre_tema/libre_palabras-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function formXtipo()
	{
		try {
			$this->temas = $this->oNegLibre_tema->buscar(array("idtema"=>$this->idtema));
			if(empty($this->temas)) { throw new Exception(JrTexto::_("Tema not found")); }

			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Tema')).': '.$this->temas[0]["nombre"] , 'link'=> '/libre_tema/' ],
                [ 'texto'=> ucfirst(JrTexto::_('Content')).' - '.ucfirst(JrTexto::_('Add')) ],
            ];
			switch (@$this->temas[0]["tipo_contenido"]) {
				case "TABLE":
					$this->oNegLibre_palabras->setLimite(0,99999);
					$this->palabras = $this->oNegLibre_palabras->buscarTable(array("idtema"=>$this->idtema));
					#var_dump($this->palabras);
					$this->documento->script('tinymce.min', '/libs/tinymce/');
					$this->documento->script('chi_audioadd', '/libs/tinymce/plugins/chingo/');
					$this->esquema = 'libre_tema/palabras_table-frm';
					break;
				case "IFRAME":
					$this->palabras = $this->oNegLibre_palabras->buscar(array("idtema"=>$this->idtema));
					if(!empty($this->palabras)){ $this->palabras = $this->palabras[0]; }
					$this->origen = @$this->temas[0]["origen"];
					$this->esquema = 'libre_tema/palabras_iframe-frm';
					break;
				case "HTML":
					$this->documento->script('tinymce.min', '/libs/tinymce/');
					$this->documento->script('chi_imageadd', '/libs/tinymce/plugins/chingo/');
					$this->documento->script('chi_audioadd', '/libs/tinymce/plugins/chingo/');
					$this->documento->script('chi_videoadd', '/libs/tinymce/plugins/chingo/');
					$this->palabras = $this->oNegLibre_palabras->buscar(array("idtema"=>$this->idtema));
					if(!empty($this->palabras)){ $this->palabras = $this->palabras[0]; }
					$this->origen = @$this->temas[0]["origen"];
					$this->esquema = 'libre_tema/palabras_html-frm';
					break;
			}
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	// ========================== Funciones ajax ========================== //
	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Libre_palabras', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpalabra"])&&@$_REQUEST["idpalabra"]!='')$filtros["idpalabra"]=$_REQUEST["idpalabra"];
			if(isset($_REQUEST["idtema"])&&@$_REQUEST["idtema"]!='')$filtros["idtema"]=$_REQUEST["idtema"];
			if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros["palabra"]=$_REQUEST["palabra"];
			if(isset($_REQUEST["audio"])&&@$_REQUEST["audio"]!='')$filtros["audio"]=$_REQUEST["audio"];
			if(isset($_REQUEST["idagrupacion"])&&@$_REQUEST["idagrupacion"]!='')$filtros["idagrupacion"]=$_REQUEST["idagrupacion"];
						
			$this->datos=$this->oNegLibre_palabras->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarLibre_palabras(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdpalabra)) {
				$this->oNegLibre_palabras->idpalabra = @$pkIdpalabra;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
	           	
			$this->oNegLibre_palabras->idtema=@$txtIdtema;
			$this->oNegLibre_palabras->palabra=@$txtPalabra;
			$this->oNegLibre_palabras->audio=@$txtAudio;
			$this->oNegLibre_palabras->idagrupacion=@$txtIdagrupacion;
			$this->oNegLibre_palabras->url_iframe= str_replace($this->documento->getUrlBase(), '__xRUTABASEx__', @$txtUrl_iframe);
					
            $updateTema = $this->oNegLibre_tema->setCampo($txtIdtema, 'origen', @$txtOrigen);
            if($accion=='_add') {
            	$res=$this->oNegLibre_palabras->agregar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Content')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegLibre_palabras->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Content')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function xGuardarArray()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $data = array();
            if(!empty($_POST['arrPalabras'])){
            	$arrPalabras = json_decode($_POST['arrPalabras'], true);
            	foreach ($arrPalabras as $i => $p) {
            		$accion='_add';            
            		if(!empty(@$p['idpalabra'])) {
            			$this->oNegLibre_palabras->idpalabra = $p['idpalabra'];
            			$accion='_edit';
            		}
            		$this->oNegLibre_palabras->idtema=@$p['idtema'];
            		$this->oNegLibre_palabras->palabra=@$p['palabra'];
            		$this->oNegLibre_palabras->audio=str_replace($this->documento->getUrlBase(), '__xRUTABASEx__', $p['audio']);
            		$this->oNegLibre_palabras->idagrupacion=@$p['idagrupacion'];
            		if($accion=='_add') {
            			$idpalabra=$this->oNegLibre_palabras->agregar();
            		}else{
            			$idpalabra=$this->oNegLibre_palabras->editar();
            		}
            		$data[] = array("idcorrelativo"=>$p['idcorrelativo'], "idpalabra"=>$idpalabra);
            	}
            }
            echo json_encode(array('code'=>'ok', 'data'=>$data,'msj'=>'Success' ));
            exit(0);
		} catch (Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}
	// ========================== Funciones xajax ========================== //
	public function xSaveLibre_palabras(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdpalabra'])) {
					$this->oNegLibre_palabras->idpalabra = $frm['pkIdpalabra'];
				}
				
				$this->oNegLibre_palabras->idtema=@$frm["txtIdtema"];
				$this->oNegLibre_palabras->palabra=@$frm["txtPalabra"];
				$this->oNegLibre_palabras->audio=@$frm["txtAudio"];
				$this->oNegLibre_palabras->idagrupacion=@$frm["txtIdagrupacion"];

				if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegLibre_palabras->agregar();
				}else{
					$res=$this->oNegLibre_palabras->editar();
				}
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegLibre_palabras->idpalabra);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDLibre_palabras(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegLibre_palabras->__set('idpalabra', $pk);
				$this->datos = $this->oNegLibre_palabras->dataLibre_palabras;
				$res=$this->oNegLibre_palabras->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegLibre_palabras->__set('idpalabra', $pk);
				$res=$this->oNegLibre_palabras->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegLibre_palabras->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}