<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-07-2017 
 * @copyright	Copyright (C) 24-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUbigeo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_detalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_apoderado', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_metas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_educacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_experiencialaboral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_referencia', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_metas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
class WebAlumno extends JrWeb
{
	private $oNegAlumno;
	private $oNegGeneral;
	private $oNegUbigeo;
	private $oNegUgel;
	private $oNegGrupoaula;
    private $oNegMatricula;
	private $oNegActividad_alumno;
	private $oNegActividad_detalle;
	private $oNegNiveles;
	private $oNegPersonal;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAlumno = new NegAlumno;
		$this->oNegGeneral = new NegGeneral;
		$this->oNegUbigeo = new NegUbigeo;
		$this->oNegUgel = new NegUgel;
        $this->oNegMatricula = new NegAcad_Matricula;
		$this->oNegActividad_alumno = new NegActividad_alumno;
		$this->oNegActividad_detalle = new NegActividad_detalle;
		$this->oNegNiveles = new NegNiveles;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegPerApoderado = new NegPersona_apoderado;
		$this->oNegPerMetas = new NegPersona_metas;
		$this->oNegPerEducacion = new NegPersona_educacion;
		$this->oNegPerExplaboral = new NegPersona_experiencialaboral;
		$this->oNegPerReferencia = new NegPersona_referencia;
		$this->oNegPerMetas = new NegPersona_metas;
		$this->oNegLocal= new NegLocal;
		$this->oNegGrupoaula=New NegAcad_grupoaula;
		$this->oNegGrupoauladetalle=New NegAcad_grupoauladetalle;
	}

	public function defecto(){
		global $aplicacion;	
		return $this->filtros();

	}

	public function filtros(){
		try{
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            $this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->script('moment.min', '/libs/fullcalendar/');
            $this->documento->script('datetimepicker', '/libs/datepicker/');
            $this->documento->script('fullcalendar', '/libs/fullcalendar/');
            $this->documento->script('locale-all', '/libs/fullcalendar/');
            $this->documento->script('calendar', '/libs/fullcalendar/');
            $this->documento->script('events', '/libs/fullcalendar/');
            $this->documento->stylesheet('fullcalendar', '/libs/fullcalendar/');
            $this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Student'), true);
			$filtros=array();
			$this->fkestados=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadogrupo','mostrar'=>1));
			if(!empty($_REQUEST["estadogrupo"]))$filtros["estado"]=$this->fkestado=$_REQUEST["estadogrupo"];

			$this->fktipos=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipogrupo','mostrar'=>1));
			$this->fklocales=$this->oNegLocal->buscar();
			if(!empty($_REQUEST["tipo"])){
				$filtros["tipo"]=$this->fktipo=$_REQUEST["tipo"];
				if($this->fktipo!='V'){
					if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$this->fkidlocal=$_REQUEST["idlocal"];
				}
			}
			$this->anios=$this->oNegGrupoauladetalle->anios();
			$this->idrol=3;
			$this->esquema = 'academico/alumnos';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}

	}

	public function listado()
	{
		try{
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            $this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');

			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$fktipo=ucfirst($_REQUEST["tipo"]);
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$fktipo=ucfirst($_REQUEST["idgrupoaula"]);
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$fktipo=ucfirst($_REQUEST["idgrupoauladetalle"]);

			$filtros["orderby"]="nombre";
			
			$this->datos=$this->oNegMatricula->buscar($filtros);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$vista=!empty($_REQUEST['vista']) ? $_REQUEST['vista'] : 1;
			$this->documento->setTitulo(JrTexto::_('Matricula'), true);
			if(!empty($_REQUEST["enhtml"]))
				$this->esquema = 'usuario/'.$_REQUEST["enhtml"];
			else
			$this->esquema = 'usuario/alumnos-list-vista0'.$vista;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function datosAlumno($dni){
		if(!empty($dni)){
			$persona=$this->oNegPersonal->buscar(array('dni'=>$dni));
			if(!empty($persona[0])) return $persona[0];
			else return false;
		}else return false;
	}

	public function perfil(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Student').' /'.JrTexto::_('Ficha'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->esquema = 'alumno/ficha';
			$usuario=NegSesion::getUsuario();
			$this->oNegAlumno->dni=$usuario['dni'];
			$this->datos=$this->oNegAlumno->dataAlumno;
				if(empty($this->datos)){
					throw new Exception(JrTexto::_('Restricted access').'!!');	
					return;
				}
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function cambiarclave(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Student').' /'.JrTexto::_('Change Password'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->user='alumno';
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!=''){
				$this->oNegAlumno->dni=$_REQUEST["id"];
				$this->datos=$this->oNegAlumno->dataAlumno;
				if(empty($this->datos)){
					throw new Exception(JrTexto::_('Restricted access').'!!');	
					return;
				}
			}else{
				throw new Exception(JrTexto::_('Restricted access').'!!');	
				return;
			}			
			$this->esquema = 'usuario/cambiarclave';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}	
	}

	public function entregamaterial(){
		try{
		global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Student'), true);

			$filtrosgrupo=$filtros=array();
			$this->fktipos=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipogrupo','mostrar'=>1));
			$filtrosgrupo["tipo"]=$this->fktipo=!empty($_REQUEST["tipo"])?$_REQUEST["tipo"]:'';

			$this->gruposaula=!empty($this->fktipo)?$this->oNegGrupoaula->buscar($filtrosgrupo):array();
			$this->idgrupoaula=$filtros["idgrupoaula"]=!empty($_REQUEST["idgrupoaula"])?$_REQUEST["idgrupoaula"]:'';

			$this->gruposauladetalle=array();			
			if(!empty($this->idgrupoaula)){
				/*if($this->fktipo!='V'){
					$this->fklocales=$this->oNegLocal->buscar();
					$filtros["idlocal"]=$this->fkidlocal=!empty($_REQUEST["idlocal"])?$_REQUEST["idlocal"]:'';					
				}*/
				$filtros["search"]=!empty($_REQUEST["texto"])?$_REQUEST["texto"]:'';
				$this->gruposauladetalle=$this->oNegGrupoauladetalle->buscar($filtros);								
			}
			$this->idgrupoauladetalle=!empty($_REQUEST["idgrupoauladetalle"])?$_REQUEST["idgrupoauladetalle"]:'';

			JrCargador::clase('sys_negocio::NegManuales', RUTA_BASE, 'sys_negocio');
			$oNegManuales = new NegManuales;
			$this->materiales=$oNegManuales->buscar();
			$this->idmanual=!empty($_REQUEST["idmanual"])?$_REQUEST["idmanual"]:'';
			$this->idrol=3;//alumnos			
			$this->esquema = 'academico/materiales-filtros';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}

	}

	public function entregamaterialvista(){
		try{
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

            $filtros=array();
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$this->idgrupoaula=$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='') $this->idgrupoauladetalle=$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idmanual"])&&@$_REQUEST["idmanual"]!='') $this->idmanual=$_REQUEST["idmanual"];
			if(isset($_REQUEST["strmanual"])&&@$_REQUEST["strmanual"]!='') $this->strmanual=$_REQUEST["strmanual"];

			$this->matriculados=$this->oNegMatricula->buscar($filtros);
            $this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
            $this->esquema = 'academico/materiales-entrega';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function seentregomaterial($idalumno,$idmanual,$idaulagrupodetalle,$idgrupoaula){
		try{
			global $aplicacion;
				JrCargador::clase('sys_negocio::NegManuales_alumno', RUTA_BASE, 'sys_negocio');
				$oNegManualesAlumno = new NegManuales_alumno;
				$filtros["idalumno"]=$idalumno;
				$filtros["idmanual"]=$idmanual;
				$filtros["idaulagrupodetalle"]=$idaulagrupodetalle;					
				$filtros["idgrupo"]=$idgrupoaula;
				$rs=$oNegManualesAlumno->buscar($filtros);
				if(!empty($rs[0])) return $rs[0];
				else return false;
			}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegAlumno->dni = @$_GET['id'];
			$this->datos = $this->oNegAlumno->dataAlumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->fkubigeo=$this->oNegUbigeo->buscar();
			$this->fkidugel=$this->oNegUgel->buscar();
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'alumno/formulario';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Alumno', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}
			$filtros=array();
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["texto"]!='')$filtros["ape_paterno"]=$_REQUEST["texto"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["texto"]!='')$filtros["ape_materno"]=$_REQUEST["texto"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["texto"]!='')$filtros["nombre"]=$_REQUEST["texto"];
			if(isset($_REQUEST["fechanac"])&&@$_REQUEST["fechanac"]!='')$filtros["fechanac"]=$_REQUEST["fechanac"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["estado_civil"])&&@$_REQUEST["estado_civil"]!='')$filtros["estado_civil"]=$_REQUEST["estado_civil"];
			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"];
			if(isset($_REQUEST["urbanizacion"])&&@$_REQUEST["urbanizacion"]!='')$filtros["urbanizacion"]=$_REQUEST["urbanizacion"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["token"])&&@$_REQUEST["token"]!='')$filtros["token"]=$_REQUEST["token"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegAlumno->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function json_datosxalumno()
	{
		$this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST["idbuscar"]) || empty($_POST["tipo"])) { 
                throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $promXHab = [];
            $this->alumnos = [];
            $filtros["idgrupoauladetalle"]=$_POST["idgrupo"];
            if($_POST['tipo']=='A'){
            	$filtros["idalumno"]=$_POST["idbuscar"];
            } elseif ($_POST['tipo']=='G') {
            	$filtros['idgrupoauladetalle']=$_POST["idbuscar"];
            }
            $exams_alum=$this->oNegMatricula->buscar($filtros);
            foreach ($exams_alum as $al) {
            	$this->oNegAlumno->dni = $al['idalumno'];
            	$alumno=$this->oNegAlumno->getXid();
            	
            	$ultimaActividad=$this->oNegActividad_alumno->ultimaActividadxAlum($al['idalumno']);
            	
            	if($alumno['estado']==1){
            		$new_alum = [];
	            	$new_alum = [
	            		'dni' => $alumno['dni'],
	            		'ape_paterno' => $alumno['ape_paterno'],
	            		'ape_materno' => $alumno['ape_materno'],
	            		'nombre' => $alumno['nombre'],
	            		'email' => $alumno['email'],
	            		'foto' => $alumno['foto'],
	            		'ultima_actividad' => $ultimaActividad,
	            	];
	            	$this->alumnos[] = $new_alum;
            	}
            }
            $data=array('code'=>'ok','data'=>$this->alumnos);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
	}

	public function guardarAlumno(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkDni)) {
				$this->oNegAlumno->dni = $frm['pkDni'];
				$accion='_edit';
			}

            	global $aplicacion;         
            	$usuarioAct = NegSesion::getUsuario();
            	@extract($_POST);
            	JrCargador::clase("sys_negocio::sistema::NegTools", RUTA_SITIO, "sys_negocio/sistema");
				$this->oNegAlumno->ape_paterno=@$txtApe_paterno;
					$this->oNegAlumno->ape_materno=@$txtApe_materno;
					$this->oNegAlumno->nombre=@$txtNombre;
					$this->oNegAlumno->fechanac=@$txtFechanac;
					$this->oNegAlumno->sexo=@$txtSexo;
					$this->oNegAlumno->estado_civil=@$txtEstado_civil;
					$this->oNegAlumno->ubigeo=@$txtUbigeo;
					$this->oNegAlumno->urbanizacion=@$txtUrbanizacion;
					$this->oNegAlumno->direccion=@$txtDireccion;
					$this->oNegAlumno->telefono=@$txtTelefono;
					$this->oNegAlumno->celular=@$txtCelular;
					$this->oNegAlumno->email=@$txtEmail;
					$this->oNegAlumno->idugel=@$txtIdugel;
					$this->oNegAlumno->regusuario=@$txtRegusuario;
					$this->oNegAlumno->regfecha=@$txtRegfecha;
					$this->oNegAlumno->usuario=@$txtUsuario;
					$this->oNegAlumno->clave=@$txtClave;
					$this->oNegAlumno->token=@$txtToken;
					$this->oNegAlumno->estado=@$txtEstado;
					$this->oNegAlumno->situacion=@$txtSituacion;
					
            if($accion=='_add') {
            	$res=$this->oNegAlumno->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Alumno')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAlumno->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Alumno')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveAlumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkDni'])) {
					$this->oNegAlumno->dni = $frm['pkDni'];
				}
				JrCargador::clase("sys_negocio::sistema::NegTools", RUTA_SITIO, "sys_negocio/sistema");
				$this->oNegAlumno->ape_paterno=@$frm["txtApe_paterno"];
					$this->oNegAlumno->ape_materno=@$frm["txtApe_materno"];
					$this->oNegAlumno->nombre=@$frm["txtNombre"];
					$this->oNegAlumno->fechanac=@$frm["txtFechanac"];
					$this->oNegAlumno->sexo=@$frm["txtSexo"];
					$this->oNegAlumno->estado_civil=@$frm["txtEstado_civil"];
					$this->oNegAlumno->ubigeo=@$frm["txtUbigeo"];
					$this->oNegAlumno->urbanizacion=@$frm["txtUrbanizacion"];
					$this->oNegAlumno->direccion=@$frm["txtDireccion"];
					$this->oNegAlumno->telefono=@$frm["txtTelefono"];
					$this->oNegAlumno->celular=@$frm["txtCelular"];
					$this->oNegAlumno->email=@$frm["txtEmail"];
					$this->oNegAlumno->idugel=@$frm["txtIdugel"];
					$this->oNegAlumno->regusuario=@$frm["txtRegusuario"];
					$this->oNegAlumno->regfecha=@$frm["txtRegfecha"];
					$this->oNegAlumno->usuario=@$frm["txtUsuario"];
					$this->oNegAlumno->clave=@$frm["txtClave"];
					$this->oNegAlumno->token=@$frm["txtToken"];
					$this->oNegAlumno->estado=@$frm["txtEstado"];
					$this->oNegAlumno->situacion=@$frm["txtSituacion"];
					
				   if(@$frm["accion"]=="Nuevo"){
					
						$txtFoto=NegTools::subirImagen("foto_",@$frm["txtFoto"], "alumno",false,100,100 );
						$this->oNegAlumno->__set('foto',@$txtFoto);
										    $res=$this->oNegAlumno->agregar();
					}else{
					
						$archivo=basename($frm["txtFoto_old"]);
						if(!empty($frm["txtFoto"])) $txtFoto=NegTools::subirImagen("foto_",@$frm["txtFoto"], "alumno",false,100,100 );
						$this->oNegAlumno->__set('foto',@$txtFoto);
						@unlink(RUTA_SITIO . SD ."static/media/alumno/".$archivo);
										    $res=$this->oNegAlumno->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAlumno->dni);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAlumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAlumno->__set('dni', $pk);
				$this->datos = $this->oNegAlumno->dataAlumno;
				$res=$this->oNegAlumno->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAlumno->__set('dni', $pk);
				$res=$this->oNegAlumno->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAlumno->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}