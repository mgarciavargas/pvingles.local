<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		29-12-2017 
 * @copyright	Copyright (C) 29-12-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAmbiente', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
class WebAmbiente extends JrWeb
{
	private $oNegAmbiente;
	private $oNegLocal;
	private $oNegGeneral;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAmbiente = new NegAmbiente;
		$this->oNegLocal = new NegLocal;
		$this->oNegGeneral = new NegGeneral;			
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idambiente"])&&@$_REQUEST["idambiente"]!='')$filtros["idambiente"]=$_REQUEST["idambiente"];
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["numero"])&&@$_REQUEST["numero"]!='')$filtros["numero"]=$_REQUEST["numero"];
			if(isset($_REQUEST["capacidad"])&&@$_REQUEST["capacidad"]!='')$filtros["capacidad"]=$_REQUEST["capacidad"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["turno"])&&@$_REQUEST["turno"]!='')$filtros["turno"]=$_REQUEST["turno"];
			
			$this->datos=$this->oNegAmbiente->buscar($filtros);
			$this->fkidlocal=$this->oNegLocal->buscar();
			$this->fktipo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipoambiente'));
			$this->fkturno=$this->oNegGeneral->buscar(array('tipo_tabla'=>'turno'));
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Ambiente'), true);
			$this->esquema = 'academico/ambiente';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Ambiente').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			$this->frmaccion='Editar';
			$this->oNegAmbiente->idambiente = @$_GET['id'];
			$this->datos = $this->oNegAmbiente->dataAmbiente;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Ambiente').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->fkidlocal=$this->oNegLocal->buscar();
			$this->fktipo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipoambiente'));
			$this->fkturno=$this->oNegGeneral->buscar(array('tipo_tabla'=>'turno'));			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'academico/ambiente-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			$filtros=array();
			if(isset($_REQUEST["idambiente"])&&@$_REQUEST["idambiente"]!='')$filtros["idambiente"]=$_REQUEST["idambiente"];
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["numero"])&&@$_REQUEST["numero"]!='')$filtros["numero"]=$_REQUEST["numero"];
			if(isset($_REQUEST["capacidad"])&&@$_REQUEST["capacidad"]!='')$filtros["capacidad"]=$_REQUEST["capacidad"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["turno"])&&@$_REQUEST["turno"]!='')$filtros["turno"]=$_REQUEST["turno"];
						
			$this->datos=$this->oNegAmbiente->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarAmbiente(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkIdambiente)) {
				$this->oNegAmbiente->idambiente = $frm['pkIdambiente'];
				$accion='_edit';
			}

            	global $aplicacion;         
            	$usuarioAct = NegSesion::getUsuario();
            	@extract($_POST);
            	
				$this->oNegAmbiente->idlocal=@$txtIdlocal;
					$this->oNegAmbiente->numero=@$txtNumero;
					$this->oNegAmbiente->capacidad=@$txtCapacidad;
					$this->oNegAmbiente->tipo=@$txtTipo;
					$this->oNegAmbiente->estado=@$txtEstado;
					$this->oNegAmbiente->turno=@$txtTurno;
					
            if($accion=='_add') {
            	$res=$this->oNegAmbiente->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Ambiente')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAmbiente->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Ambiente')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function getMisLocales()
	{
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
            $usuarioAct = NegSesion::getUsuario();
			$filtros = array(
				"idlocal" => @$_POST['idlocal'],
				"iddocente" => $usuarioAct['dni'],
			);
			$this->ambientes=$this->oNegAmbiente->getMisLocales($filtros);
			
			$data=array('code'=>'ok','data'=>$this->ambientes);
            echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e) {
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveAmbiente(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdambiente'])) {
					$this->oNegAmbiente->idambiente = $frm['pkIdambiente'];
				}
				
				$this->oNegAmbiente->idlocal=@$frm["txtIdlocal"];
					$this->oNegAmbiente->numero=@$frm["txtNumero"];
					$this->oNegAmbiente->capacidad=@$frm["txtCapacidad"];
					$this->oNegAmbiente->tipo=@$frm["txtTipo"];
					$this->oNegAmbiente->estado=@$frm["txtEstado"];
					$this->oNegAmbiente->turno=@$frm["txtTurno"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAmbiente->agregar();
					}else{
									    $res=$this->oNegAmbiente->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAmbiente->idambiente);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAmbiente(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAmbiente->__set('idambiente', $pk);
				$this->datos = $this->oNegAmbiente->dataAmbiente;
				$res=$this->oNegAmbiente->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAmbiente->__set('idambiente', $pk);
				$res=$this->oNegAmbiente->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAmbiente->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}