<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-11-2016 
 * @copyright	Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividades', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');

JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
class WebActividades extends JrWeb
{
	private $oNegActividades;
	private $oNegNiveles;
	private $oNegMetodologia;
	private $oNegMatricula;
	private $oNegCursoDetalle;
	private $oNegAcad_curso;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegActividades = new NegActividades;
		$this->oNegActividad = new NegActividad;
		$this->oNegNiveles = new NegNiveles;
		$this->oNegMetodologia = new NegMetodologia_habilidad;

		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegCursoDetalle = new NegAcad_cursodetalle;
		$this->oNegAcad_curso = new NegAcad_curso;

		$this->usuarioAct = NegSesion::getUsuario();
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Actividades', 'listar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegActividades->buscar();

			
			/*$this->fknivel=$this->oNegActividades->listarles();
			$this->fkunidad=$this->oNegActividades->listarles();
			$this->fksesion=$this->oNegActividades->listarles();*/
			//$this->fkmetodologia=$this->oNegActividades->listardologia_habilidad();
			//$this->fkhabilidad=$this->oNegActividades->listardologia_habilidad();			
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Actividades'), true);
			$this->esquema = 'actividades-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;	
			
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:$_idnivel;
			
			$this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_GET["txtUnidad"])?$_GET["txtUnidad"]:($_idunidad);
			
			$this->sesiones=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
            $_idsesion=!empty($this->sesiones[0]["idnivel"])?$this->sesiones[0]["idnivel"]:0;
            $this->idsesion=!empty($_GET["txtsesion"])?$_GET["txtsesion"]:($_idsesion);
			
			$this->metodologias=$this->oNegMetodologia->buscar(array('tipo'=>'M'));
            /*$_idmeto=!empty($this->metodologias[0]["idnivel"])?$this->metodologias[0]["idnivel"]:0;
            $this->idmeto=!empty($_GET["txtmetodologia"])?$_GET["txtmetodologia"]:($_idmeto);*/
			
			$this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
			
			
			
					
			if(!NegSesion::tiene_acceso('Actividades', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			
			$this->frmaccion='New';
			$this->documento->setTitulo(JrTexto::_('Actividades').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:$_idnivel;
			
			$this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_GET["txtUnidad"])?$_GET["txtUnidad"]:($_idunidad);
			
			$this->sesiones=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
            $_idsesion=!empty($this->sesiones[0]["idnivel"])?$this->sesiones[0]["idnivel"]:0;
            $this->idsesion=!empty($_GET["txtsesion"])?$_GET["txtsesion"]:($_idsesion);
			
			$this->metodologias=$this->oNegMetodologia->buscar(array('tipo'=>'M'));
			
			$this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
			$_idhabi=!empty($this->habilidades[0]["idnivel"])?$this->habilidades[0]["idnivel"]:0;
            $this->idhabi=!empty($_GET["txtHabilidad"])?$_GET["txtHabilidad"]:($_idhabi);
			
			if(!NegSesion::tiene_acceso('Actividades', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegActividades->idactividad = @$_GET['id'];
			$this->datos = $this->oNegActividades->dataActividades;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Actividades').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegActividades->idactividad = @$_GET['id'];
			$this->datos = $this->oNegActividades->dataActividades;
			
			$this->fknivel=$this->oNegActividades->listarles();
			$this->fkunidad=$this->oNegActividades->listarles();
			$this->fksesion=$this->oNegActividades->listarles();
			$this->fkmetodologia=$this->oNegActividades->listardologia_habilidad();
			$this->fkhabilidad=$this->oNegActividades->listardologia_habilidad();			
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/cropper.min.js");
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/main3.js");
							$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Actividades').' /'.JrTexto::_('see'), true);
			$this->esquema = 'actividades-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			/*$this->fknivel=$this->oNegActividades->listarles();
			$this->fkunidad=$this->oNegActividades->listarles();
			$this->fksesion=$this->oNegActividades->listarles();
			$this->fkmetodologia=$this->oNegActividades->listardologia_habilidad();
			$this->fkhabilidad=$this->oNegActividades->listardologia_habilidad();*/
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'actividades-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}		
	}

	public function practicar()
	{
		try {
			global $aplicacion;
			$this->idCurso = !empty(@$_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
			$this->idCursoDetalle = !empty(@$_REQUEST["iddetalle"])?$_REQUEST["iddetalle"]:0;
			$this->idHabilidad = !empty(@$_REQUEST["idhab"])?$_REQUEST["idhab"]:0;

			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Practice')) , 'link'=> '/actividades/practicar/'.($this->idHabilidad==0?'':'?idhab='.$this->idHabilidad)],
            ];

            $this->habilidades = $this->oNegMetodologia->buscar(array("tipo"=>'H', "estado"=>'1'));
            $this->cursos_nivel = array();
			if($this->idCurso==0) {
				$this->cursos_nivel = $this->oNegMatricula->cursosAlumno(array("idalumno"=>$this->usuarioAct["dni"]));
				$this->breadcrumb[] = [ 'texto'=> ucfirst(JrTexto::_('Courses')), ];
			}


			# Librerias, plugins y scripts
			 $this->documento->script('jquery-ui.min', '/tema/js/');
             $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
             $this->documento->script('slick.min', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/');
             $this->documento->script('cronometro', '/libs/chingo/');
             $this->documento->script('editactividad', '/js/new/');
             $this->documento->script('actividad_completar', '/js/new/');
             $this->documento->script('actividad_ordenar', '/js/new/');
             $this->documento->script('actividad_imgpuntos', '/js/new/');
             $this->documento->script('actividad_verdad_falso', '/js/new/');
             $this->documento->script('actividad_fichas', '/js/new/');
             $this->documento->script('actividad_dialogo', '/js/new/');
             $this->documento->script('manejadores_dby', '/js/new/');
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('jquery.md5', '/tema/js/');            
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('completar', '/js/new/');

             $this->documento->script('audioRecorder','/../media/speach/webapp/js/');
             // $this->documento->script('wavesurfer', '/js/audio/wavesurfer/dist/');
             $this->documento->script('wavesurfer.min', '/libs/audiorecord/');
             $this->documento->script('callbackManager', '/../media/speach/webapp/js/');
             $this->documento->stylesheet('speach', '/js/new/');
             $this->documento->script('speach', '/js/new/');  
             $this->documento->script('pronunciacion', '/js/');

             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('jquery-confirm.min', '/libs/alert/');

			$this->esquema = 'alumno/practicar';
			$this->documento->plantilla = 'alumno/general';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function practicar_old()
	{
		try {
			global $aplicacion;
			$this->idCurso = !empty(@$_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
			$this->idCursoDetalle = !empty(@$_REQUEST["iddetalle"])?$_REQUEST["iddetalle"]:0;
			$this->idHabilidad = !empty(@$_REQUEST["idhab"])?$_REQUEST["idhab"]:0;

			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Practice')) , 'link'=> '/actividades/practicar/'.($this->idHabilidad==0?'':'?idhab='.$this->idHabilidad)],
            ];

            $this->habilidades = $this->oNegMetodologia->buscar(array("tipo"=>'H', "estado"=>'1'));
            $this->cursos_nivel = array();
			if($this->idCurso==0) {
				$this->cursos_nivel = $this->oNegMatricula->cursosAlumno(array("idalumno"=>$this->usuarioAct["dni"]));
				$this->breadcrumb[] = [ 'texto'=> ucfirst(JrTexto::_('Courses')), ];
			} else {
				$curso = $this->oNegAcad_curso->buscar(array("idcurso"=>$this->idCurso));
				$this->breadcrumb[] = [ 'texto'=> @$curso[0]["nombre"], 'link'=> '/actividades/practicar/?idcurso='.$this->idCurso.($this->idHabilidad==0?'':'&idhab='.$this->idHabilidad) ];

				$detalles = $this->oNegCursoDetalle->buscar(array("idcurso"=>$this->idCurso, "idpadre"=>$this->idCursoDetalle));
				
				$arrBreadcrumb = array();
				if(!empty($detalles)){
					foreach ($detalles as $k=>$d) {
						if($d["tiporecurso"]!="E") {
							$this->cursos_nivel[] = $d;
						}
					}
				} else {
					$detalles = $this->oNegCursoDetalle->buscar(array("idcursodetalle"=>$this->idCursoDetalle));
					$arrBreadcrumb[] = @$detalles[0];
					$this->ejercicios = $this->obtenerEjercios_Habilidad();
				}

				if($this->idCursoDetalle!=0) {
					$nivelPadre = @$detalles[0];
					while (@$nivelPadre["idpadre"]!=0){
						$nivelPadre = $this->oNegCursoDetalle->getNivelPadre($nivelPadre);
						$arrBreadcrumb[] = $nivelPadre;
					}

					if(!empty($arrBreadcrumb)) {
						for ($i=count($arrBreadcrumb)-1; $i >= 0 ; $i--) { 
							$lvl = $arrBreadcrumb[$i];
							$this->breadcrumb[] = [ 'texto'=> $lvl["nombre"], 'link'=> '/actividades/practicar/?idcurso='.$this->idCurso.'&iddetalle='.$lvl["idcursodetalle"].($this->idHabilidad==0?'':'&idhab='.$this->idHabilidad) ];
						}
					}
				}
			}


			# Librerias, plugins y scripts
			 $this->documento->script('jquery-ui.min', '/tema/js/');
             $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
             $this->documento->script('slick.min', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/');
             $this->documento->script('cronometro', '/libs/chingo/');
             $this->documento->script('editactividad', '/js/new/');
             $this->documento->script('actividad_completar', '/js/new/');
             $this->documento->script('actividad_ordenar', '/js/new/');
             $this->documento->script('actividad_imgpuntos', '/js/new/');
             $this->documento->script('actividad_verdad_falso', '/js/new/');
             $this->documento->script('actividad_fichas', '/js/new/');
             $this->documento->script('actividad_dialogo', '/js/new/');
             $this->documento->script('manejadores_dby', '/js/new/');
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('jquery.md5', '/tema/js/');            
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('completar', '/js/new/');

             $this->documento->script('audioRecorder','/../media/speach/webapp/js/');
             // $this->documento->script('wavesurfer', '/js/audio/wavesurfer/dist/');
             $this->documento->script('wavesurfer.min', '/libs/audiorecord/');
             $this->documento->script('callbackManager', '/../media/speach/webapp/js/');
             $this->documento->stylesheet('speach', '/js/new/');
             $this->documento->script('speach', '/js/new/');  
             $this->documento->script('pronunciacion', '/js/');

             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('jquery-confirm.min', '/libs/alert/');

			$this->esquema = 'alumno/practicar_old';
			$this->documento->plantilla = 'alumno/general';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	
	public function jxEjerciciosXhabilidad()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$this->idCurso = !empty(@$_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
			$this->idCursoDetalle = !empty(@$_REQUEST["iddetalle"])?$_REQUEST["iddetalle"]:0;
			$this->idHabilidad = !empty(@$_REQUEST["idhab"])?$_REQUEST["idhab"]:0;

			$this->datos = $this->obtenerEjercios_Habilidad();
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
		} catch (Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
            exit(0);
		}
	}
	// ========================== Funciones xajax ========================== //
	public function xSaveActividades(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdactividad'])) {
					$this->oNegActividades->idactividad = $frm['pkIdactividad'];
				}
				JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
				$this->oNegActividades->__set('nivel',@$frm["txtNivel"]);
					$this->oNegActividades->__set('unidad',@$frm["txtunidad"]);
					$this->oNegActividades->__set('sesion',@$frm["txtSesion"]);					
					$this->oNegActividades->__set('metodologia',@$frm["txtMetodologia"]);
					$lista=@$frm["txtHabilidad"];
					//foreach($lista as $indice => $valor){
					for ($i=0;$i<count(@$frm["txtHabilidad"]);$i++){
						if ($i==0){
							@$habi.=@$frm["txtHabilidad"][$i];
						}else{
							@$habi.=" ".@$frm["txtHabilidad"][$i];
						}
					}
					$this->oNegActividades->__set('habilidad',@$habi);
					$this->oNegActividades->__set('titulo',@$frm["txtTitulo"]);
					$this->oNegActividades->__set('descripcion',@$frm["txtDescripcion"]);
					/*$this->oNegActividades->__set('estado',@$frm["txtEstado"]);
					$this->oNegActividades->__set('idioma',@$frm["txtIdioma"]);*/
					
				   if(@$frm["accion"]=="Nuevo"){
					
						/*$txtUrl=NegTools::subirImagen("url_",@$frm["txtUrl"], "actividades",false,100,100 );
						$this->oNegActividades->__set('url',@$txtUrl);*/
										    $res=$this->oNegActividades->agregar();
					}else{
					
						$archivo=basename($frm["txtUrl_old"]);
						if(!empty($frm["txtUrl"])) $txtUrl=NegTools::subirImagen("url_",@$frm["txtUrl"], "actividades",false,100,100 );
						$this->oNegActividades->__set('url',@$txtUrl);
						@unlink(RUTA_SITIO . SD ."static/media/actividades/".$archivo);
										    $res=$this->oNegActividades->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegActividades->idactividad);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
/*
	public function xGetxIDActividades(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegActividades->__set('idactividad', $pk);
				$this->datos = $this->oNegActividades->dataActividades;
				$res=$this->oNegActividades->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
*/
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegActividades->__set('idactividad', $pk);
				$res=$this->oNegActividades->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	// ========================== Funciones Privadas ========================== //
	private function obtenerEjercios_Habilidad()
	{
		try {

			$this->oNegCursoDetalle->idcursodetalle = $this->idCursoDetalle;
            $curso_det = $this->oNegCursoDetalle->getXid();
            $sesion = $this->oNegNiveles->buscar(array('idnivel'=>$curso_det['idrecurso']));
            $s = !empty($sesion)?$sesion[0]['idnivel']:0;
            $unidad = $this->oNegNiveles->buscar(array('idnivel'=>$sesion[0]['idpadre']));
            $u = !empty($unidad)?$unidad[0]['idnivel']:0;
            $nivel = $this->oNegNiveles->buscar(array('idnivel'=>$unidad[0]['idpadre']));
            $n = !empty($nivel)?$nivel[0]['idnivel']:0;


            $actividades=$this->oNegActividad->fullActividades(array("nivel"=>$n,"unidad"=>$u,"sesion"=>$s,"idmetodologia"=>'2', "idhabilidad"=>array($this->idHabilidad) ));

			return $actividades;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
			
		}
	}
}