<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017 
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_categoria', RUTA_BASE, 'sys_negocio');
JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
class WebBib_categoria extends JrWeb
{
	private $oNegBib_categoria;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBib_categoria = new NegBib_categoria;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_categoria', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegBib_categoria->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_categoria'), true);
			$this->esquema = 'bib_categoria-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_categoria', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Bib_categoria').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Bib_categoria', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegBib_categoria->id_categoria = @$_GET['id'];
			$this->datos = $this->oNegBib_categoria->dataBib_categoria;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bib_categoria').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegBib_categoria->id_categoria = @$_GET['id'];
			$this->datos = $this->oNegBib_categoria->dataBib_categoria;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_categoria').' /'.JrTexto::_('see'), true);
			$this->esquema = 'bib_categoria-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bib_categoria-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones xajax ========================== //
	public function xSaveBib_categoria(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId_categoria'])) {
					$this->oNegBib_categoria->id_categoria = $frm['pkId_categoria'];
				}
				
				$this->oNegBib_categoria->__set('descripcion',@$frm["txtDescripcion"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegBib_categoria->agregar();
					}else{
									    $res=$this->oNegBib_categoria->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegBib_categoria->id_categoria);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDBib_categoria(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_categoria->__set('id_categoria', $pk);
				$this->datos = $this->oNegBib_categoria->dataBib_categoria;
				$res=$this->oNegBib_categoria->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_categoria->__set('id_categoria', $pk);
				$res=$this->oNegBib_categoria->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xCategoria(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$this->oNegBib_categoria->setLimite(0,99999);
			$this->datos =$this->oNegBib_categoria->buscar();
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		}catch(Exception $e){
			$data=array('code'=>'Error xAutor','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function xRegistrarCategoria(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata,true);
            $this->oNegBib_categoria->__set('descripcion',$request['descripcion']);
			$res=$this->oNegBib_categoria->agregar();
			echo json_encode(array('code'=>'ok','data'=>$res));
		}catch(Exception $e){
			$data=array('code'=>'Error xRegistrarCategoria','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}   
}