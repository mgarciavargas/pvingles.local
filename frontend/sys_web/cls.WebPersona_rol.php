<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		07-12-2017 
 * @copyright	Copyright (C) 07-12-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE, 'sys_negocio');
class WebPersona_rol extends JrWeb
{
	private $oNegPersona_rol;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona_rol = new NegPersona_rol;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Persona_rol', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["iddetalle"])&&@$_REQUEST["iddetalle"]!='')$filtros["iddetalle"]=$_REQUEST["iddetalle"];
			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$_REQUEST["idrol"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
			
			$this->datos=$this->oNegPersona_rol->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Persona_rol'), true);
			$this->esquema = 'persona_rol-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Persona_rol', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Persona_rol').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Persona_rol', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegPersona_rol->iddetalle = @$_GET['id'];
			$this->datos = $this->oNegPersona_rol->dataPersona_rol;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Persona_rol').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'persona_rol-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Persona_rol', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}
			$filtros=array();
			if(isset($_REQUEST["iddetalle"])&&@$_REQUEST["iddetalle"]!='')$filtros["iddetalle"]=$_REQUEST["iddetalle"];
			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$_REQUEST["idrol"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
						
			$this->datos=$this->oNegPersona_rol->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarPersona_rol(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkIddetalle)) {
				$this->oNegPersona_rol->iddetalle = $frm['pkIddetalle'];
				$accion='_edit';
			}

            	global $aplicacion;         
            	$usuarioAct = NegSesion::getUsuario();
            	@extract($_POST);
            	
				$this->oNegPersona_rol->idrol=@$txtIdrol;
					$this->oNegPersona_rol->idpersonal=@$txtIdpersonal;
					
            if($accion=='_add') {
            	$res=$this->oNegPersona_rol->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Persona_rol')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPersona_rol->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Persona_rol')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSavePersona_rol(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIddetalle'])) {
					$this->oNegPersona_rol->iddetalle = $frm['pkIddetalle'];
				}
				
				$this->oNegPersona_rol->idrol=@$frm["txtIdrol"];
					$this->oNegPersona_rol->idpersonal=@$frm["txtIdpersonal"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegPersona_rol->agregar();
					}else{
									    $res=$this->oNegPersona_rol->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegPersona_rol->iddetalle);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDPersona_rol(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersona_rol->__set('iddetalle', $pk);
				$this->datos = $this->oNegPersona_rol->dataPersona_rol;
				$res=$this->oNegPersona_rol->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersona_rol->__set('iddetalle', $pk);
				$res=$this->oNegPersona_rol->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegPersona_rol->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}