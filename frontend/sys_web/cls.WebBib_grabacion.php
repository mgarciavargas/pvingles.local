<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-05-2017 
 * @copyright	Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_grabacion', RUTA_BASE, 'sys_negocio');
class WebBib_grabacion extends JrWeb
{
	private $oNegBib_grabacion;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBib_grabacion = new NegBib_grabacion;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;
            $usuarioAct = NegSesion::getUsuario();
			if(!NegSesion::tiene_acceso('bib_grabacion', 'list') && $usuarioAct['rol']!='Alumno') {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Record'), true);
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('bib_grabacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/

            /*librerias*/
<<<<<<< HEAD
             $this->documento->script('jquery-ui.min', '/tema/js/');             
             $this->documento->script('wavesurfer.min', '/libs/audiorecord/');
             $this->documento->script('audioRecord', '/libs/audiorecord/');
             $this->documento->script('recorderWorker', '/libs/audiorecord/');
=======
            $this->documento->script('jquery-ui.min', '/tema/js/');
             
            $this->documento->script('callbackManager', '/libs/audiorecord/');
            $this->documento->script('audioRecorder', '/libs/audiorecord/');
            $this->documento->script('audioRecorderWorker', '/libs/audiorecord/');
>>>>>>> 4e1f15ea50aefffe1117f3c69d8cb4c0942da8cb

			$this->frmaccion='Nuevo';
			$this->breadcrumb=JrTexto::_('Add');
			$this->documento->setTitulo(JrTexto::_('record').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('bib_grabacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
            /*librerias*/
            $this->documento->script('jquery-ui.min', '/tema/js/');
             
            $this->documento->script('callbackManager', '/libs/audiorecord/');
            $this->documento->script('audioRecorder', '/libs/audiorecord/');
            $this->documento->script('audioRecorderWorker', '/libs/audiorecord/');

			$usuarioAct = NegSesion::getUsuario();
			$this->frmaccion='Editar';
			$this->breadcrumb=JrTexto::_('Edit');
			$this->pk=@$_GET['id'];
			
			$this->documento->setTitulo(JrTexto::_('record').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver()
    {
		try{
			global $aplicacion;	
            /*librerias*/
             $this->documento->script('jquery-ui.min', '/tema/js/');
             
            $this->documento->script('callbackManager', '/libs/audiorecord/');
            $this->documento->script('audioRecorder', '/libs/audiorecord/');
            $this->documento->script('audioRecorderWorker', 'libs/audiorecord/');

			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'tools/grabacion';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'bib_modal';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function xGuardar()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST)){
				throw new Exception(JrTexto::_('No data to insert'));
			}
			$frm = $_POST;
			if(@$frm["accion"]=="Editar"){
				$this->oNegbib_grabacion->id_grabacion =  @$frm['pkId_grabacion'];
			};
			$usuarioAct = NegSesion::getUsuario();
			$this->oNegbib_grabacion->__set('nombre',@$frm["txtNombre"]);
			$this->oNegbib_grabacion->__set('ruta',@$frm["txtDescripcion"]);
			if(@$frm["accion"]=="Nuevo"){
				$id_grabacion=$this->oNegbib_grabacion->agregar();
			}else{
				$id_grabacion=$this->oNegbib_grabacion->editar();
			}

			$data=array('code'=>'ok','data'=>$id_grabacion);
            echo json_encode($data);
            return parent::getEsquema();
		} catch (Exception $e) {
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}
    public function subirarchivo(){
        $this->documento->plantilla = 'returnjson';
        try{
        
            global $aplicacion; 
            if(empty($_FILES["filearchivo"])){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit();
            }
            $folder = 'libreria';
            $file=$_FILES["filearchivo"];

            $ext=@strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
            $intipo='';
            $tipo=@$_POST["tipo"];
            
            $ext = 'mp3';
            $file['name'] = @$_POST['nombre_file'].'.'.$ext;
            $intipo=array('mp3', 'aac');
            $carpeta_tipo = 'grabacionvoz';

            if(!in_array($ext, $intipo)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
                exit(0);
            }
            $newname=date('YmdHis').'_'.$file['name'];
            $directorio = RUTA_BASE . 'static' . SD . $folder . SD . $carpeta_tipo ;
            @mkdir($directorio, '0777', true);
            if(move_uploaded_file($file["tmp_name"], $directorio.SD.$newname)){
                $this->oNegBib_grabacion->__set('nombre', $file["name"]);
                $this->oNegBib_grabacion->__set('ruta', str_replace(RUTA_BASE, '__xRUTABASEx__'.SD, $directorio.SD.$newname));
                $res=$this->oNegBib_grabacion->agregar();
                echo json_encode(array(
                    'code'=>'ok', 
                    'msj'=>JrTexto::_('File uploaded successfully'), 
                    'id_grabacion'=>$res, 
                    'ruta'=>$this->documento->getUrlStatic().'/'.$folder.'/'.$carpeta_tipo.'/'.$newname, 
                    'nombre'=>$file["name"],
                ));
                exit(0);
            }
        }catch(Exception $e){
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error tryin to upload the file').'. '.$e->getMessage()));
        }
    }

    public function xGrabacion(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$this->datos =$this->oNegBib_grabacion->buscar();
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		}catch(Exception $e){
			$data=array('code'=>'Error xIdioma','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}
	public function Eliminar2(){
        try{
            $this->documento->plantilla = 'returnjson';
			global $aplicacion;	
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata,true);

            $id=$request['id'];

            $res=$this->oNegBib_grabacion->eliminar2($id);
            echo json_encode(array('code'=>'ok','data'=>$res));
            exit;
        }catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
        
    }
	/*public function xEliminar_logica()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST['id_grabacion'])){
				throw new Exception(JrTexto::_('No id_grabacion to delete'));
			}
			$this->oNegbib_grabacion->__set('id_grabacion', $_POST['id_grabacion']);
			$res=$this->oNegbib_grabacion->eliminar_logica();
			if(empty($res)){
				throw new Exception(JrTexto::_('Error').' '.JrTexto::_('Delete Record'));
			}
			$data=array('code'=>'ok','data'=>$_POST['id_grabacion']);
            echo json_encode($data);
            return parent::getEsquema();
		} catch (Exception $e) {
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}*/

/*
	// ========================== Funciones xajax ========================== //
	public function xSavebib_grabacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkid_grabacion'])) {
					$this->oNegbib_grabacion->id_grabacion = $frm['pkid_grabacion'];
				}
				JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
				$this->oNegbib_grabacion->__set('idnivel',@$frm["opcIdnivel"]);
				$this->oNegbib_grabacion->__set('idunidad',@$frm["opcIdunidad"]);
				$this->oNegbib_grabacion->__set('idactividad',@$frm["opcIdactividad"]);
				$this->oNegbib_grabacion->__set('nombre',@$frm["txtNombre"]);
				$this->oNegbib_grabacion->__set('descripcion',@$frm["txtDescripcion"]);
				$this->oNegbib_grabacion->__set('habilidades',@$frm["txtHabilidades"]);
				$this->oNegbib_grabacion->__set('puntajemaximo',@$frm["txtPuntajemaximo"]);
				$this->oNegbib_grabacion->__set('puntajeminimo',@$frm["txtPuntajeminimo"]);
				$this->oNegbib_grabacion->__set('eliminado',@$frm["txtEliminado"]);
				
			    if(@$frm["accion"]=="Nuevo"){
					$txtFoto=NegTools::subirImagen("foto_",@$frm["txtFoto"], "bib_grabacion",false,100,100 );
					$this->oNegbib_grabacion->__set('foto',@$txtFoto);
					$res=$this->oNegbib_grabacion->agregar();
				}else{
					$archivo=basename($frm["txtFoto_old"]);
					if(!empty($frm["txtFoto"])) $txtFoto=NegTools::subirImagen("foto_",@$frm["txtFoto"], "bib_grabacion",false,100,100 );
					$this->oNegbib_grabacion->__set('foto',@$txtFoto);
					@unlink(RUTA_SITIO . SD ."static/media/bib_grabacion/".$archivo);
					$res=$this->oNegbib_grabacion->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegbib_grabacion->id_grabacion);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxid_grabacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegbib_grabacion->__set('id_grabacion', $pk);
				$this->datos = $this->oNegbib_grabacion->databib_grabacion;
				$res=$this->oNegbib_grabacion->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegbib_grabacion->__set('id_grabacion', $pk);
				$res=$this->oNegbib_grabacion->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegbib_grabacion->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
*/	     
}