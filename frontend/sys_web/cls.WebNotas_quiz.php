<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-08-2018 
 * @copyright	Copyright (C) 24-08-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
class WebNotas_quiz extends JrWeb
{
	private $oNegNotas_quiz;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegNotas_quiz = new NegNotas_quiz;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idnota"])&&@$_REQUEST["idnota"]!='')$filtros["idnota"]=$_REQUEST["idnota"];
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			
			$this->datos=$this->oNegNotas_quiz->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Notas_quiz'), true);
			$this->esquema = 'notas_quiz-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Notas_quiz').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegNotas_quiz->idnota = @$_GET['id'];
			$this->datos = $this->oNegNotas_quiz->dataNotas_quiz;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Notas_quiz').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'notas_quiz-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idnota"])&&@$_REQUEST["idnota"]!='')$filtros["idnota"]=$_REQUEST["idnota"];
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
						
			$this->datos=$this->oNegNotas_quiz->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarNotas_quiz(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdnota)) {
				$this->oNegNotas_quiz->idnota = $frm['pkIdnota'];
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	           	
           	$this->oNegNotas_quiz->idcursodetalle = @$idcursodetalle;
           	$this->oNegNotas_quiz->idrecurso = @$idrecurso;
           	$this->oNegNotas_quiz->idalumno = @$idalumno;
           	$this->oNegNotas_quiz->tipo = @$tipo;
           	$this->oNegNotas_quiz->nota = @$nota;
           	$this->oNegNotas_quiz->regfecha = @$regfecha;
					
            if($accion=='_add') {
            	$res=$this->oNegNotas_quiz->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_quiz')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegNotas_quiz->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_quiz')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveNotas_quiz(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdnota'])) {
					$this->oNegNotas_quiz->idnota = $frm['pkIdnota'];
				}
				
				$this->oNegNotas_quiz->idcursodetalle=@$frm["txtIdcursodetalle"];
					$this->oNegNotas_quiz->idrecurso=@$frm["txtIdrecurso"];
					$this->oNegNotas_quiz->idalumno=@$frm["txtIdalumno"];
					$this->oNegNotas_quiz->tipo=@$frm["txtTipo"];
					$this->oNegNotas_quiz->nota=@$frm["txtNota"];
					$this->oNegNotas_quiz->regfecha=@$frm["txtRegfecha"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegNotas_quiz->agregar();
					}else{
									    $res=$this->oNegNotas_quiz->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegNotas_quiz->idnota);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDNotas_quiz(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_quiz->__set('idnota', $pk);
				$this->datos = $this->oNegNotas_quiz->dataNotas_quiz;
				$res=$this->oNegNotas_quiz->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_quiz->__set('idnota', $pk);
				$res=$this->oNegNotas_quiz->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegNotas_quiz->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}