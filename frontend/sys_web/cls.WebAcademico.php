<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-03-2017 
 * @copyright	Copyright (C) 24-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
/*JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_detalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');*/
class WebAcademico extends JrWeb
{
	/*private $oNegAlumno;
    private $oNegGrupo_matricula;
	private $oNegActividad_alumno;
	private $oNegActividad_detalle;
	private $oNegNiveles;*/
	public function __construct()
	{
		parent::__construct();		
		/*$this->oNegAlumno = new NegAlumno;
        $this->oNegGrupo_matricula = new NegGrupo_matricula;
		$this->oNegActividad_alumno = new NegActividad_alumno;
		$this->oNegActividad_detalle = new NegActividad_detalle;
		$this->oNegNiveles = new NegNiveles;*/
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;	
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->setTitulo(JrTexto::_('Academic'), true);
			/*$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
						
			$this->datos=$this->oNegAlumno->buscar();
			*/
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			//$this->documento->setTitulo(JrTexto::_('Alumno'), true);
			$this->esquema = 'academico/inicio';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}  

	public function matricula(){
		try{		
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Matriculate'));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';

			$this->esquema = 'academico/matriculas';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function grupos(){
		try{		
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Matriculate'));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';

			$this->esquema = 'academico/matriculas';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function curso(){
		try{
			//JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
			//$this->ocurso = new NegAcad_curso;		
			global $aplicacion;
			$filtros=array();
			//$this->datos=$this->ocurso->buscar($filtros);

			$this->documento->setTitulo(JrTexto::_('Course'));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';			
			$this->esquema = 'academico/curso';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function examenes(){
		try{			
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			$filtros=array();
			$this->documento->setTitulo(JrTexto::_('SmartQuiz'));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';
            $this->url = URL_SMARTQUIZ.'?id='.@$usuarioAct['usuario'].'&pr='.IDPROYECTO.'&u='.@$usuarioAct['usuario'].'&p='.@$usuarioAct['clave']; 
			$this->esquema = 'academico/examenes';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

}