<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017 
 * @copyright	Copyright (C) 03-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursocategoria', RUTA_BASE, 'sys_negocio');
class WebAcad_categorias extends JrWeb
{
	private $oNegAcad_categorias;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_categorias = new NegAcad_categorias;
		$this->oNegAcad_cursocategoria = new NegAcad_cursocategoria;
			
	}

	public function defecto(){
		global $aplicacion;
		return $aplicacion->error(JrTexto::_("403 Forbiden access"));
	}

	public function cursos()
	{
		try {
			global $aplicacion;
			if(empty($_REQUEST["idcategoria"])) {
				throw new Exception(JrTexto::_("Categoria not found"));
			}
			$this->oNegAcad_categorias->idcategoria = $_REQUEST["idcategoria"];
			$this->categoria = $this->oNegAcad_categorias->dataAcad_categorias;
			$this->cursos = $this->oNegAcad_cursocategoria->buscarCursos(array("idcategoria"=>$this->categoria['idcategoria']));

			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');

			$this->esquema = 'alumno/cursos_diplomado';
			$this->documento->plantilla = 'alumno/diplomado';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}