<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-11-2016 
 * @copyright	Copyright (C) 19-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBiblioteca', RUTA_BASE, 'sys_negocio');
class WebBiblioteca extends JrWeb
{
	private $oNegBiblioteca;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBiblioteca = new NegBiblioteca;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			$type=!empty($_GET["type"])?$_GET["type"]:'';
			$filtros=array();
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
			if(!empty($type)) $filtros=Array('tipo'=>$type);
			$this->datos=$this->oNegBiblioteca->buscar($filtros);			
			$this->documento->plantilla = 'modal';
			//!empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Biblioteca'), true);
			$this->esquema = 'biblioteca';			
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	public function listadojson()
	{
		$this->documento->plantilla = 'returnjson';
		$type=!empty($_POST["type"])?$_POST["type"]:'';
        $nombre=!empty($_POST["texto"])?$_POST["texto"]:'';
		$filtros=array();
		if(!empty($type) && $type!='all' ) $filtros['tipo']=$type;
        if(!empty($nombre) ){$filtros['nombre']=$nombre;}
		$this->datos=$this->oNegBiblioteca->buscar($filtros);
		if(!empty($this->datos)){
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
			exit(0);
		}else{
			echo json_encode(array('code'=>'Error','msj'=>$filtros));//JrTexto::_('File type incorrect')));
			exit(0);
		}
	}

	public function cargarmedia(){
		try {			
			$this->documento->plantilla = 'returnjson';
			if(empty($_POST["tipo"])||empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
			$intipo='';
			$tipo=@$_POST["tipo"];
			if($tipo=='video'){
				$intipo=array('mp4','mov');
			}elseif($tipo=='image'){
				$intipo=array('jpg', 'jpeg','gif','png');
			}elseif($tipo=='audio'){
				$intipo=array('mp3');
			}elseif($tipo=='pdf'){
				$intipo=array('pdf', 'PDF');
			}elseif($tipo=='ppt'){
				$intipo=array('ppt', 'PPT', 'pptx', 'PPTX');
			}
			
			if(!in_array($ext, $intipo)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
				exit(0);
			}
			try{
				//$newname=date("Ymdhis")."_".$file["name"];
				//$ext=substr($file["name"],-4);
				$newname="N".@$_POST["nivel"].'-'."U".@$_POST["unidad"].'-'."A".@$_POST["leccion"].'-'.date("Ymdhis").'.'.$ext;
				
				$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo ;
				@mkdir($dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo,'0777');
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newname)) 
			  	{
			   		global $aplicacion;			
					$usuarioAct = NegSesion::getUsuario();
			   		$this->oNegBiblioteca->__set('nombre',$file["name"]);
					$this->oNegBiblioteca->__set('link',$newname);
					$this->oNegBiblioteca->__set('tipo',$tipo);
					$this->oNegBiblioteca->__set('idpersonal',@$usuarioAct["dni"]);
					$this->oNegBiblioteca->__set('idnivel',@$_POST["nivel"]);
					$this->oNegBiblioteca->__set('idunidad',@$_POST["unidad"]);
					$this->oNegBiblioteca->__set('idleccion',@$_POST["leccion"]);
					$this->oNegBiblioteca->__set('fechareg',date('Y/m/d'));
					$res=$this->oNegBiblioteca->agregar();
			   		echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File Upload success'),'namelink'=>$newname,'nombre'=>$file["name"],'id'=>$res));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error upload File')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}
	}


	public function cargarblob(){
		try {			
			$this->documento->plantilla = 'returnjson';
			if(empty($_POST["type"])||empty($_POST["name"])||empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$type=$_POST["type"];
			$intipo='';
			$tipe_=@explode('/',@$_POST["type"]);
            $tipo=$tipe_[0];
            $ext=$tipe_[1];
			if($tipo=='image'){
				$intipo=array('jpg','gif','png');
			}elseif($tipo=='audio'){
				$intipo=array('mp3','wav');
			}
			$name=@$_POST["name"];			
			if(!in_array($ext, $intipo)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
				exit(0);
			}
			try{

				$newname="N".@$_POST["nivel"].'-'."U".@$_POST["unidad"].'-'."A".@$_POST["leccion"].'-'.date("Ymdhis").".".$ext;
				
				$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo ;
				@mkdir($dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo,'0777');
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newname)) 
			  	{
			   		global $aplicacion;			
					$usuarioAct = NegSesion::getUsuario();
			   		$this->oNegBiblioteca->__set('nombre',$name);
					$this->oNegBiblioteca->__set('link',$newname);
					$this->oNegBiblioteca->__set('tipo',$tipo);
					$this->oNegBiblioteca->__set('idpersonal',@$usuarioAct["dni"]);
					$this->oNegBiblioteca->__set('idnivel',@$_POST["nivel"]);
					$this->oNegBiblioteca->__set('idunidad',@$_POST["unidad"]);
					$this->oNegBiblioteca->__set('idleccion',@$_POST["leccion"]);
					$this->oNegBiblioteca->__set('fechareg',date('Y/m/d'));
					$res=$this->oNegBiblioteca->agregar();
			   		echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File Upload success'),'namelink'=>$newname,'nombre'=>$name,'id'=>$res));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error upload File')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}

	}

	public function subirblob(){
		try {
			global $aplicacion;			
			$this->documento->plantilla = 'returnjson';
			if(empty($_POST["type"])||empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$type=$_POST["type"];
			$intipo='';
			$tipe_=@explode('/',@$_POST["type"]);
            $tipo=$tipe_[0];
            $ext=$tipe_[1];
			if($tipo=='image'){
				$intipo=array('jpg','gif','png');
			}elseif($tipo=='audio'){
				$intipo=array('mp3','wav');
			}
			$name=!empty($_POST["name"])?$_POST["name"]:date("Ymdhis");
			if(!in_array($ext, $intipo)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
				exit(0);
			}
			try{

				$usuarioAct = NegSesion::getUsuario();
				$newname=$usuarioAct["dni"]."_".$name.".".$ext;	
				$rutatmp=SD.'media'.SD."record".SD.$tipo;
				$dir_media = RUTA_BASE.'static'.$rutatmp;
				$linkbase=$dir_media. SD.$newname;
				if(is_file($linkbase))@unlink($linkbase);
				@mkdir($dir_media = RUTA_BASE . 'static' . $rutatmp,'0777');				
				if(move_uploaded_file($file["tmp_name"],$linkbase)) 
			  	{			   			
			   		$namelink='static/media/record/'.$tipo."/".$newname;		
			   		echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File Upload success'),'namelink'=>$namelink,'nombre'=>$name));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error upload File')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}

	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$link = $args[1];
				$tipo = $args[2];
				$this->oNegBiblioteca->__set('idbiblioteca', $pk);
				$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo.SD ;
				@unlink($dir_media.$link);
				$res=$this->oNegBiblioteca->eliminar();
				if(!empty($res)){
                    $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Delete record success')), 'success');
					$oRespAjax->setReturnValue($res);
				}else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}


	/************* Modulo Biblioteca - Enny ****************/
	public function modulo_biblioteca(){
		header('Location: '.$this->documento->getUrlBase().'/modbiblioteca');
	}

	public function getVarsGlobales(){
		$this->documento->plantilla = 'returnjson';
		$usuario = NegSesion::getUsuario();
		if(empty($usuario)) {header('Location: '.$this->documento->getUrlBase());}
		$url_base = $this->documento->getUrlBase();
		$url_static = $this->documento->getUrlStatic();
		echo json_encode(array('code'=>'ok', 'data'=>array('usuario'=>$usuario, 'url_base'=>$url_base, 'url_static'=>$url_static)));
	}

}