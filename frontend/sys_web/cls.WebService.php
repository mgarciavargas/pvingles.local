<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursohabilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
class WebService extends JrWeb
{
	private $oNegPersonal;
	private $oNegGrupoAulaDet;
	private $oNegCurso;
	private $oNegMatricula;
	private $oNegCursoDetalle;
	private $oNegCursoHabilidad;
	private $oNegNotas_quiz;

	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersonal = new NegPersonal;
		$this->oNegGrupoAulaDet = new NegAcad_grupoauladetalle;
		$this->oNegCurso = new NegAcad_curso;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegCursoDetalle = new NegAcad_cursodetalle;
		$this->oNegCursoHabilidad = new NegAcad_cursohabilidad;
		$this->oNegNotas_quiz = new NegNotas_quiz;
	}

	public function defecto(){
		global $aplicacion;
		return $aplicacion->error("403 - You don't have permission to access this server.");
	}


	/** 
	* -- Service::cursos_docente --
	* Retorna listado de cursos que dicta un solo docente.
	* @param iddocente (obligatorio) : dni del docente a buscar.
	*/
	public function cursos_docente(){
		try {
			global $aplicacion;
			$this->documento->plantilla = 'returnjson';       
			if(!$this->estaAfiliado()){ throw new Exception(JrTexto::_("Not affiliated")); }
			if( $this->faltanParams(array("iddocente")) ){ exit(0);  }

			$data = array();
			$iddocente = @$_GET['iddocente'];
			$now = date('Y-m-d H:i:s');
			$grupos_det = $this->oNegGrupoAulaDet->buscar(array('iddocente'=>$iddocente, 'fecha_inicio_menorigual'=>$now, 'fecha_final_mayorigual'=>$now));
			foreach ($grupos_det as $gdet) {
				$curso = $this->oNegCurso->buscar(array('idcurso'=>$gdet['idcurso']));
				if(!empty($curso)) {
					$curso[0]["imagen"] = $this->documento->getUrlBase().$curso[0]["imagen"];
					$curso[0]["idgrupoauladetalle"] = $gdet['idgrupoauladetalle'];
					$curso[0]["idgrupoaula"] = $gdet['idgrupoaula'];

					$data[] = $curso[0];
				}
			}
			
			$response = array(
				'code' => 200,
				'message' => 'Success',
				'data' => $data
			);
			echo json_encode($response);
	        return parent::getEsquema();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit(0);
		}
	}

	/** 
	* -- Service::alumnos_curso --
	* Retorna listado de alumnos matriculados en un curso.
	* @param iddocente (obligatorio) : dni del docente a buscar.
	* @param idcurso (obligatorio) : id del curso.
	*/
	public function alumnos_curso(){
		try {
			global $aplicacion;
			$this->documento->plantilla = 'returnjson';       
			if(!$this->estaAfiliado()){ throw new Exception(JrTexto::_("Not affiliated")); }
			if( $this->faltanParams(array("iddocente", "idcurso")) ){ exit(0);  }

			$data = array();
			$iddocente = @$_GET['iddocente'];
			$idcurso = @$_GET['idcurso'];
			$now = date('Y-m-d H:i:s');
			$grupos_det = $this->oNegGrupoAulaDet->buscar(array('idcurso'=>$idcurso, 'iddocente'=>$iddocente, 'fecha_inicio_menorigual'=>$now, 'fecha_final_mayorigual'=>$now));
			if (!empty($grupos_det)) { 
				$grupos_det = $grupos_det[0]; 
				$data = $this->oNegMatricula->buscar( array("idgrupoauladetalle"=>$grupos_det["idgrupoauladetalle"]) );
			}


			
			$response = array(
				'code' => 200,
				'message' => 'Success',
				'data' => $data
			);
			echo json_encode($response);
	        return parent::getEsquema();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit(0);
		}
	}

	/** 
	* -- Service::niveles_curso --
	* Retorna listado de niveles de un determinado curso.
	* @param idcurso (obligatorio) : id del curso.
	*/
	public function niveles_curso(){
		try {
			global $aplicacion;
			$this->documento->plantilla = 'returnjson';       
			if(!$this->estaAfiliado()){ throw new Exception(JrTexto::_("Not affiliated")); }
			if( $this->faltanParams(array("idcurso")) ){ exit(0);  }

			$data = array();
			$idcurso = @$_GET['idcurso'];
			$niveles = $this->oNegCursoDetalle->buscar( array('tiporecurso'=>'N', 'idcurso'=>$idcurso, 'orderby'=>array('orden ASC')) );
			if (!empty($niveles)) { 
				foreach ($niveles as $n) {
					$elem = array(
						"idcurso" => $n["idcurso"],
						"idcursodetalle" => $n["idcursodetalle"],
						"idrecurso" => $n["idrecurso"],
						"orden" => $n["orden"],
						"nombre" => $n["nombre"]
					);
					$data[] = $elem; 
				}
			}


			$response = array(
				'code' => 200,
				'message' => 'Success',
				'data' => $data
			);
			echo json_encode($response);
	        return parent::getEsquema();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit(0);
		}
	}

	/** 
	* -- Service::unidades_curso --
	* Retorna listado de unidades de un curso y nivel determinado.
	* @param idcurso (obligatorio) : id del curso.
	* @param idpadre (opcional) : id del que contiene las unidades. (default 0)
	*/
	public function unidades_curso(){
		try {
			global $aplicacion;
			$this->documento->plantilla = 'returnjson';       
			if(!$this->estaAfiliado()){ throw new Exception(JrTexto::_("Not affiliated")); }
			if( $this->faltanParams(array("idcurso")) ){ exit(0);  }

			$data = array();
			$idcurso = @$_GET['idcurso'];
			$idpadre = !empty(@$_GET['idpadre'])?@$_GET['idpadre']:0;
			$nivel = $this->oNegCursoDetalle->buscar( array('idcurso'=>$idcurso, 'idrecurso'=>$idpadre) );
			$unidades = $this->oNegCursoDetalle->buscar( array('tiporecurso'=>'U', 'idcurso'=>$idcurso, 'idpadre'=>@$nivel[0]['idcursodetalle'] , 'orderby'=>array('orden ASC')) );
			if (!empty($unidades)) { 
				foreach ($unidades as $u) {
					$elem = array(
						"idcurso" => $u["idcurso"],
						"idcursodetalle" => $u["idcursodetalle"],
						"idrecurso" => $u["idrecurso"],
						"orden" => $u["orden"],
						"nombre" => $u["nombre"],
					);
					$data[] = $elem; 
				}
			}


			$response = array(
				'code' => 200,
				'message' => 'Success',
				'data' => $data
			);
			echo json_encode($response);
	        return parent::getEsquema();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit(0);
		}
	}

	/**
	* -- Service::habilidades_curso --
	* Retorna lista de habilidades (indicadores u objetivos) de un curso.
	* Generalmente usado para SmartQuiz
	* @param idcurso (obligatorio) : id del curso.
	*/
	public function habilidades_curso() {
		try {
			global $aplicacion;
			$this->documento->plantilla = 'returnjson';       
			if(!$this->estaAfiliado()){ throw new Exception(JrTexto::_("Not affiliated")); }
			if( $this->faltanParams(array("idcurso")) ){ exit(0);  }
			$data = array();
			$idcurso = @$_GET['idcurso'];

			$habilidades = $this->oNegCursoHabilidad->buscar(array("idcurso"=>$idcurso));
			foreach ($habilidades as $hab) {
				$node = array(
					"skill_id"=> $hab["idcursohabilidad"],
					"skill_name"=> $hab["texto"],
				);
				$data[] = $node;
			};

			$response = array(
				'code' => 200,
				'message' => 'Success',
				'data' => $data
			);
			echo json_encode($response);
	        return parent::getEsquema();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit(0);
		}
	}

	/**
	* -- Service::guardarNota_quiz --
	* Recibe un resultado de Alumno de Smartquiz y actualiza la tabla Notas_quiz.
	* @param idexamen (obligatorio) : id del examen.
	* @param idalumno (obligatorio) : id del alumno.
	* @param nota (obligatorio) : nota del resultado.
	* @param fecha (obligatorio) : fecha qu rindió examen.
	*/
	public function guardarNota_quiz()
	{
		try {
			global $aplicacion;
			$this->documento->plantilla = 'returnjson'; 
			if( $this->faltanParams(array("idexamen", "idalumno", "nota", "fecha")) ){ exit(0);  }

            @extract($_POST);
            $accion='_add';

            $searchPersonal = $this->oNegPersonal->buscar(array("usuario"=>$idalumno));
            if(empty($searchPersonal)) {
            	throw new Exception(JrTexto::_("Persona not found"));            	
            }
            $personal = $searchPersonal[0];

			$searchNota = $this->oNegNotas_quiz->buscar(array("idrecurso"=>$idexamen, "idalumno"=>$personal["dni"]));
			if(!empty($searchNota)) {
				$this->oNegNotas_quiz->idnota = $searchNota[0]['idnota'];
				$accion='_edit';
			}

           	$this->oNegNotas_quiz->idcursodetalle = @$idcursodetalle;
           	$this->oNegNotas_quiz->idrecurso = @$idexamen;
           	$this->oNegNotas_quiz->idalumno = @$personal["dni"];
           	$this->oNegNotas_quiz->tipo = !empty(@$tipo)?$tipo:'-';
           	$this->oNegNotas_quiz->nota = @$nota;
           	$this->oNegNotas_quiz->regfecha = @$fecha;

           	if($accion=='_add') {
            	$res=$this->oNegNotas_quiz->agregar();
           	} else {
            	$res=$this->oNegNotas_quiz->editar();
           	}

			$response = array(
				'code' => 200,
				'message' => 'Success',
				'data' => $res
			);
			echo json_encode($response);
	        return parent::getEsquema();
		} catch (Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);			
			echo json_encode($response);exit(0);
		}
	}

	/*================= FUNCIONES PRIVADAS =================*/
	private function estaAfiliado($params = null)
	{
		try {
			/* Identificar si tiene accessos a los servicios de smartLearn segun el/los parametro/s $_GET que corresponda/n */

			return true;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function faltanParams($arrParams = array())
	{
		try {
			$paramsMissing = array();
			foreach ($arrParams as $param) {
				if (!array_key_exists($param, $_REQUEST)) {
					$paramsMissing[] = " '".$param."' ";
				}
			}

			if (!empty($paramsMissing)) {
				throw new Exception( JrTexto::_("Params are missing").': '.implode(' , ', $paramsMissing) );
			}

			return false;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}