<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamenes', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
class WebRecursos extends JrWeb
{
	private $oNegActividad;
    private $oNegNiveles;
    private $oNegMetodologia;	
    private $oNegResources;

	public function __construct()
	{
		parent::__construct();			
        $this->oNegNiveles = new NegNiveles;
        $this->oNegResources = new NegResources;
	}

	public function defecto(){
		try{
			
        }catch(Exception $e) {
             return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}

	public function ver(){
		try{
			global $aplicacion;
		    $idnivel_=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
            $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

            $_idunidad=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
            $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $idunidad_=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_idunidad)?$_idunidad:($idunidad_);

            $_idactividad=!empty(JrPeticion::getPeticion(4))?JrPeticion::getPeticion(4):0;
            $this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
           	$idactividad_=!empty($this->actividades[0]["idnivel"])?$this->actividades[0]["idnivel"]:0;
           	$this->idactividad=!empty($_idactividad)?$_idactividad:($idactividad_);
           	
           	$usuarioAct = NegSesion::getUsuario();
           	$this->iddoc=(!empty($_GET["iddoc"])&&@$_GET["iddoc"]!='00000000')?$_GET["iddoc"]:$usuarioAct["dni"];
           	$this->recursos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'P','idpersonal'=>$this->iddoc));
         	
			$this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
			$this->documento->script('circle', '/libs/graficos/progressbar/');          
			$this->documento->setTitulo(JrTexto::_('Resource'), true);
	        $this->esquema = 'docente/misrecursos';
	        return parent::getEsquema();
	    }catch(Exception $e) {
             return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}



	public function listar(){
		try{			
            $idnivel_=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
            $this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
            $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

            $idunidad_=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
            $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($idunidad_)?$idunidad_:($_idunidad);  

            $this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
            $_idactividad=!empty($this->sesiones[0]["idnivel"])?$this->sesiones[0]["idnivel"]:0;

            $this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->setTitulo(JrTexto::_('Resource'), true);
	        //$this->esquema = 'doc_recursos_listar';
			$usuarioAct = NegSesion::getUsuario();
			$rolActivo=$usuarioAct["rol"];
			if($rolActivo!='Alumno'){
				$this->esquema = 'docente/actividades_listar';				
			}else{
				$filtros["idnivel"]=$this->idnivel;
				$filtros["idunidad"]=$this->idunidad;
				$filtros["idactividad"]=$this->idactividad;
				$filtros["tipo"]=$this->tipo;
				
	        	$this->esquema = 'alumno/sesiones';
			}

	        return parent::getEsquema();
        }catch(Exception $e) {
             return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}

	public function xGetxPadre(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];
				$datos=$this->oNegNiveles->buscar($filtro);				
				$oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xCargarRecurso(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];
				$datos=$this->oNegResources->buscar($filtro);

				return $oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
    {
        if(is_a($oRespAjax, 'xajaxResponse')) {
            try {
                if(empty($args[0])) { return;}
                $this->oNegResources->setCampo($args[0],$args[1],$args[2]);
                return $oRespAjax->setReturnValue(true);
            } catch(Exception $e) {
               return  $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
            } 
        }
    }

}