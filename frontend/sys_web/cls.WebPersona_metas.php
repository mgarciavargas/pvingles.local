<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-01-2018 
 * @copyright	Copyright (C) 04-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona_metas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
class WebPersona_metas extends JrWeb
{
	private $oNegPersona_metas;		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona_metas = new NegPersona_metas;
		$this->oNegPersonal = new NegPersonal;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Persona_metas', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idmeta"])&&@$_REQUEST["idmeta"]!='')$filtros["idmeta"]=$_REQUEST["idmeta"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["meta"])&&@$_REQUEST["meta"]!='')$filtros["meta"]=$_REQUEST["meta"];
			if(isset($_REQUEST["objetivo"])&&@$_REQUEST["objetivo"]!='')$filtros["objetivo"]=$_REQUEST["objetivo"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			
			$this->datos=$this->oNegPersona_metas->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Persona_metas'), true);
			$this->esquema = 'persona_metas-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Persona_metas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Persona_metas').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Persona_metas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegPersona_metas->idmeta = @$_GET['id'];
			$this->datos = $this->oNegPersona_metas->dataPersona_metas;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Persona_metas').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'persona_metas-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Persona_metas', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}
			$filtros=array();
			if(isset($_REQUEST["idmeta"])&&@$_REQUEST["idmeta"]!='')$filtros["idmeta"]=$_REQUEST["idmeta"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["meta"])&&@$_REQUEST["meta"]!='')$filtros["meta"]=$_REQUEST["meta"];
			if(isset($_REQUEST["objetivo"])&&@$_REQUEST["objetivo"]!='')$filtros["objetivo"]=$_REQUEST["objetivo"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
						
			$this->datos=$this->oNegPersona_metas->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarPersona_metas(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idmeta)) {
				$this->oNegPersona_metas->idmeta = $idmeta;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
			$this->oNegPersona_metas->idpersona=@$idpersona;
			$this->oNegPersona_metas->meta=@$meta;
			$this->oNegPersona_metas->objetivo=@$objetivo;
			$this->oNegPersona_metas->mostrar=!empty($mostrar)?$mostrar:1;
					
            if($accion=='_add') {
            	$res=$this->oNegPersona_metas->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Persona_metas')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPersona_metas->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Persona_metas')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSavePersona_metas(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdmeta'])) {
					$this->oNegPersona_metas->idmeta = $frm['pkIdmeta'];
				}
				
				$this->oNegPersona_metas->idpersona=@$frm["txtIdpersona"];
					$this->oNegPersona_metas->meta=@$frm["txtMeta"];
					$this->oNegPersona_metas->objetivo=@$frm["txtObjetivo"];
					$this->oNegPersona_metas->mostrar=@$frm["txtMostrar"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegPersona_metas->agregar();
					}else{
									    $res=$this->oNegPersona_metas->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegPersona_metas->idmeta);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDPersona_metas(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersona_metas->__set('idmeta', $pk);
				$this->datos = $this->oNegPersona_metas->dataPersona_metas;
				$res=$this->oNegPersona_metas->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersona_metas->__set('idmeta', $pk);
				$res=$this->oNegPersona_metas->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegPersona_metas->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}