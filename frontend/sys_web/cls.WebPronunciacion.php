<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-10-2017 
 * @copyright	Copyright (C) 16-10-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPronunciacion', RUTA_BASE, 'sys_negocio');
class WebPronunciacion extends JrWeb
{
	private $oNegPronunciacion;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPronunciacion = new NegPronunciacion;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!='')$filtros["id"]=$_REQUEST["id"];
			if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros["palabra"]=$_REQUEST["palabra"];
			if(isset($_REQUEST["pron"])&&@$_REQUEST["pron"]!='')$filtros["pron"]=$_REQUEST["pron"];
			
			$this->datos=$this->oNegPronunciacion->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Pronunciacion'), true);
			$this->esquema = 'pronunciacion-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;		
			
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Pronunciacion').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			$this->frmaccion='Editar';
			$this->oNegPronunciacion->id = @$_GET['id'];
			$this->datos = $this->oNegPronunciacion->dataPronunciacion;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Pronunciacion').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'pronunciacion-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$filtros=array();
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!='')$filtros["id"]=$_REQUEST["id"];
			if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros["palabra"]=$_REQUEST["palabra"];
			if(isset($_REQUEST["pron"])&&@$_REQUEST["pron"]!='')$filtros["pron"]=$_REQUEST["pron"];
			if(!empty($_REQUEST["palabra"])){
				$palabras = explode(" ",$_REQUEST["palabra"]);
				if(count($palabras)-1==0&& $palabras[0]!=''){
					$this->datos=$this->oNegPronunciacion->buscar($filtros);
				}else if(count($palabras)>1){
					$frase=[];					
					foreach ($palabras as $p){
						if(trim($p)!=''){
							$filtros["palabra"]=$p;
							$dt=$this->oNegPronunciacion->buscar($filtros);
							if(!empty($dt[0])){
								$frase[$dt[0]["palabra"]]=$dt[0]["pron"];
							}else{
								$frase[$p]=$p;
							}
						}
					}
					$this->datos=$frase;
				}
			}else if(!empty($_REQUEST["pron"])){
				$pronun = explode("\\r\\n",$_REQUEST["pron"]);				
				if(count($pronun)-1==0&& $pronun[0]!=''){
					$this->datos=$this->oNegPronunciacion->buscar($filtros);
				}else if(count($pronun)>1){
					$frase=[];					
					foreach ($pronun as $p){
						if(trim($p)!=''){
							$filtros["pron"]=$p;
							$dt=$this->oNegPronunciacion->buscar($filtros);
							if(!empty($dt[0])){
								$frase[$dt[0]["palabra"]]=$dt[0]["pron"];
							}else{
								$frase[$p]=$p;
							}
						}							
					}
					$this->datos=$frase;
				}
			}else
				$this->datos=$this->oNegPronunciacion->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function buscarpronunciacionjson(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'blanco';
			if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros["palabra"]=$_REQUEST["palabra"];
			$this->datos=$this->oNegPronunciacion->buscarpronunciacion($_REQUEST["palabra"]);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
			exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }

	}



	public function guardarPronunciacion(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkId)) {
				$this->oNegPronunciacion->id = $frm['pkId'];
				$accion='_edit';
			}

            	global $aplicacion;         
            	$usuarioAct = NegSesion::getUsuario();
            	@extract($_POST);
            	
				$this->oNegPronunciacion->palabra=@$txtPalabra;
					$this->oNegPronunciacion->pron=@$txtPron;
					
            if($accion=='_add') {
            	$res=$this->oNegPronunciacion->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Pronunciacion')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPronunciacion->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Pronunciacion')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSavePronunciacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId'])) {
					$this->oNegPronunciacion->id = $frm['pkId'];
				}
				
				$this->oNegPronunciacion->palabra=@$frm["txtPalabra"];
					$this->oNegPronunciacion->pron=@$frm["txtPron"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegPronunciacion->agregar();
					}else{
									    $res=$this->oNegPronunciacion->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegPronunciacion->id);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDPronunciacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPronunciacion->__set('id', $pk);
				$this->datos = $this->oNegPronunciacion->dataPronunciacion;
				$res=$this->oNegPronunciacion->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPronunciacion->__set('id', $pk);
				$res=$this->oNegPronunciacion->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegPronunciacion->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}