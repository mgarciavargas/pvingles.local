<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegReportealumno', RUTA_BASE, 'sys_negocio');
class WebReportealumno extends JrWeb
{	
	
	public function __construct()
	{
		parent::__construct();
		$this->oNegReportealumno = new NegReportealumno;
	}
	
	public function defecto()
	{
		try {
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Dashboard'), true);
			$this->documento->plantilla = 'mantenimientos';
			$this->esquema = 'menu_reportealumno';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function repor_tiempoe()
	{
		try {
			global $aplicacion;

			//$this->documento->script('loader', '/libs/demochart/');

			$usuarioAct = NegSesion::getUsuario();
			
			//echo $usuarioAct['dni'];
			$this->lista_tiempo=$this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','idusuario'=>$usuarioAct['dni']));
			$this->lista_tiempoc=$this->oNegReportealumno->buscar(array('tipo'=>'tiempo_curso','idusuario'=>$usuarioAct['dni']));			
			//$this->lista_tiempoc=$this->oNegReportealumno->buscar(array('tipo'=>'examen_s','idusuario'=>$usuarioAct['dni']));

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte de Tiempo de Estudio'), true);
			$this->esquema = 'reportealumno/repor_tiempoe';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function repor_examene()
	{
		try {
			global $aplicacion;

			//$this->documento->script('loader', '/libs/demochart/');

			$usuarioAct = NegSesion::getUsuario();
			
			$this->lista_examene=$this->oNegReportealumno->buscar(array('tipo'=>'examen_e','idusuario'=>$usuarioAct['dni']));
			

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte de Examen de entrada'), true);
			$this->esquema = 'reportealumno/repor_examene';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function repor_examens()
	{
		try {
			global $aplicacion;

			//$this->documento->script('loader', '/libs/demochart/');

			$usuarioAct = NegSesion::getUsuario();
			
			$this->lista_examene=$this->oNegReportealumno->buscar(array('tipo'=>'examen_s','idusuario'=>$usuarioAct['dni']));
			

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte de Examen de Salida'), true);
			$this->esquema = 'reportealumno/repor_examene';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function repor_productividad()
	{
		try {
			global $aplicacion;

			//$this->documento->script('loader', '/libs/demochart/');

			$usuarioAct = NegSesion::getUsuario();
			
			$this->lista_curso=$this->oNegReportealumno->buscar(array('tipo'=>'productividad_curso','idusuario'=>$usuarioAct['dni']));
			
			$this->notase=[];
			$this->notass=[];

			if(!empty($this->lista_curso))
        	foreach ($this->lista_curso as $lista1){
        		$idcurso=$lista1['idcurso'];
        		$notas=$this->lista_curso2=$this->oNegReportealumno->buscar(array('tipo'=>'productividad_nota','idusuario'=>$usuarioAct['dni'],'idcurso'=>$lista1['idcurso'],'tipo2'=>"E"));
        		$this->notase[$idcurso]= $notas[0]["nota"];

        		$notas=$this->lista_curso2=$this->oNegReportealumno->buscar(array('tipo'=>'productividad_nota','idusuario'=>$usuarioAct['dni'],'idcurso'=>$lista1['idcurso'],'tipo2'=>"S"));
        		$this->notass[$idcurso]= $notas[0]["nota"];

        		//echo $notas[0]["nota"];
        	}

			/**/
			

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte de Productividad'), true);
			$this->esquema = 'reportealumno/repor_productividad';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function repor_progreso()
	{
		try {
			global $aplicacion;

			//$this->documento->script('loader', '/libs/demochart/');

			$usuarioAct = NegSesion::getUsuario();
			
			$this->lista_curso=$this->oNegReportealumno->buscar(array('tipo'=>'productividad_curso','idusuario'=>$usuarioAct['dni']));
			
			$this->notase=[];
			$this->notass=[];

			if(!empty($this->lista_curso))
        	foreach ($this->lista_curso as $lista1){
        		$idcurso=$lista1['idcurso'];
        		$notas=$this->lista_curso2=$this->oNegReportealumno->buscar(array('tipo'=>'productividad_nota','idusuario'=>$usuarioAct['dni'],'idcurso'=>$lista1['idcurso'],'tipo2'=>"E"));
        		$this->notase[$idcurso]= $notas[0]["nota"];

        		$notas=$this->lista_curso2=$this->oNegReportealumno->buscar(array('tipo'=>'productividad_nota','idusuario'=>$usuarioAct['dni'],'idcurso'=>$lista1['idcurso'],'tipo2'=>"S"));
        		$this->notass[$idcurso]= $notas[0]["nota"];

        		//echo $notas[0]["nota"];
        	}

			/**/
			

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte de Progreso del Estudiante'), true);
			$this->esquema = 'reportealumno/repor_progreso';
			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

}