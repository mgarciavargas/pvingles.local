<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		20-04-2017 
 * @copyright	Copyright (C) 20-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
class WebGrupos extends JrWeb
{
	private $oNegGrupos;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegGrupos = new NegGrupos;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegGrupos->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Grupos'), true);
			$this->esquema = 'grupos-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			

			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Grupos').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Grupos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegGrupos->idgrupo = @$_GET['id'];
			$this->datos = $this->oNegGrupos->dataGrupos;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Grupos').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegGrupos->idgrupo = @$_GET['id'];
			$this->datos = $this->oNegGrupos->dataGrupos;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Grupos').' /'.JrTexto::_('see'), true);
			$this->esquema = 'grupos-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'grupos-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveGrupos(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdgrupo'])) {
					$this->oNegGrupos->idgrupo = $frm['pkIdgrupo'];
				}
				
				$this->oNegGrupos->__set('iddocente',@$frm["txtIddocente"]);
					$this->oNegGrupos->__set('idlocal',@$frm["txtIdlocal"]);
					$this->oNegGrupos->__set('idambiente',@$frm["txtIdambiente"]);
					$this->oNegGrupos->__set('idasistente',@$frm["txtIdasistente"]);
					$this->oNegGrupos->__set('fechainicio',@$frm["txtFechainicio"]);
					$this->oNegGrupos->__set('fechafin',@$frm["txtFechafin"]);
					$this->oNegGrupos->__set('dias',@$frm["txtDias"]);
					$this->oNegGrupos->__set('horas',@$frm["txtHoras"]);
					$this->oNegGrupos->__set('horainicio',@$frm["txtHorainicio"]);
					$this->oNegGrupos->__set('horafin',@$frm["txtHorafin"]);
					$this->oNegGrupos->__set('valor',@$frm["txtValor"]);
					$this->oNegGrupos->__set('valor_asi',@$frm["txtValor_asi"]);
					$this->oNegGrupos->__set('regusuario',@$frm["txtRegusuario"]);
					$this->oNegGrupos->__set('regfecha',@$frm["txtRegfecha"]);
					$this->oNegGrupos->__set('matriculados',@$frm["txtMatriculados"]);
					$this->oNegGrupos->__set('nivel',@$frm["txtNivel"]);
					$this->oNegGrupos->__set('codigo',@$frm["txtCodigo"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegGrupos->agregar();
					}else{
									    $res=$this->oNegGrupos->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegGrupos->idgrupo);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDGrupos(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegGrupos->__set('idgrupo', $pk);
				$this->datos = $this->oNegGrupos->dataGrupos;
				$res=$this->oNegGrupos->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegGrupos->__set('idgrupo', $pk);
				$res=$this->oNegGrupos->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	     
}