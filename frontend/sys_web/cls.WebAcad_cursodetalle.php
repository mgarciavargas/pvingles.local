<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017 
 * @copyright	Copyright (C) 03-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
class WebAcad_cursodetalle extends JrWeb
{
	private $oNegAcad_cursodetalle;
	private $oNegAcad_curso;
	private $oNegNivel;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegNivel= new NegNiveles;
			
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Acad_cursodetalle', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["tiporecurso"])&&@$_REQUEST["tiporecurso"]!='')$filtros["tiporecurso"]=$_REQUEST["tiporecurso"];
			if(isset($_REQUEST["idlogro"])&&@$_REQUEST["idlogro"]!='')$filtros["idlogro"]=$_REQUEST["idlogro"];
			if(isset($_REQUEST["url"])&&@$_REQUEST["url"]!='')$filtros["url"]=$_REQUEST["url"];
			
			$this->datos=$this->oNegAcad_cursodetalle->buscar($filtros);
			$this->fkidcurso=$this->oNegAcad_curso->buscar();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Acad_cursodetalle'), true);
			$this->esquema = 'acad_cursodetalle-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function vista(){
		try{
			global $aplicacion;
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			else{throw new Exception(JrTexto::_('Course')." ".JrTexto::_("not exist").'!!'); exit();}				
			$this->idcurso=$_REQUEST["idcurso"];			
			$filtros["idcurso"]=$this->idcurso;
			$this->oNegAcad_curso->idcurso = $this->idcurso;
			$this->infocurso = $this->oNegAcad_curso->dataAcad_curso;
			if(empty($this->infocurso)){throw new Exception(JrTexto::_('Course')." ".JrTexto::_("not exist").'!!'); exit();}


			$this->oNegAcad_cursodetalle->setLimite(0, 500);			
			$datos=$this->oNegAcad_cursodetalle->sesiones($filtros["idcurso"],0);			
			$this->misdatos=$datos;
			$vista=(isset($_REQUEST["vista"])&&@$_REQUEST["vista"]!='')?$_REQUEST["vista"]:'vista01';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Details Course'), true);
			$this->esquema = 'academico/curso-detalle-'.$vista;			
			return parent::getEsquema();
		}catch(Exception $e){			
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	
	public function hijos($idcurso,$idpadre){
		$datos=$this->oNegAcad_cursodetalle->sesiones($idcurso,$idpadre);			
		return $datos;
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Acad_cursodetalle').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			$this->frmaccion='Editar';
			$this->oNegAcad_cursodetalle->idcursodetalle = @$_GET['id'];
			$this->datos = $this->oNegAcad_cursodetalle->dataAcad_cursodetalle;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Acad_cursodetalle').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function nivelesedit(){
		try {
			
			global $aplicacion;
			$tipo=!empty($_REQUEST['tipo'])?$_REQUEST['tipo']:'N';
			$id=!empty($_REQUEST['id'])?$_REQUEST['id']:'0';
			$this->acc=!empty($_REQUEST['acc'])?$_REQUEST['acc']:'nuevo';
			$vista=!empty($_REQUEST['vista'])?$_REQUEST['vista']:'vista01';
            $this->idcurso=!empty($_REQUEST['idcurso'])?$_REQUEST['idcurso']:'';            
			if($this->acc=='editar'){
				//$this->oNegAcad_cursodetalle->idcursodetalle = $id;
				$datos=$this->oNegAcad_cursodetalle->buscar(array('idcursodetalle'=>$id)); 
                if(!empty($datos[0])){ 
                    $this->datos=$datos[0];
				    if(!empty($this->datos)&&$tipo!='N'){
					    $this->datospadre=$this->oNegAcad_cursodetalle->buscar(array('idcursodetalle'=> $this->datos["idpadre"]));
                        if(!empty($this->datospadre[0])) $this->datospadre=$this->datospadre[0];
				    }
                }else{$this->datos=null;}
			}else{
				if($tipo!='N'){
                    $this->datospadre=$this->oNegAcad_cursodetalle->buscar(array('idcursodetalle'=>$id));
                    if(!empty($this->datospadre[0]))
                        $this->datospadre=$this->datospadre[0];
				}				
			}
			if($tipo==='N'){
				$this->esquema = 'academico/nivel-frm-'.$vista;
			}elseif($tipo==='U'){
				$this->esquema = 'academico/unidad-frm-'.$vista;
			}elseif($tipo==='L'){
				$this->esquema = 'academico/actividad-frm-'.$vista;
			}
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	/*public function getpadres()
	{
		#Esta funcion es para probar "oNegAcad_cursodetalle->getTodosPadresXCursoDet()" solamente. 
		#Borrar si es que ya no se necesita.
		$this->documento->plantilla = 'blanco';
		try {
			echo '<p><b>Params GET : </b></p>'; // Ejm: /?idcursodetalle=4&ordenado=DESC
			var_dump($_REQUEST);
			$arrTodoPadres =  $this->oNegAcad_cursodetalle->getTodosPadresXCursoDet($_REQUEST,$_REQUEST["ordenado"]);
			echo '<h1>Resultado Final : </h1>';
			echo "<pre>"; print_r($arrTodoPadres); echo "</pre>";
			exit(0);
		} catch (Exception $e) {
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
			var_dump($data);
			exit(0);
		}
	}*/

	private function form()
	{
		try {
			global $aplicacion;	
			$this->fkidcurso=$this->oNegAcad_curso->buscar();			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_cursodetalle-frm';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function formulario(){
		try {
			global $aplicacion;	
			$this->fkidcurso=$this->oNegAcad_curso->buscar();			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			exit('hola');
			$this->esquema = 'academico/nivel-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			$filtros=array();
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["tiporecurso"])&&@$_REQUEST["tiporecurso"]!='')$filtros["tiporecurso"]=$_REQUEST["tiporecurso"];
			if(isset($_REQUEST["idlogro"])&&@$_REQUEST["idlogro"]!='')$filtros["idlogro"]=$_REQUEST["idlogro"];
			if(isset($_REQUEST["url"])&&@$_REQUEST["url"]!='')$filtros["url"]=$_REQUEST["url"];	
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];	
			$this->datos=$this->oNegAcad_cursodetalle->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
            exit(0);
        }
	}

	public function guardarcursodetalle(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST); 
            $accion='_add';
            if(!empty($idcursodetalle)){
            	if($idcursodetalle!=-1){
					$this->oNegAcad_cursodetalle->idcursodetalle = $idcursodetalle;
					$accion='_edit';
				}
			}
       		global $aplicacion;         
        	$usuarioAct = NegSesion::getUsuario();        	           	
			$this->oNegAcad_cursodetalle->idcurso=$idcurso;
			$this->oNegAcad_cursodetalle->orden=$orden;
			$this->oNegAcad_cursodetalle->idrecurso=$idrecurso;
			$this->oNegAcad_cursodetalle->tiporecurso=$tiporecurso;
			$this->oNegAcad_cursodetalle->idlogro=$idlogro;
			$this->oNegAcad_cursodetalle->url=$url;
			$this->oNegAcad_cursodetalle->idpadre=$idpadre;
             if($accion=='_add') {
            	$res=$this->oNegAcad_cursodetalle->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('saved successfully')),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_cursodetalle->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('update successfully')),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

    public function guardarnivel(){
        $this->documento->plantilla = 'blanco';
        try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            global $aplicacion;         
            $usuarioAct = NegSesion::getUsuario();
            if($idrecurso>0){
                $this->oNegNivel->setIdnivel($idrecurso);
                $nivel=$this->oNegNivel->dataNiveles;
            }
            $this->oNegNivel->nombre=$nombre;    
            $this->oNegNivel->idpersonal=$usuarioAct['dni'];
            $this->oNegNivel->estado=$estado; 
            $this->oNegNivel->descripcion=$descripcion;
            $this->oNegNivel->imagen=$imagen;
            $idrecursopadre=0;
            if(!empty($idpadre)){ //existe padre idcurso detalle
                $datospadre=$this->oNegAcad_cursodetalle->buscar(array('idcursodetalle'=> $idpadre));
                $idrecursopadre=!empty($datospadre[0]['idrecurso'])?$datospadre[0]['idrecurso']:0;
            }
            if(empty($nivel)){                
                $this->oNegNivel->idpadre=$idrecursopadre;
                $ordenrecurso=$this->oNegNivel->maxorden($tipo,$idrecursopadre); //add
                $ordenrecurso++;
                $this->oNegNivel->tipo=$tipo;
                $this->oNegNivel->orden=$ordenrecurso;
                $idrecurso=$this->oNegNivel->agregar();
            }else{
                $idrecurso=$this->oNegNivel->editar();
            }

            if($idcursodetalle>0){
                $this->oNegAcad_cursodetalle->setIdcursodetalle($idcursodetalle);
                $cursodetalle=$this->oNegAcad_cursodetalle->dataAcad_cursodetalle;
            }
            
            $this->oNegAcad_cursodetalle->idcurso=$idcurso;
            $this->oNegAcad_cursodetalle->idrecurso=$idrecurso;
            $this->oNegAcad_cursodetalle->tiporecurso=$tipo;
            $idpadre=!empty($idpadre)?$idpadre:0;
            if(empty($cursodetalle)){               
                $orden=$this->oNegAcad_cursodetalle->getMaxorden($idcurso,$idpadre); 
                $orden++;           
                $this->oNegAcad_cursodetalle->orden=$orden;
                $this->oNegAcad_cursodetalle->idlogro=!empty($idlogro)?$idlogro:0;
                $this->oNegAcad_cursodetalle->url=!empty($url)?$url:''; 
                $this->oNegAcad_cursodetalle->idpadre=$idpadre;         
                $id=$this->oNegAcad_cursodetalle->agregar();
            }else $id=$this->oNegAcad_cursodetalle->editar();

            $datos=array('idrecurso'=>$idrecurso,'idcursodetalle'=>$id,'idpadre'=>$idpadre);
            echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('saved successfully')),'data'=>$datos));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
    }

	public function addcampo(){
		$this->documento->plantilla = 'blanco';
		try {
			@extract($_POST);
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }                       
            if(!empty(@$idcursodetalle)&&!empty($campo)&&isset($valor)) {
				$this->oNegAcad_cursodetalle->idcursodetalle = $idcursodetalle;
				$this->oNegAcad_cursodetalle->setCampo($idcursodetalle,$campo,$valor);
				echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('update successfully')));
                exit(0);
			}         	
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
    }

    public function eliminardet(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            } 
            @extract($_POST);           
            if(!empty(@$idcursodetalle)){
            	if($idcursodetalle==-1) $idcursodetalle=0;
				$this->oNegAcad_cursodetalle->eliminar($idcursodetalle);				
			}
           	echo json_encode(array('code'=>'ok')); 
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
    }		     
}