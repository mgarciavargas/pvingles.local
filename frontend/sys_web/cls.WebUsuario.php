<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-11-2016 
 * @copyright	Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
class WebUsuario extends JrWeb
{
	private $oNegAlumno;
	private $oNegPersonal;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersonal = new NegPersonal;
		$this->oNegAlumno=new NegAlumno;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			$this->usuarioAct = NegSesion::getUsuario();
			if($this->usuarioAct["rol"]!='Alumno')
				$this->datos=$this->oNegPersonal->buscar(array('dni'=>$this->usuarioAct["dni"]));
			else
				$this->datos=$this->oNegAlumno->buscar(array('dni'=>$this->usuarioAct["dni"]));

			$this->documento->script('moment', '/libs/moment/');
			$this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
            $this->documento->script('datetimepicker', '/libs/datepicker/');
           
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Usuario'), true);
			$this->esquema = 'usuario';			
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	public function buscarjson()
	{
		try{
			global $aplicacion;
			$filtros=array();
			$filtros["dni"]=!empty($_REQUEST["dni"])?$_REQUEST["dni"]:'';			
			$this->usuarioAct = NegSesion::getUsuario();
			if($this->usuarioAct["rol"]!='Alumno')
				$this->datos=$this->oNegPersonal->buscar($filtros);
			else
				$this->datos=$this->oNegAlumno->buscar($filtros);
			$this->documento->plantilla = 'returnjson';
			if(count($this->datos)==1) $this->datos=$this->datos[0];
			$data=array('code'=>'ok','data'=>$this->datos);
			echo json_encode($data);
			//return parent::getEsquema();
		}catch(Exception $e) {
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}




	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Alumno', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegAlumno->dni = @$_GET['id'];
			$this->datos = $this->oNegAlumno->dataAlumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegAlumno->dni = @$_GET['id'];
			$this->datos = $this->oNegAlumno->dataAlumno;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('see'), true);
			$this->esquema = 'alumno-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveUsuario(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkDni'])) {
					$this->oNegAlumno->dni = $frm['pkDni'];
				}
				
					$this->oNegAlumno->__set('ape_paterno',@$frm["txtApe_paterno"]);
					$this->oNegAlumno->__set('ape_materno',@$frm["txtApe_materno"]);
					$this->oNegAlumno->__set('nombre',@$frm["txtNombre"]);
					$this->oNegAlumno->__set('fechanac',@$frm["txtFechanac"]);
					$this->oNegAlumno->__set('sexo',@$frm["txtSexo"]);
					$this->oNegAlumno->__set('estado_civil',@$frm["txtEstado_civil"]);
					$this->oNegAlumno->__set('ubigeo',@$frm["txtUbigeo"]);
					$this->oNegAlumno->__set('urbanizacion',@$frm["txtUrbanizacion"]);
					$this->oNegAlumno->__set('direccion',@$frm["txtDireccion"]);
					$this->oNegAlumno->__set('telefono',@$frm["txtTelefono"]);
					$this->oNegAlumno->__set('celular',@$frm["txtCelular"]);
					$this->oNegAlumno->__set('email',@$frm["txtEmail"]);
					$this->oNegAlumno->__set('idugel',@$frm["txtIdugel"]);
					$this->oNegAlumno->__set('regusuario',@$frm["txtRegusuario"]);
					$this->oNegAlumno->__set('regfecha',@$frm["txtRegfecha"]);
					$this->oNegAlumno->__set('usuario',@$frm["txtUsuario"]);
					$this->oNegAlumno->__set('clave',@$frm["txtClave"]);
					$this->oNegAlumno->__set('token',@$frm["txtToken"]);
					$this->oNegAlumno->__set('foto',@$frm["txtFoto"]);
					$this->oNegAlumno->__set('estado',@$frm["txtEstado"]);
					$this->oNegAlumno->__set('situacion',@$frm["txtSituacion"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAlumno->agregar();
					}else{
									    $res=$this->oNegAlumno->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAlumno->dni);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	
	

	public function xSetUsuario(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {             
				if(empty($args[0])) { return;}
				$usuarioAct = NegSesion::getUsuario();
				if(isset($usuarioAct)){
					$rol=$usuarioAct['rol'];
					$data=$args[0];
					$res=0;
					if($rol=='Alumno'){
						$this->oNegAlumno->dni = $usuarioAct['dni'];				
						$this->oNegAlumno->__set($data["campo"],$data["value"]);
						$res=$this->oNegAlumno->setCampo($usuarioAct['dni'],$data["campo"],trim($data["value"]));
					}else{
						$this->oNegPersonal->dni = $usuarioAct['dni'];
						$this->oNegPersonal->__set($data["campo"],$data["value"]);
						$res=$this->oNegPersonal->setCampo($usuarioAct['dni'],$data["campo"],trim($data["value"]));						
					}

					if($res==1) {
						$oRespAjax->setReturnValue(true);
						$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_('Save changed success ')), 'success');
					}
					else $oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
	     
}