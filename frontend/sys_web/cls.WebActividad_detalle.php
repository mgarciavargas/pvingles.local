<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-11-2016 
 * @copyright	Copyright (C) 19-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividad_detalle', RUTA_BASE, 'sys_negocio');
class WebActividad_detalle extends JrWeb
{
	private $oNegActividad_detalle;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegActividad_detalle = new NegActividad_detalle;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Actividad_detalle', 'listar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegActividad_detalle->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Actividad_detalle'), true);
			$this->esquema = 'actividad_detalle-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Actividad_detalle', 'agregar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Actividad_detalle').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Actividad_detalle', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegActividad_detalle->iddetalle = @$_GET['id'];
			$this->datos = $this->oNegActividad_detalle->dataActividad_detalle;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Actividad_detalle').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegActividad_detalle->iddetalle = @$_GET['id'];
			$this->datos = $this->oNegActividad_detalle->dataActividad_detalle;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Actividad_detalle').' /'.JrTexto::_('see'), true);
			$this->esquema = 'actividad_detalle-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'actividad_detalle-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveActividad_detalle(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIddetalle'])) {
					$this->oNegActividad_detalle->iddetalle = $frm['pkIddetalle'];
				}
				
					$this->oNegActividad_detalle->__set('idactividad',@$frm["txtIdactividad"]);
					$this->oNegActividad_detalle->__set('pregunta',@$frm["txtPregunta"]);
					$this->oNegActividad_detalle->__set('tipopregunta',@$frm["txtTipopregunta"]);
					$this->oNegActividad_detalle->__set('orden',@$frm["txtOrden"]);
					$this->oNegActividad_detalle->__set('url',@$frm["txtUrl"]);
					$this->oNegActividad_detalle->__set('estado',@$frm["txtEstado"]);
					$this->oNegActividad_detalle->__set('tipo',@$frm["txtTipo"]);
					$this->oNegActividad_detalle->__set('tipo_desarrollo',@$frm["txtTipo_desarrollo"]);
					$this->oNegActividad_detalle->__set('tipo_actividad',@$frm["txtTipo_actividad"]);
					$this->oNegActividad_detalle->__set('idhabilidad',@$frm["txtIdhabilidad"]);
					$this->oNegActividad_detalle->__set('regusuario',@$frm["txtRegusuario"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegActividad_detalle->agregar();
					}else{
									    $res=$this->oNegActividad_detalle->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegActividad_detalle->iddetalle);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDActividad_detalle(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegActividad_detalle->__set('iddetalle', $pk);
				$this->datos = $this->oNegActividad_detalle->dataActividad_detalle;
				$res=$this->oNegActividad_detalle->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegActividad_detalle->__set('iddetalle', $pk);
				$res=$this->oNegActividad_detalle->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	
	     
}