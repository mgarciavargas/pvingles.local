<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
//JrCargador::clase('sys_negocio::NegLinks', RUTA_BASE, 'sys_negocio');
class WebComunidad extends JrWeb
{
	//private $oNegLinks;
	public function __construct()
	{
		parent::__construct();		
	//	$this->oNegLinks = new NegLinks;
	}

	public function defecto(){
		return $this->versetting();
	}

	public function versetting(){
		global $aplicacion;
		$this->documento->setTitulo(JrTexto::_('Setting'), true);
        $this->esquema = 'doc_cursos';            
        return parent::getEsquema();
	} 
}