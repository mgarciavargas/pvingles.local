<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-12-2017 
 * @copyright	Copyright (C) 28-12-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegUbigeo', RUTA_BASE, 'sys_negocio');
class WebUbigeo extends JrWeb
{
	private $oNegUbigeo;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegUbigeo = new NegUbigeo;
			
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["id_ubigeo"])&&@$_REQUEST["id_ubigeo"]!='')$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
			if(isset($_REQUEST["pais"])&&@$_REQUEST["pais"]!='')$filtros["pais"]=$_REQUEST["pais"];
			if(isset($_REQUEST["departamento"])&&@$_REQUEST["departamento"]!='')$filtros["departamento"]=$_REQUEST["departamento"];
			if(isset($_REQUEST["provincia"])&&@$_REQUEST["provincia"]!='')$filtros["provincia"]=$_REQUEST["provincia"];
			if(isset($_REQUEST["distrito"])&&@$_REQUEST["distrito"]!='')$filtros["distrito"]=$_REQUEST["distrito"];
			if(isset($_REQUEST["ciudad"])&&@$_REQUEST["ciudad"]!='')$filtros["ciudad"]=$_REQUEST["ciudad"];
			
			$this->datos=$this->oNegUbigeo->buscar($filtros);
			$this->fkpais=$this->oNegUbigeo->buscar(array('pais'=>'all'));
			if(!empty($this->fkpais[0])){
				$pais=$this->fkpais[0]["pais"];
				$this->idpais=$pais;
				$this->fkdepartamento=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>'all'));
				if(!empty($this->fkdepartamento[0])){
					$depa=$this->fkdepartamento[0]["departamento"];
					$this->iddepa=$depa;
					$this->fkprovincia=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>$depa,'provincia'=>'all'));
					if(!empty($this->fkprovincia[0])){
						$pro=$this->fkprovincia[0]["provincia"];
						$this->idpro=$pro;
					}
				}
			}
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Ubigeo'), true);
			$this->esquema = 'academico/ubigeo';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			$this->frmaccion='Nuevo';
			$this->fkpais=$this->oNegUbigeo->buscar(array('pais'=>'all'));
			if(!empty($this->fkpais[0])){
				$pais=$this->fkpais[0]["pais"];
				$this->idpais=$pais;
				$this->fkdepartamento=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>'all'));
				if(!empty($this->fkdepartamento[0])){
					$depa=$this->fkdepartamento[0]["departamento"];
					$this->iddepa=$depa;
					$this->fkprovincia=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>$depa,'provincia'=>'all'));
					if(!empty($this->fkprovincia[0])){
						$pro=$this->fkprovincia[0]["provincia"];
						$this->idpro=$pro;
					}
				}
			}
			$this->documento->setTitulo(JrTexto::_('Ubigeo').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			$this->frmaccion='Editar';
			$this->oNegUbigeo->id_ubigeo = @$_GET['id'];
			$this->datos = $this->oNegUbigeo->dataUbigeo;
			$this->pk=@$_GET['id'];
			$this->fkpais=$this->oNegUbigeo->buscar(array('pais'=>'all'));
			if(!empty($this->fkpais[0])){
				$pais=!empty($this->datos["pais"])?$this->datos["pais"]:$this->fkpais[0]["pais"];
				$this->idpais=$pais;
				$this->fkdepartamento=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>'all'));
				if(!empty($this->fkdepartamento[0])){
					$depa=!empty($this->datos["departamento"])?$this->datos["departamento"]:$this->fkdepartamento[0]["departamento"];
					$this->iddepa=$depa;
					$this->fkprovincia=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>$depa,'provincia'=>'all'));
					if(!empty($this->fkprovincia[0])){
						$pro=!empty($this->datos["provincia"])?$this->datos["provincia"]:$this->fkprovincia[0]["provincia"];
						$this->idpro=$pro;
					}
				}
			}

			$this->documento->setTitulo(JrTexto::_('Ubigeo').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'academico/ubigeo-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			$filtros=array();
			if(isset($_REQUEST["id_ubigeo"])&&@$_REQUEST["id_ubigeo"]!='')$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
			if(isset($_REQUEST["pais"])&&@$_REQUEST["pais"]!='')$filtros["pais"]=$_REQUEST["pais"];
			if(isset($_REQUEST["departamento"])&&@$_REQUEST["departamento"]!='')$filtros["departamento"]=$_REQUEST["departamento"];
			if(isset($_REQUEST["provincia"])&&@$_REQUEST["provincia"]!='')$filtros["provincia"]=$_REQUEST["provincia"];
			if(isset($_REQUEST["distrito"])&&@$_REQUEST["distrito"]!='')$filtros["distrito"]=$_REQUEST["distrito"];
			if(isset($_REQUEST["ciudad"])&&@$_REQUEST["ciudad"]!='')$filtros["ciudad"]=$_REQUEST["ciudad"];
						
			$this->datos=$this->oNegUbigeo->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarUbigeo(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkId_ubigeo)) {
				$this->oNegUbigeo->id_ubigeo = $frm['pkId_ubigeo'];
				$accion='_edit';
			}

            	global $aplicacion;         
            	$usuarioAct = NegSesion::getUsuario();
            	@extract($_POST);
            	
				$this->oNegUbigeo->pais=@$txtPais;
					$this->oNegUbigeo->departamento=@$txtDepartamento;
					$this->oNegUbigeo->provincia=@$txtProvincia;
					$this->oNegUbigeo->distrito=@$txtDistrito;
					$this->oNegUbigeo->ciudad=@$txtCiudad;
					
            if($accion=='_add') {
            	$res=$this->oNegUbigeo->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Ubigeo')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegUbigeo->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Ubigeo')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveUbigeo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId_ubigeo'])) {
					$this->oNegUbigeo->id_ubigeo = $frm['pkId_ubigeo'];
				}
				
				$this->oNegUbigeo->pais=@$frm["txtPais"];
					$this->oNegUbigeo->departamento=@$frm["txtDepartamento"];
					$this->oNegUbigeo->provincia=@$frm["txtProvincia"];
					$this->oNegUbigeo->distrito=@$frm["txtDistrito"];
					$this->oNegUbigeo->ciudad=@$frm["txtCiudad"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegUbigeo->agregar();
					}else{
									    $res=$this->oNegUbigeo->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegUbigeo->id_ubigeo);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDUbigeo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegUbigeo->__set('id_ubigeo', $pk);
				$this->datos = $this->oNegUbigeo->dataUbigeo;
				$res=$this->oNegUbigeo->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegUbigeo->__set('id_ubigeo', $pk);
				$res=$this->oNegUbigeo->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegUbigeo->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}