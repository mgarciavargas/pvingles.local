<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-07-2017 
 * @copyright	Copyright (C) 04-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_estudio', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBib_recursos', RUTA_BASE, 'sys_negocio');
class WebBib_recursos extends JrWeb
{
	private $oNegBib_recursos;
	private $oNegBib_estudio;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBib_estudio = new NegBib_estudio;
		$this->oNegBib_recursos = new NegBib_recursos;
	}

	public function defecto(){
		return $this->notas();
	}
    
    
      public function notas()
    {
        try{
            global $aplicacion;
            $this->documento->script('tinymce.min', '/libs/tinymce/');
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
            $this->documento->script('slick.min', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $usuarioAct = NegSesion::getUsuario();
            $nuevo=!empty($_GET["acc"])?$_GET["acc"]:false;
            $this->orden=!empty($_GET["orden"])?$_GET["orden"]:0;

            if($nuevo=="nuevo"){
                $this->orden=$this->oNegBib_recursos->newOrder($usuarioAct["dni"]);
            }            
           	
            $this->dnidoc=!empty($_GET["dni"])?$_GET["dni"]:$usuarioAct["dni"];
            if($this->dnidoc=='00000000') $this->dnidoc=$usuarioAct["dni"];
            
            $this->datosdesc=$this->oNegBib_recursos->buscar(Array('tipo'=>'D','id_personal'=>$this->dnidoc,'orden'=>$this->orden));
          	$this->datostext=$this->oNegBib_recursos->buscar(Array('tipo'=>'T','id_personal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->datospdf=$this->oNegBib_recursos->buscar(Array('tipo'=>'P','id_personal'=>$this->dnidoc,'orden'=>$this->orden));           
                 
            $this->datosVideo=$this->oNegBib_recursos->buscar(Array('tipo'=>'V','id_personal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->datosAudio=$this->oNegBib_recursos->buscar(Array('tipo'=>'A','id_personal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->datosImagen=$this->oNegBib_recursos->buscar(Array('tipo'=>'I','id_personal'=>$this->dnidoc,'orden'=>$this->orden));

            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'bib_mantenimientos';
            $this->esquema = 'tools/notas';  
            
            //Para edicion de actividades
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            //$aplicacion->redir();
        }
    }

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_recursos', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegBib_recursos->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'bib_mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_recursos'), true);
			$this->esquema = 'bib_recursos-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_recursos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Bib_recursos').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Bib_recursos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegBib_recursos->id_recursos = @$_GET['id'];
			$this->datos = $this->oNegBib_recursos->dataBib_recursos;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bib_recursos').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegBib_recursos->id_recursos = @$_GET['id'];
			$this->datos = $this->oNegBib_recursos->dataBib_recursos;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'bib_mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_recursos').' /'.JrTexto::_('see'), true);
			$this->esquema = 'bib_recursos-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bib_recursos-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'bib_mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function nuevoRecurso(){
		$usuarioAct = NegSesion::getUsuario();
		$accion=!empty($_GET["acc"])?$_GET["acc"]:'nuevo';
		$id_estudio=!empty($_GET["id"])?$_GET["id"]:false;
		$tipo=!empty($_GET["tipo"])?$_GET["tipo"]:false;
		$orden=!empty($_GET["orden"])?$_GET["orden"]:0;
        if($accion=="nuevo"){
            $orden=$this->oNegBib_recursos->newOrder($usuarioAct["dni"]);

        } 
		$libro=$this->oNegBib_estudio->buscar(array('id_estudio'=>$id_estudio));
		$nombre=!empty($libro[0])?@$libro[0]['archivo']:"";		
		$recurso=$this->oNegBib_recursos->buscar(Array('id_personal'=>@$usuarioAct["dni"],'tipo'=>$tipo,'orden'=>$orden));
       	if(empty($recurso)){
       		$tt=__xRUTABASEx__.'/static/libreria/pdf/'.$nombre;
            $acc='nuevo';
        }else{                    
            $acc='editar';
            $caratula=!empty($recurso[0])?@$recurso[0]['caratula']:"";
           	$tt=$caratula.','.__xRUTABASEx__.'/static/libreria/pdf/'.$nombre;
            $this->oNegBib_recursos->__set('id_recursos',@$recurso[0]["id_recursos"]);
        }        
        $this->oNegBib_recursos->__set('id_personal',@$usuarioAct["dni"]);
        $this->oNegBib_recursos->__set('estado',1);
        $this->oNegBib_recursos->__set('titulo','');
        $this->oNegBib_recursos->__set('texto','');
        $this->oNegBib_recursos->__set('caratula',$tt);
        $this->oNegBib_recursos->__set('orden',$orden);
        $this->oNegBib_recursos->__set('tipo',$tipo);
        $this->oNegBib_recursos->__set('publicado','');
        $this->oNegBib_recursos->__set('compartido','');
        $this->oNegBib_recursos->__set('descripcion','');
        
        if($acc=='nuevo') {
        	$res=$this->oNegBib_recursos->agregar();
        }else{
            $res=$this->oNegBib_recursos->editar();
        }
        echo json_encode($res);     // return $res;
        exit(0);
	}

	// ========================== Funciones xajax ========================== //
	public function xSaveBib_recursos(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0]; 
                $recurso=$this->oNegBib_recursos->buscar(Array('id_personal'=>@$usuarioAct["dni"],'tipo'=>@$frm["tipo"],'orden'=>@$frm["orden"]));
                if(empty($recurso)){
                      $acc='nuevo';
                }else{
                    $acc='editar';
                    
                    $this->oNegBib_recursos->__set('id_recursos',@$recurso[0]["id_recursos"]);
                } 
                
                $usuarioAct = NegSesion::getUsuario();
                $this->oNegBib_recursos->__set('id_personal',@$usuarioAct["dni"]);
                $this->oNegBib_recursos->__set('estado',1);
                $this->oNegBib_recursos->__set('titulo',@$frm["titulo"]);
                $this->oNegBib_recursos->__set('texto',@$frm["texto"]);
                $this->oNegBib_recursos->__set('caratula',@$frm["caratula"]);
                $this->oNegBib_recursos->__set('orden',@$frm["orden"]);
                $this->oNegBib_recursos->__set('tipo',@$frm["tipo"]);
                $this->oNegBib_recursos->__set('publicado',@$frm["publicado"]);
                $this->oNegBib_recursos->__set('compartido',@$frm["pos"]);
                $this->oNegBib_recursos->__set('descripcion',@$frm["descripcion"]);
                if($acc=='nuevo') {
                	$res=$this->oNegBib_recursos->agregar();
                }else {
                   
                    $res=$this->oNegBib_recursos->editar();
                }
                
                if(!empty($res)) $oRespAjax->setReturnValue($this->oNegBib_recursos->id_recursos);
				else{
                    $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDBib_recursos(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_recursos->__set('id_recursos', $pk);
				$this->datos = $this->oNegBib_recursos->dataBib_recursos;
				$res=$this->oNegBib_recursos->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBib_recursos->__set('id_recursos', $pk);
				$res=$this->oNegBib_recursos->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
    
    public function xRecursos(){
		$this->documento->plantilla = 'returnjson';
        
		try{
			global $aplicacion;	
             $usuarioAct = NegSesion::getUsuario();
			$datos =$this->oNegBib_recursos->buscar(array('id_personal'=>@$usuarioAct["dni"]));
            $res=array();
            $res_tmp1=array();
            $res_tmp2=array();
            $res_tmp3=array();
            
            $sinD=array();
            if (!empty($datos)){
                foreach($datos as $dato){
                    if(!in_array($dato["id_personal"]."_".$dato["orden"],$res_tmp3)){
                         array_push($res_tmp3,$dato["id_personal"]."_".$dato["orden"]); 
                       // array_push($res_tmp1,$dato["idpersonal"]."_".$dato["orden"].);
                    }   
                    if($dato["tipo"]=="D"){
                        array_push($res_tmp2,$dato["id_personal"]."_".$dato["orden"]);
                        array_push($res,$dato);                        
                    } 
                }
                foreach($res_tmp3 as $y=>$id){
                    if(in_array($id,$res_tmp2)){
                        unset($res_tmp3[$y]);
                    }                    
                }
                foreach($res_tmp3 as $id){
                    $ordendni=explode('_',$id);
                    $dni=$ordendni[0];    
                    $orden=$ordendni[1];  $newres=array('caratula'=>'../modbiblioteca/app/images/libro.png','titulo'=>'','id_personal'=>$dni,'orden'=>$orden);  
                    array_push($res,$newres);
                }
            }
			echo json_encode(array('code'=>'ok','data'=>$res));
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}
    public function xEliminarRecursos(){
        try{
            $this->documento->plantilla = 'returnjson';
			global $aplicacion;	
            $usuarioAct = NegSesion::getUsuario();
            $dni=@$_GET['dni'];
            $orden=@$_GET['orden'];
            if($dni!=@$usuarioAct["dni"]){
                echo json_encode(array('code'=>'error','data'=>JrTexto::_('error en dni')));
                exit;
            }
            $res=$this->oNegBib_recursos->eliminar2($dni,$orden);
            echo json_encode(array('code'=>'ok','data'=>$res));
            exit;
        }catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
        
    }
    public function xSetCampo(&$oRespAjax = null, $args = null)
    {
        if(is_a($oRespAjax, 'xajaxResponse')) {
            try {
               
                if(empty($args[0])) { return;}
                $this->oNegBib_recursos->setCampo($args[0],$args[1],$args[2]);
                $oRespAjax->setReturnValue(true);
            } catch(Exception $e) {
                $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
            } 
        }
    }  
    public function agregarCampo(){
    	try{
			global $aplicacion;	
	  
			$id=@$_GET['id_estudio'];
			$compartido=1;

		$this->datos =$this->oNegBib_recursos->buscarCambiar($id,$compartido);
        /*if(is_a($oRespAjax, 'xajaxResponse')) {
            try {
               
                if(empty($args[0])) { return;}
                $this->oNegBib_recursos->setCampo($args[0],$args[1],$args[2]);
                $oRespAjax->setReturnValue(true);
            } catch(Exception $e) {
                $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
            } 
        }*/
        
        echo json_encode(array('code'=>'ok','data'=>$this->datos));
    	}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
    }
     public function xRecursosPublicado(){
		$this->documento->plantilla = 'returnjson';
        
		try{
			global $aplicacion;	
            $usuarioAct = NegSesion::getUsuario();
			$datos =$this->oNegBib_recursos->buscar(array('id_personal'=>@$usuarioAct["dni"],'publicado'=>1));
            $res=array();
            $res_tmp1=array();
            $res_tmp2=array();
            $res_tmp3=array();
            
            $sinD=array();
            if (!empty($datos)){
                foreach($datos as $dato){
                    if(!in_array($dato["id_personal"]."_".$dato["orden"],$res_tmp3)){
                         array_push($res_tmp3,$dato["id_personal"]."_".$dato["orden"]); 
                       // array_push($res_tmp1,$dato["idpersonal"]."_".$dato["orden"].);
                    }   
                    if($dato["tipo"]=="D"){
                        array_push($res_tmp2,$dato["id_personal"]."_".$dato["orden"]);
                        array_push($res,$dato);                        
                    } 
                }
                foreach($res_tmp3 as $y=>$id){
                    if(in_array($id,$res_tmp2)){
                        unset($res_tmp3[$y]);
                    }                    
                }
                foreach($res_tmp3 as $id){
                    $ordendni=explode('_',$id);
                    $dni=$ordendni[0];    
                    $orden=$ordendni[1];  $newres=array('caratula'=>'../modbiblioteca/app/images/libro.png','titulo'=>'','id_personal'=>$dni,'orden'=>$orden);  
                    array_push($res,$newres);
                }
            }
			echo json_encode(array('code'=>'ok','data'=>$res));
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	} 
	public function xRecursosCompartido(){
		$this->documento->plantilla = 'returnjson';
        
		try{
			global $aplicacion;	
            $usuarioAct = NegSesion::getUsuario();
			$datos =$this->oNegBib_recursos->buscar(array('id_personal'=>@$usuarioAct["dni"],'compartido'=>1));
            $res=array();
            $res_tmp1=array();
            $res_tmp2=array();
            $res_tmp3=array();
            
            $sinD=array();
            if (!empty($datos)){
                foreach($datos as $dato){
                    if(!in_array($dato["id_personal"]."_".$dato["orden"],$res_tmp3)){
                         array_push($res_tmp3,$dato["id_personal"]."_".$dato["orden"]); 
                       // array_push($res_tmp1,$dato["idpersonal"]."_".$dato["orden"].);
                    }   
                    if($dato["tipo"]=="D"){
                        array_push($res_tmp2,$dato["id_personal"]."_".$dato["orden"]);
                        array_push($res,$dato);                        
                    } 
                }
                foreach($res_tmp3 as $y=>$id){
                    if(in_array($id,$res_tmp2)){
                        unset($res_tmp3[$y]);
                    }                    
                }
                foreach($res_tmp3 as $id){
                    $ordendni=explode('_',$id);
                    $dni=$ordendni[0];    
                    $orden=$ordendni[1];  $newres=array('caratula'=>'../modbiblioteca/app/images/libro.png','titulo'=>'','id_personal'=>$dni,'orden'=>$orden);  
                    array_push($res,$newres);
                }
            }
			echo json_encode(array('code'=>'ok','data'=>$res));
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	} 
	    
}