<style type="text/css">
.input-group{
	width: 100%;
}
.icon1{font-size: 4em;}
.iconerror{
	/*text-decoration:underline;*/
}
.iconerror i:after{
    font-size: 1.3em;
    position: relative;
    right: 14px;
    /* width: 100%; */
    content: "X";
    color: rgba(245, 16, 16, 0.51);

}
h4.title{
	width: 100%;
    margin: 0;
    padding: 0;
    text-align: center;
}

/*h4.title:after {
    display: inline-block;
    margin: 0 0 8px 20px;
    height: 3px;
    content: " ";
    text-shadow: none;
    background-color: #999;
    width: 140px;
}
h4.title:before {
    display: inline-block;
    margin: 0 20px 8px 0;
    height: 3px;
    content: " ";
    text-shadow: none;
    background-color: #999;
   
 }*/
</style>
<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$aula=@$this->aula[0];
$usuarioAct=$this->usuarioAct;
$portada=@$aula["portada"];
if(!empty($portada)){
	$pos = strpos($portada, 'nofoto');
	if($pos===false)
		$portada='<img class="img-responsive"  src="'.@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$portada).'" width="60%" >';
	else
		$portada='';
}else
$portada='';
$enlace=$this->documento->getUrlBase()."/smartclass/?uri=".$aula["aulaid"]."_".$idgui.$aula["aulaid"].date("Ymd")."&idioma=ES";
$fini=new DateTime($aula["fecha_inicio"]);
$fini->modify('-20 minutes');
$ffin=new DateTime($aula["fecha_final"]);
$fnow=new DateTime("now");
$msj='';
$errorlogin=false;
if($fini>=$fnow&&$fnow<$ffin){
	$msj='Smartclass '.JrTexto::_('not yet active for user connection');
	$errorlogin=true;
}else if($fnow>$ffin){
	$msj='Smartclass '.JrTexto::_('expired');
	$errorlogin=true;
}

?>
<script type="text/javascript" src="<?php echo $this->documento->getUrlBase(); ?>/static/libs/pizarra/getScreenId.js"></script>
<script type="text/javascript" src="<?php echo $this->documento->getUrlBase(); ?>/static/libs/pizarra/DetectRTC.js"></script>
<div class="row" style="padding: 0px 1ex;">
	<div class="panel panel-primary" id="ventanainfosmartclass" >
	    <div class="panel-headding bg-blue text-center" style="overflow: hidden;">
	    	<h4><?php echo ucfirst(JrTexto::_('Smartclass information')) ?></h4>
	    </div>
	    <div class="panel-body" style="overflow: hidden;">
	    	<div class="col-xs-12 col-sm-12 col-md-12 ">
	    		<h4 class="title"><?php echo ucfirst($aula["titulo"]); ?></h4>
		    	<div class="col-xs-12 col-sm-6 col-md-8">
			    	<p><?php echo $aula["descripcion"]; ?></p>
			    	<p><b><?php echo JrTexto::_("Start Date")?>: </b><?php echo $aula["fecha_inicio"]; ?></p>
			    	<p><b><?php echo JrTexto::_("Finish Date")?>: </b><?php echo $aula["fecha_final"]; ?></p>
		    	</div>
		    	<div class="col-xs-12 col-sm-6 col-md-4 text-center">
		    		<p><?php echo $portada; ?></p>
		    	</div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<?php if(!empty($msj)){?>
	    	<div class="alert alert-danger" role="alert">
	    		<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-times-circle" aria-hidden="true"></i></div>
				<div class="col-xs-8 col-sm-8 col-md-9">
				<h4 class="alert-heading"> <?php echo JrTexto::_("Error") ?> !  </h4>
				    <p class="mb-0"><?php echo $msj; ?>   </p>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php } ?>

	    </div>
   	</div>
	<div class="panel panel-primary" id="vrequisitos" >
	    <div class="panel-headding bg-red text-center" style="overflow: hidden;">
	    	<h4><?php echo ucfirst(JrTexto::_('Smartclass requirements')) ?></h4>
	    </div>
	    <div class="panel-body" style="overflow: hidden; position: relative;">
	        <a href="javascript:window.location.reload(true);" class="btnverificar btn btn-sm btn-danger pull-right" style="margin: 2em; position: absolute; z-index:8; right: 1em; "><i class="fa fa-refresh"></i> <?php echo JrTexto::_("Re-check requirements") ?></a>
	    	<div class="col-xs-12 col-sm-12 col-md-12 ">
	    		<div class="alert alert-danger" id="validarnavegador" role="alert">
		    		<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-times-circle" aria-hidden="true"></i></div>
					<div class="col-xs-8 col-sm-8 col-md-9">
					<h4 class="alert-heading"> <?php echo JrTexto::_("Error") ?> ! <span class="iconerror"> (<?php echo JrTexto::_("Browser"); ?>)</span> </h4>
					    <p class="mb-0"><?php echo JrTexto::_("we feel your web browser does not support smartclass, you must use chrome or mozilla"); ?><br>
					    <a href="https://www.google.com/chrome/browser/desktop/index.html"><?php echo JrTexto::_("Install google chrome here") ?></a>  o <br>
					    <a href="https://www.mozilla.org/es-ES/firefox/new/"><?php echo JrTexto::_("Install mozilla firefox here") ?></a><br>
					    </p>
					</div>
					<div class="clearfix"></div>				   
				</div>
		    	<div class="alert alert-danger" id="validarmicrofono" role="alert">
		    		<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-times-circle" aria-hidden="true"></i></div>
					<div class="col-xs-8 col-sm-8 col-md-9">
					<h4 class="alert-heading"> <?php echo JrTexto::_("Error") ?> ! <span class="iconerror"><i class="fa fa-microphone"></i> (<?php echo JrTexto::_("Microphone"); ?>)</span> </h4>
					    <p class="mb-0"><?php echo JrTexto::_("No connected microphone has been detected, this being necessary for communication"); ?></p>
					</div>
					<div class="clearfix"></div>				   
				</div>
				<div class="alert alert-danger" id="validaraudio" role="alert">
		    		<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-times-circle" aria-hidden="true"></i>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-10">
					<h4 class="alert-heading"> <?php echo JrTexto::_("Error") ?> ! <span class="iconerror"><i class="fa fa-volume-up"></i> (<?php echo JrTexto::_("Audio output"); ?>)</span> </h4>
					    <p class="mb-0"><?php echo JrTexto::_("No connected speakers or headphones detected, this being necessary for communication"); ?></p>
					</div>
					<div class="clearfix"></div>			    
				</div>
				<div class="alert alert-warning" id="validarcamara" role="alert">
					<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-exclamation-triangle" aria-hidden="true"></i></div>
					<div class="col-xs-8 col-sm-8 col-md-9">
					<h4 class="alert-heading"> <?php echo JrTexto::_("Warning") ?> ! <span class="iconerror"><i class="fa fa-camera"></i> (<?php echo JrTexto::_("Webcam"); ?>)</span> </h4>
					    <p class="mb-0"><?php echo JrTexto::_("No connected speakers or headphones detected, this being necessary for communication"); ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="alert alert-warning" id="validarcompartirpantalla"  role="alert">
					<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-exclamation-triangle" aria-hidden="true"></i></div>
					<div class="col-xs-8 col-sm-8 col-md-9">
					<h4 class="alert-heading"> <?php echo JrTexto::_("Warning") ?> ! <span class="iconerror"><i class="fa fa-desktop"></i> (<?php echo JrTexto::_("Share desktop"); ?>)</span></h4>
					    <p class="mb-0"><?php echo JrTexto::_("no plugin needed to share your desktop, click the following link to install it"); ?> <br>
					   <a id="ischrome" href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk" target="_blank"><?php echo JrTexto::_('Google chrome extension'); ?></a> <a id="isFirefox" href="https://www.webrtc-experiment.com/store/firefox-extension/enable-screen-capturing.xpi" target="_blank"><?php echo JrTexto::_('Mozilla firefox extension'); ?></a>
					    </p>
					</div>
					<div class="clearfix"></div>
				</div>	    	
	    	</div>
	    </div>
   	</div>
   	<div class="panel panel-primary" id="vinfoconexion" style="display: none;" >
	    <div class="panel-headding bg-green text-center" style="overflow: hidden;">
	    	<h4><?php echo ucfirst(JrTexto::_('User information')) ?></h4>
	    </div>
	    <div class="panel-body" style="overflow: hidden;">
	    	<div class="col-xs-12 col-sm-8 col-md-8">
	    		<div class="form-group">		
			        <div class='input-group' >
			        	<span class="input-group-addon btn" style="width:70px"><span class="fa fa-user"></span></span>
			            <input type='text' class="form-control " placeholder="<?php echo JrTexto::_('Enter your username'); ?>" name="txtnombre" id="txtnombre" value="<?php echo ucfirst($usuarioAct["usuario"]); ?>" />           
			        </div>
			        <div class='input-group' >
			        	<span class="input-group-addon btn" style="width:70px"><span class="fa fa-envelope-o"></span></span>
			            <input type='mail' class="form-control " placeholder="<?php echo JrTexto::_('Enter your Email');?>" name="txtemail" id="txtemail" value="<?php echo $usuarioAct["email"]; ?>" />           
			        </div>
			        <div class="clearfix"></div>			        
			    </div>
	    	</div>
	    	<div class="col-xs-12 col-sm-3 col-md-3" style=" text-align: center;">
	    		<button class="btnunirsala btn btn-sm btn-primary disabled" style="margin: 2.5em;"><span><?php echo JrTexto::_("Smartclass open"); ?></span> <i class="fa fa-sign-in"></i> </button>
	    	</div>
	    	<div class="clearfix"></div>
	    	<div class="alert alert-warning" id="infosala"  role="alert" style="display: none;">
				<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-exclamation-triangle" aria-hidden="true"></i></div>
				<div class="col-xs-8 col-sm-8 col-md-9">
				<h4 class="alert-heading"> <?php echo JrTexto::_("Warning") ?> ! </h4>
				    <p class="mb-0">Smartclass <?php echo JrTexto::_("Moderator is not connected to the room"); ?> <br>				   
				    </p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="alert alert-danger" id="infosalaerror"  role="alert" style="display: none;">
				<div class="col-xs-2 col-sm-2 col-md-1"><i class="icon1 fa fa-close" aria-hidden="true"></i></div>
				<div class="col-xs-8 col-sm-8 col-md-9">
				<h4 class="alert-heading"> <?php echo JrTexto::_("Error") ?> ! </h4>
				    <p class="mb-0">Smartclass <?php echo JrTexto::_("Moderator is not connected to the room"); ?> <br>				   
				    </p>
				</div>
				<div class="clearfix"></div>
			</div>  	
	    </div>
   	</div>
</div>
<form id="frmaula" method="post" action="<?php echo $this->documento->getUrlBase(); ?>/aulavirtual/ver/" style="display: none;">
	<input type="hidden" name="id" id="frmidaula">
	<input type="hidden" name="user" id="frmuser">
	<input type="hidden" name="email" id="frmemail">
	<input type="hidden" name="tipo" id="frmtipo">
</form>
<script type="text/javascript">
$(document).ready(function(){
	function validarcontroles(){	  
       DetectRTC.load(function(){
       	var _navegadorok=_haymicro=_haywebcam=_hayaudio=_hayScreen=false;
       	if (DetectRTC.isWebRTCSupported === true){
       		$('#validarnavegador').hide();
		    _navegadorok=true;
		}
        if (DetectRTC.hasMicrophone === true) { // enable microphone 
            $('#validarmicrofono').hide();
            _haymicro=true;
        }
        if (DetectRTC.hasWebcam === true) {// enable camera   
           $('#validarcamara').hide();          
            _haywebcam=true;
        }

        if (DetectRTC.hasSpeakers === true ||DetectRTC.browser.isFirefox) { // checking for "false"
             $('#validaraudio').hide();
         	_hayaudio=true;
        }


        if(DetectRTC.isScreenCapturingSupported){
            hayScreen=true;           
        }
        var comprobarrecursos=function(_hayScreen){
        	if(_navegadorok&&_haymicro&&_haywebcam&&_hayaudio&&_hayScreen){
        		$('#vrequisitos').hide();
        	}
        	if(_navegadorok&&_haymicro&&_hayaudio){
        		var errorlogin='<?php echo $errorlogin;?>';
        		if(!errorlogin)$('#vinfoconexion').show('fast');
        	}
        }

        getChromeExtensionStatus(function(status){
            var _hayScreen=false;
            if(status==='installed-enabled'){
                _hayScreen=true;
                $('#validarcompartirpantalla').hide();
            }else{ 
                if(DetectRTC.browser.isFirefox){
                    $('#validarcompartirpantalla #ischrome').hide();
                }else{
                    $('#validarcompartirpantalla #isFirefox').hide();
                } 
            } 
            comprobarrecursos(_hayScreen);         
        });     
    });    
}
validarcontroles();
var verificarusuario=function(){	
	var email=$('#txtemail').val();
	var users=$('#txtnombre').val();
	if(!$('.btnunirsala').hasClass('disabled')) $('.btnunirsala').addClass('disabled');
	if(email==''||users=='') return;
	if(!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email))	return false;
	sessionStorage.setItem("emailsmartclass", email);
	sessionStorage.setItem("userssmartclass", users);
	var txtemails=[{email:email,usuario:users}];
	var formData = new FormData();
        formData.append("usuario", JSON.stringify(txtemails));
        formData.append("idaula", '<?php echo @$aula["aulaid"]; ?>');
	$.ajax({
        url: _sysUrlBase_+'/aulavirtual/json_verificarusuario/',
        type: "POST",
        data:  formData,
        contentType: false,
        dataType :'json',
        cache: false,
        processData:false,            
        success: function(data)
        {                
            console.log(data);
            if(data.code==='ok'){
	            var info=data.msj;
	            $('#infosalaerror').hide('fast');
	            if(info.como==="M"){
	            	$('#infosala').hide('fast');
	            	$('.btnunirsala').removeClass('disabled').find('span').text('<?php echo JrTexto::_("Smartclass open"); ?>');	            	
	            }else if(info.estado!=="AB"){
	            	$('#infosala').show('fast');	            	
	            	if(info.estado==='0'){
	            		$('#infosala').removeClass('alert-danger').addClass('alert-warning');
	            		$('#infosala .alert-heading').text(' <?php echo JrTexto::_("Warning") ?> ! ');
	            		$('#infosala .icon1').removeClass('fa-close').addClass('fa-exclamation-triangle');
	            		$('#infosala .mb-0').text('Smartclass <?php echo JrTexto::_("Moderator is not connected to the room"); ?> ');
	            	}else if(info.estado==='CL'){
	            		$('#infosala').removeClass('alert-warning').addClass('alert-danger');
	            		$('#infosala .mb-0').text('Smartclass <?php echo JrTexto::_("close room"); ?> ');
	            		$('#infosala .alert-heading').text(' <?php echo JrTexto::_("Error") ?> ! ');
	            		$('#infosala .icon1').addClass('fa-close').removeClass('fa-exclamation-triangle');
	            	}else if(info.estado==='BL'){
	            		$('#infosala').removeClass('alert-warning').addClass('alert-danger');
	            		$('#infosala .mb-0').text('Smartclass <?php echo JrTexto::_("lock room"); ?> ');
	            		$('#infosala .alert-heading').text(' <?php echo JrTexto::_("Error") ?> ! ');
	            		$('#infosala .icon1').addClass('fa-close').removeClass('fa-exclamation-triangle');
	            	}
	            	$('.btnunirsala').find('span').text('<?php echo JrTexto::_("Smartclass Join"); ?>');
	            }else{
	            	$('#infosala').hide('fast');
	            	$('.btnunirsala').removeClass('disabled').find('span').text('<?php echo JrTexto::_("Smartclass Join"); ?>');
	            }
	            $('.btnunirsala').attr('data-tipoconex',info.como);
	            sessionStorage.setItem("tiposmartclass", info.como);
	            $('#frmidaula').val('<?php echo @$aula["aulaid"]; ?>')
	            $('#frmemail').val(email);
	            $('#frmuser').val(users);
	            $('#frmtipo').val(info.como);
            }else{
				$('#infosalaerror .mb-0').text(data.msj);            	
            	$('#infosalaerror').show('fast');
            	if(!$('.btnunirsala').hasClass('disabled')) $('.btnunirsala').addClass('disabled');
            }           
            return false;
        },error: function(xhr,status,error){
          	console.log(xhr.responseText,status,error);
            return false;
        }
    }).always(function() {
        //$('.btnsendemail').removeAttr('disabled');
    });
}
$('.btnverificar').click(function(){
	validarcontroles();
});
$('#txtnombre').blur(function(){ verificarusuario(); });
$('#txtemail').keyup(function(){ verificarusuario(); });
$('.btnunirsala').click(function(){
	var tipouser=$(this).attr('data-tipoconex')||'';
	if(tipouser=='')return;
	$('#frmaula').submit();
});
verificarusuario();
setInterval(function(){verificarusuario();},10000);
});

</script>