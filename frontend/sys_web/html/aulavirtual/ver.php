<style type="text/css">
	
	.ventanas{
		position: absolute;
		overflow: hidden;
	}

	.ventanas .panel-heading .btn-group {
			margin: 0px;
			padding: 0px;
			margin-right: -12px;
    		margin-top: -2px;
    		margin-bottom: -6px;
	}

	ul#menupersonalizado li ul .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover{
		background: rgba(90, 137, 248, 0.71);
	}

	.ventanas .panel-heading{ cursor: move; }

	ul#usersparticipante{
		padding: 1ex 1.5ex;
    	list-style: none;
	}
	ul#usersparticipante li{
		padding: 0.25ex;
    	position: relative;
	}
	.userchataccion{
		position: absolute;
    	right: 0.25ex;
	}
	hr.midivider{
		margin: 0px;
	    border: 0;
	    border-top: 1px solid #29bd5d;
	}
	.miestatus i.fa{
		width: 25px;
	}
	.miestatus i.fa-user, .userstatus i.fa-user{
		color: #a4a9a5; 
	}
	.miestatus i.fa-users, .userstatus i.fa-users{
	color: #337ab7;
	}

	.miestatus i.fa-cubes ,.userstatus i.fa-cubes{
		color: red ;
	}

	.userstatus.active::after{
	    content: "\f00c";
	    font-family: FontAwesome;
	    padding-right: 1em;
	    position: absolute;
	    right: 0;
    }

    

	#pantalla1 .panel-body{
	   	padding: 0.25px;
	   	min-height: 100px;
	   	overflow: auto;
	}

	.note-desktop {
	    display: none;
	}

	 .mce-tinymce, .mce-edit-area.mce-container, iframe, .mce-container-body.mce-stack-layout
	{
	    height: 100% !important;
	}
	.mce-container-body.mce-stack-layout{
		overflow: hidden;
	}


	.botones-compartir{
		position: absolute;
		top: 35%;
	}
	.btncompartir{
		width: 100px;
		height: 100px;
		margin-bottom: 5px;
	}

	.grupo-opciones{display: inline-block;}
	.grupo-opciones:not(:first-child){
		border-left: 1px solid #d9d9d9;
	    margin-left: 3px;
	    padding-left: 6px;
	}
	.grupo-opciones .btn{min-width: 24.7px;}

	#vparticipantes{
		top:0; width: 20%; height: 49%; display:block;
	}
	#vchat{
		top:50%; width: 20%; height: 49%; display:block;
	}
	#vinformation{
		top:0%; left: 20.5%; width: 79%; height: 99%; display:block;
	}

	#vnotas, #vpizarra, #vinvite, #vfiles, #vemiting , #vencuesta, #vvideos, #vaudios{
		border: 1px solid #ccc;
		top:25%; left: 25%; width: 50%; height: 50%; display:none;
	}

	#vwebcam{
		bottom:5%; right: 5%; width: 250px; height: 200px; display:none;
	}

	#vlink{
		bottom:30%; right: 0%; width: 50px; height: 250px;
	}

	.ventanas .panel-body{	
		position: absolute; top: 40px; width: 100%; height: 90%; color: #292925;  padding: 1ex;
		overflow: hidden;
	}

	.ventanas.active{
		z-index: 91;
	}


	/*************** chats *******************/
	.chatAll::-webkit-scrollbar {
	     width: 1.5ex;
	}
	 
	.chatAll::-webkit-scrollbar-track {
		background-color:  #545453;
	    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	}
	 
	.chatAll::-webkit-scrollbar-thumb {
	 background-color: #545453;
	 outline: 1px solid hsl(347, 4%, 31%);
	}

	.chatAll .mensajes .globoright, .chatAll .mensajes .globoleft , .chatAll .mensajes .isprivate .globoright, .chatAll .mensajes .isprivate .globoleft{	
	    padding: 0.5ex;
	    position: relative;
	    border-radius: 0.5ex;
	    color: #d0cbb9;
	    z-index: 9;
	    max-width: 90%;
	    min-width: 75%;
	    margin-right: 15px;
	   
	    background: #7b0419;
	    padding: 1ex;
	}
	.chatAll .mensajes .globoleft{ 
		margin-right: 0px;
		margin-left: 15px; 
	    color: #353232;
	    background: rgb(172, 176, 230); }
	.chatAll .mensajes .isprivate .globoleft{  background: rgb(230, 95, 73); }

	.chatAll .mensajes .globoright:after , .chatAll .mensajes .globoleft:before, .chatAll .mensajes .isprivate .globoright:after , .chatAll .mensajes .isprivate .globoleft:before{
		position: absolute; 
		bottom: 40%; 
		border: solid;
		z-index: 8;
		content: "";
	}

	.chatAll .mensajes .globoright:after{	   
	    right: -10px;   
	    border-color: rgba(123,4,25, 0.8) transparent;
	    border-width: 0px 11px 11px 4px;    
	}
	.chatAll .mensajes .isprivate .globoright:after{	   
	    right: -10px;   
	    border-color: rgb(230, 95, 73) transparent;
	    border-width: 0px 13px 27px 0px;   
	}

	.chatAll .mensajes .globoleft:before{
	    left: -15px;
	    border-color: rgb(172, 176, 230) transparent;
	    border-width: 25px 0px 0px 15px;
	}

	.chatAll .mensajes .isprivate .globoleft:before{
	    left: -15px;
	    border-color: rgb(230, 95, 73) transparent;
	    border-width: 25px 0px 0px 15px;
	}

	.globoright .chattime , .globoleft .chattime{
		font-size:11px;
		text-align: right;
		width: 100%;
	}

	.globoright .chattime{ color: #ccc;}
	.globoleft .chattime{color: #902929;}

	.chatAll .mensajes .mensaje{
		padding: 0.5ex;
	}

	.colored{
		color:#ff2525
	}
	.colorgreen{
		color:#6ce06c;
	}

	video{
		width: 100%;
	}

	canvas {
	  width: 100%;
	  min-height: 150px;
	  height: auto;
	}
	.ct-label{
		font-weight: bold;
		font-size: 0.9em;
	}

	#showiconos i.em{
		cursor: pointer;
	}
	.sysmenu , .sysmenu ul {
		list-style:none;
	}
							
	.sysmenu li {
		background-color:#fff;
		border:1px solid #ccc;
		cursor: pointer;
		text-decoration:none;
		font-size: 11px;
		padding: 0.5ex !important;
		min-width: 33px;
    	min-height: 22px;	
	}

	.sysmenu li i.fa{ font-size: 12px; }
	
	.sysmenu li:hover {
		background-color:#4f7ff1;
		
	}
	
	.sysmenu li ul {
		right: 0px;
		min-width: 140px;
		display:none;
		position:absolute;	
		z-index: 10;						
	}
	
	.sysmenu li:hover > ul {
		display:block;
	}
	
	.sysmenu li ul li {
		position:relative;
		padding: 0.5ex !important;
	}
	
	.sysmenu li ul li ul {
		right:0px;
		top:0px;
	}
	.iconactive{ color:green; }
	.iconinactive{ color:red; }	
</style>
<?php $curaula=$this->aula; ?>
<div id="pantalla1">
	 	<div class="panel ventanas" id="vparticipantes" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Participants')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">				 
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body" style="overflow: auto; ">
				<div id="userclone" style="display: none;">
					<li id="idtmp" class="dropdown liuser" >
		                <a href="#" class="dropdown-toggle miestatus" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cubes"></i><span class="caret"></span></a>
		                <ul class="liestatus dropdown-menu ">
		                  <li><a class="userstatus modeU" data-mode="U" href="#"><i class="fa fa-user"></i> <?php echo ucfirst(JrTexto::_('Participant')); ?></a></li>
		                  <li><a class="userstatus modeP" data-mode="P" href="#"><i class="fa fa-users"></i> <?php echo ucfirst(JrTexto::_('Presenter')); ?></a></li>
		                  <li><a class="userstatus modeM" data-mode="M" href="#"><i class="fa fa-cubes"></i> <?php echo ucfirst(JrTexto::_('Moderator')); ?></a></li>
		                </ul>
		     			<span class="nombre"></span>
		     			<span class="userchataccion">
			     		<div class="btn-group">
			     			<button type="button" class="btn btn-default btn-xs btnmicroprivado" title="<?php echo JrTexto::_('Enable and disable call private'); ?>"><i class="fa fa-phone animated infinite"></i></button>
							<button type="button" class="btn btn-default btn-xs btnmano" title="<?php echo JrTexto::_('Requesting a microphone'); ?>" ><i class="fa fa-hand-stop-o animated infinite"></i></button>							
							<ul class="sysmenu btn-micro oneuser">
								<li>
									<i class="vericon fa fa-microphone-slash animated infinite"></i> <span class="caret"></span>
									<ul>
										<li class="btnmicroactive"><i class="fa fa-microphone iconactive"></i> <?php echo ucfirst(JrTexto::_('On')); ?></li>
										<li class="btnmicroinactive"><i class="fa fa-microphone-slash iconinactive"></i> <?php echo ucfirst(JrTexto::_('Off')); ?></li>
										<li class="btnmicropermitir"><i class="fa fa-microphone"></i> <?php echo ucfirst(JrTexto::_('Allow to use')); ?></li>
										<li class="btnmicronopermitir"><i class="fa fa-microphone-slash"></i> <?php echo ucfirst(JrTexto::_('do not allow to use')); ?></li>
									</ul>
								</li>
							</ul>
							<!--button type="button" class="btn btn-default btn-xs btnmicro" title="Activar/desactivar microfono"><i class="fa fa-microphone-slash animated infinite"></i></button-->
					 	</div>
					 	</span>
					 </li>
					<li class="lilinea"> <hr class="midivider"></li>
				</div>
				<ul id="usersparticipante" >	
					<li class="linea"> <hr class="midivider"></li>
					<li id="totaluser" ><?php echo ucfirst(JrTexto::_('Total users')); ?> (<b>0</b>)<span class="userchataccion">
					<ul class="sysmenu btn-micro alluser" >
						<li>
							<i class="vericon fa fa-microphone-slash animated infinite"></i> <span class="caret"></span>
							<ul>
								<li class="btnmicroactive"><i class="fa fa-microphone iconactive"></i> <?php echo ucfirst(JrTexto::_('On')); ?></li>
								<li class="btnmicroinactive"><i class="fa fa-microphone-slash iconinactive"></i> <?php echo ucfirst(JrTexto::_('Off')); ?></li>
								<li class="btnmicropermitir"><i class="fa fa-microphone"></i> <?php echo ucfirst(JrTexto::_('Allow to use')); ?></li>
								<li class="btnmicronopermitir"><i class="fa fa-microphone-slash"></i> <?php echo ucfirst(JrTexto::_('do not allow to use')); ?></li>
							</ul>
						</li>
					</ul>					
					</li>
				</ul>
			</div>
		</div>

		<div class="panel ventanas" id="vchat"  >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Chat')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">				 
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body">
			    <div class="chatAll" style="position: absolute;width: 100%;height: 100%; padding: 1ex;">                                                
                    <div class="mensajes text-center" style="overflow: auto;" >
                      <div class="text-center"><?php echo ucfirst(JrTexto::_('Welcome to chat')); ?><br><span class="userid"></span></div>
                    </div>
                    <div class="newmensaje text-center" style="position: absolute;bottom: 0px;height: 100px; width: 98%;  overflow: auto;">
                      <textarea id="txtnewmensaje" placeholder="<?php echo ucfirst(JrTexto::_('Write your message')); ?>" style="width: 98%; height:60px;"></textarea>
		            </div> 		
				</div>
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px 1px;">
				<?php require("iconos.php"); ?>
				<div class="grupo-opciones pull-right">
					<div class="pull-right" style="padding-right: 1em;"><?php echo ucfirst(JrTexto::_('To')); ?> :
	                      <select id="userchatsms" >
	                      	<option value="alluser"><span class="fa fa-users"></span> <?php echo ucfirst(JrTexto::_('All users')); ?></a></option>
	                      </select>
                      </div>
				</div>
			</div>
		</div>
		
		<div class="panel ventanas" id="vinformation">
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Information')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">				 
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
				<div class="clearfix"></div>
			</div>

			<div class="panel-body">
				<div class="infosmartclass">
					<div class="row">
						<h1 class="col-xs-12 text-center"> <?php echo ucfirst(JrTexto::_('Welcome to')); ?> Smartclass</h1>
						<h3 class="text-center"><?php echo $curaula["titulo"]; ?></h3>
						<p class="text-center"><?php echo $curaula["descripcion"]; ?></p>

						<div class="col-xs-12 botones-compartir">
							<h4 class="text-center"><?php echo ucfirst(JrTexto::_('What do you want to share')); ?>?</h4>
							<div class="text-center">
								<button class="btn btn-default btncompartir btnsharepdfinboard istooltip" title="<?php echo ucfirst(JrTexto::_('share pdf')); ?>"><i class="color-danger fa fa-file-pdf-o fa-3x animated infinite"></i></button>
								<button class="btn btn-default btncompartir btnsharedesktop istooltip" title="<?php echo ucfirst(JrTexto::_('share desktop')); ?>"><i class="color-info fa fa-desktop fa-3x animated infinite"></i></button>
								<button class="btn btn-default btncompartir btnsharecamera istooltip" title="<?php echo ucfirst(JrTexto::_('Share webcam')); ?>"><i class="color-primary fa fa-camera fa-3x animated infinite"></i></button>
							</div>
							<div class="text-center">
								<button class="btn btn-default btncompartir btnsharevideo istooltip" title="<?php echo ucfirst(JrTexto::_('Share Video')); ?>"><i class="color-danger fa fa-video-camera fa-3x animated infinite"></i></button>
								<button class="btn btn-default btncompartir btnsharefile istooltip" title="<?php echo ucfirst(JrTexto::_('Upload and share files')); ?>"><i class="color-info fa fa-files-o fa-3x animated infinite"></i></button>
								
							</div>					
						</div>
					</div>
				</div>
				<div id="medias-container" style="display: none;">
			   		
			   	</div>

			</div>

			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px;">
				<?php require("iconos.php"); ?>
				<div class="grupo-opciones">
					<button class="btn btn-xs btn-default btnherramienta btnshareventana istooltip" title="<?php echo ucfirst(JrTexto::_('Share')); ?>"><i class="fa fa-share"></i><span class="sr-only"><?php echo ucfirst(JrTexto::_('Share')); ?></span></button>
					<button class="btn btn-xs btn-default btnherramienta btnfullscreen istooltip" title="<?php echo ucfirst(JrTexto::_('Full Screen')); ?>"><i class="fa fa-window-maximize"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_('Full Screen')); ?></span></button>
				</div>
			</div>
		</div>
		
		<div class="panel ventanas" id="vnotas" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Notes')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">				 
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body">			
			   	<textarea id="vtxtnotes"></textarea>			   	
			</div>
		</div>

		<div class="panel ventanas" id="vpizarra" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Board')); ?> :
				<span style="position: relative;">
				    <span class="emitpizarra" style="display: none;">Yo</span>
					<select id="userspizarra" style="display: none; background: transparent; border:0px;"></select>
				</span>
				</h3>	
					<span class="pull-right btn-group btn-group-sm">
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize"></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body" style="border: 1.2ex solid #af7010; min-height: 80%;">			
			   	<canvas id="micanvas"></canvas>
			   	<canvas id="micanvaspdf" style="display: none"></canvas>
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px; border-top: 1.2ex solid #af7010;">
				<div class="grupo-opciones">
					<div class="btn-group btn-group-sm" role="group">					  	
					  	<a href="#" class="btn btn-default istooltip btnherramienta btnshareventana" title="<?php echo ucfirst(JrTexto::_('Share')); ?>" ><i class="fa fa-share"></i></a> 
						<a href="#" class="btn btn-default istooltip btnfullscreen istooltip" title="<?php echo ucfirst(JrTexto::_('Full Screen'));?>"><i class="fa fa-desktop"></i></a>	
					</div>
				</div>				
				<div class="grupo-opciones herramienta-dibujo">
					<div class="btn-group btn-group-sm" role="group">
						<a href="#" class="btn btn-default istooltip toolcolorfondocanvas" data-color="" style="overflow: hidden; position: relative; color:#fff" title="<?php echo ucfirst(JrTexto::_('color fondo')); ?>" >
				  			<i id="idsyscolorback" class="fa fa-square jscolor {valueElement:null,value:'#ffffff',closable:true,closeText:'Cerrar',onFineChange:'updatecolor(this)'}"  style="background-image: none; background-color: rgb(255, 255, 255); color: rgb(255, 255, 255); font-size: 1.5em;"></i>
				  			
				  		</a>
						<a href="#" class="btn btn-default istooltip toolcolorline" data-color="" style="overflow: hidden; position: relative;" title="<?php echo ucfirst(JrTexto::_('color Line / border color')); ?>" >
				  			<i id="idsyscolorline" class="fa fa-circle jscolor {valueElement:null,value:'#000000',closable:true,closeText:'Cerrar',onFineChange:'updatecolor(this)'}" style=" background-image: none; color: rgb(0, 0, 0);  background-color: rgb(0, 0, 0); font-size: 1.5em;"></i>				  			
				  		</a> 
						<a href="#" class="btn btn-default istooltip toolcolorfill" data-color="" title="<?php echo ucfirst(JrTexto::_('Fill color'));?>"><i id="idsyscolorfill" class="fa fa-circle-o jscolor {valueElement:null,value:'#000000',closable:true,closeText:'Cerrar',onFineChange:'updatecolor(this)'}" style="background-image: none; color: rgb(0, 0, 0); font-size: 1.5em;  background-color: rgb(0, 0, 0); "></i>
						</a>				  		
				  	</div>
				  	<div class="btn-group btn-group-sm" role="group">
				  		<a href="#" class="btn btn-default istooltip toolpuntero" title="<?php echo ucfirst(JrTexto::_('Puntero')); ?>" ><i class="fa fa-arrow-up"></i></a>
				  		<a href="#" class="btn btn-default istooltip toollapiz" title="<?php echo ucfirst(JrTexto::_('Pencil')); ?>" ><i class="fa fa-pencil"></i></a> 
						<a href="#" class="btn btn-default istooltip toolresaltador" title="<?php echo ucfirst(JrTexto::_('Marker pen'));?>"><i class="fa fa-paint-brush"></i></a>
						<a href="#" class="btn btn-default istooltip toollinea" title="<?php echo ucfirst(JrTexto::_('Line')); ?>" ><i class="fa fa-minus"></i></a>
						<a href="#" class="btn btn-default istooltip toolsquare" title="<?php echo ucfirst(JrTexto::_('Square')); ?>" ><i class="fa fa-square-o"></i></a>
						<a href="#" class="btn btn-default istooltip toolcircle" title="<?php echo ucfirst(JrTexto::_('Circle')); ?>" ><i class="fa fa-circle-o"></i></a>
						<a href="#" class="btn btn-default istooltip toolborrador" title="<?php echo ucfirst(JrTexto::_('Eraser')); ?>" ><i class="fa fa-eraser"></i></a>
						<a href="#" class="btn btn-default istooltip toolborrador2" title="<?php echo ucfirst(JrTexto::_('Eraser All')); ?>" ><i class="fa fa-eraser"></i><i class="fa fa-eraser"></i></a>
				  	</div> 
				  	<div class="btn-group btn-group-sm" role="group">
					  <a class="btn btn-default istooltip tooltext" title="<?php echo ucfirst(JrTexto::_('Text')); ?>" ><i class="fa fa-text-width"></i><i class=""></i></a>
					  <div class="btn-group btn-group-sm dropup tooltextfont">
					    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					    <i class="fa fa-font"></i> <span class="caret"></span></button>
					    <ul class="dropdown-menu" role="menu" style="height: 150px; overflow: auto;">
					      <li><a href="#">Arial</a></li>
					      <li><a href="#">Arial Black</a></li>
					      <li><a href="#">Bodoni</a></li>
					      <li><a href="#">Book Antiqua</a></li>
					      <li><a href="#">Courier New</a></li>
					      <li><a href="#">Comic</a></li>
					      <li><a href="#">Garamond</a></li>
					      <li><a href="#">Georgia</a></li>
					      <li><a href="#">Helvetica</a></li>
					      <li><a href="#">Impact</a></li>
					      <li><a href="#">Lucida Console</a></li>
					      <li><a href="#">Lucida Sans Unicode</a></li>
					      <li><a href="#">Palatino Linotype</a></li>
					      <li><a href="#">Tahoma</a></li>
					      <li><a href="#">Times new Roman</a></li>
					      <li><a href="#">Trajan</a></li>
					      <li><a href="#">Verdana</a></li>				      
					    </ul>
					  </div>					  
					  <!--div class="btn-group btn-group-sm dropup opacity">
					    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="text">10px</span><span class="caret"></span></button>
					    <ul class="dropdown-menu" role="menu" style="height: 150px; overflow: auto;">
					      <li><a href="#">0</a></li>
					      <li><a href="#">10</a></li>
					      <li><a href="#">20</a></li>
					      <li><a href="#">30</a></li>
					      <li><a href="#">40</a></li>
					      <li><a href="#">50</a></li>
					      <li><a href="#">60</a></li>
					      <li><a href="#">70</a></li>					      
					      <li><a href="#">80</a></li>
					      <li><a href="#">90</a></li>
					      <li><a href="#">100</a></li> 
					    </ul>
					  </div-->
					  <a class="btn btn-default istooltip toolimage" title="<?php echo ucfirst(JrTexto::_('Image')); ?>" ><i class="fa fa-image"></i></a>
					  <a class="btn btn-default istooltip toolpdf btnsharepdfinboard" title="<?php echo ucfirst(JrTexto::_('File PDF')); ?>" ><i class="fa fa-file-pdf-o"></i></a>
					  <a class="btn btn-default istooltip tooldownload" title="<?php echo ucfirst(JrTexto::_('download image')); ?>" ><i class="fa fa-download"></i></a>
				  	</div>
				</div>
				<div class="grupo-opciones pull-right pdffileok" style="display: none;" >
					<div class="btninpdf btn-group btn-group-sm" role="group">					  	
					  	<a href="#" class="btn btn-default istooltip" id="pdf-prev" title="<?php echo ucfirst(JrTexto::_('Previous Page'));?>" ><i class="fa fa-chevron-circle-left"></i> <?php echo ucfirst(JrTexto::_('Previous Page')); ?></a> 
						<a href="#" class="btn btn-default istooltip istooltip" id="pdf-next" title="<?php echo ucfirst(JrTexto::_('Next Page')); ?>"><?php echo ucfirst(JrTexto::_('Next Page')); ?> <i class="fa fa-chevron-circle-right"></i></a>
						<a href="#" class="btn btn-default"><span id="page-count-container">Page <span id="pdf-current-page"></span> of <span id="pdf-total-pages"></span></span></a>
					</div>
				</div>
			</div>
		</div>

		<!--div class="panel ventanas" id="vinvite" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Invite Mail')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">				 
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body">
			   	
			</div>
		</div-->

		<div class="panel ventanas" id="vvideos" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Shared Videos')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body">
			<div class="addvideoaqui"></div>		
			   
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px;">
				<div class="grupo-opciones">
					<button class="btn btn-xs btn-default btnsharevideo istooltip" title="<?php echo ucfirst(JrTexto::_('Upload Video')); ?>"><i class="fa fa-upload"></i><span class="sr-only"><?php echo ucfirst(JrTexto::_('Upload Video')); ?></span></button>
				</div>
			</div>
		</div>
		<div class="panel ventanas" id="vaudios" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Audios de Participantes')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body" id="container-audios" style="padding: 1ex;">
			</div>			
		</div>

		<div class="panel ventanas" id="vemiting" style="display: none;" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Meeting')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  		 
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body">			
			   	<!--div id="medias-container">
			   		
			   	</div-->
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px;">
				<?php require("iconos.php"); ?>
				<div class="grupo-opciones">
					<button class="btn btn-xs btn-default btnherramienta btnshareventana istooltip" title="<?php echo ucfirst(JrTexto::_('Share')); ?>"><i class="fa fa-share"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_('Share')); ?></span></button>
					<button class="btn btn-xs btn-default btnherramienta btnfullscreen istooltip" title="<?php echo ucfirst(JrTexto::_('Full Screen')); ?>"><i class="fa fa-window-maximize"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_('Full Screen')); ?></span></button>
				</div>
			</div>
		</div>

		<div class="panel ventanas" id="vfiles" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Shared Files')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body">			
			   	<table class="table table-striped">			   		
			   			<tr id="addfileshareclone" >
			   				<td class="nombreuser"><?php echo JrTexto::_("User") ?></td>
			   				<td class="nombrefile"><?php echo JrTexto::_("Name File") ?></td>
			   				<td class="extension"><?php echo JrTexto::_("File Extension") ?></td>
			   				<td class="link"><a href="#"><?php echo ucfirst(JrTexto::_('Download')); ?></a></td>
			   			</tr>			   						   		
			   	</table>
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px;">
				<div class="grupo-opciones">
					<button class="btn btn-xs btn-default btnsharefile istooltip" title="<?php echo ucfirst(JrTexto::_('Upload file')); ?>"><i class="fa fa-upload"></i><span class="sr-only"><?php echo ucfirst(JrTexto::_('Upload file')); ?></span></button>
				</div>
			</div>
		</div>

		<div class="panel ventanas" id="vwebcam" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Webcam')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body" id="container-webcams" style="padding: 1ex;">
			</div>
			<div class="panel-footer" style="position: absolute; width: 100%; bottom: 0px; padding: 5px;">
				<div class="grupo-opciones">
					<button class="btn btn-xs btn-default btnherramienta btnshareventana istooltip" title="<?php echo ucfirst(JrTexto::_('Share')); ?>"><i class="fa fa-share"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_('Share')); ?></span></button>
					<button class="btn btn-xs btn-default btnherramienta btnfullscreen istooltip" title="<?php echo ucfirst(JrTexto::_('Full Screen')); ?>"><i class="fa fa-window-maximize"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_('Full Screen')); ?></span></button>
				</div>
			</div>
		</div>

		<div class="panel ventanas" id="vencuesta" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1em;"><?php echo ucfirst(JrTexto::_('Encuesta')); ?></h3>	
					<span class="pull-right btn-group btn-group-sm">					  
					  <span type="button" class="btn btn-primary clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  <span type="button" class="btn btn-primary clickmaximizable"><i class="fa fa-window-maximize "></i></span>
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body" style="padding: 1ex" >
				<div class="form-group">    
	                <label><?php echo ucfirst(JrTexto::_("Question"))?> :</label>
	                <input type="text" name="pregunta" id="pregunta" value="Pregunta" class="form-control" placeholder="¿Tu Pregunta ?">
	            </div> 
	            <div class="form-group">
	            <label><?php echo ucfirst(JrTexto::_("Alternatives"))?> :</label>
	            	<span class="btn btn-xs fa fa-plus-circle colorgreen" id="encuesta_addalternativa" style=" font-size: 1.5em !important;"> </span>
		            <table style="width: 100%">
		            	<tr class="no" id="encuesta_clonealternativa" style="display: none">
		            	<td style="padding: 1ex"><input type="text" name="alternative" id="alternative" value="<?php echo JrTexto::_('No') ?>" class="form-control" placeholder="alternativa"></td>
		            	<td style="text-align: center;"><span class="btn btn-xs fa fa-trash colored encuesta_removealternativa" style=" font-size: 1.2em !important;"> </span> </td>
		            	</tr>
		            	<tr class="si">
		            	<td style="padding: 1ex"><input type="text" name="alternative" id="alternative" value="<?php echo JrTexto::_('Yes') ?>" class="form-control" placeholder="alternativa"></td>
		            	<td style="text-align: center;"><span class="btn btn-xs fa fa-trash colored encuesta_removealternativa" style=" font-size: 1.2em !important;"> </span> </td>
		            	</tr>
		            	<tr class="si">
		            	<td style="padding: 1ex"><input type="text" name="alternative" id="alternative" value="<?php echo JrTexto::_('No') ?>" class="form-control" placeholder="alternativa"></td>
		            	<td style="text-align: center;"><span class="btn btn-xs fa fa-trash colored encuesta_removealternativa" style=" font-size: 1.2em !important;"> </span> </td>
		            	</tr>
		            </table>	                
	            </div>
	            <hr>
	            <div class="text-center">
	            	<button class="btn btn-primary btn_encuesta_enviar"><?php echo JrTexto::_('Send Question') ?></button>	            	
	            </div>
            	<div class="resultadoencuesta"><hr></div>
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="modalquestion" >
			  <div class="modal-dialog modal-sm" role="document">
			    <div class="modal-content">
			      <div class="modal-header">        
			        <h4 class="modal-title" ><?php echo ucfirst(JrTexto::_('Window Question')); ?></h4>
			      </div>
			      <div class="modal-body text-center">        
			        <strong class="aquipregunta text-center">¿ pregunta?</strong>
			        <div class="graficodemela"></div>
			      </div>
			      <div class="modal-footer aquialternativas" style="text-align:center !important;">        
			        <a href="">Alternativa</a>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		<!--div class="panel ventanas" id="vlink" style="display: none;">
			<div class="panel-heading bg-blue">	
			<h3></h3>				
					<span class="pull-right btn-group btn-group-sm">
					  <span type="button" class="btn btn-primary clickclose"><i class="fa fa-times"></i></span>
					</span>
					<div class="clearfix"></div>
			</div>
			<div class="panel-body" style="padding: 1ex" >
				
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="modalquestion" >
			  <div class="modal-dialog modal-sm" role="document">
			    <div class="modal-content">
			      <div class="modal-header">        
			        <h4 class="modal-title" ><?php //echo ucfirst(JrTexto::_('Window Question')); ?></h4>
			      </div>
			      <div class="modal-body text-center">        
			        <strong class="aquipregunta text-center">¿ pregunta?</strong>
			        <div class="graficodemela"></div>
			      </div>
			      <div class="modal-footer aquialternativas" style="text-align:center !important;">        
			        <a href="">Alternativa</a>
			      </div>
			    </div>
			  </div>
			</div>
		</div-->
</div>
<audio src="" style="display: none" id="audiosonidos"></audio>
<div id="divprecargafile" style="display: none; position: absolute; bottom: 2ex; right: 20%; width: 300px; height: 150px; background: #fff;">
  <div style="position: absolute; bottom:0px; width:100%">
    <div class="text-center"><img width="30px" src="<?php echo $this->documento->getUrlStatic() ?>/sysplantillas/default.gif"></div>
    <div class="progress" style="margin-bottom: 0px;">  
      <div class="progress-bar progress-bar-striped progress-bar-animated focus active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:70%">
        <?php echo JrTexto::_('loading').'...'; ?><span>70%</span>
      </div>
    </div>
  </div>
</div>
<div id="infousersala" style="display: none;">
	<span class="_salaid"><?php echo $curaula["aulaid"];?></span>
	<span class="_sala">room<?php echo date("Ymd").$curaula["dni"].$curaula["aulaid"];?>c</span>
	<span class="_user"><?php echo @str_replace(" ","_",$this->userssmart);?></span>
	<span class="_dnimoderador"><?php echo $curaula["dni"];?></span>
	<span class="_typeuser"><?php echo $this->tiposmart;?></span>
	<span class="_username"><?php echo ucfirst($this->userssmart);?></span>
	<span class="_useremail"><?php echo $this->emailsmart;?></span>
</div>

<script type="text/javascript">
/******************************** variables para pdf ******************************************/
    var __PDF_DOC,
        __PDFCURRENT_PAGE=0,
        __PDFTOTAL_PAGES,
        __PDFPAGE_RENDERING_IN_PROGRESS = 0,
        __CANVAS = document.getElementById('micanvaspdf'),
        __CANVAS_CTX = __CANVAS.getContext('2d');
        PDFJS.disableWorker = true;
    var colorcanvas='#fff';    
    var colorborde='#000000';
    var colorfondo='#000000';
    var acccionincanvas=false;
    var canvasfabric =  new fabric.Canvas('micanvas');
    var divPos = {};    
/********************************************************************************************/
	var tipouser='<?php echo $this->tiposmart;?>';
	var userid='<?php echo ucfirst($this->userssmart);?>';
	var roomid='';
    var _initsala=false;
    if(tipouser=='M') _initsala=true;
	var permiso={
		emitiendo:false,
		micro:false,
		webcam:false,
		sharedesktop:false,
		lookedroom:false,
		emitpizarra:false,
		pizarra:false,
	}
	var resetmedios=function(){
		if(permiso.micro){
			permiso.webcam=true;
			$('.btnsharemicro').closest('li').show('fast');
		}else{
			$('.btnsharemicro').closest('li').hide('fast');
		}
		if(permiso.webcam){
			$('.btnsharecamera').closest('li').show('fast');
			$('[data-ventana="vwebcam"]').closest('li').show('fast');
		}else{
			$('.btnsharecamera').closest('li').hide('fast');
			$('[data-ventana="vwebcam"]').closest('li').hide('fast');
		}		
		if(permiso.sharedesktop){
			$('.btnsharedesktop').closest('li').show('fast');
		}else{
			$('.btnsharedesktop').closest('li').hide('fast');			
		}
		if(permiso.lookedroom){
			$('.btnlokedroom').closest('li').show('fast');
		}else{
			$('.btnlokedroom').closest('li').hide('fast');			
		}
		if(permiso.emitpizarra){
			$('#vpizarra .emitpizarra').hide();
			$('#vpizarra #userspizarra').show();
		}else{
			$('#vpizarra .emitpizarra').hide();
			$('#vpizarra #userspizarra').hide();			
		}
		sessionStorage.setItem('permiso', JSON.stringify(permiso));
		sessionStorage.setItem('roomid', roomid);
		sessionStorage.setItem('userid', userid);
		sessionStorage.setItem('tipouser', tipouser);
		sessionStorage.setItem('_initsala', _initsala);
		sessionStorage.setItem('trasmitiendo', permiso.emitiendo);
	}

	var resetprivilegios=function(){
		$('#userchataccion').show();  //acciones en participantes
		var usersparticipante=$('#usersparticipante'); //participantes
		if(tipouser=='M'){
			usersparticipante.find('.miestatus').removeClass('disabled').find('span.caret').show();
			usersparticipante.find('span.userchataccion').show(0);
			//usersparticipante.find('span.userchataccion .btn-group').show(0);
    		usersparticipante.find('a.miestatus').removeClass('disabled');
    		usersparticipante.find('a.miestatus span').show(0);
    		permiso.emitiendo=true;
    		permiso.pizarra=true;
    		permiso.emitpizarra=true;
		}else if(tipouser=='P'){
			usersparticipante.find('.miestatus').removeClass('disabled').find('span.caret').show();
			usersparticipante.find('span.userchataccion').show(0);
    		usersparticipante.find('a.miestatus').addClass('disabled');
    		usersparticipante.find('a.miestatus span').hide(0);	
    		permiso.emitiendo=true;
    		permiso.pizarra=true;
    		permiso.emitpizarra=true;	
		}else if(tipouser=='U'){
			usersparticipante.find('.miestatus').addClass('disabled').find('span.caret').hide();
			usersparticipante.find('span.userchataccion').hide(0);
    		usersparticipante.find('a.miestatus').addClass('disabled');
    		usersparticipante.find('a.miestatus span').hide(0); 
			$('.userchataccion').hide();
			permiso.emitiendo=false;
			permiso.pizarra=false;
    		permiso.emitpizarra=false;
		}
		if(permiso.emitiendo){
			$('.btnalzarmano').closest('li').hide('fast');
			$('.botones-compartir').show('fast');
			$('.btnshareventana').show('fast');
			$('.menucompartir').show('fast');
			$('[data-ventana="vfiles"]').closest('li').show('fast');
			$('[data-ventana="vpizarra"]').closest('li').show('fast');
			$('[data-ventana="vencuesta"]').closest('li').show('fast');
			permiso.webcam=true;
			permiso.micro=true;
			permiso.sharedesktop=true;
			permiso.lookedroom=true;
		}else{
			$('.btnalzarmano').closest('li').show('fast');
			$('.botones-compartir').hide('fast');
			$('.btnshareventana').hide('fast');
			$('.menucompartir').hide('fast');
			$('[data-ventana="vfiles"]').closest('li').hide('fast');
			$('[data-ventana="vpizarra"]').closest('li').hide('fast');
			$('[data-ventana="vencuesta"]').closest('li').hide('fast');
			permiso.webcam=false;
			permiso.micro=false;
			permiso.sharedesktop=false;
			permiso.lookedroom=false;
		}
		resetmedios();		
	}

$(document).ready(function(){	
	var redimencionarchat=function(){
	    var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
	    var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
	    var _height=(height-60);
	    var _heightmitad=_height/2;
	    $('#pantalla1').css('height',_height+'px');//.css('width',(width-5)+'px');
	    var heightchat =$('#vchat .chatAll').height();
	    heightchat=heightchat>150?heightchat:150;
	    $('#vchat .mensajes').css('height',heightchat-110+'px');    	
	    var widthcanvas=$('#vpizarra .panel-body').width();
	    var heightcanvas=$('#vpizarra .panel-body').height();
	    __CANVAS.width=widthcanvas;
	    __CANVAS.height=heightcanvas;	  
	    canvasfabric.setWidth(widthcanvas);
    	//canvasfabric.setHeight(heightcanvas);
    	canvasfabric.renderAll();
	    //if(__PDFCURRENT_PAGE>0) showPage(__PDFCURRENT_PAGE);	   	

	    //$('#vparticipantes').css('height',_heightmitad-10+'px').removeAttr('style');
	    //$('#vchat').css('height',_heightmitad-10+'px');

	    /*var widthcanvas= ((width*74)/100)-70;
	    if(height>400){
	        var heightchat=height-225;
	        $('.pnlinformacion .panelchats').css('height',heightchat+'px');
	        $('.pnlinformacion .panelchats .chatAll').css('height',(heightchat)+'px');        
	        $('.pnlinformacion .panelchats .chatAll .mensajes').css('height',(heightchat-60)+'px');
	        $('#canvaspizarra').css('height',(height-55)+'px').css('width',(widthcanvas)+'px');
	        //$('.pnlinformacion .panelchats').css('height',heightchat+'px');
	    }*/
	}
	$(window).resize(function(){
	     redimencionarchat(); 
	});
	redimencionarchat();
	
	var mostrarventa=function(ventana,acc){		
		var dacc=acc||false;		
		if(acc==true){
			$('.ventanas').removeClass('active');
			$(ventana).addClass('active').show(300,function(){
				if(ventana==='#vpizarra'){
					var widthcanvas=$('#vpizarra .panel-body').width()+1;
				    var heightcanvas=$('#vpizarra .panel-body').height()+1;
				    __CANVAS.width=widthcanvas;
				    __CANVAS.height=heightcanvas;	  
				    canvasfabric.setWidth(widthcanvas);
			    	canvasfabric.setHeight(heightcanvas);
			    	canvasfabric.renderAll();
			    	redimencionarchat();
		    	}				
			});
		}else{
			$(ventana).hide(300).removeClass('active');
		}
	}

	$('#pantalla1').on('click','.clickclose',function(){
		var vid=$(this).closest('.ventanas').attr('id');
		$('#'+vid).hide(100);
		$('#menupersonalizado .showwindow[data-ventana="'+vid+'"]').removeClass('active');
		 screenfull.exit();		
		if(vid==='vvideos'){
			$('#'+vid).find('video').trigger('pause');
		}
	}).on('click','.clickable',function(){
		var vid=$(this).closest('.ventanas').attr('id');
		var i=$('i',this);
		var vidcur=$('#'+vid);
		var hayfooter=vidcur.find('.panel-footer').length;
		if(i.hasClass('glyphicon-chevron-up')){
			if(hayfooter>0)vidcur.find('.panel-footer').hide(0);
			vidcur.attr('data-height',vidcur.height()).css('height','auto');	
		}else{
			if(hayfooter>0)vidcur.find('.panel-footer').show(0);
			vidcur.css('height',vidcur.attr('data-height')+'px').removeAttr('data-height');
		}		
	}).on('click','.clickmaximizable',function(ev){
		ev.preventDefault();
		var vid=$(this).closest('.ventanas').attr('id');
		var i=$('i',this);
		var vidcur=$('#'+vid);		
		if(i.hasClass('fa-window-maximize')){
			i.removeClass('fa-window-maximize').addClass('fa-window-restore');			
			$('#menupersonalizado .showwindow.active').each(function(){
				var idv=$(this).attr('data-ventana');
				var _idv=$('#'+idv);
				_idv.attr('data-zindex',_idv.css('z-index'));
				if(idv==vid){
					_idv.attr('data-style',vidcur.attr('style')).attr('style','display:block; top:0px; right:0px; left:0px; bottom:0px; width:100%; height:99% !important; z-index:92;').css('top','0px');
					//screenfull.toggle(_idv[0]);
				}
			});			
		}else{
			i.removeClass('fa-window-restore').addClass('fa-window-maximize');
			$('#menupersonalizado .showwindow.active').each(function(){
				var idv=$(this).attr('data-ventana');
				var _idv=$('#'+idv);
				_idv.css('z-index',_idv.attr('data-zindex')).removeAttr('data-zindex');
				if(idv==vid){
					_idv.removeAttr('style').attr('style',vidcur.attr('data-style')).removeAttr('data-style');
				}
			});
		}
		redimencionarchat();
	}).on('click','.btnfullscreen',function(){
		screenfull.toggle($(this).closest('.panel.ventanas')[0]);
		redimencionarchat();
	});


	$('#menupersonalizado').on("click",".resetwindow",function(){
		$('.ventanas').removeAttr('style');
		$('#menupersonalizado .showwindow').removeClass('active');
		$('#menupersonalizado .showwindow.init').addClass('active');
		$('.clickmaximizable').find('i').removeAttr('class').addClass('fa fa-window-maximize');
		/*$('#menupersonalizado .showwindow.active').each(function(){
			var idv=$(this).attr('data-ventana');
			$('#'+idv).attr('style',$('#'+idv).attr('data-style'));
		});*/
	}).on("click",".showwindow",function(){
		var obj=$(this);
		var v1=$(this).attr('data-ventana');
		obj.toggleClass('active');		
		if(obj.hasClass('active'))
			mostrarventa('#'+v1,true);
		else 
			mostrarventa('#'+v1,false);
	});	

	var mostrarEditorMCE  = function(obj,showtoolstiny){
	  var showtools=showtoolstiny||'';
	  tinymce.init({
	  	theme_advanced_resizing : "true",
		theme_advanced_resize_horizontal : "true",
	    relative_urls : false,
	    remove_script_host: false,
	    convert_newlines_to_brs : true,
	    menubar: false,
	    statusbar: false,
	    verify_html : false,
	    content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
	    selector: obj,
	    height: 200,
	    paste_auto_cleanup_on_paste : true,
	    paste_preprocess : function(pl, o) {
	        var html='<div>'+o.content+'</div>';
	        var txt =$(html).text(); 
	        o.content = txt;
	    },paste_postprocess : function(pl, o) {       
	        o.node.innerHTML = o.node.innerHTML;
	    },
	    plugins:[showtools+"  link image textcolor paste" ],  //chingosave chingoinput chingoimage chingoaudio chingovideo styleselect
	    toolbar: ' undo redo |  bold italic underline | alignleft aligncenter alignright alignjustify | numlist |  forecolor backcolor |  '+showtools // chingosave chingoinput chingoimage chingoaudio chingovideo 
	  });
	};
	mostrarEditorMCE('#vtxtnotes');	
	/*var init=function(){
		resetprivilegios();
	}*/
	/*init();*/
	//$(".userchatsms").select2();
	$(".istooltip").tooltip();
	$(".ventanas" ).draggable({ containment: "#pantalla1",  handle: ".panel-heading" }).resizable({containment: "#pantalla1",minHeight: 120,minWidth: 50});
	$(".ventanas .panel-heading").mousedown(function(){
		$('.ventanas').removeClass('active');
		$(this).closest(".ventanas").addClass('active');
	});
	$('header').addClass('static');	
	$('#vencuesta').on('click','.encuesta_removealternativa',function(){
		$(this).closest('tr').remove();
	}).on('click','#encuesta_addalternativa',function(){
		var newalt=$('#vencuesta #encuesta_clonealternativa').clone(true);
		newalt.removeClass('no').addClass('si').removeAttr('style').removeAttr('id');
		newalt.find('input').val('');
		$('#vencuesta table ').append(newalt);
	});
});
</script>