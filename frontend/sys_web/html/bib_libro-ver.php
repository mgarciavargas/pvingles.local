
<?php 
$pk=$this->pk;
$reg=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Ver Bib_libro'); ?></h2>
        <div class="btn-group btn-group-md" style="float:right">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('bib_libro'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir a listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idlibro"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idlibro"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>            
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-md-12">
        <div id="msj-interno">
        <table class="table table-striped">
         <tr>
              <th><?php echo JrTexto::_("Idlibro") ;?> </th>
              <th>:</th> 
          <td><?php echo $reg["idlibro"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Codigo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["codigo"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Nombre") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["nombre"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Precio") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo number_format($reg["precio"],null,"."," "); ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Editorial") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["editorial"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Edicion") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["edicion"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Fec publicacion") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["fec_publicacion"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Foto") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><img src="<?php echo $this->documento->getUrlBase().$reg["foto"]; ?>" class="img-thumbnail" style="max-height:100px; max-width:250px;"></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Archivo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><a href="<?php echo $reg["archivo"]; ?>" target="_blank"><?php echo JrTexto::_("ver documento"); ?></a></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Tipo")."(".JrTexto::_("Idtipo").")"; ?> </th>
                <th style="max-width:20px;">:</th>
                <td ><?php echo !empty($reg["_idtipo"])?$reg["_idtipo"]:null; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Autor")."(".JrTexto::_("Idautor").")"; ?> </th>
                <th style="max-width:20px;">:</th>
                <td ><?php echo !empty($reg["_idautor"])?$reg["_idautor"]:null; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Condicion") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["condicion"] ;?></td>
          </tr>
                    </table>
          </div>
          <hr><div class="text-center">
          <div class="btn-group btn-group-md">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('bib_libro'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir al listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idlibro"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idlibro"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>
	        </div>         
	    </div>
</div>
<br />
	</div>
	<script type="text/javascript">
  var asInitVals = new Array();
	$(document).ready(function(){
    
    $('.btnNuevo').click(function(){
      $('#pkaccion').val('guardar');
      $('#pkidlibro').val('');
      $('.img-thumbnail').attr("style","display:none");
    });

    $('.editar').click(function(){
      $('.img-thumbnail').removeAttr("style");
      var id=$(this).attr("data-reg");
            recargarpagina('admin/bib_libro/frm/?tpl=g&acc=Editar&id='+id);
          
    });

    $('.eliminar').click(function(){
     if(confirm("<?php echo JrTexto::_('¿Desea eliminar el registro seleccionado?');?>")) {
        var id=$(this).attr("data-reg");
        var res = xajax__('', 'Bib_libro', 'eliminarBib_libro',id)
        if(res) {
          agregar_msj_interno('success', "<?php echo JrTexto::_('Registro eliminado');?>");
                 recargarpagina('bib_libro/frm/?tpl=g&acc=Editar&id='+id);
              } 
        $('.alert').fadeOut(4000);
      }
    });
	});
</script>