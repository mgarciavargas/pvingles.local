<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<style type="text/css">
    .btn-panel-container p { font-size: 0.7em; }
    .btn-panel-container>.btn-panel { 
        display: block; 
        padding-top: 90px;
    }
    .btn-panel-container.panelcont-xs>.btn-panel { height: 300px; }
    
    #alum-libre_tipo #slider-botones .btn-panel-container .btn-panel { border-color: transparent; }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+1) .btn-panel { background-color: rgba(47, 129, 212, 1); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+7) .btn-panel { background-color: rgba(47, 129, 212, 0.85); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+13) .btn-panel { background-color: rgba(47, 129, 212, 0.7); }

    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+2) .btn-panel { background-color: rgba(46, 204, 113, 1); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+8) .btn-panel { background-color: rgba(46, 204, 113, 0.85); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+14) .btn-panel { background-color: rgba(46, 204, 113, 0.7); }

    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+3) .btn-panel { background-color: rgba(240, 173, 78, 1); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+9) .btn-panel { background-color: rgba(240, 173, 78, 0.85); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+15) .btn-panel { background-color: rgba(240, 173, 78, 0.7); }

    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+4) .btn-panel { background-color: rgba(91, 192, 222, 1); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+10) .btn-panel { background-color: rgba(91, 192, 222, 0.85); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+16) .btn-panel { background-color: rgba(91, 192, 222, 0.7); }

    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+5) .btn-panel { background-color: rgba(217, 83, 79, 1); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+11) .btn-panel { background-color: rgba(217, 83, 79, 0.85); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+17) .btn-panel { background-color: rgba(217, 83, 79, 0.7); }

    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+6) .btn-panel { background-color: rgba(155, 89, 182, 1); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+12) .btn-panel { background-color: rgba(155, 89, 182, 0.85); }
    #alum-libre_tipo #slider-botones .btn-panel-container:nth-child(18n+18) .btn-panel { background-color: rgba(155, 89, 182, 0.7); }
</style>

<div class="row" id="alum-libre_tipo">
    <?php if(!empty($this->datos)) { ?>
    <div id="slider-botones">
        <?php foreach ($this->datos as $t) {?>
        <div class="col-xs-6 col-md-4 btn-panel-container panelcont-xs">
            <a href="<?php echo $this->documento->getUrlBase().'/tema_libre/'.$this->ruta.$t[ $this->campo_id ];?>" class="btn btn-block btn-panel btn-primary">
                <!--i class="btn-icon fa fa-bullhorn"></i-->
                <h3><?php echo $t['nombre']; ?></h3>
                <p><?php echo @$t['descripcion']; ?></p>
            </a>
        </div>
        <?php } ?>
    </div>
    <?php } else { ?>
    <div class="text-center">
        <h1> <i class="fa fa-minus-circle fa-3x"></i> </h1>
        <h4> <?php echo JrTexto::_("No categories found"); ?></h4>
    </div>
    <?php } ?>

</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#slider-botones').slick({
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3
    });
});
</script>