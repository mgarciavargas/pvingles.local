<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Tema"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
                                                                    
            <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
              <select id="fkcbidtipo" name="fkcbidtipo" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                <?php 
                if(!empty($this->fkidtipo))
                  foreach ($this->fkidtipo as $fkidtipo) { ?><option value="<?php echo $fkidtipo["idtipo"]?>" <?php echo $fkidtipo["idtipo"]==@$frm["idtipo"]?"selected":""; ?> ><?php echo $fkidtipo["nombre"] ?></option><?php } 
                ?>                        
              </select>
            </div>
                          
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Libre_tema", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Libre_tema").' - '.JrTexto::_("Add"); ?>"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('Add'))?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Nombre") ;?></th>
                    <th><?php echo JrTexto::_("Descripcion") ;?></th>
                    <th><?php echo JrTexto::_("Tipo"); ?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody> </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5ad62635cef02='';
function refreshdatos5ad62635cef02(){
    tabledatos5ad62635cef02.ajax.reload();
}
$(document).ready(function(){  
  var estados5ad62635cef02={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5ad62635cef02='<?php echo ucfirst(JrTexto::_("Tema"))." - ".JrTexto::_("edit"); ?>';
  var draw5ad62635cef02=0;

  
  $('#fkcbidtipo').change(function(ev){
    refreshdatos5ad62635cef02();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos5ad62635cef02();
  });
  tabledatos5ad62635cef02=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
        {'data': '<?php echo JrTexto::_("Descripcion") ;?>'},
        {'data': '<?php echo JrTexto::_("Tipo") ;?>'},

        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/libre_tema/buscarjson/?json=true',
        type: "post",
        data:function(d){
            d.json=true                   
            d.idtipo=$('#fkcbidtipo').val(),
            d.nombre=$('#texto').val(),
                        
            draw5ad62635cef02=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5ad62635cef02;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Nombre") ;?>': data[i].nombre,
              '<?php echo JrTexto::_("Descripcion") ;?>': data[i].descripcion,
              '<?php echo JrTexto::_("Tipo") ;?>': data[i].tipo_nombre+' - '+data[i].tipo_contenido,
                    
              '<?php echo JrTexto::_("Actions") ;?>':'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/libre_tema/editar/?id='+data[i].idtema+'" data-titulo="'+tituloedit5ad62635cef02+'" title="<?php echo JrTexto::_("Edit"); ?>"><i class="fa fa-edit"></i></a><a class="btn-palabras btn btn-xs" href="'+_sysUrlBase_+'/libre_palabras/agregar/?idtema='+data[i].idtema+'" data-id="'+data[i].idtema+'" title="<?php echo JrTexto::_("Add content"); ?>"><i class="fa fa-th-list"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idtema+'" title="<?php echo JrTexto::_("Delete"); ?>"><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'libre_tema', 'setCampo', id,campo,data);
          if(res) tabledatos5ad62635cef02.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5ad62635cef02';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Tema';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'libre_tema', 'eliminar', id);
        if(res) tabledatos5ad62635cef02.ajax.reload();
      }
    }); 
  });
});
</script>