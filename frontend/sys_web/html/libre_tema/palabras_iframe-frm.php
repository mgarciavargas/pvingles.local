<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<style>
    .input-group { margin-top: 0px; }
    iframe#url-externa { border: 0; height: 415px; width: 100%; }
</style>

<div class="row" id="palabras_iframe-frm">
    <input type="hidden" name="hIdTema" id="hIdTema" value="<?php echo $this->temas[0]['idtema']; ?>">
    <input type="hidden" name="hIdPalabra" id="hIdPalabra" value="<?php echo @$this->palabras['idpalabra']; ?>">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title"><?php echo $this->temas[0]['tipo_nombre'].': '.$this->temas[0]['nombre']; ?></h2>
            </div>
            <div class="panel-body">
                <div id="edicion">
                    <div class="col-xs-12 padding-0 form-group">
                        <div class="col-xs-12 col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon" id="lblOrigen"><?php echo JrTexto::_("Source") ?></span>
                                <input type="text" class="form-control" id="txtOrigen" name="txtOrigen" value="<?php echo @$this->origen; ?>">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon" id="lblURL"><?php echo JrTexto::_("URL") ?></span>
                                <input type="url" class="form-control" id="txtURL" name="txtURL" aria-describedby="lblURL" placeholder="https://www.mypage.com" value="<?php echo @$this->palabras['url_iframe']; ?>">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <button class="btn btn-primary btn-geturl"><i class="fa fa-link"></i> <?php echo JrTexto::_("Get URL"); ?></button>
                        </div>
                    </div>
                </div>
                <div id="vista_previa" class="hidden">
                    <div class="col-xs-12 text-center">
                        <buttton class="btn btn-success btn-guardar"><i class="fa fa-save"></i> <?php echo JrTexto::_("Save"); ?></buttton>
                    </div>
                    <div class="col-xs-12 padding-0">
                        <iframe src="<?php echo @$this->palabras['url_iframe']; ?>" id="url-externa"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var existeIframe = <?php echo !empty($this->palabras)?'true':'false' ?>;
var showVistaPrevia = function(mostrar) {
    mostrar = (typeof mostrar=="undefined")?true:mostrar;
    if(mostrar) {
        $('#vista_previa').removeClass('hidden');
    } else {
        $('#vista_previa').addClass('hidden');
    }
}
$(document).ready(function() {
    $('.btn-geturl').click(function(e) {
        var txt_url = $('#txtURL').val();
        if( esURLValida(txt_url) ){ /* funciones.js */
            $('#url-externa').attr('src', txt_url);
            showVistaPrevia();
        } else {
            showVistaPrevia(false);
        }
        return false;
    });

    $('.btn-guardar').click(function(e) {
        var url = $("#url-externa").attr('src');
        $.ajax({
            url: _sysUrlBase_+'/libre_palabras/guardarLibre_palabras',
            type: 'POST',
            dataType: 'json',
            data: {
                'pkIdpalabra': $('#hIdPalabra').val(),
                'txtIdtema': $('#hIdTema').val(),
                'txtOrigen': $('#txtOrigen').val(),
                'txtUrl_iframe': url
            },
        }).done(function(resp) {
            if (resp.code=='ok') {
                $('#hIdPalabra').val(resp.newid);
                mostrar_notificacion('<?php echo JrTexto::_("Success") ?>', resp.msj, 'success');
            } else {
                mostrar_notificacion('<?php echo JrTexto::_("Error") ?>', resp.msj, 'error');
            }
        }).fail(function(err) {
            console.log("error");
        }).always(function() {});
        
        return false;
    });

    if(existeIframe) {
        showVistaPrevia();
    }
});
</script>