
<?php 
$pk=$this->pk;
$reg=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Ver Libre_palabras'); ?></h2>
        <div class="btn-group btn-group-md" style="float:right">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('libre_palabras'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir a listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idpalabra"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idpalabra"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>            
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-md-12">
        <div id="msj-interno">
        <table class="table table-striped">
         <tr>
              <th><?php echo JrTexto::_("Idpalabra") ;?> </th>
              <th>:</th> 
          <td><?php echo $reg["idpalabra"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("E tema")."(".JrTexto::_("Nombre").")"; ?> </th>
                <th style="max-width:20px;">:</th>
                <td ><?php echo !empty($reg["_nombre"])?$reg["_nombre"]:null; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Palabra") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["palabra"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Audio") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["audio"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idagrupacion") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["idagrupacion"],0,1000)."..."; ?></td>
          </tr>
                    </table>
          </div>
          <hr><div class="text-center">
          <div class="btn-group btn-group-md">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('libre_palabras'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir al listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idpalabra"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idpalabra"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>
	        </div>         
	    </div>
</div>
<br />
	</div>
	<script type="text/javascript">
  var asInitVals = new Array();
	$(document).ready(function(){
    
    $('.btnNuevo').click(function(){
      $('#pkaccion').val('guardar');
      $('#pkidpalabra').val('');
      $('.img-thumbnail').attr("style","display:none");
    });

    $('.editar').click(function(){
      $('.img-thumbnail').removeAttr("style");
      var id=$(this).attr("data-reg");
          addFancyAjax("<?php echo JrAplicacion::getJrUrl(array('Libre_palabras', 'frm'))?>?tpl=b&acc=Editar&id="+id, true);
          
    });

    $('.eliminar').click(function(){
     if(confirm("<?php echo JrTexto::_('¿Desea eliminar el registro seleccionado?');?>")) {
        var id=$(this).attr("data-reg");
        var res = xajax__('', 'Libre_palabras', 'eliminarLibre_palabras',id)
        if(res) {
          agregar_msj_interno('success', "<?php echo JrTexto::_('Registro eliminado');?>");
                    recargarpagina(false);
                    } 
        $('.alert').fadeOut(4000);
      }
    });
	});
</script>