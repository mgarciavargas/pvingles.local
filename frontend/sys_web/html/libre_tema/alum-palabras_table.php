<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<div class="row" id="alum-palabras_table">
    <div class="col-xs-12 col-sm-offset-2 col-sm-8">
        <table class="table table-striped table-hover" id="tblPalabras">
            <thead>
                <tr class="bg-orange">
                    <?php if(!empty($this->datos)) {
                    foreach ($this->datos as $idagrupacion=>$arrPalabras) { 
                        foreach ($arrPalabras as $j=>$p) {?>
                    <th><span class="pull-right">A-Z</span></th>
                    <?php  } break; } } ?>
                </tr>
            </thead>
            <tbody>
                 <?php foreach ($this->datos as $idagrupacion=>$arrPalabras) { ?>
                 <tr data-idagrupacion="<?php echo $idagrupacion; ?>">
                    <?php foreach ($arrPalabras as $j=>$p) { ?>
                    <td>
                        <button class="btn btn-xs btn-orange <?php echo empty($p['audio'])?'pronunciar':'btn-audio'; ?>" data-idaudio="audio_<?php echo $p['idpalabra']; ?>"><i class="fa fa-volume-down"></i></button>
                        <span class="texto"><?php echo $p["palabra"]; ?></span>

                        <?php if(!empty($p["audio"])){
                            $src = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $p["audio"]);  ?>
                        <audio src="<?php echo $src; ?>" id="audio_<?php echo $p['idpalabra']; ?>" class="hidden" onended="endAudio(this)"></audio>
                        <?php } ?>

                    </td>
                    <?php } ?>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
function endAudio(audio) {
    $(audio).removeClass('playing');
    $(audio).removeClass('paused');
}

$(document).ready(function() {
    $('#tblPalabras').DataTable({
        "paging":   false,
        "info":     false
    });

    $(".btn-audio").click(function(e) {
        var idAudio = $(this).attr('data-idaudio');
        var $audio = $('#'+idAudio);
        if( $audio.length ){
            if( !$audio.hasClass('playing') ){
                $('audio.playing').each(function(){
                    this.pause(); this.currentTime = 0; /* stopping audio */
                    endAudio(this);
                });
                $audio.addClass('playing');
                $audio.removeClass('paused');
                $audio.trigger('play');
            } else {
                $audio.addClass('paused');
                $audio.removeClass('playing');
                $audio.trigger('pause');
            }
        }
        return false;
    });} );
</script>