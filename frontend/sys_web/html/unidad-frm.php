<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="title_left">
        <h3><?php echo JrTexto::_('Unit'); ?><small> <?php echo JrTexto::_($this->frmaccion);?></small></h3>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" align="center">
      
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdnivel" id="pkidnivel" value="<?php echo $this->pk; ?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo $this->frmaccion; ?>">
           <input type="hidden"  id="txtOrden" name="txtOrden" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo !empty($frm["orden"])?$frm["orden"]:0; ?>">
            <div class="form-group">
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtTipo">
              <?php echo JrTexto::_('Level');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12"> 
                <select id="txtIdpadre" class="form-control col-md-7 col-xs-12"  name="txtIdpadre" >
          <?php 
          $selidpadre=$this->frmaccion=='New'?$_GET["idnivel"]:$this->idnivel;
          foreach ($this->niveles as $nivel) { ?>
            <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $selidpadre===$nivel["idnivel"]?'selected="selected"':''?>> <?php echo $nivel["nombre"]?> </option>
          <?php } ?>             
          </select> 
              </div>
            </div>
            <div class="form-group" >
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('Nombre');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>">                                  
              </div>
            </div>

            
            <input type="hidden"  id="txtTipo" name="txtTipo" required="required" class="form-control col-md-7 col-xs-12" value="U">
           
            <input type="hidden"  id="txtIdpersonal" name="txtIdpersonal" required="required" class="form-control col-md-7 col-xs-12" value="0">
            <div class="form-group">
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtImagen">
              <?php echo ucfirst(JrTexto::_('Image'));
                $ruta=!empty($frm["imagen"])?$frm["imagen"]:$this->documento->getUrlStatic().'/media/imagenes/levels/nofoto.jpg';
                $ruta=str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$ruta);
              ?><span class="required">* :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 text-left" style="max-height:180px; overflow: hidden;">
                 <a class="biblioteca-open btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a image from our multimedia library or upload your own image')) ?>" data-tipo="image" data-url=".imgtemporal" style="position: absolute;top: 0px;"><i class="fa fa-picture-o"></i> <?php echo ucfirst(JrTexto::_('Changed image')); ?></a><img src="<?php echo $ruta; ?> " class="imgtemporal img-responsive" width="50%"  >
                 <input type="hidden" name="imagen" value="<?php echo $ruta; ?>" class="imagentemporalval">
              </div>
            </div>

            <!--div class="form-group">
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtIdpersonal">
              <?php //echo JrTexto::_('Idpersonal');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              </div>
            </div-->

            <div class="form-group">
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 " style="text-align: left;">
              <a href="javascript:;"  class="btn-chkoption" > <i class="fa fa<?php echo !empty($frm["estado"])?"-check":""; ?>-circle-o fa-lg"></i>  <span><?php echo @$frm["estado"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></span>
               <input type="hidden" name="txtEstado" value="<?php echo @$frm["estado"];?>" >
              </a>                                
              </div>
            </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveNiveles" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('unidad'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
<script type="text/javascript">
var cambiarruta=function(){
    $('input.imagentemporalval').val($('img.imgtemporal').attr('src'));
  }
$(document).ready(function(){  
  $('.biblioteca-open').click(function(e) {
    e.preventDefault();
    var txt= '<?php echo JrTexto::_('Select or upload'); ?>';
    selectedfile(e,this,txt,'cambiarruta');
  });
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Niveles', 'saveNiveles', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("unidad"))."?txtNivel="?>'+$('#txtIdpadre').val());
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("unidad"))."?txtNivel="?>'+$('#txtIdpadre').val());
        <?php endif;?> }
     }
  });

$('.btn-chkoption').bind({
    click: function() {     
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $("i",this).removeClass().addClass('fa fa'+(data==1?'-check-':'-')+'circle-o');
      $("span",this).html(data==1?'<?php echo JrTexto::_("Active"); ?>':'<?php echo JrTexto::_("Inactive"); ?>');
      $("input",this).val(data);
    }
  });
$('.btn-close').click(function(e){
  e.preventDefault();
  return redir('<?php echo JrAplicacion::getJrUrl(array("unidad"))."?txtNivel=".@$_GET["idnivel"];?>');
});
});


</script>