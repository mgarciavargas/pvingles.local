<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?><style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
  .form-group{
    margin-bottom: 0px; 
  }
  .input-group {
    margin-top: 0px;
  }
  select.select-ctrl, .form-control , .input-group-addon{   
    border: 1px solid #4683af;
    margin-bottom: 1ex;
  }
  tr:last-child .btnDown{
    display: none;
  }
  tr:last-child .btnUp{
    margin-left: 32px;
  }
  tr:first-child .btnUp{
    display: none;
  }
  .select-ctrl-wrapper:after{
    right: 0px;
  }
  .table{ width: 100%; }
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i> <?php echo JrTexto::_("Home");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("administrador"));?>"><?php echo JrTexto::_("Dashboard");?></a></li>      
        <li class="active"><?php echo JrTexto::_('Unit')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 " >
          <label><?php echo JrTexto::_('Level') ?> :</label> 
            <div class="select-ctrl-wrapper select-azul">             
              <select name="cbpadre" id="cbpadre" class="form-control select-ctrl">
                  <?php foreach ($this->niveles as $nivel) { ?>                  
                  <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $this->idnivel===$nivel["idnivel"]?'selected="selected"':''?>><?php echo $nivel["nombre"]?></option>
                  <?php } ?>                               
              </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3" >
              <label><?php echo JrTexto::_('State') ?> :</label> 
            <div class="select-ctrl-wrapper select-azul"> 
              <select name="cbestado" id="cbestado" class="form-control select-ctrl">
                  <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                  <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                  <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
              </select>
            </div>
            </div>                           
            <div class="col-xs-12 col-sm-6 col-md-4">
              <label><?php echo JrTexto::_('Search text') ?> :</label> 
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-2 text-center">
              <br>
               <a class="btn btnaddunidad btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("unidad", "agregar"));?>?tipo=U&idnivel=<?php echo $this->idnivel;?>" data-titulo="<?php echo JrTexto::_("Unit").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Nombre") ;?></th>
                  <th><?php echo JrTexto::_("Orden") ;?></th> 
                    <th><?php echo JrTexto::_("Imagen") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>                                       
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos59e7902b65f1b='';
function refreshdatos59e7902b65f1b(){
    tabledatos59e7902b65f1b.ajax.reload();
}
$(document).ready(function(){  
  var estados59e7902b65f1b={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>'}
  var tituloedit59e7902b65f1b='<?php echo ucfirst(JrTexto::_("Unit"))." - ".JrTexto::_("edit"); ?>';
  var draw59e7902b65f1b=0;

  $('#cbestado').change(function(ev){
    refreshdatos59e7902b65f1b();
  });
  $('#cbpadre').change(function(ev){
    $('.btnaddunidad').attr('href','<?php echo JrAplicacion::getJrUrl(array("unidad", "agregar"));?>?tipo=U&idnivel='+$(this).val())

    refreshdatos59e7902b65f1b();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos59e7902b65f1b();
  });
  tabledatos59e7902b65f1b=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Nombre") ;?>'},            
        {'data': '<?php echo JrTexto::_("Orden") ;?>'},
        {'data': '<?php echo JrTexto::_("Imagen") ;?>'},
        {'data': '<?php echo JrTexto::_("Estado") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/niveles/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.tipo='U',
             d.idpadre=$('#cbpadre').val(),
             d.estado=$('#cbestado').val(),
             //d.texto=$('#texto').val(),
                        
            draw59e7902b65f1b=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw59e7902b65f1b;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            var imagen = data[i].imagen;
            urlimagen=imagen!=null?imagen.replace('__xRUTABASEx__',_sysUrlBase_):(_sysUrlBase_+'/static/media/imagenes/levels/nofoto.jpg')
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Nombre") ;?>': data[i].nombre,              
              '<?php echo JrTexto::_("Orden") ;?>': '<button class="btnordenar btnDown" data-value="1"  data-id="'+data[i].idnivel+'"><i class="fa fa-arrow-down"></i></button> <button class="btnordenar btnUp" data-value="-1" data-id="'+data[i].idnivel+'"><i class="fa fa-arrow-up"></i></button></div>',
              '<?php echo JrTexto::_("Imagen") ;?>': '<img src="'+urlimagen+'" class="img-thumbnail" style="max-height:70px; max-width:50px;">',
              '<?php echo JrTexto::_("Estado") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="'+data[i].idnivel+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados59e7902b65f1b[data[i].estado]+'</a>',
                
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/unidad/editar/?id='+data[i].idnivel+'&idnivel='+data[i].idpadre+'" data-titulo="'+tituloedit59e7902b65f1b+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idnivel+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'niveles', 'setCampo', id,campo,data);
          if(res) tabledatos59e7902b65f1b.ajax.reload();
        }
      });
  });



  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos59e7902b65f1b';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Niveles';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'niveles', 'eliminar', id);
        if(res) tabledatos59e7902b65f1b.ajax.reload();
      }
    }); 
  }).on('click','.btnordenar',function(ev){
      ev.preventDefault();
      var obj=$(this);
      var id=obj.attr('data-id');    
      var accion=obj.attr('data-value');
      var tr = obj.closest("tr");
      var formData = new FormData();
      formData.append("idNivel",id);
      formData.append("ordenaa",accion);
      $.ajax({
        url: _sysUrlBase_+'/niveles/ordenarjson',
        type: "POST",
        data:  formData,
        contentType: false,
        processData: false,
        dataType:'json',
        cache: false,
        processData:false,     
        beforeSend: function(XMLHttpRequest){},      
        success: function(data)
        {  
          if(data.code==='ok'){
            tabledatos59e7902b65f1b.ajax.reload();
          }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>','Error','warning');
          }
        },
        error: function(e){ 
          console.log(e);
        },
        complete: function(xhr){}
      });
  });

});
</script>