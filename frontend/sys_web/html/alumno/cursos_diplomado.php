<!-- Encabezado -->
<header>
	<div class="container-fluid">
		<div class="navbar-header navbar-static-top"> 
            <button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button"> 
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
            </button> 
            <a href="<?php echo $this->documento->getUrlSitio() ?>" class="navbar-brand" id="logo">
                <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/logo.png" class="hvr-wobble-skew img-responsive hidden" alt="logo">
            </a>                        
        </div>

        <nav class="collapse navbar-collapse" id="bs-navbar"> 
            <ul class="nav navbar-nav navbar-right menutop1">
                <li class="dropdown" id="account"> 
                    <a href="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user-circle"></i>
                    </a>

                    <div class="user-menu dropdown-menu">
                        <div class="account-info">
                            <a  class="user-photo">
                                <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/user_avatar.jpg" alt="user" class="img-responsive">
                            </a>
                            <div class="data-user">
                                <div class="user-name"><?php echo $this->usuarioAct['nombre_full'] ?></div>
                                <div class="user-email"><?php echo $this->usuarioAct['email'] ?></div>
                                <?php 
                                $roles=$this->usuarioAct["roles"]; 
                                $idrol_=$this->usuarioAct["idrol"];
                                $verrol=$this->usuarioAct["rol"];
                                if(count($roles)>1){                                    
                                    $verrol.=' <small class="cambiarrolmenu"><a href="'.$this->documento->getUrlBase().'/sesion/cambiarrol" class="changerol">('.JrTexto::_('Change Rol').')</a></small>';
                                }else{ $verrol=$this->usuarioAct["rol"];}
                                ?>
                                <div class="user-email">
                                 <?php echo $verrol; ?></div>
                                <a class="btn btn-blue capitalize btn-profile" href="<?php echo $this->documento->getUrlBase() ?>/personal/perfil">
                                    <?php echo JrTexto::_('my profile'); ?>
                                </a>
                            </div>
                        </div>
                        <div class="action-buttons">
                            <div>
                                <a href="<?php echo $this->documento->getUrlBase() ?>/personal/cambiarclave/?id=<?php echo $this->usuarioAct["dni"];?>" class="btn btn-default capitalize"><?php echo JrTexto::_('change password'); ?></a>
                            </div>
                            <div>
                                <a href="<?php echo $this->documento->getUrlBase() ?>/sesion/salir" class="btn btn-default capitalize"><?php echo JrTexto::_('log out'); ?></a>
                            </div>
                        </div>
                    </div> 
                </li>
            </ul>
        </nav> 
	</div>
</header> <!-- Fin encabezado -->

<!-- Contenido principal -->
<div class="main">
    <h1 class="text-center bolder titulo">Diplomado en Educaci&oacute;n con menci&oacute;n en TIC's</h1>
    <h3 class="text-center bolder titulo">
        <a href="javascript:history.back();" class="btn-volver"><i class="fa fa-chevron-left"></i> Volver</a>
        <?php echo @$this->categoria["nombre"]; ?>      
    </h3>

    <div class="cursos">
        <div class="row" id="listado-cursos">
            <?php if(!empty($this->cursos)) { #var_dump($this->cursos);
            foreach ($this->cursos as $cur) { ?>
            <div class="col-sm-6 col-md-4"  title="<?php echo @$cur["nombre"]; ?>"> 
                <div class="thumbnail"> 
                    <img src="<?php 
                    if(empty(@$cur["imagen"])) {
                        $imgSrc = $this->documento->getUrlStatic().'/media/cursos/nofoto.jpg';
                    } else {
                        $imgSrc = $this->documento->getUrlBase().$cur["imagen"];
                    }
                    echo @$imgSrc; ?>"> 
                    <div class="caption"> 
                        <h3 class="cur-nombre" title="<?php echo @$cur["nombre"]; ?>"><?php echo @$cur["nombre"]; ?></h3> 
                        <p class="cur-descripcion"><?php echo @$cur["descripcion"]; ?></p> 
                        <p class="text-center">
                            <a href="<?php echo $this->documento->getUrlBase().'/smartcourse/cursos/ver/?idcurso='.@$cur["idcurso"] ?>" class="btn btn-primary" target="_blank">Ver</a>
                        </p> 
                    </div> 
                </div> 
            </div>
            <?php } } ?>
        </div>
    </div>
</div>
<!-- Fin Contenido principal -->

<script type="text/javascript">
var optSlider={
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
    {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
        }
    },
    {
        breakpoint: 600,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    },
    {
        breakpoint: 480,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }
    ]
    
};
$(document).ready(function() {
    var slikitems=$('#listado-cursos').slick(optSlider);
});
</script>