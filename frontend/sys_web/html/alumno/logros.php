<div class="container">
    <div class="row" id="breadcrumb"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
            <?php foreach ($this->breadcrumb as $b) {
            $enlace = '<li>';
            if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
            else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
            $enlace .= '</li>';
            echo $enlace;
            } ?>
        </ol>
    </div> </div>

    <div class="row"> 
        <div class="col-xs-12">
            <div class="panel border-orange" id="pnl-logros">
                <div class="panel-heading bg-orange">
                    <h3 class="panel-titulo">
                        <i class="fa fa-trophy"></i>&nbsp;&nbsp;
                        <?php echo JrTexto::_('Achievements'); ?>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php if(!empty($this->logros)){
                            foreach ($this->logros as $i=>$l) { ?>
                        <div class="col-xs-3 col-sm-3 col-md-2 item-logro border-orange">
                           <a href="#" data-idlogro="<?php echo $l['id_logro']; ?>"><img src="<?php echo !empty($l['imagen'])?$this->documento->getUrlBase().'/'.$l['imagen']:$this->documento->getUrlStatic().'/media/imagenes/trofeo.png'; ?>" class="img-responsive border-orange" alt="trofeo">
                            <div class="nombre text-center"><?php echo $l['titulo']; ?></div></a>
                        </div>
                        <?php } } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="contenido_modal" class="hidden">
    <div class="row">
        <div class="col-xs-12 col-sm-4 imagen">
            <img src="" alt="trofeo" class="img-responsive center-block">
        </div>
        <div class="col-xs-12 col-sm-8 titulo_descripcion">
            <h3 class="titulo"></h3>
            <p class="descripcion"></p>
        </div>
    </div>
</section>

<script>
var crearModal = function(idLogro=0){
    if(idLogro==0){ return false; }
    var $modal = $('#modalclone').clone();
    $modal.attr('id','mdl-'+idLogro);
    $modal.find('.modal-dialog').removeClass('modal-lg');
    $modal.find('.modal-header #modaltitle').html('<?php echo JrTexto::_('Achievement'); ?> ');
    $('body').append($modal);
    $('#mdl-'+idLogro).modal(); //.modal({keyboard:false, backdrop:'static'});
};

$(document).ready(function() {
    $('.item-logro a').click(function(e) {
        e.preventDefault();
        var idLogro = $(this).data('idlogro');
        crearModal(idLogro);
        $.get(_sysUrlBase_+'/logro/buscarjson/?id_logro='+idLogro, function(resp) {
            resp = JSON.parse(resp);
            if(resp.data.length!=0){
                var imgSrc = (resp.data[0].imagen!==null)?_sysUrlBase_+'/'+resp.data[0].imagen:_sysUrlStatic_+'/media/imagenes/trofeo.png';
                var titulo = resp.data[0].titulo;
                var descripcion = resp.data[0].descripcion;
                var $contenido = $('#contenido_modal').clone();
                $contenido.find('.imagen img').attr('src', imgSrc);
                $contenido.find('.titulo').text(titulo);
                $contenido.find('.descripcion').text(descripcion);
                $('#mdl-'+idLogro+" .modal-body").html($contenido.html());
            } else {
                $('#mdl-'.idLogro).modal('hide');
            }
        });
    });

    $('body').on('hidden.bs.modal', '.modal', function(e) { $(this).remove(); });
});
</script>