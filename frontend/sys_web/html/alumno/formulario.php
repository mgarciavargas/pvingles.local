<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
?>
<style>
  #menu-personal-add .btn{ width: 100%;}
  #menu-personal-add .btn.btn-panel.active{ opacity: 1 }
  #menu-personal-add .btn.btn-panel.active:after{
    content: "";
    width: 0px;
    height: 0px;
    border-left: 15px solid transparent;
    border-right: 15px solid transparent;
    border-bottom: 15px solid #fbf7f5;
    font-size: 0px;
    line-height: 0px;
    position: absolute;
    left: 40%;
    bottom: -7px;
  }
  #menu-personal-add .btn.btn-panel{ opacity: 0.75 }
  #menu-personal-add .btn.btn-panel.hover{ opacity: 0.8; background-color: #ffd; }
  .input-file-invisible{     
    position: absolute;
    height: 100%;
    width: 100%;
    cursor: pointer;
    opacity: 0; 
  }

  @media (min-width:768px) and (max-width: 879.99px){
    #menu-personal-add .btn.btn-panel{ font-size: 1em; }
  }
  @media (min-width: 385px) and (max-width: 767.99px){
    #menu-personal-add .btn.btn-panel{ font-size: .8em; }
    #contenido .foto_alumno{ height: 150px; }
  }
  @media (max-width: 384.99px) {
    #menu-personal-add .btn.btn-panel{ font-size: .8em; }
    #contenido .foto_alumno{ height: 150px; }
  }
  .slick-slider{ margin-bottom: 0.5ex; }
  ul.tablis li a{
     background: #f3813d;
    color: #f1f1e2;
    opacity: 0.6;
  }


  ul.tablis>li.active>a, ul.tablis>li.active>a:focus, ul.tablis>li.active>a:hover{
    background: #f3813d;
    color: #f1f1e2;
    opacity: 1;
  }

  ul.tablis li.active a:after{
    content: "";
    width: 0px;
    height: 0px;
    border-left: 15px solid transparent;
    border-right: 15px solid transparent;
    border-bottom: 15px solid #fbf7f5;
    font-size: 0px;
    line-height: 0px;
    position: absolute;
    left: 40%;
    bottom: -7px;
  }

  ul.tablis li a:hover, ul.tablis li a:focus{
    background: #f3813d;
    color: #f1f1e2;
    opacity: 0.85; 
  }
  

  

</style>
<?php if($this->documento->plantilla!='modal'){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/alumno">&nbsp;<?php echo ucfirst(JrTexto::_('Students')); ?></a></li>
        <li>&nbsp;<?php echo JrTexto::_('Edit'); ?></li>
        <?php 
        if(!empty($this->breadcrumb))
        foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>
<?php } ?>
<div class="row" id="menu-personal-add">
  <div class="col-xs-12">
    <div class="slick-items<?php echo $idgui; ?>">
       <div class="slick-item">
        <a href="#tab1_generalinformation" data-toggle="tab" class="btn btn-primary btn-panel active">
            <i class="btn-icon fa fa-user"></i> <span><?php echo JrTexto::_('General information'); ?></span>
          </a>
       </div>
       <div class="slick-item">
          <a href="#tab1_grupoestudio" data-toggle="tab" class="btn btn-primary btn-panel">
            <i class="btn-icon fa fa-users"></i> <span><?php echo JrTexto::_('Study Group'); ?></span>
          </a>
       </div>
       <div class="slick-item">
          <a href="#tab1_cursos" data-toggle="tab" class="btn btn-primary btn-panel">
            <span class="btn-icon glyphicon glyphicon-book"></span> <span><?php echo JrTexto::_('Courses'); ?></span>
          </a>
       </div>
       <div class="slick-item">
          <a href="#tab1_notas" data-toggle="tab" class="btn btn-primary btn-panel">
            <span class="btn-icon glyphicon glyphicon-list-alt"></span> <span><?php echo JrTexto::_('Qualifications'); ?></span>
          </a>
       </div>
       <div class="slick-item">
          <a href="#tab1_horarios" data-toggle="tab" class="btn btn-primary btn-panel">
            <i class="btn-icon fa fa-calendar-check-o"></i> <span><?php echo JrTexto::_('Schedule'); ?></span>
          </a>
       </div>
       <div class="slick-item">
        <a href="#tab1_historial" data-toggle="tab" class="btn btn-primary btn-panel">
          <i class="btn-icon fa fa-hourglass-half"></i> <span><?php echo JrTexto::_('Historial'); ?></span>
        </a>
       </div>
       <div class="slick-item">
          <a href="#tab1_asistencias" data-toggle="tab" class="btn btn-primary btn-panel">
            <i class="btn-icon fa fa-pencil-square-o"></i> <span><?php echo JrTexto::_('Attendance'); ?></span>
          </a>
       </div>
    </div>
  </div>
  <div class="tab-content">
    <div class="tab-pane fade in active" id="tab1_generalinformation">
      <div class="col-xs-12">   
        <div class="slick-generalinformation<?php echo $idgui; ?>">
           <div class="slick-item">
            <a href="#tab2_datospersonales" data-toggle="tab" class="btn btn-warning btn-panel active"><?php echo JrTexto::_('Personal Information'); ?></a>
           </div>
           <div class="slick-item"><a href="#tab2_direccion" data-toggle="tab" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Address'); ?></a></div>
           <div class="slick-item"><a href="#tab2_apoderado" data-toggle="tab" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Proxy Information'); ?></a></div>
           <div class="slick-item"><a hhref="#tab2_educacion" data-toggle="tab" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Qualifications'); ?></a></div>
           <div class="slick-item"><a href="#tab2_experiencia_laboral" data-toggle="tab" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Work experience'); ?></a></div>
           <div class="slick-item"><a href="#tab2_referencias" data-toggle="tab" class="btn btn-warning btn-panel"><?php echo JrTexto::_('References'); ?></a></div>
        </div>
        <div class="tab-content">
          <div class="tab-pane fade in active" id="tab2_datospersonales">
            <form class="form-horizontal" id="frmDatosPersonales">
              <div class="row">
                <div class="col-xs-12 col-sm-2 pull-right"><div class="col-xs-12 text-center">
                    <label for="fileFoto"><?php echo ucfirst(JrTexto::_('Photo')); ?></label>
                    <input type="file" class="input-file-invisible" name="fileFoto" id="fileFoto" accept="image/*">
                    <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/user_avatar.jpg" alt="foto" class="foto_alumno img-responsive center-block thumbnail">
                </div></div>

                <div class="col-xs-12 col-sm-10">
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcTipoDocumento" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('ID card')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select id="opcTipoDocumento" name="opcTipoDocumento" class="form-control"  required>
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtNroDocumento" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('N° ID card')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtNroDocumento" name="txtNroDocumento" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-8">
                    <div class="form-group">
                      <label for="txtNombres" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Names')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-6">
                        <input type="text" id="txtNombres" name="txtNombres" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtApPaterno" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Father Last Name')); ?> <span>*</span></label>

                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtApPaterno" name="txtApPaterno" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtApMaterno" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Mother Last Name')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtApMaterno" name="txtApMaterno" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtTelefono" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Telephone')); ?> <span>*</span></label>

                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtTelefono" name="txtTelefono" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtCelular" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Mobile number')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtCelular" name="txtCelular" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtEmail" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Email')); ?> <span>*</span></label>

                      <div class="col-xs-12 col-sm-8">
                        <input type="email" id="txtEmail" name="txtEmail" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtFechaNacimiento" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Birthday')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtFechaNacimiento" name="txtFechaNacimiento" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcEstadoCivil" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Marital status')); ?> <span>*</span></label>

                      <div class="col-xs-12 col-sm-8">
                        <select type="email" id="opcEstadoCivil" name="opcEstadoCivil" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <option value="Soltero"><?php echo ucfirst(JrTexto::_('Single')); ?></option>
                          <option value="Casado"><?php echo ucfirst(JrTexto::_('Married')); ?></option>
                          <option value="Viudo"><?php echo ucfirst(JrTexto::_('Widower')); ?></option>
                          <option value="Divorciado"><?php echo ucfirst(JrTexto::_('Divorced')); ?></option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcSexo" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Gender')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select type="email" id="opcSexo" name="opcSexo" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <option value="F"><?php echo ucfirst(JrTexto::_('Female')); ?></option>
                          <option value="M"><?php echo ucfirst(JrTexto::_('Male')); ?></option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row"><div class="col-xs-12 text-center">
                <a href="javascript:history.back();" class="btn btn-default btn-lg"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a> &nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary  btn-lg"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button>
              </div></div>
            </form>
          </div>

          <div class="tab-pane fade" id="tab2_direccion">
            <form class="form-horizontal" id="frmDireccion">
              <div class="row"><div class="col-xs-12">
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcPais" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Country')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select id="opcPais" name="opcPais" class="form-control"  required>
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcRegion" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Region')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select id="opcRegion" name="opcRegion" class="form-control"  required>
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcProvincia" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Province')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select id="opcProvincia" name="opcProvincia" class="form-control"  required>
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcDistrito" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('District')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select id="opcDistrito" name="opcDistrito" class="form-control"  required>
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtDireccion" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Address')); ?></label>
                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtDireccion" name="txtDireccion" class="form-control"  autocomplete="off" value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      <label for="txtNumero" class="control-label col-xs-12 col-sm-6"><?php echo ucfirst(JrTexto::_('Number')); ?></label>

                      <div class="col-xs-12 col-sm-5">
                        <input type="text" id="txtNumero" name="txtNumero" class="form-control"  autocomplete="off" value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-2">
                    <div class="form-group">
                      <label for="txtPiso" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Floor')); ?></label>

                      <div class="col-xs-12 col-sm-9">
                        <input type="text" id="txtPiso" name="txtPiso" class="form-control"  autocomplete="off" value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>
              </div></div>
              <div class="row"><div class="col-xs-12 text-center">
                <a href="javascript:history.back();" class="btn btn-default btn-lg"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a> &nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary  btn-lg"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button>
              </div></div>
            </form>
          </div>

          <div class="tab-pane fade" id="tab2_apoderado">
            <form class="form-horizontal" id="frmApoderado">
              <div class="row"><div class="col-xs-12">
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcTipoDocumento" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('ID card')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select id="opcTipoDocumento" name="opcTipoDocumento" class="form-control"  required>
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtNroDocumento" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('N° ID card')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtNroDocumento" name="txtNroDocumento" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-8">
                    <div class="form-group">
                      <label for="txtNombres" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Names')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-6">
                        <input type="text" id="txtNombres" name="txtNombres" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtApPaterno" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Father Last Name')); ?> <span>*</span></label>

                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtApPaterno" name="txtApPaterno" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtApMaterno" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Mother Last Name')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtApMaterno" name="txtApMaterno" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtTelefono" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Telephone')); ?> <span>*</span></label>

                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtTelefono" name="txtTelefono" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtCelular" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Mobile number')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtCelular" name="txtCelular" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtEmail" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Email')); ?> <span>*</span></label>

                      <div class="col-xs-12 col-sm-8">
                        <input type="email" id="txtEmail" name="txtEmail" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcSexo" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Gender')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select type="email" id="opcSexo" name="opcSexo" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <option value="F"><?php echo ucfirst(JrTexto::_('Female')); ?></option>
                          <option value="M"><?php echo ucfirst(JrTexto::_('Male')); ?></option>
                        </select>
                      </div>
                    </div>
                  </div>
              </div></div>
              <div class="row"><div class="col-xs-12 text-center">
                <a href="javascript:history.back();" class="btn btn-default btn-lg"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a> &nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary  btn-lg"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button>
              </div></div>
            </form>
          </div>
          <div class="tab-pane fade" id="tab2_educacion">
            <div class="row"><div class="col-xs-12">
              <button class="btn btn-success agregar"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add'); ?></button>
              <button class="btn btn-default cancelar" style="display: none;"><i class="fa fa-times"></i> <?php echo JrTexto::_('Cancel'); ?></button>
            </div></div>
            <form class="form-horizontal" id="frmEducacion" style="display: none;">
              <div class="row"><div class="col-xs-12">
                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="txtInstitucionEducativa" class="control-label col-xs-12 col-sm-2"><?php echo ucfirst(JrTexto::_('Educational institution')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-10">
                        <input type="text" id="txtInstitucionEducativa" name="txtInstitucionEducativa" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcNivelEstudios" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Study Level')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select type="email" id="opcNivelEstudios" name="opcNivelEstudios" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcSituacion" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Situation')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select type="email" id="opcSituacion" name="opcSituacion" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtDesde" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Since')); ?> <span>*</span></label>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriodoDesdeMes" name="opcPeriodoDesdeMes" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=1; $i <=12 ; $i++) {
                            $nroMes = ($i<10)?'0'.$i:$i;
                            $mes = date('F', strtotime('01-'.$nroMes.'-1999'));
                            echo '<option value="'.$nroMes.'">'.ucfirst(JrTexto::_($mes)).'</option>';
                          } ?>
                        </select>
                      </div>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriodoDesdeAnio" name="opcPeriodoDesdeAnio" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=date('Y'); $i >= date('Y')-80 ; $i--) {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          } ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtHasta" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('To')); ?> <span>*</span></label>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriosoHastaMes" name="opcPeriosoHastaMes" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=1; $i <=12 ; $i++) {
                            $nroMes = ($i<10)?'0'.$i:$i;
                            $mes = date('F', strtotime('01-'.$nroMes.'-1999'));
                            echo '<option value="'.$i.'">'.ucfirst(JrTexto::_($mes)).'</option>';
                          } ?>
                        </select>
                      </div>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriosoHastaAnio" name="opcPeriosoHastaAnio" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=date('Y'); $i >= date('Y')-80 ; $i--) {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          } ?>
                        </select>
                      </div>
                    </div>
                  </div>
              </div></div>
              <div class="row"><div class="col-xs-12 text-center">
                <a href="#" class="btn btn-default btn-lg cancelar"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a> &nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary  btn-lg"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button>
              </div></div>
            </form>
            <hr>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th><?php echo JrTexto::_('Educational institution'); ?></th>
                  <th><?php echo JrTexto::_('Study Level'); ?></th>
                  <th><?php echo JrTexto::_('Situation'); ?></th>
                  <th><?php echo JrTexto::_('Since'); ?></th>
                  <th><?php echo JrTexto::_('To'); ?></th>
                  <th><?php echo JrTexto::_('Options'); ?></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Colegio 01</td>
                  <td>Primaria</td>
                  <td>Terminado</td>
                  <td><?php echo date('M-Y', strtotime('01-03-2000')); ?></td>
                  <td><?php echo date('M-Y', strtotime('01-12-2002')); ?></td>
                  <td>
                    <button class="btn btn-default btn-xs"><i class="color-blue fa fa-pencil"></i></button>
                    <button class="btn btn-default btn-xs"><i class="color-red fa fa-trash"></i></button>
                  </td>
                </tr>
                <tr>
                  <td>Colegio 02</td>
                  <td>Primaria</td>
                  <td>Terminado</td>
                  <td><?php echo date('M-Y', strtotime('01-03-2003')); ?></td>
                  <td><?php echo date('M-Y', strtotime('01-12-2005')); ?></td>
                  <td>
                    <button class="btn btn-default btn-xs"><i class="color-blue fa fa-pencil"></i></button>
                    <button class="btn btn-default btn-xs"><i class="color-red fa fa-trash"></i></button>
                  </td>
                </tr>
                <tr>
                  <td>Colegio 03</td>
                  <td>Secundaria</td>
                  <td>Terminado</td>
                  <td><?php echo date('M-Y', strtotime('01-03-2006')); ?></td>
                  <td><?php echo date('M-Y', strtotime('01-12-2010')); ?></td>
                  <td>
                    <button class="btn btn-default btn-xs"><i class="color-blue fa fa-pencil"></i></button>
                    <button class="btn btn-default btn-xs"><i class="color-red fa fa-trash"></i></button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div class="tab-pane fade" id="tab2_experiencia_laboral">
            <div class="row"><div class="col-xs-12">
              <button class="btn btn-success agregar"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add'); ?></button>
              <button class="btn btn-default cancelar" style="display: none;"><i class="fa fa-times"></i> <?php echo JrTexto::_('Cancel'); ?></button>
            </div></div>
            <form class="form-horizontal" id="frmExperienciaLaboral" style="display: none;">
              <div class="row"><div class="col-xs-12">
                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="txtEmpresa" class="control-label col-xs-12 col-sm-2"><?php echo ucfirst(JrTexto::_('Company')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-10">
                        <input type="text" id="txtEmpresa" name="txtEmpresa" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcSector" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Company sector')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select type="email" id="opcSector" name="opcSector" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcArea" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Area')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select type="email" id="opcArea" name="opcArea" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtCargo" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Position')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtCargo" name="txtCargo" class="form-control"  autocomplete="off" value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtFuncionesRealizadas" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Carried out work')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <textarea id="txtFuncionesRealizadas" name="txtFuncionesRealizadas" class="form-control"  autocomplete="off" value="<?php echo '';?>"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtDesde" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Since')); ?> <span>*</span></label>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriodoDesdeMes" name="opcPeriodoDesdeMes" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=1; $i <=12 ; $i++) {
                            $nroMes = ($i<10)?'0'.$i:$i;
                            $mes = date('F', strtotime('01-'.$nroMes.'-1999'));
                            echo '<option value="'.$nroMes.'">'.ucfirst(JrTexto::_($mes)).'</option>';
                          } ?>
                        </select>
                      </div>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriodoDesdeAnio" name="opcPeriodoDesdeAnio" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=date('Y'); $i >= date('Y')-80 ; $i--) {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          } ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtHasta" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('To')); ?> <span>*</span></label>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriosoHastaMes" name="opcPeriosoHastaMes" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=1; $i <=12 ; $i++) {
                            $nroMes = ($i<10)?'0'.$i:$i;
                            $mes = date('F', strtotime('01-'.$nroMes.'-1999'));
                            echo '<option value="'.$i.'">'.ucfirst(JrTexto::_($mes)).'</option>';
                          } ?>
                        </select>
                      </div>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriosoHastaAnio" name="opcPeriosoHastaAnio" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=date('Y'); $i >= date('Y')-80 ; $i--) {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          } ?>
                        </select>
                      </div>
                    </div>
                  </div>
              </div></div>
              <div class="row"><div class="col-xs-12 text-center">
                <a href="#" class="btn btn-default btn-lg cancelar"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a> &nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary  btn-lg"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button>
              </div></div>
            </form>
            <hr>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th><?php echo JrTexto::_('Company'); ?></th>
                  <th><?php echo JrTexto::_('Company sector'); ?></th>
                  <th><?php echo JrTexto::_('Area'); ?></th>
                  <th><?php echo JrTexto::_('Position'); ?></th>
                  <th><?php echo JrTexto::_('Since'); ?></th>
                  <th><?php echo JrTexto::_('To'); ?></th>
                  <th><?php echo JrTexto::_('Options'); ?></th>
                </tr>
              </thead>
              <tbody>
              <?php for($i=1; $i<=4; $i++) { ?>
                <tr>
                  <td>Empresa <?php echo $i; ?></td>
                  <td>Ventas de PC</td>
                  <td>Ventas</td>
                  <td>Jefe de área</td>
                  <td><?php echo date('M-Y', strtotime('01-04-2010')); ?></td>
                  <td><?php echo date('M-Y', strtotime('01-09-2011')); ?></td>
                  <td>
                    <button class="btn btn-default btn-xs"><i class="color-blue fa fa-pencil"></i></button>
                    <button class="btn btn-default btn-xs"><i class="color-red fa fa-trash"></i></button>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>

          <div class="tab-pane fade" id="tab2_referencias">
            <form class="form-horizontal" id="frmDatosPersonales">
              <div class="row"><div class="col-xs-12">
                <div class="col-xs-12">
                  <div class="form-group">
                    <label for="txtNombreCompleto" class="control-label col-xs-12 col-sm-2"><?php echo ucfirst(JrTexto::_('Full name')); ?> <span>*</span></label>
                    <div class="col-xs-12 col-sm-10">
                      <input type="text" id="txtNombreCompleto" name="txtNombreCompleto" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label for="txtRelacion" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Relationship')); ?> <span>*</span></label>
                    <div class="col-xs-12 col-sm-8">
                      <input type="text" id="txtRelacion" name="txtRelacion" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label for="txtEmpresaaCargo" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Company')).'/'.ucfirst(JrTexto::_('Position')); ?> <span>*</span></label>
                    <div class="col-xs-12 col-sm-8">
                      <input type="text" id="txtEmpresaaCargo" name="txtEmpresaaCargo" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label for="txtTelefono" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Telephone')).'/'.ucfirst(JrTexto::_('Mobile number')); ?> <span>*</span></label>
                    <div class="col-xs-12 col-sm-8">
                      <input type="text" id="txtTelefono" name="txtTelefono" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label for="txtEmail" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Email')); ?> <span>*</span></label>

                    <div class="col-xs-12 col-sm-8">
                      <input type="email" id="txtEmail" name="txtEmail" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                    </div>
                  </div>
                </div>
              </div></div>
              <div class="row"><div class="col-xs-12 text-center">
                <a href="javascript:history.back();" class="btn btn-default btn-lg"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a> &nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary  btn-lg"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button>
              </div></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_grupoestudio">
      <div class="col-xs-12">
        grupo de estudios
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_cursos">
      <div class="col-xs-12">
        cursos
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_notas">
      <div class="col-xs-12">
        notas
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_horarios">
      <div class="col-xs-12">
        horarios
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_historial">
      <div class="col-xs-12">
        historial
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_asistencias">
      <div class="col-xs-12">
        asistencias
      </div>
    </div>
  </div>
<script type="text/javascript">
$(document).ready(function(){  
            
  $('#frm-<?php echo @$id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'personal', 'savePersonal', xajax.getFormValues('frm-<?php echo @$id_vent;?>'));
      /*if(res){
        if(typeof <?php echo @$ventanapadre?> == 'function'){
          <?php echo @$ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Personal"))?>');
      }*/
     }
  });

 
  
  $('.cargarfile').on('change',function(){
    var file=$(this);
     agregar_msj_interno('success', '<?php echo JrTexto::_("loading");?> '+file.attr('data-texto')+'...','msj-interno',true);
     $('#frmPersonal').attr('action',file.attr('data-action'));
     $('#frmPersonal').attr('target','if_cargar_'+file.attr('data-campo'));
     $('#frmPersonal').attr('enctype','multipart/form-data');
     $('#frmPersonal').submit();                
  });

  $('.tab-pane').on('click', '.btn.agregar', function(e) {
    e.preventDefault();
    var idTabPane = $(this).closest('.tab-pane').attr('id');
    $('#'+idTabPane+' .btn.agregar').hide();
    $('#'+idTabPane+' .btn.cancelar').show();
    $('#'+idTabPane+' form').show('fast');
  }).on('click', '.btn.cancelar', function(e) {
    e.preventDefault();
    var idTabPane = $(this).closest('.tab-pane').attr('id');
    $('#'+idTabPane+' .btn.cancelar').hide();
    $('#'+idTabPane+' .btn.agregar').show();
    $('#'+idTabPane+' form').hide('fast');
  });
});

$('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }
});

function result_archivo(result, mensaje,txt) {
  if(result) {
    $("#txt"+txt).val(mensaje);
    agregar_msj_interno2("success", '<?php echo JrTexto::_("File updated successfully");?>',"msj-interno2",true);
    var tipo=$("#id"+txt).attr("data-type");
    if(tipo=="imagen"||tipo=="video"||tipo=="audio")
      $("#id"+txt).removeAttr("style").attr("src", _sysUrlStatic_ + mensaje );
    else
      $("#id"+txt).removeAttr("style").attr("href",  _sysUrlStatic_ + mensaje );
    }else{
      agregar_msj_interno2("warning", mensaje,"msj-interno2",true);
    }
}

$(document).ready(function(){
    var optionslike={
            //dots: true,
            infinite: false,
            //speed: 300,
            //adaptiveHeight: true
            navigation: false,
            slidesToScroll: 1,
            centerPadding: '60px',
          slidesToShow: 5,
          responsive:[
              { breakpoint: 1200, settings: {slidesToShow: 5} },
              { breakpoint: 992, settings: {slidesToShow: 4 } },
              { breakpoint: 880, settings: {slidesToShow: 3 } },
              { breakpoint: 720, settings: {slidesToShow: 2 } },
              { breakpoint: 320, settings: {slidesToShow: 1 /*,arrows: false, centerPadding: '40px',*/} }       
          ]
        };
    setTimeout(function(){
      $('.slick-items<?php echo $idgui; ?>').slick(optionslike);
      $('.slick-generalinformation<?php echo $idgui; ?>').slick(optionslike);
    },400);

    $('#menu-personal-add .btn.btn-panel').click(function(){
      $(this).closest('div').siblings('div').find('a').removeClass('active');
      $(this).addClass('active');
    })

});
  
</script>

