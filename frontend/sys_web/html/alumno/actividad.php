<?php 
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"]; 
$idgui = uniqid();
$actividades=!empty($this->actividades)?$this->actividades:null; 
?>
<style type="text/css">
    .addtext{
        border-bottom: 0px solid rgba(62, 168, 239, 0.93);
        padding: 0.5ex 0;
    }
    #panel-tiempo .form-control{
        display: inline-block;
        max-width: 80px; 
        text-align: center;  
        padding: 0ex;  
        margin: 0px;
        height: 23px;
    }
    #contenedor-paneles .eje-panelinfo{
        margin-left: 0.55ex;
        text-align: center;
        min-width: 80px;
        float: left;
    }
    #contenedor-paneles .eje-panelinfo:first-child{margin-left: 0;}
    #contenedor-paneles .eje-panelinfo .title{
        margin: 0px;
        color: #fff;
        font-size: 
    }
    #contenedor-paneles .eje-panelinfo.active{
        margin: 0px;
        display: block;
    }
    #contenedor-paneles .eje-panelinfo.inactive{
        margin: 0px;
        display: none;
    }  

    #contenedor-paneles #panel-barraprogreso{
        width: 75%;
    }
    
    #contenedor-paneles #panel-barraprogreso .progress{
        background: rgba(199, 195, 189, 0.62);
    }
    @media (min-width: 1250px) and (max-height: 800px){  /* para laptops*/
        .widget-box{
            margin: 0px;
        }
        .widget-main{
            padding: 0px;
        }
        hr{
            margin: 5px;
        }
        .btn-container{
            padding: 5px !important;
        }
    }
</style>
<div id="msj-interno"></div>
<form method="post" id="frm-<?php echo $idgui;?>" class="form-horizontal form-label-left" onsubmit="return false;" data-iduser="<?php echo $usuarioAct["dni"] ?>" data-user="<?php echo $usuarioAct["nombre_full"]  ?>" >
<div class="container" data-rol="<?php echo $rolActivo; ?>"> <div class="page-content">
    <div class="row" id="actividad"> <div class="col-xs-12">
        <div class="row" id="actividad-header">
            <input type="hidden" name="txtNivel" id="txtNivel" value="<?php echo $this->idnivel; ?>" >
            <input type="hidden" name="txtunidad" id="txtunidad" value="<?php echo $this->idunidad; ?>" >
            <input type="hidden" name="txtSesion" id="txtsesion" value="<?php echo $this->idsesion; ?>" >
            <div class="col-md-12">
                <ol class="breadcrumb">
                  <li><a href="<?php echo $this->documento->getUrlBase(); ?>"><?php echo JrTexto::_("Home")?></a></li>
                  <li><a href="<?php echo $this->documento->getUrlBase(); ?>/recursos/listar/"><?php echo JrTexto::_("Activities")?></a></li>
                  <li class="active"><?php echo $this->sesion["nombre"] ?></li>
                </ol>
            </div>
        </div>
        <div class="row" id="actividad-body">
            <div class="col-xs-12 col-sm-9">
                <ul class="nav nav-pills metodologias">
                    <?php 
                    $imet=0;                    
                    if(!empty($actividades))
                    foreach ($actividades as $vmet){ 
                       $met=$vmet["met"];
                        $imet++;?>
                        <li class="<?php echo $imet==1?'active':'';?>">
                        <a href="#met-<?php echo $met["idmetodologia"] ?>" data-toggle="pill"><?php echo JrTexto::_(trim($met["nombre"])); ?></a>
                        </li>
                    <?php } ?>
                </ul>
                <div class="actividad-main-content tab-content">
                    <div id="contenedor-paneles">
                        <div id="panel-barraprogreso" class="text-center eje-panelinfo" style="display: none;">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                    <span>0</span>%
                                </div>
                            </div>
                        </div>
                        <div id="panel-tiempo" class="eje-panelinfo pull-right" style="display: none;">
                            <div class="titulo btn-yellow"><?php echo ucfirst(JrTexto::_('time')); ?></div>
                            <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50" style="display: none;">
                            <div class="info-show">00</div>
                        </div>
                        <div id="panel-intentos-dby" class="eje-panelinfo text-center pull-right" style="display: none;">
                            <div class="titulo btn-blue"><?php echo ucfirst(JrTexto::_('attempt')); ?></div>
                            <span class="actual">1</span>
                            <span class="total"><?php echo $this->intentos; ?></span>
                        </div> 
                    </div>
                    <?php 
                    $imet=0;
                    if(!empty($actividades))
                    foreach ($actividades as $vmet) { $imet++;
                        $met=$vmet["met"];
                        $act=$vmet["act"];
                        $detalle=!empty($act["det"])?$act["det"]:null;
                    ?>
                                
                    <div id="met-<?php echo $met["idmetodologia"] ?>" class="metod tab-pane fade <?php echo $imet==1?'active in':'';?>" data-idmet="<?php echo $met["idmetodologia"] ?>">
                        <div class="row" >                       
                        <?php                         
                            if($met["idmetodologia"]==1){
                               $texto=!empty($detalle[0]["texto"])?$detalle[0]["texto"]:null; ?>
                            <div class="Ejercicio"><br>
                                <?php if(!empty($texto)) {
                                    echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$texto);
                                } ?>                      
                            </div>
                            <?php }else{ 
                                $texto=null;
                                $ntexto=0;
                                if(!empty($detalle)){
                                    $textos=$detalle;
                                    $ntexto=count($textos);
                                }
                                ?>
                            <div class="col-xs-12">
                                <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', $met["nombre"]) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', $met["nombre"]) ?>" data-id-metod="<?php echo $met["idmetodologia"]; ?>">
                                    <?php if(!empty($ntexto))
                                    for ($im=1 ;$im<=$ntexto;$im++){?>
                                         <li class="<?php echo $im==1?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]).'-'.$im; ?>" data-toggle="tab"><?php echo $im; ?></a></li>
                                     <?php } ?>                                    
                                </ul>
                            </div>

                            <div class="col-xs-12">
                                <div class="tab-content" id="<?php echo str_replace(' ', '_', $met["nombre"]) ?>-main-content">
                                   <?php if(!empty($textos)){
                                        $im=0;
                                        foreach ($textos as $te){ $im++; $iddetalle=$te["iddetalle"]; ?>
                                        <div id="tab-<?php echo str_replace(' ', '_', $met["nombre"])."-".$im; ?>" class="tabhijo tab-pane fade <?php echo $im==1?'active':''; ?> in" >
                                            <div class="textosave" data-texto="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im;  ?>" data-iddetalle="<?php echo  @$iddetalle; ?>">
                                            <?php echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$te["texto"]);?>
                                            </div>
                                        </div>
                                       <?php }
                                       } ?>
                                </div>
                            </div>
                          <?php } ?>
                        </div>                      
                    </div>
                    <?php } ?>                   
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="panel panel-info" style="width: 100%; padding: 0.5ex; display:none;">
                    <div class="panel-heading text-center autor"><b><?php echo JrTexto::_('Author'); ?> : </b> <span id="infoautor"></span></div>
                </div>
                <div class="widget-box">
                    <div class="widget-header bg-red">
                        <h4 class="widget-title">
                            <i class="fa fa-paint-brush"></i>
                            <span><?php echo JrTexto::_('Skill'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row habilidades">
                                <div class="col-xs-12 ">
                                <?php 
                                $ihab=0;
                                if(!empty($this->habilidades))
                                foreach ($this->habilidades as $hab) { $ihab++;?>
                                    <div class="col-xs-12 btn-skill vertical-center" data-id-skill="<?php echo $hab['idmetodologia']; ?>" style="cursor: default;"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                <?php } ?>
                                </div>
                            </div>
                            <hr>
                            <!--div class="row" style="margin-bottom: 8px; ">
                                <div class="col-xs-12">
                                    <a href="#" class="btn btn-blue btn-square">
                                        <div class="btn-label"><?php echo JrTexto::_('advance<br>sessions');?></div>
                                        <span class="btn-information">45</span>
                                        <i class="btn-icon fa fa-play-circle-o"></i>
                                    </a>
                                </div>
                                <div class="col-xs-12">
                                    <a href="#" class="btn btn-green btn-square">
                                        <span class="btn-label"><?php echo JrTexto::_('time');?></span>
                                        <span class="btn-information">04:15</span>
                                        <i class="btn-icon glyphicon glyphicon-time"></i>
                                    </a>
                                </div>
                            </div-->
                        </div>
                    </div>
                </div>
                <div class="widget-box" id="info-avance-alumno">                    
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row"  style="margin-bottom: 8px; ">
                                
                                    <div class="col-md-6 btn-container">
                                        <a href="<?php echo $this->documento->getUrlBase()?>/recursos/ver/<?php echo $this->idnivel; ?>/<?php echo $this->idunidad; ?>/<?php echo $this->idsesion; ?>/?idoc=00000000" target="_blank" class="btn btn-yellow  btn-rectangle vertical-center " data-ventana="teacherresources"  style="margin: 0.3ex; min-height: 60px;">
                                            <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('Teaching<br>resources');?></span>
                                            <i class="btn-icon fa fa-male" style="font-size: 1.8em;"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-6 btn-container ">
                                        <a href="#" class="btn btn-blue btn-rectangle slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/diccionario" data-ventana="vocabulary" style="margin: 0.3ex; min-height: 60px;">
                                            <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('Vocabulary');?></span>
                                            <span class="btn-information"></span>
                                            <i class="btn-icon fa fa-file-text-o" style="font-size: 1.8em;"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-6 btn-container">
                                        <a href="#" class="btn btn-green2 btn-rectangle btn-inactive vertical-center" data-ventana="workbook" style="margin: 0.3ex; min-height: 60px;">
                                            <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('Workbook');?></span>
                                            <i class="btn-icon fa fa-pencil-square" style="font-size: 1.8em;"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-6 btn-container">
                                        <a href="#" class="btn btn-info btn-rectangle btnvermodal vertical-center" data-ventana="games" style="margin: 0.3ex; min-height: 60px;">
                                            <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('game');?>s</span>
                                            <i class="btn-icon fa fa-puzzle-piece" style="font-size: 1.8em;"></i>
                                        </a>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
      </div> 
    </div>
  </div> 
</div>
</form>
<div id="btns_progreso_intentos" style="display: none;">
    <div class="row" id="btns_control_alumno">
        <div class="col-sm-12 botones-control">
            <a class="btn btn-inline btn-blue btn-lg try-again"><i class="fa fa-undo"></i> <?php echo ucfirst(JrTexto::_('try again')); ?></a>
            <a class="btn btn-inline btn-success btn-lg save-progreso" ><?php echo ucfirst(JrTexto::_('continue')) ?> <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
</div>
<div id="footernoshow" style="display: none">
    <label style="float: left;margin-top: 0.5em;">
    <input type="checkbox" class="nomostrarmodalventana" name="chkDejarDeMostrar" value="1" id="chkDejarDeMostrar">   
     <?php echo ucfirst(JrTexto::_('Do not show this again')); ?>
    </label>
</div>
<div id="paneles-info" style="display: none;">
    <div id="panel-tiempo" class="panel-info" style="display: none;">
        <span class="titulo-panel"><?php echo ucfirst(JrTexto::_('time')); ?></span>
        <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50">
        <div class="info-show showonpreview" style="display: none;">00</div>
    </div>
    <!--
    <div id="panel-puntaje" class="panel-info hidden" style="display: none;">
        <span class="titulo-panel"><?php // echo ucfirst(JrTexto::_('score')); ?></span>
        <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="puntaje" placeholder="<?php //echo JrTexto::_('e.g.'); ?>: 100">
        <div class="info-show showonpreview" style="display: none;">000</div>
    </div>
    -->
</div>
<audio id="curaudio" dsec="1" style="display: none;"></audio>
<section class="hidden" id="msjes_idioma" style="display: none;">
    <input type="hidden" id="attention" value="<?php echo JrTexto::_('Attention');?>">
    <input type="hidden" id="no_puedes_arrastrar" value='<?php echo JrTexto::_('You can not drag another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="no_puedes_seleccionar" value='<?php echo JrTexto::_('You can not select another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="you_must_finish" value='<?php echo ucfirst(JrTexto::_('you must do the exercise before trying again')); ?>'>
    <input type="hidden" id="bien_hecho" value='<?php echo ucfirst(JrTexto::_('well done!')); ?>'>
    <input type="hidden" id="ejerc_exito" value='<?php echo ucfirst(JrTexto::_('you have complete the exercise succesfully')); ?>.'>
    <input type="hidden" id="ir_sgte_ejerc" value='<?php echo ucfirst(JrTexto::_('continue to the next exercise')); ?>.'>
    <input type="hidden" id="finalizar" value='<?php echo ucfirst(JrTexto::_('you have finished the activity')); ?>.'>
    <input type="hidden" id="puedes_continuar" value='<?php echo ucfirst(JrTexto::_('you can continue next exercise')); ?>.'>
    <input type="hidden" id="tiempo_acabo" value='<?php echo ucfirst(JrTexto::_('time is over')); ?>.'>
    <input type="hidden" id="intentalo_otra_vez" value='<?php echo ucfirst(JrTexto::_('you can try again')); ?>.'>
    <input type="hidden" id="select_activity" value='<?php echo ucfirst(JrTexto::_('Select Activity')); ?>.'>
    <input type="hidden" id="select_upload" value='<?php echo JrTexto::_('Select or upload'); ?>'>
    <input type="hidden" id="click_add_description" value='<?php echo JrTexto::_('Click here to add description') ?>'>
    <input type="hidden" id="click_add_title" value='<?php echo JrTexto::_('Click here to add title') ?>'>
    <input type="hidden" id="remove_tab" value='<?php echo JrTexto::_('remove tab') ?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="no_hay_audio" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback')); ?>'>
    <input type="hidden" id="no_hay_audio_en_escena" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback in scene')); ?>'>
    <input type="hidden" id="no_hay_orden_en_escena" value='<?php echo ucfirst(JrTexto::_('you have not set an order in the dialogues in scene')); ?>'>
    <input type="hidden" id="debes_establecer_orden" value='<?php echo ucfirst(JrTexto::_('you must set an order for the dialog bubbles')); ?>'>
</section>
<script>
$(document).ready(function(e){
    var gbl_INTENTOS = '<?php echo $this->intentos; ?>';
    var gbl_IDGUI = '<?php echo $idgui; ?>';
    var TMPL = [];
    var _curmetod=null; //Metodologia Activa
    var _curejercicio=null; //Ejercicio activo
    var _cureditable=false; //indica si estamos vista de edicion
    var _curdni=null;
    cargarMensajesPHP();
     $('.istooltip').tooltip();

    $('.btnvermodal').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var ventana=$(this).attr('data-ventana');
        var claseid=ventana+'<?php echo $idgui; ?>';
        var titulo=$(this).find('.btn-label').html();
        titulo=titulo.toString().replace('<br>',' ');
        var paramadd='&idnivel='+$('#txtNivel').val()+'&idunidad='+$('#txtunidad').val()+'&idactividad='+$('#txtsesion').val();
        var url='<?php echo JrAplicacion::getJrUrl(array('tools'))?>'+ventana+'/?plt=modal'+paramadd;
        var modaldata={
          titulo:titulo,
          url:url,
          ventanaid:'ventana-'+ventana,
          borrar:false
        }
        var modal=sysmodal(modaldata);
    });

    $('#actividad').on('click','[data-audio]',function(){
        var srcaudio=$(this).attr('data-audio');
        srcaudio=srcaudio.trim();
        if(srcaudio!=undefined||srcaudio!=''){
            srcaudio=_sysUrlBase_+'/static/media/audio/'+srcaudio;
            $('#curaudio').attr('src',srcaudio);
            $('#curaudio').trigger('play');
        }
    });

    $( ".word-space, .word-list" ).sortable({
        connectWith: ".word"
    }).disableSelection();
    
   $('#actividad-body').tplcompletar({editando:false});
    

        
    var registrarHistorialSesion = function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/agregar',
            type: 'POST',
            dataType: 'json',
            data: {'lugar': 'A', 'fechaentrada': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                console.log("success");
                console.log(resp);
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                console.log(resp);
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log("error");
            console.log(xhr.responseText);
        });
        
    };

    registrarHistorialSesion();
    $(window).on('beforeunload', function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/editar',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {'idhistorialsesion': _IDHistorialSesion, 'fechasalida': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                console.log("success");
                console.log(resp);
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log("error");
            console.log(xhr.responseText);
            return false;
        });
    });
});

</script>