<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php $index=0; foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link']) && (count($this->breadcrumb)-1)!=$index ){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        $index++;
        } ?>
    </ol>
</div> </div>


<div class="row" id="actividades_practicar">

    <div class="col-xs-12" id="filtros-niveles">
        <div class="col-xs-3 select-ctrl-wrapper select-azul">
            <select name="opcIdCurso" id="opcIdCurso" class="form-control select-ctrl select-nivel">
                <option value="0">- <?php echo ucfirst(JrTexto::_("Select course")); ?> -</option>
                <?php if(!empty($this->cursos_nivel)){
                foreach ($this->cursos_nivel  as $c) {
                    echo '<option value="'.$c["idcurso"].'">'.$c["nombre"].'</option>';
                }} ?>
            </select>
        </div>
    </div>

    <div class="row" id="filtro-habilidad">
        <div class="col-xs-offset-0 col-sm-offset-8 col-xs-12 col-sm-4 select-ctrl-wrapper select-azul <?php echo $this->idHabilidad!=0?'hidden':'' ?>">
            <select name="opcIdHabilidad" id="opcIdHabilidad" class="form-control select-ctrl">
                <option value="0">- <?php echo ucfirst(JrTexto::_("Select skill")); ?> -</option>
                <?php if(!empty($this->habilidades)){
                foreach ($this->habilidades  as $h) {
                    $isSelected = ($h["idmetodologia"]==@$this->idHabilidad);
                    echo '<option value="'.$h["idmetodologia"].'" '.($isSelected?"selected":"").'>'.$h["nombre"].'</option>';
                }} ?>
            </select>
        </div>
    </div>
    
    <div class="col-xs-12" id="zona-ejercicios">
        
    </div>

    <div class="col-xs-12" id="empty_data">
        <div class="jumbotron">
            <h2><?php echo ucfirst(JrTexto::_("There are no exercises to display")) ?>.</h2>
          <p><?php echo ucfirst(JrTexto::_("Please").', '.JrTexto::_("select a course or a level to show exercises")) ?>.</p>
          <p><a class="btn btn-default" href="<?php echo $this->documento->getUrlBase(); ?>" role="button"><i class="fa fa-arrow-left"></i> <?php echo ucfirst(JrTexto::_("Back home")) ?></a></p>
      </div>
    </div>

</div>

<section class="hidden">
    <div class="col-xs-3 select-ctrl-wrapper select-azul hidden" id="clonar-select-nivel">
        <select name="opcIdCursoDetalle" id="opcIdCursoDetalle" class="form-control select-ctrl select-nivel">
            <option value="-1">- <?php echo ucfirst(JrTexto::_("Select")); ?> -</option>
        </select>
    </div>

    <div id="clonar-zona-ejercicios" data-id="panel-ejercicios-practicar">
        <div class="col-xs-12">
            <ul class="nav nav-tabs ejercicios" id="" data-metod="" data-id-metod=""></ul>
        </div>

        <div class="metod active" id="met-2" data-idmet="2">
            <div class="col-xs-12 tab-content"></div>
        </div>
    </div>
</section>

<script type="text/javascript">
var cargarEjercicios = false;

var getCursoDetalle = function( dataSend , $select ) {
    $.ajax({
        async: false,
        url: _sysUrlBase_+'/acad_cursodetalle/buscarjson',
        type: 'GET',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp) {
        if(resp.code=='ok') {
            cargarEjercicios= false;
            var options = '';
            if(resp.data.length) {
                $.each(resp.data, function(i, det) {
                    if(det.tiporecurso!="E") {
                        options += '<option value="'+det.idcursodetalle+'">'+det.nombre+'</option>';
                    }
                });
                $select.append(options);
                $select.closest('.select-ctrl-wrapper').removeClass('hidden');
            }else{
                cargarEjercicios = true;
                $select.closest('.select-ctrl-wrapper').remove();
            }
        } else {
            mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
            $select.closest('.select-ctrl-wrapper').remove();
        }
    }).fail(function(err) {
        $select.closest('.select-ctrl-wrapper').remove();
        mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
    }).always(function() {  });
};

var getEjercicios = function( dataSend , $pnlEjercicios ) {
    //var $pnlEjercicios = $('#panel-ejercicios-practicar');
    $.ajax({
        url: _sysUrlBase_+'/actividades/jxEjerciciosXhabilidad',
        type: 'GET',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp) {
        if(resp.code=='ok' && typeof(resp.data[2].act.det)!="undefined") {
            var arrEjercicios = resp.data[2].act.det;
            var metod = resp.data[2].met;
            var li_tabs = '';
            var tab_panes = '';
            $.each(arrEjercicios, function(i, e) {
                let isActive = (i==0)?'active':'';
                li_tabs += '<li class="'+isActive+'"><a href="#ejerc_'+e.iddetalle+'" data-toggle="tab">'+(i+1)+'</a></li>';

                let html = e.texto_edit.replace(/__xRUTABASEx__/gi, _sysUrlBase_);
                tab_panes += '<div class="tab-pane tabhijo '+isActive+'" id="ejerc_'+e.iddetalle+'">'+html+'</div>';
            });
            $pnlEjercicios.find('.nav.ejercicios').attr({
                'id': metod.nombre+'-tabs',
                'data-metod': metod.nombre,
                'data-id-metod': metod.idmetodologia
            });
            $pnlEjercicios.find('.nav.ejercicios').html(li_tabs);
            $pnlEjercicios.find('.tab-content').html(tab_panes);
            $pnlEjercicios.removeClass('hidden');
            initTmplCompletar($pnlEjercicios);
            $('#empty_data').hide();
        }else {
            console.log('data vacia');
            $('#empty_data').show();
        }
    }).fail(function(err) {
        console.log("error");
    }).always(function() { });
};

var borrarSobrantes = function($select) {
    var $listSelect = $('#filtros-niveles select.select-nivel');
    var indexOfSelect = $listSelect.index($select);
    var cantSelect = $listSelect.length;
    x=0;
    while($('#filtros-niveles select.select-nivel').eq(indexOfSelect+1).length>=1){
        $('#filtros-niveles select.select-nivel').eq(indexOfSelect+1).closest('.select-ctrl-wrapper').remove();
        if(x>=100){ console.log('algo salio mal'); break; }
        x++;
    }
};

var limpiarVisorEjercicios = function() {
    var $panel = $('#zona-ejercicios');
    $panel.html('');
};

var nuevoSelectFiltro = function( idPadre ) {
    var $divSelect_new = $('#clonar-select-nivel').clone();
    var idSelect = $divSelect_new.find('select.select-nivel').attr('id');
    var nuevoIdSelect = idSelect+'_'+idPadre;
    $divSelect_new.removeAttr('id');
    $divSelect_new.find('select.select-nivel').attr({
        'id': nuevoIdSelect,
        'name': nuevoIdSelect
    });
    $('#filtros-niveles').append($divSelect_new);
    return $('#filtros-niveles #'+nuevoIdSelect);
};

var initTmplCompletar = function($zonaEjercicios) {
    //if( $zonaEjercicios.find('.tab-content .tabhijo').length ){
        $zonaEjercicios.tplcompletar({editando:false});
        rinittemplatealshow();
        //$zonaEjercicios.find('.nopreview').hide();
    //}
};

var nuevoZonaEjercicios = function() {
    var $zona_new = $('section #clonar-zona-ejercicios').clone();
    var now = Date.now();
    var idZona = $zona_new.attr('data-id') +'_'+ now;
    $zona_new.attr('id', idZona);
    $('#zona-ejercicios').html($zona_new);
    return $('#zona-ejercicios #'+idZona);
};

$(document).ready(function() {
    $("#opcIdHabilidad").change(function(e) {
        let value = $(this).val();
        if(value<=0) return false;

        var url = window.location.href;
        if (url.indexOf('?') > -1){ url += '&';
        }else{ url += '/?'; }
        url += 'idhab='+value;

        window.location.href = url;
    });

    $('#filtros-niveles').on('change', '.select-nivel', function(e) {
        var idPadre = 0;
        var idCurso = $('#opcIdCurso').val() || 0;
        if( $(this).attr('id') !== "opcIdCurso" ) {
            idPadre = $(this).val();
        }
        borrarSobrantes($(this));
        limpiarVisorEjercicios();
        $('#empty_data').show();
        if(idCurso==0){ return false; }

        $select_new = nuevoSelectFiltro(idPadre);
        getCursoDetalle({
            'idcurso': idCurso,
            'idpadre': idPadre,
        }, $select_new);

        if(cargarEjercicios) {
            var idHabilidad = $('#opcIdHabilidad').val() || 0;
            $zona = nuevoZonaEjercicios();
            getEjercicios({
                'idcurso': idCurso,
                'iddetalle': idPadre,
                'idhab' : idHabilidad
            } , $zona);
        }
    });

});
</script>