<style>

    td,th{
        border: 1px solid;
    }
</style>
<div class="col-xs-12 col-sm-10 col-md-11">
    <div id="msj-sistema"></div>
    <div class="row" id="breadcrumb">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
                <?php foreach ($this->breadcrumb as $b) {
                $enlace = '<li>';
                if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
                else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
                $enlace .= '</li>';
                echo $enlace;
                } ?>
            </ol>
        </div>
    </div>
    <div class="row" id="curriculum"> 
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <ul class="nav nav-tabs" style="">
                        <li class="active"><a data-toggle="tab" href="#description"><?php echo JrTexto::_("General Description"); ?></a></li>
                        <li><a data-toggle="tab" href="#organization"><?php echo JrTexto::_("Organization of Units"); ?></a></li>
                        <li><a data-toggle="tab" href="#learning"><?php echo JrTexto::_("Link with other learning"); ?></a></li>
                        <li><a data-toggle="tab" href="#materials"><?php echo JrTexto::_("Materials and Resources"); ?></a></li>
                    </ul>
                    
                    <div class="row">
                        <div class="tab-content">
                        <div id="description" class="tab-pane fade in active col-xs-12 col-sm-12">
                            <div class="bg-turquoise col-xs-12 nombre border-bottom border-turquoise-dark"><h3><?php echo JrTexto::_("General Description"); ?></h3></div>
                        <div class="col-xs-12">
                            <textarea class="col-xs-12 border-bottom border-black-dark bg-white" name="" id="" style="width: 100%;margin-top: 10px" disabled> 
                                <?php echo JrTexto::_("Descripción de curso"); ?>
                            </textarea>                            
                        </div>
                        </div>
                        <div id="organization" class="tab-pane fade col-xs-12 col-sm-12">
                             <div class="bg-turquoise col-xs-12 nombre border-bottom border-turquoise-dark"><h3><?php echo JrTexto::_("Organization of Units"); ?></h3></div>
                             <div class="col-xs-12 col-md-12 col-sm-12 organizacion" style="margin-top:10px;">
                                <table class="col-md-12" style="margin-bottom: 10px;margin-top: 10px;"> 
                                     <tr>
                                         <td rowspan="10" class="col-md-4 border-black-dark">HOLA</td>
                                         <td rowspan="10" class="col-md-2 border-black-dark">HOLA2</td>
                                         <td colspan="6" class="col-md-6 border-black-dark">HOLA3</td>
                                     </tr>
                                     <tr>
                                         <td colspan="6" class="col-md-6 border-black-dark">HOLA4</td>
                                     </tr>
                                     <tr>
                                         <td rowspan=8 class="col-md-1 border-black-dark vertical-text">HOLA5.1</td>
                                         <td rowspan=8 class="col-md-1 border-black-dark vertical-text">HOLA5.2</td>
                                         <td rowspan=8 class="col-md-1 border-black-dark vertical-text">HOLA5.3</td>
                                         <td rowspan=8 class="col-md-1 border-black-dark vertical-text">HOLA5.4</td>
                                         <td rowspan=8 class="col-md-1 border-black-dark vertical-text">HOLA5.5</td>
                                         <td rowspan=8 class="col-md-1 border-black-dark vertical-text">HOLA5.6</td>
                                     </tr>
                                     <tr style="visibility: hidden;"><td>a</td></tr>
                                     <tr style="visibility: hidden;"><td>a</td></tr>
                                     <tr style="visibility: hidden;"><td>a</td></tr>
                                     <tr style="visibility: hidden;"><td>a</td></tr>
                                     <tr style="visibility: hidden;"><td>a</td></tr>
                                     <tr style="visibility: hidden;"><td>a</td></tr>
                                     <tr style="visibility: hidden;"><td>a</td></tr>
                                     <tr > 
                                        <td class="col-md-4 border-black-dark">HOLA</td>
                                        <td class="col-md-2 border-black-dark">HOLA2</td>
                                        <td colspan="6" class="col-md-6 border-black-dark">HOLA3</td>
                                     </tr>
                                 </table>
                             </div>                            
                        </div>
                        <div id="learning" class="tab-pane fade col-xs-12 col-sm-12">
                             <div class="bg-turquoise col-xs-12 nombre border-bottom border-turquoise-dark"><h3><?php echo JrTexto::_("Link with other learning"); ?></h3></div>   
                             <div class="col-xs-12">
                            <textarea class="col-xs-12 border-bottom border-black-dark bg-white" name="" id="" style="width: 100%;margin-top: 10px" disabled> 
                                <?php echo JrTexto::_("Descripción de vínculo con otros aprendizajes"); ?>
                            </textarea>
                            
                        </div>                         
                        </div>
                        <div id="materials" class="tab-pane fade col-xs-12 col-sm-12">
                            <div class="bg-turquoise col-xs-12 nombre border-bottom border-turquoise-dark"><h3><?php echo JrTexto::_("Materials and Resourses"); ?></h3></div>
                            <div class="btn-group btn-group-sm" role="group" aria-label="Basic example" style="margin-top: 10px">
                                <button type="button" class="btn btn-grey opciones" id="materiales"><?php echo JrTexto::_("Materials"); ?></button>
                                <button type="button" class="btn btn-grey-dark opciones" id="recursos"><?php echo JrTexto::_("Resources"); ?></button>
                            </div>   
                            <div class="contenedor-opciones">
                                
                                <div class="col-md-12 pnl-opciones" data-opcion="materiales" style="margin-top: 10px; display:none;">
                                    <table style="width: 100%">
                                        <tr class="border-black-dark">
                                            <th class="col-md-4 border-black-dark">Nombre</th>
                                            <th class="col-md-4 border-black-dark">Usuario</th>
                                            <th class="col-md-4 border-black-dark">Rol</th>
                                        </tr>
                                        <tr>
                                            <td class="col-md-4 border-black-dark">Tablet</td>
                                            <td class="col-md-4 border-black-dark">Juan Pérez</td>
                                            <td class="col-md-4 border-black-dark">Alumno</td>
                                        </tr>
                                    </table>
                                </div>   
                                <div class="col-md-12 pnl-opciones" data-opcion="recursos" style="margin-top: 10px; display:none;">
                                    <table style="width: 100%">
                                        <tr>
                                            <th class="col-md-4 border-black-dark">Nombre1</th>
                                            <th class="col-md-4 border-black-dark">Usuario1</th>
                                            <th class="col-md-4 border-black-dark">Rol1</th>
                                        </tr>
                                        <tr>
                                            <td class="col-md-4 border-black-dark">Tablet</td>
                                            <td class="col-md-4 border-black-dark">Juan Pérez</td>
                                            <td class="col-md-4 border-black-dark">Alumno</td>
                                        </tr>
                                    </table>
                                </div>                         
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
             </div>
        </div> 
    </div>
</div>
<script>
    $('.opciones').on( "click", function(){
        var id=$(this).attr('id');
        $('.contenedor-opciones').find('.pnl-opciones').hide();
        $('.contenedor-opciones').find('[data-opcion="'+id+'"]').show();
    });
</script>