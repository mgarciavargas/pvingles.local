<div class="" id="inicio"> 
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="panel border-red" id="pnl-cursos">
                <div class="panel-heading bg-red">
                    <h4 class="panel-titulo">
                        <i class="fa fa-bookmark"></i>&nbsp;&nbsp;
                        <?php echo JrTexto::_('Levels'); ?>
                    </h4>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                </div>
                <div class="panel-body">
                    <?php if(!empty($this->cursosMatric)){ ?>
                    <div class="row slider-cursos" style="margin-bottom: 0;">
                        <?php foreach ($this->cursosMatric as $c) { ?>
                        <a href="<?php echo $this->documento->getUrlBase().'/curso/?id='.$c['idcurso']; ?>" class="col-xs-6 col-sm-3 item-curso" title="<?php echo $c['nombre'] ?>">
                            <img src="<?php echo $this->documento->getUrlBase().$c['imagen']; ?>" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center"><?php echo $c['nombre'] ?></div>
                        </a>
                        <?php } ?>
                    </div>
                    <?php }else{ ?>
                    <h3 class="text-center bolder color-grey-dark" style="height: 144px; width: 100%"><?php echo JrTexto::_('You do not have assigned courses'); ?></h3>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="botones-menu">
        <div class="col-xs-12 col-md-6 padding-0">
            <div class="col-xs-12 col-md-6 btn-panel-container panelcont-xs">
                <a href="#write" class="btn btn-block btn-green btn-inicio btn-panel">
                    <i class="btn-icon fa fa-pencil-square-o"></i>
                    <?php echo JrTexto::_('Writing Practice Lab'); ?>
                    <span class="color-blue-dark border-blue-dark badge hidden">+9</span>
                </a>
            </div>
            <div class="col-xs-12 col-md-6 btn-panel-container panelcont-xs">
                <a href="#read" class="btn btn-block btn-blue btn-inicio btn-panel">
                    <i class="btn-icon fa fa-book"></i>
                    <?php echo JrTexto::_('Reading Practice Lab'); ?>
                    <span class="color-blue-dark border-blue-dark badge hidden">+9</span>
                </a>
            </div>
            <div class="col-xs-12 col-md-6 btn-panel-container panelcont-xs">
                <a href="#listen" class="btn btn-block btn-orange btn-inicio btn-panel">
                    <i class="btn-icon fa fa-headphones"></i>
                    <?php echo JrTexto::_('Listening Practice Lab'); ?>
                    <span class="color-blue-dark border-blue-dark badge hidden">+9</span>
                </a>
            </div>
            <div class="col-xs-12 col-md-6 btn-panel-container panelcont-xs">
                <a href="#speak" class="btn btn-block btn-info btn-inicio btn-panel">
                    <i class="btn-icon fa fa-bullhorn"></i>
                    <?php echo JrTexto::_('Speaking Practice Lab'); ?>
                    <span class="color-green-dark border-green-dark badge hidden">5</span>
                </a>
            </div>
        </div>

        <div class="col-xs-12 col-md-3 padding-0">
            <div class="col-xs-12 col-md-12 col-lg-12 btn-panel-container panelcont-xs">
                <a href="#test" class="btn btn-block btn-red btn-inicio btn-panel">
                    <i class="btn-icon fa fa-list"></i>
                    <?php echo JrTexto::_('International Practice Tests'); ?>
                    <span class="color-blue-dark border-blue-dark badge hidden">+9</span>
                </a>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12 btn-panel-container panelcont-xs">
                <a href="#verbs" class="btn btn-block btn-purple btn-inicio btn-panel">
                    <i class="btn-icon fa fa-font"></i>
                    <?php echo JrTexto::_('Verbs Practice Lab'); ?>
                    <span class="color-blue-dark border-blue-dark badge hidden">+9</span>
                </a>
            </div>
        </div>

        <div class="col-xs-12 col-md-3 padding-0">
            <div class="col-xs-12 col-md-12 btn-panel-container panelcont-xs portfolio">
                <a href="#portfolio" class="btn btn-block btn-yellow btn-inicio btn-panel">
                    <i class="btn-icon fa fa-folder-o"></i>
                    <?php echo JrTexto::_('Student Portfolio'); ?>
                    <span class="color-blue-dark border-blue-dark badge hidden">+9</span>
                </a>
            </div>
        </div>

        
        
    </div>

    <div class="row"> <div class="col-xs-12">
        <div class="panel" id="pnl-bottom">
            <div class="panel-body bg-white-dark">
                <div class="col-xs-12 col-sm-2 col-md-3">
                    <a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=21" class="btn btn-yellow-dark btn-block"><?php echo JrTexto::_('Dictionary'); ?></a>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-3 ">
                    <a href="#" class="btn btn-primary btn-block slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/pronunciacion" ><?php echo ucfirst(JrTexto::_('Pronunciation')); ?></a>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-3 hidden">
                    <a href="#" class="btn btn-primary btn-block"><?php echo JrTexto::_('Offline Dictionary'); ?></a>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-3">
                    <a href="<?php echo $this->documento->getUrlBase();?>/modbiblioteca" class="btn btn-red btn-block"><?php echo JrTexto::_('Library'); ?></a>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-3">
                    <a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=22" class="btn btn-green btn-block"><?php echo JrTexto::_('Workbooks'); ?></a>
                </div>
            </div>
        </div>
    </div> </div>
</div>


<section class="hidden" id="submenu_botones">
    <div class="col-xs-12 btn_submenu bg-info" id="speak">
        <h4>
            <i class="btn-icon fa fa-bullhorn"></i> <?php echo JrTexto::_('Speaking Practice Lab'); ?>
            <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
        </h4>
        <ul class="list-unstyled">
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=8"><?php echo JrTexto::_('Pronunciation Exercises'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=7"><?php echo JrTexto::_('Games of Pronunciation'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=17"><?php echo JrTexto::_('Pronunciation videos'); ?></a></li>
        </ul>
    </div>

    <div class="col-xs-12 btn_submenu bg-blue" id="read">
        <h4>
            <i class="btn-icon fa fa-book"></i> <?php echo JrTexto::_('Reading Practice Lab'); ?>
            <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
        </h4>
        <ul class="list-unstyled">
            <li><a href="<?php echo $this->documento->getUrlBase(); /*.'/vocabulario'*/ ?>/tema_libre/tema/?idtipo=1"><?php echo JrTexto::_('Vocabulary'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase(); /*.'/vocabulario'*/ ?>/tema_libre/tema/?idtipo=16"><?php echo JrTexto::_('Vocabulary Games'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase(); /*.'/actividades/practicar/?idhab=5'*/ ?>/tema_libre/tema/?idtipo=4"><?php echo JrTexto::_('Reading exercises'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase(); /*.'/tarea/practicar/?idhab=5'*/?>/tema_libre/tema/?idtipo=5"><?php echo JrTexto::_('Reading tasks'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase(); /*.'/tarea/practicar/?idhab=5'*/?>/tema_libre/tema/?idtipo=23"><?php echo JrTexto::_('Reading E-News'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/modbiblioteca/#/libros"><?php echo JrTexto::_('e-Books'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/modbiblioteca/#/biblioteca"><?php echo JrTexto::_('Digital libraries'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/modbiblioteca/#/museo"><?php echo JrTexto::_('Museum visits'); ?></a></li>
            <!--li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=2"><?php echo JrTexto::_('Exercises with verbs'); ?></a></li-->
        </ul>
    </div>

    <div class="col-xs-12 btn_submenu bg-red" id="test">
        <h4>
            <i class="btn-icon fa fa-list"></i> <?php echo JrTexto::_('International Practice Tests'); ?>
            <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
        </h4>
        <ul class="list-unstyled">
            <li><a href="#">Cambridge Certification</a></li>
            <li><a href="#">TOEFL</a></li>
            <li><a href="#">PET</a></li>
        </ul>
    </div>

    <div class="col-xs-12 btn_submenu bg-orange" id="listen">
        <h4>
            <i class="btn-icon fa fa-headphones"></i> <?php echo JrTexto::_('Listening Practice Lab'); ?>
            <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
        </h4>
        <ul class="list-unstyled">
            <li><a href="<?php echo $this->documento->getUrlBase();?>/modbiblioteca/#/audio"><?php echo JrTexto::_('Audiobooks'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/modbiblioteca/#/video"><?php echo JrTexto::_('Educational videos'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=13"><?php echo JrTexto::_('Listening exercises'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=12"><?php echo JrTexto::_('Listening tasks'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/modbiblioteca/#/musica"><?php echo JrTexto::_('Songs'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=18"><?php echo JrTexto::_('E! News'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=14"><?php echo JrTexto::_('Podcasts'); ?></a></li>
        </ul>
    </div>

    <div class="col-xs-12 btn_submenu bg-green" id="write">
        <h4>
            <i class="btn-icon fa fa-pencil-square-o"></i> <?php echo JrTexto::_('Writing Practice Lab'); ?>
            <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
        </h4>
        <ul class="list-unstyled">
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=9"><?php echo JrTexto::_('Grammar exercises'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=11"><?php echo JrTexto::_('Writing exercises'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=15"><?php echo JrTexto::_('Dictation Exercises'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/modbiblioteca/#/notas"><?php echo JrTexto::_('Research work'); ?></a></li>
        </ul>
    </div>

    <div class="col-xs-12 btn_submenu bg-yellow" id="portfolio">
        <h4>
            <i class="btn-icon fa fa-folder-o"></i> <?php echo JrTexto::_('Student portfolio'); ?>
            <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
        </h4>
        <ul class="list-unstyled">
            <li><i class="fa fa-tachometer"></i> <a href="<?php echo $this->documento->getUrlBase().'/docente/panelcontrol'; ?>"><?php echo JrTexto::_('Smartracking'); ?></a></li>
            <li><i class="fa fa-briefcase"></i> <a href="<?php echo $this->documento->getUrlBase();?>/curso/tarea"><?php echo JrTexto::_('Smartask'); ?></a></li>
            <li><i class="fa fa-calendar-check-o"></i> <a href="<?php echo $this->documento->getUrlBase();?>/agenda"><?php echo JrTexto::_('Agenda'); ?></a></li>
            <li><i class="fa fa-table"></i> <a href="<?php echo $this->documento->getUrlBase();?>/calificaciones"><?php echo JrTexto::_('Scores'); ?></a></li>
            <!--li><a href="#"><?php echo JrTexto::_('Tracking by unit'); ?></a></li>
            <li><a href="#"><?php echo JrTexto::_('Tracking by skill'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/agenda/horario"><?php echo JrTexto::_('Schedules'); ?></a></li>
            <li><a href="#"><?php echo JrTexto::_('Scores'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/logro"><?php echo JrTexto::_('Achievement'); ?></a></li-->
        </ul>
    </div> 


    <div class="col-xs-12 btn_submenu bg-purple" id="verbs">
        <h4>
            <i class="btn-icon fa fa-font   "></i> <?php echo JrTexto::_('Verbs Practice Lab'); ?>
            <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
        </h4>
        <ul class="list-unstyled">
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=2"><?php echo JrTexto::_('Verb'); ?></a></li>
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tema_libre/tema/?idtipo=19"><?php echo JrTexto::_('Phrasal verbs'); ?></a></li>
        </ul>
    </div>
</section>