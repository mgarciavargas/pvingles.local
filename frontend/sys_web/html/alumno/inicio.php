<div class="" id="inicio">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="panel border-red" id="pnl-cursos">
                <div class="panel-heading bg-red">
                    <h3 class="panel-titulo">
                        <i class="fa fa-bookmark"></i>&nbsp;&nbsp;
                        <?php echo JrTexto::_('Courses'); ?>
                    </h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                </div>
                <div class="panel-body">
                    <?php if(!empty($this->cursosMatric)){ ?>
                    <div class="row slider-cursos" style="margin-bottom: 0;">
                        <?php foreach ($this->cursosMatric as $c) { ?>
                        <a href="<?php echo $this->documento->getUrlBase().'/curso/?id='.$c['idcurso']; ?>" class="col-xs-6 col-sm-3 item-curso" title="<?php echo $c['nombre'] ?>">
                            <img src="<?php echo $this->documento->getUrlBase().$c['imagen']; ?>" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center"><?php echo $c['nombre'] ?></div>
                        </a>
                        <?php } ?>
                    </div>
                    <?php }else{ ?>
                    <h3 class="text-center bolder color-grey-dark" style="height: 144px; width: 100%"><?php echo JrTexto::_('You do not have assigned courses'); ?></h3>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-3" style="padding: 0;">
            <div class="col-md-12 col-xs-6  btn-panel-container panelcont-xs">
                <a href="#" class="btn btn-block btn-green btn-panel">
                    <i class="btn-icon fa fa-list-ol"></i>
                    <?php echo JrTexto::_('Smartquality'); ?>
                    <span class="color-green-dark border-green-dark badge hidden">5</span>
                </a>
            </div>
            <div class="col-md-12 col-xs-6  btn-panel-container panelcont-xs">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/modbiblioteca" class="btn btn-block btn-blue btn-panel">
                    <i class="btn-icon fa fa-book"></i>
                    <?php echo JrTexto::_('Smartdata'); ?>
                    <span class="color-blue-dark border-blue-dark badge hidden">+9</span>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="panel border-orange" id="pnl-logros">
                <div class="panel-heading bg-orange">
                    <h3 class="panel-titulo">
                        <i class="fa fa-trophy"></i>&nbsp;&nbsp;
                        <?php echo JrTexto::_('Achievements'); ?>
                    </h3>
                    <a href="<?php echo $this->documento->getUrlBase(); ?>/logro" class="ver-todo" title="<?php echo JrTexto::_('See all'); ?>"><i class="glyphicon glyphicon-chevron-right"></i></a>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php if(!empty($this->logros)){
                            foreach ($this->logros as $i=>$l) { ?>
                        <div class="col-xs-6 col-sm-6 col-md-6 item-logro">
                            <img src="<?php echo !empty($l['imagen'])?$this->documento->getUrlBase().'/'.$l['imagen']:$this->documento->getUrlStatic().'/media/imagenes/trofeo.png'; ?>" class="img-responsive" alt="trofeo">
                            <div class="nombre text-center"><?php echo $l['titulo']; ?></div>
                        </div>
                        <?php } } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5">
            <div class="panel border-brown-dark" id="pnl-logros">
                <div class="panel-heading bg-brown-dark">
                    <h3 class="panel-titulo">
                        <i class="fa fa-calendar"></i>&nbsp;&nbsp;
                        <?php echo JrTexto::_('Agenda'); ?>
                    </h3>
                    <a href="<?php echo $this->documento->getUrlBase(); ?>/agenda" class="ver-todo" title="<?php echo JrTexto::_('See all'); ?>"><i class="glyphicon glyphicon-chevron-right"></i></a>
                </div>
                <div class="panel-body" style="overflow: hidden;">
                    <input type="hidden" name="hVistaListado" id="hVistaListado" value="<?php echo (@$this->agenda_listado)?'true':'false'; ?>">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
</div>