<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
//var_dump($frm);
?>
<style type="text/css">
 .tableinfo{
  width: 100%;
 }
 .tableinfo tr th ,.tableinfo tr td{
    padding: 0.5ex 2ex;
  }
 .tableinfo .infosep{
  max-width: 2px;
  padding: 0px;
 }
</style>
<div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <?php if($this->documento->plantilla!='modal'){?><div class="panel-heading bg-blue">
        <h3><?php echo JrTexto::_('Student'); ?><small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></h3>
        <div class="clearfix"></div>
      </div><?php } ?>      
      <div class="panel-body">
        <div id="msj-interno"></div>
        <div class="pull-right">
          <a href="#" class="btn btn-xs btn-primary"><i class="fa fa-print"></i> <?php echo JrTexto::_("Print");?></a>
          <a href="#" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> <?php echo JrTexto::_("edit");?></a>
          <?php if($this->documento->plantilla=='modal'){?><a href="#" class="btn btn-xs btn-default cerrarmodal" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo JrTexto::_("close");?></a><?php } ?>
        </div>
        <div class="infopersona"> 
          <h4><strong>I. <?php echo JrTexto::_("General information");?></strong></h4>
          <table class="tableinfo">
            <tr><th><?php echo ucfirst(JrTexto::_('Full name'));?> </th><th class="infosep">:</th>
                <td><?php echo $frm["ape_paterno"]." ".$frm["ape_materno"].", ".$frm["nombre"]; ?></td><td rowspan="6" class="text-center">
            <?php 
              $ruta=$this->documento->getUrlStatic()."/media/usuarios/".$frm["foto"];
              if(!is_file($ruta)) $ruta=$this->documento->getUrlStatic()."/media/usuarios/user_avatar.jpg";
            ?>
            <img class="img-circle img-responsive" src="<?php echo $ruta; ?>" style="max-width: 150px; display: inline-block;">
            </td></tr>
            <tr><th><?php echo ucfirst(JrTexto::_('Dni'));?> </th><th class="infosep">: </th><td><?php echo $frm["dni"]; ?></td></tr>
            <tr><th><?php echo ucfirst(JrTexto::_('Telephone'));?></th><th class="infosep">:</th><td><?php echo $frm["celular"]."/".$frm["telefono"]; ?></td></tr>
            <tr><th><?php echo ucfirst(JrTexto::_('Email'));?></th><th class="infosep">:</th><td><?php echo $frm["email"]; ?></td></tr>
            <tr><th><?php echo ucfirst(JrTexto::_('Birth date'));?></th><th class="infosep">:</th><td><?php echo $frm["fechanac"]; ?></td></tr>
            <tr><th><?php echo ucfirst(JrTexto::_('Civil status'));?></th><th class="infosep">:</th><td><?php 
            echo @$this->fkestado_civil[0]["nombre"]; ?></td></tr>
            <tr><th><?php echo ucfirst(JrTexto::_('Gender'));?></th><th class="infosep">:</th><td><?php echo @$this->fksexo[0]["nombre"]; ?></td></tr>
            <tr><th><?php echo ucfirst(JrTexto::_('Address'));?></th><th class="infosep">:</th><td><?php echo $frm["urbanizacion"].", ".$frm["direccion"]; ?></td></tr>
          </table>
        </div>
        <hr>
        <div class="infopersona"> 
          <h4><strong>I. <?php echo JrTexto::_("Education");?></strong></h4>
          <table class="tableinfo">
            <tr><th><?php echo ucfirst(JrTexto::_('I.E Quemela'));?> </th></tr>
            <tr><td><?php echo ucfirst(JrTexto::_('td')); ?></td></tr>           
          </table>
        </div>

        <hr>
        <div class="infopersona"> 
          <h4><strong>I. <?php echo JrTexto::_("Work Experience");?></strong></h4>
          <table class="tableinfo">
            <tr><th><?php echo ucfirst(JrTexto::_('Empresa'));?> </th></tr>
            <tr><td><?php echo ucfirst(JrTexto::_('Rubro')); ?> fecha</td></tr>
          </table>
        </div>

         <hr>
        <div class="infopersona"> 
          <h4><strong>I. <?php echo JrTexto::_("References");?></strong></h4>
          <table class="tableinfo">
            <tr><th><?php echo ucfirst(JrTexto::_('Full name'));?> </th><th class="infosep">:</th><td><?php echo $frm["nombre"]; ?></td></tr>
            <tr><th><?php echo ucfirst(JrTexto::_('Email'));?> </th><th class="infosep">: </th><td><?php echo $frm["email"]; ?></td></tr>
            <tr><th><?php echo ucfirst(JrTexto::_('Telephone'));?></th><th class="infosep">:</th><td><?php echo $frm["celular"]."/".$frm["telefono"]; ?></td></tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>