<style>
    #ui-datepicker-div{
        background: #fff;
        border-radius: 4px;
        border: 1px solid black;
    } 
    .calendar .fc-day.fc-today{
        box-shadow: inset 0px 0px 15px 2px #7ac2ff;
    }
</style>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>
<div class="row" id="curso-horario"> <div class="col-xs-12">   
<input type="hidden" id="idcurso" name="idcurso" value="<?php echo(@$this->idCurso)?>"> 
<input type="hidden" name="hVistaListado" id="hVistaListado" value="<?php echo (@$this->agenda_listado)?'true':'false'; ?>">
    <h2 class="col-xs-12 col-sm-8 color-brown-dark" style="margin-top: 0;">
        <i class="fa fa-calendar-check-o"></i> 
        <?php echo @$this->titulo; ?>
    </h2>
    <?php if(!empty(@$this->grupoBtns)){ ?>
    <div class="col-xs-12 col-sm-4 grupo_botones">
        <div class="btn-group pull-right" role="group">
            <?php foreach ($this->grupoBtns as $btn) { ?>
            <a href="<?php echo $this->documento->getUrlBase().@$btn['link']; ?>" class="btn btn-brown<?php echo (!empty(@$btn['activo'])?'-dark ':' ').@$btn['activo'] ?>"><?php echo @$btn['nombre']; ?></a>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
    <div class="col-xs-12 padding-0">
        <div id='calendar' class='calendar col-md-12'></div>
    </div>
    <div class="col-xs-12 padding-0">
        <div id='calendar1' class='calendar col-md-12'></div>
    </div>
</div></div>

<div class="modal fade" id="newEvent" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="newEvent"><?php echo ucfirst(JrTexto::_('New Apppointment')); ?></h4>
            </div>
            <form id="frmAgenda" name="frmAgenda" autocomplete="off">
                <input type="hidden" id="idagenda" name="idagenda" value=""> 
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title" class="form-control-label"><?php echo ucfirst(JrTexto::_('title')); ?></label>
                        <input type="text" class="form-control" id="title">
                    </div>
                        <label><?php echo ucfirst(JrTexto::_('start')); ?></label>
                    <div class='input-group date datetimepicker border1' style="margin-top: 0;margin-bottom: 15px;">
                        <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>  </span>
                          <input type='text' class="form-control border0" id="fecha"/>
                    </div>
                    <label><?php echo ucfirst(JrTexto::_('end')); ?></label>
                    <div class='input-group date datetimepicker border1' style="margin-top: 0;margin-bottom: 15px;">
                        <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></span>
                          <input type='text' class="form-control border0" id="fechaFin"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <?php foreach ($this->arrColores as $color) { ?>
                        <div class="col-xs-12 col-sm-2 col-md-2" >
                            <input type="radio" id="colores" name="colores" value="<?php echo $color['rgb']; ?>"><a class="btn <?php echo @$color['btn']; ?> disabled"></a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger left hidden" id="delete"><?php echo ucfirst(JrTexto::_('delete event')); ?></button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo ucfirst(JrTexto::_('cancel')); ?></button>
                    <button type="button" class="btn btn-success" id="submit"><?php echo ucfirst(JrTexto::_('save apppointment')); ?></button>
                </div>
                <div class="modal-body cargando" style="display: none"><?php echo ucfirst(JrTexto::_('loading')); ?></div>
            </form>

        </div>
    </div>
</div>

<script>
$( function() {
    $('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
    });
});
</script>



