<div class="" id="inicio">
    <div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12" style="padding: 0;">
            <div class="col-xs-12 col-sm-4  btn-panel-container panelcont-xs">
                <a href="<?php echo $this->documento->getUrlBase().'/curso/informacion/?id='.$this->cursoActual['idcurso']; ?>" class="btn btn-block btn-turquoise btn-panel">
                    <i class="btn-icon fa fa-info-circle fa-2x"></i>
                    <?php echo JrTexto::_('Information'); ?>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4  btn-panel-container panelcont-xs">
                <a href="<?php echo $this->documento->getUrlBase().'/curso/horario/?id='.$this->cursoActual['idcurso']; ?>" class="btn btn-block btn-brown-dark btn-panel">
                    <i class="btn-icon fa fa-calendar-check-o"></i>
                    <?php echo JrTexto::_('Schedule'); ?>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4  btn-panel-container panelcont-xs">
                <a href="<?php echo $this->documento->getUrlBase().'/curso/companieros/?id='.$this->cursoActual['idcurso']; ?>" class="btn btn-block btn-blue btn-panel">
                    <i class="btn-icon fa fa-users"></i>
                    <?php echo JrTexto::_('Classmates'); ?>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4 hidden btn-panel-container panelcont-xs">
                <a href="<?php echo $this->documento->getUrlBase().'/curso/asistencia/?id='.$this->cursoActual['idcurso']; ?>" class="btn btn-block btn-panel" style="background: #F39C12; color:#fff">
                    <i class="btn-icon fa fa-check-square"></i>
                    <?php echo JrTexto::_('Assists & Record'); ?>
                </a>
            </div>
        </div>
    </div>
</div>