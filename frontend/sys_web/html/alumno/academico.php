<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<div class="row" id="academico"> <div class="col-xs-12">
    <h2 class="col-xs-12 col-sm-8" style="color:#F39C12;margin-top: 0;">
        <i class="fa fa-check-square"></i> 
        <?php echo ucfirst(JrTexto::_('Assists & Record')); ?>
    </h2>
    <div class="col-xs-12">
        <ul class="nav nav-pills">
            <li class="active"><a href="#calificaciones" data-toggle="pill"><?php echo ucfirst(JrTexto::_('Grades')); ?></a></li>
            <li><a href="#asistencias" data-toggle="pill"><?php echo ucfirst(JrTexto::_('attendances')); ?></a></li>
        </ul>
    </div>
    <div class="col-xs-12 ">
        <div class="panel border-yellow">
            <!--<div class="panel-body">
                <div class="col-xs-6 select-ctrl-wrapper select-orange">
                    <select name="opcPeriodoAcademico" id="opcPeriodoAcademico" class="form-control select-ctrl">
                        <option value="-1">- <?php echo ucfirst(JrTexto::_('Select academic period')); ?> -</option>
                        <option value="1">Trimestre I</option>
                        <option value="2">Trimestre II</option>
                        <option value="3">Trimestre III</option>
                    </select>
                </div>
            </div>-->
            <div class="panel-body tab-content" style="max-height: 300px; overflow: auto;">
                <div class="tab-pane fade in active" id="calificaciones">
                    <table class="table table-striped">
                        <tbody>
                            <?php $suma=0;
                            if(!empty($this->notas_det)){
                                foreach ($this->notas_det as $i => $n) {
                                    $suma+=$n['nota'];
                            ?>
                            <tr>
                                <td><?php if($n['tipo']=='L') echo ucfirst(JrTexto::_('Lesson ').$n['idrecurso'].': '.$n['titulo']) ?></td>
                                <td><?php echo $n['nota']; ?></td>
                            </tr>
                            <?php } } ?>
                        </tbody>
                        <tfoot>
                            <tr class="bolder">
                                <td class="text-right">Promedio</td>
                                <td class=""><?php $prom=(count($this->notas_det)>0)?$suma/count($this->notas_det):0.0; 
                                echo number_format($prom, 2);?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="tab-pane fade" id="asistencias">
                    <table class="table table-striped">
                        <tbody>
                             <?php
                                //for($i=0;$i<$this->weeks;$i++){ 
                                    foreach ($this->asistencia_alum as $asis) {
                                        $nuevafecha = date ( 'd-m-Y' , strtotime($asis['fecha']) );
                                        $class='';
                                        if(strtoupper($asis['estado'])=='A'){ $class='color-green'; $title=JrTexto::_('Attended');
                                        }else if(strtoupper($asis['estado'])=='F') {$class='color-red'; $title=JrTexto::_('Missed');
                                        }else if(strtoupper($asis['estado'])=='T') {$class='color-yellow';$title=JrTexto::_('Late');}

                                        if(strtotime($nuevafecha)>=strtotime(date('d-m-Y'))){
                                            break;
                                        } ?>
                                    <tr>
                                       <td><?php  echo($nuevafecha) ?></td>
                                        <td><i class='fa fa-circle fa-2x <?php echo($class)?>' title="<?php echo($title)?>"></td>
                                    </tr>
                                    <?php }
                              //  }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

