<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php $index=0; foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link']) && (count($this->breadcrumb)-1)!=$index ){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        $index++;
        } ?>
    </ol>
</div> </div>


<div class="row" id="actividades_practicar">

    <div class="row" id="filtro-habilidad">
        <div class="col-xs-offset-0 col-sm-offset-8 col-xs-12 col-sm-4 select-ctrl-wrapper select-azul <?php echo $this->idHabilidad!=0?'hidden':'' ?>">
            <select name="opcIdHabilidad" id="opcIdHabilidad" class="form-control select-ctrl">
                <option value="0">- <?php echo ucfirst(JrTexto::_("Select skill")); ?> -</option>
                <?php if(!empty($this->habilidades)){
                foreach ($this->habilidades  as $h) {
                    $isSelected = ($h["idmetodologia"]==@$this->idHabilidad);
                    echo '<option value="'.$h["idmetodologia"].'" '.($isSelected?"selected":"").'>'.$h["nombre"].'</option>';
                }} ?>
            </select>
        </div>
    </div>

    <?php if(!empty($this->cursos_nivel)){
    foreach($this->cursos_nivel as $c) {
        $arrParams = array();
        $param = '';
        if (isset($c["idcurso"])) { $arrParams[] = 'idcurso='.$c["idcurso"]; }
        if (isset($c["idcursodetalle"])) { $arrParams[] = 'iddetalle='.$c["idcursodetalle"]; }
        if ($this->idHabilidad!=0) { $arrParams[] = 'idhab='.$this->idHabilidad; }

        if(!empty($arrParams)) { $param = '?'.implode('&', $arrParams); }
    ?>
    <div class="col-xs-6 col-md-3 item_cursonivel">
        <a href="<?php echo $this->documento->getUrlBase().'/actividades/practicar/'.$param ?>" class="thumbnail">
            <img src="<?php echo $this->documento->getUrlBase().( !empty(@$c['imagen'])?str_replace('__xRUTABASEx__', '', $c['imagen']):$this->documento->getUrlStatic().'/media/imagenes/level_default.png'); ?>" alt="cover">
            <h2 class="nombre_item text-center"><?php echo $c["nombre"]; ?></h2>
        </a>
    </div>
    <?php } } else if(!empty($this->ejercicios) && !empty(@$this->ejercicios[2]["act"]["det"])) { ?>
    <div id="panel-ejercicios-practicar">
        <div class="col-xs-12">
            <ul class="nav nav-tabs ejercicios"
                id="<?php echo $this->ejercicios[2]["met"]["nombre"]; ?>-tabs"
                data-metod="<?php echo $this->ejercicios[2]["met"]["nombre"]; ?>"
                data-id-metod="<?php echo $this->ejercicios[2]["met"]["idmetodologia"]; ?>">
                <?php foreach (@$this->ejercicios[2]["act"]["det"] as $i => $elem) { ?>
                <li class="<?php echo ($i==0)?'active':''; ?>"><a href="#ejerc_<?php echo $elem["iddetalle"]; ?>" data-toggle="tab"><?php echo $i+1; ?></a></li>
                <?php } ?>
            </ul>
        </div>

        <div class="metod active" id="met-2" data-idmet="2">
            <div class="col-xs-12 tab-content">
                <?php foreach ($this->ejercicios[2]["act"]["det"] as $i=>$e) { ?>
                <div class="tab-pane tabhijo <?php echo ($i==0)?'active':''; ?>" id="ejerc_<?php echo $e["iddetalle"]; ?>">
                    <?php echo str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $e["texto_edit"]); ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } else {
        #no hay data que mostrar
    }?>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $("#opcIdHabilidad").change(function(e) {
        let value = $(this).val();
        if(value<=0) return false;

        var url = window.location.href;
        if (url.indexOf('?') > -1){ url += '&';
        }else{ url += '/?'; }
        url += 'idhab='+value;

        window.location.href = url;
    });

    if( $('#panel-ejercicios-practicar').length ){
        $('#panel-ejercicios-practicar').tplcompletar({editando:false});
        rinittemplatealshow();
        $('#panel-ejercicios-practicar .nopreview').hide();
    }
});
</script>