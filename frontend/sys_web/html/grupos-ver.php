
<?php 
$pk=$this->pk;
$reg=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Ver Grupos'); ?></h2>
        <div class="btn-group btn-group-md" style="float:right">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('grupos'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir a listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idgrupo"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idgrupo"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>            
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-md-12">
        <div id="msj-interno">
        <table class="table table-striped">
         <tr>
              <th><?php echo JrTexto::_("Idgrupo") ;?> </th>
              <th>:</th> 
          <td><?php echo $reg["idgrupo"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Iddocente") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["iddocente"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idlocal") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idlocal"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idambiente") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idambiente"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idasistente") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idasistente"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Fechainicio") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["fechainicio"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Fechafin") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["fechafin"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Dias") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["dias"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Horas") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["horas"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Horainicio") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["horainicio"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Horafin") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["horafin"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Valor") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["valor"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Valor asi") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["valor_asi"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Regusuario") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["regusuario"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Regfecha") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["regfecha"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Matriculados") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["matriculados"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Nivel") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["nivel"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Codigo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["codigo"] ;?></td>
          </tr>
                    </table>
          </div>
          <hr><div class="text-center">
          <div class="btn-group btn-group-md">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('grupos'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir al listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idgrupo"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idgrupo"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>
	        </div>         
	    </div>
</div>
<br />
	</div>
	<script type="text/javascript">
  var asInitVals = new Array();
	$(document).ready(function(){
    
    $('.btnNuevo').click(function(){
      $('#pkaccion').val('guardar');
      $('#pkidgrupo').val('');
      $('.img-thumbnail').attr("style","display:none");
    });

    $('.editar').click(function(){
      $('.img-thumbnail').removeAttr("style");
      var id=$(this).attr("data-reg");
          addFancyAjax("<?php echo JrAplicacion::getJrUrl(array('Grupos', 'frm'))?>?tpl=b&acc=Editar&id="+id, true);
          
    });

    $('.eliminar').click(function(){
     if(confirm("<?php echo JrTexto::_('¿Desea eliminar el registro seleccionado?');?>")) {
        var id=$(this).attr("data-reg");
        var res = xajax__('', 'Grupos', 'eliminarGrupos',id)
        if(res) {
          agregar_msj_interno('success', "<?php echo JrTexto::_('Registro eliminado');?>");
                    recargarpagina(false);
                    } 
        $('.alert').fadeOut(4000);
      }
    });
	});
</script>