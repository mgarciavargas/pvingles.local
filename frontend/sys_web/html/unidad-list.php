<?php defined("RUTA_BASE") or die(); ?><div class="form-view" >
  <div class="page-title">
    <div class="title_left">
    <ol class="breadcrumb">
      <li><a href="<?php echo JrAplicacion::getJrUrl(array("administrador"));?>"><?php echo JrTexto::_('Dashboard');?></a></li>
      <li><a href="<?php echo JrAplicacion::getJrUrl(array("unidad"));?>"><?php echo JrTexto::_('Unit'); ?></a></li>
      <li class="active"> <?php echo JrTexto::_('list')?></li>
    </ol>     
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="div_linea"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="x_panel">
        <div class="x_title">&nbsp;&nbsp;
		  <a class="btn btn-success btn-xs" href="<?php echo JrAplicacion::getJrUrl(array("unidad", "agregar"));?>?idnivel=<?php echo $this->idnivel;?>">
          <i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?> </a>
          <div class="nav navbar-right"></div>
          <div class="clearfix"></div>
        </div>
         <div class="div_linea"></div>
         <div class="x_content">
         <form>
         <?php echo JrTexto::_('Level');?>
         
          <select id="txtNivel" name="txtNivel" onchange="this.form.submit()" >
          <?php foreach ($this->niveles as $nivel) { ?>
            <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $this->idnivel===$nivel["idnivel"]?'selected="selected"':''?>> <?php echo $nivel["nombre"]?> </option>
          <?php } ?>             
          </select>
          </form>
         </div>
         <div class="div_linea"></div>
         <div class="x_content">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>                
                    <th><?php echo JrTexto::_("Name") ;?></th>
                    <th><?php echo JrTexto::_("Order") ;?></th>
                    <th><?php echo JrTexto::_("Image") ;?></th>                  
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php $i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ $i++; ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $reg["nombre"] ;?></td>
                  <td data-tipo="U" data-orden="<?php echo $reg["orden"] ;?>" data-id="<?php echo $reg["idnivel"]; ?>" data-padre="<?php echo $reg["idpadre"] ?>">
                    <button class="btnordenar" data-value="1"  ><i class="fa fa-arrow-down"></i></button>
                    <button class="btnordenar" data-value="-1" ><i class="fa fa-arrow-up"></i></button>
                  </td>
                  <td>
                  <?php 
                    $ruta=!empty($reg["imagen"])?$reg["imagen"]:$this->documento->getUrlStatic().'/media/imagenes/levels/nofoto.jpg';
                    $ruta=str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$ruta);
                  ?>
                  <img src="<?php echo $ruta; ?>" width="100px" height="60px" >                 
                  </td>
                   <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="<?php echo $reg["idnivel"]; ?>"> <i class="fa fa<?php echo !empty($reg["estado"])?"-check":""; ?>-circle-o fa-lg"></i>  <?php echo $reg["estado"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td><a class="btn btn-xs lis_ver" href="<?php echo JrAplicacion::getJrUrl(array('unidad'))?>ver/?id=<?php echo $reg["idnivel"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update" href="<?php echo JrAplicacion::getJrUrl(array("unidad", "editar", "id=" . $reg["idnivel"]."&idnivel=".$reg["idpadre"]))?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove" href="javascript:;" data-id="<?php echo $reg["idnivel"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php } ?>
          </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
  $('.btn-eliminar').bind({   
    click: function() {
       var id=$(this).attr('data-id');
       $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'niveles', 'eliminar', id);
          if(res){
            return redir('<?php echo JrAplicacion::getJrUrl(array('unidad'))?>');
          }
        }
      });     
    }
  });

  $('.btnordenar').click(function(){    
    var obj=$(this);
    var tdobj=obj.closest('td');
    var id=tdobj.attr('data-id');
    var orden=tdobj.attr('data-orden');
    var accion=obj.attr('data-value');
    var tr = tdobj.closest("tr");
    var tipo='U';
    var padre=tdobj.attr('data-padre');
    var data={
      id:id,
      ordenactual:orden,
      tipo:tipo,
      ordenaa:accion,
      idpadre:padre
    };

    var res = xajax__('', 'niveles', 'ordenar', data);
      if(res){          
      return redir('<?php echo JrAplicacion::getJrUrl(array('unidad'))."?txtNivel=".@$_GET["txtNivel"]?>');
      }
   });
  
  $('.btn-chkoption').bind({
    click: function() {     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
           var res = xajax__('', 'niveles', 'setCampo', id,campo,data);
           if(res) {          
            return redir('<?php echo JrAplicacion::getJrUrl(array('unidad'))."?txtNivel=".@$_GET["txtNivel"]?>');
           }
        }
      });
    }
  });
  
  $('.table').DataTable(
<?php if($this->documento->getIdioma()!='EN'){?>
  {    
    "language": {
            "url": "<?php echo $this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma(); ?>.json"
        }
  }
<?php } ?>
  );

});
</script>