<?php defined("RUTA_BASE") or die();
//var_dump($this->datos);
//var_dump($this->datosxRol);

?><div class="form-view" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("administrador"));?>"><?php echo JrTexto::_("Dashboard");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('permisos'));?>"><?php echo JrTexto::_('Permisos'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="div_linea"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="x_panel">
        <!--div class="x_title">
          <a class="btn btn-success btn-xs" href="<?php echo JrAplicacion::getJrUrl(array("Permisos", "agregar"));?>">
          <i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
          <div class="nav navbar-right"></div>
          <div class="clearfix"></div>
        </div-->
        <div class="div_linea"></div>
         <div class="x_content">
         <form>
         <?php echo JrTexto::_('Role');?>         
          <select id="idRol" name="idRol" onchange="this.form.submit()" >
          <?php foreach ($this->roles as $rol) { ?>
            <option value="<?php echo $rol["idrol"]; ?>" <?php echo $this->idRol===$rol["idrol"]?'selected="selected"':''?>> <?php echo ucfirst($rol["rol"])?> </option>
          <?php } ?>             
          </select>
          </form>
         </div>
        <div class="div_linea"></div>
         <div class="x_content">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                    <th><?php echo JrTexto::_("Menu") ;?></th>
                    <th><?php echo JrTexto::_("list") ;?></th>
                    <th><?php echo JrTexto::_("add") ;?></th>
                    <th><?php echo JrTexto::_("edit") ;?></th>
                    <th><?php echo JrTexto::_("delete") ;?></th>                   
                </tr>
              </thead>
              <tbody>
              <?php $i=0; 
                $xdatos=array();
                if(!empty($this->datosxRol))
                foreach ($this->datosxRol as $reg){ $i++; 
                  $xdatos[]=$reg["nombre"];
                  ?>
                <tr>
                  <td><?php echo $i;?></td>               
                    <td><?php echo $reg["nombre"] ;?></td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="_list"  data-id="<?php echo $reg["idmenu"]; ?>"> <i class="fa fa<?php echo !empty($reg["_list"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["_list"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="_add"  data-id="<?php echo $reg["idmenu"]; ?>"> <i class="fa fa<?php echo !empty($reg["_add"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["_add"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="_edit"  data-id="<?php echo $reg["idmenu"]; ?>"> <i class="fa fa<?php echo !empty($reg["_edit"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["_edit"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="_delete"  data-id="<?php echo $reg["idmenu"]; ?>"> <i class="fa fa<?php echo !empty($reg["_delete"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["_delete"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                </tr>
            <?php }
             foreach ($this->datos as $reg){ $i++;                  
                  if(in_array($reg["nombre"],$xdatos)) continue;  
                  $xdatos[]=$reg["nombre"];
                  ?>
                <tr>
                  <td><?php echo $i;?></td>               
                    <td><?php echo $reg["nombre"] ;?></td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="_list"  data-id="<?php echo $reg["idmenu"]; ?>"> <i class="fa fa-circle-o fa-lg"></i> <?php echo JrTexto::_("Inactive"); ?></a> </td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="_add"  data-id="<?php echo $reg["idmenu"]; ?>"> <i class="fa fa-circle-o fa-lg"></i> <?php echo JrTexto::_("Inactive"); ?></a> </td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="_edit"  data-id="<?php echo $reg["idmenu"]; ?>"> <i class="fa fa-circle-o fa-lg"></i> <?php echo JrTexto::_("Inactive"); ?></a> </td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="_delete"  data-id="<?php echo $reg["idmenu"]; ?>"> <i class="fa fa-circle-o fa-lg"></i> <?php echo JrTexto::_("Inactive"); ?></a> </td>
                </tr>
            <?php } 


            ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function(){ 
  $('.btn-chkoption').bind({
    click: function() {
    var data_=new Array();  
      data_.menu=$(this).attr('data-id');
      data_.campo=$(this).attr('campo');
      data_.rol=$('#idRol').val();
      var flag=0;
      var txt='<?php echo JrTexto::_("Inactive")?>';
      if($("i",this).hasClass('fa-circle-o')) {
        flag=1;
        txt='<?php echo JrTexto::_("Active")?>';
      }
      var obji=$("i",this).parent();
      
      data_.estado=flag;
     /* $.confirm({
        title: '<?php //echo JrTexto::_('Confirm action');?>',
        content: '<?php //echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php //echo JrTexto::_('Accept');?>',
        cancelButton: '<?php //echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){*/
           var res = xajax__('', 'permisos', 'setCampo', data_);
           if(res){
            $(obji).html('<i class="fa '+(flag==1?'fa-check-circle-o':'fa-circle-o')+' fa-lg"></i> '+txt);   
            //return redir('<?php echo JrAplicacion::getJrUrl(array('permisos'))?>');
           }
        /*}
      });*/
    }
  });  
  $('.table').DataTable({
    "language": {
            "url": "<?php echo $this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma(); ?>.json"
    }
  });
});
</script>