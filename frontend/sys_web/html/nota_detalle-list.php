<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?><style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
  .form-group{
    margin-bottom: 0px; 
  }
  .input-group {
    margin-top: 0px;
  }
  select.select-ctrl, .form-control , .input-group-addon{   
    border: 1px solid #4683af;
    margin-bottom: 1ex;
  }
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("academico"));?>"><?php echo JrTexto::_("Academic");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('nota_detalle'));?>"><?php echo JrTexto::_('Nota_detalle'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
                                                                                                                                                                                    
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Nota_detalle", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Nota_detalle").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idcurso") ;?></th>
                    <th><?php echo JrTexto::_("Idmatricula") ;?></th>
                    <th><?php echo JrTexto::_("Idalumno") ;?></th>
                    <th><?php echo JrTexto::_("Iddocente") ;?></th>
                    <th><?php echo JrTexto::_("Idcursodetalle") ;?></th>
                    <th><?php echo JrTexto::_("Nota") ;?></th>
                    <th><?php echo JrTexto::_("Tipo") ;?></th>
                    <th><?php echo JrTexto::_("Observacion") ;?></th>
                    <th><?php echo JrTexto::_("Fecharegistro") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5a1cb11b984df='';
function refreshdatos5a1cb11b984df(){
    tabledatos5a1cb11b984df.ajax.reload();
}
$(document).ready(function(){  
  var estados5a1cb11b984df={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5a1cb11b984df='<?php echo ucfirst(JrTexto::_("nota_detalle"))." - ".JrTexto::_("edit"); ?>';
  var draw5a1cb11b984df=0;

  
  $('.btnbuscar').click(function(ev){
    refreshdatos5a1cb11b984df();
  });
  tabledatos5a1cb11b984df=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Idcurso") ;?>'},
            {'data': '<?php echo JrTexto::_("Idmatricula") ;?>'},
            {'data': '<?php echo JrTexto::_("Idalumno") ;?>'},
            {'data': '<?php echo JrTexto::_("Iddocente") ;?>'},
            {'data': '<?php echo JrTexto::_("Idcursodetalle") ;?>'},
            {'data': '<?php echo JrTexto::_("Nota") ;?>'},
            {'data': '<?php echo JrTexto::_("Tipo") ;?>'},
            {'data': '<?php echo JrTexto::_("Observacion") ;?>'},
            {'data': '<?php echo JrTexto::_("Fecharegistro") ;?>'},
            {'data': '<?php echo JrTexto::_("Estado") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/nota_detalle/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             //d.texto=$('#texto').val(),
                        
            draw5a1cb11b984df=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5a1cb11b984df;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Idcurso") ;?>': data[i].idcurso,
              '<?php echo JrTexto::_("Idmatricula") ;?>': data[i].idmatricula,
              '<?php echo JrTexto::_("Idalumno") ;?>': data[i].idalumno,
              '<?php echo JrTexto::_("Iddocente") ;?>': data[i].iddocente,
              '<?php echo JrTexto::_("Idcursodetalle") ;?>': data[i].idcursodetalle,
              '<?php echo JrTexto::_("Nota") ;?>': data[i].nota,
              '<?php echo JrTexto::_("Tipo") ;?>': data[i].tipo,
              '<?php echo JrTexto::_("Observacion") ;?>': data[i].observacion,
                '<?php echo JrTexto::_("Fecharegistro") ;?>': data[i].fecharegistro,
              '<?php echo JrTexto::_("Estado") ;?>': data[i].estado,
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/nota_detalle/editar/?id='+data[i].idnota_detalle+'" data-titulo="'+tituloedit5a1cb11b984df+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idnota_detalle+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'nota_detalle', 'setCampo', id,campo,data);
          if(res) tabledatos5a1cb11b984df.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5a1cb11b984df';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Nota_detalle';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'nota_detalle', 'eliminar', id);
        if(res) tabledatos5a1cb11b984df.ajax.reload();
      }
    }); 
  });
});
</script>