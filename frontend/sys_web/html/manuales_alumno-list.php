<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Manuales_alumno"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
                                                                                                                                  <div class="col-xs-6 col-sm-4 col-md-3">
                      <div class="form-group">    
                          <div class='input-group date datetimepicker1 border1' >
                            <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo  ucfirst(JrTexto::_("fecha"))?> </span>
                            <input type='text' class="form-control border0" name="datefecha" id="datefecha" />           
                          </div>
                      </div>
                    </div>
                                                        
                    <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
                      <select name="cbestado" id="cbestado" class="form-control select-ctrl">
                          <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                          <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                          <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                      </select>
                    </div>
                                              
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Manuales_alumno", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Manuales_alumno").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idgrupo") ;?></th>
                    <th><?php echo JrTexto::_("Idalumno") ;?></th>
                    <th><?php echo JrTexto::_("Idmanual") ;?></th>
                    <th><?php echo JrTexto::_("Idaulagrupodetalle") ;?></th>
                    <th><?php echo JrTexto::_("Comentario") ;?></th>
                    <th><?php echo JrTexto::_("Fecha") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    <th><?php echo JrTexto::_("Serie") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5a5fca82dc5e1='';
function refreshdatos5a5fca82dc5e1(){
    tabledatos5a5fca82dc5e1.ajax.reload();
}
$(document).ready(function(){  
  var estados5a5fca82dc5e1={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5a5fca82dc5e1='<?php echo ucfirst(JrTexto::_("manuales_alumno"))." - ".JrTexto::_("edit"); ?>';
  var draw5a5fca82dc5e1=0;

  
  $('#datefecha').change(function(ev){
    refreshdatos5a5fca82dc5e1();
  });
  $('#cbestado').change(function(ev){
    refreshdatos5a5fca82dc5e1();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos5a5fca82dc5e1();
  });
  tabledatos5a5fca82dc5e1=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Idgrupo") ;?>'},
            {'data': '<?php echo JrTexto::_("Idalumno") ;?>'},
            {'data': '<?php echo JrTexto::_("Idmanual") ;?>'},
            {'data': '<?php echo JrTexto::_("Idaulagrupodetalle") ;?>'},
            {'data': '<?php echo JrTexto::_("Comentario") ;?>'},
            {'data': '<?php echo JrTexto::_("Fecha") ;?>'},
            {'data': '<?php echo JrTexto::_("Estado") ;?>'},
            {'data': '<?php echo JrTexto::_("Serie") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/manuales_alumno/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.fecha=$('#datefecha').val(),
             d.estado=$('#cbestado').val(),
             //d.texto=$('#texto').val(),
                        
            draw5a5fca82dc5e1=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5a5fca82dc5e1;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Idgrupo") ;?>': data[i].idgrupo,
              '<?php echo JrTexto::_("Idalumno") ;?>': data[i].idalumno,
              '<?php echo JrTexto::_("Idmanual") ;?>': data[i].idmanual,
              '<?php echo JrTexto::_("Idaulagrupodetalle") ;?>': data[i].idaulagrupodetalle,
              '<?php echo JrTexto::_("Comentario") ;?>': data[i].comentario,
                '<?php echo JrTexto::_("Fecha") ;?>': data[i].fecha,
              '<?php echo JrTexto::_("Estado") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="'+data[i].identrega+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5a5fca82dc5e1[data[i].estado]+'</a>',
              '<?php echo JrTexto::_("Serie") ;?>': data[i].serie,
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/manuales_alumno/editar/?id='+data[i].identrega+'" data-titulo="'+tituloedit5a5fca82dc5e1+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].identrega+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'manuales_alumno', 'setCampo', id,campo,data);
          if(res) tabledatos5a5fca82dc5e1.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5a5fca82dc5e1';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Manuales_alumno';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'manuales_alumno', 'eliminar', id);
        if(res) tabledatos5a5fca82dc5e1.ajax.reload();
      }
    }); 
  });
});
</script>