
<?php 
$pk=$this->pk;
$reg=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Ver Local'); ?></h2>
        <div class="btn-group btn-group-md" style="float:right">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('local'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir a listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idlocal"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idlocal"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>            
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-md-12">
        <div id="msj-interno">
        <table class="table table-striped">
         <tr>
              <th><?php echo JrTexto::_("Idlocal") ;?> </th>
              <th>:</th> 
          <td><?php echo $reg["idlocal"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Nombre") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["nombre"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Direccion") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["direccion"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Id ubigeo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["id_ubigeo"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Tipo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["tipo"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Vacantes") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["vacantes"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idugel") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idugel"] ;?></td>
          </tr>
                    </table>
          </div>
          <hr><div class="text-center">
          <div class="btn-group btn-group-md">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('local'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir al listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idlocal"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idlocal"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>
	        </div>         
	    </div>
</div>
<br />
	</div>
	<script type="text/javascript">
  var asInitVals = new Array();
	$(document).ready(function(){
    
    $('.btnNuevo').click(function(){
      $('#pkaccion').val('guardar');
      $('#pkidlocal').val('');
      $('.img-thumbnail').attr("style","display:none");
    });

    $('.editar').click(function(){
      $('.img-thumbnail').removeAttr("style");
      var id=$(this).attr("data-reg");
          addFancyAjax("<?php echo JrAplicacion::getJrUrl(array('Local', 'frm'))?>?tpl=b&acc=Editar&id="+id, true);
          
    });

    $('.eliminar').click(function(){
     if(confirm("<?php echo JrTexto::_('¿Desea eliminar el registro seleccionado?');?>")) {
        var id=$(this).attr("data-reg");
        var res = xajax__('', 'Local', 'eliminarLocal',id)
        if(res) {
          agregar_msj_interno('success', "<?php echo JrTexto::_('Registro eliminado');?>");
                    recargarpagina(false);
                    } 
        $('.alert').fadeOut(4000);
      }
    });
	});
</script>