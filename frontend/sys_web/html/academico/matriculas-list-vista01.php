<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"eeeexzx-1";
if(!empty($this->grupos)) $grupos=$this->grupos;
//var_dump($grupos);
?>
<style type="text/css">
  #panel<?php echo $idgui; ?>{

  }
  #btnadd<?php echo $idgui; ?>{
    position: absolute;
    right: 2em;
    top:1ex;
    z-index: 2;
  }
</style>
<div class="col-md-12">
<div class="panel">
	<div class="panel-body" id="panel<?php echo $idgui; ?>" >    
<table class="table table-striped table-responsive" id="table_<?php echo $idgui; ?>">
  	<thead>
    	<tr class="headings">
      		<th>#</th>
        	<th><?php echo JrTexto::_("Student") ;?></th>
          <th><?php echo JrTexto::_("Course") ;?></th>
          <th><?php echo JrTexto::_("Teacher") ;?></th>
          <?php if(@$fktipo!='V'){ ?><th><?php echo JrTexto::_("Ubication") ;?></th><?php } ?>
          <th><?php echo JrTexto::_("Date register"); ?></th>        	
        	<th><?php echo JrTexto::_("State") ;?></th>
        	<th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
    	</tr>
  	</thead>
  	<tbody>
  		<?php 
  		$i=0;
  		$url=$this->documento->getUrlBase();
  		if(!empty($grupos))
  			foreach ($grupos as $rw){ 
  					$i++;  					
  					?>
  				<tr>
  					<td><?php echo $i; ?></td>
  					<td><?php echo @$rw["idalumno"]." :".@$rw["stralumno"];?></td>
            <td><?php echo @$rw["strcurso"];?></td>
            <td><?php echo @$rw["strdocente"];?></td>
             <?php if(@$fktipo!='V'){ ?><td> <?php echo @$rw["strlocal"]."<br>".@$rw["strambiente"] ?></td><?php } ?>
            <td><?php echo @$rw["fecha_registro"];?></td>  					
  					<td><?php echo @$rw["estado"]; ?></td>  					
  					<td data-idmatricula="<?php echo @$rw["idmatricula"]; ?>"  data-idgrupoauladet="<?php echo @$rw["idgrupoauladetalle"]; ?>" >
  						<!--a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/verficha/?id=<?php echo $rw["dni"]; ?>" data-titulo="<?php echo JrTexto::_('Ficha')." ".$fullnombre; ?>"><i class="fa fa-user"></i><i class="fa fa-eye"></i></a-->
              <!--a class="btn-editar btn btn-xs" href="javascript:void(0);" data-titulo="<?php echo JrTexto::_('Edit'); ?>"> <i class="fa fa-pencil"></i></a-->
              <!--a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/cambiarclave/?id=<?php echo $rw["dni"]; ?>" data-titulo="<?php echo JrTexto::_('Change')." ".JrTexto::_('Password'); ?>"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>
              <a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?id=<?php echo $rw["dni"]; ?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('notes');?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a>
              <a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/horario/?id=<?php echo $rw["dni"]; ?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a-->
              <a class="btn-verhorario btn btn-xs" href="javascript:void(0);" data-titulo="<?php echo JrTexto::_('Schedule'); ?>" ><i class="fa fa-calendar"></i></a> 
              <a class="btn-eliminar btn btn-xs" href="javascript:void(0);" data-titulo="<?php echo JrTexto::_('delete'); ?>" ><i class="fa fa-trash-o"></i></a>
  					</td>
  				</tr>
  		<?php }	?>
    </tbody>
</table>
	</div>       
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#table_<?php echo $idgui; ?>').DataTable({
			"searching": false,
      		"processing": false
			<?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
		}).on('click','.btn-eliminar',function(ev){
      var idmatricula=$(this).closest('td').attr('data-idmatricula');      
      var formData = new FormData();
      btn=$(this);
      formData.append('idmatricula', idmatricula);    
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/acad_matricula/eliminarmatricula',
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        showmsjok:true,
        callback:function(rs){        
         btn.closest('tr').remove('');
        }
      }
      sysajax(data);
    }).on('click','.btn-verhorario',function(ev){
      var idmatricula=$(this).closest('td').attr('data-idmatricula');  
      var idgrupoauladetalle=$(this).closest('td').attr('data-idgrupoauladet');  
      window.location.href=_sysUrlBase_+'/acad_grupoaula/horario/?idgrupoauladetalle='+idgrupoauladetalle;
    });
    $('#btnadd<?php echo $idgui; ?>').click(function(){
      var href=_sysUrlBase_+'/acad_grupoaula/formulario';
      window.location.href=href;
      console.log(idgrupoaula,  idgrupoauladetalle);
    })
	})
</script>