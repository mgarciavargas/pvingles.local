<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
if(!empty($frm['imagen'])) $imagen=$this->documento->getUrlBase().$frm['imagen'];
else $imagen=$this->documento->getUrlStatic().'/media/imagenes/cursos/nofoto.jpg';
$tipoaimportar=$this->tipoaImport;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  h3 i.btnchecked{
    position: absolute;
    right: 0.2ex;
    font-size:1.8em; 
    top: 0.15ex;
    z-index:90;
  }
  #acordionpk a.itemplus::before{
    content: "\f068";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
  }
  .ui-accordion-content{
    padding: 1ex !important;
    padding-right: 0px !important;
  }
</style>
<div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <?php if($this->documento->plantilla!='modal'){?>
  <div class="row" id="breadcrumb">
    <div class="col-xs-12">
      <ol class="breadcrumb">
          <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
          <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
          <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion).' '.JrTexto::_('Course'); ?></li>       
      </ol>
    </div>
  </div>
  <?php }?>
    <div class="panel">      
      <div class="panel-body" id="pnl<?php echo $idgui ?>">
        <div id="msj-interno"></div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="col-xs-12 col-sm-6 col-md-4 text-right">
                  <label><?php echo ucfirst(JrTexto::_("Level")); ?>: </label>                 
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="select-ctrl-wrapper select-azul">
                    <select name="cbnivel" id="cbnivel<?php echo $idgui ?>" class="form-control select-ctrl" data-tipo='N'>
                    <option value="-1"><?php echo ucfirst(JrTexto::_("Selected Level"))?></option>
                    <?php if(!empty($this->niveles))
                     foreach ($this->niveles as $nivel){?>                 
                      <option value="<?php echo $nivel["idnivel"]; ?>"><?php echo $nivel["nombre"]?></option>                   
                    <?php }?>
                    </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div id="acordion<?php echo $idgui ?>">
              </div>
            </div>
          </div>
          
          <div class="clearfix"><br></div>
          <div class="row">            
              <div class="col-md-12 text-center">                
                <a id="btn-importdatos<?php echo $idgui ?>" class="btn btn-primary" ><i class=" fa fa-download"></i> <?php echo JrTexto::_('Import');?> </a>
              </div>
          </div> 
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
  var datospadre<?php echo $idgui ?>=function(v){
    html='';
    if(v.hijo!=undefined){
      $.each(v.hijo,function(i,h){
        html+=datospadre<?php echo $idgui ?>(h);        
      });
      html='<div class="li_'+v.idpadre+'"><h3 data-imagen="'+v.imagen+'" data-tipo="'+v.tipo+'" data-padre="'+v.idpadre+'" class="lihijo_'+v.idpadre+'" data-id="'+v.idnivel+'"> <span class="orden"></span><span class="nombre">'+v.nombre+'</span><i class="btnchecked fa fa-square-o"></i></h3><div class="lihijo" id="lihijo_'+v.idnivel+'">'+html+'</div></div>';
    }else{
      html='<div class="li_'+v.idpadre+'"><h3 data-imagen="'+v.imagen+'" data-tipo="'+v.tipo+'" data-padre="'+v.idpadre+'"   class="lihijo_'+v.idpadre+'" data-id="'+v.idnivel+'"> <span class="orden"></span><span class="nombre">'+v.nombre+'</span><i class="btnchecked fa fa-square-o"></i></h3></div>';
    }
    return html;
  }
  var cont=0;
  var idjson<?php echo $idgui ?>={};
  var ordenar<?php echo $idgui ?>=function(ord){
    cont=0;
    var json={};
    var hijos=$('#acordion<?php echo $idgui ?> > .lihijo').children();
    $('#acordion<?php echo $idgui ?>').find('h3').removeAttr('data-orden').children('span.orden').text('');
    var ordentest=function($obj){
      if($obj.length){
        $.each($obj,function(i,v){
          var h3=$(this).children('h3');
          if(h3.length){    
            var obtj2=h3.siblings('.lihijo').children();
            if(h3.hasClass('active')){
              cont++;
              var x={
                    idnivel:h3.attr('data-id'),
                    nombre:h3.find('span.nombre').text(),
                    tipo:h3.attr('data-tipo'),
                    idpadre:h3.attr('data-padre'),imagen:h3.attr('data-imagen')
              }
              json[cont]=x;
            }
            h3.attr('data-orden',cont);
            if(obtj2.length){ordentest(obtj2);}
          }
        });
      }   
    }
    ordentest(hijos);
    idjson<?php echo $idgui ?>=json;
  }

$('#pnl<?php echo $idgui ?>').on('change','#cbnivel<?php echo $idgui ?>',function(ev){
  var formData = new FormData();
  formData.append('idNivel', $(this).val());
  formData.append('tipo', $(this).attr('data-tipo'));   
  var url=_sysUrlBase_+'/acad_curso/importarcontenido';
  $.ajax({
    url: url,
    type: "POST",
    data:  formData,
    contentType: false,
    processData: false,
    dataType:'json',
    cache: false,
    beforeSend: function(XMLHttpRequest){},
    success: function(data)
    { 
      if(data.code==='ok'){
          var result=data.data;
          var html='';         
          $.each(result,function(i,v){html+=datospadre<?php echo $idgui ?>(v);});
          html='<div class="lihijo" id="lihijo_0">'+html+'</div>';
          $('#acordion<?php echo $idgui ?>').html(html);
          var hijos=$('#acordion<?php echo $idgui ?>').find('.lihijo');
          $.each(hijos,function(i,v){
            var id = $(this).attr('id');
            $('#'+id).accordion({header:"> div > h3."+id+' ',heightStyle: "content",collapsible: true }).sortable({
              axis: "y",
              handle: "h3."+id,
              stop: function( event, ui ){              
                ui.item.children( "h3."+id ).triggerHandler( "focusout" );
                $(this).accordion("refresh");
                ordenar<?php echo $idgui ?>();
              }
            })
          });
          ordenar<?php echo $idgui ?>();

          var tipoaimportar='<?php echo $tipoaimportar; ?>';
          if(tipoaimportar=='U'){
             $('h3[data-tipo="N"] .btnchecked').remove();
          }else if(tipoaimportar=='L'){
             $('h3[data-tipo="N"] .btnchecked').remove();
             $('h3[data-tipo="U"] .btnchecked').remove();
          }
      }else{
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
      }
    },
    error: function(e){ },
    complete: function(xhr){ }
  });  
}).on('click','.btnchecked',function(ev){
   ev.preventDefault();
   ev.stopPropagation();
   if($(this).hasClass('fa-check-square')){    
    $(this).closest('div').find('h3').removeClass('active');
    $(this).closest('div').find('i').removeClass('fa-check-square').addClass('fa-square-o')
   }else{
    $(this).closest('div').find('i').removeClass('fa-square-o').addClass('fa-check-square');
    $(this).closest('div').find('h3').addClass('active');
   }
   ordenar<?php echo $idgui ?>();
}).on('click','#btn-importdatos<?php echo $idgui ?>',function(){
  <?php 
    if(!empty($ventanapadre)){?>
      var btn=$('.<?php echo $ventanapadre; ?>');
      if(btn.length){         
        btn.attr('data-import',JSON.stringify(idjson<?php echo $idgui; ?>));
        btn.trigger("addimport");
      }
      $(this).closest('.modal').find('.cerrarmodal').trigger('click'); 
  <?php } ?>
  <?php /*if($ventanapadre!='eeeexzx-1'&&$this->documento->plantilla=='modal'){ 
    echo $ventanapadre.'(idjson'.$idgui.');'; 
    echo '$(this).closest(".modal").find(".cerrarmodal").trigger("click");';
  }*/
  ?>  
});
$('#cbnivel<?php echo $idgui ?>').trigger('change');

});
</script>