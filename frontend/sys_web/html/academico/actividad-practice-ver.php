<?php defined('RUTA_BASE') or die(); 
    $idgui = uniqid(); 
    $rutabase = $this->documento->getUrlBase();
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/smartbook/ver.css">

<a href="anclaUp" class="ancla" id="anclaUp"></a>
<div id="smartbook-ver" class="editando row" data-idautor="<?php echo @$voc['idpersonal']; ?>">
    <input type="hidden" id="rolUsuario" value="<?php echo @$this->rolActivo; ?>">
    <input type="hidden" id="idCurso" value="<?php echo @$this->idCurso; ?>">
    <input type="hidden" id="idSesion" value="<?php echo @$this->sesion['idnivel']; ?>">
    <input type="hidden" id="idBitacoraAlumnoSmbook" value="<?php echo !empty($this->bitac_alum_smbook)? @$this->bitac_alum_smbook['idbitacora_smartbook']:'';?>">
    <input type="hidden" id="idLogro" value="<?php echo @$this->idLogro; ?>">

    <a href="#anclaUp" class="scroller up" id="scroll-up" style="display: none;"><i class="fa fa-angle-double-up fa-2x"></i></a>

    <div id="cuaderno" class="tabbable tabs-right col-xs-12 padding-0"> <!--div id="anillado"></div-->
        <ul id="navTabsLeft" class="nav nav-tabs metodologias hidden">
            <li class="hvr-bounce-in hidden"><a class="vertical" href="#div_look" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Look")) ?></a></li>
            <li class="hvr-bounce-in hidden"><a class="vertical" href="#div_practice" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Practice")) ?></a></li>
            <li class="hvr-bounce-in hidden"><a class="vertical" href="#div_games" data-toggle="tab"><?php echo JrTexto::_("Game") ?>s</a></li>
        </ul>

        <div id="tabContent" class="tab-content">
            <div id="contenedor-paneles" class="hidden" style="top:1ex;">
                <div id="panel-barraprogreso" class="text-center eje-panelinfo" style="display: none;">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            <span>0</span>%
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-md-12 infouserejericiopanel">                   
                    <div class="col-md-3 text-left" >
                        <a class="btn btn-inline btn-success btn-lg btnejercicio-back_" style="display: inline-block;  padding-top: 0.9em;  height: 3.1em;"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Anterior'); ?></a>
                    </div>
                    <div class="col-md-6 text-center">
                        <span class="infouserejericio ejeinfo-numero numero_ejercicio">
                            <div class="titulo">Ejercicio</div>
                            <span class="actual">1</span>
                            <span class="total">16</span>
                        </span>
                        <span id="panel-intentos-dby"  class="infouserejericio ejeinfo-intento">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Intentos')); ?></div>
                            <span class="actual">1</span>
                            <span class="total">3</span>
                        </span>
                        <span id="panel-tiempo" class="infouserejericio ejeinfo-tiempo">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Tiempo')); ?></div>
                            <span class="actual"></span>
                            <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50" style="display: none;">
                            <span class="info-show">00:00</span>
                        </span>                         
                    </div>
                    <div class="col-md-3 text-right" >
                        <a class="btn btn-inline btn-success btn-lg btnejercicio-next_" style="display: inline-block; padding-top: 0.9em;  height: 3.1em;"><?php echo JrTexto::_('Siguiente'); ?> <i class="fa fa-arrow-right"></i></a>
                    </div>                            
                </div>
               
            </div>
           
            <!-- tab-pane for #navTabsLeft -->     
            <div class="tab-pane active in" style="border:0; height:auto;" id="div_practice" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->practice['bitacora'])?'data-idbitacora='.$this->practice['bitacora']['idbitacora']:'' ?>>
                <div class="content-smartic metod" id="met-<?php echo @$this->practice["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->practice["met"]["idmetodologia"]; ?>" style="overflow: hidden;">
                    <div class="col-xs-12">
                        <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"])) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"])) ?>" data-id-metod="<?php echo @$this->practice["met"]["idmetodologia"]; ?>">
                            <?php if(!empty(@$this->practice['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            $num=0;
                            foreach ($this->practice['act']['det'] as $i=>$ejerc) { $num++;
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($num==1);
                                } ?>
                                <li class="<?php echo ($aquiSeQuedo)?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]) ).'-'.$num; ?>" data-toggle="tab"><?php echo $num; ?></a></li>
                            <?php } } ?>
                            <li class="hidden comodin"><!-- para que cuente correctamente la barra progreso --></li>
                        </ul>

                        <div class="col-xs-12 text-center numero_ejercicio" style="display: none">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Exercise')); ?></div>
                            <span class="actual"></span>
                            <span class="total"></span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="tab-content" id="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]) ) ?>-main-content">
                        <?php
                        if(!empty(@$this->practice['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            foreach (@$this->practice['act']['det'] as $i=>$ejerc) { 
                                $x=$i+1; 
                                $iddetalle=$ejerc["iddetalle"]; 
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($x==1);
                                } ?>
                            <div id="tab-<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]))."-".$x; ?>" class="tabhijo tab-pane fade <?php echo ($aquiSeQuedo)?'active in':''; ?>"  data-iddetalle="<?php echo  @$ejerc["iddetalle"]; ?>" >
                            <?php $html = $ejerc["texto_edit"];
                            if($this->rolActivo=='Alumno' && !empty($ejerc['desarrollo']) && !empty(@$ejerc['desarrollo']['html_solucion'])){
                                $html  = $ejerc['desarrollo']['html_solucion'];
                            }
                            echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$html);?>
                            </div>
                         <?php } } ?>
                        </div>
                    </div>
                    <div class="col-xs-3 hidden">
                        <div class="widget-box">
                            <div class="widget-header bg-red">
                                <h4 class="widget-title">
                                    <i class="fa fa-paint-brush"></i>
                                    <span><?php echo JrTexto::_('Skills'); ?></span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row habilidades">
                                        <div class="col-xs-12 ">
                                        <?php 
                                        $ihab=0;
                                        if(!empty($this->habilidades))
                                        foreach ($this->habilidades as $hab) { $ihab++;?>
                                            <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                        <?php } ?>                                  
                                        </div>
                                    </div> 
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div> 
            </div>
                       
        </div>
    </div>
    
    <a href="#anclaDown" class="scroller down" id="scroll-down"><i class="fa fa-angle-double-down fa-2x"></i></a>
</div>
<a href="anclaDown" class="ancla" id="anclaDown"></a>


<div id="btns_progreso_intentos" style="display: none;">
    <div class="row" id="btns_control_alumno">
        <div class="col-sm-12 botones-control">
            <a class="btn btn-inline btn-blue btn-lg try-again"><i class="fa fa-undo"></i> <?php echo ucfirst(JrTexto::_('try again')); ?></a>
            <a class="btn btn-inline btn-success btn-lg save-progreso" ><?php echo ucfirst(JrTexto::_('continue')) ?> <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
</div>

<section class="hidden" id="msjes_idioma" style="display: none;">
    <input type="hidden" id="attention" value="<?php echo JrTexto::_('Attention');?>">
    <input type="hidden" id="no_puedes_arrastrar" value='<?php echo JrTexto::_('You can not drag another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="no_puedes_seleccionar" value='<?php echo JrTexto::_('You can not select another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="you_must_finish" value='<?php echo ucfirst(JrTexto::_('you must do the exercise before trying again')); ?>'>
    <input type="hidden" id="bien_hecho" value='<?php echo ucfirst(JrTexto::_('well done!')); ?>'>
    <input type="hidden" id="cargando" value='<?php echo ucfirst(JrTexto::_('Loading')); ?>'>
    <input type="hidden" id="ejerc_exito" value='<?php echo ucfirst(JrTexto::_('you have complete the exercise succesfully')); ?>.'>
    <input type="hidden" id="ir_sgte_ejerc" value='<?php echo ucfirst(JrTexto::_('continue to the next exercise')); ?>.'>
    <input type="hidden" id="finalizar" value='<?php echo ucfirst(JrTexto::_('you have finished the activity')); ?>.'>
    <input type="hidden" id="puedes_continuar" value='<?php echo ucfirst(JrTexto::_('you can continue next exercise')); ?>.'>
    <input type="hidden" id="tiempo_acabo" value='<?php echo ucfirst(JrTexto::_('time is over')); ?>.'>
    <input type="hidden" id="intentalo_otra_vez" value='<?php echo ucfirst(JrTexto::_('you can try again')); ?>.'>
    <input type="hidden" id="select_activity" value='<?php echo ucfirst(JrTexto::_('Select Activity')); ?>.'>
    <input type="hidden" id="select_upload" value='<?php echo JrTexto::_('Select or upload'); ?>'>
    <input type="hidden" id="click_add_description" value='<?php echo JrTexto::_('Click here to add description') ?>'>
    <input type="hidden" id="click_add_title" value='<?php echo JrTexto::_('Click here to add title') ?>'>
    <input type="hidden" id="remove_tab" value='<?php echo JrTexto::_('remove tab') ?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="no_hay_audio" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback')); ?>'>
    <input type="hidden" id="no_hay_audio_en_escena" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback in scene')); ?>'>
    <input type="hidden" id="no_hay_orden_en_escena" value='<?php echo ucfirst(JrTexto::_('you have not set an order in the dialogues in scene')); ?>'>
    <input type="hidden" id="debes_establecer_orden" value='<?php echo ucfirst(JrTexto::_('you must set an order for the dialog bubbles')); ?>'>
    <input type="hidden" id="write_tag" value='<?php echo ucfirst(JrTexto::_('write tag')); ?>'>
    <input type="hidden" id="ejerc_no_completado" value='<?php echo ucfirst(JrTexto::_('you have not finished editing this exercise')); ?>'>
    <input type="hidden" id="grabar_audio" value='<?php echo ucfirst(JrTexto::_('Record')); ?>'>
    <input type="hidden" id="detener_audio" value='<?php echo ucfirst(JrTexto::_('Stop')); ?>'>
    <input type="hidden" id="escribir_texto" value='<?php echo ucfirst(JrTexto::_('Write')); ?>'>
    <input type="hidden" id="reproducir_audio" value='<?php echo ucfirst(JrTexto::_('Play')); ?>'>
    <input type="hidden" id="end" value='<?php echo ucfirst(JrTexto::_('End')); ?>'>
</section>

<script type="text/javascript">
//document.getElementById('div_imagen').style.display="none";
var _IDHistorialSesion=0;

var pos=-1;
var posv=-1;
var showdiv=[];

function fileExists(url) {
    var rspta = false;
    if(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.send();
        rspta = (req.status==200);
    }
    return rspta;
}

function existeEnSlider(url, $panel){
    var rspta = true;
    if(url){
        var archivo = $panel.find('.myslider *[src="'+url+'"]');
        rspta = (archivo.length>0);
    }
    return rspta;
}

function insertarFileLista(){
    
}

var guardarTResource = function($tipo,$file,$sw){
    var data = new Array();
    //verificar estos datos
    data.idNivel = <?php echo !empty($this->idnivel)?$this->idnivel:0?>;
    data.idUnidad = <?php echo !empty($this->idunidad)?$this->idunidad:0?>;
    data.idActividad = <?php echo !empty( $this->idactividad)?$this->idactividad:0?>;
    data.tipo = $tipo;
    data.orden = <?php echo !empty($this->orden)?$this->orden:0; ?>;
   
     if ($tipo=='A'){
        data.texto= $('#txtTita').val();
        data.titulo= "";
        data.pos= $('#txtposa').val();
        data.caratula= $('#txtaudio').val();
        //alert(data.caratula);
        if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$('#txtDesca').val();
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTita').val(texto);
            data.texto= $('#txtTita').val();
            $("#video"+data.pos).attr("data-tit",$('#txtDesca').val());
            data.titulo= "";
            data.pos= "null";
        }
    }else if ($tipo=='D'){
        data.texto= $('#txtTitp').val();
        data.titulo= "";
        data.pos= $('#txtposp').val();
        data.caratula= $('#txtpdf').val();
        //alert(data.caratula);

        //if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$sw;
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }
            $('#txtTitp').val(texto);
            data.texto= $('#txtTitp').val();
            $("#pdf"+data.pos).attr("data-tit",$('#txtDescp').val());
            data.titulo= "";
            data.pos= "null";
        //}
        //retunr;
    }
    //alert($file);
    //data.pkIdlink= idpk||0;
    var res = xajax__('', 'Resources', 'saveResource', data);
    if(res){
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo ucfirst(JrTexto::_('saved successfully'));?>.', 'success');
    }
};

var showPrimerElemento = function ($contenedor) {
    if($contenedor.length==0){ return false; }
    $contenedor.find('.items-select-list .item-select').first().trigger('click');
    $contenedor.find('.items-select-list').removeClass('items-select-list');
}

var calcularProgresoMultimedia = function($tabPane){
    var porcentaje = 0.0;
    var numDivs=$tabPane.find('.myslider .miniatura').length;
    var numDivsVistos=$tabPane.find('.myslider .miniatura.visto').length;
    if(numDivs>0){
        porcentaje=(numDivsVistos/numDivs)*100;
    }
    return porcentaje;
};  

var totalPestanias = function(){
    /* Solo las pestañas (top o left) visibles en el smartbook */
    var cant = 0;
    $("#navTabsTop, #navTabsLeft").find('li>a').not('a[href="#div_presentacion"],a#exit').each(function(i, elem) {
        var isVisible = $(elem).hasClass('hidden') || $(elem).is(':visible')
        if(isVisible){ cant++; }
    });

    cant--; /* la pestaña div_games aun no se guarda, por loq  no se está considerando. 
                Esta linea deberá borrarse una vez implementado el guardar de juegos */

    return cant;
};

var guardarBitacora = function(dataSave, $tabPane){
    if($('#rolUsuario').val()!='Alumno'){ console.log('No es Alumno - No guardar'); return false; }
    $.ajax({
        url: _sysUrlBase_+'/bitacora_smartbook/guardarBitacora_smartbook',
        type: 'POST',
        dataType: 'json',
        data: dataSave,
    }).done(function(resp) {
        if(resp.code=='ok'){
            var idLogro = $('#idLogro').val();
            var idSesion = $('#idSesion').val();
            var tieneLogro = idLogro>0;
            $tabPane.attr('data-idbitacora', resp.newid);
            if(resp.progreso>=100 && tieneLogro && resp.bandera==0){
                mostrarLogroObtenido({'id_logro':idLogro, 'idrecurso': idSesion  });
            }
        }
    }).fail(function(e) {
        console.log("!Error", e);
    });
};

var show_hide_Scroller = function($tab=null) {
    if($tab===null) return false;
    var containerHeight = $tab.closest('div.tab-pane[data-tabgroup="navTabsLeft"]').outerHeight();
    var contentHeight = 0;
    $tab.children().each(function(index, el) {
        contentHeight += $(el).outerHeight();
    });
    if( !$('#contenedor-paneles').hasClass('hidden') ){ contentHeight += $('#contenedor-paneles').outerHeight()+25; }
    if(contentHeight > containerHeight){
        $('#scroll-down').show();
    }else{
        $('#scroll-down').hide();
    }
};

var contarNumeroEjercicio = function($tabPane) {
    var $pnlNroEjercicio = $tabPane.find('.numero_ejercicio');
    if($pnlNroEjercicio.length===0){ return false; }
    
    var total_txt = $tabPane.find('ul.nav.ejercicios li').not('.comodin').length;
    var $actualTab = $tabPane.find('ul.nav.ejercicios li.active');
    var actual_txt = $actualTab.find('a[data-toggle="tab"]').text();
    if($actualTab.length==0){ actual_txt = MSJES_PHP.end; total_txt=''; }
    $pnlNroEjercicio.find('.actual').text(actual_txt);
    $pnlNroEjercicio.find('.total').text(total_txt);
    $('.infouserejericio.ejeinfo-numero .actual').text(actual_txt);
    $('.infouserejericio.ejeinfo-numero .total').text(total_txt);
};

var initBitac_Alum_Smbook = function () {
    if( $('#idBitacoraAlumnoSmbook').val()!=='' || $('#rolUsuario').val()!='Alumno' ) { return false; }
    $.ajax({
        url: _sysUrlBase_ + '/bitacora_alumno_smartbook/xGuardar',
        type: 'POST',
        dataType: 'json',
        data: {
            'txtIdcurso' : $('#idCurso').val(),
            'txtIdsesion' : $('#idSesion').val(),
            'txtEstado' : 'P',
        },
    }).done(function(resp) {
        if(resp.code=='ok'){
            $('#idBitacoraAlumnoSmbook').val(resp.newid);
        }
    }).fail(function(err) {
        console.log("!Error", err);
    }).always(function() {});
};

var resetearEjercicio_=function(aquiplantilla,tipo){
    let idmet=aquiplantilla.closest('.metod').attr('data-idmet');
    let plantilla=aquiplantilla.find('.plantilla'); 
    if(idmet==3){
        $('.infouserejericio.ejeinfo-tiempo').show().css({'opacity':1}).removeClass('hidden');
        $('.infouserejericio.ejeinfo-intento').show().css({'opacity':1}).removeClass('hidden');
        if(!plantilla.hasClass('yalointento')){
            $('.btnejercicio-next_').attr('disabled',true).addClass('disabled');           
            resetAllplantilla(plantilla);
        }else{
            $('.btnejercicio-next_').removeAttr('disabled').removeClass('disabled');
        }
        let intento_actual=parseInt(plantilla.attr('data-intento')||1);
        $('#panel-intentos-dby').find('.actual').text(intento_actual);
    }else{
        $('.infouserejericio.ejeinfo-tiempo').hide();
        $('.infouserejericio.ejeinfo-intento').hide();       
        $('.btnejercicio-next_').removeAttr('disabled').removeClass('disabled');
    }
    let pnlternativas=plantilla.find('.panelAlternativas');
    if(pnlternativas.length>0){
        txtlternativas=pnlternativas.text().replace(/\s||\n||\r/g,'');           
        if(txtlternativas=='') pnlternativas.parent().hide();
        else pnlternativas.css({'margin-bottom':'0px'});                    
    }       
    $('#btns_control_alumno .botones-control').css({'margin-top':'1.3em'});
}

/*var validartodosok=function(met,tabhijo){
    let plantilla=tabhijo.find('.plantilla');

}*/

$(document).ready(function(){
    $('.tab-pane[data-tabgroup="navTabsLeft"]').find('.nopreview').remove(); /*ejercicios y demás: quitar .nopreview*/
    $('#div_voca').find('.btnremovevoca').remove(); /*vocabulario: quitar .btnremovevoca*/
    setTimeout(function() {
        $( "#div_presentacion .div_acciones" ).fadeIn( "fast" );
    }, 2100);

    initBitac_Alum_Smbook();

    $('.btnejercicio-back_').on('click',function(ev){
        let tabPaneActivo=$('#div_practice.active');
        if(tabPaneActivo.length==0) tabPaneActivo=$('#div_autoevaluar.active');
        let tabejercicioactivo=tabPaneActivo.find('ul.ejercicios').children('li.active');
        let indexhijo=tabejercicioactivo.index();
        $('.btnejercicio-next_').removeClass('hide');
        if(indexhijo==0){
            $(this).addClass('hide');           
        }else{            
            tabejercicioactivo.prev('li').children('a').trigger('click');
        }
    })

    $('.btnejercicio-next_').on('click',function(ev){
        $('#panel-tiempo .info-show').trigger('oncropausar');
        guardarProgreso();
        $(this).addClass('hide');
        let tabPaneActivo=$('#div_practice.active');
        if(tabPaneActivo.length==0) tabPaneActivo=$('#div_autoevaluar.active');
        let progreso = parseFloat( tabPaneActivo.find('.metod').attr('data-progreso'));
        guardarBitacora({
            'Idbitacora' : tabPaneActivo.attr('data-idbitacora')||null,
            'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val(),
            'txtIdcurso' : $('#idCurso').val(),
            'txtIdsesion' : $('#idSesion').val(),
            'txtPestania' : tabPaneActivo.attr('id'),
            'txtTotal_Pestanias' : totalPestanias(),
            'txtProgreso' : progreso.toFixed(2),
            'txtOtros_datos' : null,
            'txtIdlogro' : $('#idLogro').val(),
        }, tabPaneActivo); 
            
        let tabactivo=tabPaneActivo.children('.tabhijo.active');
        let tabejercicioactivo=tabPaneActivo.find('ul.ejercicios').children('li.active');
        let indexhijo=tabejercicioactivo.index();
        let totalhijos=tabPaneActivo.find('ul.ejercicios').find('li').length-2;
        $('.btnejercicio-back_').removeClass('hide');
        if(indexhijo==totalhijos){           
            $(this).addClass('hide');            
        }else{
            tabejercicioactivo.next('li').children('a').trigger('click');
            $(this).removeClass('hide');
        } 
        
    })

    $('#smartbook-ver .nav-tabs').on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {        
        var $actualTab = $(this);
        var idTabPane = $actualTab.attr('href');
        var $tabpane = $(idTabPane);
        var $slider = $tabpane.find('.myslider');
        var idNavTab = $actualTab.closest('.nav-tabs').attr('id');
        setTimeout(function() { show_hide_Scroller($tabpane); }, 500);
        contarNumeroEjercicio($tabpane);

        if( $slider.length>0 && !$slider.hasClass('slick-initialized')){
            $slider.slick(optSlike);
        }
        showPrimerElemento($tabpane);
        $('audio').each(function(){ this.pause() });
        $('video').each(function(){ this.pause() });
        if( idNavTab==='navTabsTop' || idNavTab==='navTabsLeft' ){
            console.log(1);
            $('#smartbook-ver #navTabsTop a[data-toggle="tab"], #smartbook-ver #navTabsLeft a[data-toggle="tab"]').not($actualTab).closest('li.active').removeClass('active');            
            if(idTabPane==="#div_look" || idTabPane==="#div_practice" || idTabPane==="#div_autoevaluar"){ 

                $('.tab-pane .content-smartic.metod.active').removeClass('active');
                $tabpane.find('.content-smartic.metod').addClass('active');
                if(!$tabpane.hasClass('smartic-initialized')){
                    $tabpane.tplcompletar({editando:false});
                }
                rinittemplatealshow();
                if(idTabPane==="#div_look"){
                   $('#contenedor-paneles').addClass('hidden');  // oculta el progreso;
                }else if(idTabPane==="#div_practice"){
                    $('#contenedor-paneles').removeClass('hidden'); // muestra progreso
                    let panelActivo=$('#div_practice').find('.tabhijo.active');
                    resetearEjercicio_(panelActivo);
                }else{
                    $('#contenedor-paneles').removeClass('hidden'); // muestra progreso
                    let panelActivo=$('#div_autoevaluar').find('.tabhijo.active');                    
                    resetearEjercicio_(panelActivo);
                }//
            }else{
                $('#contenedor-paneles').addClass('hidden'); // muestra el progreso
            }

            if(idNavTab==='navTabsLeft' && (idTabPane==="#div_look" || idTabPane==="#div_voca" || idTabPane==="#div_workbook")){
                var $tabPane = $(idTabPane);
                var progreso = 100.00;
                guardarBitacora({
                    'Idbitacora' : $tabPane.attr('data-idbitacora')||null,
                    'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val()||null,
                    'txtIdcurso' : $('#idCurso').val(),
                    'txtIdsesion' : $('#idSesion').val(),
                    'txtPestania' : $tabPane.attr('id'),
                    'txtTotal_Pestanias' : totalPestanias(),
                    'txtProgreso' : progreso.toFixed(2),
                    'txtOtros_datos' : null,
                    'txtIdlogro' : $('#idLogro').val(),
                }, $tabPane);
            }
        }
    });

    $('#smartbook-ver ul.nav.ejercicios').on('shown.bs.tab', 'a[data-toggle="tab"]', function(ev) {
        ev.preventDefault();
        let idTab = $(this).attr('href');
        let $tabPane = $(idTab).closest('div[data-tabgroup="navTabsLeft"]');
        contarNumeroEjercicio($tabPane);
        let $tabhijoActive=$tabPane.find('.tabhijo.active');        
        let totalpuntaje=100/$tabPane.find('.tabhijo').length; // total puntaje por ejericio.        
        resetearEjercicio_($tabhijoActive);        
    })

    $('#div_practice, #div_autoevaluar').on('click', '#btns_control_alumno .save-progreso', function(e) {
        e.preventDefault();
        var $tabPane = $(this).closest('[data-tabgroup]');
        var progreso = parseFloat( $tabPane.find('.metod').attr('data-progreso') );
        guardarBitacora({
            'Idbitacora' : $tabPane.attr('data-idbitacora')||null,
            'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val(),
            'txtIdcurso' : $('#idCurso').val(),
            'txtIdsesion' : $('#idSesion').val(),
            'txtPestania' : $tabPane.attr('id'),
            'txtTotal_Pestanias' : totalPestanias(),
            'txtProgreso' : progreso.toFixed(2),
            'txtOtros_datos' : null,
            'txtIdlogro' : $('#idLogro').val(),
        }, $tabPane);
    });

    $('#smartbook-ver .preview').click(function(){
        $(this).hide();
        $(this).siblings('.back').show();
        $('#navTabsLeft li a[data-toggle="tab"]').first().trigger('click');
        $('.controles-edicion').hide();
        $('#smartbook-ver .descripcion-media').attr('readonly','readonly');  
        $('#smartbook-ver .nopreview').hide();
        $('#smartbook-ver').removeClass('editando');
    });

    $('#smartbook-ver .back').click(function(){
        $(this).hide();
        $(this).siblings('.preview').show();
        $('.controles-edicion').show();
        $('#smartbook-ver .descripcion-media').removeAttr('readonly');
        $('#smartbook-ver .nopreview').show();
        $('#smartbook-ver').addClass('editando');
    });

    var iScrollPos = 0;
    $('#smartbook-ver #tabContent > .tab-pane').scroll(function(e) {
        var iCurScrollPos = $(this).scrollTop();
        var tabPaneHeight = $(this).outerHeight();
        var contentHeight = 0;

        $(this).children().each(function(index, el) {
            contentHeight += $(el).outerHeight(); //+71;
        });

        if( !$('#contenedor-paneles').hasClass('hidden') ){ contentHeight += $('#contenedor-paneles').outerHeight()-3; }

        if (iCurScrollPos > iScrollPos) {
            $('#scroll-up').show();
            /*console.log(iCurScrollPos+' + '+tabPaneHeight+' +10= ', iCurScrollPos + tabPaneHeight+10);
            console.log('contentHeight = ', contentHeight);*/
            if(iCurScrollPos + tabPaneHeight-10 >= contentHeight) { /* LLego al final?*/
                $('#scroll-down').hide();
            }else {
                $('#scroll-down').show();
            }
        } else {
            $('#scroll-down').show();
            if(iCurScrollPos==0){ /* LLego al inicio?*/
                $('#scroll-up').hide();
            }else {
                $('#scroll-up').show();
            }
        }
        iScrollPos = iCurScrollPos;
    });

    $('#scroll-down').click(function(e) {
        //e.preventDefault();
        $('#smartbook-ver #tabContent > .tab-pane.active').animate({ scrollTop: 750}, 1200, 'linear');

    });
    $('#scroll-up').click(function(e) {
        //e.preventDefault();
        $('#smartbook-ver #tabContent > .tab-pane.active').animate({ scrollTop: -750}, 1200, 'linear');
    });

/*** #Fn generales(Pdf,Aud,Img,Vid) ***/
  

<?php if(strtolower($this->rolActivo)=='alumno'){?>
    $('#smartbook-ver .preview').trigger('click');
    $('#smartbook-ver .preview').hide();
    $('#smartbook-ver .back').hide();
<?php } ?>

    $('#smartbook-ver .plantilla-speach').each(function(index, el) {
        initspeach( $(el) );
        if($(el).closest('.tabhijo').hasClass('active')){
            rinittemplatealshow();
        }
    });

/******************************************************/
    var registrarHistorialSesion = function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/agregar',
            type: 'POST',
            dataType: 'json',
            data: {'lugar': 'TR', 'fechaentrada': fechahora, 'idcurso': $('#idCurso').val() },
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
        });
        
    };

    registrarHistorialSesion();
    $(window).on('beforeunload', function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/editar',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {'idhistorialsesion': _IDHistorialSesion, 'fechasalida': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            return false;
        });
    });
    console.log($('ul.navTabsLeft a[href="#div_practice"]'));
    $('ul#navTabsLeft a[href="#div_practice"]').trigger('click');
});

</script>