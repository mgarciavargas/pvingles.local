<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">

<?php if(!$ismodal){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_('local'); ?></li>       
    </ol>
</div> </div>
<?php } ?>


<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">                                        
            <!--div class="col-xs-6 col-sm-4 col-md-4 select-ctrl-wrapper select-azul" >
              <select name="cbidugel" id="cbidugel" class="form-control select-ctrl">
                  <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                  <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                  <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>
              </select>
            </div-->
            <div class="col-xs-6 col-sm-4 col-md-4 select-ctrl-wrapper select-azul">
              <select id="fkcbugel" name="fkcbugel" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('All ugel'); ?></option>               
                  <?php 
                    if(!empty($this->ugeles))
                      foreach ($this->ugeles as $ugel) { ?><option value="<?php echo $ugel["idugel"]; ?>" <?php echo $this->idugel===$ugel["idugel"]?'selected="selected"':''?>> <?php echo $ugel["descripcion"]?> </option><?php } ?>                        
              </select>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-4 select-ctrl-wrapper select-azul">
              <select id="fkcbtipo" name="fkcbtipo" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('All type'); ?></option>
                  <?php 
                    if(!empty($this->fktipo))
                      foreach ($this->fktipo as $fktipo) { ?><option value="<?php echo $fktipo["codigo"]?>" <?php echo $fktipo["codigo"]==@$frm["tipo"]?"selected":""; ?> ><?php echo $fktipo["nombre"] ?></option><?php } ?>                        
              </select>
            </div> 
                            
            <div class="col-xs-6 col-sm-6 col-md-8">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Local", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Local").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Local") ;?></th>
                    <th><?php echo JrTexto::_("Address") ;?></th>
                    <th><?php echo JrTexto::_("Ugel");?></th>
                    <th><?php echo JrTexto::_("Type"); ?></th>
                    <th><?php echo JrTexto::_("Vacantes") ;?></th>
                    <!--th><?php echo JrTexto::_("Idugel") ;?></th-->
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5a45076130b6a='';
function refreshdatos5a45076130b6a(){
    tabledatos5a45076130b6a.ajax.reload();
}
$(document).ready(function(){  
  var estados5a45076130b6a={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5a45076130b6a='<?php echo ucfirst(JrTexto::_("local"))." - ".JrTexto::_("edit"); ?>';
  var draw5a45076130b6a=0;

  
  $('#fkcbtipo').change(function(ev){
    refreshdatos5a45076130b6a();
  });
  $('#fkcbugel').change(function(ev){
    refreshdatos5a45076130b6a();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos5a45076130b6a();
  });
  tabledatos5a45076130b6a=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Local") ;?>'},
        {'data': '<?php echo JrTexto::_("Address") ;?>'},
        {'data': '<?php echo JrTexto::_("Ugel") ;?>'},
        {'data': '<?php echo JrTexto::_("Type") ;?>'},
        {'data': '<?php echo JrTexto::_("Vacantes") ;?>'},
        //{'data': '<?php echo JrTexto::_("Idugel") ;?>'},            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/local/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.tipo=$('#fkcbtipo').val(),
            d.idugel=$('#fkcbugel').val(),
             //d.texto=$('#texto').val(),
                        
            draw5a45076130b6a=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5a45076130b6a;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Local") ;?>': data[i].nombre,
              '<?php echo JrTexto::_("Address") ;?>': data[i].direccion,
              '<?php echo JrTexto::_("Ugel") ;?>': data[i].ugel,
              '<?php echo JrTexto::_("Type") ;?>': data[i].strtipolocal||'',
              '<?php echo JrTexto::_("Vacantes") ;?>': data[i].vacantes,
              //'<?php echo JrTexto::_("Idugel") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="idugel"  data-id="'+data[i].idlocal+'"> <i class="fa fa'+(data[i].idugel=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5a45076130b6a[data[i].idugel]+'</a>',
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/local/editar/?id='+data[i].idlocal+'" data-titulo="'+tituloedit5a45076130b6a+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idlocal+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'local', 'setCampo', id,campo,data);
          if(res) tabledatos5a45076130b6a.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5a45076130b6a';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Local';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid,{header:true,footer:false,borrar:true}); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'local', 'eliminar', id);
        if(res) tabledatos5a45076130b6a.ajax.reload();
      }
    }); 
  });
});
</script>