<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
  .form-group{
    margin-bottom: 0px; 
  }
  .input-group {
    margin-top: 0px;
  }
  select.select-ctrl, .form-control , .input-group-addon{   
    border: 1px solid #4683af;
    margin-bottom: 1ex;
  }
  .select-ctrl-wrapper:after{
    right: 15px;
  }
</style>
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Ubigeo"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">                                        
            <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
              <select id="fkcbpais" name="fkcbpais" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione Pais'); ?></option>
                <?php if(!empty($this->fkpais))
                    foreach ($this->fkpais as $fkpais) { ?><option value="<?php echo $fkpais["pais"]?>" <?php echo $fkpais["pais"]==@$this->idpais?"selected":""; ?>><?php echo ucfirst($fkpais["ciudad"]); ?></option>
                	<?php } ?>                        
              </select>
            </div>                                                    
            <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
              <select name="cbdepartamento" id="cbdepartamento" class="form-control select-ctrl">
              	 <option value=""><?php echo JrTexto::_('Seleccione Departamento'); ?></option>
                <?php if(!empty($this->fkdepartamento))
                    foreach ($this->fkdepartamento as $fkdepa) { ?><option value="<?php echo $fkdepa["departamento"]?>" <?php echo $fkdepa["departamento"]==@$this->iddepa?"selected":""; ?>><?php echo ucfirst($fkdepa["ciudad"]); ?></option>
                	<?php } ?>
              </select>
            </div>                                                  
            <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
              <select name="cbprovincia" id="cbprovincia" class="form-control select-ctrl">
              	 <option value=""><?php echo JrTexto::_('Seleccione Provincia'); ?></option>
                <?php if(!empty($this->fkprovincia))
                    foreach ($this->fkprovincia as $fkpro) { ?><option value="<?php echo $fkpro["provincia"]?>" <?php echo $fkpro["provincia"]==@$this->idpro?"selected":""; ?>><?php echo ucfirst($fkpro["ciudad"]); ?></option>
                	<?php } ?>
              </select>
            </div>                                                            
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Ubigeo", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Ubigeo").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
            </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Ciudad") ;?></th>
                  <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5a459bea67036='';
function refreshdatos5a459bea67036(){
    tabledatos5a459bea67036.ajax.reload();
}
$(document).ready(function(){  
  var estados5a459bea67036={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5a459bea67036='<?php echo ucfirst(JrTexto::_("ubigeo"))." - ".JrTexto::_("edit"); ?>';
  var draw5a459bea67036=0;

  var cargardatos5a459bea67036=function(obj,data,returnobj){
      var midata=data||null;  
      $.ajax({
        url: _sysUrlBase_+'/ubigeo/buscarjson/?json=true',
        type: 'POST',
        dataType: 'json',
        data: midata,
      }).done(function(resp){
        if(resp.code=='ok'){                  
            $('option:first',obj).siblings().remove();
            if(resp.data!=''){
              	$.each(resp.data,function(i,v){
               		obj.append('<option value="'+v[returnobj]+'">'+v["ciudad"]+'</option>');
            	});
            }
            if(returnobj=='departamento'){
            	$cbdepa=$('#cbdepartamento');
            	var copt=$('option',$cbdepa).length;
            	if(copt>1)
            		$('option',$cbdepa).eq(1).prop('selected',true);
                $('#cbdepartamento').trigger('change');
                return;
            }
            if(returnobj=='provincia'){
            	$cbpro=$('#cbprovincia');
            	var copt=$('option',$cbpro).length;
            	if(copt>1)
            		$('option',$cbpro).eq(1).prop('selected',true);
                $('#cbprovincia').trigger('change');
                return;
            }
            refreshdatos5a459bea67036();
        }else{              
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
      }).fail(function(xhr, textStatus, errorThrown) {        
          console.log(xhr.responseText);
      });
    }
  
  $('#fkcbpais').change(function(ev){
  	var d={};
    d.pais=$('#fkcbpais').val();
    d.return='departamento';
    if(d.pais==''){
        $('#cbdepartamento option:first').siblings().remove();
        $('#cbprovincia option:first').siblings().remove();
        refreshdatos5a459bea67036();
    }else{
        cargardatos5a459bea67036($('#cbdepartamento'),d,'departamento');     
    }
  });
  $('#cbdepartamento').change(function(ev){
    var d={};
    d.pais=$('#fkcbpais').val();
    d.departamento=$('#cbdepartamento').val();
    d.return='provincia';
    if(d.departamento==''){
        $('#cbprovincia option:first').siblings().remove();
        refreshdatos5a459bea67036();
    }else{
        cargardatos5a459bea67036($('#cbprovincia'),d,'provincia');     
    }
  });
  $('#cbprovincia').change(function(ev){
    refreshdatos5a459bea67036();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos5a459bea67036();
  });
  tabledatos5a459bea67036=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        {'data': '<?php echo JrTexto::_("Ciudad") ;?>'},
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/ubigeo/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.pais=$('#fkcbpais').val(),
            d.departamento=$('#cbdepartamento').val(),
            d.provincia=$('#cbprovincia').val(),
            //d.texto=$('#texto').val(),
            draw5a459bea67036=d.draw;
            // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5a459bea67036;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),                                        
              '<?php echo JrTexto::_("Ciudad") ;?>': data[i].ciudad,              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/ubigeo/editar/?id='+data[i].id_ubigeo+'" data-titulo="'+tituloedit5a459bea67036+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].id_ubigeo+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'ubigeo', 'setCampo', id,campo,data);
          if(res) tabledatos5a459bea67036.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5a459bea67036';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Ubigeo';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'ubigeo', 'eliminar', id);
        if(res) tabledatos5a459bea67036.ajax.reload();
      }
    }); 
  });
});
</script>