<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$grupo=!empty($this->datos)?$this->datos:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .titulo{
    font-weight: bold;
    font-size: 1.3em;
  }
  hr{
    margin-top: 0px;
    margin-bottom: 1ex;
    border: 0;
    border-top: 1px solid #00BCD4;
  }
  .datepicker {z-index: 9999! important}
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>     
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/acad_grupoaula">&nbsp;<?php echo JrTexto::_("Groups"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Edit"); ?></li>       
    </ol>
  </div>
</div>
<?php } ?>
<div class="row">
	<form id="frmcalendar<?php echo $idgui; ?>">
	<input type="hidden" name="plt" value="modal">
	<input type="hidden" name="idgrupoaula" value="<?php echo @$this->idgrupoaula; ?>">	
	<input type="hidden" name="iddocente" value="<?php echo @$this->fkdocente;?>">
	<input type="hidden" name="idcurso" value="<?php echo @$this->fkcurso;?>">
	<input type="hidden" name="strdocente" value="<?php echo @$this->strdocente;?>">
	<input type="hidden" name="strcurso" value="<?php echo @$this->strcurso;?>">
	<input type="hidden" name="idlocal" value="<?php echo @$this->fklocal;?>">
	<input type="hidden" name="tipo" value="<?php echo @$this->fktipo;?>">	
	</form>
	<div class="col-xs-12 padding-0">
		<div class="panel" >      
	      	<div class="panel-body">
	    		<div id='calendar' class='calendar col-md-12'></div>
			</div>
		</div>
	</div>
</div>
<button class="hidden" id="recargarcalendario<?php echo $idgui; ?>"></button>
<script type="text/javascript">
$(document).ready(function(){
	var idioma = (_sysIdioma_=='ES')?'es':'en';
	var tmpfechas<?php echo $idgui; ?>=[];
	function newEvent(start,end){
		var clstmp=Date.now();
		var claseid='curso_'+clstmp;
		var enmodal=$(this).attr('data-modal')||'si';
	    var fcall=$(this).attr('data-fcall')||clstmp;
	    var titulo=$(this).attr('data-titulo')||'';
	    titulo='<?php echo JrTexto::_('Course'); ?>';
	    var tmpfrm=document.getElementById('frmcalendar<?php echo $idgui; ?>');
	    var formData = new FormData(tmpfrm);
	    formData.append('fechaini', start.format('YYYY-MM-DD'));
	    formData.append('fechafin', end.format('YYYY-MM-DD'));
	    formData.append('horaini', start.format('hh:mm a'));
	    formData.append('horafin', end.format('hh:mm a'));
	    formData.append('fcall', fcall);

		  var frmtmp={
			 datasend:formData,
			 method:'POST',url:_sysUrlBase_+'/acad_grupoaula/horariosedit/',
			 header:false,footer:false,borrarmodal:true
		  }
		if(enmodal=='no'){ return redir(frmtmp.url); }
		$('#recargarcalendario<?php echo $idgui; ?>').addClass('tmp'+clstmp);
	    $('#recargarcalendario<?php echo $idgui; ?>').on('returndata',function(ev){
	    	$('#recargarcalendario<?php echo $idgui; ?>').removeClass('tmp'+clstmp);
	    	var tmpdata=JSON.parse($('#recargarcalendario<?php echo $idgui; ?>').attr('data-return'));
	    	$('#recargarcalendario<?php echo $idgui; ?>').removeAttr('data-return');
	    	$('#idcurso<?php echo $idgui; ?>').val(tmpdata.idcurso);
	    	$('#idgrupoaula<?php echo $idgui; ?>').val(tmpdata.idgrupoaula);
	    	$('#iddocente<?php echo $idgui; ?>').val(tmpdata.iddocente);
	    	$('#idlocal<?php echo $idgui; ?>').val(tmpdata.idlocal);
	    	<?php if(!empty($ventanapadre)){?>

	    		var btnobj=$('.tmp<?php echo $ventanapadre; ?>');
	    		if(btnobj.length){
	    			btnobj.attr('data-return', JSON.stringify(tmpdata));
	    			btnobj.trigger('click');
	    		}
	    	<?php } ?>
	    })
		openModal('lg',titulo,frmtmp.url,true,claseid,frmtmp);	   
	}

	function editEvent(event){
    var clstmp=Date.now();
    var claseid='curso_'+clstmp;
    var enmodal=$(this).attr('data-modal')||'si';
      var fcall=$(this).attr('data-fcall')||clstmp;
      var titulo=$(this).attr('data-titulo')||'';
      titulo='<?php echo JrTexto::_('Course'); ?>';
      var tmpfrm=document.getElementById('frmcalendar<?php echo $idgui; ?>');
      var formData = new FormData(tmpfrm);
      formData.append('idhorario',event.id);
      formData.append('fechaini', moment(event.start).format('YYYY-MM-DD'));
      formData.append('fechafin', moment(event.end).format('YYYY-MM-DD'));
      formData.append('horaini', moment(event.start).format('hh:mm a'));
      formData.append('horafin', moment(event.end).format('hh:mm a'));
      formData.append('fcall', fcall);
      var frmtmp={
       datasend:formData,
       method:'POST',url:_sysUrlBase_+'/acad_grupoaula/horariosedit/',
       header:false,footer:false,borrarmodal:true//,tam:'90%'
      }
    if(enmodal=='no'){ return redir(frmtmp.url); }
    $('#recargarcalendario<?php echo $idgui; ?>').addClass('tmp'+clstmp);
      $('#recargarcalendario<?php echo $idgui; ?>').on('returndata',function(ev){
        $('#recargarcalendario<?php echo $idgui; ?>').removeClass('tmp'+clstmp);
        var tmpdata=JSON.parse($('#recargarcalendario<?php echo $idgui; ?>').attr('data-return'));
        $('#recargarcalendario<?php echo $idgui; ?>').removeAttr('data-return');
        $('#idcurso<?php echo $idgui; ?>').val(tmpdata.idcurso);
    	$('#idgrupoaula<?php echo $idgui; ?>').val(tmpdata.idgrupoaula);
    	$('#iddocente<?php echo $idgui; ?>').val(tmpdata.iddocente);
    	$('#idlocal<?php echo $idgui; ?>').val(tmpdata.idlocal);

        <?php if(!empty($ventanapadre)){?>
        	
    		var btnobj=$('.tmp<?php echo $ventanapadre; ?>');

    		if(btnobj.length){
    			btnobj.attr('data-return', JSON.stringify(tmpdata));
    			btnobj.trigger('click');
    		}
    	<?php } ?>

      })
    openModal('lg',titulo,frmtmp.url,true,claseid,frmtmp);
		//editEvent(ev);
	}
	function cal1GoTo(date){
		$cal1=$('#calendar');
		$cal1.fullCalendar('gotoDate', date);
	}

    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay,agendaWeek,agendaDay,listWeek,listDay'
      },
      views: {
      	listDay: { buttonText: 'list day' },
      	agendaWeek: { buttonText: 'Agenda Week' },
        listDay: { buttonText: 'list day' },
        agendaDay: { buttonText: 'Agenda day' },
        listWeek: { buttonText: 'list week' }
      },
      locale: idioma, 
      selectable: true,
      selectHelper: true,     
      defaultView: 'agendaWeek',
      slotDuration: '00:10:00',
      navLinks: true, // can click day/week names to navigate views
      editable: false,
      select: function(start, end) {
          newEvent(start,end);
      },
      eventClick: function(event, jsEvent, view) {
        var newEvent={
          id:event.id,
          title:event.title,
          start:event.start.format('YYYY-MM-DD hh:mm a'),
          end:event.end.format('YYYY-MM-DD hh:mm a'),
          color:event.color
        }
        //console.log(newEvent);
        editEvent(newEvent);
      },
      dayClick: function(date) {
          cal1GoTo(date);
      },
      events:[
      	<?php if(!empty($this->horarios))
      	  foreach($this->horarios as $hr){
      	  	echo $hr;
      	 } ?>			
		]
     });
  });
</script>