<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Material delivery"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">                                                                    
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Manuales", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Manuales").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>

               <a class="btn btn-warning btnvermodal" data-modal="no" href="<?php echo JrAplicacion::getUrlBase();?>/alumno/entregamaterial" data-titulo="<?php echo JrTexto::_('Deliver material')?>"><i class="fa fa-handshake-o"></i> <?php echo JrTexto::_('Deliver material')?></a>
            </div>
          </div>
        </div>
    </div>
  </div>
	 <div class="col-md-12 col-sm-12 col-xs-12" id="pnl<?php echo $idgui ?>">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Titulo") ;?></th>
                  <th><?php echo JrTexto::_("Abreviado") ;?></th>
                  <th><?php echo JrTexto::_("Stock") ;?></th>
                  <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5a5c3b30a50d9='';
function refreshdatos5a5c3b30a50d9(){
    tabledatos5a5c3b30a50d9.ajax.reload();
}
$(document).ready(function(){  
  var estados5a5c3b30a50d9={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5a5c3b30a50d9='<?php echo ucfirst(JrTexto::_("manuales"))." - ".JrTexto::_("edit"); ?>';
  var draw5a5c3b30a50d9=0;

  
  $('.btnbuscar').click(function(ev){
    refreshdatos5a5c3b30a50d9();
  });
  tabledatos5a5c3b30a50d9=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Titulo") ;?>'},
        {'data': '<?php echo JrTexto::_("Abreviado") ;?>'},
        {'data': '<?php echo JrTexto::_("Stock") ;?>'},            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/manuales/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             //d.texto=$('#texto').val(),
                        
            draw5a5c3b30a50d9=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5a5c3b30a50d9;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Titulo") ;?>': data[i].titulo,
              '<?php echo JrTexto::_("Abreviado") ;?>': data[i].abreviado,
              '<?php echo JrTexto::_("Stock") ;?>': data[i].stock+' <?php echo JrTexto::_('of'); ?> '+data[i].total,
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a title="<?php echo JrTexto::_('Edit')?>" class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/manuales/editar/?id='+data[i].idmanual+'" data-titulo="'+tituloedit5a5c3b30a50d9+'"><i class="fa fa-edit"></i></a> <a class="btnvermodal" data-modal="no" title="<?php echo JrTexto::_('Deliver material')?>" data-id="'+data[i].idmanual+'" href="'+_sysUrlBase_+'/alumno/entregamaterial/?idmanual='+data[i].idmanual+'"><i class="fa fa-handshake-o"></i></a> <a title="<?php echo JrTexto::_('delete')?>" class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idmanual+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'manuales', 'setCampo', id,campo,data);
          if(res) tabledatos5a5c3b30a50d9.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5a5c3b30a50d9';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Manuales';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    console.log('aaa')
    openModal('lg',titulo,url,true,claseid,{header:true,footer:false,borrarmodal:true}); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'manuales', 'eliminar', id);
        if(res) tabledatos5a5c3b30a50d9.ajax.reload();
      }
    }); 
  });
});
</script>