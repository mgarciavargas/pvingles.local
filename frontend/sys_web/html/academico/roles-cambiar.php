<style type="text/css">
	.pnlrol{
		display: inline-block;
	    width: 250px;	   
	    border: 1px solid #ddd;
	    cursor: pointer;
	}
	.img-responsive{
		display: inline-block;
	}
	.titulo00{
		color:rgb(165, 42, 42);
		margin-top: 10%;
		padding: 1.5ex;
	}
	.titulo{
		color:rgb(165, 42, 42);
		padding: 1ex;
	}

</style>
<div class="container">
	<div class="row text-center">
		<div class="titulo00"><h2><?php echo JrTexto::_('Select a role'); ?> </h2></div>
		<?php
		 if(!empty($this->roles))
		 	$nrol=count($this->roles);
		 foreach ($this->roles as $rolk => $rol){?>
		<div class="pnlrol hvr-float-shadow" data-idrol="<?php echo $rol["idrol"] ?>" data-rol="<?php echo $rol["rol"]; ?>" >
			<div class="imagen">
				<?php 
				$srcimg_=$this->documento->getUrlStatic().'/media/imagenes/rol_'.strtolower($rol["rol"]).".jpg";
				//$srcimg=is_file($srcimg_)?$srcimg_:($this->documento->getUrlStatic().'/media/imagenes/rol_default.jpg');
				$srcimg=$srcimg_;
				?>
				<img src="<?php echo $srcimg; ?>" class="img-responsive">
			</div>
			<div class="titulo"><?php echo $rol["rol"];?></div>
		</div>		 	
		 <?php } ?>
	</div>
</div>
<script type="text/javascript">
	$('.pnlrol').click(function(ev){
		window.location.href=_sysUrlBase_+'/sesion/cambiar_ambito/?idrol='+$(this).attr('data-idrol')+'&rol='+$(this).attr('data-rol');
	})
</script>