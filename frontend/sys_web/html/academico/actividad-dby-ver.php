<?php defined('RUTA_BASE') or die(); 
    $idgui = uniqid(); 
    $rutabase = $this->documento->getUrlBase();
?>
<style type="text/css">
    #smartbook-ver #cuaderno #tabContent .numero_ejercicio {
    color: green;
    font-style: italic;
    font-weight: bolder;
    }

    #smartbook-ver #cuaderno #tabContent .numero_ejercicio .titulo {
        font-size: 0.8em;
    }
    #smartbook-ver #cuaderno #tabContent .numero_ejercicio .actual {
        font-size: 1.5em;
    }

    #smartbook-ver #cuaderno #tabContent .numero_ejercicio .total:before {
        content: " / ";
    }
    #smartbook-ver #contenedor-paneles #panel-barraprogreso .progress {
        margin:1ex;
        background: rgb(222, 52, 52);
    }
    .plantilla .botones-control {
        margin-top: 1ex;
        text-align: right;
    }
    #smartbook-ver #contenedor-paneles .eje-panelinfo {
        margin-left: 0.55ex;
        text-align: center;
        min-width: 80px;
        float: left;
    }
    #panel-intentos-dby {
        background: #fdfdfd;
        border: 1px solid #5a89f8;
        box-shadow: inset 0px 0px 5px #5a89f8;
        color: #5a89f8;
    }
    .panel-barraprogreso{
        width: 80%;
    }
</style>
<div class="container" >
    <div id="smartbook-ver" class="editando row" data-idautor="<?php echo @$voc['idpersonal']; ?>">
       <input type="hidden" id="rolUsuario" value="<?php echo @$this->rolActivo; ?>">
        <input type="hidden" id="idCurso" value="<?php echo @$this->idCurso; ?>">
        <input type="hidden" id="idSesion" value="<?php echo @$this->sesion['idnivel']; ?>">
        <input type="hidden" id="idBitacoraAlumnoSmbook" value="<?php echo !empty($this->bitac_alum_smbook)? @$this->bitac_alum_smbook['idbitacora_smartbook']:'';?>">
        <input type="hidden" id="idLogro" value="<?php echo @$this->idLogro; ?>">
        <div class="preview"></div>
        <div id="cuaderno" class="tabbable col-xs-12 padding-0">
        <ul id="navTabsLeft" class="nav nav-tabs metodologias hidden">           
            <li class="hvr-bounce-in"><a class="active" href="#div_autoevaluar" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Do it by yourself")) ?></a></li> 
        </ul>
        <div id="tabContent" class="tab-content">
            <div id="contenedor-paneles">
                <div id="panel-barraprogreso" class="text-center eje-panelinfo" style="width: 80%">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            <span>0</span>%
                        </div>
                    </div>
                </div>
                <div id="panel-tiempo" class="eje-panelinfo pull-right">
                    <div class="titulo btn-yellow"><?php echo ucfirst(JrTexto::_('time')); ?></div>
                    <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50" style="display: none;">
                    <div class="info-show">00</div>
                </div>
                <div id="panel-intentos-dby" class="eje-panelinfo text-center pull-right">
                    <div class="titulo btn-blue"><?php echo ucfirst(JrTexto::_('attempt')); ?></div>
                    <span class="actual">1</span>
                    <span class="total"><?php echo @$this->intentos; ?></span>
                </div> 
            </div>
            <div class="tab-pane in" id="div_autoevaluar" data-tabgroup="navTabsLeft">
                <div class="content-smartic metod" id="met-<?php echo @$this->dby["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->dby["met"]["idmetodologia"]; ?>" style="overflow: hidden;">
                    <div class="col-xs-12">
                        <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"])) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"])) ?>" data-id-metod="<?php echo @$this->dby["met"]["idmetodologia"]; ?>">
                            <?php if(!empty(@$this->dby['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            $num=0;
                            foreach ($this->dby['act']['det'] as $i=>$ejerc) { $num++;
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo  = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($num==1);
                                } ?>
                                <li class="<?php echo ($aquiSeQuedo)?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]) ).'-'.$num; ?>" data-toggle="tab"><?php echo $num; ?></a></li>
                            <?php } } ?>
                            <li class="hidden comodin"><!-- tab obligado para calcular barra progreso--></li>
                        </ul>

                        <div class="col-xs-12 text-center numero_ejercicio">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Exercise')); ?></div>
                            <span class="actual"></span>
                            <span class="total"></span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="tab-content" id="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]) ) ?>-main-content">
                        <?php 
                        if(!empty(@$this->dby['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            foreach (@$this->dby['act']['det'] as $i=>$ejerc) { 
                                $x=$i+1; 
                                $iddetalle=$ejerc["iddetalle"]; 
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($x==1);
                                } ?>
                            <div id="tab-<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]))."-".$x; ?>" class="tabhijo tab-pane fade <?php echo ($aquiSeQuedo)?'active in':''; ?>" data-iddetalle="<?php echo  @$ejerc["iddetalle"]; ?>">
                            <?php  $html = $ejerc["texto_edit"];
                            if($this->rolActivo=='Alumno' && !empty($ejerc['desarrollo']) && !empty(@$ejerc['desarrollo']['html_solucion'])){
                                $html  = $ejerc['desarrollo']['html_solucion'];
                            }
                            echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$html);?>
                            </div>
                         <?php } } ?>
                        </div>
                    </div>
                    <div class="col-xs-3 hidden">
                        <div class="widget-box">
                            <div class="widget-header bg-red">
                                <h4 class="widget-title">
                                    <i class="fa fa-paint-brush"></i>
                                    <span><?php echo JrTexto::_('Skills'); ?></span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row habilidades">
                                        <div class="col-xs-12 ">
                                        <?php 
                                        $ihab=0;
                                        if(!empty($this->habilidades))
                                        foreach ($this->habilidades as $hab) { $ihab++;?>
                                            <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                        <?php } ?>                                  
                                        </div>
                                    </div> 
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    <div id="btns_progreso_intentos" style="display: none;">
        <div class="row" id="btns_control_alumno">
            <div class="col-sm-12 botones-control">
                <a class="btn btn-inline btn-blue btn-lg try-again"><i class="fa fa-undo"></i> <?php echo ucfirst(JrTexto::_('try again')); ?></a>
                <a class="btn btn-inline btn-success btn-lg save-progreso" ><?php echo ucfirst(JrTexto::_('continue')) ?> <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </div>   
</div>
<section class="hidden" id="msjes_idioma" style="display: none;">
    <input type="hidden" id="attention" value="<?php echo JrTexto::_('Attention');?>">
    <input type="hidden" id="no_puedes_arrastrar" value='<?php echo JrTexto::_('You can not drag another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="no_puedes_seleccionar" value='<?php echo JrTexto::_('You can not select another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="you_must_finish" value='<?php echo ucfirst(JrTexto::_('you must do the exercise before trying again')); ?>'>
    <input type="hidden" id="bien_hecho" value='<?php echo ucfirst(JrTexto::_('well done!')); ?>'>
    <input type="hidden" id="cargando" value='<?php echo ucfirst(JrTexto::_('Loading')); ?>'>
    <input type="hidden" id="ejerc_exito" value='<?php echo ucfirst(JrTexto::_('you have complete the exercise succesfully')); ?>.'>
    <input type="hidden" id="ir_sgte_ejerc" value='<?php echo ucfirst(JrTexto::_('continue to the next exercise')); ?>.'>
    <input type="hidden" id="finalizar" value='<?php echo ucfirst(JrTexto::_('you have finished the activity')); ?>.'>
    <input type="hidden" id="puedes_continuar" value='<?php echo ucfirst(JrTexto::_('you can continue next exercise')); ?>.'>
    <input type="hidden" id="tiempo_acabo" value='<?php echo ucfirst(JrTexto::_('time is over')); ?>.'>
    <input type="hidden" id="intentalo_otra_vez" value='<?php echo ucfirst(JrTexto::_('you can try again')); ?>.'>
    <input type="hidden" id="select_activity" value='<?php echo ucfirst(JrTexto::_('Select Activity')); ?>.'>
    <input type="hidden" id="select_upload" value='<?php echo JrTexto::_('Select or upload'); ?>'>
    <input type="hidden" id="click_add_description" value='<?php echo JrTexto::_('Click here to add description') ?>'>
    <input type="hidden" id="click_add_title" value='<?php echo JrTexto::_('Click here to add title') ?>'>
    <input type="hidden" id="remove_tab" value='<?php echo JrTexto::_('remove tab') ?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="no_hay_audio" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback')); ?>'>
    <input type="hidden" id="no_hay_audio_en_escena" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback in scene')); ?>'>
    <input type="hidden" id="no_hay_orden_en_escena" value='<?php echo ucfirst(JrTexto::_('you have not set an order in the dialogues in scene')); ?>'>
    <input type="hidden" id="debes_establecer_orden" value='<?php echo ucfirst(JrTexto::_('you must set an order for the dialog bubbles')); ?>'>
    <input type="hidden" id="write_tag" value='<?php echo ucfirst(JrTexto::_('write tag')); ?>'>
    <input type="hidden" id="ejerc_no_completado" value='<?php echo ucfirst(JrTexto::_('you have not finished editing this exercise')); ?>'>
    <input type="hidden" id="grabar_audio" value='<?php echo ucfirst(JrTexto::_('Record')); ?>'>
    <input type="hidden" id="detener_audio" value='<?php echo ucfirst(JrTexto::_('Stop')); ?>'>
    <input type="hidden" id="escribir_texto" value='<?php echo ucfirst(JrTexto::_('Write')); ?>'>
    <input type="hidden" id="reproducir_audio" value='<?php echo ucfirst(JrTexto::_('Play')); ?>'>
    <input type="hidden" id="end" value='<?php echo ucfirst(JrTexto::_('End')); ?>'>
</section>
<script type="text/javascript">
var idTabPane="#div_autoevaluar";
var show_hide_Scroller = function($tab=null) {};
var showPrimerElemento = function ($contenedor) {
    if($contenedor.length==0){ return false; }
    $contenedor.find('.items-select-list .item-select').first().trigger('click');
    $contenedor.find('.items-select-list').removeClass('items-select-list');
}
var contarNumeroEjercicio = function($tabPane) {
    var $pnlNroEjercicio = $tabPane.find('.numero_ejercicio');
    if($pnlNroEjercicio.length===0){ return false; }
    
    var total_txt = $tabPane.find('ul.nav.ejercicios li').not('.comodin').length;
    var $actualTab = $tabPane.find('ul.nav.ejercicios li.active');
    var actual_txt = $actualTab.find('a[data-toggle="tab"]').text();
    if($actualTab.length==0){ actual_txt = MSJES_PHP.end; total_txt=''; }

    $pnlNroEjercicio.find('.actual').text(actual_txt);
    $pnlNroEjercicio.find('.total').text(total_txt);
   $('#panel-barraprogreso').show();
};

$(document).ready(function(){
    $('.tab-pane[data-tabgroup="navTabsLeft"]').find('.nopreview').remove();
    $('#smartbook-ver .nav-tabs').on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
        var $actualTab = $(this);
        var idTabPane = $actualTab.attr('href');
        var $tabpane = $(idTabPane);
        var $slider = $tabpane.find('.myslider');
        var idNavTab = $actualTab.closest('.nav-tabs').attr('id');       
        contarNumeroEjercicio($tabpane);      
        showPrimerElemento($tabpane);
        $('audio').each(function(){ this.pause() });
        $('video').each(function(){ this.pause() });
        $('#smartbook-ver #navTabsLeft a[data-toggle="tab"]').not($actualTab).closest('li.active').removeClass('active');
        $('.tab-pane .content-smartic.metod.active').removeClass('active');
        $tabpane.find('.content-smartic.metod').addClass('active');
        if(!$tabpane.hasClass('smartic-initialized')){
            $tabpane.tplcompletar({editando:false});
        }
        rinittemplatealshow();
    })
    $('#smartbook-ver ul.nav.ejercicios').on('shown.bs.tab', 'a[data-toggle="tab"]', function(e) {
        var idTab = $(this).attr('href');
        var $tabPane = $(idTab).closest('div[data-tabgroup="navTabsLeft"]');
        contarNumeroEjercicio( $tabPane );
    })
    $('#smartbook-ver .preview').click(function(){
        $('.controles-edicion').hide();
        $('#smartbook-ver .descripcion-media').attr('readonly','readonly');  
        $('#smartbook-ver .nopreview').hide();
        $('#smartbook-ver').removeClass('editando');
        $('#smartbook-ver .nav-tabs li:first a').trigger('click').tab('show');
        $('#smartbook-ver ul.nav.ejercicios li:first a').trigger('click').tab('show');
    });

    $('#smartbook-ver .plantilla-speach').each(function(index, el) {
        initspeach( $(el) );
        if($(el).closest('.tabhijo').hasClass('active')){
            rinittemplatealshow();
        }
    });

    $('#smartbook-ver .preview').trigger('click');
    //$('#smartbook-ver .preview').hide();
    //$('#smartbook-ver .nav-tabs li:first-child a').tab('show');

    /*$('ul#navTabsLefts li:first a').tab('show');
    $('#smartbook-ver ul.nav.ejercicios li:first a').tab('show');
    $('ul#navTabsLefts li:first a').trigger('click');
    $('#smartbook-ver ul.nav.ejercicios li:first a').trigger('click');*/
});
</script>