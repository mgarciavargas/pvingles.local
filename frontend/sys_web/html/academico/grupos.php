<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$persona=!empty($this->datos)?$this->datos:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-md-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>      
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <!--li><a href="<?php //echo $this->documento->getUrlBase();?>/acad">&nbsp;<?php echo JrTexto::_("Personal"); ?></a></li-->
        <li class="active">&nbsp;<?php echo JrTexto::_("Grupos"); ?></li>       
    </ol>
  </div>
</div>
<?php } ?>
<div class="row" id="filtros<?php echo $idgui ?>">
  <div class="col-md-12">
  <form id="frm00<?php echo $idgui; ?>">
    <div class="panel" >      
      <div class="panel-body">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("type")); ?> :</label>
              <div class="cajaselect">
                <select name="tipo" id="tipo<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>                    
                  <?php if(!empty($this->fktipos)) foreach ($this->fktipos as $fk) { ?>
                    <option value="<?php echo $fk["codigo"]?>" <?php echo $fk["codigo"]==@$this->fktipo?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 notypev<?php echo $idgui; ?>">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Local")); ?> :</label>
              <div class="cajaselect">
                <select name="local" id="local<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>                   
                  <?php if(!empty($this->fklocales)) foreach ($this->fklocales as $fk) { ?>
                    <option value="<?php echo $fk["idlocal"]?>" <?php echo $fk["idlocal"]==@$this->fkidlocal?'selected="selected"':'';?> ><?php echo $fk["nombre"] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 notypev<?php echo $idgui; ?>">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Ambiente")); ?> :</label>
              <div class="cajaselect">
                <select name="ambiente" id="ambiente<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>                    
                  <?php if(!empty($this->fkambientes)) foreach ($this->fkambientes as $fk) { ?>
                    <option value="<?php echo $fk["idambiente"]?>" <?php echo $fk["idambiente"]==@$this->fkidambiente?'selected="selected"':'';?> ><?php echo $fk["numero"].' '.$fk["tipo_ambiente"] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Estado")); ?> :</label>
              <div class="cajaselect">
                <select name="estado" id="estado<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>                      
                  <?php if(!empty($this->fkestadogrupos)) foreach ($this->fkestadogrupos as $fk){ ?>
                    <option value="<?php echo $fk["codigo"]?>" <?php echo $fk["codigo"]==@$this->fkidestadogrupo?'selected="selected"':'';?> ><?php echo ucfirst(JrTexto::_($fk["nombre"])); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Curso")); ?> :</label>
              <div class="cajaselect">
                <select name="curso" id="curso<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>
                  <?php if(!empty($this->fkcursos)) foreach ($this->fkcursos as $fk) { ?>
                    <option value="<?php echo $fk["idcurso"]?>"  >
                      <?php echo $fk["nombre"]; ?>
                      </option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Search")); ?></label>
              <div class="input-group">
                <input type="text" name="texto" id="texto<?php echo $idgui; ?>" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4 text-center"><br>
            <a class="btn btn-warning" id="btnchangevista<?php echo $idgui; ?>" href="javascript:void(0)" data-titulo="<?php echo JrTexto::_("Change").' - '.JrTexto::_("View"); ?>"><i class="fa fa-drivers-license-o"></i> <?php echo JrTexto::_("Change").' - '.JrTexto::_("View"); ?></a> <a href="<?php echo $this->documento->getUrlBase(); ?>/acad_grupoaula/formulario" id="btnadd<?php echo $idgui; ?>" class="btn btn-warning"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add') ?></a>
              <!--a class="btn btn-success btnvermodal" data-modal="no" href="<?php echo JrAplicacion::getJrUrl(array("personal", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a-->
             <!--a class="btn btn-primary btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("personal", "importar"));?>" data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("import"); ?>"><i class="fa fa-cloud-upload"></i> <?php echo JrTexto::_('Import')?></a-->
          </div>
        </div>  
      </div>   
    </div>  
  </form>
  </div>
</div>
<div class="row" id="pnlvista<?php echo $idgui; ?>">
    
</div>

<script type="text/javascript">
$(document).ready(function(){
  var vista=window.localStorage.vistapnl||1;
  var cargarvista<?php echo $idgui;?>=function(view){
    var _vista=view||vista||1;
    var frmtmp=document.getElementById('frm00<?php echo $idgui; ?>');
    var formData = new FormData(frmtmp);
    formData.append('vista',_vista);
    formData.append('plt','blanco');
    var data={
      fromdata:formData,
      url:_sysUrlBase_+'/acad_grupoaula/listadovista',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'html',
      callback:function(rs){
        $('#pnlvista<?php echo $idgui; ?>').html(rs);
      }
    }
    sysajax(data);
    return false;
  }

 $('#tipo<?php echo $idgui;?>').change(function(ev){
    var vtmp_=$(this).val().toLowerCase();
    if(vtmp_=='v'){
      $('.notypev<?php echo $idgui; ?>').hide();
      $('#local<?php echo $idgui;?>').val('');
      $('#ambiente<?php echo $idgui;?>').val('');
    }else{
      $('.notypev<?php echo $idgui; ?>').show();
    }
    cargarvista<?php echo $idgui;?>();
  })

  $('#local<?php echo $idgui;?>').change(function(ev){
    var vtmp_=$(this).val().toLowerCase();
    var ambtmp=$('#ambiente<?php echo $idgui;?>');
    if(vtmp_==''){
      $('#ambiente<?php echo $idgui;?> option:first-child').siblings().remove();
      ambtmp.val('');
      cargarvista<?php echo $idgui;?>();
      return false;
    }else{
      var formData = new FormData();
      formData.append('idlocal',vtmp_);
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/ambiente/buscarjson',
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        callback:function(rs){
          var tr=rs.data;              
          $('option:first',ambtmp).siblings().remove();
          if(tr!=''){
              $.each(tr,function(i,v){                    
                ambtmp.append('<option value="'+v['idambiente']+'">'+v["numero"]+' '+v['tipo_ambiente']+'</option>');
            });
          }
          cargarvista<?php echo $idgui;?>();
        }
      }
      sysajax(data);
      return false;
    }
  })
  $('#ambiente<?php echo $idgui;?>').change(function(ev){
      cargarvista<?php echo $idgui;?>();
  });

  $('#estado<?php echo $idgui;?>').change(function(ev){
      cargarvista<?php echo $idgui;?>();
  });

  $('#curso<?php echo $idgui;?>').change(function(ev){
      cargarvista<?php echo $idgui;?>();
  });

  $('#texto<?php echo $idgui;?>').keyup(function(e) {
    if(e.keyCode == 13) {
        cargarvista<?php echo $idgui;?>();
    }
  });

  $('#local<?php echo $idgui;?>').trigger('change');

  $('#btnchangevista<?php echo $idgui; ?>').click(function(){
    vista=window.localStorage.vistapnl||1;
    vista=vista==1?2:1;
    localStorage.setItem('vistapnl',vista);
    cargarvista<?php echo $idgui;?>(vista);
  });
});
</script>