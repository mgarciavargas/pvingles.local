<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Ambiente"); ?></li>       
    </ol>
  </div>
</div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
                                        
            <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
              <select id="fkcbidlocal" name="fkcbidlocal" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('All Local'); ?></option>
                  <?php 
                          if(!empty($this->fkidlocal))
                            foreach ($this->fkidlocal as $fkidlocal) { ?><option value="<?php echo $fkidlocal["idlocal"]?>" <?php echo $fkidlocal["idlocal"]==@$frm["idlocal"]?"selected":""; ?> ><?php echo $fkidlocal["nombre"] ?></option><?php } ?>                        
              </select>
            </div>
                                                      
            <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
              <select id="fkcbtipo" name="fkcbtipo" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('All Type'); ?></option>
                  <?php 
                          if(!empty($this->fktipo))
                            foreach ($this->fktipo as $fktipo) { ?><option value="<?php echo $fktipo["codigo"]?>" <?php echo $fktipo["codigo"]==@$frm["tipo"]?"selected":""; ?> ><?php echo $fktipo["nombre"] ?></option><?php } ?>                        
              </select>
            </div>
                                                    
                    
                                
            <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
              <select id="fkcbturno" name="fkcbturno" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('All turno'); ?></option>
                  <?php 
                          if(!empty($this->fkturno))
                            foreach ($this->fkturno as $fkturno) { ?><option value="<?php echo $fkturno["codigo"]?>" <?php echo $fkturno["codigo"]==@$frm["turno"]?"selected":""; ?> ><?php echo $fkturno["nombre"] ?></option><?php } ?>                        
              </select>
            </div>
             <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
              <select name="cbestado" id="cbestado" class="form-control select-ctrl">
                  <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                  <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                  <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
              </select>
            </div>             
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Ambiente", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Ambiente").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Local"); ?></th>
                  <th><?php echo JrTexto::_("Numero") ;?></th>
                  <th><?php echo JrTexto::_("Capacidad") ;?></th>
                  <th><?php echo JrTexto::_("Tipo"); ?></th>
                  <th><?php echo JrTexto::_("Turno"); ?></th>
                  <th><?php echo JrTexto::_("Estado") ;?></th>
                  <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5a467e1e5b7b5='';
function refreshdatos5a467e1e5b7b5(){
    tabledatos5a467e1e5b7b5.ajax.reload();
}
$(document).ready(function(){  
  var estados5a467e1e5b7b5={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5a467e1e5b7b5='<?php echo ucfirst(JrTexto::_("ambiente"))." - ".JrTexto::_("edit"); ?>';
  var draw5a467e1e5b7b5=0;

  
  $('#fkcbidlocal').change(function(ev){
    refreshdatos5a467e1e5b7b5();
  });
  $('#fkcbtipo').change(function(ev){
    refreshdatos5a467e1e5b7b5();
  });
  $('#cbestado').change(function(ev){
    refreshdatos5a467e1e5b7b5();
  });
  $('#fkcbturno').change(function(ev){
    refreshdatos5a467e1e5b7b5();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos5a467e1e5b7b5();
  });
  tabledatos5a467e1e5b7b5=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Local") ;?>'},
        {'data': '<?php echo JrTexto::_("Numero") ;?>'},
        {'data': '<?php echo JrTexto::_("Capacidad") ;?>'},
        {'data': '<?php echo JrTexto::_("Tipo") ;?>'},
        {'data': '<?php echo JrTexto::_("Turno") ;?>'},
        {'data': '<?php echo JrTexto::_("Estado") ;?>'},
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/ambiente/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.idlocal=$('#fkcbidlocal').val(),
             d.tipo=$('#fkcbtipo').val(),
             d.estado=$('#cbestado').val(),
             d.turno=$('#fkcbturno').val(),
             //d.texto=$('#texto').val(),
                        
            draw5a467e1e5b7b5=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5a467e1e5b7b5;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Local") ;?>': data[i].local,
              '<?php echo JrTexto::_("Numero") ;?>': data[i].numero,
              '<?php echo JrTexto::_("Capacidad") ;?>': data[i].capacidad,
              '<?php echo JrTexto::_("Tipo") ;?>': data[i].tipo_ambiente,
              '<?php echo JrTexto::_("Turno") ;?>': data[i].turno_ambiente, 
              '<?php echo JrTexto::_("Estado") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="'+data[i].idambiente+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5a467e1e5b7b5[data[i].estado]+'</a>',                                 
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/ambiente/editar/?id='+data[i].idambiente+'" data-titulo="'+tituloedit5a467e1e5b7b5+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idambiente+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'ambiente', 'setCampo', id,campo,data);
          if(res) tabledatos5a467e1e5b7b5.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5a467e1e5b7b5';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Ambiente';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{header:true,footer:false,borrar:true}); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'ambiente', 'eliminar', id);
        if(res) tabledatos5a467e1e5b7b5.ajax.reload();
      }
    }); 
  });
});
</script>