<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=='modal'||$this->documento->plantilla=='blanco'?true:false;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-md-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>    
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <!--li><a href="<?php //echo $this->documento->getUrlBase();?>/acad">&nbsp;<?php echo JrTexto::_("Personal"); ?></a></li-->
        <li class="active">&nbsp;<?php echo JrTexto::_("Cargos"); ?></li>       
    </ol>
  </div>
</div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">
            <div class="col-md-12">

            <div class="col-xs-6 col-sm-4 col-md-3 form-group" >
              <label><?php echo ucfirst(JrTexto::_("State")); ?> :</label>
              <div class="cajaselect">
                <select name="cbmostrar" id="cbmostrar" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("---"))?></option>
                  <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                  <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>
                </select>
              </div>
            </div>                     
            <div class="col-xs-6 col-sm-6 col-md-6 form-group">
                <label><?php echo ucfirst(JrTexto::_("Text to search")); ?> :</label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i>
                     </span>  
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center"><br>
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Cargos", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Cargos").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div> 
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">
         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Cargo") ;?></th>
                    <th><?php echo JrTexto::_("Mostrar") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php $i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ $i++; ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo substr($reg["cargo"],0,100)."..."; ?></td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="mostrar"  data-id="<?php echo $reg["idcargo"]; ?>"> <i class="fa fa<?php echo !empty($reg["mostrar"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["mostrar"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td><a class="btn btn-xs lis_ver " href="<?php echo JrAplicacion::getJrUrl(array('cargos'))?>ver/?id=<?php echo $reg["idcargo"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update btnvermodal" data-modal='si' href="<?php echo JrAplicacion::getJrUrl(array("cargos", "editar", "id=" . $reg["idcargo"]))?>"  data-titulo="<?php echo JrTexto::_("Cargos").' - '.JrTexto::_("edit"); ?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["idcargo"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php } ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">
var tabledatos596f9de063682='';
function refreshdatos596f9de063682(){
    tabledatos596f9de063682.ajax.reload();
}
$(document).ready(function(){  
  var estados596f9de063682={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit596f9de063682='<?php echo ucfirst(JrTexto::_("cargos"))." - ".JrTexto::_("edit"); ?>';
  var draw596f9de063682=0;

  
  $('#cbmostrar').change(function(ev){
    refreshdatos596f9de063682();
  });
  $('.btnbuscar').click(function(ev){
    console.log('aaa');
    refreshdatos596f9de063682();
  });
  tabledatos596f9de063682=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Cargo") ;?>'},
            {'data': '<?php echo JrTexto::_("Mostrar") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/cargos/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.mostrar=$('#cbmostrar').val(),
             d.cargo=$('#texto').val(),                        
             draw596f9de063682=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw596f9de063682;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Cargo") ;?>': data[i].cargo,
                '<?php echo JrTexto::_("Mostrar") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="mostrar"  data-id="'+data[i].idcargo+'"> <i class="fa fa'+(data[i].mostrar=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados596f9de063682[data[i].mostrar]+'</a>',
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/cargos/editar/?id='+data[i].idcargo+'" data-titulo="'+tituloedit596f9de063682+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idcargo+'" ><i class="fa fa-trash-o"></i>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });


  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'cargos', 'setCampo', id,campo,data);
          if(res) tabledatos596f9de063682.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos596f9de063682';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Cargos';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{header:true,footer:false,borrarmodal:true}); 
  });
  
 

  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'cargos', 'eliminar', id);
        if(res) tabledatos596f9de063682.ajax.reload();
      }
    }); 
  });

});
</script>