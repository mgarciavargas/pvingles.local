<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
//$usuarioAct = NegSesion::getUsuario();
//$grupo=!empty($this->datos)?$this->datos:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
	.slick-items{
		position: relative;
	}
	.slick-item .image{
		overflow: hidden;
		width: 100%		
	}
	.slick-item{
		text-align: center !important;	

	}
	.slick-slide img ,.slick-item img{
		display: inline-block;
		text-align: center;
	}
	.slick-item .titulo{
		font-size: 1.6em;
		text-align: center;
	}	
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li> 
        <li class="active"><i class="fa fa-graduation-cap"></i>&nbsp;<?php echo JrTexto::_("Academic"); ?></li>       
    </ol>
  </div>
</div>
<?php } ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1.8em;"><?php echo JrTexto::_('Maintenance'); ?></h3>
				<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
			</div>
			<div class="panel-body">			
			   <div class="row">
			   		<div class="col-md-12">
				   		<div class="slick-items">		                                 
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/cargos" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/cargos.png">
		                            <div class="titulo"><?php echo JrTexto::_('Cargos'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/alumno" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/estudiantes.png">
		                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Students')); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/personal" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/personal.png">
		                            <div class="titulo"><?php echo JrTexto::_('Workforce'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/academico/curso" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/cursos.png">
		                            <div class="titulo"><?php echo JrTexto::_('Courses'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/ambiente" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/infraestructura.png">
		                            <div class="titulo"><?php echo JrTexto::_('Infrastructure'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/logro/listado" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/certificados.png">
		                            <div class="titulo"><?php echo JrTexto::_('Certificates'); ?></div>
		                        </a>
		                    </div>		                   
		                    <!--div class="slick-item" style="text-align: center !important;"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/tramitedocumentario" class="hvr-float" style="text-align: center !important;">
		                        	<img src="<?php echo $this->documento->getUrlStatic();?>/media/academico/tramitedocumentario.png">
		                            <div class="titulo"><?php echo JrTexto::_('Documentary procedure'); ?></div>
		                        </a>
		                    </div-->
		                </div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1.8em;"><?php echo ucfirst(JrTexto::_('Management')); ?></h3>
				<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
			</div>
			<div class="panel-body">			
			   <div class="row">
			   		<div class="col-md-12">
				   		<div class="slick-items">		                                 
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/acad_grupoaula" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/grupoestudio.png">
		                            <div class="titulo"><?php echo JrTexto::_('Study group'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/acad_matricula" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/matriculas.png">
		                            <div class="titulo"><?php echo JrTexto::_('Matriculate'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/marcacion" class="hvr-float">
		                        	<img class="img-responsive " src="<?php echo $this->documento->getUrlStatic();?>/media/academico/marcacion.png">
		                            <div class="titulo"><?php echo JrTexto::_('Dialing'); ?></div>
		                        </a>
		                    </div>
		                    <!--div class="slick-item "> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/asistencias" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/asistencia.png">
		                            <div class="titulo"><?php echo JrTexto::_('Assistance'); ?></div>
		                        </a>
		                    </div-->
		                    <!--div class="slick-item disabled"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/notas" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/notas.png">
		                            <div class="titulo"><?php echo JrTexto::_('Notes'); ?></div>
		                        </a>
		                    </div-->		                   
		                </div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel" >
			<div class="panel-heading bg-blue">
				<h3 class="panel-title" style="font-size: 1.8em;"><?php echo JrTexto::_('Complementary options'); ?></h3>
				<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
			</div>
			<div class="panel-body">			
			   <div class="row">
			   		<div class="col-md-12">
				   		<div class="slick-items">
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/bolsatrabajo" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/bolsa.png">
		                            <div class="titulo"><?php echo JrTexto::_('Job vacancies'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/manuales" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/materialestudio.png">
		                            <div class="titulo"><?php echo JrTexto::_('Study material'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/material_ayuda" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/materialayuda.png">
		                            <div class="titulo"><?php echo JrTexto::_('Help material'); ?></div>
		                        </a>
		                    </div>		                    
		                </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(e){		
 	var slikitems=$('.slick-items').slick(optionslike); 	
    $('header').removeClass('static');
});
</script>