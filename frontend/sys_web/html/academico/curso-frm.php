<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
if(!empty($frm['imagen'])) $imagen=$this->documento->getUrlBase().$frm['imagen'];
else $imagen=$this->documento->getUrlStatic().'/media/imagenes/cursos/nofoto.jpg';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .infototal{ padding: 1em 1ex; }
  .infototal .titulo{ font-size: 1.2em; }
  .infototal .numberinfo{ font-size: 0.9em; color: #ca3c3c; }
</style>
<div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <?php if($this->documento->plantilla!='modal'){?>
  <div class="row" id="breadcrumb">
    <div class="col-xs-12">
      <div style="position: relative;">
      <ol class="breadcrumb">
          <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
          <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
          <li><a href="<?php echo $this->documento->getUrlBase();?>/academico/curso">&nbsp;<?php echo JrTexto::_('course'); ?></a></li>
          <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion).' '.JrTexto::_('Course'); ?></li> 
      </ol>
      <ul style="position: absolute; top:1ex; right: 1ex;">        
          <li class="btn btn-xs btn-default"><i class="fa fa-print"></i> <?php echo JrTexto::_('Print'); ?></li>
          <li class="btn btn-xs btn-default view01 btn-changeview<?php echo $idgui; ?>"  data-view02="<?php echo JrTexto::_('View').' 02';?>"  data-view01="<?php echo JrTexto::_('View').' Minedu';?>"><i class="fa fa-eye"></i> <span><?php echo JrTexto::_('View').' Minedu'; ?></span></li>  
      </ul>
     </div>
    </div>
  </div>
  <?php }?>
    <div class="panel">
      <?php if($this->documento->plantilla=='modal'){?>
      <div class="panel-heading bg-blue">
        <div><?php echo JrTexto::_('Course'); ?><small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></div>
        <div class="clearfix"></div>
      </div>
      <?php } ?> 
      <div class="panel-body" id="pnl<?php echo $idgui ?>">
        <div id="msj-interno"></div> 
          <input type="hidden" name="pkIdcurso" id="pkIdcurso" value="<?php echo JrTexto::_($this->pk);?>">          
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="col-md-12"><div class="form-group">       
                <label>Nombre del Curso:</label>                             
                <input type="text" class="form-control" name="nombre" id="nombre<?php echo $idgui ?>" value="<?php echo ucfirst(@$frm["nombre"]); ?>" placeholder="<?php echo JrTexto::_('Name') ?>" >
              </div></div>
              <div class="col-md-12">             
                <div class="form-group">       
                  <label>I. Descripcion General:</label>
                  <textarea class="form-control" name="descripcioncurso" id="descripcion<?php echo $idgui ?>" placeholder="Aqui debe agregar la descripcion del curso" rows="6"><?php echo @$frm['descripcion'];?></textarea> 
                </div>  
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="col-md-12">
                  <div class="form-group"> <label><?php echo ucfirst(JrTexto::_("State")); ?>: </label>                 
                  <div class="select-ctrl-wrapper select-azul">
                    <select name="cbestado" id="cbestado<?php echo $idgui ?>" class="form-control select-ctrl">                    
                    <option value="1"<?php @$frm['estado']=='1'?' selected="selected" ':'' ?>><?php echo ucfirst(JrTexto::_("Active"))?></option>
                    <option value="0"<?php @$frm['estado']=='0'?' selected="selected" ':'' ?>><?php echo ucfirst(JrTexto::_("Inactive"))?></option>
                  </select>
                  </div>
                </div>
              </div>
              <div class="col-md-12 text-center">
                <label for="titulo"><?php echo ucfirst(JrTexto::_('Portada')); ?>:</label>
                <img src="<?php echo $imagen; ?>" alt="foto" class="cargaimagen<?php echo $idgui ?> img-responsive thumbnail centrado" data-url=".cargaimagen<?php echo $idgui ?>"  data-tipo="image" alt="" title="<?php echo JrTexto::_("Click here to change te exam cover"); ?>">
                <input type="hidden" name="imagen" id="imagen<?php echo $idgui ?>" value="<?php echo @$imagen ?>">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
               <div class="col-md-12 text-center infototal">
                  <div class="titulo"><label><?php echo ucfirst(JrTexto::_('Total').' '.JrTexto::_('Unit')); ?></label></div>
                  <div class="numberinfo"><span id="nunidad<?php echo $idgui ?>"><?php echo  (!empty($frm["nunidad"])?$frm["nunidad"]:'0').'</span> '.ucfirst(JrTexto::_('Unit')); ?></div>
               </div>
                <div class="col-md-12 text-center infototal">
                  <div class="titulo"><label><?php echo ucfirst(JrTexto::_('Total').' '.JrTexto::_('Activities')); ?></label></div>
                  <div class="numberinfo"><span id="nactividad<?php echo $idgui ?>"><?php echo (!empty($frm["nactividad"])?$frm["nactividad"]:'0').'</span> '.ucfirst(JrTexto::_('Activities')); ?></div>
               </div>
                <div class="col-md-12 text-center infototal">
                  <div class="titulo"><label><?php echo ucfirst(JrTexto::_('Total').' '.JrTexto::_('Assessment')); ?></label></div>
                  <div class="numberinfo"><span id="nexamen<?php echo $idgui; ?>"><?php echo (!empty($frm["nexamenes"])?$frm["nexamenes"]:'0').'</span> '.ucfirst(JrTexto::_('Assessment')); ?></div>
               </div>
            </div>
          </div>         
          <div class="clearfix"><br></div>
          <div class="row">            
              <div class="col-md-12 text-center">
                <a id="btn-savecurso" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </a>
                <a class="btn btn-warning btn-close" href="<?php echo $this->documento->getUrlBase();?>/academico/curso" data-dismiss="modal"  >
                <i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
                <?php /*if($this->frmaccion=='Editar'){?>
                <a id="btn-importtemas" class="btn btn-primary" ><i class=" fa fa-download"></i> <?php echo JrTexto::_('Import temas');?> </a>
                <a id="btn-verdetalle" class="btn btn-primary" ><i class=" fa fa-eye"></i> <?php echo JrTexto::_('view detail');?> </a>
                <!--a id="btn-changeview" class="btn btn-primary" ><i class=" fa fa-eye"></i> <?php //echo JrTexto::_('change view');?> </a-->
                <?php }*/ ?>
              </div>
          </div>
          <div class="clearfix"><hr></div>          
          <div class="row" id="pnldetalle<?php echo $idgui; ?>">
          </div>
          <div class="row" id="newventana<?php echo $idgui;?>">
                
          </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
/*var ventanaback='#pnldetalle<?php //echo $idgui; ?>';*/
var curvista=sessionStorage.getItem('vista')||'vista01';
sessionStorage.setItem('vista',curvista);
var recargarvista<?php echo $idgui; ?>=function(vista){
  vista=vista||sessionStorage.getItem('vista');
  var formData = new FormData();
  formData.append('idcurso', $('#pkIdcurso').val());
  formData.append('vista', vista);
  formData.append('plt', 'blanco');  
  var url=_sysUrlBase_+'/acad_cursodetalle/vista';
  $.ajax({
    url: url,
    type: "POST",
    data:  formData,
    contentType: false,
    processData: false,
    //dataType:'json',
    cache: false,
    beforeSend: function(XMLHttpRequest){ },      
    success: function(data)
    {     
        $('#pnldetalle<?php echo $idgui; ?>').html(data);      
    },
    error: function(e){ },
    complete: function(xhr){ }
  });
}

var refreshdatoscurso=function(){
  var formData = new FormData();
  formData.append('idcurso', $('#pkIdcurso').val());
  var url=_sysUrlBase_+'/acad_curso/buscarjson';
  $.ajax({
    url: url,
    type: "POST",
    data:  formData,
    contentType: false,
    processData: false,
    dataType:'json',
    cache: false,
    beforeSend: function(XMLHttpRequest){ },      
    success: function(data)
    {       
      if(data.code==='ok'){ 
        if(data.data[0]!=undefined){
          info=data.data[0];
          $('#nunidad<?php echo $idgui ?>').text(info.nunidad||0);
          $('#nactividad<?php echo $idgui ?>').text(info.nactividad||0);
          $('#nexamenes<?php echo $idgui ?>').text(info.nexamen||0);
        }             
                  
      }else{
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
      }
    },
    error: function(e){ },
    complete: function(xhr){ }
  });
}
var refreshvistapadre=function(){
   recargarvista<?php echo $idgui; ?>();
   refreshdatoscurso();
}

$(document).ready(function(){ 
$('#pnl<?php echo $idgui ?>').on('click','#btn-savecurso',function(ev){
  var formData = new FormData();
  formData.append('pkIdcurso', $('#pkIdcurso').val());
  formData.append('nombre', $('#nombre<?php echo $idgui ?>').val().trim());
  formData.append('imagen', $('#imagen<?php echo $idgui ?>').val());
  formData.append('descripcion', $('#descripcion<?php echo $idgui ?>').val());
  formData.append('estado', $('#cbestado<?php echo $idgui ?>').val());
  var url=_sysUrlBase_+'/acad_curso/guardarcurso';
  $.ajax({
    url: url,
    type: "POST",
    data:  formData,
    contentType: false,
    processData: false,
    dataType:'json',
    cache: false,
    beforeSend: function(XMLHttpRequest){ },      
    success: function(data)
    { 
      if(data.code==='ok'){
         $('#pkIdcurso').val(data.newid);
         mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
         refreshvistapadre();
      }else{
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
      }
    },
    error: function(e){ },
    complete: function(xhr){ }
  });
}).on('click','#btn-verdetalle',function(ev){
  refreshvistapadre();
});

$('.btn-changeview<?php echo $idgui; ?>').on('click',function(ev){
   curvista=sessionStorage.getItem('vista')||'vista01';
   if(curvista=='vista01'){
      curvista='vista02';
      $(this).find('span').text($(this).attr('data-view02'));
   }else{
      curvista='vista01';
      $(this).find('span').text($(this).attr('data-view01'));
   }
   sessionStorage.setItem('vista',curvista);
   recargarvista<?php echo $idgui; ?>(curvista);
});


$('.cargaimagen<?php echo $idgui ?>').click(function(e){ 
  var txt="<?php echo JrTexto::_('Selected or upload'); ?> ";
  selectedfile(e,this,txt);
});

$('.cargaimagen<?php echo $idgui ?>').load(function() {
  var src = $(this).attr('src');
  if(src!=''){
      $(this).siblings('input').val(src);
  }
});

var load<?php echo $idgui ?>=function(){
  $idcurso=$('#pkIdcurso').val();
  if(!$idcurso==''){
    refreshvistapadre();
  }
}
load<?php echo $idgui ?>();   
});
</script>