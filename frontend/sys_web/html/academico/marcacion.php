<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$fechaActual=date('Y-m-d H:i:s');
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
	.pnldni{width: 300px !important; margin: 1em auto;}
	.input-group-addon.btn-success{
		background: #5cb85c;
		color:#fff;

	}
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Mark hours"); ?></li>       
    </ol>
  </div>
</div>
<?php } ?>
<div class="row" id="pnl_<?php echo $idgui; ?>">
	<div class="col-md-12">
	    <div class="panel" >      
	      	<div class="panel-body ">
		      	<div class="row">
			      	<div class="col-md-6 text-center">
			      		<h4><?php echo JrTexto::_("Mark hours"); ?> </h4>
		              <div class="form-group pnldni">                
		                <div class="input-group">
		                  <input type="text" name="dni" id="dni<?php echo $idgui; ?>" value="<?php echo @$this->texto; ?>"  class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("dni"))?>" >
		                  <span class="input-group-addon btn btn-success btnbuscardni<?php echo $idgui; ?>"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>		                  
		                </div>
		                
		              </div>
		          	</div>
		          	<div class="col-md-6 text-center">
			      		<h4><?php echo JrTexto::_("See dial"); ?> </h4>
			      		<div class="col-xs-12 col-sm-12 col-md-4 changecol">
			            <div class="form-group">			            
			              <div class="cajaselect">
			                <select name="dateinfo" id="dateinfo<?php echo $idgui;?>" class="form-control">
			                   <option value="0d"><?php echo ucfirst(JrTexto::_("Today")); ?></option>
			                   <option value="-1d"><?php echo ucfirst(JrTexto::_("Yesterday")); ?></option>
			                   <option value="0w"><?php echo ucfirst(JrTexto::_("This Week")); ?></option>
			                   <option value="-1w"><?php echo ucfirst(JrTexto::_("Last Week")); ?></option>
			                   <option value="-6d"><?php echo ucfirst(JrTexto::_("Last 07 days")); ?></option>
			                   <option value="0m"><?php echo ucfirst(JrTexto::_("This Month")); ?></option>
			                   <option value="-30d"><?php echo ucfirst(JrTexto::_("Last 30 days")); ?></option>
			                   <option value="-1m"><?php echo ucfirst(JrTexto::_("Last Month")); ?></option>
			                   <option value="1c"><?php echo ucfirst(JrTexto::_("Custom range")); ?></option>			                  
			                </select>
			              </div>
			            </div>
			          	</div>
			          	<div class="col-xs-12 col-sm-12 col-md-4 changecol">
				          	<div class="form-group">
				          		<div class="input-group date" id="datetimepickerini">
				          			<input type="text" class="form-control fechaini<?php echo $idgui; ?> fechaspicker" required name="fecha_inicio" id="fecha_inicio<?php echo $idgui; ?>" value="<?php echo @$this->fechafin ;?>"> 
					              	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
				          		</div>	          
				          	</div>			          
				        </div>
				        <div class="col-xs-12 col-sm-12 col-md-4 hide changecol"> 
				          	<div class="form-group"> 
				          		<div class="input-group date" id="datetimepickerfin">
				          			<input type="text" class="form-control fechafin<?php echo $idgui; ?> fechaspicker" required name="fecha_final" id="fecha_final<?php echo $idgui; ?>" value="<?php echo @$this->fechafin ;?>"> 
					              	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
				          		</div>
				          	</div>			          
				        </div>
				        <div class="col-xs-12 col-sm-12 col-md-4">
				          	<button class="btn btn-success" id="btnmismar<?php echo $idgui; ?>"><i class="fa fa-search"></i> <?php echo JrTexto::_('Search'); ?></button>			          
				        </div>
		          	</div>   
	          	</div>
	        </div>
	    </div>
	</div>	
</div>
<div class="row" id="pnlvalmar<?php echo $idgui; ?>">
	<div class="col-md-12" >
    <div class="panel" >      
      	<div class="panel-body  text-center" style="">
	<h4><?php echo JrTexto::_('Select Option Up'); ?></h4>
</div>
</div>
</div>

</div>
<script type="text/javascript">
	$(document).ready(function(){
		var buscarmarcacion<?php echo $idgui;?>=function(formData,_url){
			var dni=$('#dni<?php echo $idgui; ?>').val()||'';
			if(dni=='')return false;			
			formData.append('dni',dni); 
			formData.append('idioma',_sysIdioma_);			    
			var data={
			    fromdata:formData,
			    url:_url,
			    msjatencion:'<?php echo JrTexto::_('Attention');?>',
			    type:'html',
			    callback:function(html){
			    	$('#pnlvalmar<?php echo $idgui; ?>').html(html);
			    }
			}
			sysajax(data);
			return false;
		}

		$('#dni<?php echo $idgui; ?>').keyup(function(e){
			if(e.keyCode == 13){
			    var data=new FormData();
			    var url=_sysUrlBase_+'/marcacion/vermarcacion';
		        buscarmarcacion<?php echo $idgui; ?>(data,url);
		    }			
		});

		$('.btnbuscardni<?php echo $idgui; ?>').click(function(){
			var data=new FormData();
			var url=_sysUrlBase_+'/marcacion/vermarcacion';
		    buscarmarcacion<?php echo $idgui; ?>(data,url);
		});

		$('#btnmismar<?php echo $idgui; ?>').click(function(){
			var data=new FormData();
			var fini=$('.fechaini<?php echo $idgui; ?>').val();
			var ffin=$('.fechafin<?php echo $idgui; ?>').val();
			data.append('fecha_finicio',fini);
			data.append('fecha_final',ffin); 
			var url=_sysUrlBase_+'/marcacion/mismarcaciones';
			buscarmarcacion<?php echo $idgui; ?>(data,url);
		});

		$('#dateinfo<?php echo $idgui;?>').change(function(ev){
			var vd=$(this).val();
			var fini=$('.fechaini<?php echo $idgui; ?>');
			var ffin=$('.fechafin<?php echo $idgui; ?>');
			if(vd=='0d'){//hoy
				fini.val(moment().format('YYYY-MM-DD'));
				ffin.closest('.changecol').removeClass('hide').addClass('hide');
			}else if(vd=='-1d'){ //ayer
				fini.val(moment().subtract(1, 'days').format('YYYY-MM-DD'));
				ffin.closest('.changecol').removeClass('hide').addClass('hide');
			}else if(vd=='-6d'){ //los ultimos 7 dias
				fini.val(moment().subtract(6, 'days').format('YYYY-MM-DD'));
				ffin.val(moment().format('YYYY-MM-DD'));
				ffin.closest('.changecol').removeClass('hide');				
			}else if(vd=='-30d'){ // los ultimos 30 dias
				fini.val(moment().subtract(30, 'days').format('YYYY-MM-DD'));
				ffin.val(moment().format('YYYY-MM-DD'));
				ffin.closest('.changecol').removeClass('hide');
			}else if(vd=='-1w'){ // la semana anterior;
				var us=moment().subtract(6, 'days').format('YYYY-MM-DD');
				var pdiasemana=moment(us,'YYYY-MM-DD').startOf('week').format('YYYY-MM-DD');
				fini.val(pdiasemana);
				ffin.closest('.changecol').removeClass('hide');
				ffin.val(moment(us,'YYYY-MM-DD').endOf('week').format('YYYY-MM-DD'));
			}else if(vd=='0w'){ //esta semana
				fini.val(moment().startOf('week').format('YYYY-MM-DD'));
				ffin.closest('.changecol').removeClass('hide');
				ffin.val(moment().format('YYYY-MM-DD'));
			}else if(vd=='0m'){ //esta mes
				fini.val(moment().startOf('month').format('YYYY-MM-DD'));
				ffin.closest('.changecol').removeClass('hide');
				ffin.val(moment().format('YYYY-MM-DD'));
			}else if(vd=='-1m'){ //mes pasado
				var us=moment().subtract(1, 'month').format('YYYY-MM-DD');
				fini.val(moment(us,'YYYY-MM-DD').startOf('month').format('YYYY-MM-DD'));
				ffin.closest('.changecol').removeClass('hide');
				ffin.val(moment(us,'YYYY-MM-DD').endOf('month').format('YYYY-MM-DD'));
			}else if(vd=='1c'){ //esta semana
				ffin.closest('.changecol').removeClass('hide');
			}
			//$('.fechaspicker').datetimepicker({ /*/lang:'es',  //timepicker:false,*/ format:'YYYY/MM/DD'});
		})
		$('.fechaspicker').datetimepicker({ /*/lang:'es',  //timepicker:false,*/ format:'YYYY/MM/DD'});		
		$('#dateinfo<?php echo $idgui;?>').trigger('change');
	});
</script>