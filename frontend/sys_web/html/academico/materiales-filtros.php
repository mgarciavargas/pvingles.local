<?php
defined("RUTA_BASE") or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$persona=!empty($this->datos)?$this->datos:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<script src="<?php echo $this->documento->getUrlStatic(); ?>/libs/importexcel/xlsx.full.min.js"></script>
<style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
</style>
<?php if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col-md-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>&nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <!--li><a href="<?php //echo $this->documento->getUrlBase();?>/acad">&nbsp;<?php echo JrTexto::_("Matriculas"); ?></a></li-->
        <li class="active">&nbsp;<?php echo JrTexto::_("Material delivery"); ?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row" id="filtros<?php echo $idgui ?>">
  <div class="col-md-12">
  <form id="frm00<?php echo $idgui; ?>">
    <div class="panel">
      <div class="panel-body">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-2">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("type")); ?> :</label>
              <div class="cajaselect">
                <select name="tipo" id="tipo<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("---")); ?></option>                    
                  <?php if(!empty($this->fktipos)) foreach ($this->fktipos as $fk) { ?>
                    <option value="<?php echo $fk["codigo"]?>" <?php echo $fk["codigo"]==@$this->fktipo?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Groups")." ".JrTexto::_("classroom")); ?> :</label>
              <div class="cajaselect">
                <select name="idgrupoaula" id="idgrupoaula<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("---")); ?></option>
                  <?php if(!empty($this->gruposaula)) foreach ($this->gruposaula as $fk) { ?>
                    <option value="<?php echo $fk["idgrupoaula"]?>" <?php echo $fk["idgrupoaula"]==@$this->idgrupoaula?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Course")); ?> :</label>
              <div class="cajaselect">
                <select name="idgrupoauladetalle" id="idgrupoauladetalle<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst('---'); ?></option>
                  <?php if(!empty($this->gruposauladetalle)) foreach ($this->gruposauladetalle as $fk) { ?>
                    <option value="<?php echo $fk["idgrupoauladetalle"]?>" <?php echo $fk["idgrupoauladetalle"]==$this->idgrupoauladetalle?'selected="selected"':'';?> ><?php echo !empty($fk["nombre"])&&@$fk["nombre"]!='-'?$fk["nombre"]:''.$fk["strcurso"] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>          
          <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="form-group">
                <label><?php echo ucfirst(JrTexto::_("Search")); ?> :</label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto<?php echo $idgui; ?>" value="<?php echo @$this->texto; ?>"  class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Search"))?> DNI" >
                  <span class="input-group-addon btn btnbuscar<?php echo $idgui; ?>"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-8 hide" id="detallecurso<?php echo $idgui; ?>">
            <hr>
            <div class="col-md-6"><strong><?php echo JrTexto::_('Teacher') ?>: </strong><span id="iddocente"></span></div>
            <div class="col-md-6"><strong><?php echo JrTexto::_('Local') ?>: </strong><span id="idlocal"></span></div>
            <div class="col-md-6"><strong><?php echo JrTexto::_('Date')." ".JrTexto::_('course') ?>: </strong><span id="idfechacurso"></span></div>
            <!--div class="col-md-6"><strong><?php //echo JrTexto::_('Date groups') ?>: </strong><span id="idfechacurso"></span>asd</div-->
            <!--div class="col-md-6"><strong><?php //echo JrTexto::_('Teacher') ?></strong><span id="iddocente"></span></div-->
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Material")); ?> :</label>
              <div class="cajaselect">
                <select name="idmanual" id="idmanual<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst('---'); ?></option>
                  <?php if(!empty($this->materiales)) foreach ($this->materiales as $fk) { ?>
                    <option value="<?php echo $fk["idmanual"]?>" <?php echo $fk["idmanual"]==$this->idmanual?'selected="selected"':'';?> ><?php echo $fk["titulo"] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </form>
  </div>
</div>
<div class="col-md-12" >
  <div class="row" id="pnlvista<?php echo $idgui; ?>"> 
    
  </div>
  <div class="hide" id="empydata<?php echo $idgui; ?>">
    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
    <div class="jumbotron">
      <h2><strong><?php echo JrTexto::_('Select data');?></strong></h2>                
      <div class="disculpa"><?php echo JrTexto::_('Select material and select groups');?></div>
    </div>
  </div>
  </div>  
</div>
<button class="tmprecargarlista<?php echo $idgui; ?>" style="display: none"></button>

<script type="text/javascript">
$(document).ready(function(){
  var vista=window.localStorage.vistapnl||1;
  var cargarvista<?php echo $idgui;?>=function(view){
    var idgrupoaula=$('#idgrupoaula<?php echo $idgui;?>').val()||'';
    var empydata=$('#empydata<?php echo $idgui; ?>').clone();
    var pnlvista=$('#pnlvista<?php echo $idgui; ?>');
    var idmanual=$('#idmanual<?php echo $idgui;?>').val()||'';
    var idgrupoauladetalle=$('#idgrupoauladetalle<?php echo $idgui; ?>').val()||'';
    if(idgrupoaula==''||idmanual==''||idgrupoauladetalle=='') {
      pnlvista.html(empydata.html());
      return false;
    }
    var texto=$('#texto<?php echo $idgui; ?>').val()||'';   
    vista=window.localStorage.vistapnl;
    var _vista=view||vista||1;
    var formData = new FormData();
    formData.append('idgrupoaula',idgrupoaula); 
    formData.append('idgrupoauladetalle',idgrupoauladetalle); 
    formData.append('texto',texto);   
    formData.append('vista',1);
    formData.append('idmanual',idmanual);
    formData.append('strmanual',$('#idmanual<?php echo $idgui;?> option:selected').text());
    formData.append('plt','blanco');
    formData.append('idioma',_sysIdioma_);
    var data={
      fromdata:formData,
      url:_sysUrlBase_+'/alumno/entregamaterialvista',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'html',
      callback:function(data){   
        $('#pnlvista<?php echo $idgui; ?>').html(data);
      }
    }
    sysajax(data);
  }

  var cargargrupos<?php echo $idgui;?>=function(objtmp,info){
    if(objtmp.lenght<1) return;
    var url=info.url||'/acad_grupoauladetalle/buscarjson';
    var infodata=info.data||'';
    var codigo=info.codigo||'codigo';
    var nombre=info.nombre||'nombre';
    var data={
        fromdata:infodata,
        url:_sysUrlBase_+url,
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        callback:function(data){
          var tr=data.data; 
          $('option:first-child',objtmp).siblings('option').remove();        
          if(tr!=''){              
            $.each(tr,function(i,v){            
                objtmp.append('<option value="'+v[codigo]+'">'+v[nombre]+'</option>');
            });
          } 
          objtmp.trigger('change');        
        }
      }
      sysajax(data);
  }

  var verdetallecurso<?php echo $idgui;?>=function(objtmp,info){
    if(objtmp.lenght<1) return;
    var url=info.url||'/acad_grupoauladetalle/buscarjson';
    var infodata=info.data||'';
    var data={
        fromdata:infodata,
        url:_sysUrlBase_+url,
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        callback:function(data){
          var rw=data.data;
          if(rw[0]!=undefined){
              var rs=rw[0];
              var pnldet= $('#detallecurso<?php echo $idgui; ?>');
              var tipo=$('#tipo<?php echo $idgui;?>').val()||'';
              pnldet.find('#iddocente').text(rs.iddocente+' : '+rs.strdocente);               
              pnldet.find('#idfechacurso').text(rs.fecha_inicio+' - '+rs.fecha_final);
              // pnldet.find('#iddocente').text(rs.iddocente+' '+rs.strdocente);
              if(tipo.toLowerCase()=='v')  pnldet.find('#idlocal').addClass('hide');
              else {
                pnldet.find('#idlocal').text(rs.strlocal+' - '+rs.strambiente);
                pnldet.find('#idlocal').removeClass('hide'); 
              }
              pnldet.removeClass('hide');
              cargarvista<?php echo $idgui;?>();
          }  
        }
      }
    sysajax(data);
  }

  $('#pnlvista<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5976271b3962a';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Personal';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{header:true,footer:false,borrarmodal:true,tam:'85%'});    
  });

  $('#tipo<?php echo $idgui;?>').change(function(ev){
     var vtmp_=$(this).val().toLowerCase();
      if(vtmp_==''){
        $('#idgrupoaula<?php echo $idgui;?> option:first-child').siblings('option').remove();
        $('#idgrupoauladetalle<?php echo $idgui;?> option:first-child').siblings('option').remove();
        $('#idgrupoauladetalle<?php echo $idgui;?>').val('').trigger('change');
        var empydata=$('#empydata<?php echo $idgui; ?>').clone();
        $('#pnlvista<?php echo $idgui; ?>').html(empydata.html());
        return;
      }   
      var obj=$('#idgrupoaula<?php echo $idgui;?>');
      var formData = new FormData();
      formData.append('tipo',vtmp_);
      var info={
        data:formData,
        codigo:'idgrupoaula',
        nombre:'nombre',
        url:'/acad_grupoaula/buscarjson',
      }    
      cargargrupos<?php echo $idgui;?>(obj,info);
  });

  $('#idgrupoaula<?php echo $idgui;?>').change(function(ev){ 
    var idgrupoaula=$(this).val();
    if(idgrupoaula==''){
       $('#idgrupoauladetalle<?php echo $idgui;?> option:first-child').siblings('option').remove();
       $('#idgrupoauladetalle<?php echo $idgui;?>').val('').trigger('change');
       var empydata=$('#empydata<?php echo $idgui; ?>').clone();
       $('#pnlvista<?php echo $idgui; ?>').html(empydata.html());
    }else{ 
      var vtmp_=$(this).val();
      var obj=$('#idgrupoauladetalle<?php echo $idgui;?>'); 
      var formData = new FormData();
      formData.append('idgrupoaula',vtmp_);
      var info={
      data:formData,
      codigo:'idgrupoauladetalle',
      nombre:'strcurso',
     // url:'/acad_grupoaula/buscarjson',
    }    
    cargargrupos<?php echo $idgui;?>(obj,info);    
    }    
  });

  $('#idgrupoauladetalle<?php echo $idgui;?>').change(function(ev){ 
    var idgrupoauladetalle=$(this).val()||'';    
    var pnldet= $('#detallecurso<?php echo $idgui; ?>');
    if(idgrupoauladetalle==''){           
      pnldet.addClass('hide');  
      var empydata=$('#empydata<?php echo $idgui; ?>').clone();
      var pnlvista=$('#pnlvista<?php echo $idgui; ?>');
      $('#pnlvista<?php echo $idgui; ?>').html(empydata.html());
    }else{ 
      var vtmp_=$(this).val();
      var obj=$('#idgrupoauladetalle<?php echo $idgui;?>'); 
      var formData = new FormData();
      formData.append('idgrupoauladetalle',obj.val()||'');
      formData.append('idgrupoaula',vtmp_);
      var info={ data:formData,}
      verdetallecurso<?php echo $idgui;?>(obj,info);
    }    
  });

  $('#idmanual<?php echo $idgui;?>').change(function(ev){
    cargarvista<?php echo $idgui;?>();
  })

  $('#btnchangevista<?php echo $idgui; ?>').click(function(){
    vista=window.localStorage.vistapnl||1;
    vista=vista==1?2:1;
    localStorage.setItem('vistapnl',vista);
    cargarvista<?php echo $idgui;?>(vista);
  });

  $('.tmprecargarlista<?php echo $idgui; ?>').on('recargarlista',function(ev){
    cargarvista<?php echo $idgui;?>();
  });

  $('.btnbuscar<?php echo $idgui; ?>').click(function(ev){
    cargarvista<?php echo $idgui;?>();
  });

  $('#texto<?php echo $idgui; ?>').keypress(function(event) {
    var keycode = event.keyCode || event.which;
    if(keycode == '13') {
      cargarvista<?php echo $idgui;?>();
      return false;
    }
});

  $('#idgrupoauladetalle<?php echo $idgui;?>').trigger('change');

  $('#pnlvista<?php echo $idgui; ?>').on('click','.btn_removetr',function(ev){
    $(this).closest('tr').remove();
  })
});
</script>