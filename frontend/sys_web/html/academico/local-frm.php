<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ismodal=$this->documento->plantilla!='modal'?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css"> .select-ctrl-wrapper select{font-size: 14px;} .select-ctrl-wrapper:after{ right: 15px; } </style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/local">&nbsp;<?php echo JrTexto::_('Local'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">          
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdlocal" id="pkidlocal" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <input type="hidden" name="txtId_ubigeo" id="txtId_ubigeo" value="<?php echo @$frm["id_ubigeo"];?>">
          <!--div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtId_ubigeo">
              <?php //echo JrTexto::_('Id ubigeo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              </div>
          </div-->
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('Name');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDireccion">
              <?php echo JrTexto::_('Address');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDireccion" name="txtDireccion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["direccion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12">
              <?php echo JrTexto::_('Departamento');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 select-ctrl-wrapper select-azul">                
                <select id="txtiddepartamento" name="txtIddepartamento" class="form-control select-ctrl">
                <option value="">Seleccione Departamento</option>
                <?php
                if(!empty($this->fkdepartamento))
                foreach ($this->fkdepartamento as $depa){?>
                <option value="<?php echo $depa["departamento"]; ?>" <?php echo $this->iddepa===$depa["departamento"]?'selected="selected"':''?>> <?php echo $depa["ciudad"]?> </option>
                <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12">
              <?php echo JrTexto::_('Provincia');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 select-ctrl-wrapper select-azul">                
                <select id="txtidprovincia" name="txtIdprovincia" class="form-control select-ctrl">
                <option value="">Seleccione Provincia</option>
                <?php
                if(!empty($this->fkprovincia))
                foreach ($this->fkprovincia as $pro){?>
                <option value="<?php echo $pro["provincia"]; ?>" <?php echo $this->idpro===$pro["provincia"]?'selected="selected"':''?>> <?php echo $pro["ciudad"]?> </option>
                <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12">
              <?php echo JrTexto::_('Distrito');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 select-ctrl-wrapper select-azul">                
                <select id="txtiddistrito" name="txtIddistrito" class="form-control select-ctrl">
                <option value="">Seleccione Distrito</option>
                <?php
                if(!empty($this->fkdistrito))
                foreach ($this->fkdistrito as $dis){?>
                <option value="<?php echo $dis["distrito"]; ?>" <?php echo $this->iddis===$dis["distrito"]?'selected="selected"':''?>> <?php echo $dis["ciudad"]?> </option>
                <?php } ?>
                </select>
              </div>
            </div>

           

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTipo">
              <?php echo JrTexto::_('Tipo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12  select-ctrl-wrapper select-azul">
              <select id="txttipo" name="txtTipo" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fktipo))
                foreach ($this->fktipo as $fktipo) { ?><option value="<?php echo $fktipo["codigo"]?>" <?php echo $fktipo["codigo"]==@$frm["tipo"]?"selected":""; ?> ><?php echo $fktipo["nombre"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtVacantes">
              <?php echo JrTexto::_('Vacantes');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <input type="number" id="txtVacantes" name="txtVacantes" step="1" value="<?php echo !empty($frm["vacantes"])?$frm["vacantes"]:1;?>"  min="1" class="form-control col-md-7 col-xs-12" required />
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdugel">
              <?php echo JrTexto::_('Ugel');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12  select-ctrl-wrapper select-azul">                
                <select id="txtIdugel" name="txtIdugel" class="form-control">
                <option value="">-</option>
                <?php 
                $selidpadre=@$frm["idugel"];
                foreach ($this->ugeles as $idugel) { ?>
                <option value="<?php echo $idugel["idugel"]; ?>" <?php echo $selidpadre===$idugel["idugel"]?'selected="selected"':''?>> <?php echo $idugel["descripcion"]?> </option>
                <?php } ?>
                </select>
              </div>
            </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveLocal" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('local'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

  var cargardatos<?php echo $idgui;?>=function(obj,data,returnobj){
      var midata=data||null;     
      $.ajax({
        url: _sysUrlBase_+'/ubigeo/buscarjson/?json=true',
        type: 'POST',
        dataType: 'json',
        data: midata,
      }).done(function(resp){
        if(resp.code=='ok'){                  
            $('option:first',obj).siblings().remove();
            if(resp.data!=''){
                $.each(resp.data,function(i,v){
                  obj.append('<option value="'+v[returnobj]+'">'+v["ciudad"]+'</option>');
              });
            }
            if(returnobj=='departamento'){
              $cbdepa=$('#txtiddepartamento');
              var copt=$('option',$cbdepa).length;
              if(copt>1)
                $('option',$cbdepa).eq(1).prop('selected',true);
                $('#txtiddepartamento').trigger('change');
                return;
            }
            if(returnobj=='provincia'){
              $cbpro=$('#txtidprovincia');
              var copt=$('option',$cbpro).length;
              if(copt>1)
                $('option',$cbpro).eq(1).prop('selected',true);
                $cbpro.trigger('change');
                return;
            }
            if(returnobj=='distrito'){
              var iddep=$('#txtiddepartamento').val()||'00';
              var idpro=$('#txtidprovincia').val()||'00';
              var iddis=$('#txtiddistrito').val()||'00';
              var idubigeo=iddep+''+idpro+''+iddis;
              $('#txtId_ubigeo').val(idubigeo);
            }
        }else{              
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
      }).fail(function(xhr, textStatus, errorThrown) {        
          console.log(xhr.responseText);
      });
  }
  
  /*$('#fkcbpais').change(function(ev){
    var d={};
    d.pais=$('#fkcbpais').val();
    d.return='departamento';
    if(d.pais==''){
        $('#cbdepartamento option:first').siblings().remove();
        $('#cbprovincia option:first').siblings().remove();
        refreshdatos5a459bea67036();
    }else{
        cargardatos5a459bea67036($('#cbdepartamento'),d,'departamento');     
    }
  });*/
  $('#txtiddepartamento').change(function(ev){
    var d={};
    d.pais='PE';
    d.departamento=$('#txtiddepartamento').val();
    d.return='provincia';
    if(d.departamento==''){
        $('#txtidprovincia option:first').siblings().remove();
        $('#txtiddistrito option:first').siblings().remove();
    }else{
        cargardatos<?php echo $idgui;?>($('#txtidprovincia'),d,'provincia');     
    }
  });
  $('#txtidprovincia').change(function(ev){
    var d={};
    d.pais='PE';
    d.departamento=$('#txtiddepartamento').val();
    d.provincia=$('#txtidprovincia').val();
    d.return='distrito';    
    if(d.provincia==''){
        $('#txtiddistrito option:first').siblings().remove();
    }else{
        cargardatos<?php echo $idgui;?>($('#txtiddistrito'),d,'distrito');     
    }
  });
  $('#txtiddistrito').change(function(ev){
    var iddep=$('#txtiddepartamento').val()||'00';
    var idpro=$('#txtidprovincia').val()||'00';
    var iddis=$('#txtiddistrito').val()||'00';
    var idubigeo=iddep+''+idpro+''+iddis;
    $('#txtId_ubigeo').val(idubigeo);
  });

           
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'local', 'saveLocal', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Local"))?>');
      }
     }
  });
});
</script>