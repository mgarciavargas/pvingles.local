<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'';
$frmtipo=!empty($frm["tipo"])?$frm["tipo"]:'';
$file=!empty($frm["archivo"])?$frm["archivo"]:'';
if($file!=''&&$frmtipo=='vid') $file='<video width="75%" src="'.$this->documento->getUrlBase().$file.'" controls="true"><video>';
else {
  $img=$this->documento->getUrlStatic().'/media/material_ayuda/sin'.$frmtipo.'.jpg';
  if($file=='')$file ='<img class="img-responsive center-block" width="50%"  src="'.$img.'" />';
  elseif($frmtipo=='img')  $file ='<img class="img-responsive center-block" width="51%" src="'.$this->documento->getUrlBase().$file.'"/>';  
  else $file ='<img class="img-responsive center-block" width="51%" src="'.$img.'"/><a href="'.$this->documento->getUrlBase().$file.' target="black">'.JrTexto::_('see').'</a>';
}

?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .toolbarmouse{
    width: 100%;
    position: absolute;
    top: 0px;
    background: rgba(255, 255, 253, 0.07);
  }
  .toolbarmouse .btnaddfile i{cursor: pointer;}

  .toolbarmouse .btnaddfile{
    color: #e40f0f;
    background-color: #b9d869;
    cursor: pointer;
  }
  .input-file-invisible{
    position: absolute;
    width: 100%;
    z-index: 9999;
    cursor: pointer;
    left: 0;
    top: 0;
  }
</style>
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Material_ayuda'">&nbsp;<?php echo JrTexto::_('Material_ayuda'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="multipart/form-data" class="form-horizontal form-label-left" >
          <input type="hidden" name="idmaterial" id="idmaterial" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">

          <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Name")); ?> :</label>
              <input type="text"  id="nombre<?php echo $idgui; ?>" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>"> 
            </div>
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Type File")); ?> :</label>
              <div class="cajaselect">
                <select name="tipo" id="tipo<?php echo $idgui;?>" class="form-control">
                  <option value=""  <?php echo $frmtipo==''?'selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("---")); ?></option>
                  <option value="img" <?php echo $frmtipo=='img'?'selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Image")); ?></option>
                  <option value="vid" <?php echo $frmtipo=='vid'?'selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Video")); ?></option> 
                  <option value="doc" <?php echo $frmtipo=='doc'?'selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Document")); ?></option>
                  <option value="pdf" <?php echo $frmtipo=='pdf'?'selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("PDF file")); ?></option>
                </select>
              </div>
            </div>           

            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Description")); ?> :</label>
              <textarea id="descripcion<?php echo $idgui; ?>" name="descripcion" class="form-control" required="required"><?php echo @$frm["descripcion"];?></textarea>
            </div>

            <div class="form-group">
              <label ><?php echo JrTexto::_('Show');?> <span class="required"> * :</span></label>
               <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["mostrar"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["mostrar"];?>"  data-valueno="0" data-value2="<?php echo @$frm["mostrar"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["mostrar"]==1?"Yes":"No");?></span>
                 <input type="hidden" name="mostrar" value="<?php echo !empty($frm["mostrar"])?$frm["mostrar"]:0;?>" > 
                 </a>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-6 text-center">
            <div class="form-group">
              <label><?php echo JrTexto::_('Archivo');?> <span class="required"> * :</span></label>
              <div style="position: relative;">
                <div class="toolbarmouse">                  
                  <span class="btn btnaddfile"><input type="file" class="input-file-invisible" name="archivo" id="fileFoto<?php echo $idgui; ?>" accept="image/*"><i class="fa fa-pencil"></i></span>
                </div>
                <div id="sysfile<?php echo $idgui; ?>"><?php echo $file; ?></div>                
              </div>
           </div>
          </div>
    
          <div class="ln_solid"></div>
          <div class="col-md-12 text-center">
            <hr>
           <div class="form-group">            
              <button id="btn-saveMaterial_ayuda" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('material_ayuda'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> 
                <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  var msjatencion='<?php echo JrTexto::_('Attention');?>';
  $('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      var btn=$(this);
      event.preventDefault();
      var tmpfrm=document.getElementById('frm-<?php echo $idgui;?>');
      var formData = new FormData(tmpfrm); 
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/material_ayuda/guardarMaterial_ayuda',
        msjatencion:msjatencion,
        type:'json',
        showmsjok : true,
        callback:function(rs){
          var haymodal=btn.closest('.modal');
          if(haymodal.lenght) haymodal.find('.cerrarmodal').trigger('click');
          else return redir('<?php echo JrAplicacion::getJrUrl(array("Material_ayuda"))?>');      
        }
      }
      sysajax(data);
      return false;
    }
  });   
  $('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo JrTexto::_("Yes");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo JrTexto::_("No");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }
  });

  $('#fileFoto<?php echo $idgui; ?>').click(function(ev){   
    var tipo=$('#tipo<?php echo $idgui;?>').val()||'';
    if(tipo==''){
      ev.preventDefault();
      ev.stopImmediatePropagation();
      mostrar_notificacion(msjatencion,'<?php echo JrTexto::_('Selected type');?>','warning');
      return false;
    }
    if(tipo=='img') $(this).attr('accept',"image/*");
    else if(tipo=='vid') $(this).attr('accept',"video/*");
    else if(tipo=='pdf') $(this).attr('accept',"application/pdf");
    else $(this).attr('accept',"application/*");
  })

  var inputFile = document.getElementById('fileFoto<?php echo $idgui; ?>');  
      inputFile.addEventListener('change', mostrarImagen, false);
      function mostrarImagen(event) {
        var file=event.target.files[0];
        var tipo=$('#tipo<?php echo $idgui;?>').val()||'';
        var donde=$('#sysfile<?php echo $idgui; ?>');
        changefoto=true;
        var reader=new FileReader();      
        reader.onload=function(event){
          var img=document.createElement('img');
          img.classList.add('img-responsive');
          img.classList.add('center-block');
          img.style.width='60%';
          img.style.height='200px';
          if(tipo=='img'){ 
            img.src=event.target.result;
          }else if(tipo=='vid'){
            var img=document.createElement('video')
            img.controls=true,
            img.style.width='60%';
            img.src=event.target.result;
          }else{          
            img.src=_sysUrlStatic_+'/media/material_ayuda/sin'+tipo+'.jpg';
          }
          if(img!='') donde.html('').append(img);
        }
        reader.readAsDataURL(file);
      };
});

</script>