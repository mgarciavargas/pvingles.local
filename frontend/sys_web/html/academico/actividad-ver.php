<?php defined('RUTA_BASE') or die(); 
    $idgui = uniqid(); 
    $rutabase = $this->documento->getUrlBase();
    $haylook=!empty($this->look)?true:false;
    $haypractice=!empty($this->practice)?true:false;
    $haydby=!empty($this->dby)?true:false;
?>
<!--a href="anclaUp" class="ancla" id="anclaUp"></a-->
<div id="smartbook-ver" class="editando row" data-idautor="<?php echo @$voc['idpersonal']; ?>">
    <input type="hidden" id="rolUsuario" value="<?php echo @$this->rolActivo; ?>">
    <input type="hidden" id="idCurso" value="<?php echo @$this->idCurso; ?>">
    <input type="hidden" id="idSesion" value="<?php echo @$this->sesion['idnivel']; ?>">

    <div id="cuaderno" class="tabbable tabs-right col-xs-12">
        <ul id="navTabsLeft" class="nav nav-tabs metodologias hidden">   
            <?php if($haylook){?><li class="hvr-bounce-in active"><a class="vertical" href="#div_look" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Look")) ?></a></li>
            <?php }elseif($haypractice){?>
            <li class="hvr-bounce-in active"><a class="vertical" href="#div_practice" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Practice")) ?></a></li>
            <?php }elseif($haydby){?>
            <li class="hvr-bounce-in active"><a class="vertical" href="#div_autoevaluar" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Do it by yourself")) ?></a></li><?php } ?>
        </ul>

        <div id="tabContent" class="tab-content">
            <div id="contenedor-paneles" class="hidden">
                <div id="panel-barraprogreso" class="text-center eje-panelinfo" style="display: none;">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            <span>0</span>%
                        </div>
                    </div>
                </div>
                <div id="panel-tiempo" class="eje-panelinfo pull-right" style="display: none;">
                    <div class="titulo btn-yellow"><?php echo ucfirst(JrTexto::_('time')); ?></div>
                    <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50" style="display: none;">
                    <div class="info-show">00</div>
                </div>
                <div id="panel-intentos-dby" class="eje-panelinfo text-center pull-right" style="display: none;">
                    <div class="titulo btn-blue"><?php echo ucfirst(JrTexto::_('attempt')); ?></div>
                    <span class="actual">1</span>
                    <span class="total"><?php echo @$this->intentos; ?></span>
                </div> 
            </div>

            <?php if($haylook){?>
            <div class="tab-pane in" id="div_look" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->look['bitacora'])?'data-idbitacora='.$this->look['bitacora']['idbitacora']:'' ?>>
                <div class="content-smartic metod" id="met-<?php echo @$this->look["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->look["met"]["idmetodologia"]; ?>" style="overflow: hidden;">
                    <div class="col-xs-12 Ejercicio">
                    <?php foreach (@$this->look['act']['det'] as $i => $det_html) {
                        echo str_replace('__xRUTABASEx__',$rutabase,$det_html['texto_edit']);
                    } ?>
                    </div>
                    <div class="col-xs-3 hidden">
                        <div class="widget-box">
                            <div class="widget-header bg-red">
                                <h4 class="widget-title">
                                    <i class="fa fa-paint-brush"></i>
                                    <span><?php echo JrTexto::_('Skills'); ?></span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row habilidades">
                                        <div class="col-xs-12 ">
                                        <?php 
                                        $ihab=0;
                                        if(!empty($this->habilidades))
                                        foreach ($this->habilidades as $hab) { $ihab++;?>
                                            <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                        <?php } ?>                                  
                                        </div>
                                    </div> 
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>
            <?php }else if($haypractice){?>
            <div class="tab-pane in" id="div_practice" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->practice['bitacora'])?'data-idbitacora='.$this->practice['bitacora']['idbitacora']:'' ?>>
                <div class="content-smartic metod" id="met-<?php echo @$this->practice["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->practice["met"]["idmetodologia"]; ?>" style="overflow: hidden;">
                    <div class="col-xs-12">
                        <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"])) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"])) ?>" data-id-metod="<?php echo @$this->practice["met"]["idmetodologia"]; ?>">
                            <?php if(!empty(@$this->practice['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            $num=0;
                            foreach ($this->practice['act']['det'] as $i=>$ejerc) { $num++;
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($num==1);
                                } ?>
                                <li class="<?php echo ($aquiSeQuedo)?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]) ).'-'.$num; ?>" data-toggle="tab"><?php echo $num; ?></a></li>
                            <?php } } ?>
                            <li class="hidden comodin"><!-- para que cuente correctamente la barra progreso --></li>
                        </ul>

                        <div class="col-xs-12 text-center numero_ejercicio">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Exercise')); ?></div>
                            <span class="actual"></span>
                            <span class="total"></span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="tab-content" id="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]) ) ?>-main-content">
                        <?php
                        if(!empty(@$this->practice['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            foreach (@$this->practice['act']['det'] as $i=>$ejerc) { 
                                $x=$i+1; 
                                $iddetalle=$ejerc["iddetalle"]; 
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($x==1);
                                } ?>
                            <div id="tab-<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]))."-".$x; ?>" class="tabhijo tab-pane fade <?php echo ($aquiSeQuedo)?'active in':''; ?>"  data-iddetalle="<?php echo  @$ejerc["iddetalle"]; ?>" >
                            <?php $html = $ejerc["texto_edit"];
                            if($this->rolActivo=='Alumno' && !empty($ejerc['desarrollo']) && !empty(@$ejerc['desarrollo']['html_solucion'])){
                                $html  = $ejerc['desarrollo']['html_solucion'];
                            }

                            echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$html);?>
                            </div>
                         <?php } } ?>
                        </div>
                    </div>
                    <div class="col-xs-3 hidden">
                        <div class="widget-box">
                            <div class="widget-header bg-red">
                                <h4 class="widget-title">
                                    <i class="fa fa-paint-brush"></i>
                                    <span><?php echo JrTexto::_('Skills'); ?></span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row habilidades">
                                        <div class="col-xs-12 ">
                                        <?php 
                                        $ihab=0;
                                        if(!empty($this->habilidades))
                                        foreach ($this->habilidades as $hab) { $ihab++;?>
                                            <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                        <?php } ?>                                  
                                        </div>
                                    </div> 
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div> 
            </div>
            <?php }else if($haydby){?>
            <div class="tab-pane in" id="div_autoevaluar" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->dby['bitacora'])?'data-idbitacora='.$this->dby['bitacora']['idbitacora']:'' ?>>
                <div class="content-smartic metod" id="met-<?php echo @$this->dby["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->dby["met"]["idmetodologia"]; ?>" style="overflow: hidden;">
                    <div class="col-xs-12">
                        <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"])) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"])) ?>" data-id-metod="<?php echo @$this->dby["met"]["idmetodologia"]; ?>">
                            <?php if(!empty(@$this->dby['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            $num=0;
                            foreach ($this->dby['act']['det'] as $i=>$ejerc) { $num++;
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo  = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($num==1);
                                } ?>
                                <li class="<?php echo ($aquiSeQuedo)?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]) ).'-'.$num; ?>" data-toggle="tab"><?php echo $num; ?></a></li>
                            <?php } } ?>
                            <li class="hidden comodin"><!-- tab obligado para calcular barra progreso--></li>
                        </ul>

                        <div class="col-xs-12 text-center numero_ejercicio">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Exercise')); ?></div>
                            <span class="actual"></span>
                            <span class="total"></span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="tab-content" id="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]) ) ?>-main-content">
                        <?php 
                        if(!empty(@$this->dby['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            foreach (@$this->dby['act']['det'] as $i=>$ejerc) { 
                                $x=$i+1; 
                                $iddetalle=$ejerc["iddetalle"]; 
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($x==1);
                                } ?>
                            <div id="tab-<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]))."-".$x; ?>" class="tabhijo tab-pane fade <?php echo ($aquiSeQuedo)?'active in':''; ?>" data-iddetalle="<?php echo  @$ejerc["iddetalle"]; ?>">
                            <?php  $html = $ejerc["texto_edit"];
                            if($this->rolActivo=='Alumno' && !empty($ejerc['desarrollo']) && !empty(@$ejerc['desarrollo']['html_solucion'])){
                                $html  = $ejerc['desarrollo']['html_solucion'];
                            }
                            echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$html);?>
                            </div>
                         <?php } } ?>
                        </div>
                    </div>
                    <div class="col-xs-3 hidden">
                        <div class="widget-box">
                            <div class="widget-header bg-red">
                                <h4 class="widget-title">
                                    <i class="fa fa-paint-brush"></i>
                                    <span><?php echo JrTexto::_('Skills'); ?></span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row habilidades">
                                        <div class="col-xs-12 ">
                                        <?php 
                                        $ihab=0;
                                        if(!empty($this->habilidades))
                                        foreach ($this->habilidades as $hab) { $ihab++;?>
                                            <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                        <?php } ?>                                  
                                        </div>
                                    </div> 
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    
    <!--a href="#anclaDown" class="scroller down" id="scroll-down"><i class="fa fa-angle-double-down fa-2x"></i></a-->
</div>
<a href="anclaDown" class="ancla" id="anclaDown"></a>


<div id="btns_progreso_intentos" style="display: none;">
    <div class="row" id="btns_control_alumno">
        <div class="col-sm-12 botones-control">
            <a class="btn btn-inline btn-blue btn-lg try-again"><i class="fa fa-undo"></i> <?php echo ucfirst(JrTexto::_('try again')); ?></a>
            <a class="btn btn-inline btn-success btn-lg save-progreso" ><?php echo ucfirst(JrTexto::_('continue')) ?> <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
</div>

<section class="hidden" id="msjes_idioma" style="display: none;">
    <input type="hidden" id="attention" value="<?php echo JrTexto::_('Attention');?>">
    <input type="hidden" id="no_puedes_arrastrar" value='<?php echo JrTexto::_('You can not drag another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="no_puedes_seleccionar" value='<?php echo JrTexto::_('You can not select another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="you_must_finish" value='<?php echo ucfirst(JrTexto::_('you must do the exercise before trying again')); ?>'>
    <input type="hidden" id="bien_hecho" value='<?php echo ucfirst(JrTexto::_('well done!')); ?>'>
    <input type="hidden" id="cargando" value='<?php echo ucfirst(JrTexto::_('Loading')); ?>'>
    <input type="hidden" id="ejerc_exito" value='<?php echo ucfirst(JrTexto::_('you have complete the exercise succesfully')); ?>.'>
    <input type="hidden" id="ir_sgte_ejerc" value='<?php echo ucfirst(JrTexto::_('continue to the next exercise')); ?>.'>
    <input type="hidden" id="finalizar" value='<?php echo ucfirst(JrTexto::_('you have finished the activity')); ?>.'>
    <input type="hidden" id="puedes_continuar" value='<?php echo ucfirst(JrTexto::_('you can continue next exercise')); ?>.'>
    <input type="hidden" id="tiempo_acabo" value='<?php echo ucfirst(JrTexto::_('time is over')); ?>.'>
    <input type="hidden" id="intentalo_otra_vez" value='<?php echo ucfirst(JrTexto::_('you can try again')); ?>.'>
    <input type="hidden" id="select_activity" value='<?php echo ucfirst(JrTexto::_('Select Activity')); ?>.'>
    <input type="hidden" id="select_upload" value='<?php echo JrTexto::_('Select or upload'); ?>'>
    <input type="hidden" id="click_add_description" value='<?php echo JrTexto::_('Click here to add description') ?>'>
    <input type="hidden" id="click_add_title" value='<?php echo JrTexto::_('Click here to add title') ?>'>
    <input type="hidden" id="remove_tab" value='<?php echo JrTexto::_('remove tab') ?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="no_hay_audio" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback')); ?>'>
    <input type="hidden" id="no_hay_audio_en_escena" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback in scene')); ?>'>
    <input type="hidden" id="no_hay_orden_en_escena" value='<?php echo ucfirst(JrTexto::_('you have not set an order in the dialogues in scene')); ?>'>
    <input type="hidden" id="debes_establecer_orden" value='<?php echo ucfirst(JrTexto::_('you must set an order for the dialog bubbles')); ?>'>
    <input type="hidden" id="write_tag" value='<?php echo ucfirst(JrTexto::_('write tag')); ?>'>
    <input type="hidden" id="ejerc_no_completado" value='<?php echo ucfirst(JrTexto::_('you have not finished editing this exercise')); ?>'>
    <input type="hidden" id="grabar_audio" value='<?php echo ucfirst(JrTexto::_('Record')); ?>'>
    <input type="hidden" id="detener_audio" value='<?php echo ucfirst(JrTexto::_('Stop')); ?>'>
    <input type="hidden" id="escribir_texto" value='<?php echo ucfirst(JrTexto::_('Write')); ?>'>
    <input type="hidden" id="reproducir_audio" value='<?php echo ucfirst(JrTexto::_('Play')); ?>'>
    <input type="hidden" id="end" value='<?php echo ucfirst(JrTexto::_('End')); ?>'>
</section>

<script type="text/javascript">
//document.getElementById('div_imagen').style.display="none";
var _IDHistorialSesion=0;


var pos=-1;
var posv=-1;
var showdiv=[];

function fileExists(url) {
    var rspta = false;
    if(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.send();
        rspta = (req.status==200);
    }
    return rspta;
}

function existeEnSlider(url, $panel){
    var rspta = true;
    if(url){
        var archivo = $panel.find('.myslider *[src="'+url+'"]');
        rspta = (archivo.length>0);
    }
    return rspta;
}

var showPrimerElemento = function ($contenedor) {
    if($contenedor.length==0){ return false; }
    $contenedor.find('.items-select-list .item-select').first().trigger('click');
    $contenedor.find('.items-select-list').removeClass('items-select-list');
}

var calcularProgresoMultimedia = function($tabPane){
    var porcentaje = 0.0;
    var numDivs=$tabPane.find('.myslider .miniatura').length;
    var numDivsVistos=$tabPane.find('.myslider .miniatura.visto').length;
    if(numDivs>0){
        porcentaje=(numDivsVistos/numDivs)*100;
    }
    return porcentaje;
};  

var totalPestanias = function(){
    /* Solo las pestañas (top o left) visibles en el smartbook */
    var cant = 0;
    $("#navTabsTop, #navTabsLeft").find('li>a').not('a[href="#div_presentacion"],a#exit').each(function(i, elem) {
        var isVisible = $(elem).hasClass('hidden') || $(elem).is(':visible')
        if(isVisible){ cant++; }
    });

    cant--; /* la pestaña div_games aun no se guarda, por loq  no se está considerando. 
                Esta linea deberá borrarse una vez implementado el guardar de juegos */

    return cant;
};

var guardarBitacora = function(dataSave, $tabPane){
    if($('#rolUsuario').val()!='Alumno'){ console.log('No es Alumno - No guardar'); return false; }
    $.ajax({
        url: _sysUrlBase_+'/bitacora_smartbook/guardarBitacora_smartbook',
        type: 'POST',
        dataType: 'json',
        data: dataSave,
    }).done(function(resp) {
        if(resp.code=='ok'){
            var idLogro = $('#idLogro').val();
            var idSesion = $('#idSesion').val();
            var tieneLogro = idLogro>0;
            $tabPane.attr('data-idbitacora', resp.newid);
            if(resp.progreso>=100 && tieneLogro && resp.bandera==0){
                mostrarLogroObtenido({'id_logro':idLogro, 'idrecurso': idSesion  });
            }
        }
    }).fail(function(e) {
        console.log("!Error", e);
    });
};

var show_hide_Scroller = function($tab=null) {
    if($tab===null) return false;
    var containerHeight = $tab.closest('div.tab-pane[data-tabgroup="navTabsLeft"]').outerHeight();
    var contentHeight = 0;
    $tab.children().each(function(index, el) {
        contentHeight += $(el).outerHeight();
    });
    if( !$('#contenedor-paneles').hasClass('hidden') ){ contentHeight += $('#contenedor-paneles').outerHeight()+25; }
    if(contentHeight > containerHeight){
        $('#scroll-down').show();
    }else{
        $('#scroll-down').hide();
    }
};

var contarNumeroEjercicio = function($tabPane) {
    var $pnlNroEjercicio = $tabPane.find('.numero_ejercicio');
    if($pnlNroEjercicio.length===0){ return false; }
    
    var total_txt = $tabPane.find('ul.nav.ejercicios li').not('.comodin').length;
    var $actualTab = $tabPane.find('ul.nav.ejercicios li.active');
    var actual_txt = $actualTab.find('a[data-toggle="tab"]').text();
    if($actualTab.length==0){ actual_txt = MSJES_PHP.end; total_txt=''; }

    $pnlNroEjercicio.find('.actual').text(actual_txt);
    $pnlNroEjercicio.find('.total').text(total_txt);
};

$(document).ready(function(){
   initBitac_Alum_Smbook();
    $('#smartbook-ver .nav-tabs').on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
        var $actualTab = $(this);
        var idTabPane = $actualTab.attr('href');
        var $tabpane = $(idTabPane);
        var $slider = $tabpane.find('.myslider');
        var idNavTab = $actualTab.closest('.nav-tabs').attr('id');
        setTimeout(function() { show_hide_Scroller($tabpane); }, 500);
        contarNumeroEjercicio($tabpane);
        showPrimerElemento($tabpane);
        $( "#div_presentacion .div_acciones" ).hide();

        $('audio').each(function(){ this.pause() });
        $('video').each(function(){ this.pause() });

        if( idNavTab==='navTabsTop' || idNavTab==='navTabsLeft' ){
            $('#smartbook-ver #navTabsTop a[data-toggle="tab"], #smartbook-ver #navTabsLeft a[data-toggle="tab"]').not($actualTab).closest('li.active').removeClass('active');

            if(idTabPane==="#div_look" || idTabPane==="#div_practice" || idTabPane==="#div_autoevaluar"){
                $('#contenedor-paneles').removeClass('hidden');
                $('.tab-pane .content-smartic.metod.active').removeClass('active');
                $tabpane.find('.content-smartic.metod').addClass('active');
                if(!$tabpane.hasClass('smartic-initialized')){
                    $tabpane.tplcompletar({editando:false});
                }

                rinittemplatealshow();
            }else{
                $('#contenedor-paneles').addClass('hidden');
            }

            if(idNavTab==='navTabsLeft' && (idTabPane==="#div_look" || idTabPane==="#div_voca" || idTabPane==="#div_workbook")){
                var $tabPane = $(idTabPane);
                var progreso = 100.00;               
            }
        }
    });

    $('#smartbook-ver ul.nav.ejercicios').on('shown.bs.tab', 'a[data-toggle="tab"]', function(e) {
        var idTab = $(this).attr('href');
        var $tabPane = $(idTab).closest('div[data-tabgroup="navTabsLeft"]');
        contarNumeroEjercicio( $tabPane );
    })

    $('#div_practice, #div_autoevaluar').on('click', '#btns_control_alumno .save-progreso', function(e) {
        e.preventDefault();
        var $tabPane = $(this).closest('[data-tabgroup]');
        var progreso = parseFloat( $tabPane.find('.metod').attr('data-progreso') );
        guardarBitacora({
            'Idbitacora' : $tabPane.attr('data-idbitacora')||null,
            'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val(),
            'txtIdcurso' : $('#idCurso').val(),
            'txtIdsesion' : $('#idSesion').val(),
            'txtPestania' : $tabPane.attr('id'),
            'txtTotal_Pestanias' : totalPestanias(),
            'txtProgreso' : progreso.toFixed(2),
            'txtOtros_datos' : null,
            'txtIdlogro' : $('#idLogro').val(),
        }, $tabPane);
    });

   
    var iScrollPos = 0;
    $('#smartbook-ver #tabContent > .tab-pane').scroll(function(e) {
        var iCurScrollPos = $(this).scrollTop();
        var tabPaneHeight = $(this).outerHeight();
        var contentHeight = 0;

        $(this).children().each(function(index, el) {
            contentHeight += $(el).outerHeight(); //+71;
        });

        if( !$('#contenedor-paneles').hasClass('hidden') ){ contentHeight += $('#contenedor-paneles').outerHeight()-3; }

        if (iCurScrollPos > iScrollPos) {
            $('#scroll-up').show();
            /*console.log(iCurScrollPos+' + '+tabPaneHeight+' +10= ', iCurScrollPos + tabPaneHeight+10);
            console.log('contentHeight = ', contentHeight);*/
            if(iCurScrollPos + tabPaneHeight-10 >= contentHeight) { /* LLego al final?*/
                $('#scroll-down').hide();
            }else {
                $('#scroll-down').show();
            }
        } else {
            $('#scroll-down').show();
            if(iCurScrollPos==0){ /* LLego al inicio?*/
                $('#scroll-up').hide();
            }else {
                $('#scroll-up').show();
            }
        }
        iScrollPos = iCurScrollPos;
    });

    $('#scroll-down').click(function(e) {
        //e.preventDefault();
        $('#smartbook-ver #tabContent > .tab-pane.active').animate({ scrollTop: 750}, 1200, 'linear');

    });
    $('#scroll-up').click(function(e) {
        //e.preventDefault();
        $('#smartbook-ver #tabContent > .tab-pane.active').animate({ scrollTop: -750}, 1200, 'linear');
    });

/*** #Fn generales(Pdf,Aud,Img,Vid) ***/

    $('#smartbook-ver .plantilla-speach').each(function(index, el) {
        initspeach( $(el) );
        if($(el).closest('.tabhijo').hasClass('active')){
            rinittemplatealshow();
        }
    });

    $('#navTabsLeft').children('li').children('a').trigger('click');
/******************************************************/
});

</script>