<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$estados[1]=JrTexto::_('active');
$estados[0]=JrTexto::_('inactive');
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->matriculados)) $personal=$this->matriculados;
$estados[0]=JrTexto::_("No");
$estados[1]=JrTexto::_("Yes");
//var_dump($this->matriculados);
?>
<div class="panel">
	<div class="panel-body" >
    <table class="table table-striped table-responsive" id="table_<?php echo $idgui; ?>">
      	<thead>
        	<tr class="headings">
          		<th>#</th>
            	<th>DNI: <?php echo JrTexto::_("Student") ;?></th>
              <th><?php echo JrTexto::_("Material") ;?></th>
              <th><?php echo JrTexto::_("Fecha de entrega") ;?></th> 
              <th><?php echo JrTexto::_("Delivered") ;?></th>          
        	</tr>
      	</thead>
      	<tbody>
      		<?php 
      		$i=0;
      		$url=$this->documento->getUrlBase();
      		if(!empty($personal))
      			foreach ($personal as $per){                 
                $dt=$this->seentregomaterial($per["idalumno"],$this->idmanual,$this->idgrupoauladetalle,$this->idgrupoaula);               
                $fechaentrega=!empty($dt["fecha"])?$dt["fecha"]:'-';
                $identrega=!empty($dt["identrega"])?$dt["identrega"]:'';
                $entregado=!empty($dt["estado"])?$estados[$dt["estado"]]:$estados[0];
      					$i++;
      					?>
      				<tr data-id="<?php echo @$per["idalumno"]; ?>" data-idmanual="<?php echo $this->idmanual; ?>" data-identrega="<?php echo $identrega; ?>" >
      					<td><?php echo $i; ?></td>
      					<td class="fullnombre"><b><?php echo @$per["idalumno"]."</b><br>".@$per["stralumno"];?></td>
                <td><?php echo $this->strmanual; ?></td>
      					<td class="fecha"><?php echo $fechaentrega;?></td> 
      					<td><a href="javascript:;"  class="btn-entregar" campo="estado"> <i class="fa fa<?php echo @$dt["estado"]==1?'-check':''; ?>-circle-o fa-lg"></i> <?php echo @$entregado; ?></a>
      					</td>          
      				</tr>
      		<?php }	?>
        </tbody>
    </table>
	</div>       
</div>
<div class="hide" id="frmclone<?php echo $idgui; ?>">
  <form method="post" id="frm-<?php echo $idgui;?>" class="form-horizontal " >
  <input type="hidden" name="identrega" id="identrega<?php echo $idgui; ?>" value="">
  <input type="hidden"  name="idalumno" id="idalumno<?php echo $idgui;?>" value="">
  <input type="hidden"  name="idgrupo" id="idgrupoaula<?php echo $idgui;?>"  value="<?php echo @$this->idgrupoaula;?>">        
  <input type="hidden"  name="idmanual" id="idmanual<?php echo $idgui;?>" value="<?php echo @$this->idmanual;?>"> 
  <input type="hidden"  name="idaulagrupodetalle" id="idaulagrupodetalle<?php echo $idgui; ?>" value="<?php echo @$this->idgrupoauladetalle;?>">
  <input type="hidden" id="fecha" name="fecha" value="<?php echo date("Y/m/d"); ?>">
   <div class="form-group">
      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtSerie">
      <?php echo JrTexto::_('Serie');?> <span class="required"> * :</span>
      </label>
      <div class="col-md-8 col-sm-8 col-xs-12">
        <input type="text"  id="serie" name="serie" required="required" class="form-control col-md-7 col-xs-12" value="">
                          
      </div>
    </div> 
    <div class="form-group">
      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtComentario">
      <?php echo JrTexto::_('Comentario');?> <span class="required"> * :</span>
      </label>
      <div class="col-md-8 col-sm-8 col-xs-12">
        <textarea id="comentario" name="comentario<?php echo $idgui; ?>" class="form-control col-md-7 col-xs-12">  </textarea>                                  
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="estado">
      <?php echo JrTexto::_("Delivered") ;?> 
      </label>
      <div class="col-md-8 col-sm-8 col-xs-12">
        <a style="cursor:pointer;" class="chkformulario fa fa-check-circle">
         <span class="estado"> <?php echo JrTexto::_("Yes");?></span>
         <input class="estado" type="hidden" name="estado" id="estado<?php echo $idgui; ?>" value="1" > 
        </a>
      </div>
    </div> 
    <div class="ln_solid"></div>
    <div class="form-group">
      <div class="col-md-6 col-md-offset-4">
        <button id="btn-saveManuales_alumno" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
        <a type="button" class="btn btn-warning btn-close cerrarmodal" href="javascript:void(null)" data-dismiss="modal"><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#table_<?php echo $idgui; ?>').DataTable({
      "pageLength": 50,
			"searching": false,
      		"processing": false
			<?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
		}).on('click','.btn-entregar',function(){
      var tr=$(this).closest('tr');
      var idalumno=tr.attr('data-id');
      var idmanual=tr.attr('data-idmanual'); 
      var identrega= tr.attr('data-identrega');
      var idtmp='frm'+Date.now();
      $objclone=$('#frmclone<?php echo $idgui; ?>').clone();
      $objclone.find('#frm-<?php echo $idgui;?>').attr('id',idtmp);
      $objclone.find('#identrega<?php echo $idgui; ?>').val(identrega);
      $objclone.find('#idalumno<?php echo $idgui; ?>').val(idalumno);
      var obj={
        htmltxt:$objclone.html(),
        borrar:true,
        titulo:false,
      }
      var modal=sysmodal(obj);
      $(modal).on('click','#btn-saveManuales_alumno',function(ev){
        var btn=$(this);
        var frm=document.getElementById(idtmp);
        var formData = new FormData(frm);    
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/manuales_alumno/guardarManuales_alumno',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok:true,
          callback:function(rs){
            var inputestado=$('#'+idtmp).find('input.estado');
            var estado=inputestado.val()||0;
            var _estado=inputestado.siblings('span').text();
            var txtestado=estado==0?('<i class="fa fa-circle-o fa-lg"> '+_estado):('<i class="fa fa-check-circle fa-lg"> '+_estado);
            var accion=rs.accion;
            var identrega=rs.newid;
            tr.attr('data-identrega',identrega);
            tr.find('.btn-entregar').html(txtestado);
            if(accion=='_add')
            tr.find('.fecha').text($('#'+idtmp).find('input.estado').val());  
            btn.closest('.modal').modal('hide');
          }
        }
        sysajax(data);
        return false;
        
      }).on('click','.chkformulario',function(ev){
        if($(this).hasClass('fa-circle-o')){
          $('span',this).text(' <?php echo JrTexto::_("Yes");?>');
          $('input',this).val(1);
          $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
        }else {
          $('span',this).text(' <?php echo JrTexto::_("No");?>');
          $('input',this).val(0);
          $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
        }
      })      
    });
	})
</script>