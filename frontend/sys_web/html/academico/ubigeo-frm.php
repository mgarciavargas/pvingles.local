<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ismodal=$this->documento->plantilla!='modal'?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';

?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css"> .select-ctrl-wrapper select{font-size: 14px;} .select-ctrl-wrapper:after{ right: 15px; } </style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/ubigeo">&nbsp;<?php echo JrTexto::_('Ubigeo'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">           
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkId_ubigeo" id="pkid_ubigeo" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtPais">
              <?php echo JrTexto::_('Pais');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 select-ctrl-wrapper select-azul">
              <select id="txtpais<?php echo $idgui;?>" name="txtPais" class="form-control">
              <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkpais))
                foreach ($this->fkpais as $fkpais) { ?><option value="<?php echo $fkpais["pais"]?>" <?php echo $fkpais["pais"]==@$frm["pais"]?"selected":""; ?> ><?php echo $fkpais["ciudad"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDepartamento">
              <?php echo JrTexto::_('Departamento');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 select-ctrl-wrapper select-azul">
              <select id="txtdepartamento<?php echo $idgui;?>" name="txtDepartamento" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkdepartamento))
                foreach ($this->fkdepartamento as $fkdepartamento) { ?><option value="<?php echo $fkdepartamento["departamento"]?>" <?php echo $fkdepartamento["departamento"]==@$frm["departamento"]?"selected":""; ?> ><?php echo $fkdepartamento["ciudad"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtProvincia">
              <?php echo JrTexto::_('Provincia');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 select-ctrl-wrapper select-azul">
              <select id="txtprovincia<?php echo $idgui;?>" name="txtProvincia" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkprovincia))
                foreach ($this->fkprovincia as $fkprovincia) { ?><option value="<?php echo $fkprovincia["provincia"]?>" <?php echo $fkprovincia["provincia"]==@$frm["provincia"]?"selected":""; ?> ><?php echo $fkprovincia["ciudad"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <!--div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDistrito">
              <?php echo JrTexto::_('Distrito');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 select-ctrl-wrapper select-azul">
              <select id="txtdistrito" name="txtDistrito" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              /*if(!empty($this->fkdistrito))
                foreach ($this->fkdistrito as $fkdistrito) { ?><option value="<?php echo $fkdistrito["id_ubigeo"]?>" <?php echo $fkdistrito["id_ubigeo"]==@$frm["distrito"]?"selected":""; ?> ><?php echo $fkdistrito["ciudad"] ?></option><?php } */?></select>
                                  
              </div>
            </div-->

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCiudad">
              <?php echo JrTexto::_('Ciudad');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtCiudad<?php echo $idgui;?>" name="txtCiudad" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["ciudad"];?>">
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveUbigeo" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('ubigeo'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
  var cargardatos5a459bea6703600=function(obj,data,returnobj){
      var midata=data||null; 
      console.log(midata);     
      $.ajax({
        url: _sysUrlBase_+'/ubigeo/buscarjson/?json=true',
        type: 'POST',
        dataType: 'json',
        data: midata,
      }).done(function(resp){
        if(resp.code=='ok'){                  
            $('option:first',obj).siblings().remove();
            if(resp.data!=''){
                $.each(resp.data,function(i,v){
                  obj.append('<option value="'+v[returnobj]+'">'+v["ciudad"]+'</option>');
              });
            }
            if(returnobj=='departamento'){
              $cbdepa=$('#txtdepartamento<?php echo $idgui;?>');
              var copt=$('option',$cbdepa).length;
              if(copt>1)
                $('option',$cbdepa).eq(1).prop('selected',true);
                $cbdepa.trigger('change');
                return;
            }
            if(returnobj=='provincia'){
              $cbpro=$('#txtprovincia<?php echo $idgui;?>');
              var copt=$('option',$cbpro).length;
              if(copt>1)
                $('option',$cbpro).eq(1).prop('selected',true);
                $cbpro.trigger('change');
                return;
            }            
        }else{              
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
      }).fail(function(xhr, textStatus, errorThrown) {        
          console.log(xhr.responseText);
      });
  }
  $('#txtpais<?php echo $idgui;?>').change(function(ev){
    var d={};
    d.pais=$('#txtpais<?php echo $idgui;?>').val();
    d.return='departamento';
    console.log(d);
    if(d.pais==''){
        $('#txtdepartamento<?php echo $idgui;?> option:first').siblings().remove();
        $('#txtprovincia<?php echo $idgui;?> option:first').siblings().remove();        
    }else{
        cargardatos5a459bea6703600($('#txtdepartamento<?php echo $idgui;?>'),d,'departamento');     
    }
  });

  $('#txtdepartamento<?php echo $idgui;?>').change(function(ev){
    var d={};
    d.pais=$('#txtpais<?php echo $idgui;?>').val();
    d.departamento=$('#txtdepartamento<?php echo $idgui;?>').val();
    d.return='provincia';
    console.log(d);
    if(d.departamento==''){
        $('#txtprovincia<?php echo $idgui;?> option:first').siblings().remove();        
    }else{
        cargardatos5a459bea6703600($('#txtprovincia<?php echo $idgui;?>'),d,'provincia');     
    }
  });

$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'ubigeo', 'saveUbigeo', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Ubigeo"))?>');
      }
     }
  });  
});
</script>

