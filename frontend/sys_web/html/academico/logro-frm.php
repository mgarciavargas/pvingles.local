<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<style type="text/css">
  form .form-group{ padding: 1ex;}
</style>
<div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <?php if($this->documento->plantilla!='modal'){?><div class="panel-heading bg-blue">
        <div><?php echo JrTexto::_('Logro'); ?><small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></div>
        <div class="clearfix"></div>
      </div><?php } ?>      
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="idlogro" id="idlogro<?php echo $idgui ?>" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTipo">
              <?php echo JrTexto::_('Type');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">           
                <div class="select-ctrl-wrapper select-azul">
                  <select name="cbtipo" id="cbtipo<?php echo $idgui ?>" class="form-control select-ctrl">                     
                      <option value="1" <?php echo @$frm["tipo"]==1?'selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Certificate"))?></option>
                      <option value="2" <?php echo @$frm["tipo"]==2?'selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Medalla"))?></option>
                  </select>
                </div>
              </div>
          </div>


          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTitulo">
              <?php echo JrTexto::_('Titulo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="titulo<?php echo $idgui ?>" name="txtTitulo" required="required" class="form-control" value="<?php echo @$frm["titulo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDescripcion">
              <?php echo JrTexto::_('Descripcion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <textarea id="descripcion<?php echo $idgui ?>" name="txtDescripcion" class="form-control" ><?php echo @trim($frm["descripcion"]); ?></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtPuntaje">
              <?php echo JrTexto::_('Puntaje');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="number" id="puntaje<?php echo $idgui ?>" name="txtPuntaje" required="required" class="form-control" value="<?php echo @$frm["puntaje"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["estado"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["estado"];?>"  data-valueno="0" data-value2="<?php echo @$frm["estado"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txtEstado" id="estado<?php echo $idgui ?>" value="<?php echo !empty($frm["estado"])?$frm["estado"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12">
              <?php echo JrTexto::_('Image');?> 
              </label>
              <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                <?php $img=!empty($frm["imagen"])?($this->documento->getUrlBase().'/'.$frm["imagen"]):$this->documento->getUrlStatic().'/media/imagenes/cursos/nofoto.jpg'; ?>
                <img src="<?php echo @$img; ?>" alt="foto" class="frmfoto changefoto cargaimagen<?php echo $idgui ?> img-responsive thumbnail centrado" data-url=".cargaimagen<?php echo $idgui ?>"  data-tipo="image" title="<?php echo JrTexto::_("Click here to change te exam cover"); ?>">
                <input type="hidden" name="imagen" id="imagen<?php echo $idgui ?>" value="<?php echo @$img; ?>">                                 
              </div>
            </div>
           <hr class="frmlinea">
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveLogro" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning cerrarmodal" href="<?php echo JrAplicacion::getJrUrl(array('logro'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $idgui;?>').bind({
   submit: function(event){
    event.preventDefault();
    var btn=$(this);
    var img= $('#imagen<?php echo $idgui ?>').val()||'';
    var img=img.replace(_sysUrlBase_,'');
    var formData = new FormData();
    formData.append('idlogro', $('#idlogro<?php echo $idgui ?>').val());
    formData.append('titulo', $('#titulo<?php echo $idgui ?>').val());
    formData.append('descripcion', $('#descripcion<?php echo $idgui ?>').val()); 
    formData.append('tipo', $('#cbtipo<?php echo $idgui ?>').val());
    formData.append('imagen', img);
    formData.append('puntaje', $('#puntaje<?php echo $idgui ?>').val());
    formData.append('estado', $('#estado<?php echo $idgui ?>').val()); 
    var url=_sysUrlBase_+'/logro/guardarLogro';
    $.ajax({
      url: url,
      type: "POST",
      data:  formData,
      contentType: false,
      processData: false,
      dataType:'json',
      cache: false,
      beforeSend: function(XMLHttpRequest){ btn.attr('disabled', true); },      
      success: function(data)
      {  
         if(data.code=='Error'){
           mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
         }else{
          mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
          if(typeof <?php echo $ventanapadre?> == 'function'){
            <?php echo $ventanapadre?>();
            btn.closest('.modal').find('.cerrarmodal').trigger('click');
          }else return redir('<?php echo JrAplicacion::getJrUrl(array("logro"))?>');
         }
         btn.attr('disabled', false);
      },
      error: function(e){ btn.attr('disabled', false); },
      complete: function(xhr){ btn.attr('disabled', false); }
    });
  }   
});
$('#frm-<?php echo $idgui;?>').on('click','.chkformulario',function(){     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }); 
}).on("click",'.cargaimagen<?php echo $idgui ?>', function(ev){
      ev.preventDefault();
      ev.stopPropagation();
      var txt= '<?php echo JrTexto::_('Select or upload'); ?>';
      selectedfile(ev,this,txt);
      var img=$(this).attr('data-url');
      $(img).load(function(ev){
        $(this).siblings('input').val($(this).attr('src'));
      });
  }) 
</script>