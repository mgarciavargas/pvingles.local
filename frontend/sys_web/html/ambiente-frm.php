<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Ambiente'); ?><small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdambiente" id="pkidambiente" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdlocal">
              <?php echo JrTexto::_('Idlocal');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                
                <?php
                $_idlocal=@$frm["idlocal"];
                ?>
                <select id="txtIdlocal" name="txtIdlocal" class="form-control" required="required">
                <option value="">Seleccione Local</option>
                <?php
                foreach ($this->locales as $local) { ?>
                <option value="<?php echo $local["idlocal"]; ?>" <?php echo $_idlocal===$local["idlocal"]?'selected="selected"':''?>> <?php echo $local["nombre"]?> </option>
                <?php } ?>
                </select>

              
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNumero">
              <?php echo JrTexto::_('Numero');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNumero" name="txtNumero" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["numero"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCapacidad">
              <?php echo JrTexto::_('Capacidad');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtCapacidad" name="txtCapacidad" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["capacidad"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTipo">
              <?php echo JrTexto::_('Tipo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                
                <?php
                $_tipo=@$frm["tipo"];
                ?>
                <select id="txtTipo" name="txtTipo" class="form-control">
                <option value="L" <?php if ($_tipo=='L') echo "selected" ?>>Laboratorio</option>
                <option value="A" <?php if ($_tipo=='A') echo "selected" ?>>Aula</option>
                </select>
                  
              </div>
            </div>

            

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTurno">
              <?php echo JrTexto::_('Turno');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                
                <?php
                $_turno=@$frm["tipo"];
                ?>
                <select id="txtTurno" name="txtTurno" class="form-control">
                <option value="M" <?php if ($_turno=='M') echo "selected" ?>>Mañana</option>
                <option value="T" <?php if ($_turno=='T') echo "selected" ?>>Tarde</option>
                <option value="N" <?php if ($_turno=='N') echo "selected" ?>>Noche</option>
                </select>

               
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveAmbiente" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('ambiente'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Ambiente', 'saveAmbiente', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Ambiente"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Ambiente"))?>');
        <?php endif;?>       }
     }
  });







  
  
});


</script>

