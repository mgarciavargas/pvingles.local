<?php defined("RUTA_BASE") or die(); ?><div class="form-view" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("administrador"));?>"><?php echo JrTexto::_("Dashboard");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('grupos'));?>"><?php echo JrTexto::_('Grupos'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="div_linea"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="x_panel">
        <div class="x_title">
          <a class="btn btn-success btn-xs" href="<?php echo JrAplicacion::getJrUrl(array("Grupos", "agregar"));?>">
          <i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
          <div class="nav navbar-right"></div>
          <div class="clearfix"></div>
        </div>
        <div class="div_linea"></div>
         <div class="x_content">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Iddocente") ;?></th>
                    <th><?php echo JrTexto::_("Idlocal") ;?></th>
                    <th><?php echo JrTexto::_("Idambiente") ;?></th>
                    <th><?php echo JrTexto::_("Idasistente") ;?></th>
                    <th><?php echo JrTexto::_("Fechainicio") ;?></th>
                    <th><?php echo JrTexto::_("Fechafin") ;?></th>
                    <th><?php echo JrTexto::_("Dias") ;?></th>
                    <th><?php echo JrTexto::_("Horas") ;?></th>
                    <th><?php echo JrTexto::_("Horainicio") ;?></th>
                    <th><?php echo JrTexto::_("Horafin") ;?></th>
                    <th><?php echo JrTexto::_("Valor") ;?></th>
                    <th><?php echo JrTexto::_("Valor asi") ;?></th>
                    <th><?php echo JrTexto::_("Regusuario") ;?></th>
                    <th><?php echo JrTexto::_("Regfecha") ;?></th>
                    <th><?php echo JrTexto::_("Matriculados") ;?></th>
                    <th><?php echo JrTexto::_("Nivel") ;?></th>
                    <th><?php echo JrTexto::_("Codigo") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php $i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ $i++; ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $reg["iddocente"] ;?></td>
                    <td><?php echo $reg["idlocal"] ;?></td>
                    <td><?php echo $reg["idambiente"] ;?></td>
                    <td><?php echo $reg["idasistente"] ;?></td>
                    <td><?php echo $reg["fechainicio"] ;?></td>
                    <td><?php echo $reg["fechafin"] ;?></td>
                    <td><?php echo $reg["dias"] ;?></td>
                    <td><?php echo $reg["horas"] ;?></td>
                    <td><?php echo $reg["horainicio"] ;?></td>
                    <td><?php echo $reg["horafin"] ;?></td>
                    <td><?php echo $reg["valor"] ;?></td>
                    <td><?php echo $reg["valor_asi"] ;?></td>
                    <td><?php echo $reg["regusuario"] ;?></td>
                    <td><?php echo $reg["regfecha"] ;?></td>
                    <td><?php echo $reg["matriculados"] ;?></td>
                    <td><?php echo $reg["nivel"] ;?></td>
                    <td><?php echo $reg["codigo"] ;?></td>
                    <td><a class="btn btn-xs lis_ver " href="<?php echo JrAplicacion::getJrUrl(array('grupos'))?>ver/?id=<?php echo $reg["idgrupo"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update" href="<?php echo JrAplicacion::getJrUrl(array("grupos", "editar", "id=" . $reg["idgrupo"]))?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["idgrupo"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php } ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function () {
  $('.btn-eliminar').bind({   
    click: function() {
       var id=$(this).attr('data-id');
       $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'grupos', 'eliminar', id);
          if(res){
            return redir('<?php echo JrAplicacion::getJrUrl(array('grupos'))?>');
          }
        }
      });     
    }
  });
  
  $('.btn-chkoption').bind({
    click: function() {     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
           var res = xajax__('', 'grupos', 'setCampo', id,campo,data);
           if(res) {
            return redir('<?php echo JrAplicacion::getJrUrl(array('grupos'))?>');
           }
        }
      });
    }
  });
  
  $('.table').DataTable( {
    "language": {
            "url": "<?php echo $this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma(); ?>.json"
    }
  });
});
</script>