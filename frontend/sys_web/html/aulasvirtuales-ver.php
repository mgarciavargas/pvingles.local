
<?php 
$pk=$this->pk;
$reg=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Ver Aulasvirtuales'); ?></h2>
        <div class="btn-group btn-group-md" style="float:right">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('aulasvirtuales'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir a listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["aulaid"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["aulaid"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>            
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-md-12">
        <div id="msj-interno">
        <table class="table table-striped">
         <tr>
              <th><?php echo JrTexto::_("Aulaid") ;?> </th>
              <th>:</th> 
          <td><?php echo $reg["aulaid"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idnivel") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idnivel"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idunidad") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idunidad"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idactividad") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idactividad"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Fecha inicio") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["fecha_inicio"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Fecha final") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["fecha_final"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Titulo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["titulo"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Descripcion") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["descripcion"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Moderadores") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["moderadores"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Estado") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["estado"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Video") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["video"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Chat") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["chat"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Notas") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["notas"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Dirigidoa") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["dirigidoa"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Estudiantes") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["estudiantes"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Dni") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["dni"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Fecha creado") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["fecha_creado"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Portada") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["portada"],0,1000)."..."; ?></td>
          </tr>
                    </table>
          </div>
          <hr><div class="text-center">
          <div class="btn-group btn-group-md">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('aulasvirtuales'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir al listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["aulaid"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["aulaid"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>
	        </div>         
	    </div>
</div>
<br />
	</div>
	<script type="text/javascript">
  var asInitVals = new Array();
	$(document).ready(function(){
    
    $('.btnNuevo').click(function(){
      $('#pkaccion').val('guardar');
      $('#pkaulaid').val('');
      $('.img-thumbnail').attr("style","display:none");
    });

    $('.editar').click(function(){
      $('.img-thumbnail').removeAttr("style");
      var id=$(this).attr("data-reg");
          addFancyAjax("<?php echo JrAplicacion::getJrUrl(array('Aulasvirtuales', 'frm'))?>?tpl=b&acc=Editar&id="+id, true);
          
    });

    $('.eliminar').click(function(){
     if(confirm("<?php echo JrTexto::_('¿Desea eliminar el registro seleccionado?');?>")) {
        var id=$(this).attr("data-reg");
        var res = xajax__('', 'Aulasvirtuales', 'eliminarAulasvirtuales',id)
        if(res) {
          agregar_msj_interno('success', "<?php echo JrTexto::_('Registro eliminado');?>");
                    recargarpagina(false);
                    } 
        $('.alert').fadeOut(4000);
      }
    });
	});
</script>