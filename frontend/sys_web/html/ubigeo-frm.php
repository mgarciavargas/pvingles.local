<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <?php if($this->documento->plantilla!='modal'){?><div class="panel-heading bg-blue">
        <h3><?php echo JrTexto::_('Ubigeo'); ?><small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></h3>
        <div class="clearfix"></div>
      </div><?php } ?>      
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkId_ubigeo" id="pkid_ubigeo" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtPais">
              <?php echo JrTexto::_('Pais');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtpais" name="txtPais" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkpais))
                foreach ($this->fkpais as $fkpais) { ?><option value="<?php echo $fkpais["id_ubigeo"]?>" <?php echo $fkpais["id_ubigeo"]==@$frm["pais"]?"selected":""; ?> ><?php echo $fkpais["ciudad"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDepartamento">
              <?php echo JrTexto::_('Departamento');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtdepartamento" name="txtDepartamento" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkdepartamento))
                foreach ($this->fkdepartamento as $fkdepartamento) { ?><option value="<?php echo $fkdepartamento["id_ubigeo"]?>" <?php echo $fkdepartamento["id_ubigeo"]==@$frm["departamento"]?"selected":""; ?> ><?php echo $fkdepartamento["ciudad"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtProvincia">
              <?php echo JrTexto::_('Provincia');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtprovincia" name="txtProvincia" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkprovincia))
                foreach ($this->fkprovincia as $fkprovincia) { ?><option value="<?php echo $fkprovincia["id_ubigeo"]?>" <?php echo $fkprovincia["id_ubigeo"]==@$frm["provincia"]?"selected":""; ?> ><?php echo $fkprovincia["ciudad"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDistrito">
              <?php echo JrTexto::_('Distrito');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtdistrito" name="txtDistrito" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkdistrito))
                foreach ($this->fkdistrito as $fkdistrito) { ?><option value="<?php echo $fkdistrito["id_ubigeo"]?>" <?php echo $fkdistrito["id_ubigeo"]==@$frm["distrito"]?"selected":""; ?> ><?php echo $fkdistrito["ciudad"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCiudad">
              <?php echo JrTexto::_('Ciudad');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtCiudad" name="txtCiudad" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["ciudad"];?>">
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveUbigeo" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('ubigeo'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'ubigeo', 'saveUbigeo', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Ubigeo"))?>');
      }
     }
  });

 
  
});


</script>

