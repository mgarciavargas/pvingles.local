<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Bolsa_publicaciones'">&nbsp;<?php echo JrTexto::_('Bolsa_publicaciones'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdpublicacion" id="pkidpublicacion" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdempresa">
              <?php echo JrTexto::_('Idempresa');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdempresa" name="txtIdempresa" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idempresa"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTitulo">
              <?php echo JrTexto::_('Titulo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTitulo" name="txtTitulo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["titulo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDescripcion">
              <?php echo JrTexto::_('Descripcion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <textarea id="txtDescripcion" name="txtDescripcion" class="form-control" ><?php echo @trim($frm["descripcion"]); ?></textarea>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtSueldo">
              <?php echo JrTexto::_('Sueldo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtSueldo" name="txtSueldo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["sueldo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNvacantes">
              <?php echo JrTexto::_('Nvacantes');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNvacantes" name="txtNvacantes" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nvacantes"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDisponibilidadeviaje">
              <?php echo JrTexto::_('Disponibilidadeviaje');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["disponibilidadeviaje"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["disponibilidadeviaje"];?>"  data-valueno="0" data-value2="<?php echo @$frm["disponibilidadeviaje"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["disponibilidadeviaje"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txtDisponibilidadeviaje" value="<?php echo !empty($frm["disponibilidadeviaje"])?$frm["disponibilidadeviaje"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDuracioncontrato">
              <?php echo JrTexto::_('Duracioncontrato');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDuracioncontrato" name="txtDuracioncontrato" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["duracioncontrato"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtXtiempo">
              <?php echo JrTexto::_('Xtiempo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtXtiempo" name="txtXtiempo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["xtiempo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFecharegistro">
              <?php echo JrTexto::_('Fecharegistro');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input name="txtFecharegistro" id="txtFecharegistro" class="verdate form-control col-md-7 col-xs-12" required="required" type="date" value="<?php echo @$frm["fecharegistro"];?>">   
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFechapublicacion">
              <?php echo JrTexto::_('Fechapublicacion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input name="txtFechapublicacion" id="txtFechapublicacion" class="verdate form-control col-md-7 col-xs-12" required="required" type="date" value="<?php echo @$frm["fechapublicacion"];?>">   
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCambioderesidencia">
              <?php echo JrTexto::_('Cambioderesidencia');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["cambioderesidencia"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["cambioderesidencia"];?>"  data-valueno="0" data-value2="<?php echo @$frm["cambioderesidencia"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["cambioderesidencia"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txtCambioderesidencia" value="<?php echo !empty($frm["cambioderesidencia"])?$frm["cambioderesidencia"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtMostrar">
              <?php echo JrTexto::_('Mostrar');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["mostrar"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["mostrar"];?>"  data-valueno="0" data-value2="<?php echo @$frm["mostrar"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["mostrar"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txtMostrar" value="<?php echo !empty($frm["mostrar"])?$frm["mostrar"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveBolsa_publicaciones" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('bolsa_publicaciones'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
  
  $('#txtfecharegistro').datetimepicker({
    lang:'es',
    timepicker:false,
    format:'Y/m/d'
  });
  $('#txtfechapublicacion').datetimepicker({
    lang:'es',
    timepicker:false,
    format:'Y/m/d'
  });          
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'bolsa_publicaciones', 'saveBolsa_publicaciones', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Bolsa_publicaciones"))?>');
      }
     }
  });

 
  
});

$('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }
  });

</script>

