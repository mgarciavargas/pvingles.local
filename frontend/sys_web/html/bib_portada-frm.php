<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Bib_portada'); ?><small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkId_portada" id="pkid_portada" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">

          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFoto">
              <?php echo JrTexto::_('Foto');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               
              <div class="row">
                  <input type="hidden" name="txtFoto" id="txtFoto">
                  <input type="hidden" name="txtFoto_old" value="<?php echo @$frm["foto"];?>">
                  <div class="col-md-12">
                    <div class="img-container" style="position: relative;">
                      <input type="file" class="cargarfile" data-tipo="image" data-target="hImgPortada" accept="image/*" style="position: absolute; opacity: 0; height: 150px;" >
                      <img src="<?php echo !empty($frm["foto"])?$this->documento->getUrlBase().$frm["foto"]:($this->documento->getUrlStatic()."/media/web/nofoto.jpg");?>" class="img-responsive"  alt="imgfoto"  id="imgPortada" style="height: 150px; display: inline;" required>
                      <input type="hidden" name="hImgPortada" id="hImgPortada" value="<?php echo @$frm["foto"];?>">
                    </div>
                  </div>
                </div>            
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                  <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["estado"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["estado"];?>"
                data-valueno="0" 
                data-value2="<?php echo @$frm["estado"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txtEstado" value="<?php echo @$frm["estado"];?>" > 
                 </a>
                                                      
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveBib_portada" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('bib_portada'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
$('.chkformulario').bind({
    click: function() {     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($(this).hasClass('fa-circle-o')){
        $(this).addClass('fa-check-circle').removeClass('fa-circle-o');
        data=1;
        $(this).find('span').text('Activo');       
      } else{
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
        $(this).find('span').text('Inactivo'); 
      }
     
      $(this).find('input').val(data);
    } 
  });


  subirmedia=function(tipo,file, $target){
    var formData = new FormData();
    formData.append("tipo",tipo);
    formData.append("filearchivo",file[0].files[0]);
    $.ajax({
      url: '<?php echo $this->documento->getUrlBase(); ?>/bib_portada/subirarchivo',
      type: "POST",
      data:  formData,
      contentType: false,
      processData: false,
      dataType :'json',
      cache: false,
      processData:false,
      xhr:function(){
        var xhr = new window.XMLHttpRequest();
        //Upload progress
        xhr.upload.addEventListener("progress", function(evt){

          if (evt.lengthComputable) {
            var percentComplete = Math.floor((evt.loaded*100) / evt.total);            
            $('#divprecargafile .progress-bar').width(percentComplete+'%');
            $('#divprecargafile .progress-bar span').text(percentComplete+'%');
          }
        }, false);
        //Download progress
        xhr.addEventListener("progress", function(evt){
          if (evt.lengthComputable) {
            var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
            //Do something with download progress
            //console.log(percentComplete);
          }
        }, false);
        return xhr;
      },
      beforeSend: function(XMLHttpRequest){
        div=$('#divprecargafile');
        $('#divprecargafile').removeClass('progress-bar-danger progress-bar-success progress-bar-striped progress-bar-animated').addClass('progress-bar-striped progress-bar-animated');        
        $('#divprecargafile .progress-bar').width('0%');
        $('#divprecargafile .progress-bar span').text('0%'); 
        $('#divprecargafile').show('fast'); 
        $('#btn-saveBib_portada').attr('disabled','disabled');
      },      
      success: function(data)
      {        
        if(data.code==='ok'){
          $('#divprecargafile .progress-bar').width('100%');
          $('#divprecargafile .progress-bar').html('Complete <span>100%</span>');
          $('#divprecargafile').addClass('progress-bar-success').removeClass('progress-bar-animated');
          if(tipo=='image'){
            $target.siblings('#imgPortada').attr('src', _sysUrlStatic_+'/libreria/'+tipo+'/'+data.namelink);
          }else if(tipo=='pdf'){
            $target.closest('.btn-default').removeClass('btn-default').addClass('btn-info');
          }
          $target.attr('value', data.namelink);
          mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
          $('#btn-saveBib_portada').removeAttr('disabled');
        }else{
            $('#divprecargafile').addClass('progress-bar-warning progress-bar-animated');
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
            return false;
        }
      },
      error: function(e) 
      {
          $('#divprecargafile').addClass('progress-bar-danger progress-bar-animated');
           $('#divprecargafile .progress-bar').html('Error <span>-1%</span>'); 
          mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',e,'warning');
          return false;
      },
      complete: function(xhr){
         $('#divprecargafile .progress-bar').html('Complete <span>100%</span>'); 
         $('#divprecargafile').addClass('progress-bar-success progress-bar-animated').hide('slow');
      }
    });
  };
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Bib_portada', 'saveBib_portada', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      /*if(res){
        <?php //if(!empty($this->parent)):?>
        if(typeof window.<?php// echo $this->parent?> == 'function') {
          return <?php// echo $this->parent?>(res);
        } else {*/
         return redir('<?php echo JrAplicacion::getJrUrl(array("Bib_portada"))?>');
        /*}
        <?php //else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Bib_portada"))?>');
        <?php //endif;?> */      
     }
  });

  $('.cargarfile').on('change',function(e){           
     var file=$(this);
     var tipo = $(this).attr('data-tipo');
     var target = '#'+$(this).attr('data-target');
     var $target = $(target);
    if(file.val()=='') return false;
    subirmedia(tipo,file, $target);
    e.preventDefault();
    e.stopPropagation();
  });
    
});


function result_archivo(result, mensaje,txt) {
  if(result) {
    $("#txt"+txt).val(mensaje);
    agregar_msj_interno2("success", '<?php echo JrTexto::_("File updated successfully");?>',"msj-interno2",true);
    var tipo=$("#id"+txt).attr("data-type");
    if(tipo=="imagen"||tipo=="video"||tipo=="audio")
      $("#id"+txt).removeAttr("style").attr("src", "http://localhost/pvingles" + mensaje );
    else
      $("#id"+txt).removeAttr("style").attr("href",  "http://localhost/pvingles" + mensaje );
    }else{
      agregar_msj_interno2("warning", mensaje,"msj-interno2",true);
    }
}
  
</script>

