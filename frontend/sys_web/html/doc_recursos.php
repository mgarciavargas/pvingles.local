<div class="container">
 	<div class="row">     		
 		<div class="col-md-4 col-sm-6 col-xs-12"><br>
 			<div class="panel panel-danger" >
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo JrTexto::_("Test"); ?></h3>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				</div>
				<div class="panel-body" >
					<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/imagenes/children.png" class="img-responsive" width="100%">
				</div>
				<div class="panel-footer">
					<a href="<?php echo $this->documento->getUrlSitio(); ?>/test/agregar" class="btn pull-left">crear</a>
					<a href="<?php echo $this->documento->getUrlSitio(); ?>/test/listar" class="btn pull-right">ver todos</a><div class="clearfix"></div>
				</div>				
			</div> 		
 			<div class="panel panel-danger">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo JrTexto::_("Activities"); ?></h3>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				</div>
				<div class="panel-body" >
					<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/imagenes/children.png" class="img-responsive" width="100%">				
				</div>
				<div class="panel-footer">
					<a href="<?php echo $this->documento->getUrlSitio(); ?>/recursos/agregar" class="btn pull-left">crear</a>
					<a href="<?php echo $this->documento->getUrlSitio(); ?>/recursos/listar" class="btn pull-right">ver todos</a><div class="clearfix"></div>
				</div>
			</div>
 		</div>
 		<div class="col-md-4 col-sm-6 col-xs-12"><br>
 			<div class="panel panel-danger">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo JrTexto::_("Workbooks"); ?></h3>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				</div>
				<div class="panel-body" >
					<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/imagenes/children.png" class="img-responsive" width="100%">				
				</div>
				<div class="panel-footer">
					<a href="<?php echo $this->documento->getUrlSitio(); ?>/workbooks/agregar" class="btn pull-left">crear</a>
					<a href="<?php echo $this->documento->getUrlSitio(); ?>/workbooks/listar" class="btn pull-right">ver todos</a><div class="clearfix"></div>
				</div>
			</div>
 		</div>
 		<div class="col-md-4 col-sm-6 col-xs-12"><br>
 			<div class="panel panel-danger">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo JrTexto::_("GuideLines"); ?></h3>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				</div>
				<div class="panel-body" >
					<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/imagenes/children.png" class="img-responsive" width="100%">				
				</div>
				<div class="panel-footer">
					<a href="<?php echo $this->documento->getUrlSitio(); ?>/test/agregar" class="btn pull-left">crear</a>
					<a href="<?php echo $this->documento->getUrlSitio(); ?>/test/listar" class="btn pull-right">ver todos</a><div class="clearfix"></div>
				</div>
			</div>
 		</div> 		
 	</div>    	
</div>
<script type="text/javascript">
	$(document).ready(function(){		
	$('.pnlclickhref').click(function(e){
		e.preventDefault();
		href=$(this).attr('href');		
		//redir(href);
	});
 	$('.graficocircle').graficocircle();
 });
</script>