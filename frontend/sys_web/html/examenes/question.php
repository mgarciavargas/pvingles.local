<?php
$examen = $this->examen;
$preguntas=@$this->preguntas;
?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_verdad_falso.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_ordenar.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_fichas.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_imagen_puntos.css">
<style>
    #exam-preguntas{
        background: #fff;
    }

    #exam-preguntas .panel.preguntas{
        margin-top: 1em;
    }

    #exam-preguntas .panel.preguntas .panel-body{
        min-height: 500px;
    }

    #exam-preguntas .panel.preguntas .contenedor-pregs .preg{
        border: 1px solid #aaa;
        border-radius: 4px;
        margin-bottom: 12px;
        padding-bottom: 12px;
        padding-top: 12px;
        position: relative;
    }

    #exam-preguntas .panel.preguntas .contenedor-pregs .preg audio{
        min-height: 64px;
    }

    #exam-preguntas .panel.preguntas .contenedor-pregs .preg .tmpl .btns-tmpl{
        background: rgba(0, 0, 0, 0.35);
        font-size: 1.6em;
        display: none;
        height: 100%;
        position: absolute;
        /*right: 15px;*/
        text-align: center;
        width: 95%;
        z-index: 20;
    }

    #exam-preguntas .panel.preguntas .contenedor-pregs .preg .tmpl:hover .btns-tmpl{
        display: block;
    }

    #exam-preguntas .panel.preguntas .contenedor-pregs .preg .btns-tmpl>a{
        margin-left: 5%;
        padding: 5px 15px;
        /*margin-left: 5px;
        padding: 2px 5px;*/
        top: 50%;
        margin-top: -30px;
    }
    #exam-preguntas .panel.preguntas .contenedor-pregs .preg .btns-tmpl>a>i.fa{
        font-size: 3em;
    }
    
    #exam-preguntas .panel.preguntas .contenedor-pregs .preg .ejercicio{
        min-height: 60px;
        word-wrap: break-word;
    }

    #exam-preguntas .panel.preguntas .contenedor-pregs .preg .mask{
        background: #fafafa;
        border: 3px dashed #ccc;
        cursor: pointer;
        min-height: 200px;
        -moz-transition: all .15s ease-in;
        -webkit-transition: all .15s ease-in;
        -o-transition: all .15s ease-in;
        transition: all .15s ease-in;
    }

    #exam-preguntas .panel.preguntas .contenedor-pregs .preg .mask:hover{
        background: #f0f0f0;
        border-color: #bbb;
        color: #000;
        -moz-transition: all .15s ease-in;
        -webkit-transition: all .15s ease-in;
        -o-transition: all .15s ease-in;
        transition: all .15s ease-in;
    }

    #exam-preguntas .panel.preguntas .contenedor-pregs .preg .mask:before{
        content: "\f044";
        font: normal normal normal 35px/1 FontAwesome;
        left: 45%;
        position: absolute;
        top: 45%;
    }

    #exam-preguntas .panel.preguntas .contenedor-pregs .preg .close{
        opacity: 1;
    }

    .modal.edicion_tmpl .modal-body .zona{
        border-bottom: 2px solid #eee;
        border-radius: 5px;
        padding: 2.5px 0;
    }
    .modal.edicion_tmpl .modal-body .zona:last-child{
        border: none;
    }

    .modal.edicion_tmpl .modal-body .zona-tiempo-puntaje{
        text-align: center;
    }
    .modal.edicion_tmpl .modal-body .zona-tiempo-puntaje .eje-panelinfo{
        display: inline-block;
        margin-left: .5em;
        max-width: 15%;
    }
    .modal.edicion_tmpl .modal-body .zona-tiempo-puntaje .eje-panelinfo input{
        text-align: center;
    }

    .modal.edicion_tmpl .modal-body .habilidades div{
        margin-bottom: 2.5px;
        margin-top: 2.5px;
        color: #fff;
        text-align: center;
    }
    .modal.edicion_tmpl .modal-body .habilidades .btn-skill{
        background-color: #b9b9b9;
        padding: 8px;
        position: relative;
        margin-bottom: 0%;
        width: 100%;
    }
    
    .modal.edicion_tmpl .modal-body .habilidades.edicion{
        cursor: pointer;
    }

    .modal.edicion_tmpl .modal-body .habilidades.edicion .btn-skill:before {
        background: white;
        border: 1px solid #888;
        content: ' ';
        height: 17px;
        left: 3px;
        padding-top: 2px;
        position: absolute;
        top: 3px;
        width: 17px;
    }

    .modal.edicion_tmpl .modal-body .habilidades.edicion .btn-skill.active:before {
        color: #777;
        content: "\f00c";
        font: normal normal normal 12px/1 FontAwesome;
    }

    .modal.edicion_tmpl .modal-body .habilidades .skill-container:nth-child(6n+1)>.btn-skill.active{ background-color: #f59440; }
    .modal.edicion_tmpl .modal-body .habilidades .skill-container:nth-child(6n+2)>.btn-skill.active{ background-color: #337ab7; }
    .modal.edicion_tmpl .modal-body .habilidades .skill-container:nth-child(6n+3)>.btn-skill.active{ background-color: #5cb85c; }
    .modal.edicion_tmpl .modal-body .habilidades .skill-container:nth-child(6n+4)>.btn-skill.active{ background-color: #5bc0de; }
    .modal.edicion_tmpl .modal-body .habilidades .skill-container:nth-child(6n+5)>.btn-skill.active{ background-color: #7e60e0; }
    .modal.edicion_tmpl .modal-body .habilidades .skill-container:nth-child(6n+6)>.btn-skill.active{ background-color: #d9534f; }
    .tmpl-ejerc .titulo_descripcion{
       background: rgba(169, 175, 173, 0.84);
       border-radius: 1ex;
       padding: 0.5ex 2.5ex;
       border: 1px solid #ccc;
       margin: 10px 0px 5px 0px;
    }
    .tmpl-ejerc .ejercicio{
        margin: 0ex;
        background: rgba(223, 230, 228, 0.66);
        border-radius: 1ex;
        padding: 0.5ex 2.5ex;
        border: 1px solid #ccc;
    }
</style>

<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_completar.css">
<div class="row" id="exam-preguntas"><div class="col-xs-12">
    <div class="panel panel-primary preguntas">
        <div class="panel-heading"> 
            <div><ol class="breadcrumb" style="margin: 0px; padding: 0px; background:rgba(187, 197, 184, 0);">
                  <li><a href="<?php echo $this->documento->getUrlBase() ?>/examenes/" style="color:#fff"><?php echo JrTexto::_("Assessment"); ?></a></li>
                  <li><a href="#" style="color:#fff"><?php echo ucfirst(JrTexto::_("Questions")); ?></a></li>                  
                  <li class="active"  style="color:#ccc"><?php echo @$examen["titulo"]; ?></li>
                </ol>
            </div>
        </div> 
        <div class="panel-body" style="font-family: '<?php echo $examen['fuente']; ?>'; font-size: <?php echo $examen['fuentesize'].'px'; ?>;"> 
            <div class="col-xs-12 contenedor-pregs">
            <?php if(!empty($preguntas))
                  foreach ($preguntas as $pregunta){                
                    $codeidgui=uniqid();
                    ?>
                    <div class="row preg" id="preg_<?php echo $codeidgui; ?>" idpadre="<?php echo @$pregunta["P"][0]["idcontenedor"]; ?>">
                        <div class="col-xs-12 botones-elegir-plantilla">
                            <!--
                            <a href="#" class="btn btn-primary slide-sidebar-right elegirtpl" data-source="http://127.0.0.1/frontend/sidebar_pages/lista_plantillas">Choose template</a>
                            -->
                            <a href="#" class="btn btn-red close" title="Delete"><i class="fa fa-times"></i></a>
                        </div>
                        <div class="col-xs-12 col-sm-12 aquicargarplantilla">
                            <?php if($pregunta["F"]){
                                   $preg=$pregunta["F"][0];
                                    $tpltipo=$preg["template"];
                                    $tpl=($tpltipo=='image'||$tpltipo=='audio'||$tpltipo=='video')?'media':'ejerc';
                                    ?>
                                   <div class="col-xs-12 col-sm-6 tmpl tmpl-<?php echo $tpl; ?>" data-tipo="<?php echo $tpltipo; ?>" data-cls="<?php echo $tpltipo; ?>_question" data-idpregunta="<?php echo $preg["idpregunta"]; ?>" data-habilidades="<?php echo $preg["habilidades"]; ?>">
                                        <div class="btns-tmpl">
                                            <a href="#" class="btn btn-primary elegirtpl istooltip slide-sidebar-right" data-source="<?php echo $this->documento->getUrlBase(); ?>/frontend/sidebar_pages/lista_plantillas" title="<?php echo JrTexto::_("Choose template"); ?>"><i class="fa fa-th-large"></i></a>
                                            <a href="#" class="btn btn-blue editar istooltip" data-tipo="<?php echo $tpltipo; ?>" data-url=".<?php echo $tpltipo; ?>_question" title="<?php echo JrTexto::_("editar"); ?>"><i class="fa fa-pencil-square"></i></a>
                                            <a href="#" class="btn btn-red borrar istooltip" title="<?php echo JrTexto::_("Delete"); ?>"><i class="fa fa-window-close"></i></a>
                                        </div>
                                        <?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$preg["ejercicio"]); ?>
                                   </div>
                               <?php  
                             } if($pregunta["P"]){
                                foreach ($pregunta["P"] as $preg) {
                                    $tpltipo=$preg["template"];
                                    $tpl=($tpltipo=='image'||$tpltipo=='audio'||$tpltipo=='video')?'media':'ejerc';?>
                                   <div class="col-xs-12 col-sm-6 tmpl tmpl-<?php echo $tpl; ?>" data-tmpl="<?php echo $tpltipo; ?>" data-cls="<?php echo $tpltipo; ?>_question" data-idpregunta="<?php echo $preg["idpregunta"]; ?>" data-habilidades="<?php echo $preg["habilidades"]; ?>" data-tiempo="<?php echo $preg["tiempo"]; ?>" data-puntaje="<?php echo $preg["puntaje"]; ?>">
                                       <div class="btns-tmpl">
                                           <a href="#" class="btn btn-primary elegirtpl istooltip slide-sidebar-right" data-source="<?php echo $this->documento->getUrlBase(); ?>/frontend/sidebar_pages/lista_plantillas" title="<?php echo JrTexto::_("Choose template"); ?>"><i class="fa fa-th-large"></i></a>
                                           <a href="#" class="btn btn-blue editar istooltip" title="<?php echo JrTexto::_("editar"); ?>"><i class="fa fa-pencil-square"></i></a>
                                           <a href="#" class="btn btn-info showdemo istooltip" title="<?php echo ucfirst(JrTexto::_('video demo')); ?>"><i class="fa fa-video-camera"></i></a>
                                           <a href="#" class="btn btn-red borrar istooltip" title="<?php echo JrTexto::_("Delete"); ?>"><i class="fa fa-window-close"></i></a>
                                       </div>
                                        <?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$preg["ejercicio"]); ?>
                                   </div>
                               <?php } 
                             }
                             ?>
                             <div class="col-xs-12 col-sm-6 mask slide-sidebar-right elegirtpl" data-source="<?php echo $this->documento->getUrlBase();?>/frontend/sidebar_pages/lista_plantillas"></div>
                         </div>
                          
                    </div>
             <?php } ?>                
            </div>
            <div class="botones-globales">
                <a href="#" class="btn btn-primary add-question"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('question')?></a>

                <div class="row preg hidden" id="blank_question">
                    <div class="col-xs-12 botones-elegir-plantilla">
                        <!--
                        <a href="#" class="btn btn-primary slide-sidebar-right elegirtpl" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/lista_plantillas"><?php echo ucfirst(JrTexto::_('choose')).' '.JrTexto::_('template'); ?></a>
                        -->
                        <a href="#" class="btn btn-red close" title="<?php echo ucfirst(JrTexto::_('delete')); ?>"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="col-xs-12 col-sm-12 aquicargarplantilla">
                        <div class="col-xs-12 col-sm-6 mask slide-sidebar-right elegirtpl" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/lista_plantillas"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div></div>


<div id="modal_body_edicion_ejerc" class="hidden" style="display: none !important;">
    <div class="zona zona-titulo-descripcion">
        <div class="tab-title-zone">
            <h3 class="live-edit addtexttitle"><?php echo JrTexto::_('click here to').' '.JrTexto::_('add').' '.JrTexto::_('title') ?></h3>
            <input type="hidden" class="valin nopreview" name="titulo" data-nopreview=".addtexttitle">
            <div class="space-line"></div>
        </div>
        <div class="tab-description">
            <p class="live-edit addtextdescription"><?php echo JrTexto::_('click here to').' '.JrTexto::_('add').' '.JrTexto::_('description') ?></p>
            <input type="hidden" class="valin nopreview" name="descripcion" data-nopreview=".addtextdescription">
        </div>
    </div>
    <div class="zona zona-tiempo-puntaje">
        <div id="panel-puntaje" class="eje-panelinfo">
            <div class="titulo btn-lilac"><?php echo ucfirst(JrTexto::_('score')); ?></div>
            <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="puntaje" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 50" maxlength="3">
            <div class="info-show showonpreview" style="display: none;">00</div>
        </div>
        <div id="panel-tiempo" class="eje-panelinfo">
            <div class="titulo btn-yellow"><?php echo ucfirst(JrTexto::_('time')); ?></div>
            <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="tiempo" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50">
            <div class="info-show showonpreview" style="display: none;">00</div>
        </div>
    </div> 
    <div class="zona zona-plantilla-edicion"></div>
    <div class="zona zona-habilidades">
        <div class="row habilidades edicion">
            <div class="col-xs-12 ">
            <?php 
            $ihab=0;
            if(!empty($this->habilidades))
            foreach ($this->habilidades as $hab) { $ihab++;?>
                <div class="col-xs-6 col-sm-4 skill-container" title="Click to activate" >
                    <div class="btn-skill vertical-center" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>    
</div>


<section id="msjes_idioma" class="hidden" style="display: none !important; ">
    <input type="hidden" id="select_upload" value='<?php echo ucfirst(JrTexto::_('Select or upload')); ?>'>
    <input type="hidden" id="close_not_saving" value='<?php echo ucfirst(JrTexto::_('Close without saving')); ?>'>
    <input type="hidden" id="save_add_to_question" value='<?php echo ucfirst(JrTexto::_('Save and add to the question')); ?>'>
    <input type="hidden" id="editing_exercise" value='<?php echo ucfirst(JrTexto::_('Editing exercise')); ?>'>
     <input type="hidden" id="attention" value='<?php echo ucfirst(JrTexto::_('Attention')); ?>'>
     <input type="hidden" id="write_tag" value='<?php echo ucfirst(JrTexto::_('write tag')); ?>'>
     <input type="hidden" id="not_show_again" value='<?php echo ucfirst(JrTexto::_('do not show this again')); ?>'>
     <input type="hidden" id="choose_template" value='<?php echo ucfirst(JrTexto::_('choose template')); ?>'>
     <input type="hidden" id="edit" value='<?php echo ucfirst(JrTexto::_('edit')); ?>'>
     <input type="hidden" id="video_demo" value='<?php echo ucfirst(JrTexto::_('video demo')); ?>'>
     <input type="hidden" id="delete" value='<?php echo ucfirst(JrTexto::_('delete')); ?>'>
</section>


<script>
function btnsEjercicio(tipo='', clase='') {
    var _tipo = (tipo!='')?'data-tipo="'+tipo+'"' : '';
    var _url = (clase!='')?'data-url=".'+clase+'"' : '';

    var btnVideoDemo = (tipo=='')?'<a href="#" class="btn btn-info showdemo istooltip" title="'+MSJES_PHP.video_demo+'"><i class="fa fa-video-camera"></i></a>' : '';
    return '<div class="btns-tmpl">'+
        '<a href="#" class="btn btn-primary elegirtpl istooltip slide-sidebar-right" data-source="'+sitio_url_base +'/sidebar_pages/lista_plantillas" title="'+MSJES_PHP.choose_template+'"><i class="fa fa-th-large"></i></a>'+
        '<a href="#" class="btn btn-blue editar istooltip" '+_tipo+' '+_url+' title="'+MSJES_PHP.edit+'"><i class="fa fa-pencil-square"></i></a>'+
        btnVideoDemo+
        '<a href="#" class="btn btn-red borrar istooltip" title="'+MSJES_PHP.delete+'"><i class="fa fa-window-close"></i></a>'+
    '</div>';
}

function crearContenidoMedia() {
    var now = Date.now();
    var $objMedia = $('#exam-preguntas .temporal');
    var tipo = $objMedia.attr('data-tipo');
    var cls = $objMedia.attr('data-clase');
    var idPregActiva = $('.contenedor-pregs .preg.active').attr('id');
    var src = (tipo=="audio")? $objMedia.attr('data-audio') : $objMedia.attr('src');
    var src_audio = _sysUrlStatic_ +'/media/audio/'+src;
    if(tipo=='image'){
        var media_tag = '<img src="'+src+'" id="'+cls+now+'" class="img-responsive center-block">';
    }else if(tipo=='audio'){
        var media_tag = '<audio src="'+src_audio+'" id="'+cls+now+'" class="col-xs-12" controls="true"></audio>';
    }else if(tipo=='video'){
        var media_tag = '<div class="embed-responsive embed-responsive-16by9"><video src="'+src+'" id="'+cls+now+'" class="embed-responsive-item" controls="true"></video></div>';
    }
    var idpregunta=$('#'+idPregActiva+' .tmpl-media').attr('data-idpregunta')||'';
    $('#'+idPregActiva+' .tmpl-media').remove();
    var btnsEdicionEjerc = btnsEjercicio(tipo, cls);
    var html = '<div class="col-xs-12 col-sm-6 tmpl tmpl-media" data-tipo="'+tipo+'" data-cls="'+cls+'" data-idpregunta="'+idpregunta+'">'+btnsEdicionEjerc+media_tag+'</div>';
    $('#'+idPregActiva+' .aquicargarplantilla').prepend(html);
    var idcontenedor=$('#'+idPregActiva).attr('idpadre')||now;
    $('#'+idPregActiva).attr('idpadre',idcontenedor);
    $('.istooltip').tooltip();
    $('#exam-preguntas .temporal').remove();
    $('.contenedor-pregs .preg').removeClass('active');
    var descripcion=$('#'+idPregActiva+' .tmpl-media');
    var data={};
    data.idexamen='<?php echo @$this->idexamen; ?>';
    data.idpregunta=idpregunta;
    data.titulo='';
    data.descripcion='';
    data.ejercicio=media_tag;
    data.idpadre='1';
    data.tiempo='00:00:00';
    data.puntaje='0';
    data.template=tipo;
    data.habilidad='';
    data.contenedor=idcontenedor;
    var obj=$('#'+idPregActiva+' .tmpl-media');
    guardarPreguntas(obj,data,true);
}

var guardarPreguntas=function(obj,midata,ispadre){
    var msjeAttention = '<?php echo JrTexto::_('Attention') ?>';
    var formData = new FormData();
    formData.append("idexamen", midata.idexamen);
    formData.append("idpregunta", midata.idpregunta);
    formData.append("pregunta", midata.titulo);
    formData.append("descripcion", midata.descripcion);
    formData.append("ejercicio", midata.ejercicio);
    formData.append("idpadre", midata.idpadre);
    formData.append("tiempo", midata.tiempo);
    formData.append("puntaje", midata.puntaje);
    formData.append("template", midata.template);
    formData.append("habilidad", midata.habilidad);
    formData.append("idcontenedor", midata.contenedor);
    $.ajax({
        url: _sysUrlBase_+'/examenes/guardarPreguntas',
        type: "POST",
        data:  formData,
        contentType: false,
        dataType :'json',
        cache: false,
        processData:false,
        beforeSend: function(XMLHttpRequest){
            $('#procesando').show('fast'); 
        },
        success: function(data)
        {
            if(data.code==='ok'){
                mostrar_notificacion(msjeAttention, data.msj, 'success');
                var idpregunta=data.new;
                obj.attr('data-idpregunta',idpregunta);

            }else{
                mostrar_notificacion(msjeAttention, data.msj, 'warning');
            }
            $('#procesando').hide('fast');
            return false;
        },
        error: function(xhr,status,error){
            mostrar_notificacion(msjeAttention, status, 'warning');
            $('#procesando').hide('fast');
            return false;
        }               
    });
};

$(document).ready(function() {
    var cargarVideoDemo = function(name_tmpl, name_videodemo='', nombre_comun=''){
        var idDemo = 'demo_'+name_tmpl;
        var checked = false;
        if(localStorage.HideVideoDemo!=undefined) {
            var storage = JSON.parse(localStorage.HideVideoDemo);
            checked = storage['chk_demo_'+name_tmpl];
        }
        if( $('#demo_'+name_tmpl).length==0 && name_videodemo!=''){
            var new_modal_video = $('#modalclone').clone();
            new_modal_video.removeAttr('id').attr('id', idDemo).addClass('video_demo');
            new_modal_video.find('.modal-header #modaltitle').text('Video Demo: '+nombre_comun);
            new_modal_video.find('.modal-body').html('<div class="col-xs-12"><div class="embed-responsive embed-responsive-16by9 mascara_video"><video controls="true" class="valvideo embed-responsive-item vid" src="'+_sysUrlStatic_+'/sysplantillas/'+name_videodemo+'.mp4"></video></div></div><label class="col-xs-12"><input type="checkbox" class="checkbox-ctrl chkDontShow" name="chk_'+idDemo+'" id="chk_'+idDemo+'"> '+MSJES_PHP.not_show_again+'</label>')
            $('body').append(new_modal_video);
            $('#'+idDemo+' .chkDontShow').prop('checked', checked);
        }

        $('#'+idDemo).modal();

        $('#'+idDemo).trigger('resize');
        return idDemo;
    };

    var cargarPlantilla = function(name_tmpl, nombre_comun,tplobj,ideditar){
        var addclase=tplobj.attr('data-clase')||'';
        var ideditar=ideditar||'';
        var idpregunta='';
        var titulo='';
        var descripcion='';
        var habilidades='';
        var tiempo='00:00:00';
        var puntaje='0';

        if(ideditar!='') {
            idpregunta='?idpregunta='+ideditar;
            var tmpl=$('.preg.active .tmpl.editandotmpl');
            titulo=$('.titulo_descripcion .titulo',tmpl).text();
            descripcion=$('.titulo_descripcion .descripcion',tmpl).text();
            habilidades=tmpl.attr('data-habilidades')||'';
            tiempo=tmpl.attr('data-tiempo')||'00:00:00';
            puntaje=tmpl.attr('data-puntaje')||'0';
        }
        var ruta_tmpl = _sysUrlBase_  +'/template/'+name_tmpl+idpregunta;
        var new_modal = $('#modalclone').clone();
        var id_modal = 'edicion_tmpl_'+name_tmpl;
        var modal_body = $('#modal_body_edicion_ejerc').clone();
        var hasTiempo = <?php echo ($examen["tiempo_por"]=='E')? 'false':'true' ;?>;
        var hasPuntaje = <?php echo ($examen["calificacion_por"]=='E')? 'false':'true' ;?>;
        var isNumerico = <?php echo ($examen["calificacion_en"]=='N')? 'true':'false' ;?>;
        if(!hasTiempo && !hasPuntaje) {
            modal_body.find('#panel-puntaje').closest('.zona').remove();
        } else {
            if(!hasTiempo) modal_body.find('#panel-tiempo').remove();
            if(!hasPuntaje || !isNumerico) modal_body.find('#panel-puntaje').remove();
        }
        //else  modal_body.find('#panel-puntaje').hide()
        new_modal.removeAttr('id').attr('id', id_modal).addClass('edicion_tmpl');
       
        $('body').append(new_modal);
        $('#'+id_modal).modal({backdrop: 'static', keyboard: false});

        $.get(ruta_tmpl, function(data) {
            modal_body.find('.zona-plantilla-edicion').html(data);
            $('#'+id_modal+' .modal-header #modaltitle').text(MSJES_PHP.editing_exercise+': '+nombre_comun);
            $('#'+id_modal+' .modal-body').html(modal_body.html());
            //agregamos titulo y descricion
            if(titulo!=''&&titulo!=undefined){
                $('#'+id_modal+' .modal-body .zona-titulo-descripcion .addtexttitle').html(titulo);
                $('#'+id_modal+' .modal-body .zona-titulo-descripcion .tab-title-zone input').val(titulo);
            }
            if(descripcion!=''&&descripcion!=undefined){
              $('#'+id_modal+' .modal-body .zona-titulo-descripcion .addtextdescription').html(descripcion);
              $('#'+id_modal+' .modal-body .zona-titulo-descripcion .tab-description input').val(descripcion);  
            }
            if(habilidades!=''&&habilidades!=undefined){
                var hab=habilidades.split("|");
                var divhabilidades= $('#'+id_modal+' .modal-body .habilidades.edicion');
                divhabilidades.attr('data-habilidades',habilidades);
                $.each(hab,function(i,v){
                    divhabilidades.find('.btn-skill[data-id-skill="'+v+'"]').addClass('active');
                }); 
            }
            if(tiempo!='00:00:00'||tiempo!=undefined){
                var divtiempo= $('#'+id_modal+' .modal-body #panel-tiempo');
                $('input',divtiempo).val(tiempo);
                $('.info-show',divtiempo).text(tiempo);
            }
            if(puntaje!='0'||puntaje!=undefined){
                var divpuntaje= $('#'+id_modal+' .modal-body #panel-puntaje');
                $('input',divpuntaje).val(puntaje);
                $('.info-show',divpuntaje).text(puntaje);
            }
            
            if(addclase!=''){
                $('#'+id_modal).find('.plantilla').attr('data-addclass',addclase);
                if(addclase=='iswrite') $('#'+id_modal).find('#setting-textbox .distractions').hide();
            }
            $('#'+id_modal+' .modal-footer').find('*[data-dismiss="modal"]').addClass('pull-left').text(MSJES_PHP.close_not_saving);
            $('#'+id_modal+' .modal-footer').prepend('<a href="#" class="btn btn-success guardartpl">'+MSJES_PHP.save_add_to_question+'</a>');

            $('#'+id_modal+' .modal-body #panel-tiempo  input').mask("99:99:99");
        });
    };

    $('.btn.add-question').click(function(e) {
        e.preventDefault();
        var new_preg = $('#blank_question').clone(true);
        var idNewPreg = 'preg_'+ Date.now();
        new_preg.removeAttr('id').removeClass('hidden').attr('id', idNewPreg);
        $('.contenedor-pregs').append(new_preg);
    });

    $('.contenedor-pregs')
    .on('click', '.preg .close', function(e) {
        e.preventDefault();
        var idpadre=$(this).closest('.preg').attr('idpadre');
        var res=xajax__('', 'examenes', 'eliminarxPadre', idpadre);
        $(this).closest('.preg').remove();
    })
    .on('click', '.elegirtpl', function(e) {
        e.preventDefault();
        $('.contenedor-pregs .preg').removeClass('active');
        $(this).closest('.preg').addClass('active');
    })
    .on('click', '.btns-tmpl>.editar', function(e) {
        e.preventDefault();
        $('.preg').removeClass('active');
        $(this).closest('.preg').addClass('active');
        var tmpl=$(this).closest('.tmpl');
        var esMultimedia = (tmpl.hasClass('tmpl-media'))? true:false;
        if(esMultimedia){
            var tipo = $(this).attr('data-tipo');
            var cls = $(this).attr('data-url');
            cls = cls.split('.').pop();
            if($('#exam-preguntas .temporal').length==0){
                var tag= tipo;
                if(tag=='image') tag='img';
                $('#exam-preguntas').append('<'+tag+' src="" class="temporal '+cls+' hidden" data-tipo="'+tipo+'" data-clase="'+cls+'">');
            }
            selectedfile(e,this,'Select or Upload', 'crearContenidoMedia');
        }else{
            /* es un ejercicio */
            var name_tmpl = tmpl.attr('data-tmpl');
            tmpl.addClass('editandotmpl');
            var idpregunta=tmpl.attr('data-idpregunta');
            var tplobj=$('.sidebar-right ul.list-group a[data-tmpl="'+name_tmpl+'"]');
            nombre_comun=$('.nombre-tpl',tplobj).text();
            cargarPlantilla(name_tmpl, nombre_comun,tplobj,idpregunta);
        }
    })
    .on('click', '.btns-tmpl>.borrar', function(e) {
        e.preventDefault();
        var idpreg=$(this).closest('.tmpl').attr('data-idpregunta');
        xajax__('', 'examenes', 'eliminarPregunta', idpreg);
        $(this).closest('.tmpl').remove();
    })
    .on('click', '.btns-tmpl>.showdemo', function(e) {
        e.preventDefault();
        var name_tmpl = $(this).closest('.tmpl.tmpl-ejerc').attr('data-tmpl');
        var name_video = $('aside.sidebar-right a[data-tmpl="'+name_tmpl+'"]').attr('data-videodemo');
        var nombre_comun = $('aside.sidebar-right a[data-tmpl="'+name_tmpl+'"] .nombre-tpl').text();
        cargarVideoDemo(name_tmpl, name_video, nombre_comun);
    });

    
    $('#sidebar-right')
    .on('click', '#lista_plantillas .opc-tmpl', function(e) {
        e.preventDefault();
        var name_tmpl = $(this).attr('data-tmpl');
        var nombre_comun = $(this).find('.nombre-tpl').text();
        var esMultimedia = ($(this).attr('data-multimedia')!=undefined)? $(this).attr('data-multimedia'):'false';
        var name_videodemo = $(this).attr('data-videodemo');
        var tplobj=$(this);
        var openVideoDemo = false;
        $('#sidebar-right .close').trigger('click');

        if(localStorage.HideVideoDemo!=undefined) {
            var storage = JSON.parse(localStorage.HideVideoDemo);
            openVideoDemo = storage['chk_demo_'+name_tmpl];
        }
        if(esMultimedia=='false'){ 
            /*cargar video demo y luego plantilla */
            if(name_videodemo!=undefined && name_videodemo!='' && !openVideoDemo){
                var id_modal_videodemo = cargarVideoDemo( name_tmpl, name_videodemo, nombre_comun );
                $('body').one('hidden.bs.modal', '#'+id_modal_videodemo, function (e) {
                    cargarPlantilla( name_tmpl, nombre_comun,tplobj );
                });
            } else {
                cargarPlantilla( name_tmpl, nombre_comun,tplobj );
            }
        } else { 
            /*abrir biblioteca*/
            var tipo = $(this).attr('data-tipo');
            var cls = $(this).attr('data-url');
            cls = cls.split('.').pop();
            if($('#exam-preguntas .temporal').length==0){
                var tag= tipo;
                if(tag=='image') tag='img'
                $('#exam-preguntas').append('<'+tag+' src="" class="temporal '+cls+' hidden" data-tipo="'+tipo+'" data-clase="'+cls+'">');
            }
            selectedfile(e,this,'Select or Upload', 'crearContenidoMedia');
        }
    })
    .on('click', '#lista_plantillas .close', function(e) {
        e.preventDefault();
        $('#lista_plantillas').closest('aside.sidebar-right').removeClass('sidebar-toogled');
        /*$('aside.sidebar-right').html('');*/
    });


    $('body')
    .on('click', '.edicion_tmpl .guardartpl', function(e) {
        e.preventDefault();
        var idModal = $(this).closest('.edicion_tmpl').attr('id');
        var dataclone=$('#'+idModal).find('.plantilla').attr('data-clone')||'';
        var plant = $(this).closest('.edicion_tmpl').attr('id').replace(/edicion_tmpl_/g,'');
        var txtTitulo = $('#'+idModal+' .modal-body .addtexttitle').siblings('input').val()||'';
        var txtDescripcion = $('#'+idModal+' .modal-body .addtextdescription').siblings('input').val()||'';
        if(dataclone!=''){
           $('#'+idModal).find('#generarhtml').trigger('click');
           var htmlaclonar=$('#'+idModal+' '+dataclone).clone(); 
        }else{
           var htmlaclonar=$('#'+idModal+' .modal-body .tpl_plantilla').clone();
        }

        var html_ejerc = htmlaclonar;
        var cls = $('#'+idModal+' .modal-body .plantilla').attr('class');
        var idPregActiva = $('.contenedor-pregs .preg.active').attr('id');
        var targetClass = '.aquicargarplantilla ';        
        var $pregActiva = $('#'+idPregActiva);
        var hayeditando = $('.editandotmpl', $pregActiva);
        var habilidades='';
        var tiempo=$('#'+idModal+' #panel-tiempo input').val()||'00:00:00';
        var puntaje=$('#'+idModal+' #panel-puntaje input').val()||0;
        var frmtiempo=tiempo.split(":");
        var ss=frmtiempo.pop()||'00';
        var min=frmtiempo.pop()||'00';
        var hr=frmtiempo.pop()||'00';
        tiempo=hr+':'+min+':'+ss;

        if(hayeditando.length>0){ //cuando viene de editar
           habilidades=hayeditando.attr('data-habilidades');
           hayeditando.find('.titulo_descripcion .titulo').text(txtTitulo);
           hayeditando.find('.titulo_descripcion .titulo b.npregunta').remove().siblings('b').remove();
           hayeditando.find('.titulo_descripcion .descripcion').text(txtDescripcion);
           var $ejercicio = hayeditando.find('.ejercicio');
           $ejercicio.html(html_ejerc.html());
           hayeditando.attr('data-tiempo',tiempo);
           hayeditando.attr('data-puntaje',puntaje);
           hayeditando.attr('id','grabando');
           hayeditando.removeClass('editandotmpl');
        }else{  //solo para nuevos objetos
            habilidades=$('#'+idModal+' .modal-body .habilidades.edicion').attr('data-habilidades')||'';          
            var btnsEdicionEjerc = btnsEjercicio();
            var panel_titulo_descrip = '<div class="titulo_descripcion"><h3 class="titulo">'+txtTitulo+'</h3><p class="descripcion">'+txtDescripcion+'</p></div>';
            var new_ejerc= '<div class="col-xs-12 col-sm-6 tmpl tmpl-ejerc" data-tmpl="'+plant+'" id="grabando" data-habilidades="'+habilidades+'" data-tiempo="'+tiempo+'" data-puntaje="'+puntaje+'">'+btnsEdicionEjerc+panel_titulo_descrip+'<div class="ejercicio '+cls+'">'+html_ejerc.html()+'</div></div>';
            $('#'+idPregActiva).find('.aquicargarplantilla .mask').before(new_ejerc);
        }
         

        $('#'+idModal).modal('hide');
        $('.istooltip').tooltip();
        var now = Date.now();
        var idcontenedor=$('#'+idPregActiva).attr('idpadre')||now;
        $('#'+idPregActiva).attr('idpadre',idcontenedor);
        $('.contenedor-pregs .preg').removeClass('active');
        iniciaraccionenplantillas();
        var obj=$('#'+idPregActiva+' #grabando');
        var htmlaguadar=obj.clone();        
        htmlaguadar.find('.btns-tmpl').remove();
        var idpadre=$('this')
        var idpadre=$('#'+idPregActiva).attr('idpadre')||0;
        var idpregunta=obj.attr('data-idpregunta')||'';
        var data={};
        data.idexamen='<?php echo $this->idexamen; ?>';
        data.idpregunta=idpregunta;
        data.titulo=txtTitulo;
        data.descripcion=txtDescripcion;
        data.ejercicio=htmlaguadar.html();
        data.idpadre=0;
        data.tiempo=tiempo;
        data.puntaje=puntaje;
        data.template=plant;
        data.habilidad=habilidades;
        data.contenedor=idcontenedor;
        guardarPreguntas(obj,data,false);
        obj.removeAttr('id');
        numerarpreguntas();
        $('.editandotmpl').focus();
    })
    .on('hidden.bs.modal', '.edicion_tmpl', function (e) {
        $('.edicion_tmpl').remove();
        $('.tmpl').removeClass('editandotmpl');
    })
    .on('change', '.edicion_tmpl .eje-panelinfo .setpanelvalue', function(e) {
        e.preventDefault();
        var actualizar = $(this).attr('data-actualiza');
        var valor = $(this).val();
        var divClonar = $(this).closest('.modal-body').find('.plantilla').attr('data-clone');
        if(actualizar=="tiempo"){ var atributo = 'data-tiempo'; }
        if(actualizar=="puntaje"){ var atributo = 'data-puntaje'; }
        $(this).closest('.modal-body').find('.plantilla  '+divClonar).attr(atributo, valor);
    })
    .on('click', '.edicion_tmpl .live-edit', function(e){
            $(this).css({"border": "0px"}); 
            addtext1(e,this);
    })
    .on('blur','.edicion_tmpl .live-edit>input',function(e){
        e.preventDefault();
        $(this).closest('.live-edit').removeAttr('Style'); 
        addtext1blur(e,this);
    })
    .on('keypress','.edicion_tmpl .live-edit>input', function(e){
        if(e.which == 13){ 
            e.preventDefault();          
            $(this).trigger('blur');
        } 
    })
    .on('keyup','.edicion_tmpl input', function(e){
        if(e.which == 27){ 
            $(this).attr('data-esc',"1");
            $(this).trigger('blur');
        }
    })
    .on('click', '.edicion_tmpl .btn-skill', function(e) {
        e.preventDefault();
        $(this).toggleClass('active'); 
        var habilidad='';
        var divhabilidades=$(this).closest('.habilidades.edicion')
        divhabilidades.find('.btn-skill.active').each(function(){
            habilidad+=$(this).attr('data-id-skill')+'|';
        });
        $('.preg.active .tmpl.editandotmpl').attr('data-habilidades',habilidad);
        divhabilidades.attr('data-habilidades',habilidad);
    })
    .on('change', '.video_demo .chkDontShow', function(e) {
        e.preventDefault();
        var id = $(this).attr('id');
        var isChecked = $(this).is(':checked');
        if(localStorage.HideVideoDemo!=undefined) var storage = JSON.parse(localStorage.HideVideoDemo);
        else var storage = {};
        storage[id] = isChecked;
        localStorage.HideVideoDemo = JSON.stringify(storage);
    });

    cargarMensajesPHP();
    var numerarpreguntas=function(){
        $i=0;
        $('b.npregunta').remove().siblings('b').remove();
        $('.contenedor-pregs').find('.tmpl-ejerc').each(function(){
            var obj= $(this).find('.titulo_descripcion .titulo');           
            $i++;
            //if(obj.text()!=''&&obj!=undefined)
               obj.before('<b class="npregunta">'+($i)+'.- Question  </b>');
            obj.attr('data-orden',$i)
        });
    };

    var iniciaraccionenplantillas=function(){
        $('.plantilla-completar').examMultiplantilla();
        $('.plantilla-verdad_falso').examTrueFalse();
        $('.plantilla-ordenar.ord_simple').examOrdenSimple();
        $('.plantilla-ordenar.ord_parrafo').examOrderParagraph();
        $('.plantilla-fichas').examJoin();
        $('.plantilla-img_puntos').examTagImage();
    };
    showexamen('question');
    numerarpreguntas();
    //iniciaraccionenplantillas();
    $('.istooltip').tooltip();
    
    <?php if(empty($preguntas)){ ?>
        $('.add-question:last').trigger('click');
    <?php }  ?>

    $('.slide-sidebar-right:first-child').trigger('click');

    <?php if(!empty($preguntas)){ ?>
        $('aside.sidebar-right').toggleClass('sidebar-toogled');
    <?php }  ?>

});
</script>