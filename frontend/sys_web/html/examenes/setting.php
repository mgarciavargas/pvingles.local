<?php $idgui = uniqid();
$fonts = ['Georgia', 'Palatino Linotype', 'Book Antiqua', 'Times New Roman', 'Arial', 'Helvetica', 'Arial Black', 'Impact', 'Lucida Sans Unicode', 'Tahoma', 'Verdana', 'Courier New', 'Lucida Console'];
sort($fonts);

if(!empty($this->examen)) {
	$examen=$this->examen;
	$this->idtipo=$examen["tipo"];
	$portada=$examen["portada"];
	$portada=@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$portada);
	$total=$examen["calificacion_total"];
	$minimo=$examen["calificacion_min"];
}
else {
	$examen=null;
	$portada=$this->documento->getUrlStatic().'/media/imagenes/levels/nofoto.jpg';
}
 ?>
<form method="post" id="frm-<?php echo $idgui;?>" onsubmit="return false;">
<input type="hidden" name="idexamen" value="<?php echo @$examen["idexamen"]; ?>">
<div class="row">
	<div class="panel panel-primary" style="margin-top: 1ex;">
		<div class="panel-heading" style="overflow: hidden;">
		<ol class="breadcrumb pull-left" style="margin: 0px; padding: 0px; background:rgba(187, 197, 184, 0);">
			<li><a href="<?php echo $this->documento->getUrlBase() ?>/examenes/" style="color:#fff"><?php echo JrTexto::_("Assessment"); ?></a></li>                  
			<li class="active"  style="color:#ccc"><?php echo JrTexto::_("Setting"); ?></li>
        </ol>
		<div class="pull-right"><a href="#" class="btn btn-xs btn-default showdemo" data-videodemo="settings.mp4"><i class="fa fa-question-circle"></i> <?php echo JrTexto::_("Help"); ?></a></div>
	    </div>
		<div class="panel-body">
			<div class="panel pnl100">
				<div class="panel-body">
				<div class="col-xs-6 col-sm-4 col-md-3">
				  	<div class="cajaselect "> 
				  		<select name="nivel" id="level-item" class="conestilo ">
			             	<option value="" ><?php echo ucfirst(JrTexto::_("All levels")); ?></option>
			             	<?php if(!empty($this->niveles))
			                     foreach ($this->niveles as $nivel){?>
			                     <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $nivel["idnivel"]==$this->idnivel?'Selected':'';?>><?php echo $nivel["nombre"]?></option>
			              <?php }?>
			            </select>
			        </div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3">
				  	<div class="cajaselect "> 
				  	<select name="unidad" id="unit-item" class="conestilo ">
			          	<option value="" ><?php echo ucfirst(JrTexto::_("All units")); ?></option>
			          	<?php if(!empty($this->unidades))
			                     foreach ($this->unidades as $unidad){?>
			                     <option value="<?php echo $unidad["idnivel"]; ?>" <?php echo $unidad["idnivel"]==$this->idunidad?'Selected':'';?>><?php echo $unidad["nombre"]?></option>
			              <?php }?>
			        </select>
			        </div>
				</div>
			    <div class="col-xs-6 col-sm-4 col-md-3">
				  	<div class="cajaselect "> 
				  	<select name="actividad" id="activity-item" class="conestilo ">
			          	<option value="" ><?php echo ucfirst(JrTexto::_("All activities")); ?></option>
			          	 <?php if(!empty($this->actividades))
			                     foreach ($this->actividades as $act){?>
			                     <option value="<?php echo $act["idnivel"]; ?>" <?php echo $act["idnivel"]==$this->idactividad?'Selected':'';?>><?php echo $act["nombre"]?></option>
			              <?php }?>
			        </select>
			        </div>
			    </div>
				<div class="col-xs-6 col-sm-4 col-md-3">
				  	<div class="cajaselect "> 
				  	<select name="tipo" id="tipo-examen" class="conestilo ">
			          	<option value="" ><?php echo ucfirst(JrTexto::_("All types"));?></option>   
			          	<?php if(!empty($this->tipoexamen))
			                     foreach ($this->tipoexamen as $tip){?>
			                     <option value="<?php echo $tip["idtipo"]; ?>" <?php echo $tip["idtipo"]==$this->idtipo?'Selected':'';?>><?php echo ucfirst(JrTexto::_($tip["tipo"])); ?></option>
			              <?php }?>   	
			        </select>
			        </div>
				</div>
			    </div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6" style="padding: 0px;">
				<div class="panel-body">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
						    <label for="titulo"><?php echo ucfirst(JrTexto::_("Title")); ?>:</label>
						    <input type="text" class="form-control gris" id="titulo" name="titulo" placeholder="<?php echo ucfirst(JrTexto::_("add title")); ?>" value="<?php echo @$examen["titulo"] ?>">
						</div>					  	
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
						    <label for="descripcion"><?php echo ucfirst(JrTexto::_("Description")); ?>:</label>
						    <textarea name="descripcion" rows="6" class="form-control gris" name="descripcion" placeholder="<?php echo ucfirst(JrTexto::_("Add Description")); ?>"><?php echo @$examen["descripcion"] ?></textarea>
						</div>					  	
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<label for="titulo"><?php echo ucfirst(JrTexto::_("sorting of questions")); ?>:</label>							
					  	<div class="cajaselect ">
					  	<select name="aleatorio" id="aleatorio" class="conestilo">
				          	<option value="0" <?php echo @$examen["aleatorio"]=='0'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("orderly")); ?></option> 
				          	<option value="1" <?php echo @$examen["aleatorio"]=='1'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("ramdon")); ?></option>	
				        </select>
				        </div>
					</div>
					
			    </div>
			</div>
			<div class="panel col-xs-12 col-sm-6 col-md-6" style="padding: 0px;">
				<div class="panel-body">
					<div class="col-xs-6 col-sm-6 col-md-6">
						<label for="titulo"><?php echo ucfirst(JrTexto::_("State")); ?>:</label>						
					  	<div class="cajaselect">
					  	<select name="estado" id="estado" class="conestilo">
				          	<option value="1" <?php echo @$examen["estado"]=='0'?'Selected="selected"':''; ?>><?php echo JrTexto::_("Active")?></option> 
				          	<option value="0" <?php echo @$examen["estado"]=='1'?'Selected="selected"':''; ?>><?php echo JrTexto::_("Inactive")?></option>	
				        </select>
				        </div>
				        <br>
				        <label for="titulo"><?php echo ucfirst(JrTexto::_("Group")); ?>:</label>						
					  	<div class="cajaselect">
					  	<select name="engrupo" id="engrupo" class="conestilo">
					  		<option value="" <?php echo @$examen["grupo"]==''?'Selected="selected"':''; ?>></option>
				          	<option value="G1" <?php echo @$examen["grupo"]=='G1'?'Selected="selected"':''; ?>><?php echo JrTexto::_("Grupo01")?></option> 
				        </select>
				        </div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("cover")); ?>:</label>
						<div style="text-align: center; max-height: 135px; overflow: hidden;" >						
					  	<img class="img-portada img-responsive img-thumbnails istooltip" src="<?php echo $portada ?>" width="170px" data-tipo="image" data-url=".img-portada" alt="" title="<?php echo JrTexto::_("Click here to change te exam cover"); ?>" >
					  	<input type="hidden" name="portada" value="<?php echo $portada ?>">
					  	</div>					  	
					</div>										
					<div class="col-xs-12 col-sm-12 col-md-12 pnlisgrupo">						
					  	<label class="rojo isgrupo"><?php echo ucfirst(JrTexto::_("school")).' - '.ucfirst(JrTexto::_("classroom")); ?>:</label>						
					</div>
					<div class="clearfix"></div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<label for="titulo"><?php echo ucfirst(JrTexto::_("control time by")); ?>:</label>						
					  	<div class="cajaselect">
					  	<select name="tiempopor" id="tiempopor" class="conestilo">
				          	<option value="E" <?php echo @$examen["tiempo_por"]=='E'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Exam"))?></option> 
				          	<option value="Q" <?php echo @$examen["tiempo_por"]=='Q'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Question"))?></option>	
				        </select>
				        </div>
					</div>					
					<div class="col-xs-6 col-sm-6 col-md-6 pnltiempopor"><br>						
					  	<label for="titulo"><?php echo JrTexto::_("Time"); ?>:</label>
						<input type="text" class="form-control rojo" id="tiempototal" name="tiempototal" value="<?php echo $examen["tiempo_total"]?$examen["tiempo_total"]:'00:00:00';?>">
					</div>
					<div class="clearfix"></div>
					<div class="col-xs-6 col-sm-6 col-md-6"><br>
						<label for="titulo"><?php echo ucfirst(JrTexto::_("Number of Attemps")); ?>:</label>							
					  	<div class="cajaselect ">
					  	<select name="nintento" id="nintento" class="conestilo">
					  	    <?php for($j=1;$j<=15;$j++){?>
					  	    	<option value="<?php echo $j; ?>" <?php echo @$examen["nintento"]==$j?'Selected="selected"':''; ?>> <?php echo $j." ".ucfirst(JrTexto::_("Attemps")); ?></option> 
					  	    <?php }?>
				          	<option value="99999" <?php echo @$examen["nintento"]=='99999'?'Selected="selected"':''; ?>> <?php echo ucfirst(JrTexto::_("Unlimited")); ?></option>
				          	
				        </select>
				        </div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6" id="pnlcalificacion" style="display: none;"><br>
						<label for="titulo"><?php echo ucfirst(JrTexto::_("Keep rating")); ?>:</label>							
					  	<div class="cajaselect ">
						  	<select name="calificacion" id="calificacion" class="conestilo" >
						  	    <option value="U" <?php echo @$examen["calificacion"]=='U'?'Selected="selected"':''; ?>> <?php echo ucfirst(JrTexto::_("Last rating")); ?></option>
					          	<option value="M" <?php echo @$examen["calificacion"]=='M'?'Selected="selected"':''; ?>> <?php echo ucfirst(JrTexto::_("Best rating")); ?></option>
					        </select>
				        </div>
					</div>
			    </div>
			</div>
			<div class="panel col-xs-12 col-sm-12 col-md-12">
			<div class="panel-body">
			<fieldset>
				<legend><?php echo ucfirst(JrTexto::_("exam evaluation"))?></legend>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<label><?php echo ucfirst(JrTexto::_("scored by"))?>:</label>
				  	<div class="cajaselect "> 				  	 
				  	<select name="calificacionpor" id="calificacionpor" class="conestilo ">
			          	<option value="E" <?php echo @$examen["calificacion_por"]=='E'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Exam"))?></option>
			          	<option value="Q" <?php echo @$examen["calificacion_por"]=='Q'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Question"))?></option>
			        </select>
			        </div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3 pnlcalificacionpor">
					<label><?php echo ucfirst(JrTexto::_("type of score"))?>:</label> 
				  	<div class="cajaselect ">				  	
				  	<select name="calificacionen" id="calificacionen" class="conestilo ">
			          	<option value="P" <?php echo @$examen["calificacion_en"]=='P'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("percentage"))?></option>
			          	<option value="N" <?php echo @$examen["calificacion_en"]=='N'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("numeric"))?></option>
			          	<option value="A" <?php echo @$examen["calificacion_en"]=='A'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("alfabetic"))?></option>
			        </select>
			        </div>
			        <small id="msje_calificacionpor"><?php echo ucfirst(JrTexto::_("minimum score to pass"))?>: 51ptos.</small>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3 pnlcalificacionpor pnlcalificacionen">
					<label><?php echo ucfirst(JrTexto::_("highest score"))?>:</label> 
				  	<div class="cajaselect ">				  	
				  	<select name="calificaciontotal" id="calificaciontotal" class="conestilo ">
			        </select>
			        </div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3 pnlcalificacionen">
					<label><?php echo ucfirst(JrTexto::_("minimum score to pass"))?>:</label> 
				  	<div class="cajaselect ">				  	
				  	<select name="calificacionmin" id="calificacionmin" class="conestilo ">
			        </select>
			        </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 pnlcalificacionenshow" style="display: none">
					<table class="table table-reponsive table-striped">
						<tr><th><?php echo ucfirst(JrTexto::_("scale name"));?></th><th><?php echo ucfirst(JrTexto::_("minimum score to pass the scale"))?></th></tr>
						<tr><td>
							<div class="form-group">
								<input type="text" class="form-control gris txtsimbolo" name="txtsimbolo" placeholder="<?php echo JrTexto::_("e.g.");?>: A, good, ...">
							</div></td>
						<td><div class="cajaselect ">
					  	<select name="txtnota" id="txtnota" class="conestilo">
					  	<?php for ($i=100; $i >= 1; $i--) { ?>
					  	<option value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
					  	<?php }?>
				        </select>
				        </div></td>
				        <td></td>
				        </tr>
				        <tr><td>
							<div class="form-group">
								<input type="text" class="form-control gris txtsimbolo" name="txtsimbolo" placeholder="<?php echo JrTexto::_("e.g.");?>: A, good, ...">
							</div></td>
						<td><div class="cajaselect ">				  	
					  	<select name="txtnota" id="txtnota" class="conestilo">
					  	<?php for ($i=100; $i >= 1; $i--) { ?>
					  	<option value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
					  	<?php }?>
				        </select>
				        </div></td>
				        <td><i class="btn btn-removefila fa fa-trash" ></i></td>
				        </tr>
				        <tr>
				        	<td><div class="form-group">
								<input type="text" class="form-control gris txtsimbolo" name="txtsimbolo" placeholder="<?php echo JrTexto::_("e.g.");?>: A, good, ...">
							</div></td>
							<td><div class="cajaselect ">				  	
							  	<select name="txtnota" id="txtnota" class="conestilo">
							  		<?php for ($i=100; $i >= 1; $i--) { ?>
								  	<option value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
								  	<?php }?>
						        </select>
				        	</div></td>
				        	<td><i class="btn btn-removefila fa fa-trash" ></i></td>
				        </tr>
				        <tr style="display: none">
				        	<td><div class="form-group">
								<input type="text" class="form-control gris txtsimbolo" name="txtsimbolo" placeholder="<?php echo JrTexto::_("e.g.");?>: A, good, ...">
							</div></td>
							<td><div class="cajaselect ">				  	
					  			<select name="txtnota" id="txtnota" class="conestilo">
				        		</select>
				        	</div></td>
				        	<td><i class="btn btn-removefila fa fa-trash" ></i></td>
				        </tr>
					</table>
					<input type="hidden" name="txtAlfanumerico" id="txtAlfanumerico">
					<div class="pull-right"><span class="btn btn-primary  btn-xs btn-addfila"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_("add"));?></span></div>
				</div>
			</fieldset>
			</div>
			</div>
			<div class="panel col-xs-12 col-sm-12 col-md-12">
			<div class="panel-body">
			<fieldset>
				<legend><?php echo ucfirst(JrTexto::_("exam format"))?></legend>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<label><?php echo ucfirst(JrTexto::_("fonts"))?></label>
				  	<div class="cajaselect"> 				  	 
				  	<select name="fuente" id="fuente" class="conestilo">
				  		<?php foreach ($fonts as $f) { ?>
				  			<option value="<?php echo $f; ?>" <?php echo @$examen["fuente"]==$f?'Selected="selected"':''; ?>><?php echo JrTexto::_($f)?></option>
				  		<?php } ?>
			        </select>
			        </div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<label><?php echo ucfirst(JrTexto::_("font size"))?></label> 
				  	<div class="cajaselect">				  	
				  	<select name="fuentesize" id="size" class="conestilo">
				  	   <?php 
				  	   $isel=!empty($examen["fuentesize"])?$examen["fuentesize"]:14;
				  	   for ($i=10; $i < 36; $i++) { ?>
				  	   <option value="<?php echo $i; ?>" <?php echo $i==$isel?'selected="selected"':''; ?> ><?php echo $i."px"; ?></option>
				  	   <?php }?>
			        </select>
			        </div>
				</div>

				<div class="col-xs-6 col-sm-4 col-md-6 text-center"><br>
					<button class="btn btn-danger btneliminarexamen hidden" data-idexamen="<?php echo @$examen["idexamen"]; ?>"><i class="fa fa-save"></i> <?php echo JrTexto::_('Remove Assesment'); ?></button>
					<button class="btn btn-primary btnsaveexamen"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save and add Question'); ?></button>
					
				</div>
			</fieldset>	
			</div>
			</div>	
		</div>	
	</div>
</div>
</form>
<script type="text/javascript">

	$(document).ready(function(){
		$('#tiempototal').mask("99:99:99");
		$('.istooltip').tooltip();
//recargar combos de niveles 
		var leerniveles=function(data){
	        try{
	            var res = xajax__('', 'niveles', 'getxPadre', data);
	            if(res){ return res; }
	            return false;
	        }catch(error){
	            return false;
	        }       
	    }
	    var addniveles=function(data,obj){
	    	var objini=obj.find('option:first').clone();
	    	obj.find('option').remove();
	    	obj.append(objini);
	    	if(data!==false){
		    	var html='';
		    	$.each(data,function(i,v){
		    		html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
		    	});
		    	obj.append(html);
	    	}
	    	id=obj.attr('id');
	    	//if(id==='activity-item')	cargarexamenes();
	    }

	    $('#level-item').change(function(){
	    	var idnivel=$(this).val();
            var data={tipo:'U','idpadre':idnivel}
            var donde=$('#unit-item');
            if(idnivel!=='') addniveles(leerniveles(data),donde);
            else addniveles(false,donde);
            donde.trigger('change');
	    });
	    $('#unit-item').change(function(){
	    	var idunidad=$(this).val();
            var data={tipo:'L','idpadre':idunidad}
            var donde=$('#activity-item');
            if(idunidad!=='') addniveles(leerniveles(data),donde);
            else addniveles(false,donde);
	    });

	    $('#tipo-examen').change(function(){
	    	cargarexamenes();
	    });

	    var vercalificacion=function(){
	    	var x=$('#nintento').val()||1;
	    	$('#pnlcalificacion').hide();
	    	if(x>1){
	    		$('#pnlcalificacion').show();
	    	}
	    }
	    $('#nintento').change(function(){
	    	vercalificacion();
	    })

	    var verengrupo=function(){
	    	var grupo=$('#engrupo').val();
	    	if(grupo===''){
	    		$('.pnlisgrupo').hide();
	    	}else{
	    		$('.pnlisgrupo').show();
	    	}
	    }
	    $('#engrupo').change(function(){
	    	verengrupo();
	    });

	    var vertiempopor=function(){
	    	var tiempopor=$('#tiempopor').val();
	    	if(tiempopor=='E'){
	    		$('.pnltiempopor').show();
	    	}else{
	    		$('.pnltiempopor').hide();
	    	}
	    }
	    $('#tiempopor').change(function(){
	    	vertiempopor();
	    });

	    $('.img-portada').click(function(e){ 
		    var txt="<?php echo JrTexto::_('Selected or upload'); ?> ";
		    selectedfile(e,this,txt);
		});

		$('.img-portada').load(function() {
		    var src = $(this).attr('src');
		    if(src!=''){
		        $(this).siblings('input').val(src);
		    }
		});


		var vercalificacionpor=function(){
	    	var califPor=$('#calificacionpor').val();
	    	$('.pnlcalificacionpor').show();
	    	if(califPor=='Q'){
	    		$('#calificacionen option[value="A"]').hide();
	    		$('#calificaciontotal').closest('.pnlcalificacionpor').hide();
	    	} 
	    	if(califPor=='E'){
	    		$('#calificacionen option[value="A"]').show();
	    		$('#calificaciontotal').closest('.pnlcalificacionpor').show();
	    	}
	    };
		$('#calificacionpor').change(function(){
	    	vercalificacionpor();

	    	$('#calificacionen').val($('#calificacionen option:first-child').val());
	    	vercalificacionen();
	    });

	    var vercalificacionen=function(){
	    	var califEn=$('#calificacionen').val();
	    	$('#calificaciontotal').find('option').remove();
	    	$('#calificacionmin').find('option').remove();
	    	$('.pnlcalificacionen').show();
	    	var min=<?php echo !empty($minimo)?$minimo:1; ?>;
	    	var total=<?php echo !empty($total)?$total:100; ?>;
	    	if(califEn=='P'||califEn=='N'){ //porcentaje	
	    	    var simbol='';
	    	    if(califEn=='P') simbol='%';    		
	    		var option='';
	    		for (var i = 1; i <= 100; i++){
	    			option+='<option value="'+i+'" '+(min==i?'selected="selected"':'')+'>'+i+''+simbol+'</option>';
	    		}
	    		$('#calificacionmin').append(option);
	    		var option='';
	    		for (var i = 100; i >0 ; i--){
	    			option+='<option value="'+i+'" '+(total==i?'selected="selected"':'')+'>'+i+''+simbol+'</option>';
	    		}
	    		$('#calificaciontotal').append(option);
	    		if( $('#calificacionpor').val()=='Q' ) $('#calificaciontotal').closest('.pnlcalificacionpor').hide();
	    		if( $('#calificacionpor').val()=='E' ) $('#calificaciontotal').closest('.pnlcalificacionpor').show();
	    		$('.pnlcalificacionenshow').hide();
	    	}else if(califEn=='A'){ //alfanumerico
	    		$('#txtAlfanumerico').val(JSON.stringify(total));
	    		if(total) precargartable();
	    		$('.pnlcalificacionen').hide();
	    		$('.pnlcalificacionenshow').show();
	    		orndenartr();
	    	}

	    	if(califEn=='A') $('#msje_calificacionpor').show();
	    	else $('#msje_calificacionpor').hide();
	    }

	    $('#calificacionen').change(function(){
	    	vercalificacionen();
	    });


	    var calificacionmin=function(_simbolo,valor,obj){
	    	var option='';	    
	    	var simbolo=_simbolo=='P'?'%':'';
	    	obj.find('option').remove();
    		for (var i = 1; i <= valor; i++){
    			option+='<option value="'+i+'">'+i+simbolo+'</option>';
    		}
    		obj.append(option);
	    }

	    $('#calificaciontotal').change(function(){
	    	var val=$(this).val();
	    	var _simbolo=$('#calificacionen').val();
	    	calificacionmin(_simbolo,val,$('#calificacionmin'));
	    });

	    //calificacion alfanumerica
	    var orndenartr=function(){
	    	var trs=$('.pnlcalificacionenshow table').find('tr');
	    	var i=0;
	    	trs.each(function(){
	    		i++;
	    		$(this).find('#txtnota').attr('data-fila',i);
	    		$(this).attr('data-fila',i);
	    	})
	    } 
	    $('.pnlcalificacionenshow').on('click','.btn-removefila',function(){
	    	$(this).closest('tr').remove();
	    	orndenartr();
	    	cargartxtAlfanumerico();
	    });

	    $('.pnlcalificacionenshow').on('click','.btn-addfila',function(){
	    	var ultimafila=$('.pnlcalificacionenshow table').find('tr:last');
	    	var clonado=ultimafila.clone();
	    	ultimafila.remove();
	    	clonado.show();
	    	$('.pnlcalificacionenshow table').append(clonado);
	    	$('.pnlcalificacionenshow table').append(ultimafila);
	    	orndenartr();
	    	cargartxtAlfanumerico();
	    });

	    var calificacionmin2=function(_simbolo,valor,obj){
	    	var option='';	    
	    	var simbolo=_simbolo||'';
	    	obj.find('option').remove();
    		for (var i = valor ; i >= 1; i--){
    			option+='<option value="'+i+'">'+i+simbolo+'</option>';
    		}
    		obj.append(option);
	    }

	    var cargartxtAlfanumerico=function(){
	    	var data={};
	    	var tr=$('.pnlcalificacionenshow table').find('tr');
	    	var ntr=tr.length-1;
	    	tr.each(function(i,v){
	    		if(i>0&&i<ntr){
	    			var sim=$(v).find('.txtsimbolo').val()||i;
	    			var sel=$(v).find('select.conestilo').val()||i;
	    			data[sim]=sel;
	    		}
	    	});
	    	$('#txtAlfanumerico').val(JSON.stringify(data));
	    }

	     var precargartable =function(){
	    	var table=$('.pnlcalificacionenshow table');
	    	var tr1=table.find('tr:first').nextAll().remove();
	    	var datos=JSON.parse($('#txtAlfanumerico').val());	    	
	    	var html=''; 
	    	var vi=100;	    	
	    	if(!$.isEmptyObject(datos))
	    	$.each(datos,function(i,v){
	    		 html+='<tr><td>'
							+'<div class="form-group">'
								+'<input type="text" class="form-control gris txtsimbolo" name="txtsimbolo" placeholder="<?php echo JrTexto::_("e.g.");?>: A, good, ..." value="'+i+'">'
							+'</div></td>'
						+'<td><div class="cajaselect ">'			  	
					  	+'<select name="txtnota" id="txtnota" class="conestilo">';
					  	for (var i = vi; i >= 0; i--) {
					  	 html +='<option value="'+i+'" '+(i==v?'selected="selected"':'')+' >'+i+'</option>';	
					  	}
				      html +='</select>'
				        +'</div></td>'
				        +'<td><i class="btn btn-removefila fa fa-trash" ></i></td>'
				        +'</tr>';
				        vi=v;
	    	});
	    	html+='<tr style="display:none"><td>'
							+'<div class="form-group">'
								+'<input type="text" class="form-control gris txtsimbolo" name="txtsimbolo" placeholder="<?php echo JrTexto::_("e.g.");?>: A, good, ..." value="">'
							+'</div></td>'
						+'<td><div class="cajaselect ">'			  	
					  	+'<select name="txtnota" id="txtnota" class="conestilo">';
					  	for (var i = (vi-1); i >= 0; i--) {
					  	 html +='<option value="'+i+'"  >'+i+'</option>';	
					  	}
				      html +='</select>'
				        +'</div></td>'
				        +'<td><i class="btn btn-removefila fa fa-trash" ></i></td>'
				        +'</tr>';
	    	table.append(html);
	    }
	    $('.table').on('change','select.conestilo',function(){
	    	var i=$(this).val();
	    	obj=$(this).closest('tr').next().find('select.conestilo');
	    	calificacionmin2('%',i,obj);
	    	cargartxtAlfanumerico();
	    });

	    $('.table').on('blur','.txtsimbolo',function(){	    	
	    	cargartxtAlfanumerico();
	    });

	    $('.btnsaveexamen').click(function(){
	    	guardarexamen();
	    });

	    <?php if(!empty($this->examen)) { ?>
	    	$('.btneliminarexamen').removeClass('hidden');
	    <?php } ?>

	    $('.btneliminarexamen').click(function(){
	    	try{
	    		var idexamen=$(this).attr('data-idexamen')||-1;
	            var res = xajax__('', 'examenes', 'eliminar', parseInt(idexamen));	          
	            if(res)  redir('<?php echo $this->documento->getUrlBase() ?>/examenes/');
	        }catch(error){
	            
	        }
	    });

	    var guardarexamen=function(){
              var msjeAttention = '<?php echo JrTexto::_('Attention') ?>';
              var IDGUI = '<?php echo $idgui; ?>';
              var formData = new FormData($("#frm-"+IDGUI)[0]);
              $.ajax({
                url: _sysUrlBase_+'/examenes/guardar',
                type: "POST",
                data:  formData,
                contentType: false,
                dataType :'json',
                cache: false,
                processData:false,
                beforeSend: function(XMLHttpRequest){
                   $('#procesando').show('fast'); 
                },
                success: function(data)
                {
                  if(data.code==='ok'){
                    mostrar_notificacion(msjeAttention, data.msj, 'success');
                    setTimeout(redir(_sysUrlBase_+'/examenes/preguntas/?idexamen='+data.new),2000);
                  }else{
                    mostrar_notificacion(msjeAttention, data.msj, 'warning');
                  }
                  $('#procesando').hide('fast');
                  return false;
                },
                error: function(xhr,status,error){
                  mostrar_notificacion(msjeAttention, status, 'warning');
                  $('#procesando').hide('fast');
                  return false;
                }               
            });
        };

        $('.showdemo').click(function(e) {
        	e.preventDefault();
        	var video = $(this).attr('data-videodemo');
        	var ruta_video = _sysUrlStatic_+'/sysplantillas/examen/'+video;
        	var name_tmpl = 'settings';
        	var nombre_comun = '<?php echo JrTexto::_("Settings");?>';
        	var idDemo = 'demo_'+name_tmpl;
	        if( $('#demo_'+name_tmpl).length==0){
	            var new_modal_video = $('#modalclone').clone();
	            new_modal_video.removeAttr('id').attr('id', idDemo).addClass('video_demo');
	            new_modal_video.find('.modal-header #modaltitle').text('Video Demo: '+nombre_comun);
	            new_modal_video.find('.modal-body').html('<div class="col-xs-12"><div class="embed-responsive embed-responsive-16by9 mascara_video"><video controls="true" class="valvideo embed-responsive-item vid" src="'+ruta_video+'"></video></div></div>');
	            $('body').append(new_modal_video);
	        }

	        $('#'+idDemo).modal();
	        $('#'+idDemo).trigger('resize');
        });
    
	    //funciones de inicio
	    verengrupo();
	    vertiempopor();
	    vercalificacionpor();
	    vercalificacionen();
	    vercalificacion();
	    showexamen('setting');
	});
</script>