<?php $usuarioActivo=NegSesion::getUsuario(); 
$usuariodni=@$usuarioActivo["dni"];
?>
<style>
    .portada{
        overflow: hidden;
    }
    .portada img{
        height: 140px;
    }
</style>
<div class="row">
	<div class="alert alert-default panel-filtros">
	  <div class="col-xs-6 col-sm-4 col-md-3">
	  	<div class="cajaselect"> 
	  		<select name="level" id="level-item" class="conestilo">
             	<option value="" ><?php echo JrTexto::_("All Levels")?></option>
             	<?php if(!empty($this->niveles))
                     foreach ($this->niveles as $nivel){?>
                     <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $nivel["idnivel"]==$this->idnivel?'Selected':'';?>><?php echo $nivel["nombre"]?></option>
              <?php }?>
            </select>            
        </div>
	  </div>
	  <div class="col-xs-6 col-sm-4 col-md-3">
	  	<div class="cajaselect"> 
	  	<select name="level" id="unit-item" class="conestilo">
          	<option value="" ><?php echo JrTexto::_("All Unit")?></option>
          	<?php if(!empty($this->unidades))
                     foreach ($this->unidades as $unidad){?>
                     <option value="<?php echo $unidad["idnivel"]; ?>" <?php echo $unidad["idnivel"]==$this->idunidad?'Selected':'';?>><?php echo $unidad["nombre"]?></option>
              <?php }?>
        </select>
        </div>
	  </div>
	  <div class="col-xs-6 col-sm-4 col-md-3">
	  	<div class="cajaselect"> 
	  	<select name="level" id="activity-item" class="conestilo">
          	<option value="" ><?php echo JrTexto::_("All Activity")?></option>
          	 <?php if(!empty($this->actividades))
                     foreach ($this->actividades as $act){?>
                     <option value="<?php echo $act["idnivel"]; ?>" <?php echo $act["idnivel"]==$this->idactividad?'Selected':'';?>><?php echo $act["nombre"]?></option>
              <?php }?>
        </select>
        </div>
	  </div>
	  <div class="col-xs-6 col-sm-4 col-md-3">
	  	<div class="cajaselect"> 
	  	<select name="tipo" id="tipo-examen" class="conestilo">
          	<option value="" ><?php echo JrTexto::_("All Types")?></option>
          	<?php if(!empty($this->tipoexamen))
                     foreach ($this->tipoexamen as $tip){ ?>
                     <option value="<?php echo $tip["idtipo"]; ?>" <?php echo $tip["idtipo"]==$this->tipo?'Selected':'';?>><?php echo ucfirst(JrTexto::_($tip["tipo"])); ?></option>
              <?php }?>   	
        </select>
        </div>
	  </div>
	  <div class="clearfix"></div>
	</div>
	<div class="panel panel-info pnl100">
		<div class="panel-heading pnl100">
			<a href="#" class="verexamen btn btn-default"><i class="fa fa-plus"></i> <?php echo JrTexto::_("Add") ?></a>
			<a href="#" class="btn btn-primary"><?php echo JrTexto::_("Publish") ?></a>	
		</div>
		<div class="panel-body pnl100 pnllisexamenes">
			<?php 
			if(!empty($this->examenes))
			foreach ($this->examenes as $examen){
				if(!empty($examen["portada"]))
					$portada=@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$examen["portada"]);
				else
					$portada='$this->documento->getUrlStatic() ?>/media/imagenes/levels/a1.png';
			    if($usuariodni==$examen["idpersonal"]) $ruta='/examenes/ver/?idexamen='.$examen["idexamen"];
			    else $ruta='/examenes/teacherresrc_view/?idexamen='.$examen["idexamen"];
				?>
				<div class="col-xs-6 col-sm-4 col-md-2 " style="padding: 0.7ex">
				    <a href="<?php echo $this->documento->getUrlBase().$ruta;?>" class="">
					<div class="exa-item">
						<div class="titulo"><?php echo $examen["titulo"]; ?></div>
						<div class="portada"><img class="img-responsive" width="100%" src="<?php echo $portada;?>"></div>
					</div>
					</a>
				</div>
			<?php } ?>			
		</div>	
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
//recargar listado de examenes		
		var cargarexamenes=function(){
			var msjeAttention = '<?php echo JrTexto::_('Attention') ?>';
			var dniuser='<?php echo $usuariodni ?>';
			var nivel=$('#level-item').val()||0;
	    	var unidad=$('#unit-item').val()||0;
	    	var actividad=$('#activity-item').val()||0;
	    	var tipo=$('#tipo-examen').val()||0;
	    	var url=_sysUrlBase_+'/examenes/publicadosjson/'+nivel+'/'+unidad+'/'+actividad+'/'+tipo;
	    	try{
              var formData = new FormData();
              formData.append("nivel", nivel);
              formData.append("unidad", unidad);
              formData.append("actividad", actividad);
              formData.append("tipo", tipo);
              $.ajax({
                url: url,
                type: "POST",
                data:  formData,
                contentType: false,
                dataType :'json',
                cache: false,
                processData:false,
                beforeSend: function(XMLHttpRequest){
                   $('#procesando').show('fast'); 
                },
                success: function(data)
                {
                  if(data.code==='ok'){
                    var examenes=data.data;
                    html='';
                    if(examenes)
                    	$.each(examenes,function(i,obj){
                    		if(obj.portada)
								var portada=obj.portada.replace(/__xRUTABASEx__/gi,_sysUrlBase_);
							else
								var portada=_sysUrlBase_+'/media/imagenes/levels/a1.png';
							 if(dniuser==obj.idpersonal) var ruta='/examenes/ver/?idexamen='+obj.idexamen;
			    			 else var ruta='/examenes/teacherresrc_view/?idexamen='+obj.idexamen;
			    			 console.log(dniuser+'--'+obj.idpersonal);
                    		html+='<div class="col-xs-6 col-sm-4 col-md-2 " style="padding: 0.7ex">'
				    +'<a href="'+_sysUrlBase_+ruta+'" class="">'
					+'<div class="exa-item">'
						+'<div class="titulo">'+obj.titulo+'</div>'
						+'<div class="portada"><img class="img-responsive" width="100%" src="'+portada+'"></div>'
					+'</div></a></div>'
                    	});
                    if(html!=''){
                    	$('.pnllisexamenes').html(html);
                    }else{
                    	$('.pnllisexamenes').html('<h3><?php echo JrTexto::_("No data for this search"); ?></h3>');
                    }	
                  }else{
                    mostrar_notificacion(msjeAttention, data.mensaje, 'warning');
                  }
                  $('#procesando').hide('fast');
                  return false;
                },
                error: function(xhr,status,error){
                  mostrar_notificacion(msjeAttention, status, 'warning');
                  $('#procesando').hide('fast');
                  return false;
                }               
            });
	    	}catch(error){
	    		 mostrar_notificacion(msjeAttention, status, 'warning');
	    	}
	    }

//recargar combos de niveles 
		var leerniveles=function(data){
	        try{
	            var res = xajax__('', 'niveles', 'getxPadre', data);
	            if(res){ return res; }
	            return false;
	        }catch(error){
	            return false;
	        }       
	    }
	    var addniveles=function(data,obj){
	    	var objini=obj.find('option:first').clone();
	    	obj.find('option').remove();
	    	obj.append(objini);
	    	if(data!==false){
		    	var html='';
		    	$.each(data,function(i,v){
		    		html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
		    	});
		    	obj.append(html);
	    	}
	    	id=obj.attr('id');
	    	if(id==='activity-item')	cargarexamenes();
	    }

	    $('#level-item').change(function(){
	    	var idnivel=$(this).val();
            var data={tipo:'U','idpadre':idnivel}
            var donde=$('#unit-item');
            if(idnivel!=='') addniveles(leerniveles(data),donde);
            else addniveles(false,donde);
            donde.trigger('change');
	    });
	    $('#unit-item').change(function(){
	    	var idunidad=$(this).val();
            var data={tipo:'L','idpadre':idunidad}
            var donde=$('#activity-item');
            if(idunidad!=='') addniveles(leerniveles(data),donde);
            else addniveles(false,donde);
	    });
	    $('#activity-item').change(function(){
	    	cargarexamenes();
	    });

	    $('#tipo-examen').change(function(){
	    	cargarexamenes();
	    });

	    $('.panel').on('click','.verexamen',function(){
	    	var nivel=$('#level-item').val();
	    	var unidad=$('#unit-item').val();
	    	var actividad=$('#activity-item').val();
	    	var tipo=$('#tipo-examen').val();
	    	redir(_sysUrlBase_+'/examenes/ver/'+nivel+'/'+unidad+'/'+actividad+'/');			
	    });

	    showexamen('home');

	});
</script>