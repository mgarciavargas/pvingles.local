<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<div class="row" id="exam-reporte_grafico"><div class="col-xs-12">
	<h2><?php echo JrTexto::_("Assessment report"); ?></h2>
	<div class="col-xs-12">
		<div class="form-group">
			<div class="col-xs-6 select-ctrl-wrapper select-azul">
				<select class="form-control select-ctrl" id="opcCurso" name="opcCurso">
					<option value="">- <?php echo ucfirst(JrTexto::_("Select").' '.JrTexto::_("Course")); ?> -</option>
					<?php foreach ($this->cursosMatriculados as $i => $m) { ?>
					<option value="<?php echo $m['idcurso'] ?>"><?php echo $m['nombre'] ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<div class="col-xs-12 resultado">
		
	</div>
</div></div>

<script type="text/javascript">
var getDataReporte = function(dataGet){
	$.ajax({
		url: _sysUrlBase_+'/examenes/getResultados',
		type: 'GET',
		dataType: 'json',
		data: dataGet,
	}).done(function(resp) {
		if(resp.code=="ok"){
			console.log("success");
		}
	}).fail(function(err) {
		console.log("!error: ", err);
	});
	
};

$(document).ready(function() {
	$("#opcCurso").change(function(e) {
		var idCurso = $(this).val();
		getDataReporte({
			'idcurso' : idCurso,
		});
	});
});
</script>