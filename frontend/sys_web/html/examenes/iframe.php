<a href="<?php echo $this->urlGoBack; ?>" class="btn btn-primary" style="position: absolute; top: 10px;">
	<i class="fa fa-home"></i> <?php echo ucfirst(JrTexto::_('Go back')); ?>
</a>

<iframe src="<?php echo $this->url; ?>" frameborder="0" id="frameExam" style="width: 100%;"></iframe>

<script type="text/javascript">
var redimension = function(){
	var ventana = $(window).height();
	var header = $('header').height();
	$('#frameExam').css('height', (ventana-header-10)+'px');
};

$(document).ready(function() {
	redimension();
});
$(window).resize(function(e) {
	redimension();
});
</script>