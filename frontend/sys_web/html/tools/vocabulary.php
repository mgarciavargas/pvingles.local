<?php 
    defined('RUTA_BASE') or die(); $idgui = uniqid(); 
    $usuarioAct = NegSesion::getUsuario();
    $rolActivo=$usuarioAct["rol"];
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/css/tool_vocabulary.css">
<style type="text/css">
    .pnlvocabulario{
        border: 1px solid #88bdd8;
        padding: 1em 1ex;
        font-size: 1.2em;
        margin-bottom: 1ex;
        position: relative;
    }
    .pnlvocabulario .btnremovevoca{
        position: absolute;
        right: 0px;
        display: none;
    }
    .pnlvocabulario.edit .btnremovevoca{
        position: absolute;
        right: 0px;
        display: block;
        top:0;
    }
    .pnlvocabulario .texto
    {
       border:0px; 
    }
    .pnlvocabulario.edit .texto, .pnlvocabulario.edit .textoarea{
        min-width: 50px;
        height: 32px;
        cursor: pointer;
        display: inline-block;
        border:1px solid #ccc;
    }
    .pnlvocabulario.edit .textoarea{
         min-width: 100%;
        height: 32px;
    }
    .pnlvocabulario .icon1{
        color:#000;
        cursor: pointer;
    }
    .pnlvocabulario .icon2{
        color:#fac;
        cursor: pointer;
    }

</style>
<?php 
$vocAdmin=null;
$vocdocente=null;
$texto='';
if(!empty($this->datos))
    foreach ($this->datos as $voc){
        if($voc["orden"]=='0'){  $vocAdmin=$voc; }
        if($voc["idpersonal"]===$usuarioAct["dni"]&&$rolActivo!=1){  $vocdocente=$voc; }
        $texto.=$texto.$voc['texto'];
    }
    $texto=str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$texto);

if($rolActivo=='Alumno'){ ?>
<div class="container-fluid">
 <?php echo  $texto;  ?>    
</div>
<?php }else{ ?>
<div class="container-fluid" id="vocaall<?php echo $idgui; ?>">
    <div class="addallpnlvocabulario">
        <div class="row pnlvocabulario edit hidden" id="idclone<?php echo $idgui; ?>">       
            <div class="col-md-4 col-sm-6 col-xs-12">
                <img class="img-responsive img-thumbnails istooltip cargarimagenall imgportada" src="<?php echo $this->documento->getUrlStatic(); ?>/media/imagenes/levels/nofoto.jpg" width="170px" data-tipo="image" data-url="" alt="" title="<?php echo JrTexto::_('change image'); ?>">
            </div>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                    <div class="form-group" idioma="EN">
                        <label for="vocabulary"><?php echo JrTexto::_('Vocabulary') ?> <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/idiomas/en.png" style="width: 32px;">:</label>
                        <div class="text-center">
                        <span class="texto ingles"><?php echo JrTexto::_('text') ?> </span>                    
                            <i class="pronunciar icon1 fa fa-volume-up"></i>
                            <i class="pronunciar icon2 fa fa-volume-up deletrear"></i>
                        </div>                 
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                    <div class="form-group" idioma="ES">
                        <label for="word" ><?php echo JrTexto::_('Vocabulary') ?> <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/idiomas/es.png" style="width: 32px;">:</label>                    
                        <div class="text-center">
                            <span class="texto spanish"><?php echo JrTexto::_('text') ?></span>
                            <i class="pronunciar icon1 fa fa-volume-up"></i>
                            <!--i class="pronunciar icon2 fa fa-volume-up deletrear"></i-->
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                    <div class="textoarea ingles"><?php echo JrTexto::_('text') ?> </div>
                </div>            
            </div>
            <span class="btnremovevoca btn btn-danger nopreview"><i class="fa fa-trash"></i></span>            
        </div>
        <?php echo  $texto;  ?>
    </div>
    <div class="nova col-md-12 col-sm-12 col-xs-12 text-center">
        <a href="#" class="btn btn-warning btnvocaclone"> <i class="fa fa-plus"></i> <?php echo JrTexto::_('Add word'); ?></a>
        <a href="#" class="btn btn-primary " id="btnsavevoca<?php echo $idgui; ?>"> <i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
    </div>
    <div class="nova">
        <script type="text/javascript">
        $(document).ready(function(){

            $('.pnlvocabulario.edit span.texto').click(function(ev){
                ev.preventDefault();
                if($(this).find('input').length>0){
                    $(this).find('input').focus();
                    return;
                }
                var txt=$(this).text();
                var txtold='<?php echo JrTexto::_('text') ?>';
                var input='<input type="text" class="form-control" placeholder="'+txt+'" value="'+(txt!=txtold?txt:'')+'">';
                $(this).html(input);
                $(this).find('input').focus();
            });
            $('.pnlvocabulario.edit span.texto').on('focusout','input',function(ev){
                ev.preventDefault();                
                var txt=$(this).val();
                $(this).closest('.texto').html(txt);
            });

            $('.pnlvocabulario.edit .textoarea').click(function(ev){
                ev.preventDefault();
                if($(this).find('textarea').length>0){
                    $(this).find('textarea').focus();
                    return;
                }
                var txt=$(this).text();
                var txtold='<?php echo JrTexto::_('text') ?>';
                var input='<textarea class="form-control" placeholder="'+txt+'" style="width:100%">'+(txt!=txtold?txt:'')+'</textarea>';
                $(this).html(input);
                $(this).find('textarea').focus();
            });
            $('.pnlvocabulario.edit .textoarea').on('blur','textarea',function(ev){
                console.log('sadasd');
                ev.preventDefault();
                var txt=$(this).val();
                $(this).closest('.textoarea').html(txt);
            });

            $('.btnvocaclone').click(function(){
                var clone=$('#idclone<?php echo $idgui; ?>').clone(true);
                var idtmp=Date.now();
                clone.removeClass('hidden');
                clone.removeAttr('id');
                clone.find('img.imgportada').addClass('image'+idtmp).attr('data-url','.image'+idtmp);               
                $('#vocaall<?php echo $idgui; ?> .addallpnlvocabulario').append(clone);
                
            });            
            if($('.pnlvocabulario.edit').length==1) $('.btnvocaclone').trigger('click');

            $('#btnsavevoca<?php echo $idgui; ?>').click(function(){
                var texto=$('#vocaall<?php echo $idgui; ?> .addallpnlvocabulario').clone(true);
                texto.find('.hidden').remove();
                texto.find('.nova').remove();                
                var data = new Array();
                data.idNivel = $('#actividad-header').find('#txtNivel').val();
                data.idUnidad = $('#actividad-header').find('#txtunidad').val();
                data.idActividad = $('#actividad-header').find('#txtsesion').val();
                data.texto= texto.html();
                data.tool = 'V';
                var res = xajax__('', 'Tools', 'saveTools', data);
                if(res){console.log(res);}
            });

            $('.btnremovevoca').click(function(e){
                $(this).closest('.pnlvocabulario').remove();
            });

        });
        </script>
    </div>
</div>
<?php } ?>
<script type="text/javascript">
  $(document).ready(function(){
    loadVoices();
  });
</script>