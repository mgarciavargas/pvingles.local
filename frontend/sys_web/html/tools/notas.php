<?php defined('RUTA_BASE') or die(); 
$idgui = uniqid(); 
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
$img=null;
$vid=null;
$audios=null;
$pdf1=null;
$publicado=0;
$compartido=0;
$editando=false;
$orden=$this->orden;
$id_recurso=''; //no usamos esta tonteria
$voc=null;
if(!empty($this->datosdesc)){
 foreach ($this->datosdesc as $img1){        
        $vocAdmin=$img1;  
        $voc=$img1;
        if($img1["id_personal"]===$usuarioAct["dni"]&&$rolActivo!=1){
            $vocdocente=$img1;
        }
        if(!empty($img1["publicado"])){
            $publicado=$img1["publicado"];
        }
        if(!empty($img1["compartido"])){
            $compartido=$img1["compartido"];
        }
        if(!empty($img1["id_recurso"])){
            $id_recurso=$img1["id_recurso"];
        }
    }   
    $editando=true;
}
if(!empty($this->datostext)) $datostext=$this->datostext[0]; 
if(!empty($this->datosImagen)) $img=$this->datosImagen[0]; 
if(!empty($this->datosVideo)) $vid=$this->datosVideo[0];
if(!empty($this->datospdf)) $pdf1=$this->datospdf[0];
if(!empty($this->datosAudio)) $audios=$this->datosAudio[0];

$rutabase = $this->documento->getUrlBase();
$txtini='description';
?>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlStatic() ?>/libs/resource/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/css/tool_teacherresources.css">

<div id="teacher_resrc-tool" class="editando" data-idautor="<?php echo $voc['id_personal']; ?>">
    <div class="botones-generales text-center <?php echo $editando?'editando':''; ?>" data-id="<?php echo $id_recurso; ?>" >
        <a class="btn btn-info preview" href="#">
            <i class="fa fa-eye"></i> <?php echo JrTexto::_("Vista Previa") ?>
        </a>
        <a class="btn btn-info back"  href="#" style="display: none;">
            <i class="fa fa-arrow-left"></i> <?php echo JrTexto::_("Regresar") ?>
        </a>
        <a href="#" class="btn btn-success <?php echo $publicado==1?'':'btn-inactive'; ?> btnpublicar"><i class="fa fa-address-book-o"></i> <?php echo ucfirst(JrTexto::_('Publicar')) ?></a>
        
    </div>
    <table class="cuaderno" cellpadding="0" cellspacing="0">
        <tr>
            <td class="cuaderno-izq" background="<?php echo $this->documento->getUrlStatic() ?>/libs/resource/images/cuadernillo1.png?1" style="background-repeat:no-repeat" width="74"></td>
            <td class="cuaderno-contenido" background="<?php echo $this->documento->getUrlStatic() ?>/libs/resource/images/cuadernillo2.png?2" style="background-repeat:repeat; padding-right: 15px;" align="center">

                <div id="div_presentacion" class="row panel-resource aquihtml" style="background-image: url('<?php echo @str_replace('__xRUTABASEx__', $rutabase, $voc["caratula"]) ;?>'); ">
                    <div style="padding-right: 12px;padding-left: 12px;">
                        <div class="col-xs-12 controles-edicion" style="display: block;">      
                            <div class="btn btn-primary edithtml "><i class="fa fa-text-width"></i> <?php echo JrTexto::_('Editar texto') ?></div>
                            <div class="btn btn-primary savehtml " style="display: none;"><i class="fa fa-save"></i> <?php echo JrTexto::_('Guardar texto') ?></div>
                        </div>

                        <h1 class="col-xs-12 txttitulo div_fondo" style=""><?php echo $voc["titulo"] ;?></h1>

                        <div class="col-xs-12 txtpresentacion div_fondo"><?php echo $voc["texto"] ;?></div>

                        <div class="col-xs-12 div_txttitulo" id="div_txttitulo" style="display: none;">

                            <label class="control-label col-md-2" for="txtTitulo">
                                <?php echo JrTexto::_('Titulo');?>
                            </label>
                            <input type="text"  id="txtTitulo" name="txtTitulo" required="required" class="form-control" value="<?php echo $voc["titulo"] ;?>" >

                            <textarea class="txtpresentacionedit" id="txtarea_pnl1<?php echo $idgui; ?>" style="display: none;">
                                <?php echo @@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$vocdocente["texto"]); ?>
                            </textarea>

                            <a class="biblioteca-portada btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a image from our multimedia library or upload your own video')) ?>" data-tipo="image" data-url=".imgtemporal" style="display:">
                          <i class="fa fa-picture-o"></i> 
                                <?php echo ucfirst(JrTexto::_('select')).' '.JrTexto::_('image'); ?>
                            </a>
                            <img src="<?php echo @str_replace('__xRUTABASEx__', $rutabase, $voc["caratula"]) ;?>" class="imgtemporal hidden">

                        </div>
                    </div>
                    <div class="creado-por div_fondo">
                        <?php echo JrTexto::_("Created by") ?>: 
                        <span><?php echo $usuarioAct['nombre_full'] ;?></span>
                    </div>
                </div>

            <!--===============CREANDO TEXTO===========-->
            <div id="div_descripcion" class="row panel-resource aquihtml" style="background-image: url('<?php echo @str_replace('__xRUTABASEx__', $rutabase, $datostext["descripcion"]) ;?>'); ">
                    <div style="padding-right: 12px;padding-left: 12px;">
                        <div class="col-xs-12 controles-edicion" style="display: block;">      
                            <div class="btn btn-primary edithtml1 "><i class="fa fa-text-width"></i> <?php echo JrTexto::_('Editar texto') ?></div>
                            <div class="btn btn-primary savehtml " style="display: none;"><i class="fa fa-save"></i> <?php echo JrTexto::_('Guardar texto') ?></div>
                        </div>

                        

                        <div class="col-xs-12 txtpresentacion div_fondo"><?php echo $datostext["descripcion"] ;?></div>

                        <div class="col-xs-12 div_txttitulo" id="div_txttitulo" style="display: none;">

                            <textarea class="txtpresentacionedit" id="txtarea_pnl2<?php echo $idgui; ?>" style="display: none;">
                                <?php echo @@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$datostext["descripcion"]); ?>
                            </textarea>

                        </div>
                    </div>
                    <!--<div class="creado-por div_fondo">
                        <?php echo JrTexto::_("Created by") ?>: 
                        <span><?php echo $usuarioAct['nombre_full'] ;?></span>
                    </div>-->
                </div>
            <!--===============TERMINANDO DE CREAR TEXTO===========-->


                <div id="div_imagen" class="row panel-resource" style="display: block; padding:0 15px;">
                    <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("Images") ?></h1>
                    <div class="col-xs-12 controles-edicion nopreview">
                        <a class="open-biblioteca btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a image from our multimedia library or upload your own video')) ?>" data-tipo="image" data-url=".imgtemporal">
                            <i class="fa fa-picture-o"></i> 
                            <?php echo ucfirst(JrTexto::_('select')).' '.JrTexto::_('image') ?>
                        </a>
                        <img class="hidden imgtemporal temp" src="" style="display: none;">
                    </div>
                    <div class="row">
                        <div class="col-xs-12 image-selecc contenedor-slider">
                            <div class="myslider" style="width:90%; border: solid 0px #f00; ">
                                   <?php
                                   //echo $img["caratula"];
                                   $imgs = explode(",", $img["caratula"]);
                                   $canttext = explode(",", $img["texto"]);
                                   if ($img["caratula"]){
                                        $cantimg=count($imgs)-1;
                                    }else{
                                        $cantimg=-1;
                                   }
                                   
                                   for ($y=0;$y<=$cantimg;$y++){                                    
                                        echo '<a href="#" class="miniatura miniaturai" > <span class="del-miniatura nopreview"></span><img src="'.@str_replace('__xRUTABASEx__', $rutabase, $imgs[$y]).'" alt="image" style="height: 86px; width: 100%" data-tit="'.@$canttext[$y].'" data-pos="'.$y.'" ></a>';
                                   }
                                   ?>

                            </div>
                        </div>                        
                        <div class="col-xs-12 mascara_video">
                            <img class="image_resrc img-responsive img-thumbnail" src="" style="display: none; height: 300px;">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/noimage.png" class="img-responsive nopreview mask" style="/*width: 100%;height: 400px*/">
                        </div>
                        <div>
                            <input type="text"  id="txtDesc" name="txtDesc" required="required" class="form-control descripcion-media hidden" value="" style="width: 50%;" >
                            <div class="btn btn-primary saveimage nopreview hidden"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save Image') ?></div>

                            <input type="hidden"  id="txtpos" name="txtpos" required="required" value="">
                            <input type="hidden" class="contenido-caratula" id="txtimg" name="txtimg" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, $img["caratula"]); ?>">
                            <input type="hidden" class="contenido-texto" id="txtTit" name="txtTit" required="required" class="form-control" value="<?php echo $img["texto"]?>">
                        </div>
                    </div>
                </div>
        
                <div id="div_videos" class="row panel-resource" style="display: block">
                    <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("Videos") ?></h1>
                    <div class="col-xs-12 controles-edicion nopreview">
                        <a class="open-biblioteca btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a video from our multimedia library or upload your own video')) ?>" data-tipo="video" data-url=".vidtemporal">
                            <i class="fa fa-video-camera"></i>
                            <?php echo ucfirst(JrTexto::_('select')) ?> video
                        </a>
                        <video class="hidden vidtemporal temp" src="" style="display: none"></video>
                    </div>
                    <div class="col-xs-12 video-selecc contenedor-slider">
                        <div class="myslider">
                            <?php
                               //echo $img["caratula"];
                               $vids = explode(",", $vid["caratula"]);
                               $canttextv = explode(",", $vid["texto"]);
                               if ($vid["caratula"]){
                                    $cantvid=count($vids)-1;
                                }else{
                                    $cantvid=-1;
                               }
                               
                               for ($y=0;$y<=$cantvid;$y++){                                    
                                    echo '<a href="#" class="miniatura miniaturav" ><span class="del-miniatura nopreview"></span><video src="'.@str_replace('__xRUTABASEx__', $rutabase, $vids[$y]).'" alt="video" style="height: 86px; width: 100%" data-tit="'.$canttextv[$y].'" data-pos="'.$y.'" id="video'.$y.'"> </video></a>';
                               }
                               ?>
                        </div>
                    </div>

                    <div class="col-xs-12 mascara_video">
                        <video controls="true" class="valvideo video_resrc" src="" style="display: none;width: 100%;height: 300px;"></video>
                        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/novideo.png" class="img-responsive nopreview mask" style="/*width: 100%;height: 300px;*/">

                    </div>
                    <div style="/*width: 100%; position: relative; top:300px;*/">
                        <input type="text"  id="txtDescv" name="txtDescv" required="required" class="form-control descripcion-media hidden" value="" style="width: 50%" >
                        <div class="btn btn-primary savevideo nopreview hidden"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save Video') ?></div>

                        <input type="hidden"  id="txtposv" name="txtposv" required="required" value="">
                        <input type="hidden" class="contenido-caratula" id="txtvideo" name="txtvideo" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, $vid["caratula"]); ?>">
                        <input type="hidden" class="contenido-texto" id="txtTitv" name="txtTitv" required="required" class="form-control" value="<?php echo $vid["texto"]?>">
                    </div>
                </div>

                <div id="div_audios" class="row panel-resource" style="display: block">
                    <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("Audios") ?></h1>
                    <div class="col-xs-12 controles-edicion nopreview">
                        <a class="open-biblioteca btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select an audio from our multimedia library or upload your own audio')) ?>" data-tipo="audio" data-url=".audtemporal">
                            <i class="fa fa-video-camera"></i>
                            <?php echo ucfirst(JrTexto::_('select')) ?> audio
                        </a>
                        <audio class="hidden audtemporal temp" src="" style="display: none"></audio>
                    </div>
                    <div class="col-xs-12 audio-selecc contenedor-slider">
                        <div class="myslider">
                            <?php
                               //echo $img["caratula"];
                               $auds = explode(",", $audios["caratula"]);
                               $canttextA = explode(",", $audios["texto"]);
                               if ($audios["caratula"]){
                                    $cantvid=count($auds)-1;
                                }else{
                                    $cantvid=-1;
                               }
                               
                               for ($y=0;$y<=$cantvid;$y++){
                                    echo '<a href="#" class="miniatura miniatura_a"><span class="del-miniatura nopreview"></span><span href="#" class="file" src="'.@str_replace('__xRUTABASEx__', $rutabase, $auds[$y]).'" alt="audio" style="height: 86px; width: 100%" data-pos="'.$y.'" data-tit="'.$canttextA[$y].'" id="img'.$y.'"><i class="fa fa-file-audio-o fa-4x"></i></span></a>';
                               }
                               ?>
                        </div>
                    </div>

                    <div class="col-xs-12 mascara_video">
                        <audio controls="true" class="valvideo audio_resrc" src="" style="display: none;width: 100%;"></audio>
                        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/noaudio.png" class="img-responsive nopreview mask" style="/*width: 100%;height: 300px;*/">

                    </div>
                    <div style="/*width: 100%; position: relative; top:300px;*/">
                        <input type="text"  id="txtDesca" name="txtDesca" required="required" class="form-control descripcion-media hidden" value="" style="width: 50%" >
                        <div class="btn btn-primary saveaudio nopreview hidden"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save Audio') ?></div>

                        <input type="hidden"  id="txtposa" name="txtposa" required="required" value="">
                        <input type="hidden" class="contenido-caratula" id="txtaudio" name="txtaudio" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, $audios["caratula"]); ?>">
                        <input type="hidden" class="contenido-texto" id="txtTita" name="txtTita" required="required" class="form-control" value="<?php echo $audios["texto"]?>">
                    </div>
                </div>

                <div id="div_pdfs" class="row panel-resource" style="display: block">
                    <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("PDF") ?></h1>
                    <div class="col-xs-12 controles-edicion nopreview">
                        <a class="open-biblioteca btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a file from our multimedia library or upload your own video')) ?>" data-tipo="pdf" data-url=".ifrtemporal">
                            <i class="fa fa-file-pdf-o"></i> 
                            <?php echo ucfirst(JrTexto::_('select')).' '.JrTexto::_('file') ?>
                        </a>
                        <iframe class="hidden ifrtemporal temp" src="" style="display: none"></iframe>
                    </div>
                    <div class="col-xs-12 pdf-selecc contenedor-slider">
                        <div class="myslider">
                            <?php
                               //echo $img["caratula"];
                               $pdfs = explode(",", $pdf1["caratula"]);
                               $canttextp = explode(",", $pdf1["texto"]);
                               if ($pdf1["caratula"]){
                                    $cantpdf=count($pdfs)-1;
                                }else{
                                    $cantpdf=-1;
                               }

                               for ($y=0;$y<=$cantpdf;$y++){
                                    echo '<a href="#" class="miniatura miniaturap" ><span class="del-miniatura nopreview"></span><span class="file" src="'.@str_replace('__xRUTABASEx__', $rutabase, $pdfs[$y]).'" alt="pdf" style="height: 86px; width: 100%" data-tit="'.@$canttextp[$y].'" data-pos="'.$y.'" id="pdf'.$y.'"><i class="fa fa-file-pdf-o fa-4x"></i><span class="nombre-file">'.@$canttextp[$y].'</span></span></a>';
                               }
                            ?>
                        </div>
                    </div>                        
                    <div class="col-xs-12 mascara_video">
                        <iframe src="" frameborder="0" class="pdf_resrc" style="width: 100%; height: 375px; display: none;"></iframe>
                        <!--<img src="<?php echo $this->documento->getUrlStatic() ?>/media/web/pdf_file.jpg" class="img-responsive nopreview mask" style="/*width: 100%;height: 300px;*/">-->
                        <div class="mask">
                            <img src="<?php echo $this->documento->getUrlStatic() ?>/media/web/pdf_file.jpg" class="img-responsive center-block <?php echo !empty($pdf)?'hidden':''; ?>">
                            <span class="<?php echo !empty($pdf)?'hidden':''; ?>"><?php echo JrTexto::_('there is no file to display') ?></span>
                        </div>
                    </div>                        

                    <div style="/*width: 100%;position: relative; top:300px;*/" >
                        <input type="hidden"  id="txtDescp" name="txtDescp" required="required" class="form-control descripcion-media hidden" value="" style="width: 50%" >
                        <!--
                        <div class="btn btn-primary savepdf nopreview hidden" style="display:;"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save PDF') ?></div>
                        -->

                        <input type="hidden" id="txtposp" name="txtposp" required="required" value="">
                        <input type="hidden" class="contenido-caratula" id="txtpdf" name="txtpdf" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, $pdf1["caratula"]); ?>">
                        <input type="hidden" class="contenido-texto" id="txtTitp" name="txtTitp" required="required" class="form-control" value="<?php echo $pdf1["texto"]?>">

                    </div>
                </div>
            </td>
            <td class="cuaderno-der" background="<?php echo $this->documento->getUrlStatic() ?>/libs/resource/images/cuadernillo3.png?3" style="background-repeat:no-repeat" width="74"></td>
        </tr>
    </table>

    <div class="" id="main" style="position:absolute; top:-65px; right: 5px;">
        <ul id="" class="tabs">
            <li id="fuxia" class="hvr-bounce-in" >
                <a class="vertical" href="javascript:ver_div('div_presentacion')">
                    <?php echo JrTexto::_("Presentacion") ?>
                </a>
            </li>
            <li id="azul" class="hvr-bounce-in" >
                <a class="vertical" href="javascript:ver_div('div_descripcion')">
                    <?php echo JrTexto::_("Texto") ?>
                </a>
            </li>
            <li id="morado" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_pdfs')">
                    <span><?php echo JrTexto::_("PDF") ?></span>
                </a>
            </li>
            <li id="celeste" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_audios')">
                    <span><?php echo JrTexto::_("Audios") ?></span>
                </a>
            </li>
            <li id="verde" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_imagen')">
                    <span><?php echo JrTexto::_("Imágenes") ?></span>
                </a>
            </li>
            <li id="mostaza" class="hvr-bounce-in">
                <a class="vertical" href="javascript:ver_div('div_videos')">
                    <span><?php echo JrTexto::_("Videos") ?></span>
                </a>
            </li>
            <li id="azul" class="hvr-bounce-in">
                <a class="vertical" onclick="goBack()">
                    <span><?php echo JrTexto::_("Exit") ?></span>
                </a>
            </li>

        </ul>    
    </div>
</div>
<script type="text/javascript">
function goBack() {
    window.history.back();
}
//document.getElementById('div_imagen').style.display="none";
var _IDHistorialSesion=0;

var optSlike={
    infinite: false,
    navigation: false,
    slidesToScroll: 1,
    centerPadding: '60px',
    slidesToShow: 5,
    responsive:[
    { breakpoint: 1200, settings: {slidesToShow: 5} },
    { breakpoint: 992, settings: {slidesToShow: 4 } },
    { breakpoint: 880, settings: {slidesToShow: 3 } },
    { breakpoint: 720, settings: {slidesToShow: 2 } },
    { breakpoint: 320, settings: {slidesToShow: 1} }
    ]
};

var pos=-1;
var posv=-1;
var showdiv=[];
function ver_div(div){
    $('.panel-resource').hide();
    $('#'+div).show();
    if($('#'+div+' .myslider').hasClass('slick-slider')) $('#'+div+' .myslider').slick('unslick');
    if( $('#'+div+' .myslider').length>0 ){
        $('#'+div+' .myslider').slick(optSlike);
        if($.inArray(div,showdiv)==-1){
            showdiv.push(div);
            $('#'+div+' .myslider').find('a.miniatura').eq(0).trigger('click');
        }
        
    }
    $('audio').each(function(){ this.pause() });
    $('video').each(function(){ this.pause() });
}

function fileExists(url) {
    var rspta = false;
    if(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.send();
        rspta = (req.status==200);
    }
    return rspta;
}

function existeEnSlider(url, $panel){
    var rspta = true;
    if(url){
        var archivo = $panel.find('.myslider *[src="'+url+'"]');
        rspta = (archivo.length>0);
    }
    return rspta;
}

function insertarFileLista(){
    var tipo_file = $('.panel-resource:visible .open-biblioteca').attr('data-tipo');
    var tag_file = '<span class="del-miniatura nopreview"></span>';
    if(tipo_file=='video'){
        var $panel = $('#div_videos');
        var src = $panel.find('.vidtemporal').attr('src');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        var posv = $panel.find('.miniatura').length-1;
        if(existeArchivo){
            posv = posv + 1;
            tag_file += '<video src="'+src+'" alt="'+tipo_file+'" style="height: 86px; width: 100%" data-pos="'+posv+'" data-tit="null" id="video'+posv+'"> </video>';

            var vidold= $('#txtvideo').val();
            if (vidold) vidold=vidold+',';
            $('#txtvideo').val( vidold+src);

            $('#txtDescv').val('null');
            
            var txtTitv= $('#txtTitv').val();
            if (txtTitv) txtTitv=txtTitv+',';
            $('#txtTitv').val( txtTitv+'null');
            //alert("as");
            class_mini ="miniaturav";
            var tipo_file = 'V';
            var sw = 'OK';
        }
    }else if(tipo_file=='image'){
        var $panel = $('#div_imagen');
        var src = $panel.find('.imgtemporal').attr('src');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        var pos = $panel.find('.miniatura').length-1;
        if(existeArchivo){
            pos = pos + 1;
            tag_file += '<img src="'+src+'" alt="'+tipo_file+'" style="height: 86px; width: 100%" data-pos="'+pos+'" data-tit="null" id="img'+pos+'">';
            
            var imgold= $('#txtimg').val();
            if (imgold) imgold=imgold+',';
            $('#txtimg').val( imgold+src);

            $('#txtDesc').val('null');
            
            var txtTit= $('#txtTit').val();
            if (txtTit) txtTit=txtTit+',';
            $('#txtTit').val( txtTit+'null');
       
            class_mini ="miniaturai";
            var tipo_file = 'I';
            var sw = 'OK';
        }
    }else if(tipo_file=='audio'){
        var $panel = $('#div_audios');
        var dataAudio = $panel.find('.audtemporal').attr('data-audio');
        var src = _sysUrlStatic_+'/media/audio/'+dataAudio;
        var nombre = $panel.find('.audtemporal').attr('data-nombre-file');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        var posA = $panel.find('.miniatura').length-1;
        if(existeArchivo){
            posA = posA + 1;

            tag_file += '<span hre="#" class="file" src="'+src+'" alt="'+tipo_file+'" style="height: 86px; width: 100%" data-pos="'+posA+'" data-tit="'+nombre+'" id="img'+posA+'"><i class="fa fa-file-audio-o fa-4x"></i></span>';

            var audioOld= $('#txtaudio').val();
            if (audioOld) audioOld=audioOld+',';
            $('#txtaudio').val( audioOld+src);

            $('#txtDesca').val(nombre);

            var txtTita= $('#txtTita').val();
            if (txtTita) txtTita=txtTita+',';
            $('#txtTita').val( txtTita+nombre);

            class_mini ="miniatura_a";
            var tipo_file = 'A';
            var sw = 'OK';
        }
    }else if(tipo_file=='pdf'){
        var $panel = $('#div_pdfs');
        var src = $panel.find('.ifrtemporal').attr('src');
        var nombre = $panel.find('.ifrtemporal').attr('data-nombre-file');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        if(existeArchivo){
            
            posv = posv + 1;

            tag_file += '<span class="file" src="'+src+'" alt="'+tipo_file+'" style="height: 86px; width: 100%" data-pos="'+posv+'" data-tit="'+nombre+'" id="img'+posv+'"><i class="fa fa-file-pdf-o fa-4x"></i><span class="nombre-file">'+nombre+'</span></span>';

            var pdfold= $('#txtpdf').val();
            if (pdfold) pdfold=pdfold+',';
            $('#txtpdf').val( pdfold+src);

            $('#txtDescp').val(nombre);

            $('#txtposp').val(posv);
            var txtTitp= $('#txtTitp').val();
            if (txtTitp) txtTitp=txtTitp+',';
            $('#txtTitp').val( txtTitp+'null');

            class_mini ="miniaturap";
            var tipo_file = 'P';
            var sw = nombre;
        }
    }

    if(existeArchivo){
        if(!estaCargadoEnSlider){
            var _html = '<a href="#" class="miniatura '+class_mini+'">'+tag_file+'</a>';
            $panel.find('.myslider').slick('unslick');
            $panel.find('.myslider').append(_html);
            $panel.find('.myslider').slick(optSlike);
            $panel.find('.temp').attr('src','');
            guardarTResource(tipo_file,src,sw);
        } else {
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo JrTexto::_('The file has been already loaded');?>.', 'warning');
        }
    } else {
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo JrTexto::_('The file does not exist');?>.', 'error');
    }
}

function cambiarPortada(){
    var $divPresentacion = $('#div_presentacion');
    var src = $divPresentacion.find('.imgtemporal').attr('src');
    $divPresentacion.css({
        "background-image": 'url('+src+')',
    });
    //$divPresentacion.find('.imgtemporal').attr('src','');
}

var guardarTResource = function($tipo,$file,$sw){
    var data = new Array();
    
    data.tipo = $tipo;
    data.orden = <?php echo $this->orden; ?>;
    data.compartido=$('.btncompartir').hasClass('btn-inactive')?0:1;
    data.publicado=$('.btnpublicar').hasClass('btn-inactive')?0:1;

    if ($tipo=='D'){
        data.texto= document.getElementById('txtarea_pnl1<?php echo $idgui; ?>').value;
        data.titulo= document.getElementById('txtTitulo').value;
        data.caratula= $('#div_presentacion').find('.imgtemporal').attr('src');
    }else if($tipo=='T'){
        data.descripcion= document.getElementById('txtarea_pnl2<?php echo $idgui; ?>').value;
    }else if ($tipo=='I'){

        data.texto= $('#txtTit').val();
        data.titulo= "";
        data.pos= $('#txtpos').val();            
        //alert(data.pos);            
        data.caratula= $('#txtimg').val();

        if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$('#txtDesc').val();
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTit').val(texto);
            data.texto= $('#txtTit').val();

            $("#img"+data.pos).attr("data-tit",$('#txtDesc').val());

            data.titulo= "";
            data.pos= "null";
        }
    }else if ($tipo=='V'){
        data.texto= $('#txtTitv').val();
        data.titulo= "";
        data.pos= $('#txtposv').val();
        data.caratula= $('#txtvideo').val();
        //alert(data.caratula);
        if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$('#txtDescv').val();
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTitv').val(texto);
            data.texto= $('#txtTitv').val();
            $("#video"+data.pos).attr("data-tit",$('#txtDescv').val());
            data.titulo= "";
            data.pos= "null";
        }
    }else if ($tipo=='A'){
        data.texto= $('#txtTita').val();
        data.titulo= "";
        data.pos= $('#txtposa').val();
        data.caratula= $('#txtaudio').val();
        //alert(data.caratula);
        if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$('#txtDesca').val();
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTita').val(texto);
            data.texto= $('#txtTita').val();
            $("#video"+data.pos).attr("data-tit",$('#txtDesca').val());
            data.titulo= "";
            data.pos= "null";
        }
    }else if ($tipo=='P'){
        data.texto= $('#txtTitp').val();            
        data.titulo= "";
        data.pos= $('#txtposp').val();
        data.caratula= $('#txtpdf').val();
        //alert(data.caratula);

        //if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$sw;
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTitp').val(texto);
            data.texto= $('#txtTitp').val();
            $("#pdf"+data.pos).attr("data-tit",$('#txtDescp').val());
            data.titulo= "";
            data.pos= "null";
        //}
        //return;
    }
    //alert($file);
    //data.pkIdlink= idpk||0;
    console.log(data);
    var res = xajax__('', 'bib_recursos', 'saveBib_recursos', data);
    console.log(res);
    if(res){
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo JrTexto::_('saved successfully');?>.', 'success');
    }
};

var IDGame = '';

$(document).ready(function(){
    $('.btnpublicar').click(function(ev){
        var obj=$(this);
        var data =$(this).hasClass('btn-inactive')?1:0;
       if(data==1){
           $(this).removeClass('btn-inactive').addClass('btn-success');
        }else{
           $(this).addClass('btn-inactive').removeClass('btn-success');
        }
        var parent=$(this).parent();
        var id=parent.attr('data-id'); 
        if(id!=undefined&&id!=''){
            var res = xajax__('', 'bib_recursos', 'setCampo', id,'publicado',data);
        }
         
    });

    $('.btncompartir').click(function(ev){
        var obj=$(this);
        var data =$(this).hasClass('btn-inactive')?1:0;
        if(data==1){
           $(this).removeClass('btn-inactive').addClass('btn-primary');
        }else{
           $(this).addClass('btn-inactive').removeClass('btn-primary');
        }
        var parent=$(this).parent();
        var id=parent.attr('data-id');
        if(id!=undefined&&id!=''){
            var res = xajax__('', 'bib_recursos', 'setCampo', id,'compartido',data);
        }
    });
    var ajustarAltura = function(){
        $('.cuaderno-contenido').children('.panel-resource').css({
            'max-height': '610px',
        });
    }; 
    var cargarAutorLibro = function(){
        var idautor = $('#teacher_resrc-tool').attr('data-idautor');
        if(idautor==undefined || idautor=='') return false;
        var formData = new FormData();
        formData.append("dni", idautor.trim());
        $.ajax({
            url: _sysUrlBase_+'/usuario/buscarjson/',
            type: "POST",
            data:  {'dni' : idautor},
            dataType: 'json',
            success: function(resp){
                if(resp.code==='ok'){
                    var datainfo=resp.data;
                    var nombre_full=datainfo.ape_paterno+' '+datainfo.ape_materno+' '+datainfo.nombre;
                    if(nombre_full.trim()!='') $('.creado-por span').html(nombre_full);
                }
            },
            error: function(e){
                $('.autor span#infoautor').html('Error?');
            }
        });
    };
    var mostrarOcultarBtnGuardar= function($this){
        var idPanel = $this.closest('.panel-resource').attr('id');
        var $panel = $('#'+idPanel);
        var cant_tabs = $panel.find('ul.nav-tabs li').length;
        if(cant_tabs>=1) $panel.find('.btn.save').removeClass('hidden');
        else $panel.find('.btn.save').addClass('hidden');
    };
    ajustarAltura(); /* ¡¡¡MUY NECESARIO PARA QUE NO SE DESBORDE HACIA ABAJO!!! */
    cargarAutorLibro();

 /*** #Fn generales(Img,Vid,Pdf) ***/
    $('.panel-resource')
        .on('click',".open-biblioteca",function(e){ 
            e.preventDefault();
            var txt= '<?php echo JrTexto::_('Select or upload'); ?>';
            selectedfile(e,this,txt, 'insertarFileLista');
        }).on('click',".miniaturai",function(e){ 
            e.preventDefault();
            var id = $(this).parents('.panel-resource').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $('#txtDesc').val(tit).removeClass('hidden');
            $('#txtDesc').siblings('.btn.saveimage').removeClass('hidden');
            $('#txtpos').val(pos);
        }).on('click',".miniaturav",function(e){ 
            e.preventDefault();
            var id = $(this).closest('.panel-resource').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            console.log($panel.find('.'+tipo+'_resrc'));
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $('#txtDescv').val(tit).removeClass('hidden');
            $('#txtDescv').siblings('.btn.savevideo').removeClass('hidden');
            $('#txtposv').val(pos);
            
            //alert("as");
        }).on('click',".miniatura_a",function(e){ 
            e.preventDefault();
            var id = $(this).closest('.panel-resource').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            console.log($panel.find('.'+tipo+'_resrc'));
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $('#txtDesca').val(tit).removeClass('hidden');
            $('#txtDesca').siblings('.btn.saveaudio').removeClass('hidden');
            $('#txtposa').val(pos);
            
            //alert("as");
        }).on('click',".miniaturap",function(e){ 
            e.preventDefault();
            var id = $(this).closest('.panel-resource').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $('#txtDescp').val(tit).removeClass('hidden');
            $('#txtDescp').siblings('.btn.savepdf').removeClass('hidden');
            $('#txtposp').val(pos);
            
            //alert("as");
        }).on('click', '.miniatura>.del-miniatura', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var idPanel = $(this).closest('.panel-resource').attr('id');
            var $miniatura = $(this).parent();
            var src = $miniatura.children('*[src]').attr('src');
            var tipo = $miniatura.children('*[src]').attr('alt');
            var $slider = $(this).parents('.myslider');

            var all_media = $('#'+idPanel+' .contenido-caratula').val()
            var all_textos = $('#'+idPanel+' .contenido-texto').val()
            var arrMedia = all_media.split(',');
            var arrTextos = all_textos.split(',');
            var index = arrMedia.indexOf(src);
            var sw = 'OK';
            arrMedia.splice(index, 1);
            arrTextos.splice(index, 1);

            var new_media = arrMedia.join(',');
            var new_textos = arrTextos.join(',');
            $('#'+idPanel+' .contenido-caratula').attr('value', new_media);
            $('#'+idPanel+' .contenido-texto').attr('value', new_textos);

            $slider.slick('unslick');
            $miniatura.remove();
            $slider.slick(optSlike);
            if(tipo=='image'){
                var type = 'I';
            }else if(tipo=='video'){
                var type = 'V';
            }else if(tipo=='audio'){
                var type = 'A';
            }else if(tipo=='pdf'){
                var type = 'P';
                sw = ''
            }
            guardarTResource(type, '', sw);
        });

    $('#teacher_resrc-tool')
        .on('click', '.live-edit', function(e){
            console.log('live edit CLICK');
            if(!$('#teacher_resrc-tool').hasClass('editando')){
                return false;
            }
            $(this).css({"border": "0px"}); 
            addtext1(e,this);
        })
        .on('blur','.live-edit>input',function(e){                e.preventDefault();
            $(this).closest('.live-edit').removeAttr('Style'); 
            addtext1blur(e,this);
        })
        .on('keypress','.live-edit>input', function(e){
            if(e.which == 13){ 
                e.preventDefault();          
                $(this).trigger('blur');
            } 
        })
        .on('keyup','input', function(e){
            if(e.which == 27){ 
                $(this).attr('data-esc',"1");
                $(this).trigger('blur');
            }
        });
    
    $('#div_imagen').on('click','.saveimage',function(){
        //alert("img");
        var txtimg= $('#txtimg').val();        
        //alert(txtimg);
        guardarTResource('I',txtimg,'');
    });

    $('#div_videos').on('click','.savevideo',function(){
        //alert("img");
        var txtvid= $('#txtvid').val();        
        //alert(txtimg);
        guardarTResource('V',txtvid,'');
    });

    $('#div_audios').on('click','.saveaudio',function(){
        //alert("img");
        var txtaud= $('#txtaudio').val();        
        //alert(txtimg);
        guardarTResource('A',txtaud,'');
    });

    $('#div_pdfs').on('click','.savepdf',function(){
        //alert("img");
        var txtpdf= $('#txtpdf').val();        
        //alert(txtimg);
        guardarTResource('P',txtpdf,'');
    });
   
    $("form").keypress(function(e) {
        if (e.which == 13) {
            e.preventDefault();
        }
    });

    $('#teacher_resrc-tool .preview').click(function(){
        $(this).hide();
        $(this).siblings('.back').show();
        $('#div_presentacion .btn-toolbar').hide();
        $('form.form-inline').hide();
        $('#pnlactividadesresourses').hide();
        $('#div_voca input.palabra').val('');
        $('#div_voca .btn.search-word').trigger('click');
        $('#div_voca .result-busqueda .palabra_vocab').removeClass('palabra_vocab').removeClass('active');
        $('#div_games .resultado-buscar').hide();
        $('#div_examen .resultado-buscar').hide();
        $('.controles-edicion').hide();
        $('.embed-responsive #txtDesc').hide();
        $('.embed-responsive .saveimage').hide();

        $('#teacher_resrc-tool .nopreview').hide();
        $('#teacher_resrc-tool .descripcion-media').attr('readonly','readonly');
        $('.botones-generales').children('a:not(.preview,.back)').hide();
        ver_div('div_presentacion');        
    });

    $('#teacher_resrc-tool .back').click(function(){
        $(this).hide();
        $(this).siblings('.preview').show();
        $('#div_presentacion .btn-toolbar').show();
        $('form.form-inline').show();
        $('#pnlactividadesresourses').show();
        $('#div_voca .result-busqueda .list-group-item').addClass('palabra_vocab').addClass('active');
        $('#div_games .resultado-buscar').show();
        $('#div_examen .resultado-buscar').show();
        $('.controles-edicion').show();
        $('#div_imagen #txtDesc').show();
        $('.embed-responsive #txtDesc').show();
        $('.embed-responsive .saveimage').show();
        $('.botones-generales').children('a:not(.preview,.back)').show();
        $('#teacher_resrc-tool .nopreview').show();
        $('#teacher_resrc-tool .descripcion-media').removeAttr('readonly');
    });

 /*** #Presentacion ***/
    var edithtml=function(id){
        tinymce.init({
            relative_urls : false,
            convert_newlines_to_brs : true,
            menubar: false,
            statusbar: false,
            verify_html : false,
            content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
            selector: '#'+id,
            height: 250,       
          plugins:["table textcolor" ], 
          toolbar: 'undo redo | table | styleselect |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor'
        });
    }; 
    $('#div_presentacion').on('click','.edithtml',function(){
        $(this).hide("fast");
        $(this).siblings('.savehtml').show();
        var content=$(this).parents('.aquihtml');
        var txt=content.find('.txtpresentacion');
        var txtedit=content.find('.txtpresentacionedit');
            txtedit.val(txt.html());
            txt.hide();
            txtedit.show();

            var txt1=content.find('.txttitulo');
            var txttit=content.find('.div_txttitulo');            
            txt1.hide();
            txttit.show();
            edithtml(txtedit.attr('id'));
    });
    $('#div_presentacion').on('click','.savehtml',function(){
        $(this).hide("fast");
        var content=$(this).closest('.aquihtml');
        tinyMCE.triggerSave();
        var txt=content.find('.txtpresentacion');
        var txtedit=content.find('.txtpresentacionedit'); 
            txt.show();        
            txt.html(txtedit.val()); 
            tinymce.remove('#'+txtedit.attr('id')); 
            txtedit.hide();          

            var txt1=content.find('.txttitulo');
            var txttit=content.find('.div_txttitulo');            
            var txttit1=document.getElementById('txtTitulo').value;
            txt1.show();
            txt1.html(txttit1); 
            txttit.hide();
            guardarTResource('D','','');
       $(this).siblings('.edithtml').show();
    });
    $('#div_presentacion').on('click', '.biblioteca-portada', function(e) {
        e.preventDefault();
        var txt= '<?php echo JrTexto::_('Select or upload'); ?>';
        selectedfile(e,this,txt, 'cambiarPortada');
    });
    var edithtml1=function(id){
        tinymce.init({
            relative_urls : false,
            convert_newlines_to_brs : true,
            menubar: false,
            statusbar: false,
            verify_html : false,
            content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
            selector: '#'+id,
            height: 250,       
          plugins:["table textcolor" ], 
          toolbar: 'undo redo | table | styleselect |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor'
        });
    }; 
    $('#div_descripcion').on('click','.edithtml1',function(){
        $(this).hide("fast");
        $(this).siblings('.savehtml').show();
        var content=$(this).parents('.aquihtml');
        var txt=content.find('.txtpresentacion');
        var txtedit=content.find('.txtpresentacionedit');
            txtedit.val(txt.html());
            txt.hide();
            txtedit.show();

            var txt1=content.find('.txttitulo');
            var txttit=content.find('.div_txttitulo');            
            txt1.hide();
            txttit.show();
            edithtml1(txtedit.attr('id'));
    });
    $('#div_descripcion').on('click','.savehtml',function(){
        $(this).hide("fast");
        var content=$(this).closest('.aquihtml');
        tinyMCE.triggerSave();
        var txt=content.find('.txtpresentacion');
        var txtedit=content.find('.txtpresentacionedit'); 
            txt.show();        
            txt.html(txtedit.val()); 
            tinymce.remove('#'+txtedit.attr('id')); 
            txtedit.hide();          

            var txt1=content.find('.txttitulo');
            var txttit=content.find('.div_txttitulo');            
            var txttit1=document.getElementById('txtTitulo').value;
            txt1.show();
            txt1.html(txttit1); 
            txttit.hide();
            guardarTResource('T','','');
       $(this).siblings('.edithtml1').show();
    });
    $('#div_descripcion').on('click', '.biblioteca-portada', function(e) {
        e.preventDefault();
        var txt= '<?php echo JrTexto::_('Select or upload'); ?>';
        selectedfile(e,this,txt, 'cambiarPortada');
    });

    /******************************************************/
    var registrarHistorialSesion = function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/agregar',
            type: 'POST',
            dataType: 'json',
            data: {'lugar': 'TR', 'fechaentrada': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                console.log("success");
                console.log(resp);
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                console.log(resp);
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log("error");
            console.log(xhr.responseText);
        });
        
    };
    ver_div('div_presentacion');
    registrarHistorialSesion();
    $(window).on('beforeunload', function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/editar',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {'idhistorialsesion': _IDHistorialSesion, 'fechasalida': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                console.log("success");
                console.log(resp);
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log("error");
            console.log(xhr.responseText);
            return false;
        });
    });
});
</script>