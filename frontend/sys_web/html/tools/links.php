<?php defined('RUTA_BASE') or die(); $idgui = uniqid(); 
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/css/tool_links.css">
<?php 
$linkAdmin=null;
$linkdocente=null;
if(!empty($this->datos))
    foreach ($this->datos as $link){
        if($link["orden"]=='0'){
            $linkAdmin=$link;
        }
        if($link["idpersonal"]===$usuarioAct["dni"]&&$rolActivo!=1){
            $linkdocente=$link;
        }
    }
$txtini='<p>'.JrTexto::_('First interest link').':<a href="/" target="_blank" title="English">'.JrTexto::_('Learn English easily').'</a></p>';
 if(empty($linkAdmin)){
    $linkAdmin["texto"]=$txtini;
    }
  if(empty($linkdocente)){
    $linkdocente["texto"]=$txtini;
    }
if($rolActivo=='Alumno'){?>
<div class="container-fluid">
    <div class="row links-container" >
        <div class="to_clone">
            <h2 class="col-md-12 col-sm-12 col-xs-12 title-links"><?php echo JrTexto::_('check these out') ?>...</h2>
            <div class="col-md-12 col-sm-12 col-xs-12 links-main">
                <div class="tbl-links">
                    <?php if(!empty($this->datos))
                        foreach ($this->datos as $link){  
                            echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$link["texto"])."<br>";
                        } ?>
                </div>
            </div>
        </div>
    </div>
    <hr>
</div>
<?php }else{ ?>
<div class="container-fluid" id="links-tool">
    <div class="row links-container" id="admin-links">
        <div class="to_clone" data-edit="pnl0<?php echo $idgui; ?>">
            <h2 class="col-md-12 col-sm-12 col-xs-12 title-links"><?php echo JrTexto::_('check these out') ?>...</h2>
            <?php if($rolActivo==1){?>
            <div class="col-md-12 col-sm-12 col-xs-12 nopreview">
                <div class="btn-toolbar" role="toolbar" >
                    <div class="btn btn-xs btn-group btnedithtml<?php echo $idgui; ?>"><i class="fa fa-text-width"></i>
                    </div>
                    <div class="btn btn-xs btn-group btnsavehtml<?php echo $idgui; ?> disabled" data-id="<?php echo @$linkAdmin["idtool"] ?>">
                        <i class="fa fa-save"></i>
                    </div>
                    <div class="btn btn-xs btn-group btnborrarhtml<?php echo $idgui; ?>" data-id="<?php echo @$linkAdmin["idtool"] ?>">
                        <i class="fa fa-trash"></i>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="col-md-12 col-sm-12 col-xs-12 links-main">
                <?php if($rolActivo==1){?>
                <textarea id="txtarea_pnl0<?php echo $idgui; ?>" style="display: none">
                   <?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$linkAdmin["texto"]); ?>
                </textarea>
                <?php } ?>
                <div class="tbl-links panel_pnl0<?php echo $idgui; ?>">
                    <?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$linkAdmin["texto"]); ?>
                </div>
            </div>
        </div>
    </div>
<?php if($rolActivo!=1){?>
    <hr>
    <div class="row links-container" id="other-links">
        <div class="row other-addpnl1<?php echo $idgui; ?>">
            <div class="col-md-12 col-sm-12 col-xs-12 nopreview">
                <a href="#" class="btn btn-primary addMyLinks" style="display: block;" data-edit="pnl1<?php echo $idgui; ?>">
                    <i class="fa fa-plus-square"></i> 
                    <span><?php echo JrTexto::_('add my own links') ?></span>
                </a>
            </div>
        </div>
        <div class="to_clone hidden d_pnl1<?php echo $idgui; ?>" data-edit="pnl1<?php echo $idgui; ?>">            
            <h2 class="col-md-12 col-sm-12 col-xs-12 title-links text-right">...<?php echo JrTexto::_('a few links more'); ?></h2>
            <?php  if($rolActivo!=1){?>
            <div class="col-md-12 col-sm-12 col-xs-12 nopreview">
                <div class="btn-toolbar" role="toolbar" >
                    <div class="btn btn-xs btn-group btnedithtml<?php echo $idgui; ?>">
                        <i class="fa fa-text-width"></i>
                    </div>
                    <div class="btn btn-xs btn-group btnsavehtml<?php echo $idgui; ?> disabled" data-id="<?php echo @$linkdocente["idtool"] ?>">
                        <i class="fa fa-save"></i>
                    </div>
                    <div class="btn btn-xs btn-group btnborrarhtml<?php echo $idgui; ?>" data-id="<?php echo @$linkdocente["idtool"] ?>">
                        <i class="fa fa-trash"></i>
                    </div>
                </div>
            </div>
            <?php  } ?>
            <div class="col-md-12 col-sm-12 col-xs-12 links-main">
                <?php if($rolActivo!=1){?>
                <textarea id="txtarea_pnl1<?php echo $idgui; ?>" style="display: none">
                   <?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$linkdocente["texto"]); ?>
                </textarea>
                <?php } ?>
                <div class="tbl-links panel_pnl1<?php echo $idgui; ?>">
                    <?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$linkdocente["texto"]); ?>
                </div>
            </div>
            <div class="row other-removepnl1<?php echo $idgui; ?>">
                <div class="col-md-12 col-sm-12 col-xs-12 nopreview">
                    <a href="#" class="btn btn-danger btnborrarhtml<?php echo $idgui; ?>" data-id="<?php echo @$linkdocente["idtool"] ?>" style="display: block;">
                        <i class="fa fa-trash"></i> 
                        <span><?php echo JrTexto::_('Remove just my own links') ?></span>
                    </a>
                </div>
            </div>  
        </div>        
    </div>
    <?php } ?>
</div>
<script type="text/javascript">
    $('.btnborrarhtml<?php echo $idgui; ?>').click(function(e){
        e.preventDefault();
        var obj=$(this).closest('.to_clone');
        var pnl=obj.data('edit');        
        var res = xajax__('', 'Tools', 'eliminar', $(this).data('id'));
        $(obj).find('#textarea_'+pnl).html('<?php echo $txtini;?>');
        $(obj).find('.tbl-links.panel_'+pnl).html('<?php echo $txtini;?>');
        $(this).addClass('disabled').attr('data-id',0);
        $(this).siblings('[data-id]').addClass('disabled').attr('data-id',0);
        $('.other-add'+pnl).show();
        $('.other-add'+pnl+' .addMyLinks').show();
        $('.d_'+pnl).hide();
    });


    var iniciarLink = function(idlinks){
        $(idlinks+'   .btn.btnedithtml<?php echo $idgui; ?>').trigger('click');
        $(idlinks+'   .panel<?php echo $idgui; ?>').hide();
        $(idlinks+'   .btnedithtml<?php echo $idgui; ?>').addClass('disabled');
    };

    var asignarDimensionModal = function($elem_tool){
        var height = $(document).outerHeight();
        var idModal = $elem_tool.parents('.modal').attr('id');
        $('#'+idModal+' #modalcontent').css({
            'max-height': (height-180)+'px',
            'overflow' : 'auto',
        })
    };

    var darFormatoLink = function(){
        var $p_link = $('#links-tool .tbl-links p');
        $p_link.removeAttr('style');
        $p_link.children().removeAttr('style');
        $p_link.find('a').removeAttr('target');
        $p_link.find('a').attr('target', '_blank');
        $p_link.each(function(){
            if( $(this).text()=='' ) $(this).remove();
            $(this).html(function(i,h){
                return h.replace(/&nbsp;/g,'');
            })
        })
    };

    var guardarLinks = function($textarea,idpk,btn){
        var data = new Array();
        data.idNivel = $('#actividad-header').find('select#txtNivel').val();
        data.idUnidad = $('#actividad-header').find('select#txtunidad').val();
        data.idActividad = $('#actividad-header').find('select#txtsesion').val();
        data.tool = 'L';
        data.texto= $textarea.val();
        data.pkIdlink= idpk||0;
        var res = xajax__('', 'Tools', 'saveTools', data);
        if(res){
            $(btn).attr('data-id',res);
            $(btn).siblings('[data-id]').attr('data-id',res);
        }
    };

    var previewLinks = function(){
        $('#links-tool .nopreview').hide();
        $('#links-tool .btnsavehtml<?php echo $idgui; ?>').trigger('click');
    };

    var vertinymce_Link=function(id,txt=null){
        if(txt!=''&&txt!=undefined&&txt!=null){
            $(id).html(txt);
        }
        tinymce.init({
            relative_urls : false,
            convert_newlines_to_brs : true,
            menubar: false,
            statusbar: false,
            verify_html : false,
            content_css : URL_BASE+'/static/tema/css/bootstrap.min.css',
            selector: id,
            height: 400,
            plugins:["table link chingoinput chingoimage chingoaudio chingovideo textcolor" ], 
            toolbar: 'undo redo | table | styleselect |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor | chingoinput chingoimage chingoaudio chingovideo | link'
        });
    };

    $('#links-tool #admin-links, #links-tool #other-links')
        .on('click', '.btnsavehtml<?php echo $idgui; ?>', function(e){
            if($(this).hasClass('disabled')){
                return false;
            }
            $(this).addClass('disabled');
            var obj=$(this).closest('.to_clone');
            var pnl=obj.data('edit');
            var idContainer = ' #' + $(this).parents('.links-container').attr('id');
            $(idContainer+' .btnedithtml<?php echo $idgui; ?>').removeClass('disabled');
            tinyMCE.triggerSave();
            var txt=$(idContainer+' #txtarea_'+pnl).val();
            $(idContainer+' .panel_'+pnl).show();
            $(idContainer+' .panel_'+pnl).html(txt);

            tinymce.remove(idContainer+' textarea');   
            $(idContainer+' textarea').hide();
            $(idContainer+' .btnsavehtml<?php echo $idgui; ?>').addClass('disabled');
            $(idContainer+' .btnborrarhtml<?php echo $idgui; ?>').removeClass('disabled');            
            btn=$(this);
            darFormatoLink();
            guardarLinks( $(idContainer+' textarea'),$(this).data('id'),btn);
            
        })
        .on('click', '.btnedithtml<?php echo $idgui; ?>', function(e){
            var idContainer = ' #' + $(this).parents('.links-container').attr('id');
            if($(this).hasClass('disabled')){
                return false;
            }else{
                var obj=$(this).closest('.to_clone');
                var pnl=obj.data('edit');
                $(idContainer+' .panel_'+pnl).hide();
                $(idContainer+' textarea').show();
                $(idContainer+' .btnedithtml<?php echo $idgui; ?>').addClass('disabled');
                $(idContainer+' .btnsavehtml<?php echo $idgui; ?>').removeClass('disabled');
                vertinymce_Link('#'+$(idContainer+' textarea').attr('id'));
            }
        });

    $('#links-tool #other-links')
        .on('click', '.btn.addMyLinks', function(e){
            e.preventDefault();
            var objid=$(this).data('edit');
            $('.d_'+objid).show().removeClass('hidden');
            $('.other-remove'+objid+' .btn').removeClass('disabled');
            $(this).hide();           
        })
        .on('click', '.btn.removeLinks', function(e){
            e.preventDefault();
            $('#other-links').find('.cloned').remove();
            $('.btn.addMyLinks').removeClass('hidden');
        });


    $('.preview').click(function(e) {
        previewLinks();
    });

    $('.btnsaveActividad').click(function(e){
        previewLinks();
    });

   // iniciarLink('#admin-links');
    asignarDimensionModal( $('#links-tool') );
    <?php if(!empty($linkdocente["idlink"])){?>
        $('.btn.addMyLinks').trigger('click');
    <?php } ?>
</script>
<?php } ?>