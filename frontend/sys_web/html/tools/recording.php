<?php defined('RUTA_BASE') or die(); $idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
if(!empty($this->datos[0])){
  $recording=@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$this->datos[0]["texto"]);
}
?>
<div class="container-fluid" id="record-tool">
 <div class="row">
 	<div class="col-md-12 col-sm-12 col-xs-12">
 	<?php if($rolActivo!='Alumno'){?>
      <div class="col-md-12 col-sm-12 col-xs-12 nopreview">
        <div class="btn-toolbar" role="toolbar" >
          <div class="btn btn-xs btn-group btn-orange btnselectedfile" data-tipo="audio" data-url=".adoc<?php echo $idgui ?>" ><i class="fa fa-music" ></i></div>
          <div class="btn btn-xs btn-group btn-warning btngrabar" data-donde="waveform" data-multiple="0"><i class="fa fa-microphone"></i></div>
          <div class="btn btn-xs btn-group btn-warning btnsave" data-donde="wavegrabar"><i class="fa fa-save"></i></div>         
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12"> Name :<input type="text" name="nameaduodoccente" id="nameaduodoccente" value="" placeholder="Nombre de Audio"></div>
      </div>
    <?php } ?> 
     <div>
          <audio src="<?php echo @$recording; ?>" class="adoc<?php echo $idgui ?>" id="audioadoc<?php echo $idgui ?>" style="display: none;"></audio>
     </div>	
 	</div> 	
 </div>
 <div id="waveform" style="width: 100%"></div> <br>
 <div class="btn btn-xs btn-group btn-warning btngrabar" data-donde="grabado" data-multiple="1">Grabar Audio <i class="fa fa-microphone"></i></div>
 <div id="grabado" style="width: 100%"></div>
 <!--ul id="recordingslist"></ul-->

 <script type="text/javascript">
 var rutaslib='<?php echo $this->documento->getUrlStatic() ?>/libs/audiorecord/';
 var saveaudiotipo='-1';
 </script>
 <script src="<?php echo $this->documento->getUrlStatic(); ?>/libs/audiorecord/wavesurfer.min.js"></script>
 <script src="<?php echo $this->documento->getUrlStatic(); ?>/libs/audiorecord/audioRecord.js?id=3"></script>
 <script src="<?php echo $this->documento->getUrlStatic(); ?>/libs/audiorecord/main.js?id=2"></script>
 <script type="text/javascript">
  
  if($('#audioadoc<?php echo $idgui ?>').attr('src')!=''){
    _loadaudio();
  };

  $('#grabado').on('click','.aplayblob',function(){
    recrearonda($(this));        
  });
  
  $('#grabado').on('click','.btnremovelista',function(){
    $(this).closest('tr').remove(); 
  });

  $('#grabado').on('click','.btnSave',function(){
    alert('enviado'); 
  });

 	$('.tooltip').tooltip();  
	$('.btnselectedfile').click(function(e){ 
     $(this).addClass('disabled');
	   var txt="<?php echo JrTexto::_('Selected or upload'); ?>";
     saveaudiotipo='sel';
	   selectedfile(e,this,txt,'_loadaudio');
	});
	function _loadaudio(){
      var obj=$('#audioadoc<?php echo $idgui ?>').attr('src');       
		  crearonda(obj,'waveform');      
	}
	var asignarDimensionModal = function($elem_tool){
        var height = $(document).outerHeight();
        var idModal = $elem_tool.parents('.modal').attr('id');
        $('#'+idModal+' #modalcontent').css({
            'max-height': (height-180)+'px',
            'overflow' : 'auto',
        });
        $('#'+idModal).css('z-index', '1049');
        $('#'+idModal).siblings('.modal-backdrop').css('z-index', '1048');
  };

  var i=0;
  $('.btngrabar').click(function(e){
    var obj=$(this);
    saveaudiotipo='rec';
    if(i==0){
      i=1;
      obj.css({"color":"red"});
      obj.children(i).removeClass('animated infinite zoomIn').addClass('animated infinite zoomIn');
      startRecording();
    }else{
      obj.attr("style", "");
      obj.children(i).removeClass('animated infinite zoomIn');
      i=0;
      var donde=obj.data('donde');
      var multiple=(obj.data('multiple')==1?true:false);
      console.log(multiple);
      stopRecording(donde,multiple);
    }
  });

  var saveaudio=function(data){
      data.idNivel = $('#actividad-header').find('select#txtNivel').val();
      data.idUnidad = $('#actividad-header').find('select#txtunidad').val();
      data.idActividad = $('#actividad-header').find('select#txtsesion').val();
      data.tool = 'A';
     var res1 = xajax__('', 'Tools', 'saveTools', data);
     if(res1){
        console.log(res1);
     }
  }

  var respuesta=function(res){ //funcion que se ejecuta despues de guardar audio blob
    var data = new Array();      
        data.texto= _sysUrlBase_+'/static/media/audio/'+res.namelink;
    saveaudio(data);     
  }

  $('.btnsave').click(function(){
    var nameaudio=$('#nameaduodoccente').val();
    var data = new Array();
    if(saveaudiotipo==='rec'){
           data.idNivel = $('#actividad-header').find('select#txtNivel').val();
           data.idUnidad = $('#actividad-header').find('select#txtunidad').val();
           data.idActividad = $('#actividad-header').find('select#txtsesion').val();
           data.namefile=nameaudio;
      if(nameaudio!=''){
        subirfile(0,data,respuesta);
      }else{
        alert('ingrese nombre a audio');
      }
    }else{
      data.texto=$('#audioadoc<?php echo $idgui ?>').attr('src');
      saveaudio(data);
    }
  });



  asignarDimensionModal( $('#record-tool') );
  var audio_context;
  var recorder;
  var recording = 0; 
 </script>
</div>