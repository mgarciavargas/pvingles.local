<?php defined('RUTA_BASE') or die(); $idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
$idTool = null;
$url='nivel='.$this->idnivel.'&unidad='.$this->idunidad.'&actividad='.$this->idactividad;
$idgameinicio='';

?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <ul class="nav nav-tabs listagames">
        <?php
        $i=1;
        if(!empty($this->datos))
            foreach ($this->datos as $game) {
                if($i==1) $idgameinicio=$game["idtool"];
                ?>
           <li class="<?php echo $i==1?'active':''; ?>" data-idgame="<?php echo $game["idtool"]; ?>">
               <a href="#"><?php echo $i++; ?></a>
           </li>                      
        <?php }
        $urlinicio=!empty($idgameinicio)?($this->documento->getUrlBase().'/game/crear/?idgame='.$idgameinicio.'&'.$url):'';
         ?>        
        <li class="btn-Add-game" style="display: block;"><a href="#tab-Practice-0" class="istooltip" title="add game"><i class="fa fa-plus-circle"></i></a></li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12" id="div_games">
        <iframe src="<?php echo $urlinicio; ?>" class="tab-content" style="border:none; width: 100%; height: 500px;"></iframe>
    </div>
</div>
<script type="text/javascript"> 
        function ejecutarli(){ 
           var ul=$('ul.listagames');
           $i=1; 
            $('li',ul).each(function(){
            if(!$(this).hasClass('btn-Add-game'))
                $(this).find('a').text($i);
                $i++;
            }) ;   
           ul.find("li:first a").trigger('click'); 
        }
    $(document).ready(function(){
        var cargargame=function(obj){
          var idGame = $(obj).attr('data-idgame');
          $('#div_games iframe.tab-content').attr('src', _sysUrlBase_+'/game/crear/?idgame='+idGame+'&<?php echo $url ?>');  
        }

        $('ul.listagames').on('click','li a',function(){
            var ul=$('ul.listagames');
            if($(this).parent().hasClass('btn-Add-game')){              
                var nli=ul.find('li').length;
                var ultimoli=ul.find('li:last');
                var newli=ultimoli.clone();
                newli.removeAttr('class');
                newli.find('a').text(nli);
                ultimoli.before(newli);
                newli.find('a').trigger('click');  
            }else{                
              $obj=$(this).closest('li');
              if($obj.hasClass('active')) return false;
              ul.find('li').removeClass('active');
              $obj.addClass('active'); 
              cargargame($obj);  
          }
        });
    });
</script>