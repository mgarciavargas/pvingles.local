<?php 
if(!empty($this->datos)){
    $rutabase=$this->documento->getUrlBase();
    $game=@str_replace('__xRUTABASEx__',$rutabase,$this->datos[0]["texto"]);
    $titulo=$this->datos[0]["titulo"];
    $descripcion=$this->datos[0]["descripcion"];
}
?>
<input type="hidden" name="hRol" id="hRol" value="Alumno">
<input type="hidden" name="idNivel" id="idNivel" value="<?php echo $this->idnivel; ?>">
<input type="hidden" name="idUnidad" id="idUnidad" value="<?php echo $this->idunidad; ?>">
<input type="hidden" name="idActividad" id="idActividad" value="<?php echo $this->idactividad; ?>">
<input type="hidden" name="hIdGame" id="hIdGame" value="<?php echo @$this->datos[0]['idtool']; ?>">
<input type="hidden" name="hRolUsuarioAct" id="hRolUsuarioAct" value="<?php echo @$this->rolUsuarioAct; ?>">

<?php /*Datos de Tarea_Archivo, si es que hubiera*/ ?>
<input type="hidden" name="hIdTareaArchivo" id="hIdTareaArchivo" value="<?php echo @$this->idTareaArchivo; ?>">
<input type="hidden" name="hIdArchivoRespuesta" id="hIdArchivoRespuesta" value="<?php echo (@$this->tarea_archivo['tablapadre']=='R')?@$this->tarea_archivo['idtarea_archivos']:''; ?>">
<input type="hidden" name="hIdTareaRespuesta" id="hIdTareaRespuesta" value="<?php echo @$this->tarea_respuesta['idtarea_respuesta']; ?>">

<style type="text/css"> #games-tool .addtext{ margin: 0; } </style>
<div class="container-fluid" id="games-tool">
    <div class="row games-container">
        <div class="col-xs-12 games-titulo-descripcion" style="display: none;">
            <div class="col-md-12 titulo text-center">
                <h3 ><?php echo $titulo; ?></h3>
            </div>
            <div class="col-md-12 descripcion text-center">
                <?php echo $descripcion; ?>                
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 games-main">
            <?php if( (empty(@$this->tarea_archivo) && !empty($game)) || 
                (@$this->tarea_archivo['tablapadre']=='T' || @$this->tarea_archivo['tablapadre']==null && !empty($game)) ){
                echo $game;
            }elseif( @$this->tarea_archivo['tablapadre']=='R' ){
                $texto = json_decode(@$this->tarea_archivo['texto'],true);
                echo str_replace('__xRUTABASEx__', $rutabase, $texto[0]['html']);
            } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
var msjes = {
    'confirm_action' : '<?php echo JrTexto::_('Confirm action');?>',
    'are_you_sure' : '<?php echo JrTexto::_('are you sure you want to delete this').' '.JrTexto::_('game'); ?>?',
    'accept' : '<?php echo JrTexto::_('Accept');?>',
    'cancel' : '<?php echo JrTexto::_('Cancel');?>',
    'guardar' : '<?php echo JrTexto::_('Save');?>',
};
var fnPreviewGame = function(){
    $('.discard-game').hide();
    $('.save-crossword').hide();
    $('.plantilla-crucigrama .nopreview').hide();
    $('.plantilla-sopaletras .nopreview').hide();
    $('.plantilla-rompecabezas .nopreview').hide();
    $('.btn.preview-game').hide();
    $('.btn.backedit-game').show();        
    $('.titulo-game').closest('.input-group').hide().closest('.titulo').find('p').html($('.titulo-game').val()).show();
    $('.descripcion-game').closest('.input-group').hide().siblings('p').html($('.descripcion-game').val()).show();
};
function imgLoaded(imgElement) {
    return imgElement.complete && imgElement.naturalHeight !== 0;
}
$(document).ready(function(){
    $('.games-titulo-descripcion').show();
    //if( $('#hRol').val()=='Alumno' ){ 
     fnPreviewGame(); 
    //}
    $('.games-main').iniciarVisor({
        fnAlIniciar: function(){
            var $tmpl = $('.games-main').find('.plantilla');
            if($tmpl.hasClass('plantilla-sopaletras')){iniciarSopaLetras($tmpl);}
            else if($tmpl.hasClass('plantilla-crucigrama')){iniciarCrucigrama($tmpl);}
            else if($tmpl.hasClass('plantilla-rompecabezas')){
                var $img = $tmpl.find('#puzzle-containment img.valvideo');
                var _img_ = document.getElementsByClassName("valvideo")[0];
                if(imgLoaded(_img_)){
                    iniciarRompecabezas($tmpl);
                }else{
                    $img.load(function() { iniciarRompecabezas($tmpl); });
                }
            }
        },
        texto:msjes,
        btnGuardar: ($('#hRol').val()=='Alumno')?'.btn_guardar_juego':false,
    });
    mostrarGame(true);
});
</script>