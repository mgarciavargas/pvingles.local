<?php defined('RUTA_BASE') or die(); $idgui = uniqid(); 
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
if(!empty($this->datos[0])){
    $pdf=@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$this->datos[0]["texto"]);
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/css/tool_workbook.css">
<div class="container-fluid" id="workbook-tool">
    <div class="row workbook-container">
        <?php if($rolActivo!='Alumno'){?>
        <div class="col-md-12 col-sm-12 col-xs-12 nopreview">
            <a class="btnselectedfile btn btn-primary" data-tipo="pdf" data-url="iframe.pdf-viewer">
                <i class="fa fa-file-pdf-o"></i> 
                <?php echo JrTexto::_('select') ?> PDF
            </a>
            <a class="btndiscardpdf btn btn-danger pull-right disabled" data-idtool="">
                <i class="fa fa-trash-o"></i>
                <?php echo JrTexto::_('discard') ?> PDF
            </a>
        </div>
        <?php } ?>
        <div class="col-md-12 col-sm-12 col-xs-12 workbook-main">
            <iframe src="<?php echo @$pdf; ?>" frameborder="0" class="pdf-viewer"></iframe>            
            <div class="mask">
                <img src="<?php echo $this->documento->getUrlStatic() ?>/media/web/pdf_file.jpg" class="img-responsive center-block <?php echo !empty($pdf)?'hidden':''; ?>">
                <span class="<?php echo !empty($pdf)?'hidden':''; ?>"><?php echo JrTexto::_('there is no file to display') ?></span>
            </div>
        </div>
    </div>
</div>
<?php if($rolActivo!='Alumno'){?>
<script type="text/javascript">
    <?php if(empty($pdf)){ ?>
    $('.pdf-viewer').hide();
    <?php } ?>

    var asignarDimensionModal = function($elem_tool){
        var height = $(document).outerHeight();
        var idModal = $elem_tool.parents('.modal').attr('id');
        $('#'+idModal+' #modalcontent').css({
            'max-height': (height-180)+'px',
            'overflow' : 'auto',
        });

        $('#'+idModal).css('z-index', '1049');
        $('#'+idModal).siblings('.modal-backdrop').css('z-index', '1048');
    };
    var guardarworkbook = function( ruta ){
        var data = new Array();
        data.idNivel = $('#actividad-header').find('#txtNivel').val();
        data.idUnidad = $('#actividad-header').find('#txtunidad').val();
        data.idActividad = $('#actividad-header').find('#txtsesion').val();
        data.texto= ruta;
        data.tool = 'P';
        var res = xajax__('', 'Tools', 'saveTools', data);
        if(res){
           $('.btndiscardpdf').attr('data-idtool',res);
        }
    };

    $('.btnselectedfile').click(function(e){
        var txt="<?php echo JrTexto::_('Selected or upload'); ?> ";
        selectedfile(e,this,txt);
    });

    $('iframe.pdf-viewer').load(function(){
        var iframe_src = $(this).attr('src').toString();
        if(iframe_src!=''){
            $('.btndiscardpdf').removeClass('disabled');
            guardarworkbook(iframe_src);
        }       
    });

    $('.btndiscardpdf').click(function(e) {
        e.preventDefault();
        if( !$(this).hasClass('disabled') ){
            var $iframe = $('iframe.pdf-viewer');
            var iframe_src = $iframe.attr('src');
            if(iframe_src!=''){
                $iframe.removeAttr('src');
                $iframe.attr('src','');
                $iframe.hide();
                $iframe.siblings().show('fast');
                $(this).addClass('disabled');
                xajax__('', 'Tools', 'eliminar', $('.btndiscardpdf').data('idtool'));
                $('.btndiscardpdf').addClass('disabled');
            }
        }
    });

    $('.preview').click(function(e) {
        previewworkbook();
    });

    $('.btnsaveActividad').click(function(e){
        previewworkbook();
    });

    asignarDimensionModal( $('#workbook-tool') );
</script>
<?php }?>