<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php $index=0; foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link']) && (count($this->breadcrumb)-1)!=$index ){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        $index++;
        } ?>
    </ol>
</div> </div>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/css/tool_vocabulary.css">
<style type="text/css">
    .pnlvocabulario .textoarea {
        height: auto !important;
    }
    .pnlvocabulario .imgportada {
        pointer-events: none;
    }
    .pnlvocabulario .btnremovevoca {
        display: none !important;
    }
</style>

<div class="row" id="visor-vocabulario">

    <div class="col-xs-12" id="filtros-niveles">
        <div class="col-xs-3 select-ctrl-wrapper select-azul">
            <select name="opcIdCurso" id="opcIdCurso" class="form-control select-ctrl select-nivel">
                <option value="0">- <?php echo ucfirst(JrTexto::_("Select course")); ?> -</option>
                <?php if(!empty($this->cursos_nivel)){
                foreach ($this->cursos_nivel  as $c) {
                    echo '<option value="'.$c["idcurso"].'">'.$c["nombre"].'</option>';
                }} ?>
            </select>
        </div>
    </div>

    <div class="col-xs-12 ">
        <div class="col-xs-12 " id="zona-vocabulario" style="background: #fff;"></div>
    </div>

    <div class="col-xs-12" id="empty_data">
        <div class="jumbotron">
            <h2><?php echo ucfirst(JrTexto::_("There are no vocabulary to display")) ?>.</h2>
          <p><?php echo ucfirst(JrTexto::_("Please").', '.JrTexto::_("select a course or a level to show vocabulary")) ?>.</p>
          <p><a class="btn btn-default" href="<?php echo $this->documento->getUrlBase(); ?>" role="button"><i class="fa fa-arrow-left"></i> <?php echo ucfirst(JrTexto::_("Back home")) ?></a></p>
      </div>
    </div>

</div>


<section class="hidden">
    <div class="col-xs-3 select-ctrl-wrapper select-azul hidden" id="clonar-select-nivel">
        <select name="opcIdCursoDetalle" id="opcIdCursoDetalle" class="form-control select-ctrl select-nivel">
            <option value="-1">- <?php echo ucfirst(JrTexto::_("Select")); ?> -</option>
        </select>
    </div>
</section>

<script type="text/javascript">
var cargarVocabulario = false;

var getCursoDetalle = function( dataSend , $select ) {
    $.ajax({
        async: false,
        url: _sysUrlBase_+'/acad_cursodetalle/buscarjson',
        type: 'GET',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp) {
        if(resp.code=='ok') {
            cargarVocabulario= false;
            var options = '';
            if(resp.data.length) {
                $.each(resp.data, function(i, det) {
                    if(det.tiporecurso!="E") {
                        options += '<option value="'+det.idcursodetalle+'">'+det.nombre+'</option>';
                    }
                });
                $select.append(options);
                $select.closest('.select-ctrl-wrapper').removeClass('hidden');
            }else{
                cargarVocabulario = true;
                $select.closest('.select-ctrl-wrapper').remove();
            }
        } else {
            mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
            $select.closest('.select-ctrl-wrapper').remove();
        }
    }).fail(function(err) {
        $select.closest('.select-ctrl-wrapper').remove();
        mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
    }).always(function() {  });
};

var getVocabulario = function( dataSend ) {
    $.ajax({
        url: _sysUrlBase_+'/vocabulario/jxBuscarXCursoDet',
        type: 'POST',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp) {
        if(resp.code=="ok" && resp.data!=null) {
            let html_voc = resp.data.texto.replace(/__xRUTABASEx__/gi, _sysUrlBase_);
            $('#zona-vocabulario').html(html_voc);
            $('#zona-vocabulario .cargarimagenall').removeClass('cargarimagenall');
            $('#empty_data').hide();
        } else {
            //mostrar_notificacion('<?php echo JrTexto::_("Error") ?>', resp.msj, 'error');
            $('#empty_data').show();
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log("error", err);
    }).always(function() {});
};

var borrarSobrantes = function($select) {
    var $listSelect = $('#filtros-niveles select.select-nivel');
    var indexOfSelect = $listSelect.index($select);
    var cantSelect = $listSelect.length;
    x=0;
    while($('#filtros-niveles select.select-nivel').eq(indexOfSelect+1).length>=1){
        $('#filtros-niveles select.select-nivel').eq(indexOfSelect+1).closest('.select-ctrl-wrapper').remove();
        if(x>=100){ console.log('algo salio mal'); break; }
        x++;
    }
};

var limpiarVisorVocabulario = function() {
    var $panel = $('#zona-vocabulario');
    $panel.html('');
};


var nuevoSelectFiltro = function( idPadre ) {
    var $divSelect_new = $('#clonar-select-nivel').clone();
    var idSelect = $divSelect_new.find('select.select-nivel').attr('id');
    var nuevoIdSelect = idSelect+'_'+idPadre;
    $divSelect_new.removeAttr('id');
    $divSelect_new.find('select.select-nivel').attr({
        'id': nuevoIdSelect,
        'name': nuevoIdSelect
    });
    $('#filtros-niveles').append($divSelect_new);
    return $('#filtros-niveles #'+nuevoIdSelect);
};

$(document).ready(function() {
    $('#filtros-niveles').on('change', '.select-nivel', function(e) {
        var idPadre = 0;
        var idCurso = $('#opcIdCurso').val() || 0;
        if( $(this).attr('id') !== "opcIdCurso" ) {
            idPadre = $(this).val();
        }
        borrarSobrantes($(this));
        limpiarVisorVocabulario();
        $('#empty_data').show();
        if(idCurso==0){ return false; }

        $select_new = nuevoSelectFiltro(idPadre);
        getCursoDetalle({
            'idcurso': idCurso,
            'idpadre': idPadre,
        }, $select_new);

        if(cargarVocabulario) {
            getVocabulario({
                'idcurso': idCurso,
                'iddetalle': idPadre,
            });
        }
    });
});
</script>