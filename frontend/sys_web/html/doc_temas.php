<div class="row pnlman">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<div class="panel panel-danger">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo JrTexto::_("Platform Language"); ?></h3>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				</div>
				<div class="panel-body">
					<?php $idioma=$this->documento->getIdioma(); ?>
					<table class="table table-responsive table-striped tableidioma">
						<tr><td><?php echo JrTexto::_("Spanish"); ?></td>
						<td><a href="javascript:;"  class="changeidioma2" idioma="ES"> <i class="idioma fa fa<?php echo $idioma=='ES'?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $idioma=='ES'?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a></td></tr>
						<tr><td><?php echo JrTexto::_("English"); ?></td>
						<td><a href="javascript:;"  class="changeidioma2" idioma="EN"> <i class="idioma fa fa<?php echo $idioma=='EN'?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $idioma=='EN'?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a></td></tr>
					</table>
				</div>
			</div> 
		</div>    	
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.changeidioma2').click(function(){
		idi=$(this).attr('idioma');
		localStorage.setItem("sysidioma", idi);
		xajax__('', 'idioma', 'cambiaridioma', idi);
		var iobj=$(this).closest('tableidioma').find('.idioma');
		$(iobj).removeClass('fa-check-circle-o').addClass('fa-circle-o');
		$('i.idioma',this).removeClass('fa-circle-o').addClass('fa-check-circle-o');
	});
});	
</script>