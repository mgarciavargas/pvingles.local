<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Acad_marcacion'">&nbsp;<?php echo JrTexto::_('Acad_marcacion'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdmarcacion" id="pkidmarcacion" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdgrupodetalle">
              <?php echo JrTexto::_('Idgrupodetalle');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <input type="number" id="txtIdgrupodetalle" name="txtIdgrupodetalle" step="1" value="<?php echo !empty($frm["idgrupodetalle"])?$frm["idgrupodetalle"]:1;?>"  min="1" class="form-control col-md-7 col-xs-12" required />
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdcurso">
              <?php echo JrTexto::_('Idcurso');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <input type="number" id="txtIdcurso" name="txtIdcurso" step="1" value="<?php echo !empty($frm["idcurso"])?$frm["idcurso"]:1;?>"  min="1" class="form-control col-md-7 col-xs-12" required />
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdhorario">
              <?php echo JrTexto::_('Idhorario');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <input type="number" id="txtIdhorario" name="txtIdhorario" step="1" value="<?php echo !empty($frm["idhorario"])?$frm["idhorario"]:1;?>"  min="1" class="form-control col-md-7 col-xs-12" required />
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFecha">
              <?php echo JrTexto::_('Fecha');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input name="txtFecha" id="txtFecha" class="verdate form-control col-md-7 col-xs-12" required="required" type="date" value="<?php echo @$frm["fecha"];?>">   
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtHoraingreso">
              <?php echo JrTexto::_('Horaingreso');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: tipo2_horaingreso in D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php on line <i>112</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>268872</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\wamp64\www\pvingles.local\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3033864</td><td bgcolor='#eeeeec'>xajax->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>19</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>3</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3034328</td><td bgcolor='#eeeeec'>xajaxPluginManager->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>462</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>4</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3035472</td><td bgcolor='#eeeeec'>xajaxFunctionPlugin->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>54</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>5</td><td bgcolor='#eeeeec' align='center'>0.0279</td><td bgcolor='#eeeeec' align='right'>3036280</td><td bgcolor='#eeeeec'>xajaxUserFunction->call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1043</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>6</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3073096</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.call-user-func-array:{D:\wamp64\www\pvingles.local\sys-lib\xajax\xajax-core\xajaxAIO.inc.php:1107}' target='_new'>call_user_func_array:{D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php:1107}</a>
(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1107</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>7</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3073864</td><td bgcolor='#eeeeec'>WebAdminxAjax::call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1107</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>8</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3076520</td><td bgcolor='#eeeeec'>WebAdminxAjax::recurso(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>23</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>9</td><td bgcolor='#eeeeec' align='center'>0.0293</td><td bgcolor='#eeeeec' align='right'>3188416</td><td bgcolor='#eeeeec'>WebGenerador->xsetGenerarPagina(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>63</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>10</td><td bgcolor='#eeeeec' align='center'>0.6627</td><td bgcolor='#eeeeec' align='right'>3204664</td><td bgcolor='#eeeeec'>JrWeb->getEsquema(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\sys_web\cls.WebGenerador.php' bgcolor='#eeeeec'>...\cls.WebGenerador.php<b>:</b>141</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>11</td><td bgcolor='#eeeeec' align='center'>0.9764</td><td bgcolor='#eeeeec' align='right'>3374464</td><td bgcolor='#eeeeec'>require_once( <font color='#00bb00'>'D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php'</font> )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\jrAdwen\cls.JrWeb.php' bgcolor='#eeeeec'>...\cls.JrWeb.php<b>:</b>34</td></tr>
</table></font>
<br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: tipo2_horaingreso in D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php on line <i>114</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>268872</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\wamp64\www\pvingles.local\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3033864</td><td bgcolor='#eeeeec'>xajax->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>19</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>3</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3034328</td><td bgcolor='#eeeeec'>xajaxPluginManager->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>462</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>4</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3035472</td><td bgcolor='#eeeeec'>xajaxFunctionPlugin->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>54</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>5</td><td bgcolor='#eeeeec' align='center'>0.0279</td><td bgcolor='#eeeeec' align='right'>3036280</td><td bgcolor='#eeeeec'>xajaxUserFunction->call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1043</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>6</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3073096</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.call-user-func-array:{D:\wamp64\www\pvingles.local\sys-lib\xajax\xajax-core\xajaxAIO.inc.php:1107}' target='_new'>call_user_func_array:{D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php:1107}</a>
(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1107</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>7</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3073864</td><td bgcolor='#eeeeec'>WebAdminxAjax::call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1107</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>8</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3076520</td><td bgcolor='#eeeeec'>WebAdminxAjax::recurso(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>23</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>9</td><td bgcolor='#eeeeec' align='center'>0.0293</td><td bgcolor='#eeeeec' align='right'>3188416</td><td bgcolor='#eeeeec'>WebGenerador->xsetGenerarPagina(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>63</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>10</td><td bgcolor='#eeeeec' align='center'>0.6627</td><td bgcolor='#eeeeec' align='right'>3204664</td><td bgcolor='#eeeeec'>JrWeb->getEsquema(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\sys_web\cls.WebGenerador.php' bgcolor='#eeeeec'>...\cls.WebGenerador.php<b>:</b>141</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>11</td><td bgcolor='#eeeeec' align='center'>0.9764</td><td bgcolor='#eeeeec' align='right'>3374464</td><td bgcolor='#eeeeec'>require_once( <font color='#00bb00'>'D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php'</font> )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\jrAdwen\cls.JrWeb.php' bgcolor='#eeeeec'>...\cls.JrWeb.php<b>:</b>34</td></tr>
</table></font>
                    
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtHorasalida">
              <?php echo JrTexto::_('Horasalida');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: tipo2_horasalida in D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php on line <i>112</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>268872</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\wamp64\www\pvingles.local\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3033864</td><td bgcolor='#eeeeec'>xajax->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>19</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>3</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3034328</td><td bgcolor='#eeeeec'>xajaxPluginManager->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>462</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>4</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3035472</td><td bgcolor='#eeeeec'>xajaxFunctionPlugin->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>54</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>5</td><td bgcolor='#eeeeec' align='center'>0.0279</td><td bgcolor='#eeeeec' align='right'>3036280</td><td bgcolor='#eeeeec'>xajaxUserFunction->call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1043</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>6</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3073096</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.call-user-func-array:{D:\wamp64\www\pvingles.local\sys-lib\xajax\xajax-core\xajaxAIO.inc.php:1107}' target='_new'>call_user_func_array:{D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php:1107}</a>
(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1107</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>7</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3073864</td><td bgcolor='#eeeeec'>WebAdminxAjax::call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1107</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>8</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3076520</td><td bgcolor='#eeeeec'>WebAdminxAjax::recurso(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>23</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>9</td><td bgcolor='#eeeeec' align='center'>0.0293</td><td bgcolor='#eeeeec' align='right'>3188416</td><td bgcolor='#eeeeec'>WebGenerador->xsetGenerarPagina(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>63</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>10</td><td bgcolor='#eeeeec' align='center'>0.6627</td><td bgcolor='#eeeeec' align='right'>3204664</td><td bgcolor='#eeeeec'>JrWeb->getEsquema(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\sys_web\cls.WebGenerador.php' bgcolor='#eeeeec'>...\cls.WebGenerador.php<b>:</b>141</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>11</td><td bgcolor='#eeeeec' align='center'>0.9764</td><td bgcolor='#eeeeec' align='right'>3374464</td><td bgcolor='#eeeeec'>require_once( <font color='#00bb00'>'D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php'</font> )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\jrAdwen\cls.JrWeb.php' bgcolor='#eeeeec'>...\cls.JrWeb.php<b>:</b>34</td></tr>
</table></font>
<br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: tipo2_horasalida in D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php on line <i>114</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>268872</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\wamp64\www\pvingles.local\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3033864</td><td bgcolor='#eeeeec'>xajax->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>19</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>3</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3034328</td><td bgcolor='#eeeeec'>xajaxPluginManager->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>462</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>4</td><td bgcolor='#eeeeec' align='center'>0.0261</td><td bgcolor='#eeeeec' align='right'>3035472</td><td bgcolor='#eeeeec'>xajaxFunctionPlugin->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>54</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>5</td><td bgcolor='#eeeeec' align='center'>0.0279</td><td bgcolor='#eeeeec' align='right'>3036280</td><td bgcolor='#eeeeec'>xajaxUserFunction->call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1043</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>6</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3073096</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.call-user-func-array:{D:\wamp64\www\pvingles.local\sys-lib\xajax\xajax-core\xajaxAIO.inc.php:1107}' target='_new'>call_user_func_array:{D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php:1107}</a>
(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1107</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>7</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3073864</td><td bgcolor='#eeeeec'>WebAdminxAjax::call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1107</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>8</td><td bgcolor='#eeeeec' align='center'>0.0283</td><td bgcolor='#eeeeec' align='right'>3076520</td><td bgcolor='#eeeeec'>WebAdminxAjax::recurso(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>23</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>9</td><td bgcolor='#eeeeec' align='center'>0.0293</td><td bgcolor='#eeeeec' align='right'>3188416</td><td bgcolor='#eeeeec'>WebGenerador->xsetGenerarPagina(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>63</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>10</td><td bgcolor='#eeeeec' align='center'>0.6627</td><td bgcolor='#eeeeec' align='right'>3204664</td><td bgcolor='#eeeeec'>JrWeb->getEsquema(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\sys_web\cls.WebGenerador.php' bgcolor='#eeeeec'>...\cls.WebGenerador.php<b>:</b>141</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>11</td><td bgcolor='#eeeeec' align='center'>0.9764</td><td bgcolor='#eeeeec' align='right'>3374464</td><td bgcolor='#eeeeec'>require_once( <font color='#00bb00'>'D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php'</font> )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\jrAdwen\cls.JrWeb.php' bgcolor='#eeeeec'>...\cls.JrWeb.php<b>:</b>34</td></tr>
</table></font>
                    
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIddocente">
              <?php echo JrTexto::_('Iddocente');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <input type="number" id="txtIddocente" name="txtIddocente" step="1" value="<?php echo !empty($frm["iddocente"])?$frm["iddocente"]:1;?>"  min="1" class="form-control col-md-7 col-xs-12" required />
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNalumnos">
              <?php echo JrTexto::_('Nalumnos');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNalumnos" name="txtNalumnos" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nalumnos"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtObservacion">
              <?php echo JrTexto::_('Observacion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtObservacion" name="txtObservacion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["observacion"];?>">
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveAcad_marcacion" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('acad_marcacion'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
  
  $('#txtfecha').datetimepicker({
    lang:'es',
    timepicker:false,
    format:'Y/m/d'
  });          
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'acad_marcacion', 'saveAcad_marcacion', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Acad_marcacion"))?>');
      }
     }
  });

 
  
});


</script>

