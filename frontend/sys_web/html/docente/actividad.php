<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
$actividades=!empty($this->actividades)?$this->actividades:null; 
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
if(!empty($this->datos)) $frm=$this->datos;
?>
<style type="text/css">
    .addtext{
        border-bottom: 1px solid rgba(62, 168, 239, 0.93);
        padding: 1ex 0;
        line-height: 25px
    }
    .addtext>input{
        margin:  1em 0;
        padding: 0 7px;
    }
    #panel-tiempo .form-control{
        display: inline-block;
        max-width: 80px; 
        text-align: center;  
        padding: 0ex;  
        margin: 0px;
        height: 23px;
    }
    #contenedor-paneles .eje-panelinfo{
        margin-left: 0.55ex;
        text-align: center;
        min-width: 80px;
        float: left;
    }
    #contenedor-paneles .eje-panelinfo:first-child{margin-left: 0;}
    #contenedor-paneles .eje-panelinfo .title{
        margin: 0px;
        color: #fff;
        font-size: 
    }
    #contenedor-paneles .eje-panelinfo.active{
        margin: 0px;
        display: block;
    }
    #contenedor-paneles .eje-panelinfo.inactive{
        margin: 0px;
        display: none;
    }
    #contenedor-paneles #panel-barraprogreso{
        width: 75%;
    }
    
    #contenedor-paneles #panel-barraprogreso .progress{
        background: rgba(199, 195, 189, 0.62);
    }
    
    .btn{
        font-size: 15px;
    }  
    
    @media (min-width: 1250px) and (max-height: 800px){  /* para laptops*/
        .widget-box{
            margin: 0px;
        }
        .widget-main{
            padding: 0px;
        }
        hr{
            margin: 5px;
        }
        .btn-container{
            padding: 5px !important;
        }
    }
</style>
<div id="msj-interno"></div>
<form method="post" id="frm-<?php echo $idgui;?>" class="form-horizontal form-label-left editable" onsubmit="return false;" data-iduser="<?php echo $usuarioAct["dni"] ?>" data-user="<?php echo $usuarioAct["nombre_full"]  ?>" >
<div class="container" data-rol="<?php echo $rolActivo; ?>"> <div class="page-content">
    <div class="row" id="actividad"> <div class="col-xs-12">
        <div class="row" id="actividad-header">
            <input type="hidden" name="txtNivel" id="txtNivel" value="<?php echo $this->idnivel; ?>" >
            <input type="hidden" name="txtunidad" id="txtunidad" value="<?php echo $this->idunidad; ?>" >
            <input type="hidden" name="txtSesion" id="txtsesion" value="<?php echo $this->idsesion; ?>" >
            <div class="col-md-12">
                <ol class="breadcrumb">
                  <li><a href="<?php echo $this->documento->getUrlSitio(); ?>"><?php echo JrTexto::_("Home")?></a></li>
                  <li><a href="<?php echo $this->documento->getUrlSitio(); ?>/recursos/listar/<?php echo $this->idnivel."/".$this->idunidad."/".$this->idsesion; ?>"><?php echo JrTexto::_("Activities")?></a></li>
                  <li><a href="#"><?php echo $this->actividad["nombre"] ?></a></li>
                  <li class="active"><?php echo JrTexto::_("Edit");?></li>
                </ol>
            </div>
        </div>

        <div class="row" id="actividad-body">
            <div class="col-xs-12 col-sm-9">
                <ul class="nav nav-pills  metodologias">
                    <?php 
                    $imet=0;
                    $metcode=null;
                    if(!empty($this->metodologias))
                    foreach ($this->metodologias as $met) { $imet++;?>
                        <li class="<?php echo $imet==1?'active':'';?>">
                        <a href="#met-<?php echo $met["idmetodologia"] ?>" data-toggle="pill"><?php echo JrTexto::_(trim($met["nombre"])); ?></a>
                        </li>
                    <?php $metcode.=$met["idmetodologia"].'|'; } ?>
                    <input name="metodologias" type="hidden" value="<?php echo substr($metcode,0,-1); ?>" >
                </ul>
                <div class="actividad-main-content tab-content">
                    <div id="contenedor-paneles">
                        <div id="panel-barraprogreso" class="text-center eje-panelinfo" style="display: none;">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                    <span>0</span>%
                                </div>
                            </div>
                        </div>
                        <div id="panel-changedplantilla" class="eje-panelinfo  text-center" >
                            <div class="btnchoiceplantilla nopreview" data-tpl=""> 
                                <span class="btn btn-primary"><?php echo ucfirst(JrTexto::_('choose')).' '.JrTexto::_('template')?></span>
                            </div>
                        </div>
                        <div id="panel-ejericiosinfo" class="eje-panelinfo text-center" style="display: none;">
                            <span class="btn btn-sm  btn-primary eje-btnanterior"><i class="fa fa-arrow-left"></i></span>
                            <span class="actual" style="padding: 0.5ex 1ex; margin: 0px;">1/3</span>
                            <span class="btn  btn-sm btn-primary eje-btnsiguiente"><i class="fa fa-arrow-right"></i></span>                           
                        </div>
                        <div id="panel-tiempo" class="eje-panelinfo pull-right" style="display: none;">
                            <div class="titulo btn-yellow"><?php echo ucfirst(JrTexto::_('time')); ?></div>
                            <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50">
                            <div class="info-show showonpreview" style="display: none;">00</div>
                        </div>
                        <div id="panel-intentos-dby" class="eje-panelinfo text-center pull-right" style="display: none;">
                            <div class="titulo btn-blue"><?php echo ucfirst(JrTexto::_('attempt')); ?></div>
                            <span class="actual">1</span>
                            <span class="total"><?php echo $this->intentos; ?></span>
                        </div> 
                    </div>
                    <?php 
                    $imet=0;
                    if(!empty($this->metodologias))
                    foreach ($this->metodologias as $met) { $imet++;?>                                
                    <div id="met-<?php echo $met["idmetodologia"] ?>" class="metod tab-pane fade <?php echo $imet==1?'active in':'';?>" data-idmet="<?php echo $met["idmetodologia"]; ?>">
                        <?php if($imet!=1){?>
                        <a class="btn btn-blue btnordenarEjericios nopreview" href="#" title="<?php echo JrTexto::_('Order Exercices'); ?>"><i class="fa fa-sort-amount-asc"></i></a>
                        <?php } ?>

                        <div class="row">                   
                        <?php 
                        $tpl=$this->oNegActividad->tplxMetodologia($met["idmetodologia"]);
                        if(!empty($tpl))
                            if($tpl["padre"]==false){
                                if(!empty($this->actividades[$met["idmetodologia"]]["act"]["det"][0]["texto_edit"])){
                                    $Ejericios=$this->actividades[$met["idmetodologia"]]["act"]["det"][0];
                                    $texto_edit=$Ejericios["texto_edit"];
                                    $iddetalle=$Ejericios["iddetalle"];
                                }  ?>                            
                                    <div class="Ejercicio"><br>
                                    <div class="textosave"  data-texto="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>" data-iddetalle="<?php echo @$iddetalle; ?>">
                                        <?php if(!empty($texto_edit)){
                                            echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$texto_edit);
                                        }else{?>
                                        <div class="col-xs-12">
                                            <div class="tab-title-zone pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;"> 
                                                <div class="col-md-12">
                                                    <h3 class="addtext addtexttitle<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('title') ?></h3>
                                                    <input type="hidden" class="valin nopreview"  name="titulo[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtexttitle<?php echo $met["idmetodologia"]; ?>">
                                                </div>
                                                <div class="space-line pw1_<?php echo $met["idmetodologia"] ?>"></div>
                                            </div>
                                            <div class="tab-description pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;"> 
                                                <div class="col-md-12">
                                                    <p class="addtext addtextdescription<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('description') ?></p>
                                                    <input type="hidden" class="valin nopreview"  name="descripcion[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtextdescription<?php echo $met["idmetodologia"]; ?>" >
                                                </div>
                                            </div>
                                            <div class="aquicargaplantilla"  id="tmppt_<?php echo $met["idmetodologia"] ?>"></div>
                                            <input type="hidden" name="habilidades[<?php echo $met["idmetodologia"] ?>][1]" class="selected-skills" id="habilidades_met-<?php echo $met["idmetodologia"] ?>_1" value="0">
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>" class="hidden"></textarea>
                                    <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_edit" class="hidden"></textarea>
                                    </div>
                            <?php }else{ 
                                $texto_edit=null;
                                $ntexto_edit=0;
                                if(!empty($this->actividades[$met["idmetodologia"]]["act"]["det"])){
                                    $texto_edit=$this->actividades[$met["idmetodologia"]]["act"]["det"];
                                    $ntexto_edit=count($texto_edit);
                                }
                                ?>
                            <div class="col-xs-12">
                                <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', $met["nombre"]) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', $met["nombre"]) ?>" data-id-metod="<?php echo $met["idmetodologia"]; ?>">
                                    <?php if(!empty($ntexto_edit))
                                    for ($im=1 ;$im<=$ntexto_edit;$im++){?>
                                         <li class="<?php echo $im==1?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]).'-'.$im; ?>" data-toggle="tab"><?php echo $im; ?></a></li>
                                     <?php }else{ ?>
                                    <li class="active"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1" data-toggle="tab">1</a></li>
                                    <?php } ?>
                                    <li class="btn-Add-Tab nopreview"><a href="#tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-0" class="istooltip" title="<?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('tab') ?>"><i class="fa fa-plus-circle"></i></a></li>
                                </ul>
                            </div>

                            <div class="col-xs-12">
                                <div class="tab-content" id="<?php echo str_replace(' ', '_', $met["nombre"]) ?>-main-content">
                                   <?php if(!empty($texto_edit)){
                                        $im=0;
                                        foreach ($texto_edit as $tedit){ $im++; $iddetalle=$tedit["iddetalle"];?>
                                        <div id="tab-<?php echo str_replace(' ', '_', $met["nombre"])."-".$im; ?>" class="tabhijo tab-pane fade <?php echo $im==1?'active':''; ?> in"  data-iddetalle="<?php echo  @$iddetalle; ?>">
                                           <div class="textosave" data-texto="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im;  ?>" data-iddetalle="<?php echo  @$iddetalle; ?>">
                                           <?php echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$tedit["texto_edit"]);?>
                                           </div>
                                           <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][<?php echo $im;?>]; " id="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im; ?>" class="hidden"></textarea>
                                           <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][<?php echo $im;?>]; " id="text_<?php echo $idgui."_".$met["idmetodologia"]."_".$im; ;  ?>_edit" class="hidden"></textarea>
                                        </div>
                                       <?php }
                                       }else{ ?>
                                    <div id="tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1" class="tabhijo tab-pane fade active in " >
                                        <div class="textosave" data-texto="text_<?php echo $idgui."_".$met["idmetodologia"]."_1";  ?>" data-iddetalle="">
                                        <div class="row"> 
                                          <div class="col-xs-12">
                                            <div class="nopreview" style="position: absolute; right: 1.5em; z-index:10">
                                                <div class="btn-close-tab istooltip" title="<?php echo JrTexto::_('remove').' '.JrTexto::_('tab') ?>" data-id-tab="tab-<?php echo str_replace(' ', '_', $met["nombre"]) ?>-1">
                                                    <button type="button" class="close"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                            </div>
                                            <div class="tab-title-zone pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;">
                                                <div class="col-md-12">
                                                    <h3 class="addtext addtexttitulo<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('title') ?></h3>
                                                    <input type="hidden" class="valin nopreview"  name="titulo[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtexttitulo<?php echo $met["idmetodologia"]; ?>">
                                                </div>
                                                <div class="space-line pw1_<?php echo $met["idmetodologia"] ?>"></div>
                                            </div>
                                            <div class="tab-description pw1_<?php echo $met["idmetodologia"] ?>" style="display:none;"> 
                                                <div class="col-md-12">
                                                    <p class="addtext addtextdescription<?php echo $met["idmetodologia"]; ?>"><?php echo JrTexto::_('click here to') ?> <?php echo JrTexto::_('add') ?> <?php echo JrTexto::_('description') ?></p>
                                                    <input type="hidden" class="valin nopreview"  name="descripcion[<?php echo $met["idmetodologia"] ?>][1]" data-nopreview=".addtextdescription<?php echo $met["idmetodologia"]; ?>" >
                                                </div>
                                            </div>
                                            <div class="aquicargaplantilla" id="tmppt_<?php echo $met["idmetodologia"] ?>_1"></div>
                                            <input type="hidden" name="habilidades[<?php echo $met["idmetodologia"] ?>][1]" class="selected-skills" id="habilidades_met-<?php echo $met["idmetodologia"] ?>_1" value="0">
                                          </div>
                                        </div>
                                        </div>
                                        <textarea name="det_texto[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_1" class="hidden"></textarea>
                                        <textarea name="det_texto_edit[<?php echo $met["idmetodologia"] ?>][1] " id="text_<?php echo $idgui."_".$met["idmetodologia"];  ?>_1_edit" class="hidden"></textarea>
                                    </div>
                                    
                                    <?php } ?>
                                </div>
                            </div>
                          <?php } ?>
                        </div>                      
                    </div>

                    <?php } ?>

                    <div class="text-right" id="btns-guardar-actividad" style="display: none;">
                        <a href="#" class="btn btn-info preview pull-left"> <?php echo JrTexto::_('Preview');?></a>
                        <a href="#" class="btn btn-default back pull-left" style="display: none; position: absolute; bottom: 3ex; left: 3ex; "> <?php echo JrTexto::_('Exit preview');?></a>
                        <a href="#" class="btn btn-success btnsaveActividad continue"> <?php echo JrTexto::_('Save and add exercise');?></a>
                        <a href="#" class="btn btn-success btnsaveActividad"> <?php echo JrTexto::_('Save and finish');?></a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3">
                <div class="panel panel-info" style="width: 100%; padding: 0.5ex;">
                    <div class="panel-heading text-center autor"><b><?php echo JrTexto::_('Author'); ?> : </b> <span id="infoautor"></span></div>
                </div>
                <div class="widget-box">
                    <div class="widget-header bg-red">
                        <h4 class="widget-title">
                            <i class="fa fa-paint-brush"></i>
                            <span><?php echo JrTexto::_('Skills'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row habilidades edicion">
                                <div class="col-xs-12 ">
                                <?php 
                                $ihab=0;
                                if(!empty($this->habilidades))
                                foreach ($this->habilidades as $hab) { $ihab++;?>
                                    <div class="col-xs-12 col-md-6 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                <?php } ?>                                  
                                </div>
                            </div> 
                        </div>
                    </div>                   
                </div>

                <div class="widget-box" id="info-avance-alumno">
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row"  style="margin-bottom: 8px; ">
                                <!--div class="col-md-6 btn-container">
                                    <a href="<?php echo $this->documento->getUrlBase()?>/recursos/ver/<?php echo $this->idnivel; ?>/<?php echo $this->idunidad; ?>/<?php echo $this->idsesion; ?>/?iddoc=00000000" target="_blank" class="btn btn-yellow  btn-rectangle vertical-center " data-ventana="teacherresources"  style="margin: 0.3ex; min-height: 60px;">
                                        <span class="btn-label" style="font-size: 13px;"><?php //echo JrTexto::_('Teaching<br>resources');?></span>
                                        <i class="btn-icon fa fa-male" style="font-size: 1.8em;"></i>
                                    </a>
                                </div-->
                                <div class="col-xs-6 btn-container ">
                                    <!--data-source="<?php //echo $this->documento->getUrlSitio() ?>/sidebar_pages/diccionario"--> 
                                    <a href="#" class="btn btn-blue btn-rectangle btnvermodal slide-sidebar-right" data-ventana="vocabulary" style="margin: 0.3ex; min-height: 60px;">
                                        <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('Vocabulary');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-file-text-o" style="font-size: 1.8em;"></i>
                                    </a>
                                </div>
                                <div class="col-xs-6 btn-container ">
                                    <a href="#" class="btn btn-lilac btn-rectangle  slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/pronunciacion" data-ventana="vocabulary" style="margin: 0.3ex; min-height: 60px;">
                                        <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('Pronunciation');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-headphones" style="font-size: 1.8em;"></i>
                                    </a>
                                </div>
                                <!--iv class="col-md-6 btn-container">
                                    <a href="#" class="btn btn-pink btn-rectangle btn-inactive vertical-center" data-ventana="speakinglabs"  style="margin: 0.3ex; min-height: 60px;">
                                        <span class="btn-label" style="font-size: 13px;"><?php //echo JrTexto::_('Recording');?></span>
                                        <i class="btn-icon fa fa-microphone" style="font-size: 1.8em;"></i>
                                    </a>
                                </div-->
                                <div class="col-md-6 btn-container">
                                    <a href="#" class="btn btn-green2 btn-rectangle btnvermodal vertical-center" data-ventana="workbook" style="margin: 0.3ex; min-height: 60px;">
                                        <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('Workbook');?></span>
                                        <i class="btn-icon fa fa-pencil-square" style="font-size: 1.8em;"></i>
                                    </a>
                                </div>
                                <div class="col-md-6 btn-container">
                                    <a href="#" class="btn btn-info btn-rectangle btnvermodal vertical-center" data-ventana="games" style="margin: 0.3ex; min-height: 60px;">
                                        <span class="btn-label" style="font-size: 13px;"><?php echo JrTexto::_('game');?>s</span>
                                        <i class="btn-icon fa fa-puzzle-piece" style="font-size: 1.8em;"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="widget-box" id="setting-textbox" style="display: none;">
                    <div class="widget-header bg-blue">
                        <h4 class="widget-title">
                            <i class="fa fa-edit"></i>
                            <span><?php echo JrTexto::_('edit').' '.JrTexto::_('blank'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <ol class="row">
                                <li class="col-xs-12 correct-ans">
                                    <span class="item-title"><?php echo JrTexto::_('correct answer'); ?></span>
                                    <input type="text" class="form-control">
                                </li>
                                <li class="col-xs-12 distractions" style="display: none;">
                                    <span class="item-title">
                                        <?php echo JrTexto::_('distractors'); ?>
                                        <a href="#" class="btn btn-primary btn-xs pull-right add-distraction"> 
                                            <i class="fa fa-plus"></i>
                                            <?php echo JrTexto::_('add'); ?>
                                        </a>
                                    </span>
                                    <input type="text" class="form-control">
                                    <input type="text" class="form-control">
                                    <input type="text" class="form-control">
                                </li>
                                <li class="col-xs-12 assisted" style="display: none;">
                                    <span class="item-title"><?php echo JrTexto::_('assisted exercise'); ?></span>
                                    <input type="checkbox" class="isayuda checkbox-ctrl">
                                </li>
                            </ol>
                            <a href="#" class="btn btn-primary center-block save-setting-textbox" >
                                <i class="fa fa-floppy-o"></i> 
                                <?php echo JrTexto::_('save').' '.JrTexto::_('changes'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    </div>
</div> 
</div>
</form>

<div class="modal fade" id="addinfotxt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="min-height: 500px;">
      <div class="modal-header text-center">
        <button type="button" class="close btncloseaddinfotxt" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="addinfotitle"></h4>
      </div>
      <div class="modal-body" id="addinfocontent">
        <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>
      </div>
    </div>
  </div>
</div>
<div id="tpledittoolbar" class="col-md-12" style="display:none">
    <div class="btn-toolbar" role="toolbar" >
      <div><h4 class="color-blue"><?php echo ucfirst(JrTexto::_('tool')).'s '.JrTexto::_('to edit').' '.JrTexto::_('the template'); ?>:</h4></div>
      <div class="btn btn-primary btnedithtml" id="btnedithtml<?php echo $idgui; ?>"  ><i class="fa fa-text-width"></i> <?php echo ucfirst(JrTexto::_('edit')).' '.JrTexto::_('template'); ?></div>
      <div class="btn btn-primary disabled btnsavehtmlsystem" id="btnsavehtml<?php echo $idgui; ?>"  ><i class="fa fa-save"></i>  <?php echo ucfirst(JrTexto::_('save')).' '.JrTexto::_('template'); ?></div>
      <div class="btn btn-primary btndistribucion"><i class="fa fa-th-large"></i> <?php echo ucfirst(JrTexto::_('divide into')).' 2 '.JrTexto::_('column'); ?>s</div>
      <div class="btn btn-primary vervideohelp"><i class="fa fa-film"></i> <?php echo ucfirst(JrTexto::_('show guide video')); ?> </div>
    </div>
</div>

<div id="btns_progreso_intentos" style="display: none;">
    <div class="row" id="btns_control_alumno">
        <div class="col-sm-12 botones-control">
            <a class="btn btn-inline btn-blue btn-lg try-again"><i class="fa fa-undo"></i> <?php echo ucfirst(JrTexto::_('try again')); ?></a>
            <a class="btn btn-inline btn-success btn-lg save-progreso" ><?php echo ucfirst(JrTexto::_('continue')) ?> <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
</div>
<div id="footernoshow" style="display: none">
    <label style="float: left;margin-top: 0.5em;">
    <input type="checkbox" class="nomostrarmodalventana" name="chkDejarDeMostrar" value="1" id="chkDejarDeMostrar">   
     <?php echo ucfirst(JrTexto::_('Do not show this again')); ?>
    </label>
</div>

<div id="paneles-info" style="display: none;">    
    <!--
    <div id="panel-puntaje" class="panel-info hidden" style="display: none;">
        <span class="titulo-panel"><?php // echo ucfirst(JrTexto::_('score')); ?></span>
        <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="puntaje" placeholder="<?php //echo JrTexto::_('e.g.'); ?>: 100">
        <div class="info-show showonpreview" style="display: none;">000</div>
    </div>
    -->
</div>
<button id="btnresetinputEdicion" style="display: none"></button>
<button id="btnActivarinputEdicion" style="display: none"></button>
<audio id="curaudio" dsec="1" style="display: none;"></audio>
<section class="hidden" id="msjes_idioma" style="display: none;">
    <input type="hidden" id="attention" value="<?php echo JrTexto::_('Attention');?>">
    <input type="hidden" id="no_puedes_arrastrar" value='<?php echo JrTexto::_('You can not drag another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="no_puedes_seleccionar" value='<?php echo JrTexto::_('You can not select another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="you_must_finish" value='<?php echo ucfirst(JrTexto::_('you must do the exercise before trying again')); ?>'>
    <input type="hidden" id="bien_hecho" value='<?php echo ucfirst(JrTexto::_('well done!')); ?>'>
    <input type="hidden" id="cargando" value='<?php echo ucfirst(JrTexto::_('Loading')); ?>'>
    <input type="hidden" id="ejerc_exito" value='<?php echo ucfirst(JrTexto::_('you have complete the exercise succesfully')); ?>.'>
    <input type="hidden" id="ir_sgte_ejerc" value='<?php echo ucfirst(JrTexto::_('continue to the next exercise')); ?>.'>
    <input type="hidden" id="finalizar" value='<?php echo ucfirst(JrTexto::_('you have finished the activity')); ?>.'>
    <input type="hidden" id="puedes_continuar" value='<?php echo ucfirst(JrTexto::_('you can continue next exercise')); ?>.'>
    <input type="hidden" id="tiempo_acabo" value='<?php echo ucfirst(JrTexto::_('time is over')); ?>.'>
    <input type="hidden" id="intentalo_otra_vez" value='<?php echo ucfirst(JrTexto::_('you can try again')); ?>.'>
    <input type="hidden" id="select_activity" value='<?php echo ucfirst(JrTexto::_('Select Activity')); ?>.'>
    <input type="hidden" id="select_upload" value='<?php echo JrTexto::_('Select or upload'); ?>'>
    <input type="hidden" id="click_add_description" value='<?php echo JrTexto::_('Click here to add description') ?>'>
    <input type="hidden" id="click_add_title" value='<?php echo JrTexto::_('Click here to add title') ?>'>
    <input type="hidden" id="remove_tab" value='<?php echo JrTexto::_('remove tab') ?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="no_hay_audio" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback')); ?>'>
    <input type="hidden" id="no_hay_audio_en_escena" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback in scene')); ?>'>
    <input type="hidden" id="no_hay_orden_en_escena" value='<?php echo ucfirst(JrTexto::_('you have not set an order in the dialogues in scene')); ?>'>
    <input type="hidden" id="debes_establecer_orden" value='<?php echo ucfirst(JrTexto::_('you must set an order for the dialog bubbles')); ?>'>
    <input type="hidden" id="write_tag" value='<?php echo ucfirst(JrTexto::_('write tag')); ?>'>
    <input type="hidden" id="ejerc_no_completado" value='<?php echo ucfirst(JrTexto::_('you have not finished editing this exercise')); ?>'>
    <input type="hidden" id="grabar_audio" value='<?php echo ucfirst(JrTexto::_('Record')); ?>'>
    <input type="hidden" id="detener_audio" value='<?php echo ucfirst(JrTexto::_('Stop')); ?>'>
    <input type="hidden" id="escribir_texto" value='<?php echo ucfirst(JrTexto::_('Write')); ?>'>
    <input type="hidden" id="reproducir_audio" value='<?php echo ucfirst(JrTexto::_('Play')); ?>'>
    <input type="hidden" id="escribir_texto_aqui" value='<?php echo ucfirst(JrTexto::_('Write text here')); ?>'>
    <input type="hidden" id="seleccionevoz" value='<?php echo ucfirst(JrTexto::_('Select voice')); ?>'>
</section>
<script type="text/javascript">
var _IDHistorialSesion = 0;
$(document).ready(function(e){
    var gbl_INTENTOS = '<?php echo $this->intentos; ?>';
    var gbl_IDGUI = '<?php echo $idgui; ?>';
    var TMPL = [];
    TMPL[0] = <?php echo json_encode($this->oNegActividad->tplxMetodologia2(1)) ?>;
    TMPL[1] = <?php echo json_encode($this->oNegActividad->tplxMetodologia2(2)) ?>;
    TMPL[2] = <?php echo json_encode($this->oNegActividad->tplxMetodologia2(3)) ?>;
    var _curmetod=null; //Metodologia Activa
    var _curejercicio=null; //Ejercicio activo
    var _cureditable=true; //indica si estamos vista de edicion
    var _curdni=null;

   // actualizarPanelesInfo(); /* actividad_completar.js */
    cargarMensajesPHP();


    $('.istooltip').tooltip();

    
     $('#actividad').on('click','[data-audio]',function(){
        var srcaudio=$(this).attr('data-audio');
        srcaudio=srcaudio.trim();
        if(srcaudio!=undefined||srcaudio!=''){
            srcaudio=_sysUrlBase_+'/static/media/audio/'+srcaudio;
            $('#curaudio').attr('src',srcaudio);
            $('#curaudio').trigger('play');
        }
    });

    $('.btnvermodal').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var ventana=$(this).attr('data-ventana');
        var claseid=ventana+gbl_IDGUI;
        var titulo=$(this).find('.btn-label').html();
        titulo=titulo.toString().replace('<br>',' ');
        var paramadd='&idnivel='+$('#txtNivel').val()+'&idunidad='+$('#txtunidad').val()+'&idactividad='+$('#txtsesion').val();
        var url=_sysUrlBase_ +'/tools/' +ventana+'/?plt=modal'+paramadd;
        var modaldata={
          titulo:titulo,
          url:url,
          ventanaid:'ventana-'+ventana,
          borrar:false
        }
        var modal=sysmodal(modaldata);
    });
    
    /*************** solo Edicion */


    $("form").keypress(function(e) {
        if (e.which == 13) {
            return false;
        }
    });
   

    seleccionarmedia=function(e,tipo,donde){ //faltaria modicar
        if(tipo==''||tipo==undefined)return false;
        var rutabiblioteca=sitio_url_base+'/biblioteca/?plt=tinymce&robj=afile&donde='+donde+'&type='+tipo;
        $('#addinfotitle').html(MSJES_PHP.select_upload+tipo);
        $('#addinfocontent').load(rutabiblioteca);
        $('#addinfotxt').modal('show');
        $('#addinfotxt').show();
    };

    
    var registrarHistorialSesion = function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/agregar',
            type: 'POST',
            dataType: 'json',
            data: {'lugar': 'A', 'fechaentrada': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                console.log(resp);
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log("error");
            console.log(xhr.responseText);
        });        
    };

    registrarHistorialSesion();
    $(window).on('beforeunload', function(){    
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/editar',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {'idhistorialsesion': _IDHistorialSesion, 'fechasalida': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log("error");
            console.log(xhr.responseText);
            return false;
        });
    });

    $('.btnordenarEjericios').click(function(ev){
        ev.preventDefault();
        var titulo=$(this).attr('title');
        titulo=titulo.toString().replace('<br>',' ');
        var met=$(this).closest('.metod').attr('data-idmet');
        var paramadd='&nivel='+$('#txtNivel').val()+'&unidad='+$('#txtunidad').val()+'&sesion='+$('#txtsesion').val()+'&met='+met;
        paramadd+='&fcall=reordenartabsmetodologia';
        var url=_sysUrlBase_ +'/actividad/ordenarejericios/?plt=modal'+paramadd;
        var modaldata={
          titulo:titulo,
          url:url,
          ventanaid:'ventana-ordenarejericios'+met,
          borrar:true,
        }
        var modal=sysmodal(modaldata);
    })
    //plantilla completar
    $("*[data-tooltip=\"tooltip\"]").tooltip();
    $('#actividad-body').tplcompletar({editando:true,tpls:TMPL,idgui:gbl_IDGUI,nintentos:gbl_INTENTOS});
});
function reordenartabsmetodologia(met,posini,posifin){
    if($('.nav-tabs[data-id-metod="'+met+'"]').length==0) return;
        var nav=$('.nav-tabs[data-id-metod="'+met+'"]');
        var id=$(nav).attr('data-metod');
        var li=$(nav).find('a[href="#tab-'+id+'-'+posini+'"]').closest('li');
        li.insertBefore(li.siblings(':eq('+posifin+')'));
}
</script>
