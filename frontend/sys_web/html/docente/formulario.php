<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
$frm=@$this->datos;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style>
  .form-control, .input-group-addon {
    border: 1px solid #4683af;
    margin: 0px;
  }
  #menu-personal-add .slick-item>.btn{ width: 100%;}
  #menu-personal-add .slick-item>.btn.btn-panel.active{ opacity: 1 }
  #menu-personal-add .slick-item>.btn.btn-panel.active:after{
    content: "";
    width: 0px;
    height: 0px;
    border-left: 15px solid transparent;
    border-right: 15px solid transparent;
    border-bottom: 15px solid #fbf7f5;
    font-size: 0px;
    line-height: 0px;
    position: absolute;
    left: 40%;
    bottom: -7px;
  }
  #menu-personal-add .btn.btn-panel{ opacity: 0.75 }
  #menu-personal-add .btn.btn-panel.hover{ opacity: 0.8; background-color: #ffd; }
  .input-file-invisible{     
    position: absolute;
    height: 100%;
    width: 100%;
    cursor: pointer;
    opacity: 0; 
  }

  @media (min-width:768px) and (max-width: 879.99px){
    #menu-personal-add .btn.btn-panel{ font-size: 1em; }
  }
  @media (min-width: 385px) and (max-width: 767.99px){
    #menu-personal-add .btn.btn-panel{ font-size: .8em; }
    #contenido .foto_alumno{ height: 150px; }
  }
  @media (max-width: 384.99px) {
    #menu-personal-add .btn.btn-panel{ font-size: .8em; }
    #contenido .foto_alumno{ height: 150px; }
  }
  .slick-slider{ margin-bottom: 0.5ex; }
  ul.tablis li a{
     background: #f3813d;
    color: #f1f1e2;
    opacity: 0.6;
  }


  ul.tablis>li.active>a, ul.tablis>li.active>a:focus, ul.tablis>li.active>a:hover{
    background: #f3813d;
    color: #f1f1e2;
    opacity: 1;
  }

  ul.tablis li.active a:after{
    content: "";
    width: 0px;
    height: 0px;
    border-left: 15px solid transparent;
    border-right: 15px solid transparent;
    border-bottom: 15px solid #fbf7f5;
    font-size: 0px;
    line-height: 0px;
    position: absolute;
    left: 40%;
    bottom: -7px;
  }

  ul.tablis li a:hover, ul.tablis li a:focus{
    background: #f3813d;
    color: #f1f1e2;
    opacity: 0.85; 
  }

  .cajaselect {
    overflow: hidden;
    width: 100%;
    position: relative;
    border-radius: 5px;
  }
  
  .cajaselect::after {
    font-family: FontAwesome;
    font-size: 1.3em;
    content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #0070a0;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: #FFF;
    border-radius: 0px 0.3ex 0.3ex 0px;
  }

  .date > .input-group-addon{
    color: #fff;
    background-color: #0070a0;
    width: 30px;
  }

  .date.input-group{
    width: 100%;
    margin-top:0px;
  }
  .contool .toolbarmouse{
    display: none;
  }
  .contool:hover .toolbarmouse{
    display: block;
  }

  .toolbarmouse{    
    position: absolute;
    bottom: 1em;
    background-color: rgba(32, 33, 33, 0.32);
    color: aliceblue;
    width: 100%;
  }
</style>
<?php if($this->documento->plantilla!='modal'){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/alumno">&nbsp;<?php echo ucfirst(JrTexto::_('Students')); ?></a></li>
        <li>&nbsp;<?php echo JrTexto::_('Edit'); ?></li>
        <?php 
        if(!empty($this->breadcrumb))
        foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>
<?php } ?>
<div class="row" id="menu-personal-add">
  <div class="col-xs-12">
    <div class="slick-items<?php echo $idgui; ?>">
       <div class="slick-item">
        <a href="#tab1_generalinformation" data-toggle="tab" class="btn btn-primary btn-panel active">
            <i class="btn-icon fa fa-user"></i> <span><?php echo JrTexto::_('General information'); ?></span>
          </a>
       </div>
       <div class="slick-item">
          <a href="#tab1_grupoestudio" data-toggle="tab" class="btn btn-primary btn-panel">
            <i class="btn-icon fa fa-users"></i> <span><?php echo JrTexto::_('Study Group'); ?></span>
          </a>
       </div>
       <div class="slick-item">
          <a href="#tab1_cursos" data-toggle="tab" class="btn btn-primary btn-panel">
            <span class="btn-icon glyphicon glyphicon-book"></span> <span><?php echo JrTexto::_('Courses'); ?></span>
          </a>
       </div>
       <div class="slick-item">
          <a href="#tab1_notas" data-toggle="tab" class="btn btn-primary btn-panel">
            <span class="btn-icon glyphicon glyphicon-list-alt"></span> <span><?php echo JrTexto::_('Qualifications'); ?></span>
          </a>
       </div>
       <div class="slick-item">
          <a href="#tab1_horarios" data-toggle="tab" class="btn btn-primary btn-panel">
            <i class="btn-icon fa fa-calendar-check-o"></i> <span><?php echo JrTexto::_('Schedule'); ?></span>
          </a>
       </div>
       <div class="slick-item">
        <a href="#tab1_historial" data-toggle="tab" class="btn btn-primary btn-panel">
          <i class="btn-icon fa fa-hourglass-half"></i> <span><?php echo JrTexto::_('Historial'); ?></span>
        </a>
       </div>
       <div class="slick-item">
          <a href="#tab1_asistencias" data-toggle="tab" class="btn btn-primary btn-panel">
            <i class="btn-icon fa fa-pencil-square-o"></i> <span><?php echo JrTexto::_('Attendance'); ?></span>
          </a>
       </div>
    </div>
  </div>
  <div class="tab-content">
    <div class="tab-pane fade in active" id="tab1_generalinformation">
      <div class="col-xs-12">   
        <div class="slick-generalinformation<?php echo $idgui; ?>">
           <div class="slick-item">
            <a href="#tab2_datospersonales" data-toggle="tab" class="btn btn-warning btn-panel active"><?php echo JrTexto::_('Personal Information'); ?></a>
           </div>
           <div class="slick-item"><a href="#tab2_direccion" data-toggle="tab" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Address'); ?></a></div>
           <div class="slick-item"><a href="#tab2_apoderado" data-toggle="tab" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Proxy Information'); ?></a></div>
           <div class="slick-item"><a hhref="#tab2_educacion" data-toggle="tab" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Qualifications'); ?></a></div>
           <div class="slick-item"><a href="#tab2_experiencia_laboral" data-toggle="tab" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Work experience'); ?></a></div>
           <div class="slick-item"><a href="#tab2_referencias" data-toggle="tab" class="btn btn-warning btn-panel"><?php echo JrTexto::_('References'); ?></a></div>
        </div>
        <div class="tab-content" style="border: 1px solid #fa9f2d;">
          <div class="tab-pane fade in active" id="tab2_datospersonales">
            <form class="" id="frmDatosPersonales">              
              <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9">
                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                      <label for="titulo" class="text-left">N° <?php echo ucfirst(JrTexto::_('Id card')); ?></label>                    
                        <input type="text" class="form-control" name="dni" value="<?php echo @$frm["dni"]; ?>" placeholder=""> 
                    </div>  
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                      <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Names')); ?></label>                    
                        <input type="text" class="form-control" name="nombre" value="<?php echo ucfirst(@$frm["dni"]); ?>" placeholder=""> 
                    </div>  
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                      <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Father's last name")); ?></label>                    
                        <input type="text" class="form-control" name="ape_paterno" value="<?php echo ucfirst(@$frm["ape_paterno"]); ?>" placeholder=""> 
                    </div>  
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                      <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Mother's last name")); ?></label>                    
                        <input type="text" class="form-control" name="ape_materno" value="<?php echo ucfirst(@$frm["ape_materno"]); ?>" placeholder=""> 
                    </div>  
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                      <label><?php echo ucfirst(JrTexto::_("Gender")); ?></label>
                      <div class="cajaselect">
                        <select name="sexo" id="sexo" class="form-control">                     
                          <?php if(!empty($this->fksexo)) foreach ($this->fksexo as $fksexo) { ?>
                            <option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> >
                              <?php echo $fksexo["nombre"] ?>
                              </option>
                          <?php } ?>                            
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                      <label><?php echo ucfirst(JrTexto::_("Marital Status")); ?></label>
                      <div class="cajaselect">
                        <select name="estado_civil" id="estado_civil" class="form-control">
                          <?php if(!empty($this->fkestado_civil)) foreach ($this->fkestado_civil as $fkestado_civil) { ?>
                            <option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> >
                              <?php echo $fkestado_civil["nombre"] ?>
                              </option>
                          <?php } ?>                        
                        </select>
                      </div>
                    </div>
                  </div> 

                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                      <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Telephone')); ?></label>                    
                        <input type="text" class="form-control" name="telefono" value="<?php echo ucfirst(@$frm["telefono"]); ?>" > 
                    </div> 
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                      <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Mobile number')); ?></label>                    
                        <input type="text" class="form-control" name="celular" value="<?php echo ucfirst(@$frm["celular"]); ?>" > 
                    </div> 
                  </div> 
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 text-center">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center contool">
                      <div class="toolbarmouse"><span class="btn"><i class="fa fa-pencil"></i></span></div>
                      <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Photo')); ?></label>                    
                      <input type="file" class="input-file-invisible" name="fileFoto" id="fileFoto" accept="image/*">
                      <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/user_avatar.jpg" alt="foto" class="foto_alumno img-responsive center-block thumbnail">
                    </div>                   
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="form-group">
                      <label><?php echo ucfirst(JrTexto::_("Birthday")); ?></label>
                      <div class="form-group">
                        <div class="input-group date datetimepicker">
                          <input type="text" class="form-control" name="fechanac" id="fechanac" value="<?php echo ucfirst(@$frm["fechanac"]); ?>"> 
                          <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                      </div>
                      </div>
                    </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                  <a href="" class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a>
                  <a href="" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></a>
                </div>
                <div class="clearfix"></div><br>
              </div>            
            </form>
          </div>
          <div class="tab-pane fade" id="tab2_direccion">
            <form class="" id="frmDireccion">
              <div class="row">             
                <div class="col-xs-12 col-sm-12 col-md-12"> 
                  <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                      <label><?php echo ucfirst(JrTexto::_("Country")); ?></label>
                      <div class="cajaselect">
                        <select name="pais" id="pais" class="form-control">
                          <?php if(!empty($this->fkestado_civil)) foreach ($this->fkestado_civil as $fkestado_civil) { ?>
                            <option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> >
                              <?php echo $fkestado_civil["nombre"] ?>
                              </option>
                          <?php } ?>                        
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                      <label><?php echo ucfirst(JrTexto::_("Region")); ?></label>
                      <div class="cajaselect">
                        <select name="departamento" id="departamento" class="form-control">
                          <?php if(!empty($this->fkestado_civil)) foreach ($this->fkestado_civil as $fkestado_civil) { ?>
                            <option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> >
                              <?php echo $fkestado_civil["nombre"] ?>
                              </option>
                          <?php } ?>                        
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                      <label><?php echo ucfirst(JrTexto::_("Province")); ?></label>
                      <div class="cajaselect">
                        <select name="provincia" id="provincia" class="form-control">
                          <?php if(!empty($this->fkestado_civil)) foreach ($this->fkestado_civil as $fkestado_civil) { ?>
                            <option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> >
                              <?php echo $fkestado_civil["nombre"] ?>
                              </option>
                          <?php } ?>                        
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                      <label><?php echo ucfirst(JrTexto::_("District")); ?></label>
                      <div class="cajaselect">
                        <select name="distrito" id="distrito" class="form-control">
                          <?php if(!empty($this->fkestado_civil)) foreach ($this->fkestado_civil as $fkestado_civil) { ?>
                            <option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> >
                              <?php echo $fkestado_civil["nombre"] ?>
                              </option>
                          <?php } ?>                        
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                      <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('City')); ?></label>                    
                       <input type="text" class="form-control" name="ciudad" value="" placeholder=""> 
                    </div>  
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                      <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('urbanization')); ?></label>                    
                       <input type="text" class="form-control" name="ciudad" value="" placeholder=""> 
                    </div>  
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                      <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Street')); ?></label>                    
                       <input type="text" class="form-control" name="ciudad" value="" placeholder=""> 
                    </div>  
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <a href="" class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a>
                    <a href="" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></a>
                  </div>
                  <div class="clearfix"></div><br>
                </div>
              </div>           
            </form>
          </div>

          <div class="tab-pane fade" id="tab2_apoderado">
            <form class="" id="frmApoderado">
              <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group">
                    <label for="apo_dni" class="text-left">N° <?php echo ucfirst(JrTexto::_('Id card')); ?></label>                    
                      <input type="text" class="form-control" name="apo_dni" value="" placeholder=""> 
                  </div>  
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <label for="apo_nombre" class="text-left"><?php echo ucfirst(JrTexto::_('Names')); ?></label>                    
                      <input type="text" class="form-control" name="apo_nombre" value="" placeholder=""> 
                  </div>  
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group">
                    <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Paternal last name')); ?></label>                    
                      <input type="text" class="form-control" name="apo_ape_paterno" value="" placeholder=""> 
                  </div>  
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group">
                    <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Mother's last name")); ?></label>                    
                      <input type="text" class="form-control" name="apo_ape_materno" value="" placeholder=""> 
                  </div>  
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                    <label><?php echo ucfirst(JrTexto::_("Gender")); ?></label>
                    <div class="cajaselect">
                      <select name="apo_sexo" id="apo_sexo" class="form-control">                     
                        <?php if(!empty($this->fksexo)) foreach ($this->fksexo as $fksexo) { ?>
                          <option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> >
                            <?php echo $fksexo["nombre"] ?>
                            </option>
                        <?php } ?>                            
                      </select>
                    </div>
                  </div>
                </div>
            </form>
          </div>
          <div class="tab-pane fade" id="tab2_educacion">
            <div class="row"><div class="col-xs-12">
              <button class="btn btn-success agregar"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add'); ?></button>
              <button class="btn btn-default cancelar" style="display: none;"><i class="fa fa-times"></i> <?php echo JrTexto::_('Cancel'); ?></button>
            </div></div>
            <form class="form-horizontal" id="frmEducacion" style="display: none;">
              <div class="row"><div class="col-xs-12">
                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="txtInstitucionEducativa" class="control-label col-xs-12 col-sm-2"><?php echo ucfirst(JrTexto::_('Educational institution')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-10">
                        <input type="text" id="txtInstitucionEducativa" name="txtInstitucionEducativa" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcNivelEstudios" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Study Level')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select type="email" id="opcNivelEstudios" name="opcNivelEstudios" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcSituacion" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Situation')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select type="email" id="opcSituacion" name="opcSituacion" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtDesde" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Since')); ?> <span>*</span></label>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriodoDesdeMes" name="opcPeriodoDesdeMes" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=1; $i <=12 ; $i++) {
                            $nroMes = ($i<10)?'0'.$i:$i;
                            $mes = date('F', strtotime('01-'.$nroMes.'-1999'));
                            echo '<option value="'.$nroMes.'">'.ucfirst(JrTexto::_($mes)).'</option>';
                          } ?>
                        </select>
                      </div>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriodoDesdeAnio" name="opcPeriodoDesdeAnio" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=date('Y'); $i >= date('Y')-80 ; $i--) {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          } ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtHasta" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('To')); ?> <span>*</span></label>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriosoHastaMes" name="opcPeriosoHastaMes" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=1; $i <=12 ; $i++) {
                            $nroMes = ($i<10)?'0'.$i:$i;
                            $mes = date('F', strtotime('01-'.$nroMes.'-1999'));
                            echo '<option value="'.$i.'">'.ucfirst(JrTexto::_($mes)).'</option>';
                          } ?>
                        </select>
                      </div>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriosoHastaAnio" name="opcPeriosoHastaAnio" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=date('Y'); $i >= date('Y')-80 ; $i--) {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          } ?>
                        </select>
                      </div>
                    </div>
                  </div>
              </div></div>
              <div class="row"><div class="col-xs-12 text-center">
                <a href="#" class="btn btn-default btn-lg cancelar"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a> &nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary  btn-lg"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button>
              </div></div>
            </form>
            <hr>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th><?php echo JrTexto::_('Educational institution'); ?></th>
                  <th><?php echo JrTexto::_('Study Level'); ?></th>
                  <th><?php echo JrTexto::_('Situation'); ?></th>
                  <th><?php echo JrTexto::_('Since'); ?></th>
                  <th><?php echo JrTexto::_('To'); ?></th>
                  <th><?php echo JrTexto::_('Options'); ?></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Colegio 01</td>
                  <td>Primaria</td>
                  <td>Terminado</td>
                  <td><?php echo date('M-Y', strtotime('01-03-2000')); ?></td>
                  <td><?php echo date('M-Y', strtotime('01-12-2002')); ?></td>
                  <td>
                    <button class="btn btn-default btn-xs"><i class="color-blue fa fa-pencil"></i></button>
                    <button class="btn btn-default btn-xs"><i class="color-red fa fa-trash"></i></button>
                  </td>
                </tr>
                <tr>
                  <td>Colegio 02</td>
                  <td>Primaria</td>
                  <td>Terminado</td>
                  <td><?php echo date('M-Y', strtotime('01-03-2003')); ?></td>
                  <td><?php echo date('M-Y', strtotime('01-12-2005')); ?></td>
                  <td>
                    <button class="btn btn-default btn-xs"><i class="color-blue fa fa-pencil"></i></button>
                    <button class="btn btn-default btn-xs"><i class="color-red fa fa-trash"></i></button>
                  </td>
                </tr>
                <tr>
                  <td>Colegio 03</td>
                  <td>Secundaria</td>
                  <td>Terminado</td>
                  <td><?php echo date('M-Y', strtotime('01-03-2006')); ?></td>
                  <td><?php echo date('M-Y', strtotime('01-12-2010')); ?></td>
                  <td>
                    <button class="btn btn-default btn-xs"><i class="color-blue fa fa-pencil"></i></button>
                    <button class="btn btn-default btn-xs"><i class="color-red fa fa-trash"></i></button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div class="tab-pane fade" id="tab2_experiencia_laboral">
            <div class="row"><div class="col-xs-12">
              <button class="btn btn-success agregar"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add'); ?></button>
              <button class="btn btn-default cancelar" style="display: none;"><i class="fa fa-times"></i> <?php echo JrTexto::_('Cancel'); ?></button>
            </div></div>
            <form class="form-horizontal" id="frmExperienciaLaboral" style="display: none;">
              <div class="row"><div class="col-xs-12">
                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="txtEmpresa" class="control-label col-xs-12 col-sm-2"><?php echo ucfirst(JrTexto::_('Company')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-10">
                        <input type="text" id="txtEmpresa" name="txtEmpresa" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcSector" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Company sector')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select type="email" id="opcSector" name="opcSector" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="opcArea" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Area')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <select type="email" id="opcArea" name="opcArea" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtCargo" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Position')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <input type="text" id="txtCargo" name="txtCargo" class="form-control"  autocomplete="off" value="<?php echo '';?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtFuncionesRealizadas" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Carried out work')); ?> <span>*</span></label>
                      <div class="col-xs-12 col-sm-8">
                        <textarea id="txtFuncionesRealizadas" name="txtFuncionesRealizadas" class="form-control"  autocomplete="off" value="<?php echo '';?>"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtDesde" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Since')); ?> <span>*</span></label>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriodoDesdeMes" name="opcPeriodoDesdeMes" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=1; $i <=12 ; $i++) {
                            $nroMes = ($i<10)?'0'.$i:$i;
                            $mes = date('F', strtotime('01-'.$nroMes.'-1999'));
                            echo '<option value="'.$nroMes.'">'.ucfirst(JrTexto::_($mes)).'</option>';
                          } ?>
                        </select>
                      </div>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriodoDesdeAnio" name="opcPeriodoDesdeAnio" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=date('Y'); $i >= date('Y')-80 ; $i--) {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          } ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="txtHasta" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('To')); ?> <span>*</span></label>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriosoHastaMes" name="opcPeriosoHastaMes" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=1; $i <=12 ; $i++) {
                            $nroMes = ($i<10)?'0'.$i:$i;
                            $mes = date('F', strtotime('01-'.$nroMes.'-1999'));
                            echo '<option value="'.$i.'">'.ucfirst(JrTexto::_($mes)).'</option>';
                          } ?>
                        </select>
                      </div>
                      <div class="col-xs-6 col-sm-4">
                        <select type="email" id="opcPeriosoHastaAnio" name="opcPeriosoHastaAnio" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                          <option value="0">-<?php echo ucfirst(JrTexto::_('Select')); ?>-</option>
                          <?php for ($i=date('Y'); $i >= date('Y')-80 ; $i--) {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          } ?>
                        </select>
                      </div>
                    </div>
                  </div>
              </div></div>
              <div class="row"><div class="col-xs-12 text-center">
                <a href="#" class="btn btn-default btn-lg cancelar"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a> &nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary  btn-lg"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button>
              </div></div>
            </form>
            <hr>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th><?php echo JrTexto::_('Company'); ?></th>
                  <th><?php echo JrTexto::_('Company sector'); ?></th>
                  <th><?php echo JrTexto::_('Area'); ?></th>
                  <th><?php echo JrTexto::_('Position'); ?></th>
                  <th><?php echo JrTexto::_('Since'); ?></th>
                  <th><?php echo JrTexto::_('To'); ?></th>
                  <th><?php echo JrTexto::_('Options'); ?></th>
                </tr>
              </thead>
              <tbody>
              <?php for($i=1; $i<=4; $i++) { ?>
                <tr>
                  <td>Empresa <?php echo $i; ?></td>
                  <td>Ventas de PC</td>
                  <td>Ventas</td>
                  <td>Jefe de área</td>
                  <td><?php echo date('M-Y', strtotime('01-04-2010')); ?></td>
                  <td><?php echo date('M-Y', strtotime('01-09-2011')); ?></td>
                  <td>
                    <button class="btn btn-default btn-xs"><i class="color-blue fa fa-pencil"></i></button>
                    <button class="btn btn-default btn-xs"><i class="color-red fa fa-trash"></i></button>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>

          <div class="tab-pane fade" id="tab2_referencias">
            <form class="form-horizontal" id="frmDatosPersonales">
              <div class="row"><div class="col-xs-12">
                <div class="col-xs-12">
                  <div class="form-group">
                    <label for="txtNombreCompleto" class="control-label col-xs-12 col-sm-2"><?php echo ucfirst(JrTexto::_('Full name')); ?> <span>*</span></label>
                    <div class="col-xs-12 col-sm-10">
                      <input type="text" id="txtNombreCompleto" name="txtNombreCompleto" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label for="txtRelacion" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Relationship')); ?> <span>*</span></label>
                    <div class="col-xs-12 col-sm-8">
                      <input type="text" id="txtRelacion" name="txtRelacion" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label for="txtEmpresaaCargo" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Company')).'/'.ucfirst(JrTexto::_('Position')); ?> <span>*</span></label>
                    <div class="col-xs-12 col-sm-8">
                      <input type="text" id="txtEmpresaaCargo" name="txtEmpresaaCargo" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label for="txtTelefono" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Telephone')).'/'.ucfirst(JrTexto::_('Mobile number')); ?> <span>*</span></label>
                    <div class="col-xs-12 col-sm-8">
                      <input type="text" id="txtTelefono" name="txtTelefono" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label for="txtEmail" class="control-label col-xs-12 col-sm-4"><?php echo ucfirst(JrTexto::_('Email')); ?> <span>*</span></label>

                    <div class="col-xs-12 col-sm-8">
                      <input type="email" id="txtEmail" name="txtEmail" class="form-control"  autocomplete="off" required value="<?php echo '';?>">
                    </div>
                  </div>
                </div>
              </div></div>
              <div class="row"><div class="col-xs-12 text-center">
                <a href="javascript:history.back();" class="btn btn-default btn-lg"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a> &nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary  btn-lg"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button>
              </div></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_grupoestudio">
      <div class="col-xs-12">
        grupo de estudios
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_cursos">
      <div class="col-xs-12">
        cursos
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_notas">
      <div class="col-xs-12">
        notas
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_horarios">
      <div class="col-xs-12">
        horarios
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_historial">
      <div class="col-xs-12">
        historial
      </div>
    </div>
    <div class="tab-pane fade in " id="tab1_asistencias">
      <div class="col-xs-12">
        asistencias
      </div>
    </div>
  </div>
<script type="text/javascript">
$(document).ready(function(){  
            
  $('#frm-<?php echo @$id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'personal', 'savePersonal', xajax.getFormValues('frm-<?php echo @$id_vent;?>'));
      /*if(res){
        if(typeof <?php echo @$ventanapadre?> == 'function'){
          <?php echo @$ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Personal"))?>');
      }*/
     }
  });


    var croopimage=function(obj){
      var w=$(obj).attr('data-w')||130;
      var h=$(obj).attr('data-w')||150;
      var mw=$(obj).attr('data-w')||w;
      var mh=$(obj).attr('data-w')||h;
      var clstmp='image'+Date.now();
      $(obj).addClass(clstmp);
      var objimage=$(obj).croppie({
        enableExif: true,
        viewport: { width: w,  height: w  },
        boundary:{ width: wh,  height: wh },
        showZoomer:false
      });
    }
                        
    /*miavatar.croppie('result', 'blob').then(function(blob) {
      console.log(blob);
    });*/


 
  
  $('.cargarfile').on('change',function(){
    var file=$(this);
     agregar_msj_interno('success', '<?php echo JrTexto::_("loading");?> '+file.attr('data-texto')+'...','msj-interno',true);
     $('#frmPersonal').attr('action',file.attr('data-action'));
     $('#frmPersonal').attr('target','if_cargar_'+file.attr('data-campo'));
     $('#frmPersonal').attr('enctype','multipart/form-data');
     $('#frmPersonal').submit();                
  });

  $('.tab-pane').on('click', '.btn.agregar', function(e) {
    e.preventDefault();
    var idTabPane = $(this).closest('.tab-pane').attr('id');
    $('#'+idTabPane+' .btn.agregar').hide();
    $('#'+idTabPane+' .btn.cancelar').show();
    $('#'+idTabPane+' form').show('fast');
  }).on('click', '.btn.cancelar', function(e) {
    e.preventDefault();
    var idTabPane = $(this).closest('.tab-pane').attr('id');
    $('#'+idTabPane+' .btn.cancelar').hide();
    $('#'+idTabPane+' .btn.agregar').show();
    $('#'+idTabPane+' form').hide('fast');
  });
});

$('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }
});

function result_archivo(result, mensaje,txt) {
  if(result) {
    $("#txt"+txt).val(mensaje);
    agregar_msj_interno2("success", '<?php echo JrTexto::_("File updated successfully");?>',"msj-interno2",true);
    var tipo=$("#id"+txt).attr("data-type");
    if(tipo=="imagen"||tipo=="video"||tipo=="audio")
      $("#id"+txt).removeAttr("style").attr("src", _sysUrlStatic_ + mensaje );
    else
      $("#id"+txt).removeAttr("style").attr("href",  _sysUrlStatic_ + mensaje );
    }else{
      agregar_msj_interno2("warning", mensaje,"msj-interno2",true);
    }
}

$(document).ready(function(){
    var optionslike={
            //dots: true,
            infinite: false,
            //speed: 300,
            //adaptiveHeight: true
            navigation: false,
            slidesToScroll: 1,
            centerPadding: '60px',
            slidesToShow: 5,
            responsive:[
                { breakpoint: 1200, settings: {slidesToShow: 5} },
                { breakpoint: 992, settings: {slidesToShow: 4 } },
                { breakpoint: 880, settings: {slidesToShow: 3 } },
                { breakpoint: 720, settings: {slidesToShow: 2 } },
                { breakpoint: 320, settings: {slidesToShow: 1 /*,arrows: false, centerPadding: '40px',*/} }       
            ]
          };
    setTimeout(function(){
      $('.slick-items<?php echo $idgui; ?>').slick(optionslike);
      $('.slick-generalinformation<?php echo $idgui; ?>').slick(optionslike);
    },400);

    $('#menu-personal-add .btn.btn-panel').click(function(){
      $(this).closest('div').siblings('div').find('a').removeClass('active');
      $(this).addClass('active');
    })

});
  
</script>

