<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
//var_dump($frm);
?>
<div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <?php if($this->documento->plantilla!='modal'){?><div class="panel-heading bg-blue">
        <h3><?php echo JrTexto::_('Alumno'); ?><small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></h3>
        <div class="clearfix"></div>
      </div><?php } ?>      
      <div class="panel-body">
        <div id="msj-interno"></div>
        <div class="pull-right">
          <a href="#" class="btn btn-xs btn-primary"><i class="fa fa-print"></i> <?php echo JrTexto::_("Print");?></a>
          <a href="#" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> <?php echo JrTexto::_("edit");?></a>
          <?php if($this->documento->plantilla=='modal'){?><a href="#" class="btn btn-xs btn-default"><i class="fa fa-close"></i> <?php echo JrTexto::_("close");?></a><?php } ?>
        </div>
        <div> 
          <h4>I. <?php echo JrTexto::_("Personal information");?></h4>
          <div class="col-md-12 col-sm-12 col-xs-12">
          <strong><?php echo JrTexto::_('Document');?> :</strong> <?php echo $frm["ape_paterno"]." ".$frm["ape_materno"].", ".$frm["nombre"]; ?>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
          <strong><?php echo JrTexto::_('Full name');?> :</strong> <?php echo $frm["ape_paterno"]." ".$frm["ape_materno"].", ".$frm["nombre"]; ?>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
          <strong><?php echo JrTexto::_('Telephone');?> :</strong> <?php echo $frm["celular"]." / ".$frm["telefono"]; ?>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
          <strong><?php echo JrTexto::_('Email');?> :</strong> <?php echo $frm["email"]; ?>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
          <strong><?php echo JrTexto::_('Birthday');?> :</strong> <?php echo $frm["fecha_nac"]; ?>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
          <strong><?php echo JrTexto::_('Marital Status');?> :</strong> <?php echo $frm["estado"]; ?>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
          <strong><?php echo JrTexto::_('Marital Status');?> :</strong> <?php echo $frm["estado"]; ?>
          </div>
          
          
        </div>
      </div>
    </div>
  </div>
</div>