<?php
$usuarioAct = NegSesion::getUsuario();
$urlbase = $this->documento->getUrlBase();
?>
<style type="text/css">
	.row >.item-recurso{
		text-align: center;
		padding: 1ex;
	}
	.row >.item-recurso .panel{
		text-align: center;
		padding: 0.2ex;
		margin:0.05ex; 0.1ex;
	}
	.row >.item-recurso .panel-body{
		padding: 0.1ex;
	}
		.slick-slide{
		position: relative;
	}

	.cajaselect {  
	   overflow: hidden;
	   width: 230px;
	   position:relative;
	   font-size: 1.8em;
	}
	select#level-item,select#unidad-item ,select#actividad-item {
	   background: transparent;
	   border: 2px solid #4683af;   
	   padding: 5px;
	   width: 250px;
	   padding: 0.3ex 2ex; 
	}
	select:focus{ outline: none;}

	.cajaselect::after{
	   font-family: FontAwesome;
	   content: "\f0dd";
	  display: inline-block;
	  text-align: center;
	  width: 30px;
	  height: 100%;
	  background-color: #4683af;
	  position: absolute;
	  top: 0;
	  right: 0px;
	  pointer-events: none;
	  color: antiquewhite;
	  bottom: 0px;
	}

	.titulo{
		border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
		width: 85%;
		background-color: rgba(255, 255, 255, 0.53);
    	color: #000;
	}
	.autor{
		border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
		width: 85%; text-align: right; 
		background-color: rgba(255, 255, 255, 0.53);
    	color: #000;
	}
	.caratula{
		border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%	
	}
</style>
<div class="container">
	<div class="row" id="levels" style="padding-top: 1ex; ">
            <div class="col-md-3">
                <ol class="breadcrumb">
                  <li><a href="<?php echo $this->documento->getUrlSitio(); ?>"><?php echo JrTexto::_("Home")?></a></li>                  
                  <li class="active"><?php echo JrTexto::_("Resourses")?></li>
                </ol>
            </div>
             <div class="col-md-3 cajaselect "> <select name="level" id="level-item" style="">
             <option value="" ><?php echo JrTexto::_("All Levels")?></option>
              <?php if(!empty($this->niveles))
                     foreach ($this->niveles as $nivel){?>
                     <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $nivel["idnivel"]==$this->idnivel?'Selected':'';?>><?php echo $nivel["nombre"]?></option>
              <?php }?>               
             </select></div>
             <div class="col-md-3 cajaselect "><select name="unidad" id="unidad-item" style="">
             <option value="" ><?php echo JrTexto::_("All Unit")?></option>
              <?php if(!empty($this->unidades))
                     foreach ($this->unidades as $unidad){?>
                     <option value="<?php echo $unidad["idnivel"]; ?>" <?php echo $unidad["idnivel"]==$this->idunidad?'Selected':'';?>><?php echo $unidad["nombre"]?></option>
              <?php }?>               
             </select></div>
             <div class="col-md-3 cajaselect "> <select name="actividad" id="actividad-item" style="">
             <option value="" ><?php echo JrTexto::_("All activities")?></option>
              <?php if(!empty($this->actividades))
                     foreach ($this->actividades as $act){?>
                     <option value="<?php echo $act["idnivel"]; ?>" <?php echo $act["idnivel"]==$this->idactividad?'Selected':'';?>><?php echo $act["nombre"]?></option>
              <?php }?>               
             </select></div>   

    </div>
 	<div class="row" id="div_cajas">

 		

 	<?php if(!empty($this->recursos))
	foreach ($this->recursos as $rec){?>

 		<div class="col-md-2 col-sm-3 col-xs-12 item-recurso">
 			<div class="panel panel-danger" >
				<a href="<?php echo $this->documento->getUrlBase(); ?>/tools/smartbook/<?php echo $rec["idnivel"];?>/<?php echo $rec["idunidad"];?>/<?php echo $rec["idactividad"];?>/?orden=<?php echo $rec["orden"];?>&dni=<?php echo $rec["idpersonal"]; ?>">
				<div class="panel-body" style="padding: " >					
					<h3 class="panel-title titulo" ><?php echo $rec["titulo"];?></h3>					
					<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/imagenes/caja_resource.png" class="img-responsive" width="100%">
					<img src="<?php echo str_replace('__xRUTABASEx__', $urlbase, $rec["caratula"]); ?>" class="caratula" >
					<h3 class="panel-title autor" >
					<?php echo @$usuarioAct["nombre_full"];?></h3>
				</div>
				</a>
			</div>
 		</div>
 		<?php } ?>
 	</div>    	
</div>
<div class="col-md-2 col-sm-6 col-xs-6 item-recurso" id="div_nuevo" style="display: none;">
 	<div class="panel panel-danger" >
		<a id="a_nuevo" target="_blank">
			<div class="panel-body">					
				<h3 class="panel-title " >&nbsp</h3>
				<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/imagenes/caja_resource.png" class="img-responsive" width="100%">				
				<span class="col-md-2 col-sm-3 col-xs-12 fa fa-plus" style="font-size: 100px; position: absolute; top:70px; left: 40px"></span>
				<h3 class="panel-title " >&nbsp</h3>
			</div>
		</a>				
	</div>
</div>


<script type="text/javascript">
$(document).ready(function(){		
	

	$('#level-item').change(function(){
		var idlevel=$(this).val();
		//alert(idpro);
		$('#unidad-item option').remove();
		var data={'tipo':'U','idpadre':idlevel}
		var res = xajax__('', 'Recursos', 'getxPadre',data);
		if(res){
		$('#unidad-item').append('<option value=""><?php echo JrTexto::_("All Unit")?></option>');
		$.each(res,function(){
		x=this;
		$('#unidad-item').append('<option value='+x["idnivel"]+'>'+x["nombre"]+'</option>');
		});
		//$( "#filtros" ).submit();
		}

		cargar_recursos();

	});

	$('#unidad-item').change(function(){
		var idunidad=$(this).val();
		//alert(idpro);
		$('#actividad-item option').remove();
		var data={'tipo':'L','idpadre':idunidad}
		var res = xajax__('', 'Recursos', 'getxPadre',data);
		if(res){
		$('#actividad-item').append('<option value=""><?php echo JrTexto::_("All Activities")?></option>');
		$.each(res,function(){
		x=this;
		$('#actividad-item').append('<option value='+x["idnivel"]+'>'+x["nombre"]+'</option>');
		});
		//$( "#filtros" ).submit();
		}

		cargar_recursos();

	});

	$('#actividad-item').change(function(){
		cargar_recursos();
	});


});

function adicionarcajaadd(){
	var idnivel=$("#level-item").val();
	var idunidad=$("#unidad-item").val();
	var idactividad=$("#actividad-item").val();
    if(idnivel&&idunidad&&idactividad){
    	var addnuevo=$('#div_nuevo').clone(true,true);    	
    	orden=$('#div_cajas').find('.item-recurso').length;
    	addnuevo.find('#a_nuevo').attr('href',_sysUrlBase_+'/tools/teacherresources/'+idnivel+'/'+idunidad+'/'+idactividad+'/?orden='+orden);
    	addnuevo.show();
    	$('#div_cajas').prepend(addnuevo);	
	}else 
	$('#div_cajas #div_nuevo').hide();
}

function cargar_recursos(){

	var idlevel=$("#level-item").val();
	var idunidad=$("#unidad-item").val();
	var idactividad=$("#actividad-item").val();
	var tipo="P";
	var idpersonal=<?php echo $this->iddoc;?>;
	var orden=0;
	$('#div_cajas').html("");
	var data={'idnivel':idlevel,'idunidad':idunidad,'idactividad':idactividad,'tipo':tipo,'idpersonal':idpersonal}
	var res = xajax__('', 'Recursos', 'CargarRecurso',data);
	if(res){		
	$.each(res,function(){
	x=this;
    var caratula=x["caratula"];
    caratula=caratula.replace(/__xRUTABASEx__/gi,_sysUrlBase_);
	
	$('#div_cajas').append('<div class="col-md-2 col-sm-3 col-xs-12 item-recurso"><div class="panel panel-danger" ><a href="'+_sysUrlBase_+'/tools/teacherresources/'+x["idnivel"]+'/'+x["idunidad"]+'/'+x["idactividad"]+'/?orden='+x["orden"]+'&dni=<?php echo $this->iddoc; ?>" target="_blank"><div class="panel-body"><h3 class="panel-title titulo" >'+x["titulo"]+'</h3><img src="'+_sysUrlStatic_+'/media/imagenes/caja_resource.png" class="img-responsive" width="100%"><img src="'+caratula+'" class="img-responsive caratula"><h3 class="panel-title autor"><?php echo @$usuarioAct["nombre_full"];?></h3></div></a></div></div>');
		orden=parseInt(orden)+parseInt(x["orden"]);
	});
	
	}
	
	orden=parseInt(orden)+1;
	if (idlevel && idunidad && idactividad){
		adicionarcajaadd();
	}else{
		$('#div_nuevo').hide();
	}

}
adicionarcajaadd();
</script>    