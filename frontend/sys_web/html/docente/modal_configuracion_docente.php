<?php 
if(!empty($this->config_doc)) $conf=$this->config_doc[0];
?>
<div class="row">
    <form method="post" id="frmConfiguracion_docente"  target="" enctype="" class="col-xs-12 form-horizontal form-label-left" >
        <input type="hidden" name="accion" id="accion" value="<?php echo $this->frmaccion; ?>">
        <input type="hidden" name="pkIdconfigdocente" id="pkidconfigdocente" value="<?php echo @$conf["idconfigdocente"];?>">

        <div class="row form-group" id="levels" style="padding-top: 1ex; ">
            <div class="col-xs-12 col-sm-6 select-ctrl-wrapper select-azul">
                <select name="opcIdnivel" id="level-item" class="form-control select-ctrl">
                    <option value="-1" >- <?php echo JrTexto::_("Select Level")?> -</option>
                    <?php if(!empty($this->niveles))
                    foreach ($this->niveles as $nivel){?>
                    <option value="<?php echo $nivel["idnivel"]; ?>"><?php echo $nivel["nombre"]?></option>
                    <?php }?>               
                </select>
            </div>
            <div class="col-xs-12 col-sm-6 select-ctrl-wrapper select-azul">
                <select name="opcIdunidad" id="unit-item" class="form-control select-ctrl">
                    <option value="-1" >- <?php echo JrTexto::_("Select Unit")?> -</option>             
                </select>
            </div>
        </div>
<!--
        <?php if(isset($this->mostrarCampo['tiempoactividades']) && $this->mostrarCampo['tiempoactividades']==true ){ ?>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTiempoactividades">
                <?php echo JrTexto::_('Activities');?>:
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="txtTiempoactividades" name="txtTiempoactividades" required="required" class="form-control col-md-7 col-xs-12 change-time" value="<?php echo @$conf["tiempoactividades"];?>" placeholder="HH:MM:SS">
            </div>
        </div>
        <?php } ?>

        <?php if(isset($this->mostrarCampo['tiempoteacherresrc']) && $this->mostrarCampo['tiempoteacherresrc']==true ){ ?>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTiempoteacherresrc">
                <?php echo JrTexto::_('Teacher Resources');?>:
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="txtTiempoteacherresrc" name="txtTiempoteacherresrc" required="required" class="form-control col-md-7 col-xs-12 change-time" value="<?php echo @$conf["tiempoteacherresrc"];?>" placeholder="HH:MM:SS">
            </div>
        </div>
        <?php } ?>

        <?php if(isset($this->mostrarCampo['tiempogames']) && $this->mostrarCampo['tiempogames']==true ){ ?>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTiempogames">
                <?php echo JrTexto::_('Game').'s';?>:
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="txtTiempogames" name="txtTiempogames" required="required" class="form-control col-md-7 col-xs-12 change-time" value="<?php echo @$conf["tiempogames"];?>" placeholder="HH:MM:SS">
            </div>
        </div>
        <?php } ?>

        <?php if(isset($this->mostrarCampo['tiempoexamenes']) && $this->mostrarCampo['tiempoexamenes']==true ){ ?>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTiempoexamenes">
                <?php echo ucfirst(JrTexto::_('Exams'));?>:
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="txtTiempoexamenes" name="txtTiempoexamenes" required="required" class="form-control col-md-7 col-xs-12 change-time" value="<?php echo @$conf["tiempoexamenes"];?>" placeholder="HH:MM:SS">
            </div>
        </div>
        <?php } ?>

        <?php if(isset($this->mostrarCampo['tiempototal']) && $this->mostrarCampo['tiempototal']==true ){ ?>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTiempototal">
                <?php echo JrTexto::_('Total');?>:
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text"  id="txtTiempototal" name="txtTiempototal" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$conf["tiempototal"];?>" placeholder="HH:MM:SS" readonly="readonly">
            </div>
        </div>
        <?php } ?>
-->
        
        <table class="table table-striped table-bordered" id="tblConfiguracion">
            <thead>
                <tr>
                    <th style="width: 30%;"></th>
                    <th colspan="4">
                        <label class="col-xs-12 text-center">Tiempo óptimo</label>
                    </th>
                </tr>
                <tr>
                    <th></th>
                    <th>
                        <label class="col-sm-12 text-center">Actividades</label>
                        <div class="col-sm-9" style="padding:0;">
                            <input type="text" class="form-control tiempo-general" name="txtTiempoGeneralAct" id="txtTiempoGeneralAct" placeholder="HH:MM:SS">
                        </div>
                        <div class="cool-xs-12 col-sm-3" style="padding:0;">
                            <button class="btn btn-primary btn-block aplicar-todos act" data-aplicaclase="tiempo-actividad"><i class="fa fa-level-down"></i></button>
                        </div>
                    </th>
                    <th>
                        <label class="col-sm-12 text-center">Teacher Resources</label>
                        <div class="col-sm-9" style="padding:0;">
                            <input type="text" class="form-control tiempo-general" name="txtTiempoGeneralAct" id="txtTiempoGeneralAct" placeholder="HH:MM:SS">
                        </div>
                        <div class="cool-xs-12 col-sm-3" style="padding:0;">
                            <button class="btn btn-primary btn-block aplicar-todos tresrc" data-aplicaclase="tiempo-tresrc"><i class="fa fa-level-down"></i></button>
                        </div>
                    </th>
                    <th>
                        <label class="col-sm-12 text-center">Games</label>
                        <div class="col-sm-9" style="padding:0;">
                            <input type="text" class="form-control tiempo-general" name="txtTiempoGeneralAct" id="txtTiempoGeneralAct" placeholder="HH:MM:SS">
                        </div>
                        <div class="cool-xs-12 col-sm-3" style="padding:0;">
                            <button class="btn btn-primary btn-block aplicar-todos game" data-aplicaclase="tiempo-game"><i class="fa fa-level-down"></i></button>
                        </div>
                    </th>
                    <th width="15%">
                        <label class="col-sm-12 text-center">Total</label>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr><td colspan="5">Elija un nivel y/o actividad</td></tr>
            </tbody>
        </table>
    </form>
</div>

<script>
$(document).ready(function() {
    $("#txtTiempototal").mask("99:99:99");
    $("#txtTiempoactividades").mask("99:99:99");
    $("#txtTiempoteacherresrc").mask("99:99:99");
    $("#txtTiempogames").mask("99:99:99");
    $("#txtTiempoexamenes").mask("99:99:99");

    $(".tiempo-general").mask("99:99:99");

    /*
    //******************************************************
    //*** Las funciones toSeconds() y toHHMMSS()         ***
    //*** estan declaradas en docente/panelcontrol.php   ***
    //******************************************************
    var toSeconds = function ( time ) {
        var parts = time.split(':');
        return (+parts[0]) * 60 * 60 + (+parts[1]) * 60 + (+parts[2]); 
    };

    var toHHMMSS = function (sec) {
        var sec_num = parseInt(sec, 10); 
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        var time    = hours+':'+minutes+':'+seconds;
        return time;
    };*/
    var leerniveles=function(data){
        try{
            var res = xajax__('', 'niveles', 'getxPadre', data);
            if(res){ return res; }
            return false;
        }catch(error){
            return false;
        }       
    };

    var currentRequest = null;
    var listarActividades = function (idnivel, idunidad) {
        currentRequest = $.ajax({
            url: _sysUrlBase_+'/configuracion_docente/tiemposXactividad/',
            type: 'POST',
            dataType: 'json',
            data: {'tipo':'L','idpadre':idunidad},
            beforeSend: function(){
                if(currentRequest != null) {
                    currentRequest.abort();
                }
                $('#tblConfiguracion tbody').html('<tr><td colspan="5"><h3 class="text-center"><i class="fa fa-cog fa-spin fa-fw"></i><span class="">Loading...</span></h3></td></tr>');
            }
        }).done(function(resp) {
            if(resp.code=='ok'){
                var html='';
                var opciones='<option value="-1" >- <?php echo JrTexto::_("Select Activity")?> -</option>';
                var actividades=resp.data;
                var claseA=$('#tblConfiguracion .btn.aplicar-todos.act').data('aplicaclase');
                var claseTR=$('#tblConfiguracion .btn.aplicar-todos.tresrc').data('aplicaclase');
                var claseG=$('#tblConfiguracion .btn.aplicar-todos.game').data('aplicaclase');
                $.each(actividades, function(i, v) {
                    html+='<tr data-idnivel="'+idnivel+'" data-idunidad="'+v['idpadre']+'" data-idactividad="'+v['idnivel']+'" data-idconfigdocente="'+v['idconfigdocente']+'">'+
                        '<td>'+v['nombre']+'</td>'+
                        '<td><div class="col-xs-12 pull-right"><input type="text" class="form-control '+claseA+'" name="txtTiempoActividad_'+v['idnivel']+'" id="txtTiempoActividad_'+v['idnivel']+'" value="'+v['tiempoactividades']+'" placeholder="HH:MM:SS"></div></td>'+
                        '<td><div class="col-xs-12 pull-right"><input type="text" class="form-control '+claseTR+'" name="txtTiempoActividad_'+v['idnivel']+'" id="txtTiempoActividad_'+v['idnivel']+'" value="'+v['tiempoteacherresrc']+'" placeholder="HH:MM:SS"></div></td>'+
                        '<td><div class="col-xs-12 pull-right"><input type="text" class="form-control '+claseG+'" name="txtTiempoActividad_'+v['idnivel']+'" id="txtTiempoActividad_'+v['idnivel']+'" value="'+v['tiempogames']+'" placeholder="HH:MM:SS"></div></td>'+
                        '<td><div class="col-xs-12 pull-right"><input type="text" class="form-control tiempo-total" name="txtTiempoActividad_'+v['idnivel']+'" id="txtTiempoActividad_'+v['idnivel']+'" value="'+v['tiempototal']+'" placeholder="HH:MM:SS" readonly="readonly"></div></td>';
                    opciones+='<option value="'+v['idnivel']+'">'+v['nombre']+'</option>';
                });
                $('#tblConfiguracion tbody').html(html);
                $(".tiempo-actividad").mask("99:99:99");
                $('#actividad-item').html(opciones);
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.mensaje, 'error');
            }
            currentRequest=null;
        }).fail(function(xhr, textStatus, errorThrown) {
            //console.log(xhr); 
            //console.log(xhr.responseText);
            if(xhr.status!=0) {
                mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
            }
        });
    };

    var cargarListados = function(){
        var nivel = $('#level-item').val();
        var unidad = ($('#unit-item').val()!=='-1')?$('#unit-item').val():'';
        var actividad = ($('#actividad-item').val()!=='-1')?$('#actividad-item').val():'';
        listarActividades(nivel, unidad);
    };

    var addniveles=function(data,obj){
        var objini=obj.find('option:first').clone();
        obj.find('option').remove();
        obj.append(objini);
        if(data!==false){
            var html='';
            $.each(data,function(i,v){
                html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
            });
            obj.append(html);
        }
        id=obj.attr('id');
        //if(id==='unit-item') cargarListados();
    };

    $('#level-item').change(function(){
        var idnivel=$(this).val();
        var data={tipo:'U','idpadre':idnivel}
        var donde=$('#unit-item');
        if(idnivel!=='' || idnivel!=='-1') addniveles(leerniveles(data),donde);
        else addniveles(false,donde);
        donde.trigger('change');
    });
    $('#unit-item').change(function(){
        cargarListados();
        $('table .tiempo-general').val('');
    });

    $('.btn.aplicar-todos').click(function(e) {
        e.preventDefault();
        var clase=$(this).data('aplicaclase');
        var nuevo_tiempo=$(this).closest('th').find('input.tiempo-general').val();
        $('.'+clase).each(function(index, el) {
            $(this).attr('value', nuevo_tiempo).val(nuevo_tiempo);
        });
        $('table .tiempo-general').val('');
        $('.'+clase).trigger('change');
    });
    $('input.tiempo-general').keypress(function(e) {
        if(e.which==13){
            e.preventDefault();
            $(this).closest('th').find('.btn.aplicar-todos').trigger('click');
        }
    });

    $('#tblConfiguracion').on('change', '.tiempo-actividad, .tiempo-tresrc, .tiempo-game', function(event) {
        var total=0;
        var $fila=$(this).closest('tr');
        $fila.each(function(){
            $(this).find('input.form-control').not('[readonly]').each(function(index, el) {
                total += toSeconds( $(this).val() );
            });
            $(this).find('input.form-control[readonly]').val( toHHMMSS(total) );
        });
    }).on('keypress', '.tiempo-actividad, .tiempo-tresrc, .tiempo-game', function(e)  {
        if(e.which==13){ 
            e.preventDefault(); 
            $(this).trigger('change');
        }
    });

});
</script>