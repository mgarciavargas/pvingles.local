<div class="row" id="breadcrumb"> 
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div>

<div class="row" id="calificaciones-listar">
	<input type="hidden" id="hIdDocente" value="<?php echo $this->usuarioAct["dni"]; ?>">
	<div class="row" id="filtros">
		<div class="form-group col-xs-12 col-sm-4 select-ctrl-wrapper select-azul">
			<select name="opcLocal" id="opcLocal" class="form-control select-ctrl">
				<option value="0">- <?php echo JrTexto::_("Select Local"); ?> -</option>
				<?php foreach ($this->locales as $l) { ?>
				<option value="<?php echo $l["idlocal"]; ?>"><?php echo $l["nombre"]; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="form-group col-xs-12 col-sm-4 select-ctrl-wrapper select-azul">
			<select name="opcAmbiente" id="opcAmbiente" class="form-control select-ctrl">
				<option value="0">- <?php echo JrTexto::_("Select Classroom"); ?> -</option>
			</select>
		</div>
		<div class="form-group col-xs-12 col-sm-4 select-ctrl-wrapper select-azul">
			<select name="opcGrupoAulaDetalle" id="opcGrupoAulaDetalle" class="form-control select-ctrl">
				<option value="">- <?php echo JrTexto::_("Select Group"); ?> -</option>
			</select>
		</div>
	</div>

	<div class="row" id="resultado_busqueda"></div>
</div>

<section id="mensajes_idioma" class="hidden">
	<input type="hidden" id="select_classroom" value="<?php echo ucfirst(JrTexto::_("Select Classroom")); ?>">
	<input type="hidden" id="select_group" value="<?php echo ucfirst(JrTexto::_("Select Group")); ?>">
	<input type="hidden" id="there_are_no_files" value="<?php echo ucfirst(JrTexto::_("There are no files to display")); ?>">
	<input type="hidden" id="view" value="<?php echo ucfirst(JrTexto::_("view")); ?>">
	<input type="hidden" id="loading" value="<?php echo ucfirst(JrTexto::_("loading")); ?>">
</section>