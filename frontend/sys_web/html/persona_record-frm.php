<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Persona_record'">&nbsp;<?php echo JrTexto::_('Persona_record'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdrecord" id="pkidrecord" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdpersona">
              <?php echo JrTexto::_('Idpersona');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdpersona" name="txtIdpersona" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idpersona"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTiporecord">
              <?php echo JrTexto::_('Tiporecord');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <select id="txttiporecord" name="txtTiporecord" class="form-control col-md-7 col-xs-12" required>
              <option value=""><?php JrTexto::_("Seleccione");?></option>
                              <option value="1"><?php echo JrTexto::_('Activo');?></option>
              <option value="0"><?php echo JrTexto::_('Inactivo');?></option>
                                   
              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTitulo">
              <?php echo JrTexto::_('Titulo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTitulo" name="txtTitulo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["titulo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDescripcion">
              <?php echo JrTexto::_('Descripcion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <textarea id="txtDescripcion" name="txtDescripcion" class="form-control" ><?php echo @trim($frm["descripcion"]); ?></textarea>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtArchivo">
              <?php echo JrTexto::_('Archivo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <a href=""  data-type="archivo" class="img-thumbnail" id="idarchivo"> <?php echo JrTexto::_('ver Archivo');?></a>
                 <span class="btn btn-info btn-file"> <i class="fa fa-upload"></i> <?php echo JrTexto::_('Seleccionar Documento');?> 
                 <input data-texto="Documento" data-campo="archivo" data-action="<?php echo JrAplicacion::getJrUrl(array('persona_record', 'subir_archivo'));?>?arc=archivo&tipo=doc" type="file" name="fileArchivo" id="fileArchivo" class="cargarfile" data-type="doc"></span>
                 <input type="hidden" id="txtArchivo" name="txtarchivo" class="form-control" required />
                 <iframe name="if_cargar_file" style="display:none"></iframe>
                                  
              </div>
            </div>

              <input type="hidden" id="txtFecharegistro" name="txtFecharegistro" value="<?php echo !empty($frm["fecharegistro"])?$frm["fecharegistro"]:date("Y/m/d H:i:s"); ?>">

              <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtMostrar">
              <?php echo JrTexto::_('Mostrar');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <select id="txtmostrar" name="txtMostrar" class="form-control col-md-7 col-xs-12" required>
              <option value=""><?php JrTexto::_("Seleccione");?></option>
                              <option value="1"><?php echo JrTexto::_('Activo');?></option>
              <option value="0"><?php echo JrTexto::_('Inactivo');?></option>
                                   
              </select>
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-savePersona_record" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('persona_record'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'persona_record', 'savePersona_record', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Persona_record"))?>');
      }
     }
  });

 
  
  $('.cargarfile').on('change',function(){
    var file=$(this);
     agregar_msj_interno('success', '<?php echo JrTexto::_("loading");?> '+file.attr('data-texto')+'...','msj-interno',true);
     $('#frmPersona_record').attr('action',file.attr('data-action'));
     $('#frmPersona_record').attr('target','if_cargar_'+file.attr('data-campo'));
     $('#frmPersona_record').attr('enctype','multipart/form-data');
     $('#frmPersona_record').submit();                
  });
    
});


function result_archivo(result, mensaje,txt) {
  if(result) {
    $("#txt"+txt).val(mensaje);
    agregar_msj_interno2("success", '<?php echo JrTexto::_("File updated successfully");?>',"msj-interno2",true);
    var tipo=$("#id"+txt).attr("data-type");
    if(tipo=="imagen"||tipo=="video"||tipo=="audio")
      $("#id"+txt).removeAttr("style").attr("src", "https://192.168.11.55/pvingles.local" + mensaje );
    else
      $("#id"+txt).removeAttr("style").attr("href",  "https://192.168.11.55/pvingles.local" + mensaje );
    }else{
      agregar_msj_interno2("warning", mensaje,"msj-interno2",true);
    }
}
  
</script>

