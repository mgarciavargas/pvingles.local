<?php $usuarioAct = NegSesion::getUsuario(); ?>
<style type="text/css">
	.slick-slide{
		position: relative;
	}
</style>
<div class="container">
 	<div class="row" id="todas-unidades">     		
 		<div class="row" id="levels">
            <h3 class="col-xs-12 level-title"><?php echo JrTexto::_('level'); ?> <span id="namelevel"></span></h3>
            <div class="col-xs-12 level-wrapper">
                <div class="level-list">
                   <?php if(!empty($this->niveles))
                     foreach ($this->niveles as $nivel){?>
                    <div class="item-tools rname"  data-iname="namelevel" title="<?php echo $nivel["nombre"]?>" data-idnivel="<?php echo $nivel["idnivel"]; ?>" >
                      <span>
                        <a href="javascript:void(0)" class="hvr-float level-item <?php echo $nivel["idnivel"]==$this->idnivel?'active':'';?>" >
                           <div class="level-name"   ><?php echo $nivel["nombre"]; ?></div>
                        </a>                        
                      </span>
                    </div>
                     <?php } ?>
                </div>
            </div>
        </div>
        <div class="row" id="units">
            <h3 class="col-xs-12 unit-title"><?php echo JrTexto::_('Unit'); ?><span id="nameunit"></span></h3>
            <div class="col-xs-12 unit-wrapper">
                <div class="unit-list">
                    <?php 
                    $i=0;
                    if(!empty($this->unidades))
                     foreach ($this->unidades as $unidad){ $i++;?>
                    <div class="item-tools rname" data-iname="nameunit" title="<?php echo $unidad["nombre"]; ?>" data-idnivel="<?php echo $unidad["idnivel"]; ?>">
                      <span>
                        <a href="javascript:void(0)" class="hvr-float unit-item <?php echo $unidad["idnivel"]==$this->idunidad?'active':'';?> ">
                            <div class="unit-name"><?php echo JrTexto::_('Unit').''; ?></div>
                            <div class="unit-name"><?php echo str_pad($i,2,"0",STR_PAD_LEFT); ?></div>
                        </a>                        
                      </span>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row" id="lessons">
            <h3 class="col-xs-12 lesson-title"><?php echo ucfirst(JrTexto::_('Activity')); ?><span id="namelesson"></span></h3>
            <div class="col-xs-12 lesson-wrapper">
                <div class="lesson-list">
                    <?php $j=0;
                    if(!empty($this->actividades))
                     foreach ($this->actividades as $sesion){ $j++?>
                    <div class="item-tools rname" data-iname="namelesson" title="<?php echo $sesion["nombre"]; ?>" data-idnivel="<?php echo $sesion["idnivel"]; ?>">
                    	<span>
                        	<a href="javascript:void(0) " class="lesson-item hvr-float">
                           	 <div class="lesson-name" ><?php echo ucfirst(JrTexto::_('Activity')); ?></div>
                            	<div class="lesson-name"><?php echo str_pad($j,2,"0",STR_PAD_LEFT); ?></div>
                        	</a>
                         <?php                          
                         if($sesion["idpersonal"]==$usuarioAct["dni"]){ ?> 
                          <div class="toolbottom hide">
                            <span class="btn-group-vertical text-center">
                              <span class="btn btn-xs btneditnivel"  title="<?php echo JrTexto::_('Edit'); ?>"><i class="fa fa-edit"></i></span>
                              <span class="btn btn-xs btndeletenivel" title="<?php echo JrTexto::_('Delete'); ?>"><i class="fa fa-trash"></i></span>
                            </span>
                          </div>
                        <?php } ?>
                        </span>
                    </div>
                    <?php }if(NegSesion::tiene_acceso('Actividad', 'add')){ ?>		 
                    <div class="item-tools item-add rname" data-addhtml="#a1" data-toggle="popover" data-iname="namelesson" title="<?php echo ucfirst(JrTexto::_('Add')).' '.JrTexto::_('Activity'); ?>">
                    	<span>
	                        <a href="javascript:void(0)" class="lesson-add"  >
	                            <div class="lesson-name" style="font-size: 2em"><br><?php echo ucfirst(JrTexto::_('Add')); ?><br><i class="fa fa-plus-circle"></i></div>
	                        </a>
                      </span>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
 	</div>    	
</div>

<div class="hidden" id="a1">
    	<div class="form-group">       
	    	<label class="control-label" for="txtNombre"><?php echo ucfirst(JrTexto::_('Activity'));?> <span class="required"> * :</span></label>
        <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>" placeholder="<?php echo JrTexto::_('Type the name of the activity here')?>"> <br>
        <div>
        <button class="btn btn-cancel cerrarmodal pull-left"></span><?php echo JrTexto::_('Cancel');?></span> <i class="fa fa-close"></i></button>
        <button class="btn btn-success savenewactividad pull-right"><span><?php echo JrTexto::_('Save and add exercise');?></span> <i class="fa fa-text-o"></i></button> 
        <button class="btn btn-primary savenewactividad pull-right"><span><?php echo JrTexto::_('Save');?></span> <i class="fa fa-save"></i></button>                  
        <div class="clearfix"></div>
        </div>
    	</div>
      <div class="clearfix"></div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
    var niveles=function(data){
     	try{
     		var res = xajax__('', 'niveles', 'GetxPadre', data);
        	if(res){ return res; }
        	return false;
     	}catch(error){
     		console.log(error);
     		return false;
     	}     	
     }
    var initseleccion=function(){
    	$('a.active').each(function(){
	        var dt=$(this).closest('.item-tools').data('iname');
	        var name=$(this).closest('.item-tools').attr('title');
	        $('#'+dt).text(' : '+name);
    	});
    };
    var cargaractividaddes=function(data,txt){
		if(data!=false){
	  		var i=0;
	  		$('.'+txt+'-list').slick('unslick');
	  		$('.'+txt+'-list').html('').hide('fast');
	  		var html='';
	  		var idselected='';
	  		var txt_=(txt=='unit'?'<?php echo JrTexto::_('Unit'); ?>':'<?php echo JrTexto::_('Activity'); ?>');
        var dni='<?php echo $usuarioAct["dni"]; ?>';
	  		$.each(data,function(oi,obj){
	  			i++;
	  			idselected=idselected||obj.idnivel;	
	  			html+='<div class="item-tools rname" data-iname="name'+txt+'" title="'+obj.nombre+'"  data-idnivel="'+obj.idnivel+'"><span >'
                        +'<a href="javascript:void(0) " class=" hvr-float '+txt+'-item '+(i==1?'active':'')+'">'
                            +'<div class="'+txt+'-name">'+txt_+'</div>'
                            +'<div class="'+txt+'-name">'+(String("00" + i).slice(-2))+'</div>'
                        +'</a>';
                 if(txt=='lesson'&&dni==obj.idpersonal){       
                        html+='<div class="toolbottom hide ">'
							+'<span class="btn-group-vertical text-center">'						 
							  +'<span class="btn btn-xs btneditnivel" title="<?php echo JrTexto::_('Edit'); ?>"><i class="fa fa-edit"></i></span>'
							  +'<span class="btn btn-xs btndeletenivel" title="<?php echo JrTexto::_('Delete'); ?>"><i class="fa fa-trash"></i></span>'
							+'</span>'
						+'</div></span>'
                    }
                   html+='</div>';
	  		});
        <?php if(NegSesion::tiene_acceso('Actividad', 'add')){ ?>
	  		if(txt=='lesson'){
	  			html+='<div class="item-tools item-add rname" data-iname="namelesson" title="<?php echo ucfirst(JrTexto::_('Add'))." ".JrTexto::_('Activity'); ?>">'
	  					+'<span><a href="javascript:void(0)" class="lesson-add  hvr-float">'
                            +'<div class="lesson-name" style="font-size: 1.8em"><br><?php echo ucfirst(JrTexto::_('Add')); ?><br><i class="fa fa-plus-circle"></i></div>'
                        +'</a></span>'
                    +'</div>';
	  		}
        <?php } ?>
	  		$('.'+txt+'-list').html(html).show('fast');
	  		$('.'+txt+'-list').slick(optionslike);
	  		return idselected;	  		
	  }
	}

  $('#todas-unidades').on("click",'.btneditnivel',function(ev){
    ev.preventDefault();
    ev.stopPropagation();
    var data={
      titulo:$(this).attr('title')+' <?php echo JrTexto::_('Activity'); ?>',
      html:$('#a1').html(),
      ventanaid:'itemedit'
    }
    var modal=sysmodal(data);
    var txt=$(this).closest('.item-tools').attr('title');
    var idactividad=$(this).closest('.item-tools').data('idnivel');
    modal.find('#txtNombre').val(txt.trim());
    modal.find('.savenewactividad span').text('<?php echo JrTexto::_('Update').' '.JrTexto::_('and').' '.JrTexto::_('continue');?>');
    modal.on('click','.savenewactividad',function(){
      var idnivel=$('.level-list a.active').closest('.item-tools').data('idnivel');
      var idunidad=$('.unit-list a.active').closest('.item-tools').data('idnivel');   
      var frmdata={
          pkIdnivel:idactividad,
          txtNombre:$(this).closest('#modalcontent').find('#txtNombre').val(),
          txtTipo:'L',
          txtIdpadre:idunidad,
          txtEstado:0,
          accion:'Edit'
        } 
      var res = xajax__('', 'Niveles', 'saveNiveles', frmdata);
      if(res){
        redir(_sysUrlBase_+'/actividad/agregar2/?idnivel='+idnivel+'&txtUnidad='+idunidad+'&txtsesion='+res);
      }
    });
  });

  $('#todas-unidades').on("click",'.btndeletenivel',function(ev){
    ev.preventDefault();
    ev.stopPropagation();
    var idactividad=$(this).closest('.item-tools').data('idnivel');
    var res = xajax__('', 'Niveles', 'Eliminar', idactividad);
    if(res){
      var ia=$(this).closest('.item-tools').data('slick-index');
      $('.lesson-list').slick('slickRemove',ia);
    }

  });

  $('#todas-unidades').on("click",'.item-add',function(){
    var data={
      titulo:$(this).attr('title'),
      html:$('#a1').html(),
      ventanaid:'additem'
    }
    var modal=sysmodal(data);
    modal.on('click','.savenewactividad',function(){
      var idnivel=$('.level-list a.active').closest('.item-tools').data('idnivel');
      var idunidad=$('.unit-list a.active').closest('.item-tools').data('idnivel');   
      var frmdata={
          txtNombre:$(this).closest('#modalcontent').find('#txtNombre').val(),
          txtTipo:'L',
          txtIdpadre:idunidad,
          txtEstado:0,
          accion:'New'
        } 
      var res = xajax__('', 'Niveles', 'saveNiveles', frmdata);
      if(res){
        redir(_sysUrlBase_+'/actividad/agregar2/?idnivel='+idnivel+'&txtUnidad='+idunidad+'&txtsesion='+res);
      }
    });
  });



	$('#todas-unidades').on("click",'.item-tools[data-iname="namelevel"]',function(ev){
	  	var aobj=$(this).closest('.level-list').find('a');
	  	$(aobj).removeClass('active');
	  	$(this).find('a').addClass('active');
	  	var idnivel=$(this).data('idnivel');
	  	var data={tipo:'U','idpadre':idnivel}
	  	var unidades=niveles(data);
	  	if(unidades!=false){
	  		var idunidad=cargaractividaddes(unidades,'unit'); 	
	  		var data={tipo:'L','idpadre':idunidad}
	  		cargaractividaddes(niveles(data),'lesson');
	  		initseleccion();
	  	}
	}).on("click",'.item-tools[data-iname="nameunit"]',function(ev){
	  	var aobj=$(this).closest('.unit-list').find('a');
	  	$(aobj).removeClass('active');
	  	$(this).find('a').addClass('active');
	  	var idnivel=$(this).data('idnivel');
	  	var data={tipo:'L','idpadre':idnivel}
	  	cargaractividaddes(niveles(data),'lesson');	 
	  	initseleccion(); 	
	}).on("click",'.item-tools[data-iname="namelesson"]',function(ev){
	  var aobj=$(this).closest('.lesson-list').find('a');
	  $(aobj).removeClass('active');
	  $(this).find('a').addClass('active');
	  var idnivel=$('.level-list a.active').closest('.item-tools').data('idnivel');
	  var idunidad=$('.unit-list a.active').closest('.item-tools').data('idnivel');
	  var idlesson=$(this).data('idnivel');
	  if(!$(this).hasClass('item-add'))
	  	redir(_sysUrlBase_+'/actividad/agregar2/?idnivel='+idnivel+'&txtUnidad='+idunidad+'&txtsesion='+idlesson);
	}).on("mouseover",".item-tools.rname",function(){
        var dt=$(this).data('iname');
        var name=$(this).attr('title');
        $('#'+dt).text(' : '+name);
        $(this).find('.toolbottom').removeClass('hide');
  }).on("mouseout",".item-tools.rname",function(){       
        var dt=$(this).data('iname');
        $obj=$('a.active').closest('[data-iname='+dt+']');
        var name=$obj.attr('title')||' ';
        $('#'+dt).text(' : '+name);          
        $(this).find('.toolbottom').addClass('hide');             
  });    
    initseleccion();  

  var optionslike={
 		//dots: true,
	  	infinite: false,
	  	//speed: 300,
	   	//adaptiveHeight: true
 		navigation: false,
		slidesToScroll: 1,
		centerPadding: '60px',
	  slidesToShow: 6,
	  responsive:[
        { breakpoint: 1200, settings: {slidesToShow: 5} },
        { breakpoint: 992, settings: {slidesToShow: 4 } },
        { breakpoint: 880, settings: {slidesToShow: 3 } },
        { breakpoint: 720, settings: {slidesToShow: 2 } },
        { breakpoint: 320, settings: {slidesToShow: 1 /*,arrows: false, centerPadding: '40px',*/} }	  	
	  ]
 	};
 	var slikelevel=$('.level-list').slick(optionslike);
 	var slikunidad=$('.unit-list').slick(optionslike);
 	var slickactividad=$('.lesson-list').slick(optionslike);

 });
</script>