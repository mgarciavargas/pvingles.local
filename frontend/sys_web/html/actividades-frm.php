<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Actividades'); ?><small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdactividad" id="pkidactividad" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNivel">
              <?php echo JrTexto::_('Nivel');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtNivel" name="txtNivel" class="form-control"> 
                <?php
				      $selidpadre=$this->frmaccion=='New'?$_GET["idnivel"]:$this->idnivel;
          		foreach ($this->niveles as $nivel) { ?>
                <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $selidpadre===$nivel["idnivel"]?'selected="selected"':''?>><?php echo $nivel["nombre"]?> </option>
                <?php } ?>
                </select>                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtUnidad">
              <?php echo JrTexto::_('Unidad');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtunidad" name="txtunidad" class="form-control">
              <?php 
          $selidpadre=$this->frmaccion=='New'?$_GET["idunidad"]:$this->idunidad;
          foreach ($this->unidades as $unit) { ?>
            <option value="<?php echo $unit["idnivel"]; ?>" <?php echo $selidpadre===$unit["idnivel"]?'selected="selected"':''?>> <?php echo $unit["nombre"]?> </option>
          <?php } ?>
			</select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtSesion">
              <?php echo JrTexto::_('Sesion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtsesion" name="txtSesion" class="form-control">
              <?php
               $selidpadre1=$this->frmaccion=='New'?$_GET["idsesion"]:$this->idsesion;
          		foreach ($this->sesiones as $ses) { ?>
            	<option value="<?php echo $ses["idnivel"]; ?>" <?php echo $selidpadre1===$ses["idnivel"]?'selected="selected"':''?>> <?php echo $ses["nombre"]?> </option>
          		<?php } ?>
                </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtMetodologia">
              <?php echo JrTexto::_('Metodologia');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtmetodologia" name="txtMetodologia" class="form-control">
              <?php
               $selidpadre2=$this->frmaccion=='New'?$_GET["idmeto"]:$this->idmeto;
          		foreach ($this->metodologias as $meto) { ?>
            	<option value="<?php echo $meto["idmetodologia"]; ?>" <?php echo $selidpadre2===$meto["idmetodologia"]?'selected="selected"':''?>> <?php echo $meto["nombre"]?> </option>
          		<?php } ?>
                </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtHabilidad">
              <?php echo JrTexto::_('Habilidad');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txthabilidad" name="txtHabilidad" class="form-control" multiple="multiple">
                <?php
               $selidpadre3=$this->frmaccion=='New'?$_GET["idhabi"]:$this->idhabi;
          		foreach ($this->habilidades as $habi) { ?>
            	<option value="<?php echo $habi["idmetodologia"]; ?>" <?php echo $selidpadre3===$habi["idmetodologia"]?'selected="selected"':''?>> <?php echo $habi["nombre"]?> </option>
          		<?php } ?>
                </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTitulo">
              <?php echo JrTexto::_('Titulo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTitulo" name="txtTitulo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["titulo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDescripcion">
              <?php echo JrTexto::_('Descripcion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDescripcion" name="txtDescripcion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["descripcion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 " style="text-align: left;">
              <a href="javascript:;"  class="btn-chkoption" > <i class="fa fa<?php echo !empty($frm["estado"])?"-check":""; ?>-circle-o fa-lg"></i>  <span><?php echo @$frm["estado"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></span>
               <input type="hidden" name="txtEstado" value="<?php echo @$frm["estado"];?>" >
              </a>                                
              </div>
            </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveActividades" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('actividades'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<audio id="curaudio" dsec="1" style="display: none;"></audio>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
		 //alert("as");
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Actividades', 'saveActividades', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Actividades"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Actividades"))?>');
        <?php endif;?>       }
     }
  });


$('#txtNivel').change(function(){
    var idnivel=$(this).val();
    $('#txtunidad option').remove();
    var data={tipo:'U','idpadre':idnivel}
    var res = xajax__('', 'niveles', 'getxPadre',data);
    if(res){      //$('#txtUnidad').append('<option value=-1>'+x["nombre"]+'</option>');
      $.each(res,function(){
        x=this;
        $('#txtunidad').append('<option value='+x["idnivel"]+'>'+x["nombre"]+'</option>');
      });
       $( "#filtros" ).submit();
    }
  });
  
$('#txtunidad').change(function(){
    var idunidad=$(this).val();
    $('#txtsesion option').remove();
    var data={tipo:'L','idpadre':idunidad}
    var res = xajax__('', 'niveles', 'getxPadre',data);
    if(res){      //$('#txtUnidad').append('<option value=-1>'+x["nombre"]+'</option>');
      $.each(res,function(){
        x=this;
        $('#txtsesion').append('<option value='+x["idnivel"]+'>'+x["nombre"]+'</option>');
      });
       $( "#filtros" ).submit();
    }
  });


$('.btn-chkoption').bind({
    click: function() {     
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $("i",this).removeClass().addClass('fa fa'+(data==1?'-check-':'-')+'circle-o');
      $("span",this).html(data==1?'<?php echo JrTexto::_("Active"); ?>':'<?php echo JrTexto::_("Inactive"); ?>');
      $("input",this).val(data);
    }
  });
$('.btn-close').click(function(e){
  e.preventDefault();
  return redir('<?php echo JrAplicacion::getJrUrl(array("unidad"))."?txtNivel=".@$_GET["idnivel"];?>');
});


});
</script>