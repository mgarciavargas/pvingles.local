
<?php 
$pk=$this->pk;
$reg=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Ver Examenes'); ?></h2>
        <div class="btn-group btn-group-md" style="float:right">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('examenes'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir a listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idexamen"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idexamen"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>            
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-md-12">
        <div id="msj-interno">
        <table class="table table-striped">
         <tr>
              <th><?php echo JrTexto::_("Idexamen") ;?> </th>
              <th>:</th> 
          <td><?php echo $reg["idexamen"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idnivel") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idnivel"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idunidad") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idunidad"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idactividad") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idactividad"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Titulo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["titulo"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Descipcion") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["descipcion"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Portada") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["portada"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Fuente") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["fuente"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Fuentesize") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["fuentesize"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Tipo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["tipo"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Grupo") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo substr($reg["grupo"],0,1000)."..."; ?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Aleatroio") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["aleatroio"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Calificacion por") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><a style="cursor:pointer;" class="chklistado fa  <?php echo $reg["calificacion_por"]=="1"?"fa-check-circle":"fa-circle-o"; ?> fa-lg" data-value="<?php echo $reg["calificacion_por"]; ?>"
              data-campo="calificacion_por" data-txtid="<?php echo $reg["idexamen"]; ?>" data-value2="<?php echo $reg["calificacion_por"]==1?0:1; ?>" >
              <?php echo $reg["calificacion_por"]=="1"?JrTexto::_("Activo"):JrTexto::_("Inactivo"); ?></a></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Calificacion en") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><a style="cursor:pointer;" class="chklistado fa  <?php echo $reg["calificacion_en"]=="1"?"fa-check-circle":"fa-circle-o"; ?> fa-lg" data-value="<?php echo $reg["calificacion_en"]; ?>"
              data-campo="calificacion_en" data-txtid="<?php echo $reg["idexamen"]; ?>" data-value2="<?php echo $reg["calificacion_en"]==1?0:1; ?>" >
              <?php echo $reg["calificacion_en"]=="1"?JrTexto::_("Activo"):JrTexto::_("Inactivo"); ?></a></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Calificacion total") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><a style="cursor:pointer;" class="chklistado fa  <?php echo $reg["calificacion_total"]=="1"?"fa-check-circle":"fa-circle-o"; ?> fa-lg" data-value="<?php echo $reg["calificacion_total"]; ?>"
              data-campo="calificacion_total" data-txtid="<?php echo $reg["idexamen"]; ?>" data-value2="<?php echo $reg["calificacion_total"]==1?0:1; ?>" >
              <?php echo $reg["calificacion_total"]=="1"?JrTexto::_("Activo"):JrTexto::_("Inactivo"); ?></a></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Calificacion min") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><a style="cursor:pointer;" class="chklistado fa  <?php echo $reg["calificacion_min"]=="1"?"fa-check-circle":"fa-circle-o"; ?> fa-lg" data-value="<?php echo $reg["calificacion_min"]; ?>"
              data-campo="calificacion_min" data-txtid="<?php echo $reg["idexamen"]; ?>" data-value2="<?php echo $reg["calificacion_min"]==1?0:1; ?>" >
              <?php echo $reg["calificacion_min"]=="1"?JrTexto::_("Activo"):JrTexto::_("Inactivo"); ?></a></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Tiempo por") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><a style="cursor:pointer;" class="chklistado fa  <?php echo $reg["tiempo_por"]=="1"?"fa-check-circle":"fa-circle-o"; ?> fa-lg" data-value="<?php echo $reg["tiempo_por"]; ?>"
              data-campo="tiempo_por" data-txtid="<?php echo $reg["idexamen"]; ?>" data-value2="<?php echo $reg["tiempo_por"]==1?0:1; ?>" >
              <?php echo $reg["tiempo_por"]=="1"?JrTexto::_("Activo"):JrTexto::_("Inactivo"); ?></a></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Tiempo total") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["tiempo_total"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Estado") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><a style="cursor:pointer;" class="chklistado fa  <?php echo $reg["estado"]=="1"?"fa-check-circle":"fa-circle-o"; ?> fa-lg" data-value="<?php echo $reg["estado"]; ?>"
              data-campo="estado" data-txtid="<?php echo $reg["idexamen"]; ?>" data-value2="<?php echo $reg["estado"]==1?0:1; ?>" >
              <?php echo $reg["estado"]=="1"?JrTexto::_("Activo"):JrTexto::_("Inactivo"); ?></a></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Idpersonal") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["idpersonal"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Fecharegistro") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><?php echo $reg["fecharegistro"] ;?></td>
          </tr>
                    </table>
          </div>
          <hr><div class="text-center">
          <div class="btn-group btn-group-md">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('examenes'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir al listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["idexamen"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["idexamen"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>
	        </div>         
	    </div>
</div>
<br />
	</div>
	<script type="text/javascript">
  var asInitVals = new Array();
	$(document).ready(function(){
        $('.chklistado').click(function(){
        var id= $(this).attr('data-txtid');
        var campo_= $(this).attr('data-campo');
        var v1=$(this).attr('data-value');
        var v2=$(this).attr('data-value2');
        var data={pk:id,campo:campo_,value:v2};
        var res = xajax__('', 'Examenes', 'saveEstatus',data);
        if(res) {
          $(this).attr('data-value',v2);
          $(this).attr('data-value2',v1);
          $(this).removeClass('fa-circle-o').removeClass('fa-check-circle');
          $(this).addClass(('0' == v2) ? ' fa-circle-o ' : ' fa-check-circle ');
          $(this).text(('0' == v2) ? v2 : v1);
          agregar_msj_interno('success', '<?php echo JrTexto::_("Información actualizada");?>');
          $('.alert').fadeOut(4000);
        }
    });
    
    $('.btnNuevo').click(function(){
      $('#pkaccion').val('guardar');
      $('#pkidexamen').val('');
      $('.img-thumbnail').attr("style","display:none");
    });

    $('.editar').click(function(){
      $('.img-thumbnail').removeAttr("style");
      var id=$(this).attr("data-reg");
          addFancyAjax("<?php echo JrAplicacion::getJrUrl(array('Examenes', 'frm'))?>?tpl=b&acc=Editar&id="+id, true);
          
    });

    $('.eliminar').click(function(){
     if(confirm("<?php echo JrTexto::_('¿Desea eliminar el registro seleccionado?');?>")) {
        var id=$(this).attr("data-reg");
        var res = xajax__('', 'Examenes', 'eliminarExamenes',id)
        if(res) {
          agregar_msj_interno('success', "<?php echo JrTexto::_('Registro eliminado');?>");
                    recargarpagina(false);
                    } 
        $('.alert').fadeOut(4000);
      }
    });
	});
</script>