<style type="text/css">
    .div_menu{
        height: 120px; font-size: 20px; text-align: center;
        padding: 40px;
    }
    .color1{
        background-color: #EB6B56;
    }
    .color2{
        background-color: #2C82C9;
    }
    .color3{
        background-color: #9365B8;
    }
    .color4{
        background-color: #FAC51C;
    }

    .color5{
        background-color: #61BD6D;
    }
    
</style>
<?php defined("RUTA_BASE") or die(); ?>
<div class="form-view" >
  <div class="page-title">
    <div class="title_left"><h3></h3>
        <?php echo JrTexto::_('Report');?>        
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="div_linea"></div>
  <div class="row">

    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
         <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/repor_tiempoe" style="color: #fff;">
         <div class="color1 div_menu x_content ">
            <?php echo JrTexto::_("Tiempo de Estudio en la PV"); ?>
         </div>
         </a>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
         <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/repor_examene" style="color: #fff;">
         <div class="color2 div_menu x_content">
            <?php echo JrTexto::_("Exámenes de entrada"); ?>
         </div>
         </a>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
         <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/repor_examens" style="color: #fff;">
         <div class="color3 div_menu x_content" >
            <?php echo JrTexto::_("Exámenes de Salida"); ?>
         </div>
         </a>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
         <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/repor_productividad" style="color: #fff;">
         <div class="color4 div_menu x_content" >
            <?php echo JrTexto::_("Reporte de Productividad"); ?>
         </div>
         </a>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
         <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/repor_progreso" style="color: #fff;">
         <div class="color5 div_menu x_content" >
            <?php echo JrTexto::_("Progreso del Estudiante"); ?>
         </div>
         </a>
    </div>


    <!--div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/Unidad"><?php echo JrTexto::_("Unit"); ?></a>
         </div>       
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/leccion"><?php echo JrTexto::_("Activities"); ?></a>
         </div>       
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/metodologia"><?php echo JrTexto::_("Methodology"); ?></a>
         </div>       
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/habilidad"><?php echo JrTexto::_("Ability"); ?></a>
         </div>       
    </div> 
   
     <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/actividad/agregar"><?php echo JrTexto::_("Add Activities"); ?></a>
         </div>       
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/usuario"><?php echo JrTexto::_("My profile"); ?></a>
         </div>       
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/roles"><?php echo JrTexto::_("Roles"); ?></a>
         </div>       
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/menu"><?php echo JrTexto::_("Menus"); ?></a>
         </div>       
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/permisos"><?php echo JrTexto::_("Permisos"); ?></a>
         </div>       
    </div>
    
    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/ugel"><?php echo JrTexto::_("Ugel"); ?></a>
         </div>       
    </div>

    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/local"><?php echo JrTexto::_("Local"); ?></a>
         </div>       
    </div>

    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/ambiente"><?php echo JrTexto::_("Ambiente"); ?></a>
         </div>       
    </div>
    
    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/generador"><?php echo JrTexto::_("Generator"); ?></a>
         </div>       
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/bib_autor"><?php echo JrTexto::_("Author"); ?></a>
         </div>       
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">               
         <div class="x_content">
            <a href="<?php echo $this->documento->getUrlSitio(); ?>/bib_libro"><?php echo JrTexto::_("Book"); ?></a>
         </div>       
    </div-->
    
    
    
  </div>
</div>
<script type="text/javascript">
  
</script>