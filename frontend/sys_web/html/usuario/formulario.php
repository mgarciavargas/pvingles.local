<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
$rol=!empty($_REQUEST["rol"])?$_REQUEST["rol"]:"";
$persona=!empty($this->datos)?$this->datos:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .slick-items<?php echo $idgui; ?> .slick-item>.btn{ width: 100%;}
  .slick-items<?php echo $idgui; ?> .slick-item>.btn.btn-panel.active{ opacity: 1 }
  .slick-items<?php echo $idgui; ?> .slick-item>.btn.btn-panel.active:after{
    content: "";
    width: 0px;
    height: 0px;
    border-left: 15px solid transparent;
    border-right: 15px solid transparent;
    border-bottom: 15px solid #fbf7f5;
    font-size: 0px;
    line-height: 0px;
    position: absolute;
    left: 40%;
    bottom: -7px;
  }
  .slick-slider{ margin-bottom: 0.25ex !important; }
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/personal">&nbsp;<?php echo JrTexto::_("Personal"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Edit"); ?></li>       
    </ol>
  </div>
</div>
<?php } ?>
<div class="row" >
	<div class="col-md-12" style="<?php echo $this->isalumno||$this->isdocente?'':'display: none';  ?>">
	   <div class="slick-items<?php echo $idgui; ?>" >
	       <div class="slick-item">
	        <a href="javascript:void(0)" data-tab="informacion" class="btn btn-primary btn-panel active ">
	            <i class="btn-icon fa fa-user"></i> <span><?php echo JrTexto::_('General information'); ?></span>
	          </a>
	       </div>
	       <div class="slick-item">
	          <a href="javascript:void(0)" data-tab="grupoestudio" class="btn btn-primary btn-panel">
	            <i class="btn-icon fa fa-users"></i> <span><?php echo JrTexto::_('Study Group'); ?></span>
	          </a>
	       </div>
	       <div class="slick-item">
	          <a href="javascript:void(0)" data-tab="cursos" class="btn btn-primary btn-panel">
	            <span class="btn-icon glyphicon glyphicon-book"></span> <span><?php echo JrTexto::_('Courses'); ?></span>
	          </a>
	       </div>
	       <div class="slick-item">
	          <a href="javascript:void(0)" data-tab="notas" class="btn btn-primary btn-panel">
	            <span class="btn-icon glyphicon glyphicon-list-alt"></span> <span><?php echo JrTexto::_('Qualifications'); ?></span>
	          </a>
	       </div>
	       <div class="slick-item">
	          <a href="javascript:void(0)" data-tab="horarios" class="btn btn-primary btn-panel">
	            <i class="btn-icon fa fa-calendar-check-o"></i> <span><?php echo JrTexto::_('Schedule'); ?></span>
	          </a>
	       </div>
	       <div class="slick-item">
	        <a href="javascript:void(0)" data-tab="historial" class="btn btn-primary btn-panel">
	          <i class="btn-icon fa fa-hourglass-half"></i> <span><?php echo JrTexto::_('Historial'); ?></span>
	        </a>
	       </div>
	       <div class="slick-item">
	          <a href="javascript:void(0)" data-tab="asistencias" class="btn btn-primary btn-panel">
	            <i class="btn-icon fa fa-pencil-square-o"></i> <span><?php echo JrTexto::_('Attendance'); ?></span>
	          </a>
	       </div>
	   </div>
	</div>
  <input type="hidden" class="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$persona["idpersona"] ?>">
  <input type="hidden" class="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion ?>">
	<div class="col-md-12" id="pnlvista<?php echo $idgui; ?>">
		
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
    <?php if($this->isalumno||$this->isdocente){?>
    var optionslike<?php echo $idgui; ?>={
            infinite: false,
            navigation: false,
            slidesToScroll: 1,
            centerPadding: '60px',
            slidesToShow: 5,
            responsive:[
                { breakpoint: 1200,settings: {slidesToShow: 5} },
                { breakpoint: 992, settings: {slidesToShow: 4} },
                { breakpoint: 880, settings: {slidesToShow: 3} },
                { breakpoint: 720, settings: {slidesToShow: 2} },
                { breakpoint: 320, settings: {slidesToShow: 1 /*,arrows: false, centerPadding: '40px',*/} }       
            ]
          };
    setTimeout(function(){$('.slick-items<?php echo $idgui; ?>').slick(optionslike<?php echo $idgui; ?>);},350);
    <?php } ?>
    var cargarvista<?php echo $idgui; ?>=function(view){
        console.log(view);
        var _vista=view||'';
        var formData = new FormData(); 
        formData.append('idpersona',$('#idpersona<?php echo $idgui; ?>').val());
        formData.append('accion',$('#accion<?php echo $idgui; ?>').val());
        formData.append('vista',_vista);
        formData.append('plt','blanco');
        formData.append('datareturn','<?php echo $datareturn; ?>');
        formData.append('rol','<?php echo $rol; ?>');
        formData.append('fcall','<?php echo $fcall; ?>')
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/personal/'+view,
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'html',
          callback:function(data){
            $('#pnlvista<?php echo $idgui; ?>').html(data);       
          }
        }
        sysajax(data);
    }  
    var curtab<?php echo $idgui; ?>='';
    $('.slick-items<?php echo $idgui; ?>').on('click','.btn-panel',function(ev){
    	var tab=$(this).attr('data-tab');
    	if(tab==curtab<?php echo $idgui; ?>) return;
    	$(this).closest('.slick-items<?php echo $idgui; ?>').find('a').removeClass('active');
    	$(this).addClass('active');
    	curtab<?php echo $idgui; ?>=tab;
    	cargarvista<?php echo $idgui; ?>(tab);
    });

    $('.slick-items<?php echo $idgui; ?> .slick-item:first-child a').trigger('click');
});
</script>