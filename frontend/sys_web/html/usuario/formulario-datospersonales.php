<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:false;
$frm=!empty($this->datos)?$this->datos:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'user_avatar.jpg';
$rol=!empty($_REQUEST["rol"])?$_REQUEST["rol"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .toolbarmouse{
    position: absolute;
    right: 0px;
    top: 0px;
    background: rgba(255, 255, 253, 0.72);
  }
  .input-file-invisible{
    position: absolute;
    top: 0px;
  }
</style>
<div class="col-md-12">
<div class="panel panel-body">
  <form class="" id="frm<?php echo $idgui; ?>"  target="" enctype="multipart/form-data">
    <input type="hidden" name="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$frm["idpersona"]; ?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion; ?>">
    <input type="hidden" name="rol" id="rol<?php echo $idgui; ?>" value="<?php echo @$rol; ?>">
    <div class="row">
      <div class="col-xs-12 col-sm-9 col-md-9">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Document type")); ?></label>
            <div class="cajaselect">
              <select name="tipodoc" id="tipodoc<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fktipodoc)) foreach ($this->fktipodoc as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>"  >
                    <?php echo $fk["nombre"] ?>
                    </option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left">N° <?php echo ucfirst(JrTexto::_('Id card')); ?></label>                    
              <input type="text" class="form-control" name="dni" id="tmpdni<?php echo $idgui; ?>" required="required" value="<?php echo @$frm["dni"]; ?>" placeholder=""> 
          </div>  
        </div>
        <?php if($datareturn!='false'){ ?>
        <div class="col-xs-12 col-sm-6 col-md-4"><br>
          <a class="btn btn-info btnsearch<?php echo $idgui ?>" href="javascript:void(0)"><i class="fa fa-search"></i> <?php echo JrTexto::_('Search'); ?></a>          
        </div> 
        <?php }?>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Names')); ?></label>                    
              <input type="text" class="form-control"  required="required" name="nombre" id="nombre<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["nombre"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Father's last name")); ?></label>                    
              <input type="text" class="form-control"  required="required" name="ape_paterno" id="ape_paterno<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["ape_paterno"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Mother's last name")); ?></label>                    
              <input type="text" class="form-control"  required="required" name="ape_materno" id="ape_materno<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["ape_materno"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Gender")); ?></label>
            <div class="cajaselect">
              <select name="sexo" id="sexo<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fksexo)) foreach ($this->fksexo as $fksexo) { ?>
                  <option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> >
                    <?php echo $fksexo["nombre"] ?>
                    </option>
                <?php } ?>                            
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Marital Status")); ?></label>
            <div class="cajaselect">
              <select name="estado_civil" id="estado_civil<?php echo $idgui;?>" class="form-control">
                <?php if(!empty($this->fkestado_civil)) foreach ($this->fkestado_civil as $fkestado_civil) { ?>
                  <option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> >
                    <?php echo $fkestado_civil["nombre"] ?>
                    </option>
                <?php } ?>                        
              </select>
            </div>
          </div>
        </div> 

        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Telephone')); ?></label>                    
              <input type="text" class="form-control"  required="required" name="telefono" id="telefono<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["telefono"]); ?>" > 
          </div> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Mobile number')); ?></label>                    
              <input type="text" class="form-control" name="celular" id="celular<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["celular"]); ?>" > 
          </div> 
        </div> 
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 text-center">        
          <div class="col-xs-12 col-sm-12 col-md-12 text-center contool">
            <div><label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Photo')); ?></label></div>
            <div style="position: relative;">
              <div class="toolbarmouse"><span class="btn"><i class="fa fa-pencil"></i></span></div>
              <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $fotouser; ?>" alt="foto" class="foto_alumno img-responsive center-block thumbnail" id="foto<?php echo $idgui; ?>">
              <input type="file" class="input-file-invisible" name="foto" id="fileFoto<?php echo $idgui; ?>" accept="image/*">
            </div>      
          </div>                   
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Birthday")); ?></label>
            <div class="form-group">
              <div class="input-group date datetimepicker">
                <input type="text" class="form-control" required="required" name="fechanac" id="fechanac<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["fechanac"]); ?>"> 
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
              </div>
            </div>
            </div>
          </div>
      </div>
      <div class="clearfix"></div><br>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-default btnretornar<?php echo $idgui; ?>" href="javascript:void(0)" ><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>
        <?php if($datareturn!='false'){ ?><a class="btn btn-warning btnseleccionar<?php echo $idgui ?> hide" href="javascript:void(0)"><i class="fa fa-hand-o-down"></i> <?php echo JrTexto::_('Selected'); ?></a><?php } ?>
      </div>
      <div class="clearfix"></div><br>
    </div>            
  </form>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    var changefoto=false;
    var msjatencion='<?php echo JrTexto::_('Attention');?>';
    var buscaralumno<?php echo $idgui; ?>=function(){
      var dniobj=$('#tmpdni<?php echo $idgui; ?>');
      var dni=dniobj.val();
      if(dni=='') {
         mostrar_notificacion(msjatencion,'<?php echo JrTexto::_('empty information');?>','warning');
        dniobj.focus();
        return false;
      }      
      var formData = new FormData();       
      formData.append('dni', dni); 
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/personal/buscarjson',
        msjatencion:msjatencion,
        type:'json',
        //showmsjok : true,
        callback:function(rs){
          var dt=rs.data;
          if(dt[0]!=undefined){
            var rw=dt[0];
            $('#nombre<?php echo $idgui; ?>').val(rw.nombre);
            $('#ape_paterno<?php echo $idgui; ?>').val(rw.ape_paterno);
            $('#ape_materno<?php echo $idgui; ?>').val(rw.ape_materno);
            $('#sexo<?php echo $idgui; ?>').val(rw.sexo);
            $('#telefono<?php echo $idgui; ?>').val(rw.telefono);
            $('#celular<?php echo $idgui; ?>').val(rw.celular);
            $('#fechanac<?php echo $idgui; ?>').val(rw.fechanac);
            $('#estado_civil<?php echo $idgui; ?>').val(rw.estado_civil||'S');
            if(rw.foto!=''){
              $('#foto<?php echo $idgui; ?>').attr('src',_sysUrlStatic_+'/media/usuarios/'+rw.foto);              
            }
            $('.btnseleccionar<?php echo $idgui ?>').removeClass('hide');
          }else{
            $('#frm<?php echo $idgui; ?>')[0].reset();
            $('.btnseleccionar<?php echo $idgui ?>').addClass('hide');
          }        
        }
      }
      sysajax(data);
    }
    $('.btnsearch<?php echo $idgui ?>').click(function(){
      buscaralumno<?php echo $idgui; ?>();
    });

    $('#frm<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
        //$('#pnlvista<?php //echo $idgui; ?>').html(data);
        var myForm = document.getElementById('frm<?php echo $idgui; ?>'); 
        var formData = new FormData(myForm); 
        if(changefoto==true) formData.append('fotouser', $('.input-file-invisible')[0].files[0]); 
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/personal/guardarDatos',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok : true,
          callback:function(data){
            $('input.idpersona').val(data.newid);
            <?php if(!empty($fcall)){ ?>
              $('.btnseleccionar<?php echo $idgui ?>').removeClass('hide');
            <?php } ?>       
          }
        }
        sysajax(data);
        return false;
      }
    });


    $('#fechanac<?php echo $idgui; ?>').datetimepicker({ //lang:'es',  //timepicker:false,
      format:'YYYY/MM/DD'
    }); 

    var inputFile = document.getElementById('fileFoto<?php echo $idgui; ?>');
    inputFile.addEventListener('change', mostrarImagen, false);
    function mostrarImagen(event) {
      var file=event.target.files[0];
      changefoto=true;
      var reader=new FileReader();      
      reader.onload=function(event){
        var img=document.getElementById('foto<?php echo $idgui; ?>');
        img.src=event.target.result;
      }
      reader.readAsDataURL(file);
    };

    <?php if(!empty($fcall)){ ?>
    $('.btnseleccionar<?php echo $idgui ?>').click(function(){      
        var tmpobj=$('.tmp<?php echo $fcall ?>');
        if(tmpobj.length){
            var dniobj=$('#tmpdni<?php echo $idgui; ?>');
            tmpobj.attr('data-return',dniobj.val());
            tmpobj.on('returndata').trigger('returndata');
        } 
        <?php if($this->documento->plantilla=='modal'){ ?>
        $(this).closest('.modal').modal('hide');
        <?php } ?>
    });
    <?php } ?>
    $('.btnretornar<?php echo $idgui; ?>').click(function(){
      <?php if($this->documento->plantilla=='modal'){ ?>
        $(this).closest('.modal').modal('hide');
        <?php }else{ ?>       
          window.history.back();
        <?php } ?>

    })
  });
</script>