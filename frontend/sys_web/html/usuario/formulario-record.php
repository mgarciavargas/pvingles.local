<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$frm=!empty($this->datos)?$this->datos:"";
$records=$this->records;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .toolbarmouse{
    position: absolute;
    right: 0px;
    top: 0px;
    background: rgba(255, 255, 253, 0.72);
  }
  .input-file-invisible{
    position: absolute;
    top: 0px;
  }
  .item-user{
    padding: 1ex;
    position: relative;
  }
  .pnlacciones{
    position: absolute;
    bottom: 0px;
    text-align: center;
    width: 100%;
    background: #469de8;
    /*margin: -11px;*/
    padding: 5px;
    -webkit-animation: fadein 1s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 1s; /* Firefox < 16 */
        -ms-animation: fadein 1s; /* Internet Explorer */
         -o-animation: fadein 1s; /* Opera < 12.1 */
            animation: fadein 1s;
  }
  @keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
  }
  .pnlacciones a{
    margin: 0.25ex;
    width: 30px;
  }
  .panel-user .pnlacciones{
    display: none;    
  }
  .panel-user.active .pnlacciones{
    display: block;
    top:auto !important;
  }
</style>
<div class="col-md-12">
  <div class="panel panel-body" style="display: block" id="pnla_<?php echo $idgui; ?>">
    <div id="addaqui<?php echo $idgui; ?>">
      <div class="col-md-6 item-user cls<?php echo $idgui ?> " id="item-userclone<?php echo $idgui; ?>" style="display: none;" >
        <div class="panel-user">
        <div class="row"> 
          <div class="col-md-12 text-right tiporecordclone<?php echo $idgui; ?>" style="color:#2e6d1e"></div>
          <h4 class=" col-xs-12 border-turquoise color-turquoise text-center titulo tituloclone<?php echo $idgui; ?>"><i></i><br>
            <small class="fecharegistroclone<?php echo $idgui; ?>"></small><br>
            <small class="archivoclone<?php echo $idgui; ?>"><a href="#"><?php echo JrTexto::_('download')." ".JrTexto::_('File'); ?></a></small>
          </h4>
          <div class="col-md-12">
            <div class="descripcionclone<?php echo $idgui; ?>">
              <a href="" download=""><?php echo JrTexto::_('download')." ".JrTexto::_('File'); ?></a></div>
          </div>         
        </div>
        <div class="pnlacciones text-center">
          <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/verficha/?id=<?php echo $apo["ndoc"];?>" data-titulo="<?php echo JrTexto::_('Ficha')." ".$fullnombre; ?>"> <i class="fa fa-user"></i><i class="fa fa-eye"></i></a-->
          <a class="btn btn-warning btn-xs btneditarrecord" data-modal="si" href="javascript:void(0);"  data-id="0" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Edit'); ?>"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>
          <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/cambiarclave/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('Change')." ".JrTexto::_('Password'); ?>"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>
          <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('notes');?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a>
          <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/horario/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a-->
          <a class="btneliminarrecord btn btn-danger btn-xs" href="javascript:void(0);" data-id="0" data-titulo="<?php echo JrTexto::_('Delete'); ?>"><i class="fa fa-trash-o"></i></a>
        </div>
        </div>
      </div> 
<?php 
$xuserrec=array();
if(!empty($records))
  foreach ($records as $rec){
    $url=$this->documento->getUrlBase();
    $xuserrec[$rec["idrecord"]]=$rec; ?>
  <div class="col-md-6 item-user cls<?php echo $idgui ?> " id="item-user<?php echo $rec["idrecord"]; ?>" >
    <div class="panel-user" style="<?php echo $rec["tiporecord"]==1?'border:1.5px solid #4eca30':'border:1.5px solid #F30'; ?>">
      <div class="row"> 
        <div class="col-md-12 text-right tiporecord<?php echo $rec["idrecord"]; ?>" data-tiporecord="<?php echo $rec["tiporecord"];?>" style="<?php echo $rec["tiporecord"]==1?'color:#2e6d1e':'color:#F30'; ?>"> <?php echo ucfirst(JrTexto::_($rec["strtiporecord"])); ?></div>     
        <h4 class=" col-xs-12 border-turquoise color-turquoise text-center titulo titulo<?php echo $rec["idrecord"]; ?>"><i><?php echo ucfirst($rec["titulo"]); ?></i><br>
          <small class="fecharegistro<?php echo $rec["idrecord"]; ?>"><?php echo @$rec["fecharegistro"]; ?></small><br>
          <small class="archivo<?php echo $rec["idrecord"]; ?>">
            <a href="<?php echo $this->documento->getUrlBase()."/".$rec["archivo"]; ?>" download=""><?php echo JrTexto::_('download')." ".JrTexto::_('File'); ?></a>
            </small></h4>
          <div class="col-md-12">
          <div class="descripcion<?php echo $rec["idrecord"]; ?>"><?php echo substr(@$rec["descripcion"],0,250); ?></div>
        </div>
      </div>
      <div class="pnlacciones text-center">
        <a class="btn btn-warning btn-xs btneditarrecord" data-modal="si" href="javascript:void(0);"  data-id="<?php echo $rec["idrecord"];?>" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Edit'); ?>"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>        
        <a class="btneliminarrecord btn btn-danger btn-xs" href="javascript:void(0);" data-id="<?php echo $rec["idrecord"];?>" data-titulo="<?php echo JrTexto::_('delete'); ?>"><i class="fa fa-trash-o"></i></a>
      </div>
  </div>
</div>
<?php } ?>
</div>
<div class="clearfix"></div>
<div class="col-md-12 text-center">
  <!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a-->
  <button class="btn btn-warning btnaddrec<?php echo $idgui ?>"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('Add'));?></button></div>  
</div>
</div>
<div class="col-md-12">
<div class="panel panel-body" style="display: none;" id="pnlb_<?php echo $idgui; ?>">
  <form class="" id="frm<?php echo $idgui; ?>"  target="" enctype="multipart/form-data">
    <input type="hidden" class="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$frm["idpersona"] ?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion ?>">
    <input type="hidden" name="accionrec" id="accionrec<?php echo $idgui; ?>" value="add">
    <input type="hidden" name="idrecord" id="idrecord<?php echo $idgui; ?>" value="">
    <input type="hidden" name="mostrar" id="mostrar<?php echo $idgui; ?>" value="1">
    <input type="hidden" name="archivotmp" id="archivotmp<?php echo $idgui;?>" value="">
    <input type="hidden" name="fecharegistro" id="fecharegistro<?php echo $idgui;?>" value="">
    <div class="row">
      <div class="col-xs-12 col-sm-9 col-md-9">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Record type")); ?></label>
            <div class="cajaselect">
              <select name="tiporecord" id="tiporecord<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fktiporecord)) foreach ($this->fktiporecord as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>"  >
                    <?php echo ucfirst(JrTexto::_($fk["nombre"])); ?>
                    </option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class=" form-group">
              <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('title')); ?></label>                    
              <input type="text" class="form-control" name="titulo" id="titulo<?php echo $idgui; ?>" value="" placeholder="" required="required" > 
            </div>  
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Description")); ?></label>
            <textarea name="descripcion" class="form-control" id="descripcion<?php echo $idgui; ?>" value="" placeholder=""></textarea>  
          </div>  
        </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 text-center contool">
        <div><label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('File')); ?></label></div>
        <div style="position: relative;">
          <div class="toolbarmouse" style="width: 90%; text-align:center; right: none;"><span class="btn"><i class="fa fa-pencil"></i></span></div>
          <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/file.png" alt="archivo" class="img-responsive center-block thumbnail" id="archivo<?php echo $idgui; ?>">
          <input type="file" class="input-file-invisible" name="archivo" id="archivo<?php echo $idgui; ?>" accept="Aplication/*">
        </div>      
      </div>    
      <div class="clearfix"></div><br>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a href="javascript:void(0);" class="btn btn-danger btncancel<?php echo $idgui ?>"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Cancel');?></a>
        <button type="submit" class="btn btn-primary btnsave<?php echo $idgui ?>"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>
      </div>
      <div class="clearfix"></div><br>
    </div>            
  </form>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
  var userrec=<?php echo !empty($xuserrec)?json_encode($xuserrec):'[]' ?>;
  $('.cls<?php echo $idgui ?>').on("mouseenter",'.panel-user', function(){
    if(!$(this).hasClass('active')){ 
      $(this).siblings('.panel-user').removeClass('active');
      $(this).addClass('active');
    }
  }).on("mouseleave",'.panel-user', function(){
    $(this).removeClass('active');
  });

   $(".btnaddrec<?php echo $idgui ?>").click(function(ev){
    $('#accionrec<?php echo $idgui; ?>').val('add');
    $('#idrecord<?php echo $idgui; ?>').val('');    
    $('#titulo<?php echo $idgui; ?>').val('');
    $('#descripcion<?php echo $idgui; ?>').val('');
    $('#fecharegistro<?php echo $idgui; ?>').val(moment().format('YYYY-MM-D HH:mm:ss'));   
    $("#pnla_<?php echo $idgui; ?>").hide();
    $("#pnlb_<?php echo $idgui; ?>").show();
  });
  $('.btncancel<?php echo $idgui ?>').click(function(ev){
    $('#accionapo<?php echo $idgui; ?>').val('add');
    $('#titulo<?php echo $idgui; ?>').val('');
    $('#descripcion<?php echo $idgui; ?>').val('');
    $('#fecharegistro<?php echo $idgui; ?>').val();  
    $("#pnla_<?php echo $idgui; ?>").show();
    $("#pnlb_<?php echo $idgui; ?>").hide();
  });

  $("#pnla_<?php echo $idgui; ?>").on('click','.btneditarrecord',function(ev){
    var id=$(this).attr('data-id');
    var user=userrec!=[]?(userrec[id]!=undefined?userrec[id]:[]):[];    
    $('#idrecord<?php echo $idgui; ?>').val(user.idrecord||'');
    $('#accionrec<?php echo $idgui; ?>').val('edit');
    $('#titulo<?php echo $idgui; ?>').val(user.titulo||'');
    $('#descripcion<?php echo $idgui; ?>').val(user.descripcion||'');
    $('#tiporecord<?php echo $idgui; ?>').val(user.tiporecord||''); 
    $('#archivotmp<?php echo $idgui; ?>').val(user.archivo||'');
    $('#fecharegistro<?php echo $idgui; ?>').val(user.fecharegistro||'');  
    $("#pnla_<?php echo $idgui; ?>").hide();
    $("#pnlb_<?php echo $idgui; ?>").show();
  }).on('click','.btneliminarrecord',function(ev){
    var id=$(this).attr('data-id');
    var res = xajax__('', 'persona_record', 'eliminar', id);
    if(userrec[id]!=undefined) delete userrec[id];
    $(this).closest('.item-user').remove();    
  });
  var changefoto=false;
  $('#frm<?php echo $idgui;?>').bind({ 
    keyup:function(ev){
      if(ev.which == 13) return false;
    },submit: function(event){
      var idguitmp='<?php echo $idgui; ?>';      
      var myForm = document.getElementById('frm<?php echo $idgui; ?>'); 
      var formData = new FormData(myForm);
      var accion=$('#accionrec'+idguitmp).val();
      formData.append('accion',accion);
      formData.append('idpersona', $('#idpersona'+idguitmp).val()); 
      formData.append('idrecord', $('#idrecord'+idguitmp).val()); 
      formData.append('plt','blanco');
      var url=_sysUrlBase_+'/persona_record/guardarPersona_record';     
      $.ajax({
        url: url,
        type: "POST",
        data:  formData,
        contentType: false,
        processData: false,
        cache: false,
        dataType:'json',
        beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
        success: function(data){  
          if(data.code=='Error'){
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
          }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
            var _id=data.newid; 
            var archivo=data.archivo;
            var usertmp={
              idrecord:_id,
              idpersona:$('#idpersona'+idguitmp).val(),
              titulo:$('#titulo'+idguitmp).val(),
              descripcion:$('#descripcion'+idguitmp).val(),              
              tiporecord:$('#tiporecord'+idguitmp).val(),
              archivotmp:archivo,
              fecharegistro:$('#fecharegistro'+idguitmp).val(),
              tiporecord:$('#tiporecord'+idguitmp).val().trim(),
              strtiporecord:$('#tiporecord'+idguitmp+' option:selected').text().trim(),
            }  
            if(accion=='add'){
              $newuser=$('#pnla_'+idguitmp+' .item-user:first-child').clone(true,true);
              $newuser.removeAttr('style');
              $newuser.attr('id','item-user'+_id);

              var tmpid=idguitmp+_id;              
              $newuser.find('.tituloclone'+idguitmp).addClass('titulo'+_id).removeClass('tituloclone'+idguitmp);              
              $newuser.find('.titulo'+_id+' i').text(usertmp.titulo); 
              $newuser.find('.archivoclone'+idguitmp).addClass('archivo'+_id).removeClass('archivoclone'+idguitmp);          
              if(usertmp.archivotmp!='') $newuser.find('.archivo'+_id+' a').attr('href',_sysUrlBase_+'/'+usertmp.archivotmp);
              else {$newuser.find('.archivo'+_id+' a').addClass('hide');}
              $newuser.find('.descripcionclone'+idguitmp).addClass('descripcion'+_id).removeClass('descripcionclone'+idguitmp).text(usertmp.descripcion);              
              $newuser.find('.tiporecordclone'+idguitmp).addClass('tiporecord'+_id).removeClass('tiporecordclone'+idguitmp).attr('data-tiporecord',usertmp.tiporecord).text(usertmp.strtiporecord);
              $newuser.find('.tiporecord'+_id). removeAttr('style').css('color',(usertmp.tiporecord==1?'#2e6d1e':'#F30'));             
              $newuser.find('.panel-user').css('border','1.5px solid '+(usertmp.tiporecord==1?'#4eca30':'#F30'));
              $newuser.find('.fecharegistroclone'+idguitmp).addClass('fecharegistro'+_id).removeClass('fecharegistroclone'+idguitmp).text(usertmp.fecharegistro);              
              $newuser.find('.btneditarrecord').attr('data-id',_id);
              $newuser.find('.btneliminarrecord').attr('data-id',_id);
            
              userrec[_id]=usertmp;         
              $("#pnla_"+idguitmp+' #addaqui'+idguitmp).append($newuser);
            }else{             
              $edituser=$('#item-user'+_id); 
              $edituser.find('.titulo'+_id+' i').text(usertmp.titulo);
              $edituser.find('.archivo'+_id+' a').attr('href',_sysUrlBase_+'/'+usertmp.archivotmp);
              $edituser.find('.descripcion'+_id).text(usertmp.descripcion); 
              $edituser.find('.fecharegistro'+_id).text(usertmp.fecharegistro);  
              $edituser.find('.tiporecord'+_id).attr('data-tiporecord',usertmp.tiporecord).text(usertmp.strtiporecord);
              $edituser.find('.tiporecord'+_id).removeAttr('style').css('color',(usertmp.tiporecord==1?'#2e6d1e':'#F30'));  
              $edituser.find('.panel-user').css('border','1.5px solid '+(usertmp.tiporecord==1?'#4eca30':'#F30'));
              $edituser.find('.btneditarrecord').attr('data-id',_id);
              $edituser.find('.btneliminarrecord').attr('data-id',_id);
              userrec[_id]=usertmp;              
            }
            $("#pnla_"+idguitmp).show();
            $("#pnlb_"+idguitmp).hide();
          }
        },
        error: function(e){ console.log(e); },
        complete: function(xhr){ }
      });
      return false;
    }
  });

  <?php 
  if(empty($records)) echo '$(".btnaddrec'.$idgui.'").trigger("click");';
  ?>
});
</script>