<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$frm=!empty($this->datos)?$this->datos:"";
$apoderados=$this->apoderados;
//var_dump($frm);
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .toolbarmouse{
    position: absolute;
    right: 0px;
    top: 0px;
    background: rgba(255, 255, 253, 0.72);
  }
  .input-file-invisible{
    position: absolute;
    top: 0px;
  }
  .item-user{
    padding: 1ex;
    position: relative;
  }
  .pnlacciones{
    position: absolute;
    bottom: 0px;
    text-align: center;
    width: 100%;
    background: #469de8;
    /*margin: -11px;*/
    padding: 5px;
    -webkit-animation: fadein 1s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 1s; /* Firefox < 16 */
        -ms-animation: fadein 1s; /* Internet Explorer */
         -o-animation: fadein 1s; /* Opera < 12.1 */
            animation: fadein 1s;
  }
  @keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
  }
  .pnlacciones a{
    margin: 0.25ex;
    width: 30px;
  }
  .panel-user .pnlacciones{
    display: none;    
  }
  .panel-user.active .pnlacciones{
    display: block;
    top:auto !important;
  }
</style>
<div class="col-md-12">
  <div class="panel panel-body" style="display: block" id="pnla_<?php echo $idgui; ?>">
    <div id="addaqui<?php echo $idgui; ?>">
      <div class="col-md-6 item-user cls<?php echo $idgui ?> " id="item-userclone<?php echo $idgui; ?>" style="display: none;" >
        <div class="panel-user">
        <div class="row">       
          <h4 class=" col-xs-12 border-turquoise color-turquoise text-center titulo fullnombreclone<?php echo $idgui; ?>"><i></i><br>
            <small class="parentescoclone<?php echo $idgui; ?>"></small></h4>
          <ul class="col-xs-12 list-unstyled informacion">
            <li class="col-xs-12">
              <div class="col-xs-2 icono tipodocclone<?php echo $idgui; ?>"></div>
              <div class="col-xs-10 texto_info ndocclone<?php echo $idgui; ?> "></div>
            </li>
            <li class="col-xs-12">
              <div class="col-xs-2 icono "><i class="fa fa-at"></i></div>
              <div class="col-xs-10 texto_info correoclone<?php echo $idgui; ?>"></div>
            </li>
            <li class="col-xs-12">
              <div class="col-xs-2 icono "><i class="fa fa-phone"></i></div>
              <div class="col-xs-10 texto_info telefonoclone<?php echo $idgui; ?>"></div>
            </li>
          </ul>
        </div>
        <div class="pnlacciones text-center">
          <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/verficha/?id=<?php echo $apo["ndoc"];?>" data-titulo="<?php echo JrTexto::_('Ficha')." ".$fullnombre; ?>"> <i class="fa fa-user"></i><i class="fa fa-eye"></i></a-->
          <a class="btn btn-warning btn-xs btneditarapoderado" data-modal="si" href="javascript:void(0);"  data-id="0" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Edit'); ?>"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>
          <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/cambiarclave/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('Change')." ".JrTexto::_('Password'); ?>"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>
          <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('notes');?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a>
          <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/horario/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a-->
          <a class="btneliminarapoderado btn btn-danger btn-xs" href="javascript:void(0);" data-id="0" data-titulo="<?php echo JrTexto::_('Delete'); ?>"><i class="fa fa-trash-o"></i></a>
        </div>
        </div>
      </div> 
<?php 
$xuserapo=array();
if(!empty($apoderados))
  foreach ($apoderados as $apo){
    $url=$this->documento->getUrlBase();
    $xuserapo[$apo["idapoderado"]]=$apo;
    $fullnombre=ucfirst($apo["ape_paterno"])." ".ucfirst($apo["ape_materno"]).", ".ucfirst($apo["nombre"]); ?>
  <div class="col-md-6 item-user cls<?php echo $idgui ?> " id="item-user<?php echo $apo["idapoderado"]; ?>" >
    <div class="panel-user">
      <div class="row">       
        <h4 class=" col-xs-12 border-turquoise color-turquoise text-center titulo fullnombre<?php echo $apo["idapoderado"]; ?>"><i><?php echo $fullnombre; ?></i><br>
          <small class="parentesco<?php echo $apo["idapoderado"]; ?>"><?php echo $apo["strparentesco"]; ?></small></h4>
        <ul class="col-xs-12 list-unstyled informacion">
          <li class="col-xs-12">
            <div class="col-xs-2 icono tipodoc<?php echo $apo["idapoderado"]; ?>"><?php echo $apo["tipo_documento"];?></div>
            <div class="col-xs-10 texto_info ndoc<?php echo $apo["idapoderado"]; ?> ">: <?php echo $apo["ndoc"]; ?></div>
          </li>
          <li class="col-xs-12">
            <div class="col-xs-2 icono "><i class="fa fa-at"></i></div>
            <div class="col-xs-10 texto_info correo<?php echo $apo["idapoderado"]; ?>">: <?php echo $apo["correo"]; ?></div>
          </li>
          <li class="col-xs-12">
            <div class="col-xs-2 icono "><i class="fa fa-phone"></i></div>
            <div class="col-xs-10 texto_info telefono<?php echo $apo["idapoderado"]; ?>">: <?php echo $apo["telefono"]." / ".$apo["celular"];?></div>
          </li>
        </ul>
      </div>
      <div class="pnlacciones text-center">
        <a class="btn btn-warning btn-xs btneditarapoderado" data-modal="si" href="javascript:void(0);"  data-id="<?php echo $apo["idapoderado"];?>" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Edit'); ?>"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>        
        <a class="btneliminarapoderado btn btn-danger btn-xs" href="javascript:void(0);" data-id="<?php echo $apo["idapoderado"];?>" data-titulo="<?php echo JrTexto::_('delete'); ?>"><i class="fa fa-trash-o"></i></a>
      </div>
  </div>
</div>
<?php } ?>
</div>
<div class="clearfix"></div>
<div class="col-md-12 text-center">
  <!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a-->
  <button class="btn btn-warning btnaddapo<?php echo $idgui ?>"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('Add'));?></button></div>  
</div>
</div>
<div class="col-md-12">
<div class="panel panel-body" style="display: none;" id="pnlb_<?php echo $idgui; ?>">
  <form class="" id="frm<?php echo $idgui; ?>"  target="" enctype="multipart/form-data">
    <input type="hidden" class="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$frm["idpersona"] ?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion ?>">
    <input type="hidden" name="accionapo" id="accionapo<?php echo $idgui; ?>" value="add">
    <input type="hidden" name="idapoderado" id="idapoderado<?php echo $idgui; ?>" value="">
    <div class="row">
      <div class="col-xs-12 col-sm-9 col-md-12">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Document type")); ?></label>
            <div class="cajaselect">
              <select name="tipodoc" id="tipodoc<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fktipodoc)) foreach ($this->fktipodoc as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>"  >
                    <?php echo $fk["nombre"] ?>
                    </option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
              <label for="titulo" class="text-left">N° <?php echo ucfirst(JrTexto::_('Id card')); ?></label>                    
                <input type="text" class="form-control" name="ndoc" id="ndoc<?php echo $idgui; ?>" value="" placeholder="" required="required" > 
            </div>  
        </div>
      </div>
      <div class="col-xs-12 col-sm-9 col-md-12">
        <div class="col-xs-12 col-sm-12 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Names')); ?></label>                    
              <input type="text" class="form-control"  name="nombre" id="nombre<?php echo $idgui; ?>" value="" placeholder="" required="required"> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Father's last name")); ?></label>                    
              <input type="text" class="form-control" name="ape_paterno" id="ape_paterno<?php echo $idgui; ?>" value="" placeholder="" required="required"> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Mother's last name")); ?></label>                    
              <input type="text" class="form-control" name="ape_materno" id="ape_materno<?php echo $idgui; ?>" value="" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Gender")); ?></label>
            <div class="cajaselect">
              <select name="sexo" id="sexo<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fksexo)) foreach ($this->fksexo as $fksexo) { ?>
                  <option value="<?php echo $fksexo["codigo"]?>"  >
                    <?php echo $fksexo["nombre"] ?>
                    </option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Marital Status")); ?></label>
            <div class="cajaselect">
              <select name="estado_civil" id="estado_civil<?php echo $idgui;?>" class="form-control">
                <?php if(!empty($this->fkestado_civil)) foreach ($this->fkestado_civil as $fkestado_civil) { ?>
                  <option value="<?php echo $fkestado_civil["codigo"]?>" >
                    <?php echo $fkestado_civil["nombre"] ?>
                    </option>
                <?php } ?>                        
              </select>
            </div>
          </div>
        </div> 
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Relationship")); ?></label>
            <div class="cajaselect">
              <select name="parentesco" id="parentesco<?php echo $idgui;?>" class="form-control">
                <?php if(!empty($this->fkparentesco)) foreach ($this->fkparentesco as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>" >
                    <?php echo $fk["nombre"] ?>
                    </option>
                <?php } ?>                        
              </select>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Telephone')); ?></label>                    
              <input type="text" class="form-control" name="telefono" id="telefono<?php echo $idgui; ?>" value="" required="required"> 
          </div> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Mobile number')); ?></label>                    
              <input type="text" class="form-control" name="celular" id="celular<?php echo $idgui; ?>" value="" > 
          </div> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('E-mail')); ?></label>                    
              <input type="text" class="form-control" name="correo" id="correo<?php echo $idgui; ?>" value="" > 
          </div> 
        </div>
       
      </div>
      <div class="clearfix"></div><br>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a href="javascript:void(0);" class="btn btn-danger btncancel<?php echo $idgui ?>"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Cancel');?></a>
        <button type="submit" class="btn btn-primary btnsave<?php echo $idgui ?>"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>
      </div>
      <div class="clearfix"></div><br>
    </div>            
  </form>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
  var userapo=<?php echo !empty($xuserapo)?json_encode($xuserapo):'[]' ?>;
  $('.cls<?php echo $idgui ?>').on("mouseenter",'.panel-user', function(){
    if(!$(this).hasClass('active')){ 
      $(this).siblings('.panel-user').removeClass('active');
      $(this).addClass('active');
    }
  }).on("mouseleave",'.panel-user', function(){
    $(this).removeClass('active');
  });

  $('#ndoc<?php echo $idgui; ?>').keyup(function(ev){
    //ev.preventDefault();
    var ndoc=$(this).val().toString();
    if(ndoc.length>4|| ev.which == 13){
      var formData = new FormData();
      formData.append('dni',ndoc); 
      formData.append('plt','blanco');
      var url=_sysUrlBase_+'/personal/buscarjson';
      $.ajax({
        url: url,
        type: "POST",
        data:  formData,
        contentType: false,
        processData: false,
        cache: false,
        dataType:'json',
        beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
        success: function(data){
          if(data.code=='Error'){
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
          }else{
            rd=data.data;
            if(rd.length>0){
              var rdt=rd[0];
              $('#idpersona<?php echo $idgui; ?>').val(rdt.idpersona||'');
              $('#nombre<?php echo $idgui; ?>').val(rdt.nombre||'');
              $('#ape_paterno<?php echo $idgui; ?>').val(rdt.ape_paterno||'');
              $('#ape_materno<?php echo $idgui; ?>').val(rdt.ape_materno||'');
              $('#sexo<?php echo $idgui; ?>').val(rdt.sexo||'');
              $('#telefono<?php echo $idgui; ?>').val(rdt.telefono||'');
              $('#celular<?php echo $idgui; ?>').val(rdt.celular||'');
              $('#correo<?php echo $idgui; ?>').val(rdt.email||''); 
            }else{
              $('#idpersona<?php echo $idgui; ?>').val(rdt.idpersona||'');
              $('#nombre<?php echo $idgui; ?>').val('');
              $('#ape_paterno<?php echo $idgui; ?>').val('');
              $('#ape_materno<?php echo $idgui; ?>').val(''); 
              $('#telefono<?php echo $idgui; ?>').val('');
              $('#celular<?php echo $idgui; ?>').val('');
              $('#correo<?php echo $idgui; ?>').val('');
            }
          }
        },
        error: function(e){ console.log(e); },
        complete: function(xhr){ }
      });
      return false;    
    }else $(this).focus();
    //ev.stopImmediatePropagation();
  });
  $(".btnaddapo<?php echo $idgui ?>").click(function(ev){
    $('#accionapo<?php echo $idgui; ?>').val('add');
    $('#idapoderado<?php echo $idgui; ?>').val('');
    $('#ndoc<?php echo $idgui; ?>').val('');
    $('#nombre<?php echo $idgui; ?>').val('');
    $('#ape_paterno<?php echo $idgui; ?>').val('');
    $('#ape_materno<?php echo $idgui; ?>').val(''); 
    $('#telefono<?php echo $idgui; ?>').val('');
    $('#celular<?php echo $idgui; ?>').val('');
    $('#correo<?php echo $idgui; ?>').val('');    
    $("#pnla_<?php echo $idgui; ?>").hide();
    $("#pnlb_<?php echo $idgui; ?>").show();
  });
  $('.btncancel<?php echo $idgui ?>').click(function(ev){
    $('#accionapo<?php echo $idgui; ?>').val('add');
    $('#ndoc<?php echo $idgui; ?>').val('');
    $('#nombre<?php echo $idgui; ?>').val('');
    $('#ape_paterno<?php echo $idgui; ?>').val('');
    $('#ape_materno<?php echo $idgui; ?>').val(''); 
    $('#telefono<?php echo $idgui; ?>').val('');
    $('#celular<?php echo $idgui; ?>').val('');
    $('#correo<?php echo $idgui; ?>').val('');    
    $("#pnla_<?php echo $idgui; ?>").show();
    $("#pnlb_<?php echo $idgui; ?>").hide();
  })

  $("#pnla_<?php echo $idgui; ?>").on('click','.btneditarapoderado',function(ev){
    var id=$(this).attr('data-id');
    var user=userapo!=[]?(userapo[id]!=undefined?userapo[id]:[]):[];
    $('#ndoc<?php echo $idgui; ?>').val(user.ndoc||'');
    $('#idapoderado<?php echo $idgui; ?>').val(user.idapoderado||'');
    $('#accionapo<?php echo $idgui; ?>').val('edit');
    $('#nombre<?php echo $idgui; ?>').val(user.nombre||'');
    $('#ape_paterno<?php echo $idgui; ?>').val(user.ape_paterno||'');
    $('#ape_materno<?php echo $idgui; ?>').val(user.ape_materno||''); 
    $('#telefono<?php echo $idgui; ?>').val(user.telefono||'');
    $('#celular<?php echo $idgui; ?>').val(user.celular||'');
    $('#correo<?php echo $idgui; ?>').val(user.correo||'');
    $("#pnla_<?php echo $idgui; ?>").hide();
    $("#pnlb_<?php echo $idgui; ?>").show();
  }).on('click','.btneliminarapoderado',function(ev){
    var id=$(this).attr('data-id');
    var res = xajax__('', 'persona_apoderado', 'eliminar', id);
    if(userapo[id]!=undefined) delete userapo[id];
    $(this).closest('.item-user').remove();    
  });
  var changefoto=false;
  $('#frm<?php echo $idgui;?>').bind({ 
    keyup:function(ev){
      if(ev.which == 13) return false;
    },submit: function(event){
      var idguitmp='<?php echo $idgui; ?>';
      var ndoc=$('#ndoc'+idguitmp).val().toString();
      if(ndoc=='') return;
      var myForm = document.getElementById('frm<?php echo $idgui; ?>'); 
      var formData = new FormData(myForm);
      var accion=$('#accionapo'+idguitmp).val();
      formData.append('accion',accion);
      formData.append('idpersona', $('#idpersona'+idguitmp).val()); 
      formData.append('idapoderado', $('#idapoderado'+idguitmp).val()); 
      formData.append('plt','blanco');
      var url=_sysUrlBase_+'/persona_apoderado/guardarPersona_apoderado';     
      $.ajax({
        url: url,
        type: "POST",
        data:  formData,
        contentType: false,
        processData: false,
        cache: false,
        dataType:'json',
        beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
        success: function(data){  
          if(data.code=='Error'){
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
          }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
            var _id=data.newid; 
            var usertmp={
              idapoderado:_id,
              idpersona:$('#idpersona'+idguitmp).val(),
              nombre:$('#nombre'+idguitmp).val(),
              ape_paterno:$('#ape_paterno'+idguitmp).val(),
              ape_materno:$('#ape_materno'+idguitmp).val(),
              parentesco:$('#parentesco'+idguitmp).val(),
              strparentesco:$('#parentesco'+idguitmp+' option:selected').text().trim(),
              sexo:$('#sexo'+idguitmp).val(),
              strsexo:$('#sexo'+idguitmp+' option:selected').text().trim(),
              correo:$('#correo'+idguitmp).val(),
              tipodoc:$('#tipodoc'+idguitmp).val(),
              tipo_documento:$('#tipodoc'+idguitmp+' option:selected').text().trim(),
              ndoc:$('#ndoc'+idguitmp).val(),
              telefono:$('#telefono'+idguitmp).val(),
              celular:$('#celular'+idguitmp).val(),
            }       
            if(accion=='add'){
              $newuser=$('#pnla_'+idguitmp+' .item-user:first-child').clone(true,true);
              $newuser.removeAttr('style');
              $newuser.attr('id','item-user'+_id);
              var tmpid=idguitmp+_id;
              $newuser.find('.fullnombreclone'+idguitmp).addClass('fullnombre'+_id).removeClass('fullnombreclone'+idguitmp);
              $newuser.find('.fullnombre'+_id+' i').text(usertmp.ape_paterno+' '+usertmp.ape_materno+', '+usertmp.nombre);
              $newuser.find('.parentescoclone'+idguitmp).addClass('parentesco'+_id).removeClass('parentescoclone'+idguitmp).text(usertmp.strparentesco);
              $newuser.find('.tipodocclone'+idguitmp).addClass('tipodoc'+_id).removeClass('tipodoccoclone'+idguitmp).text(usertmp.tipo_documento);
              $newuser.find('.ndocclone'+idguitmp).addClass('ndoc'+_id).removeClass('ndocclone'+idguitmp).text(' : '+usertmp.ndoc);
              $newuser.find('.correoclone'+idguitmp).addClass('correo'+_id).removeClass('correoclone'+idguitmp).text(' : '+usertmp.correo);
              $newuser.find('.telefonoclone'+idguitmp).addClass('telefono'+_id).removeClass('telefonoclone'+idguitmp).text(' : '+usertmp.telefono+' / '+usertmp.celular);
              $newuser.find('.btneditarapoderado').attr('data-id',_id);
              $newuser.find('.btneliminarapoderado').attr('data-id',_id);
              userapo[_id]=usertmp;          
              $("#pnla_"+idguitmp+' #addaqui'+idguitmp).append($newuser);
            }else{
              $edituser=$('#item-user'+data.newid);
              $edituser.find('.fullnombre'+_id+' i').text(usertmp.ape_paterno+' '+usertmp.ape_materno+', '+usertmp.nombre);
              $edituser.find('.parentesco'+_id).text(usertmp.strparentesco);
              $edituser.find('.tipodoc'+_id).text(usertmp.tipo_documento);
              $edituser.find('.ndoc'+_id).text(' : '+usertmp.ndoc);
              $edituser.find('.correo'+_id).text(' : '+usertmp.correo);
              $edituser.find('.telefono'+_id).text(' : '+usertmp.telefono+' / '+usertmp.celular);
              $edituser.find('.btneditarapoderado').attr('data-id',_id);
              $edituser.find('.btneliminarapoderado').attr('data-id',_id);
              userapo[_id]=usertmp;              
            }
            $("#pnla_"+idguitmp).show();
            $("#pnlb_"+idguitmp).hide();
          }
        },
        error: function(e){ console.log(e); },
        complete: function(xhr){ }
      });
      return false;
    }
  });

  <?php 
  if(empty($apoderados)) echo '$(".btnaddapo'.$idgui.'").trigger("click");';
  ?>
});
</script>