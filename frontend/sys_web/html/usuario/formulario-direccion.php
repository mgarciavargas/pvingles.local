<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$frm=!empty($this->datos)?$this->datos:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'user_avatar.jpg';
$ubigeo=!empty($frm["ubigeo"])?$frm["ubigeo"]:'000000';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .toolbarmouse{
    position: absolute;
    right: 0px;
    top: 0px;
    background: rgba(255, 255, 253, 0.72);
  }
  .input-file-invisible{
    position: absolute;
    top: 0px;
  }
</style>

<div class="col-md-12">
<div class="panel panel-body">
  <form class="" id="frm<?php echo $idgui; ?>"  target="" enctype="multipart/form-data">
    <input type="hidden" name="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$frm["idpersona"] ?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion ?>">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_('Country')); ?> </label>
            <div class="cajaselect">
              <select name="pais" id="pais<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fkpais)) foreach ($this->fkpais as $fk) { ?>
                  <option value="<?php echo $fk["pais"]?>" <?php echo $fk["pais"]==@$this->idpais?"selected":""; ?> >
                    <?php echo $fk["ciudad"] ?>
                  </option>
                <?php } ?>                            
              </select>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_('Region')); ?> </label>
            <div class="cajaselect">
              <select name="departamento" id="departamento<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fkdepartamento)) foreach ($this->fkdepartamento as $fk) { ?>
                  <option value="<?php echo $fk["departamento"]?>" <?php echo $fk["departamento"]==@$this->iddepa?"selected":""; ?> >
                    <?php echo $fk["ciudad"] ?>
                    </option>
                <?php } ?>                            
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_('Province')); ?></label>
            <div class="cajaselect">
              <select name="provincia" id="provincia<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fkprovincia)) foreach ($this->fkprovincia as $fk) { ?>
                  <option value="<?php echo $fk["provincia"]?>" <?php echo $fk["provincia"]==@$this->idpro?"selected":""; ?> >
                    <?php echo $fk["ciudad"] ?>
                    </option>
                <?php } ?>                            
              </select>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_('District')); ?> </label>
            <div class="cajaselect">
              <select name="distrito" id="distrito<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fkdistrito)) foreach ($this->fkdistrito as $fk) { ?>
                  <option value="<?php echo $fk["distrito"]?>" <?php echo $fk["distrito"]==@$this->iddepa?"selected":""; ?> >
                    <?php echo $fk["ciudad"] ?>
                    </option>
                <?php } ?>                            
              </select>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Urbanization')); ?></label>                    
            <input type="text" class="form-control"  name="urbanizacion" id="urbanizacion<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["urbanizacion"]); ?>" placeholder="<?php echo ucfirst(JrTexto::_('Urbanization')); ?>"> 
          </div>  
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Address')); ?></label>                    
            <input type="text" class="form-control"  name="direccion" id="direccion<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["direccion"]); ?>" placeholder="<?php echo ucfirst(JrTexto::_('Address')); ?>"> 
          </div>  
        </div>

      </div>

      <div class="clearfix"></div><br>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <!--a href="" class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a-->
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>
      </div>
      <div class="clearfix"></div><br>
    </div>            
  </form>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    var cargardatos<?php echo $idgui;?>=function(obj,data,returnobj){
      var midata=data||null;     
      $.ajax({
        url: _sysUrlBase_+'/ubigeo/buscarjson/?json=true',
        type: 'POST',
        dataType: 'json',
        data: midata,
      }).done(function(resp){
        if(resp.code=='ok'){                  
            $('option',obj).remove();
            if(resp.data!=''){
                $.each(resp.data,function(i,v){
                  obj.append('<option value="'+v[returnobj]+'">'+v["ciudad"]+'</option>');
              });
            }
            if(returnobj=='departamento'){
              $cbdepa=$('#departamento<?php echo $idgui;?>');
              var copt=$('option',$cbdepa).length;
              $('option',$cbdepa).eq(0).prop('selected',true);
              $('#departamento<?php echo $idgui;?>').trigger('change');
              return;
            }
            if(returnobj=='provincia'){
              $cbpro=$('#provincia<?php echo $idgui;?>');
              var copt=$('option',$cbpro).length;             
              $('option',$cbpro).eq(0).prop('selected',true);
              $cbpro.trigger('change');
              return;
            }
            if(returnobj=='distrito'){
              var iddep=$('#departamento<?php echo $idgui;?>').val()||'00';
              var idpro=$('#provincia<?php echo $idgui;?>').val()||'00';
              var iddis=$('#distrito<?php echo $idgui;?>').val()||'00';
              var idubigeo=iddep+''+idpro+''+iddis;
              $('#txtId_ubigeo').val(idubigeo);
            }
        }else{              
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
      }).fail(function(xhr, textStatus, errorThrown) {        
          console.log(xhr.responseText);
      });
  }

  $('#departamento<?php echo $idgui;?>').change(function(ev){
    var d={};
    d.pais='PE';
    d.departamento=$('#departamento<?php echo $idgui;?>').val();
    d.return='provincia';
    if(d.departamento==''){
        $('#provincia<?php echo $idgui;?> option').remove();
        $('#distrito<?php echo $idgui;?> option').remove();
    }else{
        cargardatos<?php echo $idgui;?>($('#provincia<?php echo $idgui;?>'),d,'provincia');     
    }
  });
  $('#provincia<?php echo $idgui;?>').change(function(ev){
    var d={};
    d.pais='PE';
    d.departamento=$('#departamento<?php echo $idgui;?>').val();
    d.provincia=$('#provincia<?php echo $idgui;?>').val();
    d.return='distrito';    
    if(d.provincia==''){
        $('#distrito<?php echo $idgui;?> option').remove();
    }else{
        cargardatos<?php echo $idgui;?>($('#distrito<?php echo $idgui;?>'),d,'distrito');     
    }
  });
  $('#distrito<?php echo $idgui;?>').change(function(ev){
    var iddep=$('#departamento<?php echo $idgui;?>').val()||'00';
    var idpro=$('#provincia<?php echo $idgui;?>').val()||'00';
    var iddis=$('#distrito<?php echo $idgui;?>').val()||'00';
    var idubigeo=iddep+''+idpro+''+iddis;
    
  });

    var changefoto=false;
    $('#frm<?php echo $idgui;?>').bind({    
     submit: function(event){
        var myForm = document.getElementById('frm<?php echo $idgui; ?>'); 
        var iddep=$('#departamento<?php echo $idgui;?>').val()||'00';
        var idpro=$('#provincia<?php echo $idgui;?>').val()||'00';
        var iddis=$('#distrito<?php echo $idgui;?>').val()||'00';
        var idubigeo=iddep+''+idpro+''+iddis;
        var formData = new FormData(myForm);
        formData.append('ubigeo',idubigeo); 
        formData.append('plt','blanco');
        var url=_sysUrlBase_+'/personal/guardarDatos';
        $.ajax({
          url: url,
          type: "POST",
          data:  formData,
          contentType: false,
          processData: false,
          cache: false,
          dataType:'json',
          beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
          success: function(data)
          {  
            if(data.code=='Error'){
              mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
            }else{
              console.log(data.msj);
              mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
              $('input.idpersona').val(data.newid)
            }
          },
          error: function(e){ console.log(e); },
          complete: function(xhr){ }
        });
        return false;
      }
    });
  });
</script>