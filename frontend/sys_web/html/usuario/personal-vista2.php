
<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
if(!empty($this->datos)) $personal=$this->datos;
?>
<style type="text/css">
	img.user-circle{
		display: block;
	    max-height: 120px;
	    max-width: 120px;
	    text-align: center;
	    margin: auto;
	    border: 1px solid #ccc;
	}
	.item-user{
		padding: 1ex;
		position: relative;
	}
	.pnlacciones{
    position: absolute;
    bottom: 0px;
    text-align: center;
    width: 100%;
    background: #469de8;
    /*margin: -11px;*/
    padding: 5px;
    -webkit-animation: fadein 1s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 1s; /* Firefox < 16 */
        -ms-animation: fadein 1s; /* Internet Explorer */
         -o-animation: fadein 1s; /* Opera < 12.1 */
            animation: fadein 1s;
  }
  @keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
  }
  .pnlacciones a{
    margin: 0.25ex;
    width: 30px;
  }
  .panel-user .pnlacciones{
    display: none;    
  }
  .panel-user.active .pnlacciones{
    display: block;
    top:auto !important;
  }
</style>
<?php
//var_dump($personal);
$i=0;
$url=$this->documento->getUrlBase();
$personaltmp=array();
if(!empty($personal))
	foreach ($personal as $per){ 
    if(!in_array($per["dni"],$personaltmp)){
    array_push($personaltmp,$per["dni"]);
    $i++;
  	$fullnombre=$per["ape_paterno"]." ".$per["ape_materno"].", ".$per["nombre"];
  	$idpersona=$per["idpersona"];
    $dni=$per["idpersona"];
?>
<div class="col-md-4 item-user cls<?php echo $idgui ?>" id="item-user<?php echo $idgui ?>">
<div class="panel-user" data-id="<?php echo $idpersona;?>">
    <div class="row">
        <div class="col-xs-12"><img class="img-circle img-responsive user-circle" src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo !empty($per["foto"])?$per["foto"]:'user_avatar.jpg' ?>" style="max-width: 200px; min-width: 150px; height: 130px;" ></div>
        <h4 class=" col-xs-12 border-turquoise color-turquoise text-center titulo"><i class="fullnombre"><?php echo $fullnombre; ?></i></h4>
        <ul class="col-xs-12 list-unstyled informacion">
            <!--li class="col-xs-12">
                <div class="col-xs-2 icono"><i class="fa fa-graduation-cap"></i></div>
                <div class="col-xs-10 texto_info">Eder F.P. Docente</div>
            </li-->
            <li class="col-xs-12">
                <div class="col-xs-2 icono"><i class="fa fa-at"></i></div>
                <div class="col-xs-10 texto_info">: <?php echo $per["email"]; ?></div>
            </li>
            <li class="col-xs-12">
                <div class="col-xs-2 icono"><i class="fa fa-phone"></i></div>
                <div class="col-xs-10 texto_info">: <?php echo $per["telefono"]."/".$per["celular"];?></div>
            </li>
        </ul>
    </div>
    <div class="pnlacciones text-center">
        <?php if(!empty($datareturn)){?>
            <a class="btn-selected btn btn-xs btn-warning" title="<?php echo ucfirst(JrTexto::_('Selected')); ?>"><i class="fa fa-hand-o-down"></i></a>
        <?php }else{?>
        <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/perfil/?idpersona=<?php echo $dni;?>&rol=<?php echo @$this->idrol; ?>" data-titulo="<?php echo JrTexto::_('Ficha')." ".$fullnombre; ?>"> <i class="fa fa-user"></i><i class="fa fa-eye"></i></a>
        <a class="btn btn-warning btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/formulario/?idpersona=<?php echo $dni;?>&rol=<?php echo @$this->idrol; ?>" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Edit'); ?>"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>
        <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/cambiarclave/?idpersona=<?php echo $dni;?>&rol=<?php echo @$this->idrol; ?>" data-titulo="<?php echo JrTexto::_('Change')." ".JrTexto::_('Password'); ?>"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>
         <?php if($this->idrol=='2'){?>
        <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?idpersonal=<?php echo md5($idpersona);?>&rol=<?php echo @$this->idrol; ?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('notes');?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a-->
        <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/horarios/?idpersona=<?php echo $dni;?>&rol=<?php echo @$this->idrol; ?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a>
        <?php } ?>
        <a class="btn-eliminar btn btn-danger  btn-xs" href="javascript:;" data-id="<?php echo $idpersona;?>" data-titulo="<?php echo JrTexto::_('delete'); ?>"><i class="fa fa-trash-o"></i></a>
        <?php } ?>
    </div>
</div>
</div>
<?php } } ?>
<script type="text/javascript">
$(document).ready(function(){
	$('.cls<?php echo $idgui ?>').on("mouseenter",'.panel-user', function(){
    if(!$(this).hasClass('active')){ 
      $(this).siblings('.panel-user').removeClass('active');
      $(this).addClass('active');
    }
  }).on("mouseleave",'.panel-user', function(){
    $(this).removeClass('active');
  });

  $('.cls<?php echo $idgui ?>').on('click','.btn-selected',function(){    
      var id=$(this).closest('.panel-user').attr('data-id');
      var nombre=$(this).closest('.panel-user').find('.fullnombre').text().trim();     
      <?php if(!empty($ventanapadre)){ ?>
      var datareturn={id:id,nombre:nombre}; 
      var tmpobj=$('.tmp<?php echo $ventanapadre ?>')
      if(tmpobj.length){
        tmpobj.attr('data-return',JSON.stringify(datareturn));
        tmpobj.on('returndata').trigger('returndata');
      } 
      $(this).closest('.modal').modal('hide'); 
      <?php } ?> 
  });
});
</script>