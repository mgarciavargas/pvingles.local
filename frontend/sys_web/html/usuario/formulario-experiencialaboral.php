<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$frm=!empty($this->datos)?$this->datos:"";
$experiencias=$this->experiencias;
//var_dump($frm);
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .toolbarmouse{
    position: absolute;
    right: 0px;
    top: 0px;
    background: rgba(255, 255, 253, 0.72);
  }
  .input-file-invisible{
    position: absolute;
    top: 0px;
  }
  .item-user{
    padding: 1ex;
    position: relative;
  }
  .pnlacciones{
    position: absolute;
    bottom: 0px;
    text-align: center;
    width: 100%;
    background: #469de8;
    /*margin: -11px;*/
    padding: 5px;
    -webkit-animation: fadein 1s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 1s; /* Firefox < 16 */
        -ms-animation: fadein 1s; /* Internet Explorer */
         -o-animation: fadein 1s; /* Opera < 12.1 */
            animation: fadein 1s;
  }
  @keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
  }
  .pnlacciones a{
    margin: 0.25ex;
    width: 30px;
  }
  .panel-user .pnlacciones{
    display: none;    
  }
  .panel-user.active .pnlacciones{
    display: block;
    top:auto !important;
  }
</style>
<div class="col-md-12">
  <div class="panel panel-body" style="display: block" id="pnla_<?php echo $idgui; ?>">
    <div id="addaqui<?php echo $idgui; ?>">
      <div class="col-md-6 item-user cls<?php echo $idgui ?> " id="item-userclone<?php echo $idgui; ?>" style="display: none;" >
        <div class="panel-user">
        <div class="row"> 
          <h4 class="col-xs-12 border-turquoise color-turquoise text-center titulo ">
          <i class="cargoclone<?php echo $idgui; ?>"></i><br>        
          <small class="fechaclone<?php echo $idgui; ?>" data-textode="<?php echo JrTexto::_('From the'); ?>:" data-textohasta="<?php echo JrTexto::_('to the present'); ?>">
          </small></h4>                 
          <div class="col-xs-12 texto_info text-center"> <strong class="empresaclone<?php echo $idgui; ?>"> </strong></div>
          <div class="col-xs-12 texto_info funcionesclone<?php echo $idgui; ?>"> </div>
        </div>
        <div class="pnlacciones text-center">
          <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/verficha/?id=<?php echo $exp["ndoc"];?>" data-titulo="<?php echo JrTexto::_('Ficha')." ".$fullnombre; ?>"> <i class="fa fa-user"></i><i class="fa fa-eye"></i></a-->
          <a class="btn btn-warning btn-xs btneditarexperiencia" data-modal="si" href="javascript:void(0);"  data-id="0" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Edit'); ?>"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>
          <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/cambiarclave/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('Change')." ".JrTexto::_('Password'); ?>"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>
          <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('notes');?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a>
          <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/horario/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a-->
          <a class="btneliminarexperiencia btn btn-danger btn-xs" href="javascript:void(0);" data-id="0" data-titulo="<?php echo JrTexto::_('Delete'); ?>"><i class="fa fa-trash-o"></i></a>
        </div>
        </div>
      </div> 
<?php 
$xuserexp=array();
if(!empty($experiencias))
  foreach ($experiencias as $exp){
    $url=$this->documento->getUrlBase();
    $xuserexp[$exp["idexperiencia"]]=$exp;  ?>
  <div class="col-md-6 item-user cls<?php echo $idgui ?> " id="item-user<?php echo $exp["idexperiencia"]; ?>" >
    <div class="panel-user">
      <div class="row">
        <?php 
          $tituloexp='';
          if(!empty($exp["cargo"])&&!empty($exp["strareaempresa"])) $tituloexp=$exp["cargo"]." - ".$exp["strareaempresa"]; 
          elseif (empty($exp["cargo"])&&!empty($exp["strareaempresa"])) $tituloexp=$exp["institucion"];
          elseif (!empty($exp["cargo"])&&empty($exp["strareaempresa"])) $tituloexp=$exp["cargo"];
          ?>     
        <h4 class="col-xs-12 border-turquoise color-turquoise text-center titulo ">
          <i class="cargo<?php echo $exp["idexperiencia"]; ?>"><?php echo $tituloexp; ?></i><br>        
          <small class="fecha<?php echo $exp["idexperiencia"]; ?>" data-textode="<?php echo JrTexto::_('From the'); ?>:" data-textohasta="<?php echo JrTexto::_('to the present'); ?>">
            <?php echo JrTexto::_('From the'); ?>: <?php echo $exp["fechade"] ." - ".(!empty($exp["actualmente"])?JrTexto::_('to the present'):$exp["fechahasta"]); ?></small></h4>
        <?php 
          $tituloemp='';
          if(!empty($exp["empresa"])&&!empty($exp["rubro"])) $tituloemp=$exp["empresa"]." - ".$exp["rubro"]; 
          elseif (empty($exp["empresa"])&&!empty($exp["rubro"])) $tituloemp=$exp["rubro"];
          elseif (!empty($exp["empresa"])&&empty($exp["rubro"])) $tituloemp=$exp["empresa"];
          ?>         
          <div class="col-xs-12 texto_info text-center"> <strong class="empresa<?php echo $exp["idexperiencia"]; ?>"> <?php echo $tituloemp; ?></strong></div>
          <div class="col-xs-12 texto_info funciones<?php echo $exp["idexperiencia"]; ?>"> <?php echo $exp["funciones"]; ?></div>    
        
      </div>
      <div class="pnlacciones text-center">
        <a class="btn btn-warning btn-xs btneditarexperiencia" data-modal="si" href="javascript:void(0);"  data-id="<?php echo $exp["idexperiencia"];?>" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Edit'); ?>"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>        
        <a class="btneliminarexperiencia btn btn-danger btn-xs" href="javascript:void(0);" data-id="<?php echo $exp["idexperiencia"];?>" data-titulo="<?php echo JrTexto::_('delete'); ?>"><i class="fa fa-trash-o"></i></a>
      </div>
  </div>
</div>
<?php } ?>
</div>
<div class="clearfix"></div>
<div class="col-md-12 text-center">
  <!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a-->
  <button class="btn btn-warning btnaddexp<?php echo $idgui ?>"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('Add'));?></button></div>  
</div>
</div>
<div class="col-md-12">
<div class="panel panel-body" style="display: none;" id="pnlb_<?php echo $idgui; ?>">
  <form class="" id="frmex<?php echo $idgui; ?>"  target="" enctype="multipart/form-data">
    <input type="hidden" class="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$frm["idpersona"] ?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion ?>">
    <input type="hidden" name="accionexp" id="accionexp<?php echo $idgui; ?>" value="add">
    <input type="hidden" name="idexperiencia" id="idexperiencia<?php echo $idgui; ?>" value="">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Business')); ?></label>                    
              <input type="text" class="form-control" name="empresa" id="empresa<?php echo $idgui; ?>" value="" placeholder="<?php echo JrTexto::_('Company') ?>" required="required" > 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Business sector')); ?></label>                    
              <input type="text" class="form-control" name="rubro" id="rubro<?php echo $idgui; ?>" value="" placeholder="<?php echo JrTexto::_('Rubro') ?>" required="required" > 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Área")); ?></label>
            <div class="cajaselect">
              <select name="area" id="area<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fkarea)) foreach ($this->fkarea as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>"  ><?php echo $fk["nombre"] ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Position')); ?></label>                    
              <input type="text" class="form-control" name="cargo" id="cargo<?php echo $idgui; ?>" value="" placeholder="<?php echo JrTexto::_('Jefe') ?>" required="required" > 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Funciones')); ?></label>
            <textarea name="funciones" class="form-control" id="funciones<?php echo $idgui; ?>"></textarea> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
          <label><?php echo ucfirst(JrTexto::_("First Date")); ?></label>
          <div class="form-group">
            <div class="input-group date datetimepicker">
              <input type="text" class="form-control" name="fechade" id="fechade<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["fechade"]); ?>"> 
              <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
          </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
          <label><?php echo ucfirst(JrTexto::_("Finish Date")); ?></label>
          <div class="form-group">
            <div class="input-group date datetimepicker">
              <input type="text" class="form-control" <?php echo @$frm["actualmente"]==1?"disabled='disabled'":"";?> name="fechahasta" id="fechahasta<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["fechahasta"]); ?>"> 
              <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
          </div>
          </div>
          <a style="cursor:pointer;" class="btnalpresente<?php echo $idgui; ?> fa  <?php echo @$frm["actualmente"]==1?"fa-check-circle":"fa-circle-o";?>">
                 <span> <?php echo JrTexto::_("Al Presente");?></span>
                 <input type="hidden" name="actualmente" id="actualmente<?php echo $idgui; ?>" value="<?php echo !empty($frm["actualmente"])?$frm["actualmente"]:0;?>" > 
                 </a>
        </div>        
      </div>
      
      <div class="clearfix"></div><br>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a href="javascript:void(0);" class="btn btn-danger btncancel<?php echo $idgui ?>"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Cancel');?></a>
        <button type="submit" class="btn btn-primary btnsave<?php echo $idgui ?>"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>
      </div>
      <div class="clearfix"></div><br>
    </div>            
  </form>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
  var userexp=<?php echo !empty($xuserexp)?json_encode($xuserexp):'[]' ?>;
  $('.cls<?php echo $idgui ?>').on("mouseenter",'.panel-user', function(){
    if(!$(this).hasClass('active')){ 
      $(this).siblings('.panel-user').removeClass('active');
      $(this).addClass('active');
    }
  }).on("mouseleave",'.panel-user', function(){
    $(this).removeClass('active');
  });
 
  $(".btnaddexp<?php echo $idgui ?>").click(function(ev){
    $('#accionexp<?php echo $idgui; ?>').val('add');
    $('#idexperiencia<?php echo $idgui; ?>').val('');
    $('#cargo<?php echo $idgui; ?>').val('');
    $('#area<?php echo $idgui; ?>').val('');
    $('#empresa<?php echo $idgui; ?>').val(''); 
    $('#rubro<?php echo $idgui; ?>').val('');
    $('#funciones<?php echo $idgui; ?>').val('');
    $('#fechade<?php echo $idgui; ?>').val('');
    $('#fechahasta<?php echo $idgui; ?>').val('');    
    $("#pnla_<?php echo $idgui; ?>").hide();
    $("#pnlb_<?php echo $idgui; ?>").show();
  });

  $('.btncancel<?php echo $idgui ?>').click(function(ev){
    $('#accionexp<?php echo $idgui; ?>').val('');
    $('#idexperiencia<?php echo $idgui; ?>').val();
    $('#cargo<?php echo $idgui; ?>').val('');
    $('#area<?php echo $idgui; ?>').val('');
    $('#empresa<?php echo $idgui; ?>').val(''); 
    $('#rubro<?php echo $idgui; ?>').val('');
    $('#funciones<?php echo $idgui; ?>').val('');
    $('#fechade<?php echo $idgui; ?>').val('');
    $('#fechahasta<?php echo $idgui; ?>').val('');    
    $("#pnla_<?php echo $idgui; ?>").show();
    $("#pnlb_<?php echo $idgui; ?>").hide();
  })

  $("#pnla_<?php echo $idgui; ?>").on('click','.btneditarexperiencia',function(ev){
    var id=$(this).attr('data-id');
    var user=userexp!=[]?(userexp[id]!=undefined?userexp[id]:[]):[];    
    $('#idexperiencia<?php echo $idgui; ?>').val(user.idexperiencia||'');
    $('#accionexp<?php echo $idgui; ?>').val('edit');
    $('#cargo<?php echo $idgui; ?>').val(user.cargo||'');
    $('#area<?php echo $idgui; ?>').val(user.area||'');
    $('#empresa<?php echo $idgui; ?>').val(user.empresa||''); 
    $('#rubro<?php echo $idgui; ?>').val(user.rubro||'');
    $('#funciones<?php echo $idgui; ?>').val(user.funciones||'');
    $('#fechade<?php echo $idgui; ?>').val(user.fechade||'');
    $('#fechahasta<?php echo $idgui; ?>').val(user.fechahasta||'');
    $("#pnla_<?php echo $idgui; ?>").hide();
    $("#pnlb_<?php echo $idgui; ?>").show();
  }).on('click','.btneliminarexperiencia',function(ev){
    var id=$(this).attr('data-id');
    var res = xajax__('', 'persona_experiencialaboral', 'eliminar', id);
    if(userexp[id]!=undefined) delete userexp[id];
    $(this).closest('.item-user').remove();    
  });

  $('#frmex<?php echo $idgui;?>').bind({ 
    keyup:function(ev){
      if(ev.which == 13) return false;
    },submit: function(event){
      var idguitmp='<?php echo $idgui; ?>';
      var myForm = document.getElementById('frmex<?php echo $idgui; ?>'); 
      var formData = new FormData(myForm);
      var accion=$('#accionexp'+idguitmp).val();
      formData.append('accion',accion);
      formData.append('idpersona', $('#idpersona'+idguitmp).val()); 
      formData.append('idexperiencia', $('#idexperiencia'+idguitmp).val()); 
      formData.append('plt','blanco');
      var url=_sysUrlBase_+'/persona_experiencialaboral/guardarPersona_experiencialaboral';     
      $.ajax({
        url: url,
        type: "POST",
        data:  formData,
        contentType: false,
        processData: false,
        cache: false,
        dataType:'json',
        beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
        success: function(data){  
          if(data.code=='Error'){
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
          }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
            var _id=data.newid; 
            var usertmp={
              idexperiencia:_id,
              idpersona:$('#idpersona'+idguitmp).val(),
              cargo:$('#cargo'+idguitmp).val(),
              area:$('#area'+idguitmp).val(),
              empresa:$('#empresa'+idguitmp).val(),
              rubro:$('#rubro'+idguitmp).val(),
              strareaempresa:$('#area'+idguitmp+' option:selected').text().trim(),             
              funciones:$('#funciones'+idguitmp).val(),
              fechade:$('#fechade'+idguitmp).val(),              
              fechahasta:$('#fechahasta'+idguitmp).val(),
              actualmente:$('#actualmente'+idguitmp).val(),
            }
            var tituloall='';
            if(usertmp.cargo!=''&&usertmp.strareaempresa!='')tituloall=usertmp.cargo+' - '+usertmp.strareaempresa;
            else if(usertmp.cargo==''&&usertmp.strareaempresa!='')tituloall=usertmp.strareaempresa;
            else if(usertmp.cargo!=''&&usertmp.strareaempresa=='')tituloall=usertmp.cargo;

            var tituloempresa='';
            if(usertmp.empresa!=''&&usertmp.rubro!='')tituloempresa=usertmp.empresa+' - '+usertmp.rubro;
            else if(usertmp.empresa==''&&usertmp.rubro!='')tituloempresa=usertmp.rubro;
            else if(usertmp.empresa!=''&&usertmp.rubro=='')tituloempresa=usertmp.empresa;

            if(accion=='add'){
              $newuser=$('#pnla_'+idguitmp+' .item-user:first-child').clone(true,true);
              $newuser.removeAttr('style');
              $newuser.attr('id','item-user'+_id);
              var tmpid=idguitmp+_id;
              $newuser.find('.cargoclone'+idguitmp).addClass('cargo'+_id).removeClass('cargoclone'+idguitmp).text(tituloall);
              var txtde=$newuser.find('.fechaclone'+idguitmp).attr('data-textode');
              var txthasta=$newuser.find('.fechaclone'+idguitmp).attr('data-textohasta');
              var _txthasta=usertmp.actualmente==1?(txthasta):(' - '+usertmp.fechahasta);              
              $newuser.find('.fechaclone'+idguitmp).addClass('fecha'+_id).removeClass('fechaclone'+idguitmp).text(txtde+' '+usertmp.fechade+' '+_txthasta);
              $newuser.find('.empresaclone'+idguitmp).addClass('empresa'+_id).removeClass('empresaclone'+idguitmp).text(tituloempresa);
              $newuser.find('.funcionesclone'+idguitmp).addClass('funciones'+_id).removeClass('funcionesclone'+idguitmp).text(usertmp.funciones);              
              $newuser.find('.btneditarexperiencia').attr('data-id',_id);
              $newuser.find('.btneliminarexperiencia').attr('data-id',_id);
              userexp[_id]=usertmp;          
              $("#pnla_"+idguitmp+' #addaqui'+idguitmp).append($newuser);
            }else{
              $edituser=$('#item-user'+data.newid);
              $edituser.find('.cargo'+_id).text(tituloall);
              var txtde=$edituser.find('.fecha'+_id).attr('data-textode');
              var txthasta=$edituser.find('.fecha'+_id).attr('data-textohasta');
              var _txthasta=usertmp.actualmente==1?(txthasta):(' - '+usertmp.fechahasta);
              $edituser.find('.fecha'+_id).text(txtde+' '+usertmp.fechade+' '+_txthasta);
              $edituser.find('.empresa'+_id).text(tituloempresa);
              $edituser.find('.funciones'+_id).text(usertmp.funciones);             
              $edituser.find('.btneditarexperiencia').attr('data-id',_id);
              $edituser.find('.btneliminarexperiencia').attr('data-id',_id);
              userexp[_id]=usertmp;              
            }
            $("#pnla_"+idguitmp).show();
            $("#pnlb_"+idguitmp).hide();
          }
        },
        error: function(e){ console.log(e); },
        complete: function(xhr){ }
      });
      return false;
    }
  });
  <?php if(empty($experiencias)) echo '$(".btnaddexp'.$idgui.'").trigger("click");'; ?>
  $('#fechade<?php echo $idgui; ?>').datetimepicker({
      //lang:'es',
      //timepicker:false,
      format:'YYYY/MM/DD'
  });
  $('#fechahasta<?php echo $idgui; ?>').datetimepicker({
      //lang:'es',
      //timepicker:false,
      format:'YYYY/MM/DD'
  });
  $('.btnalpresente<?php echo $idgui; ?>').click(function(){
    if($(this).hasClass('fa-circle-o')){
        $('#fechahasta<?php echo $idgui; ?>').attr('disabled','disabled');
        //$('span',this).text(' <?php //echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('#fechahasta<?php echo $idgui; ?>').removeAttr('disabled');
        //$('span',this).text(' <?php //echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }
  });
});
</script>