<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$grupo=!empty($this->datos)?$this->datos:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .titulo{
    font-weight: bold;
    font-size: 1.3em;
  }
  hr{
    margin-top: 0px;
    margin-bottom: 1ex;
    border: 0;
    border-top: 1px solid #00BCD4;
  }
  .datepicker {z-index: 9999! important}
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="#" onclick="window.history.back();"><i class="fa fa-history"></i>&nbsp;<?php echo JrTexto::_('Return'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <!--li><a href="<?php echo $this->documento->getUrlBase();?>/acad_grupoaula">&nbsp;<?php echo JrTexto::_("Schedule"); ?></a></li-->
        <li class="active">&nbsp;<?php echo JrTexto::_("Schedule"); ?></li>       
    </ol>
  </div>
</div>
<?php } ?>
<div class="row">
	<div class="col-xs-12 padding-0">
		<div class="panel" >      
	      	<div class="panel-body">
	    		<div id='calendar' class='calendar col-md-12'></div>
			</div>
		</div>
	</div>
</div>
<button class="hidden" id="recargarcalendario<?php echo $idgui; ?>"></button>
<script type="text/javascript">
$(document).ready(function(){
	var idioma = (_sysIdioma_=='ES')?'es':'en';
	  $('#calendar').fullCalendar({
      redener:true,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay,agendaWeek,agendaDay,listWeek,listDay'
      },
      views: {
      	listDay: { buttonText: 'list day' },
      	agendaWeek: { buttonText: 'Agenda Week' },
        listDay: { buttonText: 'list day' },
        agendaDay: { buttonText: 'Agenda day' },
        listWeek: { buttonText: 'list week' }
      },
      locale: idioma, 
      selectable: true,
      selectHelper: true,     
      defaultView: 'agendaWeek',
      slotDuration: '00:10:00',
      navLinks: true, // can click day/week names to navigate views
      editable: false,
      select: function(start, end) {
         // newEvent(start,end);
      },
      eventClick: function(event, jsEvent, view) {
       /* var newEvent={
          id:event.id,
          title:event.title,
          start:event.start.format('YYYY-MM-DD hh:mm a'),
          end:event.end.format('YYYY-MM-DD hh:mm a'),
          color:event.color
        }*/
        //console.log(newEvent);
        //editEvent(newEvent);
      },
      dayClick: function(date) {
          cal1GoTo(date);
      },
      events:[
      	<?php if(!empty($this->horarios))
      	  foreach($this->horarios as $hr){
      	  	echo $hr;
      	 } ?>			
		]
     });
  });
</script>