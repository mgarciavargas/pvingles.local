<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$persona=!empty($this->datos)?$this->datos:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
$rol=!empty($_REQUEST["rol"])?$_REQUEST["rol"]:"";
$br=$this->isalumno||$this->isdocente?true:false;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .slick-items<?php echo $idgui; ?> .slick-item>.btn{ width: 100%; <?php echo $br==true?'':'padding: 1em 0px;';?>}
  .slick-items<?php echo $idgui; ?> .slick-item>.btn.btn-panel.active{ opacity: 1;  }
  .slick-items<?php echo $idgui; ?> .slick-item>.btn.btn-panel.active:after{
    content: "";
    width: 0px;
    height: 0px;
    border-left: 15px solid transparent;
    border-right: 15px solid transparent;
    border-bottom: 15px solid #fbf7f5;
    font-size: 0px;
    line-height: 0px;
    position: absolute;
    left: 40%;
    bottom: -7px;
  }
  .slick-slider{ margin-bottom: 0.25ex !important; }
</style>
<div class="slick-items<?php echo $idgui; ?>">
  <div class="slick-item"><a href="javascript:void(0);" data-tab="datospersonales" class="btn btn-warning btn-panel active"><?php echo JrTexto::_('Personal Information'); ?></a></div>
  <div class="slick-item"><a href="javascript:void(0);" data-tab="direccion" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Address'); ?></a></div>
  <?php if($this->isalumno){?><div class="slick-item"><a href="javascript:void(0);" data-tab="apoderado" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Proxy Information'); ?></a></div> <?php }?>
  <div class="slick-item"><a href="javascript:void(0);" data-tab="metas" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Metas'); ?></a></div>
  <div class="slick-item"><a href="javascript:void(0);" data-tab="educacion" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Educational Instruction'); ?></a></div>
  <div class="slick-item"><a href="javascript:void(0);" data-tab="experiencialaboral" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Work experience'); ?></a></div>  
  <div class="slick-item"><a href="javascript:void(0);" data-tab="referencias" class="btn btn-warning btn-panel"><?php echo JrTexto::_('References'); ?></a></div>
  <div class="slick-item"><a href="javascript:void(0);" data-tab="record" class="btn btn-warning btn-panel"><?php echo JrTexto::_('Record'); ?></a></div>
</div>
<input type="hidden" class="idpersonal" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$persona["idpersona"] ?>">
<input type="hidden" class="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion ?>">
<div class="row" id="pnlvista<?php echo $idgui; ?>">

</div>

<script type="text/javascript">
  $(document).ready(function(){
    var optionslike<?php echo $idgui; ?>={
            //dots: true,
            infinite: false,
            //speed: 300,
            //adaptiveHeight: true
            navigation: false,
            slidesToScroll: 1,
            centerPadding: '60px',
            slidesToShow: 5,
            responsive:[
                { breakpoint: 1200, settings: {slidesToShow: 5} },
                { breakpoint: 992, settings: {slidesToShow: 4 } },
                { breakpoint: 880, settings: {slidesToShow: 3 } },
                { breakpoint: 720, settings: {slidesToShow: 2 } },
                { breakpoint: 320, settings: {slidesToShow: 1 /*,arrows: false, centerPadding: '40px',*/} }       
            ]
          };
    setTimeout(function(){
      $('.slick-items<?php echo $idgui; ?>').slick(optionslike<?php echo $idgui; ?>);
      //$('.slick-generalinformation<?php echo $idgui; ?>').slick(optionslike);
    },450);

    var cargarvista<?php echo $idgui; ?>=function(view){
        var _vista=view||'';
        var formData = new FormData(); 
        formData.append('idpersona',$('#idpersona<?php echo $idgui; ?>').val());
        formData.append('accion',$('#accion<?php echo $idgui; ?>').val());
        formData.append('vista',_vista);
        formData.append('plt','blanco');
        formData.append('fcall','<?php echo $fcall; ?>');
        formData.append('datareturn','<?php echo $datareturn; ?>');
        formData.append('rol','<?php echo $rol; ?>');
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/personal/frm_'+view,
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'html',
          callback:function(data){
            $('#pnlvista<?php echo $idgui; ?>').html(data);
          }
        }
        sysajax(data);
    }  
    var curtab<?php echo $idgui; ?>='';
   
    $('.slick-items<?php echo $idgui; ?>').on('click','.btn-panel',function(ev){
      var tab=$(this).attr('data-tab');
      if(tab==curtab<?php echo $idgui; ?>) return;
      $(this).closest('.slick-items<?php echo $idgui; ?>').find('a').removeClass('active');
      $(this).addClass('active');
      curtab<?php echo $idgui; ?>=tab;
      cargarvista<?php echo $idgui; ?>(tab);
    });

    $('.slick-items<?php echo $idgui; ?> .slick-item:first-child a').trigger('click');
  });
</script>