<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$frm=!empty($this->datos)?$this->datos:"";
$educacion=$this->educacion;
//var_dump($frm);
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .toolbarmouse{
    position: absolute;
    right: 0px;
    top: 0px;
    background: rgba(255, 255, 253, 0.72);
  }
  .input-file-invisible{
    position: absolute;
    top: 0px;
  }
  .item-user{
    padding: 1ex;
    position: relative;
  }
  .pnlacciones{
    position: absolute;
    bottom: 0px;
    text-align: center;
    width: 100%;
    background: #469de8;
    /*margin: -11px;*/
    padding: 5px;
    -webkit-animation: fadein 1s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 1s; /* Firefox < 16 */
        -ms-animation: fadein 1s; /* Internet Explorer */
         -o-animation: fadein 1s; /* Opera < 12.1 */
            animation: fadein 1s;
  }
  @keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
  }
  .pnlacciones a{
    margin: 0.25ex;
    width: 30px;
  }
  .panel-user .pnlacciones{
    display: none;    
  }
  .panel-user.active .pnlacciones{
    display: block;
    top:auto !important;
  }
</style>
<div class="col-md-12">
  <div class="panel panel-body" style="display: block" id="pnla_<?php echo $idgui; ?>">
    <div id="addaqui<?php echo $idgui; ?>">
      <div class="col-md-6 item-user cls<?php echo $idgui ?> " id="item-userclone<?php echo $idgui; ?>" style="display: none;" >
        <div class="panel-user">
        <div class="row">       
          <h4 class=" col-xs-12 border-turquoise color-turquoise text-center titulo institucionclone<?php echo $idgui; ?>"><i></i><br>
            <small class="fechaclone<?php echo $idgui; ?>" data-textode="<?php echo JrTexto::_('From the'); ?>:" data-textohasta="<?php echo JrTexto::_('to the present'); ?>"></small>
          </h4>
          <ul class="col-xs-12 list-unstyled informacion">
            <li class="col-xs-12 text-center">
              <strong><div class="col-xs-12 icono tipoestudioclone<?php echo $idgui; ?>"></div></strong>
            </li>
            <li class="col-xs-12 text-center">
            <div class="col-xs-12 icono situacionclone<?php echo $idgui; ?>"></div>
          </li>               
          </ul>
        </div>
        <div class="pnlacciones text-center">
          <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/verficha/?id=<?php echo $edu["ndoc"];?>" data-titulo="<?php echo JrTexto::_('Ficha')." ".$fullnombre; ?>"> <i class="fa fa-user"></i><i class="fa fa-eye"></i></a-->
          <a class="btn btn-warning btn-xs btneditareducacion" data-modal="si" href="javascript:void(0);"  data-id="0" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Edit'); ?>"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>
          <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/cambiarclave/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('Change')." ".JrTexto::_('Password'); ?>"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>
          <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('notes');?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a>
          <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/horario/?id=<?php echo $dni;?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a-->
          <a class="btneliminareducacion btn btn-danger btn-xs" href="javascript:void(0);" data-id="0" data-titulo="<?php echo JrTexto::_('Delete'); ?>"><i class="fa fa-trash-o"></i></a>
        </div>
        </div>
      </div> 
<?php 
$xuseredu=array();
if(!empty($educacion))
  foreach ($educacion as $edu){
    $url=$this->documento->getUrlBase();
    $xuseredu[$edu["ideducacion"]]=$edu;
    ?>
  <div class="col-md-6 item-user cls<?php echo $idgui ?> " id="item-user<?php echo $edu["ideducacion"]; ?>" >
    <div class="panel-user">
      <div class="row">       
        <h4 class=" col-xs-12 border-turquoise color-turquoise text-center titulo institucion<?php echo $edu["ideducacion"]; ?>"><i>
          <?php 
          if(!empty($edu["titulo"])&&!empty($edu["institucion"])) echo $edu["titulo"]." - ".$edu["institucion"]; 
          elseif (empty($edu["titulo"])&&!empty($edu["institucion"])) echo $edu["institucion"];
          elseif (!empty($edu["titulo"])&&empty($edu["institucion"])) echo $edu["titulo"];
          ?>            
          </i><br>
          <small class="fecha<?php echo $edu["ideducacion"]; ?>" data-textode="<?php echo JrTexto::_('From the'); ?>:" data-textohasta="<?php echo JrTexto::_('to the present'); ?>"><span><?php echo JrTexto::_('From the'); ?>: </span><?php echo $edu["fechade"] ." - ".(!empty($edu["actualmente"])?JrTexto::_('to the present'):$edu["fechahasta"]); ?></small></h4>
        <ul class="col-xs-12 list-unstyled informacion">
          <li class="col-xs-12 text-center">
            <strong><div class="col-xs-12 icono tipoestudio<?php echo $edu["ideducacion"]; ?>">             
              <?php 
                if(!empty($edu["strtipoestudio"])&&!empty($edu["strareaestudio"])) echo $edu["strtipoestudio"]." - ".$edu["strareaestudio"]; 
                elseif (empty($edu["strtipoestudio"])&&!empty($edu["strareaestudio"])) echo $edu["institucion"];
                elseif (!empty($edu["strtipoestudio"])&&empty($edu["strareaestudio"])) echo $edu["strtipoestudio"];
              ?>
              </div></strong>
          </li>
          <li class="col-xs-12 text-center">
            <div class="col-xs-12 icono situacion<?php echo $edu["ideducacion"]; ?>"><?php echo $edu["strsituacion"];?></div>
          </li>         
        </ul>
      </div>
      <div class="pnlacciones text-center">
        <a class="btn btn-warning btn-xs btneditareducacion" data-modal="si" href="javascript:void(0);"  data-id="<?php echo $edu["ideducacion"];?>" data-titulo="<?php echo JrTexto::_('Personal')." ".JrTexto::_('Edit'); ?>"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>        
        <a class="btneliminareducacion btn btn-danger btn-xs" href="javascript:void(0);" data-id="<?php echo $edu["ideducacion"];?>" data-titulo="<?php echo JrTexto::_('delete'); ?>"><i class="fa fa-trash-o"></i></a>
      </div>
  </div>
</div>
<?php } ?>
</div>
<div class="clearfix"></div>
<div class="col-md-12 text-center">
  <!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a-->
  <button class="btn btn-warning btnaddedu<?php echo $idgui ?>"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('Add'));?></button></div>  
</div>
</div>
<div class="col-md-12">
<div class="panel panel-body" style="display: none;" id="pnlb_<?php echo $idgui; ?>">
  <form class="" id="frm<?php echo $idgui; ?>"  target="" enctype="multipart/form-data">
    <input type="hidden" class="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$frm["idpersona"] ?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion ?>">
    <input type="hidden" name="accionedu" id="accionedu<?php echo $idgui; ?>" value="add">
    <input type="hidden" name="ideducacion" id="ideducacion<?php echo $idgui; ?>" value="">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('place of study')); ?></label>                    
              <input type="text" class="form-control" name="institucion" id="institucion<?php echo $idgui; ?>" value="" required="required"> 
          </div> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Study type")); ?></label>
            <div class="cajaselect">
              <select name="tipodeestudio" id="tipoestudio<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fktipoestudio)) foreach ($this->fktipoestudio as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>" ><?php echo $fk["nombre"] ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 ">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Situation")); ?></label>
            <div class="cajaselect">
              <select name="situacion" id="situacion<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fksituacionestudio)) foreach ($this->fksituacionestudio as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>" ><?php echo $fk["nombre"] ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 pnlparent<?php echo $idgui; ?>">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Study Area")); ?></label>
            <div class="cajaselect">
              <select name="areaestudio" id="areaestudio<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fkareaestudio)) foreach ($this->fkareaestudio as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>" ><?php echo $fk["nombre"] ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-8 pnlparent<?php echo $idgui; ?>">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Degree')); ?></label>                    
              <input type="text" class="form-control" name="titulo" id="titulo<?php echo $idgui; ?>" value="" > 
          </div> 
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
          <label><?php echo ucfirst(JrTexto::_("First Date")); ?></label>
          <div class="form-group">
            <div class="input-group date datetimepicker">
              <input type="text" class="form-control" name="fechade" id="fechade<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["fechade"]); ?>"> 
              <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
          </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
          <label><?php echo ucfirst(JrTexto::_("Finish Date")); ?></label>
          <div class="form-group">
            <div class="input-group date datetimepicker">
              <input type="text" class="form-control" <?php echo @$frm["actualmente"]==1?"disabled='disabled'":"";?> name="fechahasta" id="fechahasta<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["fechahasta"]); ?>"> 
              <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
          </div>
          </div>
          <a style="cursor:pointer;" class="btnalpresente<?php echo $idgui; ?> fa  <?php echo @$frm["actualmente"]==1?"fa-check-circle":"fa-circle-o";?>">
           <span> <?php echo JrTexto::_("Al Presente");?></span>
           <input type="hidden" name="actualmente" id="actualmente<?php echo $idgui; ?>" value="<?php echo !empty($frm["actualmente"])?$frm["actualmente"]:0;?>" > 
          </a>
        </div>        
      </div>
      
      <div class="clearfix"></div><br>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a href="javascript:void(0);" class="btn btn-danger btncancel<?php echo $idgui ?>"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Cancel');?></a>
        <button type="submit" class="btn btn-primary btnsave<?php echo $idgui ?>"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>
      </div>
      <div class="clearfix"></div><br>
    </div>            
  </form>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
  var useredu=<?php echo !empty($xuseredu)?json_encode($xuseredu):'[]' ?>;
  $('.cls<?php echo $idgui ?>').on("mouseenter",'.panel-user', function(){
    if(!$(this).hasClass('active')){ 
      $(this).siblings('.panel-user').removeClass('active');
      $(this).addClass('active');
    }
  }).on("mouseleave",'.panel-user', function(){
    $(this).removeClass('active');
  });

  $(".btnaddedu<?php echo $idgui ?>").click(function(ev){
    $('#accionedu<?php echo $idgui; ?>').val('add');
    $('#ideducacion<?php echo $idgui; ?>').val('');    
    $('#institucion<?php echo $idgui; ?>').val('');
    $('#tipoestudio<?php echo $idgui; ?>').val(1);
    $('#situacion<?php echo $idgui; ?>').val(2);
    $('#areaestudio<?php echo $idgui; ?>').val('');
    $('#titulo<?php echo $idgui; ?>').val('');
    $('#fechade<?php echo $idgui; ?>').val('');
    $('#fechahasta<?php echo $idgui; ?>').val('');   
    $("#pnla_<?php echo $idgui; ?>").hide();
    $("#pnlb_<?php echo $idgui; ?>").show();
    $('#actualmente<?php echo $idgui; ?>').val(0);
    $('#fechahasta<?php echo $idgui; ?>').removeAttr('disabled');
    $('.btnalpresente<?php echo $idgui; ?>').addClass('fa-circle-o').removeClass('fa-check-circle');
    $('#tipoestudio<?php echo $idgui; ?>').trigger('change');    
  });
  $('.btncancel<?php echo $idgui ?>').click(function(ev){ 
    $('#accionedu<?php echo $idgui; ?>').val('');
    $('#ideducacion<?php echo $idgui; ?>').val('');    
    $('#institucion<?php echo $idgui; ?>').val('');
    $('#tipoestudio<?php echo $idgui; ?>').val('');
    $('#situacion<?php echo $idgui; ?>').val('');
    $('#areaestudio<?php echo $idgui; ?>').val('');
    $('#titulo<?php echo $idgui; ?>').val('');
    $('#fechade<?php echo $idgui; ?>').val('');
    $('#fechahasta<?php echo $idgui; ?>').val(''); 
    $("#pnla_<?php echo $idgui; ?>").show();
    $("#pnlb_<?php echo $idgui; ?>").hide();
  })

  $("#pnla_<?php echo $idgui; ?>").on('click','.btneditareducacion',function(ev){
    var id=$(this).attr('data-id');
    var user=useredu!=[]?(useredu[id]!=undefined?useredu[id]:[]):[];
    $('#ideducacion<?php echo $idgui; ?>').val(user.ideducacion||'');
    $('#accionedu<?php echo $idgui; ?>').val('edit');    
    $('#institucion<?php echo $idgui; ?>').val(user.institucion||'');
    $('#tipoestudio<?php echo $idgui; ?>').val(user.tipodeestudio||'');
    $('#situacion<?php echo $idgui; ?>').val(user.situacion||'');
    $('#areaestudio<?php echo $idgui; ?>').val(user.areaestudio||'');
    $('#titulo<?php echo $idgui; ?>').val(user.titulo||'');
    $('#fechade<?php echo $idgui; ?>').val(user.fechade||'');
    $('#fechahasta<?php echo $idgui; ?>').val(user.fechahasta||'');
    $("#pnla_<?php echo $idgui; ?>").hide();
    $("#pnlb_<?php echo $idgui; ?>").show();    
    if(user.actualmente==1){
      $('#fechahasta<?php echo $idgui; ?>').attr('disabled','disabled');
      $('.btnalpresente<?php echo $idgui; ?>').removeClass('fa-circle-o').addClass('fa-check-circle');
    }else{
      $('#fechahasta<?php echo $idgui; ?>').removeAttr('disabled');
      $('.btnalpresente<?php echo $idgui; ?>').addClass('fa-circle-o').removeClass('fa-check-circle');
    } 
    $('#actualmente<?php echo $idgui; ?>').val(user.actualmente);
    $('#tipoestudio<?php echo $idgui; ?>').trigger('change');

  }).on('click','.btneliminareducacion',function(ev){
    var id=$(this).attr('data-id');
    var res = xajax__('', 'persona_educacion', 'eliminar', id);
    if(useredu[id]!=undefined) delete useredu[id];
    $(this).closest('.item-user').remove();    
  });
  var changefoto=false;
  $('#frm<?php echo $idgui;?>').bind({ 
    keyup:function(ev){
      if(ev.which == 13) return false;
    },submit: function(event){
      var idguitmp='<?php echo $idgui; ?>';      
      var myForm = document.getElementById('frm<?php echo $idgui; ?>'); 
      var formData = new FormData(myForm);
      var accion=$('#accionedu'+idguitmp).val();
      formData.append('accion',accion);
      formData.append('idpersona', $('#idpersona'+idguitmp).val()); 
      formData.append('ideducacion', $('#ideducacion'+idguitmp).val()); 
      formData.append('plt','blanco');
      var url=_sysUrlBase_+'/persona_educacion/guardarPersona_educacion';     
      $.ajax({
        url: url,
        type: "POST",
        data:  formData,
        contentType: false,
        processData: false,
        cache: false,
        dataType:'json',
        beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
        success: function(data){  
          if(data.code=='Error'){
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
          }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
            var _id=data.newid; 
            var usertmp={
              ideducacion:_id,
              idpersona:$('#idpersona'+idguitmp).val(), 
              titulo:$('#titulo'+idguitmp).val(),
              fechade:$('#fechade'+idguitmp).val(),
              fechahasta:$('#fechahasta'+idguitmp).val(),
              tipodeestudio:$('#tipoestudio'+idguitmp).val(),
              strtipoestudio:$('#tipoestudio'+idguitmp+' option:selected').text().trim(),
              situacion:$('#situacion'+idguitmp).val(),
              strsituacion:$('#situacion'+idguitmp+' option:selected').text().trim(),
              areaestudio:$('#areaestudio'+idguitmp).val(),
              strareaestudio:$('#areaestudio'+idguitmp+' option:selected').text().trim(),
              actualmente:$('#actualmente'+idguitmp).val(),
              institucion:$('#institucion'+idguitmp).val()
            }
            var tituloall='';
            if(usertmp.titulo!=''&&usertmp.institucion!='')tituloall=usertmp.titulo+' - '+usertmp.institucion;
            else if(usertmp.titulo==''&&usertmp.institucion!='')tituloall=usertmp.institucion;
            else if(usertmp.titulo!=''&&usertmp.institucion=='')tituloall=usertmp.titulo;
            var tipoestudioall='';
            if(usertmp.strtipoestudio!=''&&usertmp.strareaestudio!='')tipoestudioall=usertmp.strtipoestudio+' - '+usertmp.strareaestudio;
            else if(usertmp.strtipoestudio==''&&usertmp.strareaestudio!='')tipoestudioall=usertmp.strareaestudio;
            else if(usertmp.strtipoestudio!=''&&usertmp.strareaestudio=='')tipoestudioall=usertmp.strtipoestudio;
     
            if(accion=='add'){
              $newuser=$('#pnla_'+idguitmp+' .item-user:first-child').clone(true,true);
              $newuser.removeAttr('style');
              $newuser.attr('id','item-user'+_id);
              var tmpid=idguitmp+_id;
              $newuser.find('.institucionclone'+idguitmp).addClass('institucion'+_id).removeClass('institucionclone'+idguitmp);
              $newuser.find('.institucion'+_id+' i').text(tituloall);
              var txtde=$newuser.find('.fechaclone'+idguitmp).attr('data-textode');
              var txthasta=$newuser.find('.fechaclone'+idguitmp).attr('data-textohasta');
              var _txthasta=usertmp.actualmente==1?(txthasta):(' - '+usertmp.fechahasta);              
              $newuser.find('.fechaclone'+idguitmp).addClass('fecha'+_id).removeClass('fechaclone'+idguitmp).text(txtde+' '+usertmp.fechade+' '+_txthasta);
              $newuser.find('.tipoestudioclone'+idguitmp).addClass('tipoestudio'+_id).removeClass('tipoestudioclone'+idguitmp).text(tipoestudioall);
              $newuser.find('.situacionclone'+idguitmp).addClass('situacion'+_id).removeClass('situacionclone'+idguitmp).text(usertmp.strsituacion);
              $newuser.find('.btneditareducacion').attr('data-id',_id);
              $newuser.find('.btneliminareducacion').attr('data-id',_id);
              useredu[_id]=usertmp;          
              $("#pnla_"+idguitmp+' #addaqui'+idguitmp).append($newuser);
            }else{
              $edituser=$('#item-user'+data.newid);
              $edituser.find('.institucion'+_id+' i').text(tituloall);
              var txtde=$edituser.find('.fecha'+_id).attr('data-textode');
              var txthasta=$edituser.find('.fecha'+_id).attr('data-textohasta');
              var _txthasta=usertmp.actualmente==1?(txthasta):(' - '+usertmp.fechahasta);
              $edituser.find('.fecha'+_id).text(txtde+' '+usertmp.fechade+' '+_txthasta);
              $edituser.find('.tipoestudio'+_id).text(tipoestudioall);
              $edituser.find('.situacion'+_id).text(usertmp.strsituacion);
              $edituser.find('.btneditareducacion').attr('data-id',_id);
              $edituser.find('.btneliminareducacion').attr('data-id',_id);
              useredu[_id]=usertmp;              
            }
            $("#pnla_"+idguitmp).show();
            $("#pnlb_"+idguitmp).hide();
          }
        },
        error: function(e){ console.log(e); },
        complete: function(xhr){ }
      });
      return false;
    }
  });

  $('#fechade<?php echo $idgui; ?>').datetimepicker({
      //lang:'es',
      //timepicker:false,
      format:'YYYY/MM/DD'
  });
  $('#fechahasta<?php echo $idgui; ?>').datetimepicker({
      //lang:'es',
      //timepicker:false,
      format:'YYYY/MM/DD'
  });
  $('.btnalpresente<?php echo $idgui; ?>').click(function(){
    if($(this).hasClass('fa-circle-o')){
        $('#fechahasta<?php echo $idgui; ?>').attr('disabled','disabled');
        //$('span',this).text(' <?php //echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('#fechahasta<?php echo $idgui; ?>').removeAttr('disabled');
        //$('span',this).text(' <?php //echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }
  });

  $('#tipoestudio<?php echo $idgui; ?>').change(function(){
    var idtipo=$(this).val();
    if(idtipo<3){
      $('#areaestudio<?php echo $idgui; ?>').addClass('inactive').closest('.pnlparent<?php echo $idgui; ?>').hide();
      $('#titulo<?php echo $idgui; ?>').addClass('inactive').closest('.pnlparent<?php echo $idgui; ?>').hide();
      $('#titulo<?php echo $idgui; ?>').attr('data-val',$('#titulo<?php echo $idgui; ?>').val()).val('');
    }else{
      $('#areaestudio<?php echo $idgui; ?>').removeClass('inactive').closest('.pnlparent<?php echo $idgui; ?>').show();
      $('#titulo<?php echo $idgui; ?>').removeClass('inactive').closest('.pnlparent<?php echo $idgui; ?>').show();
      //$('#titulo<?php echo $idgui; ?>').val($('#titulo<?php //echo $idgui; ?>').attr('data-val')).removeAttr('data-val');
    }
  });
  $('#tipoestudio<?php echo $idgui; ?>').trigger('change');
  <?php 
  if(empty($educacion)) echo '$(".btnaddedu'.$idgui.'").trigger("click");';
  ?>
});
</script>