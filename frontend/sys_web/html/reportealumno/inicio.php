<?php defined('RUTA_BASE') or die();
$idgui = uniqid();

$usuarioAct = NegSesion::getUsuario();
$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();
?>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/examenes/general.css"
>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
}

</style>
<div class="container">
  <div class="row " id="levels" style="padding-top: 1ex; ">
            <div class="col-md-3">
                <ol class="breadcrumb">
                  <li><a href="<?php echo $this->documento->getUrlBase();?>/administrador"><?php echo JrTexto::_("Home")?></a></li>                  
                  <li class="active"><?php echo JrTexto::_("Components")?></li>
                </ol>
            </div>
             
               

    </div>

  

<div class="row " id="levels" style="padding-top: 1ex; ">

<div class = 'panel panel-primary' >
    <div class = 'panel-heading' style="text-align: left;">
    <?php echo JrTexto::_("SmartCourse")?>
    </div>

    <div class = 'panel-body' style="text-align: center;  " >
        


        <div class = 'col-xs-12 col-sm-4 col-md-2' >
        <div class="exa-item" style="height: 150px; padding-top: 3px">
          <a id="add_compo" href="#">
            
                <i class="fa fa-plus-circle" style="font-size:140px"></i>
              
          </a>
        </div>
        </div>


        <?php        
        if(!empty($this->lista_nivel1))
        foreach ($this->lista_nivel1 as $lista1){
        ?>

            <div class = 'col-xs-12 col-sm-4 col-md-2'  onmouseover="ver_mante(1,<?=$lista1["idnivel"]?>)" onmouseout="ver_mante(2,<?=$lista1["idnivel"]?>)">
            <a href = '<?php echo $this->documento->getUrlSitio(); ?>/componentes/unidades/?id1=<?=$lista1["idnivel"]?>'>
                  
                  <div class="exa-item" >
                  <p style="font-weight: bold;"><?=$lista1["nombre"]?></p>
                  <?
                  $imgSrc=str_replace('__xRUTABASEx__', $RUTA_BASE, @$lista1['imagen']);
                  ?>
                  <div class="portada">
                  <img src="<?php echo $imgSrc ?>" width="100%" alt="cover" class="img-responsive">
                  </div>
                  </div>
                
            </a>

            <div id="div_mante<?=$lista1["idnivel"]?>" class="div_mante border" style="position: absolute; bottom: 10px; right: 15px;" onmouseover="ver_mante(1,<?=$lista1["idnivel"]?>)" onmouseout="ver_mante(2,<?=$lista1["idnivel"]?>)">
            <a class="btn btn-success" onclick="ver_editar(<?=$lista1["idnivel"]?>);"><i class="fa fa-pencil-square-o"></i></a>
            <a class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
            </div>

          </div>

        <?
        }//for
        ?>


    </div>
</div>



</div>
</div>




<div class="modal fade" id="addunidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p id="addinfotitle" style="font-weight: bold;"></p>
      </div>
      <div class="modal-body" id="addinfocontent">
        <h4><i class="fa fa-cog fa-spin fa-fw"></i> <?php echo JrTexto::_('Loading') ?>...</h4>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){   
  
   $('#add_compo').on('click', '', function(e){
      
      e.preventDefault();
      e.stopPropagation();
      //var ran = Math.floor((Math.random() * 100) + 1);
      //alert(ran);
      //var claseid="div_unidad"+'<?php echo $idgui; ?>';
      var claseid="div_unidad";
      if ($("."+claseid).length) $("."+claseid).remove();
      //if(!$('.modal').hasClass(claseid)){
        var url='<?php echo JrAplicacion::getJrUrl(array('componentes','agregar'))?>?plt=modal';
        openModal('lg',"Crear",url,"div_unidad",claseid);
      //}

  });

  /*$('#level-item').change(function(){
    var idlevel=$(this).val();
    //alert(idpro);
    $('#unidad-item option').remove();
    var data={'tipo':'U','idpadre':idlevel}
    var res = xajax__('', 'Recursos', 'getxPadre',data);
    if(res){
    $('#unidad-item').append('<option value=""><?php echo JrTexto::_("All Unit")?></option>');
    $.each(res,function(){
    x=this;
    $('#unidad-item').append('<option value='+x["idnivel"]+'>'+x["nombre"]+'</option>');
    });
    //$( "#filtros" ).submit();
    }

    cargar_recursos();

  });

  $('#unidad-item').change(function(){
    var idunidad=$(this).val();
    //alert(idpro);
    $('#actividad-item option').remove();
    var data={'tipo':'L','idpadre':idunidad}
    var res = xajax__('', 'Recursos', 'getxPadre',data);
    if(res){
    $('#actividad-item').append('<option value=""><?php echo JrTexto::_("All Activities")?></option>');
    $.each(res,function(){
    x=this;
    $('#actividad-item').append('<option value='+x["idnivel"]+'>'+x["nombre"]+'</option>');
    });
    //$( "#filtros" ).submit();
    }

    cargar_recursos();

  });

  $('#actividad-item').change(function(){
    cargar_recursos();
  });*/


});

function ver_mante(sw,id){  
    if (sw==1){
      $("#div_mante"+id).css("opacity","1");
    }else{
      $("#div_mante"+id).css("opacity","0");
    }
    
}


function ver_editar(id) {      
    
    //var claseid="div_unidad"+'<?php echo $idgui; ?>';
    var claseid="div_unidad";
    if ($("."+claseid).length) $("."+claseid).remove();
    var url='<?php echo JrAplicacion::getJrUrl(array('componentes','editar'))?>?plt=modal&id='+id;
    openModal('lg',"Editar",url,"div_unidad",claseid);

}

/*
function adicionarcajaadd(){
  var idnivel=$("#level-item").val();
  var idunidad=$("#unidad-item").val();
  var idactividad=$("#actividad-item").val();
    if(idnivel&&idunidad&&idactividad){
      var addnuevo=$('#div_nuevo').clone(true,true);      
      orden=$('#div_cajas').find('.item-recurso').length;
      addnuevo.find('#a_nuevo').attr('href',_sysUrlBase_+'/tools/teacherresources/'+idnivel+'/'+idunidad+'/'+idactividad+'/?orden='+orden);
      addnuevo.show();
      $('#div_cajas').prepend(addnuevo);  
  }else 
  $('#div_cajas #div_nuevo').hide();
}

function cargar_recursos(){

  var idlevel=$("#level-item").val();
  var idunidad=$("#unidad-item").val();
  var idactividad=$("#actividad-item").val();
  var tipo="P";
  var idpersonal=<?php echo $this->iddoc;?>;
  var orden=0;
  $('#div_cajas').html("");
  var data={'idnivel':idlevel,'idunidad':idunidad,'idactividad':idactividad,'tipo':tipo,'idpersonal':idpersonal}
  var res = xajax__('', 'Recursos', 'CargarRecurso',data);
  if(res){    
  $.each(res,function(){
  x=this;
    var caratula=x["caratula"];
    caratula=caratula.replace(/__xRUTABASEx__/gi,_sysUrlBase_);
  
  $('#div_cajas').append('<div class="col-md-2 col-sm-3 col-xs-12 item-recurso"><div class="panel panel-danger" ><a href="'+_sysUrlBase_+'/tools/teacherresources/'+x["idnivel"]+'/'+x["idunidad"]+'/'+x["idactividad"]+'/?orden='+x["orden"]+'&dni=<?php echo $this->iddoc; ?>" target="_blank"><div class="panel-body"><h3 class="panel-title titulo" >'+x["titulo"]+'</h3><img src="'+_sysUrlStatic_+'/media/imagenes/caja_resource.png" class="img-responsive" width="100%"><img src="'+caratula+'" class="img-responsive caratula"><h3 class="panel-title autor"><?php echo @$usuarioAct["nombre_full"];?></h3></div></a></div></div>');
    orden=parseInt(orden)+parseInt(x["orden"]);
  });
  
  }
  
  orden=parseInt(orden)+1;
  if (idlevel && idunidad && idactividad){
    adicionarcajaadd();
  }else{
    $('#div_nuevo').hide();
  }

}
adicionarcajaadd();*/
</script>    