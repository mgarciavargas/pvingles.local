<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php $index=0; foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link']) && (count($this->breadcrumb)-1)!=$index ){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        $index++;
        } ?>
    </ol>
</div> </div>


<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">
<div class="row" id="tarea_practicar">
    <!-- filter zone -->
    <div class="col-xs-12" id="filtros-niveles">
        <div class="col-xs-3 select-ctrl-wrapper select-azul">
            <select name="opcIdCurso" id="opcIdCurso" class="form-control select-ctrl select-nivel">
                <option value="0">- <?php echo ucfirst(JrTexto::_("Select course")); ?> -</option>
                <?php if(!empty($this->cursos_nivel)){
                foreach ($this->cursos_nivel  as $c) {
                    echo '<option value="'.$c["idcurso"].'">'.$c["nombre"].'</option>';
                }} ?>
            </select>
        </div>
    </div>

    <div class="row" id="filtro-habilidad">
        <div class="col-xs-offset-0 col-sm-offset-8 col-xs-12 col-sm-4 select-ctrl-wrapper select-azul <?php echo $this->idHabilidad!=0?'hidden':'' ?>">
            <select name="opcIdHabilidad" id="opcIdHabilidad" class="form-control select-ctrl">
                <option value="0">- <?php echo ucfirst(JrTexto::_("Select skill")); ?> -</option>
                <?php if(!empty($this->habilidades)){
                foreach ($this->habilidades  as $h) {
                    $isSelected = ($h["idmetodologia"]==@$this->idHabilidad);
                    echo '<option value="'.$h["idmetodologia"].'" '.($isSelected?"selected":"").'>'.$h["nombre"].'</option>';
                }} ?>
            </select>
        </div>
        <br><br><br>
    </div>

    <!-- display results zone -->
    <div class="col-xs-12" id="zona-resultados">
        
    </div>

    <!-- default empty message zone -->
    <div class="col-xs-12" id="empty_data">
        <div class="jumbotron">
            <h2><?php echo ucfirst(JrTexto::_("There are no task to display")) ?>.</h2>
          <p><?php echo ucfirst(JrTexto::_("Please").', '.JrTexto::_("select a course or a level to show tasks")) ?>.</p>
          <p><a class="btn btn-default" href="<?php echo $this->documento->getUrlBase(); ?>" role="button"><i class="fa fa-arrow-left"></i> <?php echo ucfirst(JrTexto::_("Back home")) ?></a></p>
        </div>
    </div>

</div>


<!-- hidden components zone -->
<section class="hidden">
    <div class="col-xs-3 select-ctrl-wrapper select-azul hidden" id="clonar-select-nivel">
        <select name="opcIdCursoDetalle" id="opcIdCursoDetalle" class="form-control select-ctrl select-nivel">
            <option value="-1">- <?php echo ucfirst(JrTexto::_("Select")); ?> -</option>
        </select>
    </div>


    <div class="col-xs-12 col-sm-6" id="clone-tarea_item">
        <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="">
            <div class="panel-heading titulo">
                <h3>title</h3>
            </div>
            <div class="panel-body contenido">
                <div class="col-xs-4 portada">
                    <img src="<?php echo $this->documento->getUrlStatic();?>/media/web/nofoto.jpg" alt="img" class="img-responsive">
                </div>
                <div class="col-xs-8 descripcion">
                    descripcion
                </div>
                <!--div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                    <div class="col-xs-8 fecha-entrega">
                        <div class="dato">dd-mm-yyyy hh:ii aa</div>
                        <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                    </div>
                    <div class="col-xs-4 estado_promedio border-left">
                        <div class="dato">00</div>
                        <div class="info"><?php echo JrTexto::_("State")?></div>
                    </div>
                </div-->
            </div>
            <div class="panel-footer text-center">
                <a href="<?php echo $this->documento->getUrlBase().'/tarea/ver/?id=';?>" class="btn btn-blue vertarea"><?php echo ucfirst(JrTexto::_("View"))?></a>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
var cargarEjercicios = false;

var ESTADOS = JSON.parse('<?php 
    $arrEstados=array(
        'N'=> array('nombre'=>JrTexto::_('New'), 'clase'=>'color-info'),
        'P'=> array('nombre'=>JrTexto::_('Submitted'), 'clase'=>'color-yellow'),
        'D'=> array('nombre'=>JrTexto::_('Returned'), 'clase'=>'color-danger'),
        'E'=> array('nombre'=>JrTexto::_('Evaluated'), 'clase'=>'color-success'),
    );
    echo json_encode($arrEstados); 
?>');

var getCursoDetalle = function( dataSend , $select ) {
    $.ajax({
        async: false,
        url: _sysUrlBase_+'/acad_cursodetalle/buscarjson',
        type: 'GET',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp) {
        if(resp.code=='ok') {
            cargarEjercicios= false;
            var options = '';
            if(resp.data.length) {
                $.each(resp.data, function(i, det) {
                    if(det.tiporecurso!="E") {
                        options += '<option value="'+det.idcursodetalle+'">'+det.nombre+'</option>';
                    }
                });
                $select.append(options);
                $select.closest('.select-ctrl-wrapper').removeClass('hidden');
            }else{
                cargarEjercicios = true;
                $select.closest('.select-ctrl-wrapper').remove();
            }
        } else {
            mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
            $select.closest('.select-ctrl-wrapper').remove();
        }
    }).fail(function(err) {
        $select.closest('.select-ctrl-wrapper').remove();
        mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
    }).always(function() {  });
};

var borrarSobrantes = function($select) {
    var $listSelect = $('#filtros-niveles select.select-nivel');
    var indexOfSelect = $listSelect.index($select);
    var cantSelect = $listSelect.length;
    x=0;
    while($('#filtros-niveles select.select-nivel').eq(indexOfSelect+1).length>=1){
        $('#filtros-niveles select.select-nivel').eq(indexOfSelect+1).closest('.select-ctrl-wrapper').remove();
        if(x>=100){ console.log('algo salio mal'); break; }
        x++;
    }
};

var limpiarVisorEjercicios = function() {
    var $panel = $('#zona-ejercicios');
    $panel.html('');
};

var nuevoSelectFiltro = function( idPadre ) {
    var $divSelect_new = $('#clonar-select-nivel').clone();
    var idSelect = $divSelect_new.find('select.select-nivel').attr('id');
    var nuevoIdSelect = idSelect+'_'+idPadre;
    $divSelect_new.removeAttr('id');
    $divSelect_new.find('select.select-nivel').attr({
        'id': nuevoIdSelect,
        'name': nuevoIdSelect
    });
    $('#filtros-niveles').append($divSelect_new);
    return $('#filtros-niveles #'+nuevoIdSelect);
};

var nuevoItemTarea = function(tarea, $contenedor) {
    var $itemTarea = $('#clone-tarea_item').clone();
    $itemTarea.removeAttr('id');
    $itemTarea.find('.panel-heading.titulo>h3').html(tarea.nombre);
    $itemTarea.find('.tarea-item').attr('data-idtarea',tarea.idtarea);
    var img_src=(tarea.foto!='')?tarea.foto:_sysUrlStatic_+'/media/web/nofoto.jpg';
    $itemTarea.find('.panel-body.contenido>.portada>img').attr('src', img_src.replace(/__xRUTABASEx__/gi,_sysUrlBase_));
    $itemTarea.find('.panel-body.contenido>.descripcion').html(tarea.descripcion);
    /*
    $itemTarea.find('.panel-body.contenido .fecha-entrega>.dato').html(tarea.fechaentrega+' '+tarea.horaentrega);
    if(tarea.asignacion_alumno.estado=='N' || tarea.asignacion_alumno.estado=='D'){
        $itemTarea.find('.panel-body.contenido .estado_promedio>.dato').html(ESTADOS[tarea.asignacion_alumno.estado]['nombre']).addClass(ESTADOS[tarea.asignacion_alumno.estado]['clase']);
        $itemTarea.find('.panel-body.contenido .estado_promedio>.info').html('<?php echo JrTexto::_('State'); ?>');
    }
    if(tarea.asignacion_alumno.estado=='P' || tarea.asignacion_alumno.estado=='E'){
        var calificacion = (tarea.asignacion_alumno.notapromedio!=null)?tarea.asignacion_alumno.notapromedio:'---';
        $itemTarea.find('.panel-body.contenido .estado_promedio>.dato').html('<i>'+calificacion+'</i><small> / '+parseFloat(tarea.puntajemaximo).toFixed(0)+'</small>');
        $itemTarea.find('.panel-body.contenido .estado_promedio>.info').html('<?php echo JrTexto::_('Score'); ?>');
    }
    */
    $itemTarea.find('.panel-footer a.vertarea').attr('href', _sysUrlBase_+'/tarea/asignar_alummo/?idtarea='+tarea.idtarea);

    $contenedor.append($itemTarea);
};

var getTareas = function (dataSend , $pnlResult) {
    $.ajax({
        url: _sysUrlBase_+'/tarea/jxTareasXHabilidad',
        type: 'POST',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp) {
        $pnlResult.html('');
        if(resp.code=="ok" && resp.data.length) {
            $.each(resp.data, function(i, tarea) {
                nuevoItemTarea(tarea, $pnlResult);
            });
            $('#empty_data').hide();
        } else {
            console.log('data vacia');
            $('#empty_data').show();
        }
    }).fail(function(err) {
        $select.closest('.select-ctrl-wrapper').remove();
        mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
    }).always(function() {  });
    
};

$(document).ready(function() {
    $("#opcIdHabilidad").change(function(e) {
        let value = $(this).val();
        if(value<=0) return false;

        var url = window.location.href;
        if (url.indexOf('?') > -1){ url += '&';
        }else{ url += '?'; }
        url += 'idhab='+value;

        window.location.href = url;
    });

    $('#filtros-niveles').on('change', '.select-nivel', function(e) {
        var idPadre = 0;
        var idCurso = $('#opcIdCurso').val() || 0;
        if( $(this).attr('id') !== "opcIdCurso" ) {
            idPadre = $(this).val();
        }
        borrarSobrantes($(this));
        limpiarVisorEjercicios();
        $('#empty_data').show();
        if(idCurso==0){ return false; }

        $select_new = nuevoSelectFiltro(idPadre);
        getCursoDetalle({
            'idcurso': idCurso,
            'idpadre': idPadre,
        }, $select_new);

        if(cargarEjercicios) {
            var idHabilidad = $('#opcIdHabilidad').val() || 0;
            getTareas({
                'idcurso': idCurso,
                'iddetalle': idPadre,
                'idhab' : idHabilidad
            } , $('#zona-resultados'));
        }
    });
});

</script>