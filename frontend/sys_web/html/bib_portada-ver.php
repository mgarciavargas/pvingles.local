
<?php 
$pk=$this->pk;
$reg=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Ver Portada'); ?></h2>
        <div class="btn-group btn-group-md" style="float:right">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('bib_portada'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir a listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["id_portada"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["id_portada"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>            
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-md-12">
        <div id="msj-interno">
        <table class="table table-striped">
         <tr>
              <th><?php echo JrTexto::_("Id portada") ;?> </th>
              <th>:</th> 
          <td><?php echo $reg["id_portada"] ;?></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Foto") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><img src="<?php echo $this->documento->getUrlBase().$reg["foto"]; ?>" class="img-thumbnail" style="max-height:100px; max-width:250px;"></td>
          </tr>
          <tr>
              <th style="max-width:150px;"><?php echo JrTexto::_("Estado") ;?> </th>
                <th style="max-width:20px;">:</th> 
          <td><a style="cursor:pointer;" class="chklistado fa  <?php echo $reg["estado"]=="1"?"fa-check-circle":"fa-circle-o"; ?> fa-lg" data-value="<?php echo $reg["estado"]; ?>"
              data-campo="estado" data-txtid="<?php echo $reg["id_portada"]; ?>" data-value2="<?php echo $reg["estado"]==1?0:1; ?>" >
              <?php echo $reg["estado"]=="1"?JrTexto::_("Activo"):JrTexto::_("Inactivo"); ?></a></td>
          </tr>
                    </table>
          </div>
          <hr><div class="text-center">
          <div class="btn-group btn-group-md">
          <a class="btn btn-primary" href="<?php echo JrAplicacion::getJrUrl(array('bib_portada'));?>"><i class="fa fa-repeat"></i> <?php echo JrTexto::_('Ir al listado'); ?></a>
          <button class="editar btn btn-success" data-reg="<?php echo $reg["id_portada"]; ?>"  href="#"><i class="fa fa-edit"></i> <?php echo JrTexto::_('Editar'); ?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo $reg["id_portada"]; ?>" href="#"><i class="fa fa-remove"></i> <?php echo JrTexto::_('Eliminar'); ?></button>
        </div>
	        </div>         
	    </div>
</div>
<br />
	</div>
	<script type="text/javascript">
  var asInitVals = new Array();
	$(document).ready(function(){
        $('.chklistado').click(function(){
        var id= $(this).attr('data-txtid');
        var campo_= $(this).attr('data-campo');
        var v1=$(this).attr('data-value');
        var v2=$(this).attr('data-value2');
        var data={pk:id,campo:campo_,value:v2};
        var res = xajax__('', 'Bib_portada', 'saveEstatus',data);
        if(res) {
          $(this).attr('data-value',v2);
          $(this).attr('data-value2',v1);
          $(this).removeClass('fa-circle-o').removeClass('fa-check-circle');
          $(this).addClass(('0' == v2) ? ' fa-circle-o ' : ' fa-check-circle ');
          $(this).text(('0' == v2) ? v2 : v1);
          agregar_msj_interno('success', '<?php echo JrTexto::_("Información actualizada");?>');
          $('.alert').fadeOut(4000);
        }
    });
    
    $('.btnNuevo').click(function(){
      $('#pkaccion').val('guardar');
      $('#pkid_portada').val('');
      $('.img-thumbnail').attr("style","display:none");
    });

    $('.editar').click(function(){
      $('.img-thumbnail').removeAttr("style");
      var id=$(this).attr("data-reg");
            recargarpagina('admin/bib_portada/frm/?tpl=g&acc=Editar&id='+id);
          
    });

    $('.eliminar').click(function(){
     if(confirm("<?php echo JrTexto::_('¿Desea eliminar el registro seleccionado?');?>")) {
        var id=$(this).attr("data-reg");
        var res = xajax__('', 'Bib_portada', 'eliminarBib_portada',id)
        if(res) {
          agregar_msj_interno('success', "<?php echo JrTexto::_('Registro eliminado');?>");
                 recargarpagina('bib_portada/frm/?tpl=g&acc=Editar&id='+id);
              } 
        $('.alert').fadeOut(4000);
      }
    });
	});
</script>