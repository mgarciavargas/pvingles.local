<?php defined('RUTA_BASE') or die(); 
    $idgui = uniqid(); 
    $rutabase = $this->documento->getUrlBase();
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/css/tool_vocabulary.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/smartbook/ver.css">

<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> 
</div>

<a href="anclaUp" class="ancla" id="anclaUp"></a>
<div id="smartbook-ver" class="editando row" data-idautor="<?php echo @$voc['idpersonal']; ?>">
    <input type="hidden" id="rolUsuario" value="<?php echo @$this->rolActivo; ?>">
    <input type="hidden" id="idCurso" value="<?php echo @$this->idCurso; ?>">
    <input type="hidden" id="idSesion" value="<?php echo @$this->sesion['idnivel']; ?>">
    <input type="hidden" id="idBitacoraAlumnoSmbook" value="<?php echo !empty($this->bitac_alum_smbook)? @$this->bitac_alum_smbook['idbitacora_smartbook']:'';?>">
    <input type="hidden" id="idLogro" value="<?php echo @$this->idLogro; ?>">
    <div class="col-xs-12 col-sm-6">
       <!-- <a class="btn btn-default" href="<?php echo $rutabase.$this->urlBtnRetroceder ?>">
            <i class="fa fa-arrow-left"></i> <?php echo JrTexto::_("Go back") ?>
        </a>-->
        <a class="btn btn-info preview" href="#">
            <i class="fa fa-eye"></i> <?php echo JrTexto::_("Preview") ?>
        </a>
        <a class="btn btn-info back"  href="#" style="display: none;">
            <i class="fa fa-eye-slash"></i> <?php echo ucfirst(JrTexto::_("Close").' '.JrTexto::_("preview")) ?>
        </a>
        <a href="#" class="btn btn-lilac btn-pronunciar slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/pronunciacion">
            <i class="fa fa-bullhorn"></i> <?php echo ucfirst(JrTexto::_("Pronunciation")) ?>
        </a>
    </div>
    <div class="col-xs-12 col-sm-6" style="padding-right: 70px;">
        <ul id="navTabsTop" class="nav nav-tabs pull-right">
            <li class="hvr-bounce-in <?php echo @$this->pdf['bitacora']['ultimo_visto'].' '.(empty(@$this->pdfs["caratula"])?'hidden':''); ?>"><a class="" href="#div_pdfs" data-toggle="tab"><?php echo ucfirst(JrTexto::_("PDF")); ?></a></li>
            <li class="hvr-bounce-in <?php echo @$this->audios['bitacora']['ultimo_visto'].' '.(empty(@$this->audios["caratula"])?'hidden':''); ?>"><a class="" href="#div_audios" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Audios")); ?></a></li>
            <li class="hvr-bounce-in <?php echo @$this->imagenes['bitacora']['ultimo_visto'].' '.(empty(@$this->imagenes["caratula"])?'hidden':''); ?>"><a class="" href="#div_imagen" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Images")); ?></a></li>
            <li class="hvr-bounce-in <?php echo @$this->videos['bitacora']['ultimo_visto'].' '.(empty(@$this->videos["caratula"])?'hidden':''); ?>"><a class="" href="#div_videos" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Videos")); ?></a></li>
        </ul>
    </div>

    <a href="#anclaUp" class="scroller up" id="scroll-up" style="display: none;"><i class="fa fa-angle-double-up fa-2x"></i></a>

    <div id="cuaderno" class="tabbable tabs-right col-xs-12 padding-0"> <div id="anillado"></div>
        <ul id="navTabsLeft" class="nav nav-tabs metodologias">
            <li class="active hvr-bounce-in"><a class="vertical" href="#div_presentacion" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li class="hvr-bounce-in <?php echo @$this->look['bitacora']['ultimo_visto'].' '.(empty(@$this->look['act']['det'])?'hidden':''); ?>"><a class="vertical" href="#div_look" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Look")) ?></a></li>
            <li class="hvr-bounce-in <?php echo @$this->practice['bitacora']['ultimo_visto'].' '.(empty(@$this->practice['act']['det'])?'hidden':''); ?>"><a class="vertical" href="#div_practice" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Practice")) ?></a></li>
            <li class="hvr-bounce-in <?php echo @$this->games['bitacora']['ultimo_visto'].' '.(empty(@$this->games)?'hidden':''); ?>"><a class="vertical" href="#div_games" data-toggle="tab"><?php echo JrTexto::_("Game") ?>s</a></li>
            <li class="hvr-bounce-in <?php echo @$this->vocabulario['bitacora']['ultimo_visto'].' '.(empty($this->vocabulario)?'hidden':''); ?>"><a class="vertical" href="#div_voca" data-toggle="tab"><?php echo JrTexto::_("Vocabulary") ?></a></li>
            <!--li class="hvr-bounce-in <?php echo @$this->workbook['bitacora']['ultimo_visto'].' '.(empty($this->workbook['texto'])?'hidden':''); ?>"><a class="vertical" href="#div_workbook" data-toggle="tab"><?php echo JrTexto::_("Workbook") ?></a></li-->
            <li class="hvr-bounce-in <?php echo @$this->dby['bitacora']['ultimo_visto'].' '.(empty(@$this->dby['act']['det'])?'hidden':''); ?>"><a class="vertical" href="#div_autoevaluar" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Do it by yourself")) ?></a></li> 
            <li ><a class="vertical btn btn-default" id="exit" href="<?php echo $rutabase.$this->urlBtnRetroceder ?>"><i class="fa fa-sign-out vertical"></i> <?php echo JrTexto::_("Exit") ?></a></li>
            <span class="hvr-bounce-in hidden"><a class="vertical" href="javascript:history.back();"><?php echo JrTexto::_("Exit") ?></a></span>
        </ul>

        <div id="tabContent" class="tab-content">
            <div id="contenedor-paneles" class="hidden">
                <div id="panel-barraprogreso" class="text-center eje-panelinfo" style="display: none;">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            <span>0</span>%
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-md-12 infouserejericiopanel">                   
                    <div class="col-md-3 text-left" >
                        <a class="btn btn-inline btn-success btn-lg btnejercicio-back_ " style="display: inline-block;  padding-top: 0.9em;  height: 3.1em;"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a>
                    </div>
                    <div class="col-md-6 text-center">
                        <span class="infouserejericio ejeinfo-numero numero_ejercicio">
                            <div class="titulo">Exercise</div>
                            <span class="actual">1</span>
                            <span class="total">16</span>
                        </span>
                        <span id="panel-intentos-dby"  class="infouserejericio ejeinfo-intento">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('attempt')); ?></div>
                            <span class="actual">1</span>
                            <span class="total">3</span>
                        </span>
                        <span id="panel-tiempo" class="infouserejericio ejeinfo-tiempo">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('time')); ?></div>
                            <span class="actual"></span>
                            <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50" style="display: none;">
                            <span class="info-show">00:00</span>
                        </span>                         
                    </div>
                    <div class="col-md-3 text-right" >
                        <a class="btn btn-inline btn-success btn-lg btnejercicio-next_" style="display: inline-block; padding-top: 0.9em;  height: 3.1em;"><?php echo JrTexto::_('Next'); ?> <i class="fa fa-arrow-right"></i></a>
                    </div>                            
                </div>
                <!--div  class="eje-panelinfo text-center pull-right" >
                    <div class="titulo btn-blue"><?php echo ucfirst(JrTexto::_('attempt')); ?></div>
                    <span class="actual">1</span>
                    <span class="total"><?php echo @$this->intentos; ?></span>
                </div>
                <div  class="eje-panelinfo pull-right" >
                    <div class="titulo btn-yellow"><?php echo ucfirst(JrTexto::_('time')); ?></div>
                    <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="<?php echo JrTexto::_('e.g.'); ?>: 03:50" style="display: none;">
                    <div class="info-show">00</div>
                </div-->
            </div>
           
            <!-- tab-pane for #navTabsLeft -->
            <div class="tab-pane aquihtml active" id="div_presentacion" data-tabgroup="navTabsLeft" style="background-image: url('<?php echo @str_replace('__xRUTABASEx__', $rutabase, $this->sesion["imagen"]) ;?>'); ">
                <div class="col-xs-12 portada-info" style="display: block;">
                    <h2 class="col-xs-12 col-sm-6 div_titulo hidden"><div class="div_fondo"><?php echo @$this->nivel["nombre"];?></div></h2>
                    <h2 class="col-xs-12 col-sm-6 div_titulo hidden"><div class="div_fondo"><?php echo @$this->unidad["nombre"];?></div></h2>
                    <h1 class="col-xs-12 col-sm-12 div_titulo"><div class="div_fondo"><?php echo @$this->sesion["nombre"];?></div></h1>
                    <?php if (!empty(@$this->bitacora)) { ?>
                    <div class="col-xs-6 div_acciones" style="display: none;">
                        <button class="btn btn-red btn-lg btn-continuar">
                            <span><?php echo ucfirst(JrTexto::_("Continue where I left")); ?> </span> &nbsp;
                            <i class="fa fa-hand-o-right"></i> 
                        </button>
                    </div>
                    <?php } ?>
                    <div class="col-xs-12 div_descripcion hidden div_fondo"><?php echo @$voc["texto"] ;?></div>
                    <div class="creado-por div_fondo hidden">
                        <?php echo JrTexto::_("Created by") ?>: 
                        <span><?php echo @$this->usuarioAct['nombre_full'] ;?></span>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="div_look" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->look['bitacora'])?'data-idbitacora='.$this->look['bitacora']['idbitacora']:'' ?>>
                <div class="content-smartic metod" id="met-<?php echo @$this->look["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->look["met"]["idmetodologia"]; ?>" style="overflow: hidden;">
                    <div class="col-xs-12 Ejercicio">
                    <?php foreach (@$this->look['act']['det'] as $i => $det_html) {
                        echo str_replace('__xRUTABASEx__',$rutabase,$det_html['texto_edit']);
                    } ?>
                    </div>
                    <div class="col-xs-3 hidden">
                        <div class="widget-box">
                            <div class="widget-header bg-red">
                                <h4 class="widget-title">
                                    <i class="fa fa-paint-brush"></i>
                                    <span><?php echo JrTexto::_('Skills'); ?></span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row habilidades">
                                        <div class="col-xs-12 ">
                                        <?php 
                                        $ihab=0;
                                        if(!empty($this->habilidades))
                                        foreach ($this->habilidades as $hab) { $ihab++;?>
                                            <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                        <?php } ?>                                  
                                        </div>
                                    </div> 
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="div_practice" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->practice['bitacora'])?'data-idbitacora='.$this->practice['bitacora']['idbitacora']:'' ?>>
                <div class="content-smartic metod" id="met-<?php echo @$this->practice["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->practice["met"]["idmetodologia"]; ?>" style="overflow: hidden;">
                    <div class="col-xs-12">
                        <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"])) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"])) ?>" data-id-metod="<?php echo @$this->practice["met"]["idmetodologia"]; ?>">
                            <?php if(!empty(@$this->practice['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            $num=0;
                            foreach ($this->practice['act']['det'] as $i=>$ejerc) { $num++;
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($num==1);
                                } ?>
                                <li class="<?php echo ($aquiSeQuedo)?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]) ).'-'.$num; ?>" data-toggle="tab"><?php echo $num; ?></a></li>
                            <?php } } ?>
                            <li class="hidden comodin"><!-- para que cuente correctamente la barra progreso --></li>
                        </ul>

                        <div class="col-xs-12 text-center numero_ejercicio" style="display: none">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Exercise')); ?></div>
                            <span class="actual"></span>
                            <span class="total"></span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="tab-content" id="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]) ) ?>-main-content">
                        <?php
                        if(!empty(@$this->practice['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            foreach (@$this->practice['act']['det'] as $i=>$ejerc) { 
                                $x=$i+1; 
                                $iddetalle=$ejerc["iddetalle"]; 
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($x==1);
                                } ?>
                            <div id="tab-<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]))."-".$x; ?>" class="tabhijo tab-pane fade <?php echo ($aquiSeQuedo)?'active in':''; ?>"  data-iddetalle="<?php echo  @$ejerc["iddetalle"]; ?>" >
                            <?php $html = $ejerc["texto_edit"];
                            if($this->rolActivo=='Alumno' && !empty($ejerc['desarrollo']) && !empty(@$ejerc['desarrollo']['html_solucion'])){
                                $html  = $ejerc['desarrollo']['html_solucion'];
                            }
                            echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$html);?>
                            </div>
                         <?php } } ?>
                        </div>
                    </div>
                    <div class="col-xs-3 hidden">
                        <div class="widget-box">
                            <div class="widget-header bg-red">
                                <h4 class="widget-title">
                                    <i class="fa fa-paint-brush"></i>
                                    <span><?php echo JrTexto::_('Skills'); ?></span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row habilidades">
                                        <div class="col-xs-12 ">
                                        <?php 
                                        $ihab=0;
                                        if(!empty($this->habilidades))
                                        foreach ($this->habilidades as $hab) { $ihab++;?>
                                            <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                        <?php } ?>                                  
                                        </div>
                                    </div> 
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div> 
            </div>
            <div class="tab-pane" id="div_games" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->games['bitacora'])?'data-idbitacora='.$this->games['bitacora']['idbitacora']:'' ?>>
                <div class="col-xs-12">
                    <ul class="nav nav-tabs items-select-list">
                        <?php
                        if(!empty($this->games)){
                            foreach ($this->games as $i=>$g) {
                                echo '<li'.(($i==0)?' class="active"':'').'><a href="#game_'.$g['idtool'].'" data-idgame="'.$g['idtool'].'" class="item-select" data-toggle="tab">'.($i+1).'</a></li>';
                                $i++;
                            }
                        } ?>
                    </ul>
                </div>
                <div class="tab-content col-xs-12">
                    <div class=" mascara_visor">                    
                        <iframe src="" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="div_voca" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->vocabulario['bitacora'])?'data-idbitacora='.$this->vocabulario['bitacora']['idbitacora']:'' ?>>
                <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("Vocabulary") ?></h1>
                <div class="col-xs-12">
                    <?php if(!empty($this->vocabulario)){
                        foreach ($this->vocabulario as $i => $v) {
                            echo str_replace('__xRUTABASEx__', $rutabase, @$v['texto']);
                        } 
                    }else{ ?>
                    <div class="text-center">
                        <h1 style="color: rgb(148, 148, 148);"><i class="fa fa-minus-circle fa-3x"></i></h1>
                        <h4><?php echo JrTexto::_("No vocabulary to display"); ?></h4>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!--div class="tab-pane" id="div_workbook" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->workbook['bitacora'])?'data-idbitacora='.$this->workbook['bitacora']['idbitacora']:'' ?>>
                <h1 class="title-resrc"><?php echo JrTexto::_("Workbook") ?></h1>
                <div class=" mascara_visor">                    
                    <iframe src="<?php echo @str_replace('__xRUTABASEx__', $rutabase, @$this->workbook['texto']) ;?>" frameborder="0" height="100"></iframe>
                </div>
            </div-->
            <div class="tab-pane" id="div_autoevaluar" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->dby['bitacora'])?'data-idbitacora='.$this->dby['bitacora']['idbitacora']:'' ?>>
                <div class="content-smartic metod" id="met-<?php echo @$this->dby["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->dby["met"]["idmetodologia"]; ?>" style="overflow: hidden;">
                    <div class="col-xs-12">
                        <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"])) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"])) ?>" data-id-metod="<?php echo @$this->dby["met"]["idmetodologia"]; ?>">
                            <?php if(!empty(@$this->dby['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            $num=0;
                            foreach ($this->dby['act']['det'] as $i=>$ejerc) { $num++;
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo  = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($num==1);
                                } ?>
                                <li class="<?php echo ($aquiSeQuedo)?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]) ).'-'.$num; ?>" data-toggle="tab"><?php echo $num; ?></a></li>
                            <?php } } ?>
                            <li class="hidden comodin"><!-- tab obligado para calcular barra progreso--></li>
                        </ul>

                        <div class="col-xs-12 text-center numero_ejercicio" style="display: none;">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Exercise')); ?></div>
                            <span class="actual"></span>
                            <span class="total"></span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="tab-content" id="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]) ) ?>-main-content">
                        <?php 
                        if(!empty(@$this->dby['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            foreach (@$this->dby['act']['det'] as $i=>$ejerc) { 
                                $x=$i+1; 
                                $iddetalle=$ejerc["iddetalle"]; 
                                if($this->rolActivo=='Alumno'){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else{
                                    $aquiSeQuedo = ($x==1);
                                }
                                ?>
                            <div id="tab-<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]))."-".$x; ?>" class="tabhijo tab-pane fade <?php echo ($aquiSeQuedo)?'active in':''; ?>" data-iddetalle="<?php echo  @$ejerc["iddetalle"]; ?>">
                            <?php  $html = $ejerc["texto_edit"];
                            if($this->rolActivo=='Alumno' && !empty($ejerc['desarrollo']) && !empty(@$ejerc['desarrollo']['html_solucion'])){
                                $html  = $ejerc['desarrollo']['html_solucion'];
                            }
                            echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$html);?>
                            </div>
                         <?php } } ?>
                        </div>
                    </div>
                    <div class="col-xs-3 hidden">
                        <div class="widget-box">
                            <div class="widget-header bg-red">
                                <h4 class="widget-title">
                                    <i class="fa fa-paint-brush"></i>
                                    <span><?php echo JrTexto::_('Skills'); ?></span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row habilidades">
                                        <div class="col-xs-12 ">
                                        <?php 
                                        $ihab=0;
                                        if(!empty($this->habilidades))
                                        foreach ($this->habilidades as $hab) { $ihab++;?>
                                            <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                        <?php } ?>                                  
                                        </div>
                                    </div> 
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>

            <!-- tab-pane for #navTabsTop -->
            <div class="tab-pane" id="div_pdfs" data-tabgroup="navTabsTop" <?php echo !empty(@$this->pdfs['bitacora'])?'data-idbitacora='.$this->pdfs['bitacora']['idbitacora']:'' ?>>
                <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("PDF") ; ?></h1>

                <div class="col-xs-12 col-sm-2 padding-0 controles-edicion text-center nopreview">
                    <a class="open-biblioteca btn btn-app btn-sm btn-danger nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a file from our multimedia library or upload your own video')) ?>" data-tipo="pdf" data-url=".ifrtemporal">
                        <i class="app-icon fa fa-plus-circle"></i>
                        <?php echo ucfirst(JrTexto::_('add')); ?>
                    </a>
                    <iframe class="hidden ifrtemporal temp" src="" style="display: none"></iframe>
                </div>

                <div class="col-xs-12 col-sm-10 items-select-list pdf-selecc contenedor-slider">
                    <div class="myslider">
                        <?php //echo @$this->imagenes["caratula"];
                        $pdfs = explode(",", @$this->pdfs["caratula"]);
                        $canttextp = explode(",", @$this->pdfs["texto"]);
                        if (@$this->pdfs["caratula"]){ $cantpdf=count($pdfs)-1; }
                        else{ $cantpdf=-1; }

                        for ($y=0;$y<=$cantpdf;$y++){
                            $srcFile = @str_replace('__xRUTABASEx__', $rutabase, $pdfs[$y]);
                            $clsVisto = (!empty(@$this->pdfs['bitacora']) && in_array($srcFile, $this->pdfs['bitacora']['otros_datos']))?'visto':'';
                            echo '<a href="#" class="item-select miniatura miniaturap '.$clsVisto.'" ><span class="del-miniatura nopreview"></span><div class="file color-danger" src="'.$srcFile.'" alt="pdf" data-tit="'.@$canttextp[$y].'" data-pos="'.$y.'" id="pdf'.$y.'"><i class="fa fa-file-pdf-o fa-4x"></i><span class="nombre-file">'.@$canttextp[$y].'</span></div></a>';
                        } ?>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="mascara_visor">
                        <iframe src="" frameborder="0" class="pdf_resrc" style="display: none;"></iframe>
                        <!--<img src="<?php echo $this->documento->getUrlStatic() ?>/media/web/pdf_file.jpg" class="img-responsive nopreview mask" style="/*width: 100%;height: 300px;*/">-->
                        <div class="mask">
                            <img src="<?php echo $this->documento->getUrlStatic() ?>/media/web/pdf_file.jpg" class="img-responsive center-block <?php echo !empty($pdf)?'hidden':''; ?>">
                            <span class="<?php echo !empty($pdf)?'hidden':''; ?>"><?php echo ucfirst(JrTexto::_('There is no file to display')); ?></span>
                        </div>
                    </div>
                </div>

                <div  class="col-xs-12">
                    <input type="hidden"  id="txtDescp" name="txtDescp" class="descripcion-media hidden" value="">
                    <input type="hidden" id="txtposp" name="txtposp" required="required" value="">
                    <input type="hidden" class="contenido-caratula" id="txtpdf" name="txtpdf" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, @$this->pdfs["caratula"]); ?>">
                    <input type="hidden" class="contenido-texto" id="txtTitp" name="txtTitp" required="required" class="form-control" value="<?php echo @$this->pdfs["texto"]?>">
                </div>
            </div>
            <div class="tab-pane" id="div_audios" data-tabgroup="navTabsTop" <?php echo !empty(@$this->audios['bitacora'])?'data-idbitacora='.$this->audios['bitacora']['idbitacora']:'' ?>>
                <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("Audios"); ?></h1>
                <div class="col-xs-12 col-sm-2 controles-edicion nopreview">
                    <a class="open-biblioteca btn btn-app btn-sm btn-orange nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select an audio from our multimedia library or upload your own audio')) ?>" data-tipo="audio" data-url=".audtemporal">
                        <i class="app-icon fa fa-plus-circle"></i>
                        <?php echo ucfirst(JrTexto::_('add')); ?>
                    </a>
                    <audio class="hidden audtemporal temp" src="" style="display: none"></audio>
                </div>
                <div class="col-xs-12 col-sm-10 padding-0 items-select-list audio-selecc contenedor-slider">
                    <div class="myslider">
                        <?php 
                        $auds = explode(",", $this->audios["caratula"]);
                        $canttextA = explode(",", $this->audios["texto"]);
                        if ($this->audios["caratula"]){ $cantvid=count($auds)-1; }
                        else{ $cantvid=-1; }
                        for ($y=0;$y<=$cantvid;$y++){
                            $srcFile = @str_replace('__xRUTABASEx__', $rutabase, $auds[$y]);
                            $clsVisto = (!empty(@$this->audios['bitacora']) && in_array($srcFile, $this->audios['bitacora']['otros_datos']))?'visto':'';
                            echo '<a href="#" class="item-select miniatura miniatura_a '.$clsVisto.'"><span class="del-miniatura nopreview"></span><div href="#" class="file color-orange" src="'.$srcFile.'" alt="audio" data-pos="'.$y.'" data-tit="'.$canttextA[$y].'" id="img'.$y.'"><i class="fa fa-file-audio-o fa-4x"></i></div></a>';
                        } ?>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="col-xs-offset-3 col-xs-5 padding-0">
                        <input type="text"  id="txtDesca" name="txtDesca" required="required" class="form-control descripcion-media hidden" value="">
                    </div>
                    <div class="col-xs-4 padding-0 text-left">
                        <a class="btn btn-orange saveaudio nopreview hidden" title="<?php echo JrTexto::_('Save text') ?>"><i class="fa fa-save"></i> <span class="sr-only"><?php echo JrTexto::_('Save text') ?></span></a>
                    </div>

                    <input type="hidden"  id="txtposa" name="txtposa" required="required" value="">
                    <input type="hidden" class="contenido-caratula" id="txtaudio" name="txtaudio" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, $this->audios["caratula"]); ?>">
                    <input type="hidden" class="contenido-texto" id="txtTita" name="txtTita" required="required" class="form-control" value="<?php echo $this->audios["texto"]?>">
                </div>

                <div class="col-xs-12">
                    <div class="mascara_visor">
                        <audio controls="true" class="valvideo audio_resrc" src="" style="display: none;"></audio>
                        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/noaudio.png" class="img-responsive center-block nopreview mask">
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="div_imagen" data-tabgroup="navTabsTop" <?php echo !empty(@$this->imagenes['bitacora'])?'data-idbitacora='.$this->imagenes['bitacora']['idbitacora']:'' ?>>
                <h1 class="col-xs-12 title-resrc"><?php echo ucfirst(JrTexto::_("Images")); ?></h1>
                <div class="col-xs-12 col-sm-2 padding-0 controles-edicion nopreview">
                    <a class="open-biblioteca btn btn-app btn-sm btn-success nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a image from our multimedia library or upload your own video')) ?>" data-tipo="image" data-url=".imgtemporal">
                        <i class="app-icon fa fa-plus-circle"></i> 
                        <?php echo ucfirst(JrTexto::_('add')); ?>
                    </a>
                    <img class="hidden imgtemporal temp" src="" style="display: none;">
                </div>

                <div class="col-xs-12 col-sm-10 items-select-list image-selecc contenedor-slider">
                    <div class="myslider">
                        <?php 
                        $imgs = explode(",", @$this->imagenes["caratula"]);
                        $canttext = explode(",", @$this->imagenes["texto"]);
                        if (@$this->imagenes["caratula"]){ $cantimg=count(@$imgs)-1; }
                        else{ $cantimg=-1; } 
                        for ($y=0;$y<=$cantimg;$y++){
                            $srcFile = @str_replace('__xRUTABASEx__', $rutabase, @$imgs[$y]);
                            $clsVisto = (!empty(@$this->imagenes['bitacora']) && in_array($srcFile, $this->imagenes['bitacora']['otros_datos']))?'visto':'';
                            echo '<a href="#" class="item-select miniatura miniaturai '.$clsVisto.'"> <span class="del-miniatura nopreview"></span><img src="'.$srcFile.'" alt="image" style="height: 86px; width: 100%" data-tit="'.@$canttext[$y].'" data-pos="'.$y.'" ></a>';
                        } ?>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="col-xs-offset-3 col-xs-5 padding-0 ">
                        <input type="text"  id="txtDesc" name="txtDesc" required="required" class="form-control descripcion-media hidden" value="">
                    </div>
                    <div class="col-xs-4 padding-0 text-left">
                         <a class="btn btn-success saveimage nopreview hidden" title="<?php echo JrTexto::_('Save text') ?>"><i class="fa fa-save"></i> <span class="sr-only"><?php echo JrTexto::_('Save text') ?></span></a>
                    </div>
                    <input type="hidden"  id="txtpos" name="txtpos" required="required" value="">
                    <input type="hidden" class="contenido-caratula" id="txtimg" name="txtimg" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, @$this->imagenes["caratula"]); ?>">
                    <input type="hidden" class="contenido-texto" id="txtTit" name="txtTit" required="required" class="form-control" value="<?php echo @$this->imagenes["texto"]?>">
                </div>

                <div class="col-xs-12">
                    <div class="mascara_visor">
                        <img class="image_resrc img-responsive img-thumbnail" src="" style="display: none;">
                        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/noimage.png" class="img-responsive center-block nopreview mask">
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="div_videos" data-tabgroup="navTabsTop" <?php echo !empty(@$this->videos['bitacora'])?'data-idbitacora='.$this->videos['bitacora']['idbitacora']:'' ?>>
                <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("Videos") ?></h1>
                <div class="col-xs-12 col-sm-2 padding-0 controles-edicion nopreview">
                    <a class="open-biblioteca btn btn-app btn-sm btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a video from our multimedia library or upload your own video')) ?>" data-tipo="video" data-url=".vidtemporal">
                        <i class="app-icon fa fa-plus-circle"></i>
                        <?php echo ucfirst(JrTexto::_('add')); ?>
                    </a>
                    <video class="hidden vidtemporal temp" src="" style="display: none"></video>
                </div>
                <div class="col-xs-12 col-sm-10 items-select-list video-selecc contenedor-slider">
                    <div class="myslider">
                        <?php //echo @$this->imagenes["caratula"];
                        @$vids = explode(",", @$this->videos["caratula"]);
                        $canttextv = explode(",", @$this->videos["texto"]);
                        if (@$this->videos["caratula"]){ $cantvid=count(@$vids)-1; }
                        else{ $cantvid=-1; }

                        for ($y=0;$y<=$cantvid;$y++){
                            $srcFile = @str_replace('__xRUTABASEx__', $rutabase, @$vids[$y]);
                            $clsVisto = (!empty(@$this->imagenes['bitacora']) && in_array($srcFile, $this->imagenes['bitacora']['otros_datos']))?'visto':'';
                            echo '<a href="#" class="item-select miniatura miniaturav '.$clsVisto.'"><span class="del-miniatura nopreview"></span><video src="'.$srcFile.'" alt="video" style="height: 65px; width: 100%" data-tit="'.$canttextv[$y].'" data-pos="'.$y.'" id="video'.$y.'"> </video></a>';
                        } ?>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-offset-3 col-xs-5 padding-0">
                        <input type="text"  id="txtDescv" name="txtDescv" required="required" class="form-control descripcion-media hidden" value="">
                    </div>
                    <div class="col-xs-4 padding-0 text-left">
                        <a class="btn btn-primary savevideo nopreview hidden"><i class="fa fa-save"></i> <span class="sr-only"><?php echo JrTexto::_('Save Video') ?></span></a>
                    </div>

                    <input type="hidden"  id="txtposv" name="txtposv" required="required" value="">
                    <input type="hidden" class="contenido-caratula" id="txtvideo" name="txtvideo" required="required" class="form-control" value="<?php echo @str_replace('__xRUTABASEx__', $rutabase, @$this->videos["caratula"]); ?>">
                    <input type="hidden" class="contenido-texto" id="txtTitv" name="txtTitv" required="required" class="form-control" value="<?php echo @$this->videos["texto"]?>">
                </div>
                <div class="col-xs-12">
                    <div class="mascara_visor">
                        <video controls="true" class="valvideo video_resrc" src="" style="display: none;"></video>
                        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/novideo.png" class="img-responsive center-block nopreview mask">
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <a href="#anclaDown" class="scroller down" id="scroll-down"><i class="fa fa-angle-double-down fa-2x"></i></a>
</div>
<a href="anclaDown" class="ancla" id="anclaDown"></a>


<div id="btns_progreso_intentos" style="display: none;">
    <div class="row" id="btns_control_alumno">
        <div class="col-sm-12 botones-control">
            <a class="btn btn-inline btn-blue btn-lg try-again"><i class="fa fa-undo"></i> <?php echo ucfirst(JrTexto::_('try again')); ?></a>
            <a class="btn btn-inline btn-success btn-lg save-progreso" ><?php echo ucfirst(JrTexto::_('continue')) ?> <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
</div>

<section class="hidden" id="msjes_idioma" style="display: none;">
    <input type="hidden" id="attention" value="<?php echo JrTexto::_('Attention');?>">
    <input type="hidden" id="no_puedes_arrastrar" value='<?php echo JrTexto::_('You can not drag another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="no_puedes_seleccionar" value='<?php echo JrTexto::_('You can not select another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="you_must_finish" value='<?php echo ucfirst(JrTexto::_('you must do the exercise before trying again')); ?>'>
    <input type="hidden" id="bien_hecho" value='<?php echo ucfirst(JrTexto::_('well done!')); ?>'>
    <input type="hidden" id="cargando" value='<?php echo ucfirst(JrTexto::_('Loading')); ?>'>
    <input type="hidden" id="ejerc_exito" value='<?php echo ucfirst(JrTexto::_('you have complete the exercise succesfully')); ?>.'>
    <input type="hidden" id="ir_sgte_ejerc" value='<?php echo ucfirst(JrTexto::_('continue to the next exercise')); ?>.'>
    <input type="hidden" id="finalizar" value='<?php echo ucfirst(JrTexto::_('you have finished the activity')); ?>.'>
    <input type="hidden" id="puedes_continuar" value='<?php echo ucfirst(JrTexto::_('you can continue next exercise')); ?>.'>
    <input type="hidden" id="tiempo_acabo" value='<?php echo ucfirst(JrTexto::_('time is over')); ?>.'>
    <input type="hidden" id="intentalo_otra_vez" value='<?php echo ucfirst(JrTexto::_('you can try again')); ?>.'>
    <input type="hidden" id="select_activity" value='<?php echo ucfirst(JrTexto::_('Select Activity')); ?>.'>
    <input type="hidden" id="select_upload" value='<?php echo JrTexto::_('Select or upload'); ?>'>
    <input type="hidden" id="click_add_description" value='<?php echo JrTexto::_('Click here to add description') ?>'>
    <input type="hidden" id="click_add_title" value='<?php echo JrTexto::_('Click here to add title') ?>'>
    <input type="hidden" id="remove_tab" value='<?php echo JrTexto::_('remove tab') ?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="no_hay_audio" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback')); ?>'>
    <input type="hidden" id="no_hay_audio_en_escena" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback in scene')); ?>'>
    <input type="hidden" id="no_hay_orden_en_escena" value='<?php echo ucfirst(JrTexto::_('you have not set an order in the dialogues in scene')); ?>'>
    <input type="hidden" id="debes_establecer_orden" value='<?php echo ucfirst(JrTexto::_('you must set an order for the dialog bubbles')); ?>'>
    <input type="hidden" id="write_tag" value='<?php echo ucfirst(JrTexto::_('write tag')); ?>'>
    <input type="hidden" id="ejerc_no_completado" value='<?php echo ucfirst(JrTexto::_('you have not finished editing this exercise')); ?>'>
    <input type="hidden" id="grabar_audio" value='<?php echo ucfirst(JrTexto::_('Record')); ?>'>
    <input type="hidden" id="detener_audio" value='<?php echo ucfirst(JrTexto::_('Stop')); ?>'>
    <input type="hidden" id="escribir_texto" value='<?php echo ucfirst(JrTexto::_('Write')); ?>'>
    <input type="hidden" id="reproducir_audio" value='<?php echo ucfirst(JrTexto::_('Play')); ?>'>
    <input type="hidden" id="end" value='<?php echo ucfirst(JrTexto::_('End')); ?>'>
</section>

<script type="text/javascript">
//document.getElementById('div_imagen').style.display="none";
var _IDHistorialSesion=0;

var optSlike={
    infinite: false,
    navigation: false,
    slidesToScroll: 1,
    centerPadding: '10px',
    slidesToShow: 7,
    responsive:[
    { breakpoint: 1200, settings: {slidesToShow: 6} },
    { breakpoint: 992, settings: {slidesToShow: 5 } },
    { breakpoint: 880, settings: {slidesToShow: 4 } },
    { breakpoint: 720, settings: {slidesToShow: 3 } },
    { breakpoint: 320, settings: {slidesToShow: 2} }
    ]
};

var pos=-1;
var posv=-1;
var showdiv=[];

function fileExists(url) {
    var rspta = false;
    if(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.send();
        rspta = (req.status==200);
    }
    return rspta;
}

function existeEnSlider(url, $panel){
    var rspta = true;
    if(url){
        var archivo = $panel.find('.myslider *[src="'+url+'"]');
        rspta = (archivo.length>0);
    }
    return rspta;
}

function insertarFileLista(){
    var tipo_file = $('#tabContent .tab-pane.active .open-biblioteca').attr('data-tipo');
    var tag_file = '<span class="del-miniatura nopreview"></span>';
    if(tipo_file=='video'){
        var $panel = $('#div_videos');
        var src = $panel.find('.vidtemporal').attr('src');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        var posv = $panel.find('.miniatura').length-1;
        if(existeArchivo){
            posv = posv + 1;
            tag_file += '<video src="'+src+'" alt="'+tipo_file+'" style="height: 65px; width: 100%" data-pos="'+posv+'" data-tit="null" id="video'+posv+'"> </video>';

            var vidold= $('#txtvideo').val();
            if (vidold) vidold=vidold+',';
            $('#txtvideo').val( vidold+src);

            $('#txtDescv').val('null');
            
            var txtTitv= $('#txtTitv').val();
            if (txtTitv) txtTitv=txtTitv+',';
            $('#txtTitv').val( txtTitv+'null');
            //alert("as");
            class_mini ="miniaturav";
            var tipo_file = 'V';
            var sw = 'OK';
        }
    }else if(tipo_file=='image'){
        var $panel = $('#div_imagen');
        var src = $panel.find('.imgtemporal').attr('src');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        var pos = $panel.find('.miniatura').length-1;
        if(existeArchivo){
            pos = pos + 1;
            tag_file += '<img src="'+src+'" alt="'+tipo_file+'" style="height: 86px; width: 100%" data-pos="'+pos+'" data-tit="null" id="img'+pos+'">';
            
            var imgold= $('#txtimg').val();
            if (imgold) imgold=imgold+',';
            $('#txtimg').val( imgold+src);

            $('#txtDesc').val('null');
            
            var txtTit= $('#txtTit').val();
            if (txtTit) txtTit=txtTit+',';
            $('#txtTit').val( txtTit+'null');
       
            class_mini ="miniaturai";
            var tipo_file = 'I';
            var sw = 'OK';
        }
    }else if(tipo_file=='audio'){
        var $panel = $('#div_audios');
        var dataAudio = $panel.find('.audtemporal').attr('data-audio');
        var src = _sysUrlStatic_+'/media/audio/'+dataAudio;
        var nombre = $panel.find('.audtemporal').attr('data-nombre-file');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        var posA = $panel.find('.miniatura').length-1;
        if(existeArchivo){
            posA = posA + 1;

            tag_file += '<span hre="#" class="file color-orange" src="'+src+'" alt="'+tipo_file+'" data-pos="'+posA+'" data-tit="'+nombre+'" id="img'+posA+'"><i class="fa fa-file-audio-o fa-4x"></i></span>';

            var audioOld= $('#txtaudio').val();
            if (audioOld) audioOld=audioOld+',';
            $('#txtaudio').val( audioOld+src);

            $('#txtDesca').val(nombre);

            var txtTita= $('#txtTita').val();
            if (txtTita) txtTita=txtTita+',';
            $('#txtTita').val( txtTita+nombre);

            class_mini ="miniatura_a";
            var tipo_file = 'A';
            var sw = 'OK';
        }
    }else if(tipo_file=='pdf'){
        var $panel = $('#div_pdfs');
        var src = $panel.find('.ifrtemporal').attr('src');
        var nombre = $panel.find('.ifrtemporal').attr('data-nombre-file');
        var existeArchivo = fileExists(src);
        var estaCargadoEnSlider = existeEnSlider(src, $panel);
        var posv = $panel.find('.miniatura').length-1;
        if(existeArchivo){
            posv = posv + 1;

            tag_file += '<div class="file color-danger" src="'+src+'" alt="'+tipo_file+'" data-pos="'+posv+'" data-tit="'+nombre+'" id="img'+posv+'"><i class="fa fa-file-pdf-o fa-4x"></i><span class="nombre-file">'+nombre+'</span></div>';

            var pdfold= $('#txtpdf').val();
            if (pdfold) pdfold=pdfold+',';
            $('#txtpdf').val( pdfold+src);

            $('#txtDescp').val(nombre);

            $('#txtposp').val(posv);
            var txtTitp= $('#txtTitp').val();
            if (txtTitp) txtTitp=txtTitp+',';
            $('#txtTitp').val( txtTitp+'null');

            class_mini ="miniaturap";
            var tipo_file = 'D';
            var sw = nombre;
        }
    }

    if(existeArchivo){
        if(!estaCargadoEnSlider){
            var _html = '<a href="#" class="miniatura '+class_mini+'">'+tag_file+'</a>';
            $panel.find('.myslider').slick('unslick');
            $panel.find('.myslider').append(_html);
            $panel.find('.myslider').slick(optSlike);
            $panel.find('.temp').attr('src','');
            guardarTResource(tipo_file,src,sw);
        } else {
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo JrTexto::_('The file has been already loaded');?>.', 'warning');
        }
    } else {
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo JrTexto::_('The file does not exist');?>.', 'error');
    }
}

var guardarTResource = function($tipo,$file,$sw){
    var data = new Array();
    
    data.idNivel = <?php echo $this->idnivel?>;
    data.idUnidad = <?php echo $this->idunidad?>;
    data.idActividad = <?php echo $this->idactividad?>;
    data.tipo = $tipo;
    data.orden = <?php echo $this->orden; ?>;
    data.compartido=$('.btncompartir').hasClass('btn-inactive')?0:1;
    data.publicado=$('.btnpublicar').hasClass('btn-inactive')?0:1;

    if ($tipo=='P'){
        data.texto= $('#txtarea_pnl1<?php echo $idgui; ?>').val();
        data.titulo= $('#txtTitulo').val();
        data.caratula= $('#div_presentacion').find('.imgtemporal').attr('src');
    }else if ($tipo=='I'){

        data.texto= $('#txtTit').val();
        data.titulo= "";
        data.pos= $('#txtpos').val();            
        //alert(data.pos);            
        data.caratula= $('#txtimg').val();

        if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$('#txtDesc').val();
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTit').val(texto);
            data.texto= $('#txtTit').val();

            $("#img"+data.pos).attr("data-tit",$('#txtDesc').val());

            data.titulo= "";
            data.pos= "null";
        }
    }else if ($tipo=='V'){
        data.texto= $('#txtTitv').val();
        data.titulo= "";
        data.pos= $('#txtposv').val();
        data.caratula= $('#txtvideo').val();
        //alert(data.caratula);
        if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$('#txtDescv').val();
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTitv').val(texto);
            data.texto= $('#txtTitv').val();
            $("#video"+data.pos).attr("data-tit",$('#txtDescv').val());
            data.titulo= "";
            data.pos= "null";
        }
    }else if ($tipo=='A'){
        data.texto= $('#txtTita').val();
        data.titulo= "";
        data.pos= $('#txtposa').val();
        data.caratula= $('#txtaudio').val();
        //alert(data.caratula);
        if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$('#txtDesca').val();
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }            
            $('#txtTita').val(texto);
            data.texto= $('#txtTita').val();
            $("#video"+data.pos).attr("data-tit",$('#txtDesca').val());
            data.titulo= "";
            data.pos= "null";
        }
    }else if ($tipo=='D'){
        data.texto= $('#txtTitp').val();
        data.titulo= "";
        data.pos= $('#txtposp').val();
        data.caratula= $('#txtpdf').val();
        //alert(data.caratula);

        //if ($sw!='OK'){
            //alert("as");
            var str = data.texto;
            var res = str.split(",");
            var texto="";
            for (y=0;y<=res.length-1;y++){
                if (data.pos==y){
                    texto=texto+$sw;
                }else{
                    texto=texto+res[y];
                }
                if (y!=res.length-1) texto=texto+',';
            }
            $('#txtTitp').val(texto);
            data.texto= $('#txtTitp').val();
            $("#pdf"+data.pos).attr("data-tit",$('#txtDescp').val());
            data.titulo= "";
            data.pos= "null";
        //}
        //retunr;
    }else if ($tipo=='O'){
        data.titulo = 'Vocabulary';
        data.texto = '';
        data.caratula = $file; /*para vocabulary, $file es un Json en string.*/
    }else if($tipo=='E'){
        data.titulo = 'Exercises';
        data.texto = '';
        data.caratula = $file;
    }else if($tipo=='G'){
        data.titulo = 'Games';
        data.texto = '';
        data.caratula = $file;
    }else if($tipo=='X'){
        data.titulo = 'Exam';
        data.texto = '';
        data.caratula = $file;
    }
    //alert($file);
    //data.pkIdlink= idpk||0;
    var res = xajax__('', 'Resources', 'saveResource', data);
    if(res){
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo ucfirst(JrTexto::_('saved successfully'));?>.', 'success');
    }
};

var showPrimerElemento = function ($contenedor) {
    if($contenedor.length==0){ return false; }
    $contenedor.find('.items-select-list .item-select').first().trigger('click');
    $contenedor.find('.items-select-list').removeClass('items-select-list');
}

var calcularProgresoMultimedia = function($tabPane){
    var porcentaje = 0.0;
    var numDivs=$tabPane.find('.myslider .miniatura').length;
    var numDivsVistos=$tabPane.find('.myslider .miniatura.visto').length;
    if(numDivs>0){
        porcentaje=(numDivsVistos/numDivs)*100;
    }
    return porcentaje;
};  

var totalPestanias = function(){
    /* Solo las pestañas (top o left) visibles en el smartbook */
    var cant = 0;
    $("#navTabsTop, #navTabsLeft").find('li>a').not('a[href="#div_presentacion"],a#exit').each(function(i, elem) {
        var isVisible = $(elem).hasClass('hidden') || $(elem).is(':visible')
        if(isVisible){ cant++; }
    });

    cant--; /* la pestaña div_games aun no se guarda, por loq  no se está considerando. 
                Esta linea deberá borrarse una vez implementado el guardar de juegos */

    return cant;
};

var guardarBitacora = function(dataSave, $tabPane){
    if($('#rolUsuario').val()!='Alumno'){ console.log('No es Alumno - No guardar'); return false; }
    $.ajax({
        url: _sysUrlBase_+'/bitacora_smartbook/guardarBitacora_smartbook',
        type: 'POST',
        dataType: 'json',
        data: dataSave,
    }).done(function(resp) {
        if(resp.code=='ok'){
            var idLogro = $('#idLogro').val();
            var idSesion = $('#idSesion').val();
            var tieneLogro = idLogro>0;
            $tabPane.attr('data-idbitacora', resp.newid);
            if(resp.progreso>=100 && tieneLogro && resp.bandera==0){
                mostrarLogroObtenido({'id_logro':idLogro, 'idrecurso': idSesion  });
            }
        }
    }).fail(function(e) {
        console.log("!Error", e);
    });
};

var show_hide_Scroller = function($tab=null) {
    if($tab===null) return false;
    var containerHeight = $tab.closest('div.tab-pane[data-tabgroup="navTabsLeft"]').outerHeight();
    var contentHeight = 0;
    $tab.children().each(function(index, el) {
        contentHeight += $(el).outerHeight();
    });
    if( !$('#contenedor-paneles').hasClass('hidden') ){ contentHeight += $('#contenedor-paneles').outerHeight()+25; }
    if(contentHeight > containerHeight){
        $('#scroll-down').show();
    }else{
        $('#scroll-down').hide();
    }
};

var contarNumeroEjercicio = function($tabPane) {
    var $pnlNroEjercicio = $tabPane.find('.numero_ejercicio');
    if($pnlNroEjercicio.length===0){ return false; }
    
    var total_txt = $tabPane.find('ul.nav.ejercicios li').not('.comodin').length;
    var $actualTab = $tabPane.find('ul.nav.ejercicios li.active');
    var actual_txt = $actualTab.find('a[data-toggle="tab"]').text();
    if($actualTab.length==0){ actual_txt = MSJES_PHP.end; total_txt=''; }
    $pnlNroEjercicio.find('.actual').text(actual_txt);
    $pnlNroEjercicio.find('.total').text(total_txt);
    $('.infouserejericio.ejeinfo-numero .actual').text(actual_txt);
    $('.infouserejericio.ejeinfo-numero .total').text(total_txt);
};

var initBitac_Alum_Smbook = function () {
    if( $('#idBitacoraAlumnoSmbook').val()!=='' || $('#rolUsuario').val()!='Alumno' ) { return false; }
    $.ajax({
        url: _sysUrlBase_ + '/bitacora_alumno_smartbook/xGuardar',
        type: 'POST',
        dataType: 'json',
        data: {
            'txtIdcurso' : $('#idCurso').val(),
            'txtIdsesion' : $('#idSesion').val(),
            'txtEstado' : 'P',
        },
    }).done(function(resp) {
        if(resp.code=='ok'){
            $('#idBitacoraAlumnoSmbook').val(resp.newid);
        }
    }).fail(function(err) {
        console.log("!Error", err);
    }).always(function() {});
};

var resetearEjercicio_=function(aquiplantilla,tipo){
    let idmet=aquiplantilla.closest('.metod').attr('data-idmet');
    let plantilla=aquiplantilla.find('.plantilla'); 
    if(idmet==3){
        $('.infouserejericio.ejeinfo-tiempo').show().css({'opacity':1}).removeClass('hidden');
        $('.infouserejericio.ejeinfo-intento').show().css({'opacity':1}).removeClass('hidden');
        if(!plantilla.hasClass('yalointento')){
            $('.btnejercicio-next_').attr('disabled',true).addClass('disabled');           
            resetAllplantilla(plantilla);
        }else{
            $('.btnejercicio-next_').removeAttr('disabled').removeClass('disabled');
        }
        let intento_actual=parseInt(plantilla.attr('data-intento')||1);
        $('#panel-intentos-dby').find('.actual').text(intento_actual);
    }else{
        $('.infouserejericio.ejeinfo-tiempo').hide();
        $('.infouserejericio.ejeinfo-intento').hide();       
        $('.btnejercicio-next_').removeAttr('disabled').removeClass('disabled');
    }
    let pnlternativas=plantilla.find('.panelAlternativas');
    if(pnlternativas.length>0){
        txtlternativas=pnlternativas.text().replace(/\s||\n||\r/g,'');           
        if(txtlternativas=='') pnlternativas.parent().hide();
        else pnlternativas.css({'margin-bottom':'0px'});                    
    }       
    $('#btns_control_alumno .botones-control').css({'margin-top':'1.3em'});
}

/*var validartodosok=function(met,tabhijo){
    let plantilla=tabhijo.find('.plantilla');

}*/

$(document).ready(function(){
    $('.tab-pane[data-tabgroup="navTabsLeft"]').find('.nopreview').remove(); /*ejercicios y demás: quitar .nopreview*/
    $('#div_voca').find('.btnremovevoca').remove(); /*vocabulario: quitar .btnremovevoca*/
    setTimeout(function() {
        $( "#div_presentacion .div_acciones" ).fadeIn( "fast" );
    }, 2100);

    initBitac_Alum_Smbook();

    $('.btnejercicio-back_').on('click',function(ev){
        let tabPaneActivo=$('#div_practice.active');
        if(tabPaneActivo.length==0) tabPaneActivo=$('#div_autoevaluar.active');
        let tabejercicioactivo=tabPaneActivo.find('ul.ejercicios').children('li.active');
        let indexhijo=tabejercicioactivo.index();
        if(indexhijo==0){
            idtab=tabPaneActivo.attr('id');
            if(idtab=='div_practice') $('a[href="#div_look"]').trigger('click');
            else if(idtab=='div_autoevaluar') $('a[href="#div_voca"]').trigger('click');
        }else{            
            tabejercicioactivo.prev('li').children('a').trigger('click');
        }
    })

    $('.btnejercicio-next_').on('click',function(ev){
        $('#panel-tiempo .info-show').trigger('oncropausar');
        guardarProgreso();
        $(this).addClass('hide');
        let tabPaneActivo=$('#div_practice.active');
        if(tabPaneActivo.length==0) tabPaneActivo=$('#div_autoevaluar.active');
        let progreso = parseFloat( tabPaneActivo.find('.metod').attr('data-progreso'));
        guardarBitacora({
            'Idbitacora' : tabPaneActivo.attr('data-idbitacora')||null,
            'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val(),
            'txtIdcurso' : $('#idCurso').val(),
            'txtIdsesion' : $('#idSesion').val(),
            'txtPestania' : tabPaneActivo.attr('id'),
            'txtTotal_Pestanias' : totalPestanias(),
            'txtProgreso' : progreso.toFixed(2),
            'txtOtros_datos' : null,
            'txtIdlogro' : $('#idLogro').val(),
        }, tabPaneActivo); 
            
        let tabactivo=tabPaneActivo.children('.tabhijo.active');
        let tabejercicioactivo=tabPaneActivo.find('ul.ejercicios').children('li.active');
        let indexhijo=tabejercicioactivo.index();
        let totalhijos=tabPaneActivo.find('ul.ejercicios').find('li').length-2;
        if(indexhijo==totalhijos){           
            idtab=tabPaneActivo.attr('id');
            if(idtab=='div_practice') $('a[href="#div_games"]').trigger('click');
            else if(idtab=='div_autoevaluar'){
                let href=$('a#exit').attr('href');
                console.log(' ir a cargar siguiente sesion',href);
                window.location.href=href;
            }
        }else{
            tabejercicioactivo.next('li').children('a').trigger('click');
        } 
        $(this).removeClass('hide');
    })

    $('#smartbook-ver .nav-tabs').on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {        
        var $actualTab = $(this);
        var idTabPane = $actualTab.attr('href');
        var $tabpane = $(idTabPane);
        var $slider = $tabpane.find('.myslider');
        var idNavTab = $actualTab.closest('.nav-tabs').attr('id');
        setTimeout(function() { show_hide_Scroller($tabpane); }, 500);
        contarNumeroEjercicio($tabpane);

        if( $slider.length>0 && !$slider.hasClass('slick-initialized')){
            $slider.slick(optSlike);
        }
        showPrimerElemento($tabpane);
        $( "#div_presentacion .div_acciones" ).hide();

        $('audio').each(function(){ this.pause() });
        $('video').each(function(){ this.pause() });
        console.log(idNavTab,idTabPane);
        if( idNavTab==='navTabsTop' || idNavTab==='navTabsLeft' ){
            console.log(1);
            $('#smartbook-ver #navTabsTop a[data-toggle="tab"], #smartbook-ver #navTabsLeft a[data-toggle="tab"]').not($actualTab).closest('li.active').removeClass('active');            
            if(idTabPane==="#div_look" || idTabPane==="#div_practice" || idTabPane==="#div_autoevaluar"){ 

                $('.tab-pane .content-smartic.metod.active').removeClass('active');
                $tabpane.find('.content-smartic.metod').addClass('active');
                if(!$tabpane.hasClass('smartic-initialized')){
                    $tabpane.tplcompletar({editando:false});
                }
                rinittemplatealshow();
                if(idTabPane==="#div_look"){
                   $('#contenedor-paneles').addClass('hidden');  // oculta el progreso;
                }else if(idTabPane==="#div_practice"){
                    $('#contenedor-paneles').removeClass('hidden'); // muestra progreso
                    let panelActivo=$('#div_practice').find('.tabhijo.active');
                    resetearEjercicio_(panelActivo);
                }else{
                    $('#contenedor-paneles').removeClass('hidden'); // muestra progreso
                    let panelActivo=$('#div_autoevaluar').find('.tabhijo.active');                    
                    resetearEjercicio_(panelActivo);
                }//
            }else{
                $('#contenedor-paneles').addClass('hidden'); // muestra el progreso
            }

            if(idNavTab==='navTabsLeft' && (idTabPane==="#div_look" || idTabPane==="#div_voca" || idTabPane==="#div_workbook")){
                var $tabPane = $(idTabPane);
                var progreso = 100.00;
                guardarBitacora({
                    'Idbitacora' : $tabPane.attr('data-idbitacora')||null,
                    'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val()||null,
                    'txtIdcurso' : $('#idCurso').val(),
                    'txtIdsesion' : $('#idSesion').val(),
                    'txtPestania' : $tabPane.attr('id'),
                    'txtTotal_Pestanias' : totalPestanias(),
                    'txtProgreso' : progreso.toFixed(2),
                    'txtOtros_datos' : null,
                    'txtIdlogro' : $('#idLogro').val(),
                }, $tabPane);
            }
        }
    });

    $('#smartbook-ver ul.nav.ejercicios').on('shown.bs.tab', 'a[data-toggle="tab"]', function(ev) {
        ev.preventDefault();
        let idTab = $(this).attr('href');
        let $tabPane = $(idTab).closest('div[data-tabgroup="navTabsLeft"]');
        contarNumeroEjercicio($tabPane);
        let $tabhijoActive=$tabPane.find('.tabhijo.active');        
        let totalpuntaje=100/$tabPane.find('.tabhijo').length; // total puntaje por ejericio.        
        resetearEjercicio_($tabhijoActive);        
    })

    $('#div_practice, #div_autoevaluar').on('click', '#btns_control_alumno .save-progreso', function(e) {
        e.preventDefault();
        var $tabPane = $(this).closest('[data-tabgroup]');
        var progreso = parseFloat( $tabPane.find('.metod').attr('data-progreso') );
        guardarBitacora({
            'Idbitacora' : $tabPane.attr('data-idbitacora')||null,
            'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val(),
            'txtIdcurso' : $('#idCurso').val(),
            'txtIdsesion' : $('#idSesion').val(),
            'txtPestania' : $tabPane.attr('id'),
            'txtTotal_Pestanias' : totalPestanias(),
            'txtProgreso' : progreso.toFixed(2),
            'txtOtros_datos' : null,
            'txtIdlogro' : $('#idLogro').val(),
        }, $tabPane);
    });

    $('.btnpublicar').click(function(ev){
        var obj=$(this);
        var data =$(this).hasClass('btn-inactive')?1:0;
       if(data==1){
           $(this).removeClass('btn-inactive').addClass('btn-success');
        }else{
           $(this).addClass('btn-inactive').removeClass('btn-success');
        }
        var parent=$(this).parent();
        var id=parent.attr('data-id'); 
        if(id!=undefined&&id!=''){
            var res = xajax__('', 'Recursos', 'setCampo', id,'publicado',data);
        }
    });

    $('.btncompartir').click(function(ev){
        var obj=$(this);
        var data =$(this).hasClass('btn-inactive')?1:0;
        if(data==1){
           $(this).removeClass('btn-inactive').addClass('btn-primary');
        }else{
           $(this).addClass('btn-inactive').removeClass('btn-primary');
        }
        var parent=$(this).parent();
        var id=parent.attr('data-id');
        if(id!=undefined&&id!=''){
            var res = xajax__('', 'Recursos', 'setCampo', id,'compartido',data);
        }
    });

    $('#smartbook-ver .preview').click(function(){
        $(this).hide();
        $(this).siblings('.back').show();
        $('#navTabsLeft li a[data-toggle="tab"]').first().trigger('click');
        $('.controles-edicion').hide();
        $('#smartbook-ver .descripcion-media').attr('readonly','readonly');  
        $('#smartbook-ver .nopreview').hide();
        $('#smartbook-ver').removeClass('editando');
    });

    $('#smartbook-ver .back').click(function(){
        $(this).hide();
        $(this).siblings('.preview').show();
        $('.controles-edicion').show();
        $('#smartbook-ver .descripcion-media').removeAttr('readonly');
        $('#smartbook-ver .nopreview').show();
        $('#smartbook-ver').addClass('editando');
    });

    var iScrollPos = 0;
    $('#smartbook-ver #tabContent > .tab-pane').scroll(function(e) {
        var iCurScrollPos = $(this).scrollTop();
        var tabPaneHeight = $(this).outerHeight();
        var contentHeight = 0;

        $(this).children().each(function(index, el) {
            contentHeight += $(el).outerHeight(); //+71;
        });

        if( !$('#contenedor-paneles').hasClass('hidden') ){ contentHeight += $('#contenedor-paneles').outerHeight()-3; }

        if (iCurScrollPos > iScrollPos) {
            $('#scroll-up').show();
            /*console.log(iCurScrollPos+' + '+tabPaneHeight+' +10= ', iCurScrollPos + tabPaneHeight+10);
            console.log('contentHeight = ', contentHeight);*/
            if(iCurScrollPos + tabPaneHeight-10 >= contentHeight) { /* LLego al final?*/
                $('#scroll-down').hide();
            }else {
                $('#scroll-down').show();
            }
        } else {
            $('#scroll-down').show();
            if(iCurScrollPos==0){ /* LLego al inicio?*/
                $('#scroll-up').hide();
            }else {
                $('#scroll-up').show();
            }
        }
        iScrollPos = iCurScrollPos;
    });

    $('#scroll-down').click(function(e) {
        //e.preventDefault();
        $('#smartbook-ver #tabContent > .tab-pane.active').animate({ scrollTop: 750}, 1200, 'linear');

    });
    $('#scroll-up').click(function(e) {
        //e.preventDefault();
        $('#smartbook-ver #tabContent > .tab-pane.active').animate({ scrollTop: -750}, 1200, 'linear');
    });

/*** #Fn generales(Pdf,Aud,Img,Vid) ***/
    $('#tabContent .tab-pane')
        .on('click',".open-biblioteca",function(e){ 
            e.preventDefault();
            var txt= '<?php echo JrTexto::_('Select or upload'); ?>';
            selectedfile(e,this,txt, 'insertarFileLista');
        }).on('click',".miniaturai",function(e){ 
            e.preventDefault();
            var id = $(this).closest('.tab-pane').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $panel.find('#txtDesc').val(tit).removeClass('hidden');
            $panel.find('.btn.saveimage').removeClass('hidden');
            $panel.find('#txtpos').val(pos);
        }).on('click',".miniaturav",function(e){ 
            e.preventDefault();
            var id = $(this).closest('.tab-pane').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $panel.find('#txtDescv').val(tit).removeClass('hidden');
            $panel.find('.btn.savevideo').removeClass('hidden');
            $panel.find('#txtposv').val(pos);
            
            //alert("as");
        }).on('click',".miniatura_a",function(e){ 
            e.preventDefault();
            var id = $(this).closest('.tab-pane').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $panel.find('#txtDesca').val(tit).removeClass('hidden');
            $panel.find('.btn.saveaudio').removeClass('hidden');
            $panel.find('#txtposa').val(pos);
            
            //alert("as");
        }).on('click',".miniaturap",function(e){ 
            e.preventDefault();
            var id = $(this).closest('.tab-pane').attr('id');
            var $panel = $('#'+id);
            var $media = $(this).children('*[src]');
            var tipo = $media.attr('alt');
            var src = $media.attr('src');
            var tit = $media.attr('data-tit');
            var pos = $media.attr('data-pos');
            
            $panel.find('.'+tipo+'_resrc').attr('src', src).show();
            $panel.find('.mask').addClass('hidden');
            //document.getElementById('txtDesc').value=tit;
            $panel.find('#txtDescp').val(tit).removeClass('hidden');
            $panel.find('.btn.savepdf').removeClass('hidden');
            $panel.find('#txtposp').val(pos);
            
            //alert("as");
        }).on('click', '.miniatura>.del-miniatura', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var idPanel = $(this).closest('.tab-pane').attr('id');
            var $miniatura = $(this).parent();
            var src = $miniatura.children('*[src]').attr('src');
            var tipo = $miniatura.children('*[src]').attr('alt');
            var $slider = $(this).parents('.myslider');

            var all_media = $('#'+idPanel+' .contenido-caratula').val()
            var all_textos = $('#'+idPanel+' .contenido-texto').val()
            var arrMedia = all_media.split(',');
            var arrTextos = all_textos.split(',');
            var index = arrMedia.indexOf(src);
            var sw = 'OK';
            arrMedia.splice(index, 1);
            arrTextos.splice(index, 1);

            var new_media = arrMedia.join(',');
            var new_textos = arrTextos.join(',');
            $('#'+idPanel+' .contenido-caratula').attr('value', new_media);
            $('#'+idPanel+' .contenido-texto').attr('value', new_textos);

            $slider.slick('unslick');
            $miniatura.remove();
            $slider.slick(optSlike);
            if(tipo=='image'){
                var type = 'I';
            }else if(tipo=='video'){
                var type = 'V';
            }else if(tipo=='audio'){
                var type = 'A';
            }else if(tipo=='pdf'){
                var type = 'D';
                sw = ''
            }
            guardarTResource(type, '', sw);
        }).on('click', '.miniatura', function(e) {
            e.preventDefault();
            if($(this).hasClass('visto')){ return false; }
            $(this).addClass('visto');
            var $tabPane = $(this).closest('.tab-pane');
            var progreso = calcularProgresoMultimedia($tabPane);
            var archVistos = [];
            $tabPane.find('.myslider .miniatura.visto').each(function(i, elem) {
                var source = $(elem).find('*[src]').attr('src');
                archVistos.push(source);
            });
            guardarBitacora({
                'Idbitacora' : $tabPane.attr('data-idbitacora')||null,
                'Idbitacora_smartbook' : $('idBitacoraAlumnoSmbook').val(),
                'txtIdcurso' : $('#idCurso').val(),
                'txtIdsesion' : $('#idSesion').val(),
                'txtPestania' : $tabPane.attr('id'),
                'txtTotal_Pestanias' : totalPestanias(),
                'txtProgreso' : progreso.toFixed(2),
                'txtOtros_datos' : JSON.stringify(archVistos),
                'txtIdlogro' : $('#idLogro').val(),
            }, $tabPane);
        });

    $('#smartbook-ver')
        .on('click', '.live-edit', function(e){
            if(!$('#smartbook-ver').hasClass('editando')){
                return false;
            }
            $(this).css({"border": "0px"}); 
            addtext1(e,this);
        }).on('blur','.live-edit>input',function(e){
            e.preventDefault();
            $(this).closest('.live-edit').removeAttr('Style'); 
            addtext1blur(e,this);
        }).on('keypress','.live-edit>input', function(e){
            if(e.which == 13){ 
                e.preventDefault();          
                $(this).trigger('blur');
            } 
        }).on('keyup','input', function(e){
            if(e.which == 27){ 
                $(this).attr('data-esc',"1");
                $(this).trigger('blur');
            }
        });

    $('#div_presentacion').on('click', '.btn-continuar', function(e) {
        e.preventDefault();
        $('.nav-tabs li.ultimo_visto a').trigger('click');
    });
    
    $('#div_imagen').on('click','.saveimage',function(){
        //alert("img");
        var txtimg= $('#txtimg').val();
        //alert(txtimg);
        guardarTResource('I',txtimg,'');
    });

    $('#div_videos').on('click','.savevideo',function(){
        //alert("img");
        var txtvid= $('#txtvid').val();        
        //alert(txtimg);
        guardarTResource('V',txtvid,'');
    });

    $('#div_audios').on('click','.saveaudio',function(){
        //alert("img");
        var txtaud= $('#txtaudio').val();        
        //alert(txtimg);
        guardarTResource('A',txtaud,'');
    });

    $('#div_pdfs').on('click','.savepdf',function(){
        //alert("img");
        var txtpdf= $('#txtpdf').val();        
        //alert(txtimg);
        guardarTResource('D',txtpdf,'');
    });

    $('#div_games').on('click', '.nav-tabs a[data-toggle="tab"]', function(e) {
        e.preventDefault();
        var idGame = $(this).data('idgame');
        var $iframe = $(this).closest('.tab-pane').find('iframe');
        $iframe.attr('src', _sysUrlBase_+'/game/ver/'+idGame);
    });

<?php if(strtolower($this->rolActivo)=='alumno'){?>
    $('#smartbook-ver .preview').trigger('click');
    $('#smartbook-ver .preview').hide();
    $('#smartbook-ver .back').hide();
<?php } ?>

    $('#smartbook-ver .plantilla-speach').each(function(index, el) {
        initspeach( $(el) );
        if($(el).closest('.tabhijo').hasClass('active')){
            rinittemplatealshow();
        }
    });

/******************************************************/
    var registrarHistorialSesion = function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/agregar',
            type: 'POST',
            dataType: 'json',
            data: {'lugar': 'TR', 'fechaentrada': fechahora, 'idcurso': $('#idCurso').val() },
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
        });
        
    };

    registrarHistorialSesion();
    $(window).on('beforeunload', function(){
        var now = new Date();
        var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/editar',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {'idhistorialsesion': _IDHistorialSesion, 'fechasalida': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                _IDHistorialSesion = resp.data.idhistorialsesion;
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            return false;
        });
    });
});

</script>