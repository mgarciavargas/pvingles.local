<?php $usuarioAct = NegSesion::getUsuario(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/smartbook/listar.css">

<div class="container">
  <div class="row" id="smartbook-listar">
    <div class="row" id="levels">
      <div class="col-md-6">
        <ol class="breadcrumb">
          <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
          <?php foreach ($this->breadcrumb as $b) {
          $enlace = '<li>';
          if(!empty($b['link'])){ 
            $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; 
          }else{ 
            $enlace .= ucfirst(JrTexto::_($b['texto'])); }
            $enlace .= '</li>';
            echo $enlace;
          } ?>
        </ol>
      </div>
      <div class="col-md-6 padding-0">
        <div class="col-sm-offset-6 col-sm-6 select-ctrl-wrapper select-azul">
          <select name="level" id="level-item" class="form-control select-ctrl">
          <?php if(!empty($this->niveles))
          foreach ($this->niveles as $nivel){?>
          <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $nivel["idnivel"]==$this->idnivel?'Selected':'';?>><?php echo JrTexto::_('level')."-".$nivel["nombre"]?></option>
          <?php }?>               
          </select>
        </div>
      </div>
    </div>
    <div class="row" id="units">
      <h3 class="col-xs-12 unit-title"><?php echo JrTexto::_('Unit'); ?><!--span id="nameunit"></span--></h3>
      <div class="col-xs-12 unit-wrapper">
        <div class="unit-list" style="position: relative;">
          <?php 
          $i=0;
          if(!empty($this->unidades))
            foreach ($this->unidades as $unidad){ $i++;
              $img='';
              if(!empty($unidad["imagen"])){
                $img='<img class="caratula-libro" src="'.(str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$unidad["imagen"])).'">';
              }                    
              ?>                  
            <div class="item-tools rname" data-iname="nameunit" title="<?php echo $unidad["nombre"]; ?>" data-idnivel="<?php echo $unidad["idnivel"]; ?>">                     
              <a href="javascript:void(0)" class="hvr-float unit-item <?php echo $unidad["idnivel"]==$this->idunidad?'active':'';?> ">
                <?php echo $img; ?>
                <div class="unit-name"><?php echo $unidad["nombre"]; //str_pad($i,2,"0",STR_PAD_LEFT); ?></div>
              </a>
            </div>
            <?php } ?>
        </div>
      </div>
    </div>
    <div class="row" id="lessons">
          <h3 class="col-xs-12 lesson-title"><?php echo ucfirst(JrTexto::_('Activity')); ?><!--span id="namelesson"></span--></h3>
          <div class="col-xs-12 lesson-wrapper">
            <div class="lesson-list" style="position: relative;">
              <?php $j=0;
              if(!empty($this->actividades))
                foreach ($this->actividades as $sesion){ $j++;
                  $img='';
                  if(!empty($sesion["imagen"])){
                    $img='<img class="caratula-libro" src="'.(str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$sesion["imagen"])).'">';
                  } 
                  ?>
                  <div class="item-tools rname" data-iname="namelesson" title="<?php echo $sesion["nombre"]; ?>" data-idnivel="<?php echo $sesion["idnivel"]; ?>">
                    <a href="javascript:void(0) " class="lesson-item hvr-float <?php echo $j==1?'active':''; ?>">
                      <?php echo $img; ?>
                      <div class="lesson-name"><?php echo $sesion["nombre"]; //str_pad($j,2,"0",STR_PAD_LEFT); ?></div>
                    </a>
                  </div>
              <?php }/*if(NegSesion::tiene_acceso('Actividad', 'add')){ ?>		 
              <div class="item-tools item-add rname" data-addhtml="#a1" data-toggle="popover" data-iname="namelesson" title="<?php echo ucfirst(JrTexto::_('Add')).' '.JrTexto::_('Activity'); ?>">
              <span>
              <a href="javascript:void(0)" class="lesson-add"  >
              <div class="lesson-name" style="font-size: 2em"><br><?php echo ucfirst(JrTexto::_('Add')); ?><br><i class="fa fa-plus-circle"></i></div>
              </a>
              </span>
              </div>
              <?php } */?>
              </div>
          </div>
        </div>
  </div>    	
</div>

<div class="hidden" id="a1">
  <div class="form-group">       
    <label class="control-label" for="txtNombre"><?php echo ucfirst(JrTexto::_('Activity'));?> <span class="required"> * :</span></label>
    <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>" placeholder="<?php echo JrTexto::_('Type the name of the activity here')?>"> <br>
    <div>
      <button class="btn btn-cancel cerrarmodal pull-left"></span><?php echo JrTexto::_('Cancel');?></span> <i class="fa fa-close"></i></button>
      <button class="btn btn-success savenewactividad pull-right"><span><?php echo JrTexto::_('Save and add exercise');?></span> <i class="fa fa-text-o"></i></button> 
      <button class="btn btn-primary savenewactividad pull-right"><span><?php echo JrTexto::_('Save');?></span> <i class="fa fa-save"></i></button>                  
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    var niveles=function(data){
      try{
        var res = xajax__('', 'niveles', 'GetxPadre', data);
        if(res){ return res; }
        return false;
      }catch(error){
        console.log(error);
        return false;
      }     	
    }
    var initseleccion=function(){
      $('a.active').each(function(){
        var dt=$(this).closest('.item-tools').data('iname');
        var name=$(this).closest('.item-tools').attr('title');
        $('#'+dt).text(' : '+name);
      });
    };

    $('#smartbook-listar').on("change",'select#level-item',function(ev){
      var idnivel=$(this).val();
      var data={tipo:'U','idpadre':idnivel}
      redir(_sysUrlBase_+'/smartbook/listar/'+idnivel);
    }).on("click",'.item-tools[data-iname="nameunit"]',function(ev){
      var aobj=$(this).closest('.unit-list').find('a');
      $(aobj).removeClass('active');
      $(this).find('a').addClass('active');
      var idnivel=$(this).data('idnivel');

      var nivel=$('select#level-item').val();
      redir(_sysUrlBase_+'/smartbook/listar/'+nivel+'/'+idnivel);
    }).on("click",'.item-tools[data-iname="namelesson"]',function(ev){
      var aobj=$(this).closest('.lesson-list').find('a');
      $(aobj).removeClass('active');
      $(this).find('a').addClass('active');
      var idnivel=$('select#level-item').val();
      var idunidad=$('.unit-list a.active').closest('.item-tools').data('idnivel');
      var idlesson=$(this).data('idnivel');
      if(!$(this).hasClass('item-add'))
        redir(_sysUrlBase_+'/smartbook/ver/'+idnivel+'/'+idunidad+'/'+idlesson);
    }).on("mouseover",".item-tools.rname",function(){
      var dt=$(this).data('iname');
      var name=$(this).attr('title');
      $('#'+dt).text(' : '+name);
      $(this).find('.toolbottom').removeClass('hide');
    }).on("mouseout",".item-tools.rname",function(){
      var dt=$(this).data('iname');
      $obj=$('a.active').closest('[data-iname='+dt+']');
      var name=$obj.attr('title')||' ';
      $('#'+dt).text(' : '+name);          
      $(this).find('.toolbottom').addClass('hide');             
    });

    initseleccion();  

    var optionslike={
   		//dots: true,
  	  	infinite: false,
  	  	//speed: 300,
  	   	//adaptiveHeight: true
     		navigation: false,
    		slidesToScroll: 1,
    		centerPadding: '60px',
    	  slidesToShow: 6,
    	  responsive:[
            { breakpoint: 1200, settings: {slidesToShow: 5} },
            { breakpoint: 992, settings: {slidesToShow: 4 } },
            { breakpoint: 880, settings: {slidesToShow: 3 } },
            { breakpoint: 720, settings: {slidesToShow: 2 } },
            { breakpoint: 320, settings: {slidesToShow: 1 /*,arrows: false, centerPadding: '40px',*/} }	  	
    	  ]
   	};
 	  var slikunidad=$('.unit-list').slick(optionslike);
 	  var slickactividad=$('.lesson-list').slick(optionslike);
    $('header').show('fast').addClass('static');
 });
</script>
