<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Ugel'); ?><small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdugel" id="pkidugel" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDescripcion">
              <?php echo JrTexto::_('Descripcion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDescripcion" name="txtDescripcion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["descripcion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtAbrev">
              <?php echo JrTexto::_('Abrev');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtAbrev" name="txtAbrev" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["abrev"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIddepartamento">
              <?php echo JrTexto::_('Iddepartamento');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtiddepartamento" name="txtIddepartamento" class="form-control">
              
              <?php
          
          $selidpadre=@$frm["iddepartamento"];

          foreach ($this->departamentos as $depa) { ?>
            <option value="<?php echo $depa["id_ubigeo"]; ?>" <?php echo $selidpadre===$depa["id_ubigeo"]?'selected="selected"':''?>> <?php echo $depa["ciudad"]?> </option>
          <?php } ?>

              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdprovincia">
              <?php echo JrTexto::_('Idprovincia');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtidprovincia" name="txtIdprovincia" class="form-control">

              <?php 
              $selidpadre=@$frm["idprovincia"];
              foreach ($this->provincias as $idpro) { ?>
                <option value="<?php echo $idpro["id_ubigeo"]; ?>" <?php echo $selidpadre===$idpro["id_ubigeo"]?'selected="selected"':''?>> <?php echo $idpro["ciudad"]?> </option>
              <?php } ?>

              </select>
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveUgel" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('ugel'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Ugel', 'saveUgel', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Ugel"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Ugel"))?>');
        <?php endif;?>       }
     }
  });



$('#txtiddepartamento').change(function(){
    var iddep=$(this).val();
    //alert(iddep);
    $('#txtidprovincia option').remove();
    var data={'idpadre':iddep}
    var res = xajax__('', 'ugel', 'getxPadre',data);
    if(res){      //$('#txtUnidad').append('<option value=-1>'+x["nombre"]+'</option>');
      $.each(res,function(){
        x=this;
        $('#txtidprovincia').append('<option value='+x["id_ubigeo"]+'>'+x["ciudad"]+'</option>');
      });
       $( "#filtros" ).submit();
    }
  });





  
  
});


</script>

