<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?><style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
  .form-group{
    margin-bottom: 0px; 
  }
  .input-group {
    margin-top: 0px;
  }
  .select-ctrl-wrapper:after{
      right: 0px;
    }
  select.select-ctrl, .form-control , .input-group-addon{   
    border: 1px solid #4683af;
    margin-bottom: 1ex;
  }
  .breadcrumb a{ color: #fff; }
  .breadcrumb li.active{ color:#d2cccc; }
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("academico"));?>"><?php echo JrTexto::_("Academic");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('alumno'));?>"><?php echo JrTexto::_('Alumno'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row"> 
            <div class="col-xs-6 col-sm-4 col-md-3">
            <label><?php echo JrTexto::_('Sexo'); ?></label>
            <div class="select-ctrl-wrapper select-azul">
              <select id="fkcbsexo" name="fkcbsexo" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                  <?php 
                  if(!empty($this->fksexo))
                  foreach ($this->fksexo as $fksexo) { ?><option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> ><?php echo $fksexo["nombre"] ?></option>
                   <?php } ?>                        
              </select>
            </div>
            </div>
                          
            <div class="col-xs-6 col-sm-4 col-md-3">
            <label><?php echo JrTexto::_('Estado Civil'); ?></label>
            <div class="select-ctrl-wrapper select-azul">
              <select id="fkcbestado_civil" name="fkcbestado_civil" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                  <?php 
                          if(!empty($this->fkestado_civil))
                            foreach ($this->fkestado_civil as $fkestado_civil) { ?><option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> ><?php echo $fkestado_civil["nombre"] ?></option><?php } ?>                        
              </select>
            </div>
            </div>                                                                                                                            
            <div class="col-xs-6 col-sm-4 col-md-3 ">
              <label><?php echo JrTexto::_('Estado'); ?></label>
              <div class="select-ctrl-wrapper select-azul">
                <select name="cbestado" id="cbestado" class="form-control select-ctrl">
                    <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                    <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                    <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                </select>
              </div>
            </div>                                                         
            
                                
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 text-center">
              <a class="btn btn-warning btnvermodal" data-modal="no" href="<?php echo JrAplicacion::getJrUrl(array("alumno"));?>?verlis=2" data-titulo="<?php echo JrTexto::_("Alumno").' - '.JrTexto::_("list"); ?>"><i class="fa fa-drivers-license-o"></i> </a>
              <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("alumno", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Alumno").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              <a class="btn btn-primary btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("alumno", "importar"));?>" data-titulo="<?php echo JrTexto::_("Alumno").' - '.JrTexto::_("Import"); ?>"><i class="fa fa-cloud-upload"></i> <?php echo JrTexto::_('import')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Nombre") ;?></th> 
                    <th><?php echo JrTexto::_("Sexo"); ?></th>                                       
                    <th><?php echo JrTexto::_("Telefonos") ;?></th>
                    <th><?php echo JrTexto::_("Email") ;?></th> 
                    <th><?php echo JrTexto::_("Usuario") ;?></th>                    
                    <th><?php echo JrTexto::_("Foto") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>                    
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos597627b63b43a='';
function refreshdatos597627b63b43a(){
    tabledatos597627b63b43a.ajax.reload();
}
$(document).ready(function(){  
  var estados597627b63b43a={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var sexos597627b63b43a=<?php echo json_encode($this->fksexo); ?>;
  var sexo597627b63b43a={};
  $.each(sexos597627b63b43a,function(i,v){
    sexo597627b63b43a["'"+v.codigo+"'"]=v.nombre;
  })
  var estadocivil597627b63b43a=<?php echo json_encode($this->fkestado_civil); ?>;
  var estadociv597627b63b43a={};
  $.each(estadocivil597627b63b43a,function(i,v){
    estadociv597627b63b43a["'"+v.codigo+"'"]=v.nombre;
  })
  var tituloedit597627b63b43a='<?php echo ucfirst(JrTexto::_("student"))." - ".JrTexto::_("edit"); ?>';
  var tituloficha597627b63b43a='<?php echo ucfirst(JrTexto::_("Ficha"));?> - ';
  var titulocambiaruser597627b63b43a='<?php echo ucfirst(JrTexto::_("Change"))." - ".JrTexto::_("User"); ?> ';
  var draw597627b63b43a=0;

  
  $('#datefechanac').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#fkcbsexo').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#fkcbestado_civil').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#fkcbubigeo').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#fkcbidugel').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#dateregfecha').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#cbestado').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#cbsituacion').change(function(ev){
    refreshdatos597627b63b43a();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos597627b63b43a();
  });
  $('#texto').keyup(function(e) {
    if(e.keyCode == 13) {
        refreshdatos597627b63b43a();
    }
  });
  tabledatos597627b63b43a=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
        {'data': '<?php echo JrTexto::_("Sexo") ;?>'},
       // {'data': '<?php //echo JrTexto::_("Estado civil") ;?>'},                  
        {'data': '<?php echo JrTexto::_("Telefono / celular") ;?>'},
        {'data': '<?php echo JrTexto::_("Email") ;?>'}, 
        {'data': '<?php echo JrTexto::_("Usuario") ;?>'},           
        {'data': '<?php echo JrTexto::_("Foto") ;?>'},
        {'data': '<?php echo JrTexto::_("Estado") ;?>'},                
        {'data': '<?php echo JrTexto::_("Actions") ;?>'}
      ],
      "ajax":{
        url:_sysUrlBase_+'/alumno/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.fechanac=$('#datefechanac').val(),
             d.sexo=$('#fkcbsexo').val(),
             d.estado_civil=$('#fkcbestado_civil').val(),
             d.texto=$('#texto').val(),            
             //d.situacion=$('#cbsituacion').val(),
             //d.texto=$('#texto').val(),
                        
            draw597627b63b43a=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw597627b63b43a;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            var fullname=data[i].ape_paterno+' '+data[i].ape_materno+', '+data[i].nombre;
            var actions='<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/alumno/verficha/?id='+data[i].dni+'" data-titulo="'+tituloficha597627b63b43a+fullname+'"><i class="fa fa-user"></i><i class="fa fa-eye"></i></a>'+
                        '<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/alumno/editar/?id='+data[i].dni+'" data-titulo="'+tituloedit597627b63b43a+'"><i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>'+
                        '<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/alumno/cambiarclave/?id='+data[i].dni+'" data-titulo="'+titulocambiaruser597627b63b43a+'"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>'+
                        '<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/alumno/notas/?id='+data[i].dni+'" data-titulo="'+tituloedit597627b63b43a+'"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a>'+
                        '<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/alumno/horario/?id='+data[i].dni+'" data-titulo="'+tituloedit597627b63b43a+'"><i class="fa fa-calendar"></i></a>'+
                        '<a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].dni+'" ><i class="fa fa-trash-o"></i></a>'; 
              var img='<img class="img-circle img-responsive" src="'+_sysUrlStatic_ +'/media/usuarios/'+(data[i].foto||'user_avatar.jpg')+'" style="max-height:40px; max-width:40px;">';
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Nombre") ;?>': fullname,             
              '<?php echo JrTexto::_("Sexo") ;?>': data[i].sexo!=''?sexo597627b63b43a["'"+data[i].sexo+"'"]:'',
             // '<?php //echo JrTexto::_("Estado civil") ;?>': data[i].estado_civil!=''?estadociv597627b63b43a["'"+data[i].estado_civil+"'"]:'',
              '<?php echo JrTexto::_("Telefono / celular") ;?>': data[i].telefono+'<br>'+data[i].celular,
              '<?php echo JrTexto::_("Email") ;?>': data[i].email,            
              '<?php echo JrTexto::_("Usuario") ;?>': data[i].usuario,             
              '<?php echo JrTexto::_("Foto") ;?>': img,
              '<?php echo JrTexto::_("Estado") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="'+data[i].dni+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados597627b63b43a[data[i].estado]+'</a>',              
              '<?php echo JrTexto::_("Actions") ;?>'  :actions
            });
          }
          return datainfo 
        }, error: function(d){console.log(d)}
        }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'alumno', 'setCampo', id,campo,data);
          if(res) tabledatos597627b63b43a.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos597627b63b43a';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Alumno';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'alumno', 'eliminar', id);
        if(res) tabledatos597627b63b43a.ajax.reload();
      }
    }); 
  });
});
</script>