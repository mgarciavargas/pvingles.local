<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="page-title">
    
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("administrador"));?>"><?php echo JrTexto::_("Dashboard");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('bib_libro'));?>"><?php echo JrTexto::_('Bib_libro'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('Add')?></li>
      </ol>
    
  </div>
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Bib_libro'); ?><small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdlibro" id="pkidlibro" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCodigo">
              <?php echo JrTexto::_('Codigo');?> <span > :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <input type="number" id="txtCodigo" name="txtCodigo" step="1" value=""  min="1" class="form-control col-md-7 col-xs-12" />
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('Nombre');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtPrecio">
              <?php echo JrTexto::_('Precio');?> <span > :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <input type="number"  id="txtprecio" name="txtPrecio" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["txtPrecio"] ?>">    
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEditorial">
              <?php echo JrTexto::_('Editorial');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtEditorial" name="txtEditorial" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["editorial"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEdicion">
              <?php echo JrTexto::_('Edicion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtEdicion" name="txtEdicion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["edicion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFec_publicacion">
            <?php echo JrTexto::_('Fec publicacion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <input type="date"  id="txtFec_publicacion" name="txtFec_publicacion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["Fec_publicacion"];?>">                   
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFoto">
              <?php echo JrTexto::_('Foto');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               
              <div class="row">
                  <input type="hidden" name="txtFoto" id="txtFoto">
                  <input type="hidden" name="txtFoto_old" value="<?php echo @$frm["foto"];?>">
                  <div class="col-md-12">
                    <div class="img-container" style="position: relative;">
                      <input type="file" class="cargarfile" data-tipo="image" data-target="hImgPortada" accept="image/*" style="position: absolute; opacity: 0; height: 150px;" >
                      <img src="<?php echo !empty($frm["foto"])?$this->documento->getUrlBase().$frm["foto"]:($this->documento->getUrlStatic()."/media/web/nofoto.jpg");?>" class="img-responsive"  alt="imgfoto"  id="imgPortada" style="height: 150px; display: inline;">
                      <input type="hidden" name="hImgPortada" id="hImgPortada" value="<?php echo @$frm["foto"];?>">
                    </div>
                  </div>
                </div>
                

                                 
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtArchivo">
              <?php echo JrTexto::_('Archivo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               
                 <span class="btn btn-default btn-file"> 
                  <i class="fa fa-upload"></i> <?php echo JrTexto::_('Seleccionar Documento');?> 
                  <input data-texto="Documento" data-campo="archivo" data-action="<?php echo JrAplicacion::getJrUrl(array('bib_libro', 'subir_archivo'));?>?arc=archivo&tipo=doc" type="file" name="fileArchivo" id="fileArchivo" class="cargarfile" data-tipo="pdf" data-target="hNombreArchivo" accept="application/pdf">
                  <input type="hidden" name="hNombreArchivo" id="hNombreArchivo" value="<?php echo @$frm["archivo"];?>">
                </span>
              </div>
              <div class="col-md-8 col-sm-8 col-xs-12 col-xs-offset-4">
                <div id="divprecargafile" style="display:none; position: relative; background: #fff;">
                    <div style="bottom:0px; width:100%">
                      <div class="progress" style="margin-bottom: 0px;">  
                        <div class="progress-bar progress-bar-striped progress-bar-animated focus active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                          <?php echo JrTexto::_('loading').'...'; ?><span>70%</span>
                        </div>
                      </div>
                  </div>
                </div> 
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdtipo">
              <?php echo JrTexto::_('Idtipo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtIdtipo" name="txtIdtipo" class="form-control">

              <?php 
              if(!empty($this->tipo))
                foreach ($this->tipo as $t) { ?><option value="<?php echo $t["idtipo"]?>"><?php echo $t["nombre"] ?></option><?php } ?>
              </select></div>
                                  
              </div>
              <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdautor">
              <?php echo JrTexto::_('Idautor');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtIdautor" name="txtIdautor" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->autores))
                foreach ($this->autores as $a) { ?><option value="<?php echo $a["idautor"]?>"><?php echo $a["ap_paterno"].' '.$a["ap_materno"].' '.$a["nombre"] ?></option><?php } ?>
              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCondicion">
              <?php echo JrTexto::_('Condicion');?> <span > :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <select id="txtCondicion" name="txtCondicion" class="form-control">
                <option value="gratis"><?php echo JrTexto::_('Gratuito'); ?></option>
                <option value="venta"><?php echo JrTexto::_('Venta'); ?></option>
                <option value="alquiler"><?php echo JrTexto::_('Alquiler'); ?></option>
                </select>

                <!--<input type="text"  id="txtCondicion" name="txtCondicion"  class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["condicion"];?>">-->
                                  
              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdioma">
              <?php echo JrTexto::_('Idioma');?> <span > :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <select id="txtIdioma" name="txtIdioma" class="form-control">
                <option value="español"><?php echo JrTexto::_('Español'); ?></option>
                <option value="ingles"><?php echo JrTexto::_('Inglés'); ?></option>
                </select>

                <!--<input type="text"  id="txtCondicion" name="txtCondicion"  class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["condicion"];?>">-->
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtResumen">
              <?php echo JrTexto::_('Resumen');?> <span >  :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <textarea name="txtResumen"  id="txtResumen" name="txtResumen" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["resumen"];?>"></textarea>
                                  
              </div>
            </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveBib_libro" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('bib_libro'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div></form>

<script type="text/javascript">
$(document).ready(function(){  
  subirmedia=function(tipo,file, $target){
    var formData = new FormData();
    formData.append("tipo",tipo);
    formData.append("filearchivo",file[0].files[0]);
    $.ajax({
      url: '<?php echo $this->documento->getUrlBase(); ?>/bib_libro/subirarchivo',
      type: "POST",
      data:  formData,
      contentType: false,
      processData: false,
      dataType :'json',
      cache: false,
      processData:false,
      xhr:function(){
        var xhr = new window.XMLHttpRequest();
        //Upload progress
        xhr.upload.addEventListener("progress", function(evt){

          if (evt.lengthComputable) {
            var percentComplete = Math.floor((evt.loaded*100) / evt.total);            
            $('#divprecargafile .progress-bar').width(percentComplete+'%');
            $('#divprecargafile .progress-bar span').text(percentComplete+'%');
          }
        }, false);
        //Download progress
        xhr.addEventListener("progress", function(evt){
          if (evt.lengthComputable) {
            var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
            //Do something with download progress
            //console.log(percentComplete);
          }
        }, false);
        return xhr;
      },
      beforeSend: function(XMLHttpRequest){
        div=$('#divprecargafile');
        $('#divprecargafile').removeClass('progress-bar-danger progress-bar-success progress-bar-striped progress-bar-animated').addClass('progress-bar-striped progress-bar-animated');        
        $('#divprecargafile .progress-bar').width('0%');
        $('#divprecargafile .progress-bar span').text('0%'); 
        $('#divprecargafile').show('fast'); 
        $('#btn-saveBib_libro').attr('disabled','disabled');
      },      
      success: function(data)
      {        
        if(data.code==='ok'){
          $('#divprecargafile .progress-bar').width('100%');
          $('#divprecargafile .progress-bar').html('Complete <span>100%</span>');
          $('#divprecargafile').addClass('progress-bar-success').removeClass('progress-bar-animated');
          if(tipo=='image'){
            $target.siblings('#imgPortada').attr('src', _sysUrlStatic_+'/libreria/'+tipo+'/'+data.namelink);
          }else if(tipo=='pdf'){
            $target.closest('.btn-default').removeClass('btn-default').addClass('btn-info');
          }
          $target.attr('value', data.namelink);
          mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
          $('#btn-saveBib_libro').removeAttr('disabled');
        }else{
            $('#divprecargafile').addClass('progress-bar-warning progress-bar-animated');
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
            return false;
        }
      },
      error: function(e) 
      {
          $('#divprecargafile').addClass('progress-bar-danger progress-bar-animated');
           $('#divprecargafile .progress-bar').html('Error <span>-1%</span>'); 
          mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',e,'warning');
          return false;
      },
      complete: function(xhr){
         $('#divprecargafile .progress-bar').html('Complete <span>100%</span>'); 
         $('#divprecargafile').addClass('progress-bar-success progress-bar-animated').hide('slow');
      }
    });
  };
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Bib_libro', 'saveBib_libro', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
         /* return redir('<?php echo JrAplicacion::getJrUrl(array("Bib_libro"))?>');*/
        }
        <?php else:?>
       /* return redir('<?php echo JrAplicacion::getJrUrl(array("Bib_libro"))?>');*/
        <?php endif;?>       
      }
     }
  });

  $('.cargarfile').on('change',function(e){           
     var file=$(this);
     var tipo = $(this).attr('data-tipo');
     var target = '#'+$(this).attr('data-target');
     var $target = $(target);
    if(file.val()=='') return false;
    subirmedia(tipo,file, $target);
    e.preventDefault();
    e.stopPropagation();
  });
    
});


function result_archivo(result, mensaje,txt) {
  if(result) {
    $("#txt"+txt).val(mensaje);
    agregar_msj_interno2("success", '<?php echo JrTexto::_("File updated successfully");?>',"msj-interno2",true);
    var tipo=$("#id"+txt).attr("data-type");
    if(tipo=="imagen")
      $("#id"+txt).removeAttr("style").attr("src", "http://localhost/pvingles" + mensaje );
    else
      $("#id"+txt).removeAttr("style").attr("href",  "http://localhost/pvingles" + mensaje );
    }else{
      agregar_msj_interno2("warning", mensaje,"msj-interno2",true);
    }
}
  
</script>

