<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Acad_marcacion"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
                                                                                                      <div class="col-xs-6 col-sm-4 col-md-3">
                      <div class="form-group">    
                          <div class='input-group date datetimepicker1 border1' >
                            <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo  ucfirst(JrTexto::_("fecha"))?> </span>
                            <input type='text' class="form-control border0" name="datefecha" id="datefecha" />           
                          </div>
                      </div>
                    </div>
                                                  <div class="col-xs-6 col-sm-4 col-md-3">
                      <div class="form-group">    
                          <div class='input-group date datetimepicker1 border1' >
                            <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo  ucfirst(JrTexto::_("horaingreso"))?> </span>
                            <input type='text' class="form-control border0" name="datehoraingreso" id="datehoraingreso" />           
                          </div>
                      </div>
                    </div>
                                                  <div class="col-xs-6 col-sm-4 col-md-3">
                      <div class="form-group">    
                          <div class='input-group date datetimepicker1 border1' >
                            <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo  ucfirst(JrTexto::_("horasalida"))?> </span>
                            <input type='text' class="form-control border0" name="datehorasalida" id="datehorasalida" />           
                          </div>
                      </div>
                    </div>
                                                                        
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Acad_marcacion", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Acad_marcacion").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idgrupodetalle") ;?></th>
                    <th><?php echo JrTexto::_("Idcurso") ;?></th>
                    <th><?php echo JrTexto::_("Idhorario") ;?></th>
                    <th><?php echo JrTexto::_("Fecha") ;?></th>
                    <th><?php echo JrTexto::_("Horaingreso") ;?></th>
                    <th><?php echo JrTexto::_("Horasalida") ;?></th>
                    <th><?php echo JrTexto::_("Iddocente") ;?></th>
                    <th><?php echo JrTexto::_("Nalumnos") ;?></th>
                    <th><?php echo JrTexto::_("Observacion") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5a62130c66d82='';
function refreshdatos5a62130c66d82(){
    tabledatos5a62130c66d82.ajax.reload();
}
$(document).ready(function(){  
  var estados5a62130c66d82={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5a62130c66d82='<?php echo ucfirst(JrTexto::_("acad_marcacion"))." - ".JrTexto::_("edit"); ?>';
  var draw5a62130c66d82=0;

  
  $('#datefecha').change(function(ev){
    refreshdatos5a62130c66d82();
  });
  $('#datehoraingreso').change(function(ev){
    refreshdatos5a62130c66d82();
  });
  $('#datehorasalida').change(function(ev){
    refreshdatos5a62130c66d82();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos5a62130c66d82();
  });
  tabledatos5a62130c66d82=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Idgrupodetalle") ;?>'},
            {'data': '<?php echo JrTexto::_("Idcurso") ;?>'},
            {'data': '<?php echo JrTexto::_("Idhorario") ;?>'},
            {'data': '<?php echo JrTexto::_("Fecha") ;?>'},
            {'data': '<?php echo JrTexto::_("Horaingreso") ;?>'},
            {'data': '<?php echo JrTexto::_("Horasalida") ;?>'},
            {'data': '<?php echo JrTexto::_("Iddocente") ;?>'},
            {'data': '<?php echo JrTexto::_("Nalumnos") ;?>'},
            {'data': '<?php echo JrTexto::_("Observacion") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/acad_marcacion/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.fecha=$('#datefecha').val(),
             d.horaingreso=$('#datehoraingreso').val(),
             d.horasalida=$('#datehorasalida').val(),
             //d.texto=$('#texto').val(),
                        
            draw5a62130c66d82=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5a62130c66d82;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Idgrupodetalle") ;?>': data[i].idgrupodetalle,
                '<?php echo JrTexto::_("Idcurso") ;?>': data[i].idcurso,
                '<?php echo JrTexto::_("Idhorario") ;?>': data[i].idhorario,
                '<?php echo JrTexto::_("Fecha") ;?>': data[i].fecha,
              '<?php echo JrTexto::_("Horaingreso") ;?>': data[i].horaingreso,
              '<?php echo JrTexto::_("Horasalida") ;?>': data[i].horasalida,
              '<?php echo JrTexto::_("Iddocente") ;?>': data[i].iddocente,
                '<?php echo JrTexto::_("Nalumnos") ;?>': data[i].nalumnos,
              '<?php echo JrTexto::_("Observacion") ;?>': data[i].observacion,
                
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/acad_marcacion/editar/?id='+data[i].idmarcacion+'" data-titulo="'+tituloedit5a62130c66d82+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idmarcacion+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'acad_marcacion', 'setCampo', id,campo,data);
          if(res) tabledatos5a62130c66d82.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5a62130c66d82';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Acad_marcacion';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'acad_marcacion', 'eliminar', id);
        if(res) tabledatos5a62130c66d82.ajax.reload();
      }
    }); 
  });
});
</script>