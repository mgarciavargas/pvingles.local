<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
class WebDocente extends JrWeb
{	
	
    protected $oNegAlumno;
    protected $oNegMetodologia;
    //protected $oNegGrupos;
    protected $oNegGrupoAulaDet;
    protected $oNegLocal;
    protected $oNegNiveles;
    protected $oNegPersonal;
    private $oNegMatricula;
    
	public function __construct()
	{
		parent::__construct();
        $this->oNegAlumno = new NegAlumno;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        //$this->oNegGrupos = new NegGrupos;
        $this->oNegGrupoAulaDet = new NegAcad_grupoauladetalle;
        $this->oNegLocal = new NegLocal;
        $this->oNegNiveles = new NegNiveles;
        $this->oNegPersonal = new NegPersonal;
        $this->oNegMatricula = new NegAcad_matricula;

        $this->usuarioAct = NegSesion::getUsuario();
	}
	
	public function defecto()
	{
		try {
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('docente'), true);
			$this->esquema = 'alumno/tester';
			$this->documento->plantilla = 'general';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function panelcontrol()
	{
		try {
			global $aplicacion;
            #Librerias
             $this->documento->script('jquery-confirm.min', '/libs/alert/');
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('slick.min', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
             $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
             $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
             $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
             $this->documento->script('loader', '/libs/googlecharts/');
             $this->documento->script('chartist.min', '/libs/chartist/');
             $this->documento->stylesheet('chartist.min', '/libs/chartist/');
             $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
             $this->documento->script('circle', '/libs/graficos/progressbar/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/');
             $this->documento->script('html2canvas.min', '/libs/html2canvas/');
             $this->documento->script('rgbcolor', '/libs/svg2canvas/');
             $this->documento->script('StackBlur', '/libs/svg2canvas/');
             $this->documento->script('canvg.min', '/libs/svg2canvas/');
             $this->documento->script('SVG2Bitmap', '/libs/chartist2image/');
            $filtros = [];
            if($this->usuarioAct["rol"]=="Alumno") {
                $this->vistaAlumno();
            } else{
                /*if(!NegSesion::tiene_acceso('docente', 'add')) {
                    throw new Exception(JrTexto::_('Restricted access').'!!');
                }*/
                $this->breadcrumb = [
                    [ 'texto'=> ucfirst(JrTexto::_('Student Progress Tracking'))],
                ];
                $filtros['iddocente'] = $this->usuarioAct["dni"];
                //$this->grupos=$this->oNegGrupos->buscar($filtros);
                $this->grupos=$this->oNegGrupoAulaDet->buscar($filtros);
                $this->locales=[];
                $arrLocales = [];
                foreach ($this->grupos as $g) {
                    if(!in_array($g['idlocal'], $arrLocales)){
                        $this->oNegLocal->idlocal = $g['idlocal'];
                        $this->locales[]=$this->oNegLocal->getXid();
                        $arrLocales[]=$g['idlocal'];
                    }
                }
                /*$this->alumnos=$this->oNegAlumno->buscar();*/
                $this->documento->plantilla = 'seguimientoalumno/inicio';
                $this->documento->setTitulo(JrTexto::_('Smartracking'), true);
                $this->esquema = 'docente/panelcontrol';
            }
            $this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

    public function configuracion()
    {
        try {
            global $aplicacion;
            if(!NegSesion::tiene_acceso('Configuracion_docente', 'add')) {
                throw new Exception(JrTexto::_('Restricted access').'!!');
            }
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');

            $this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));

            $this->documento->plantilla = 'seguimientoalumno/inicio';
            $this->documento->setTitulo(JrTexto::_('Setting'), true);
            $this->esquema = 'docente/configuracion';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function curriculum()
    {
        try {
            global $aplicacion;
            if(empty($_GET['id'])){ throw new Exception(JrTexto::_("Teacher not found")); }
            $this->idDocente = (isset($_GET['id']))?$_GET['id']:0;
            $this->idCurso = (isset($_GET['idcurso']))?$_GET['idcurso']:0;
            $this->documento->setTitulo(JrTexto::_('Curriculum'), true);

            $this->oNegPersonal->dni = $this->idDocente;
            $this->docente = $this->oNegPersonal->getXid();

            #var_dump($this->docente);

            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('teacher')), ],
                //[ 'texto'=> $this->docente['ape_paterno'].' '.$this->docente['ape_materno'].' '.$this->docente['nombre'] ],
            ];
            $this->documento->plantilla = 'mantenimientos';

            if(!empty($this->idCurso)){
                $this->breadcrumb = [
                   // [ 'texto'=> ucfirst(JrTexto::_('teacher')), 'link'=> '/docente/?id='.$this->idDocente ],
                    [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                    [ 'texto'=> ucfirst(JrTexto::_('information')), 'link'=> '/curso/informacion/?id='.$this->idCurso ],
                    [ 'texto'=> ucfirst(JrTexto::_('teacher profile')) ],
                    //[ 'texto'=> $this->docente['ape_paterno'].' '.$this->docente['ape_materno'].' '.$this->docente['nombre'] ],
                ];
                $this->documento->plantilla = 'alumno/curso';
            }
            $this->esquema = 'docente/curriculum';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }


    // ========================== Funciones Privadas ========================== //
    private function tieneAccesoAlumno()
    {
        $rol = $this->usuarioAct["rol"];
        $this->idCurso = (isset($_GET['idcurso']))?$_GET['idcurso']:null;
        if($rol=="Alumno" && empty($this->idCurso)){ 
            return false;
            //throw new Exception(JrTexto::_("Course not found"));
        }
        $this->cursoActual = $this->oNegMatricula->estaMatriculado( $this->idCurso);
        if(empty($this->cursoActual)){
            return false;
        }
        return true;
    }

    private function elegirCurso()
    {
        try {
            global $aplicacion;
            $this->cursosMatric = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$this->usuarioAct['dni'], 'estado'=>1));
            $this->documento->setTitulo(JrTexto::_('Smartracking'), true);
            $this->esquema = 'alumno/cursos_matriculados';
            $this->documento->plantilla = 'alumno/curso';
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    private function vistaAlumno()
    {
        try {
            global $aplicacion;
            if(!$this->tieneAccesoAlumno()) {
                $this->elegirCurso();
            } else {
                $this->breadcrumb = [
                    [ 'texto'=> ucfirst(JrTexto::_('course')) , 'link'=> '/curso/?id='.$this->idCurso ],
                    [ 'texto'=> ucfirst(JrTexto::_('tracking')) /*, 'link'=> '/curso/?id='.$this->idCurso*/ ],
                    [ 'texto'=> $this->cursoActual['nombre'] ],
                ];
                #var_dump($this->cursoActual);
                $this->documento->plantilla = 'alumno/curso';
                $this->esquema = 'docente/panelcontrol';
            }
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

}