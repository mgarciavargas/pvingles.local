<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		05-01-2018 
 * @copyright	Copyright (C) 05-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_horariogrupodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAmbiente', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
class WebAcad_grupoaula extends JrWeb
{
	private $oNegAcad_grupoaula;
	private $oNegAcad_grupoauladetalle;
	private $oNegGeneral;
	private $oNegLocal;
	private $oNegCurso;
	private $oNegAmbiente;
	private $oNegHorariogrupoaula;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegGeneral = new NegGeneral;
		$this->oNegLocal = new NegLocal;
		$this->oNegAmbiente = new NegAmbiente;
		$this->oNegCurso = new NegAcad_curso;
		$this->oNegHorariogrupoaula = new NegAcad_horariogrupodetalle;		
	}

	public function defecto(){
		return $this->listado();
	}

	

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Acad_grupoaula', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');

            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->fktipos=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipogrupo','mostrar'=>1));
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$this->fktipo=$_REQUEST["tipo"];

			$this->fkestadogrupos=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadogrupo','mostrar'=>1));
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$this->fkidestadogrupo=$_REQUEST["estado"];

			$this->fklocales=$this->oNegLocal->buscar();
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$this->fkidlocal=$_REQUEST["idlocal"];

			if(!empty($this->fkidlocal)){
				$this->fkambientes=$this->oNegAmbiente->buscar(array('idlocal'=>$this->fkidlocal));
				if(isset($_REQUEST["idambiente"])&&@$_REQUEST["idambiente"]!='')$this->fkidambiente=$_REQUEST["idambiente"];
			}			

			$this->fkcursos=$this->oNegCurso->buscar();
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$this->fkidcurso=$_REQUEST["idcurso"];

			//$this->fklocal=$this->oNegLocal->buscar();
			//if(isset($_REQUEST["local"])&&@$_REQUEST["local"]!='')$this->fklocal_=$_REQUEST["local"];

			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Grupos'), true);
			$this->vista = !empty($_REQUEST['verlis']) ? $_REQUEST['verlis'] : '';
			$this->esquema = 'academico/grupos';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function listadovista(){
		try {
			global $aplicacion;
			$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:1;
			$this->esquema = 'academico/grupos-list-vista0'.$vista;
			$this->documento->setTitulo(JrTexto::_('Groups').' /'.JrTexto::_('List'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'blanco';

			$filtros=array();
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["local"])&&@$_REQUEST["local"]!='')$filtros["idlocal"]=$_REQUEST["local"];
			if(isset($_REQUEST["ambiente"])&&@$_REQUEST["ambiente"]!='')$filtros["idambiente"]=$_REQUEST["ambiente"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["curso"])&&@$_REQUEST["curso"]!='')$filtros["idcurso"]=$_REQUEST["curso"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];

			$this->grupos=$this->oNegAcad_grupoaula->buscargrupos($filtros);
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function horario(){
		try {
			global $aplicacion;
			$this->documento->script('moment.min', '/libs/fullcalendar/');
            $this->documento->script('datetimepicker', '/libs/datepicker/');
            $this->documento->script('fullcalendar', '/libs/fullcalendar/');
            $this->documento->script('locale-all', '/libs/fullcalendar/');
            $this->documento->script('calendar', '/libs/fullcalendar/');
            $this->documento->script('events', '/libs/fullcalendar/');
            $this->documento->stylesheet('fullcalendar', '/libs/fullcalendar/');
            $this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
			$this->esquema = 'usuario/horarios';

			if(!empty($_REQUEST['idgrupoauladetalle'])){
				$idgrupoauladetalle=@$_REQUEST['idgrupoauladetalle'];
				$gruposdetalle=$this->oNegAcad_grupoauladetalle->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle));
				$this->horarios=array();
				if(!empty($gruposdetalle))
					foreach ($gruposdetalle as $rw){
						$idgrupoauladetalle=$rw["idgrupoauladetalle"];
		    			$tmphorarios=$this->oNegHorariogrupoaula->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle));
		    			if(!empty($tmphorarios))
		    			foreach ($tmphorarios as $hr){
		    				$this->horarios[$hr["idhorario"]]='{id:'.$hr["idhorario"].
		    									',title:\''.$rw["strcurso"].
		    									'\',start:\''.$hr["fecha_finicio"].
		    									'\',end:\''.$hr["fecha_final"].
		    									'\',backgroundColor:\''.$hr["color"].
		    									'\',color:\''.$hr["color"].'\'},';
		    			}
	    		}
			}			
			$this->documento->setTitulo(JrTexto::_('Schedule'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function formulario(){
		try {
			$this->documento->script('moment.min', '/libs/fullcalendar/');
            $this->documento->script('datetimepicker', '/libs/datepicker/');
            $this->documento->script('fullcalendar', '/libs/fullcalendar/');
            $this->documento->script('locale-all', '/libs/fullcalendar/');
            //$this->documento->script('calendar', '/libs/fullcalendar/');
            //$this->documento->script('events', '/libs/fullcalendar/');
            $this->documento->stylesheet('fullcalendar', '/libs/fullcalendar/');
            $this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            //$this->documento->script('jscolor.min', '/libs/jscolor/');
			global $aplicacion;
			$this->esquema = 'academico/grupos-formulario';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			$tipo=!empty($_REQUEST["tipo"])?$_REQUEST["tipo"]:'';
			$estado=!empty($_REQUEST["estado"])?$_REQUEST["estado"]:'';

			if(!empty($_REQUEST["idgrupoaula"])){
				$idgrupoaula=$_REQUEST["idgrupoaula"];
				$datosgrupo=$this->oNegAcad_grupoaula->buscar(array('idgrupoaula'=>$idgrupoaula));
				if(!empty($datosgrupo[0])){
					$this->datos=$datosgrupo[0];
					$tipo=$this->datos["tipo"];
					$estado=$this->datos["estado"];
					$this->accion='edit';
				}
			}

			$this->acciongrupodetalle='add';
			if(!empty($_REQUEST["idgrupoauladetalle"])){				
				$idgrupoauladetalle=$_REQUEST["idgrupoauladetalle"];
				$datosgrupodetalle=$this->oNegAcad_grupoauladetalle->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle));
				//var_dump($datosgrupodetalle);
				if(!empty($datosgrupodetalle[0])){
					$this->datosgrupodetalle=$datosgrupodetalle[0];
					$this->acciongrupodetalle='edit';
				}
			}

			$this->fktipos=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipogrupo','mostrar'=>1));
			$this->fktipo=$tipo;

			$this->fkestadogrupos=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadogrupo','mostrar'=>1));
			$this->fkidestadogrupo=$estado;

			$this->fklocales=$this->oNegLocal->buscar();
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$this->fkidlocal=$_REQUEST["idlocal"];

			if(!empty($this->fkidlocal)){
				$this->fkambientes=$this->oNegAmbiente->buscar(array('idlocal'=>$this->fkidlocal));
				if(isset($_REQUEST["idambiente"])&&@$_REQUEST["idambiente"]!='')$this->fkidambiente=$_REQUEST["idambiente"];
			}

			$this->fkcursos=$this->oNegCurso->buscar();
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$this->fkidcurso=$_REQUEST["idcurso"];

			$this->documento->setTitulo(JrTexto::_('Groups').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function horarios(){
		try {
			global $aplicacion;
			$this->esquema = 'academico/grupos-horario';
			$this->documento->script('moment.min', '/libs/fullcalendar/');
            $this->documento->script('datetimepicker', '/libs/datepicker/');
            $this->documento->script('fullcalendar', '/libs/fullcalendar/');
            $this->documento->script('locale-all', '/libs/fullcalendar/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');


           	$filtros=array();
           	if(!empty($_REQUEST["idgrupoaula"])){
           		$this->idgrupoaula=$_REQUEST["idgrupoaula"];
           		$filtros["idgrupoaula"]=$this->idgrupoaula;
           	}
            $this->idgrupoauladetalle=!empty($_REQUEST["idgrupoauladetalle"])?$_REQUEST["idgrupoauladetalle"]:'';
            $this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';

            if(!empty($_REQUEST["iddocente"])){
           		$this->fkdocente=$_REQUEST["iddocente"];
           		$filtros["iddocente"]=$this->fkdocente;
           	}

           	if(!empty($_REQUEST["tipo"])){
           		$this->fktipo=$_REQUEST["tipo"];
           		$filtros["tipo"]=$this->fktipo;
           		if($this->fktipo!='V'){
           			if(!empty($_REQUEST["idlocal"])) $filtros["idlocal"]=$this->fklocal=$_REQUEST["idlocal"];
		           	else $filtros["idlocal"]=$this->fklocal=0;
           		}
           	}
           	if(!empty($_REQUEST["idcurso"])){
           		$this->fkcurso=$_REQUEST["idcurso"];
           		$filtros["idcurso"]=$this->fkcurso;
           	}
           	
            $this->nombre=!empty($_REQUEST["nombre"])?$_REQUEST["nombre"]:'';
            $this->nvacantes=!empty($_REQUEST["nvacantes"])?$_REQUEST["nvacantes"]:5; 
            $this->comentario=!empty($_REQUEST["comentario"])?$_REQUEST["comentario"]:'';
            $this->estado=!empty($_REQUEST["estado"])?$_REQUEST["estado"]:1;
            $this->strdocente=!empty($_REQUEST["strdocente"])?$_REQUEST["strdocente"]:'';
            $this->strlocal=!empty($_REQUEST["strlocal"])?$_REQUEST["strlocal"]:'';
            $this->strcurso=!empty($_REQUEST["strcurso"])?$_REQUEST["strcurso"]:'';
            $this->horarios=array();

            if(!empty($this->idgrupoaula))
            $this->grupoauladetalle=$this->oNegAcad_grupoaula->buscargrupos($filtros);
        	if(!empty($this->grupoauladetalle))
        		foreach ($this->grupoauladetalle as $rw){
        			$tmphorarios=$this->oNegHorariogrupoaula->buscar(array('idgrupoauladetalle'=>$rw["idgrupoauladetalle"]));
        			if(!empty($tmphorarios))
        			foreach ($tmphorarios as $hr){
        				$this->horarios[]='{id:'.$hr["idhorario"].
        									',title:\''.$rw["strcurso"].
        									'\',start:\''.$hr["fecha_finicio"].
        									'\',end:\''.$hr["fecha_final"].
        									'\',backgroundColor:\''.$hr["color"].
        									'\',color:\''.$hr["color"].'\'},';
        			}
        			//$this->grupoauladetalle['horarios']=$horarios;        			
        		}	
			$this->documento->setTitulo(JrTexto::_('Groups').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function horariosedit(){
		try {

			global $aplicacion;
			$this->esquema = 'academico/grupos-horario-formulario';
			$this->documento->setTitulo(JrTexto::_('Groups').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';

			$this->fktipos=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipogrupo','mostrar'=>1));
			$this->fktipo=!empty($_REQUEST["tipo"])?$_REQUEST["tipo"]:'V';
			$this->fkdocente=!empty($_REQUEST["iddocente"])?$_REQUEST["iddocente"]:0;
			$this->fkcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
			$this->strdocente=!empty($_REQUEST["strdocente"])&&!empty($this->fkdocente)?$_REQUEST["strdocente"]:'';
			$this->strcurso=!empty($_REQUEST["strcurso"])&&!empty($this->fkcurso)?$_REQUEST["strcurso"]:'';
			$this->fechaini=!empty($_REQUEST["fechaini"])?$_REQUEST["fechaini"]:date('Y-m-d');
			$this->fechafin=!empty($_REQUEST["fechafin"])?$_REQUEST["fechafin"]:date('Y-m-d');
			$this->horaini=!empty($_REQUEST["horaini"])?$_REQUEST["horaini"]:'08:00 am';
			$this->horafin=!empty($_REQUEST["horafin"])?$_REQUEST["horafin"]:'10:00 am';

			$this->grupoauladetalle=array();
			$this->horarios=array();
			if(!empty($_REQUEST["idgrupoaula"])){
				$this->idgrupoaula=$_REQUEST["idgrupoaula"];
           		$datosgrupo=$this->oNegAcad_grupoaula->buscar(array('idgrupoaula'=>$this->idgrupoaula));
				if(!empty($datosgrupo[0])){
					$this->datosgrupoaula=$datosgrupo[0];
					$this->fktipo=$this->datosgrupoaula["tipo"];
					if(empty($this->fkcurso)&&!empty($_REQUEST["idhorario"])){
						$this->idhorario=$_REQUEST["idhorario"];
						$horario_=$this->oNegHorariogrupoaula->buscar(array('idhorario'=>$this->idhorario));
						if(!empty($horario_[0])){
							$this->idgrupoauladetalle=$horario_[0]["idgrupoauladetalle"];
							$grupoauladetalle=$this->oNegAcad_grupoauladetalle->buscar(array('idgrupoauladetalle'=>$this->idgrupoauladetalle));
							if(!empty($grupoauladetalle[0])){
								$this->grupoauladetalle=$grupoauladetalle[0];
								$this->fkcurso=$this->grupoauladetalle["idcurso"];
								$this->strcurso=$this->grupoauladetalle["strcurso"];
								$this->fkdocente=$this->grupoauladetalle["iddocente"];
								$this->strdocente=$this->grupoauladetalle["strdocente"];
								$this->idgrupoauladetalle=$this->grupoauladetalle["idgrupoauladetalle"];
							}
						}
					}elseif(!empty($this->fkcurso)){
						$grupoauladetalle=$this->oNegAcad_grupoauladetalle->buscar(array('idgrupoaula'=>$this->datosgrupoaula["idgrupoaula"],'idcurso'=>$this->fkcurso));
						if(!empty($grupoauladetalle[0])){
							$this->grupoauladetalle=$grupoauladetalle[0];
							$this->fkcurso=$this->grupoauladetalle["idcurso"];
							$this->strcurso=$this->grupoauladetalle["strcurso"];
							//$this->fkdocente=$this->grupoauladetalle["iddocente"];
							//$this->strdocente=$this->grupoauladetalle["strdocente"];
							$this->idgrupoauladetalle=$this->grupoauladetalle["idgrupoauladetalle"];							
						}
					}
					if(!empty($this->idgrupoauladetalle)){	
						$tmphorarios=$this->oNegHorariogrupoaula->buscar(array('idgrupoauladetalle'=>$this->idgrupoauladetalle));
						if(!empty($tmphorarios))
	        			foreach ($tmphorarios as $hr){
	        				$this->horarios[]=array('id'=>$hr["idhorario"],'title'=>$this->grupoauladetalle["strcurso"],'start'=>$hr["fecha_finicio"],'end'=>$hr["fecha_final"],'backgroundColor'=>$hr["color"],'color'=>$hr["color"],'descripcion'=>$hr["descripcion"],'idhorariopadre'=>$hr["idhorariopadre"],'diasemana'=>$hr["diasemana"],'idhorariopadre'=>$hr["idhorariopadre"]);
	        			}
					}
				}
           	}
           	//var_dump(array('idgrupoaula'=>$this->datosgrupoaula["idgrupoaula"],'idcurso'=>$this->fkcurso));
           	//var_dump($this->horarios);
           	if($this->fktipo!='V'){
           		$this->fklocales=$this->oNegLocal->buscar();
           		$this->fklocal=!empty($_REQUEST["idlocal"])?$_REQUEST["idlocal"]:$this->fklocales[0]["idlocal"];
           		if(!empty($this->fklocal)){
           			$this->fkambientes=$this->oNegAmbiente->buscar(array('idlocal'=>$this->fkidlocal));
           		}
           	}
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Acad_grupoaula', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Acad_grupoaula').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Acad_grupoaula', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->oNegAcad_grupoaula->idgrupoaula = @$_GET['id'];
			$this->datos = $this->oNegAcad_grupoaula->dataAcad_grupoaula;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Acad_grupoaula').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->fktipo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipogrupo','mostrar'=>1));
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_grupoaula-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //
	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_grupoaula', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["comentario"])&&@$_REQUEST["comentario"]!='')$filtros["comentario"]=$_REQUEST["comentario"];
			if(isset($_REQUEST["nvacantes"])&&@$_REQUEST["nvacantes"]!='')$filtros["nvacantes"]=$_REQUEST["nvacantes"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			$this->datos=$this->oNegAcad_grupoaula->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function buscargruposjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_grupoaula', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["comentario"])&&@$_REQUEST["comentario"]!='')$filtros["comentario"]=$_REQUEST["comentario"];
			if(isset($_REQUEST["nvacantes"])&&@$_REQUEST["nvacantes"]!='')$filtros["nvacantes"]=$_REQUEST["nvacantes"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			$this->datos=$this->oNegAcad_grupoaula->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarAcad_grupoaula(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idgrupoaula)) {
				$this->oNegAcad_grupoaula->idgrupoaula = $idgrupoaula;
				$accion='_edit';				
			}
           	$usuarioAct = NegSesion::getUsuario();           	
			$this->oNegAcad_grupoaula->nombre=@$nombre;
			$this->oNegAcad_grupoaula->tipo=@$tipo;
			$this->oNegAcad_grupoaula->comentario=@$comentario;
			$this->oNegAcad_grupoaula->nvacantes=!empty($nvacantes)?$nvacantes:5;
			$this->oNegAcad_grupoaula->estado=!empty($estado)?$estado:1;
					
            if($accion=='_add') {
            	$res=$this->oNegAcad_grupoaula->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('grupoaula')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_grupoaula->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('grupoaula')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
            exit(0);
        }
	}

	public function guardarhorario(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			//var_dump($_POST);
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);

            $accion='_add';
            if(!empty($idgrupoaula)){
            	$grupoaula=$this->oNegAcad_grupoaula->buscar(array('idgrupoaula'=>$idgrupoaula));
            	if(!empty($grupoaula[0])){
            		$grupoaula_=$grupoaula[0];
					$this->oNegAcad_grupoaula->idgrupoaula = $idgrupoaula;
					$accion='_edit';
            	}
			}

           	$usuarioAct = NegSesion::getUsuario();           	
			$this->oNegAcad_grupoaula->nombre=$nombre;
			$this->oNegAcad_grupoaula->tipo=!empty($tipo)?$tipo:'';
			$this->oNegAcad_grupoaula->comentario=$comentario;
			$this->oNegAcad_grupoaula->nvacantes=!empty($nvacantes)?$nvacantes:5;
			$this->oNegAcad_grupoaula->estado=!empty($estado)?$estado:1;
			if($accion=='_add'){ $idgrupoaula=$this->oNegAcad_grupoaula->agregar();
            }else{$idgrupoaula=$this->oNegAcad_grupoaula->editar(); }

            if(ucfirst($tipo)!='V'){
				$idlocal=0;
				$idambiente=0;
			}
			$accion='_add';
			if(!empty($idgrupoauladetalle)) {
				$grupoauladetalle=$this->oNegAcad_grupoauladetalle->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle));
				//var_dump($grupoauladetalle);
				if(!empty($grupoauladetalle[0])){
            		$grupoauladetalle_=$grupoauladetalle[0];
					$this->oNegAcad_grupoauladetalle->idgrupoauladetalle = $idgrupoauladetalle;
					$accion='_edit';
            	}				
			}
            $this->oNegAcad_grupoauladetalle->idgrupoaula=$idgrupoaula;
			$this->oNegAcad_grupoauladetalle->idcurso=!empty($idcurso)?$idcurso:0;
			$this->oNegAcad_grupoauladetalle->iddocente=!empty($iddocente)?$iddocente:0;
			$this->oNegAcad_grupoauladetalle->idlocal=!empty($local)?intVal($local):0;
			$this->oNegAcad_grupoauladetalle->idambiente=!empty($ambiente)?intVal($ambiente):0;
			$this->oNegAcad_grupoauladetalle->nombre='-';
			$this->oNegAcad_grupoauladetalle->fecha_inicio=!empty($fecha_inicio)?$fecha_inicio:date('Y/m/d');
			$this->oNegAcad_grupoauladetalle->fecha_final=!empty($fecha_final)?$fecha_final:date('Y/m/d');			
			if($accion=='_add'){ $idgrupoauladetalle=$this->oNegAcad_grupoauladetalle->agregar();
            }else{$idgrupoauladetalle=$this->oNegAcad_grupoauladetalle->editar(); }	

			$datos=array();
			$datos['horarios']=array();
			$idpadresarray=array();
			$tmpfechas=json_decode($fechashorario);
			if(!empty($tmpfechas))
			foreach ($tmpfechas as $tmp){
				$accion='_add';
				$idhorario=!empty($tmp->idhorario)?$tmp->idhorario:0;
				$idhorariopadre=!empty($tmp->idhorariopadre)?$tmp->idhorariopadre:0;
				if($tmp->idhorario>0){
					$ghorariodetalle=$this->oNegHorariogrupoaula->buscar(array('idhorario'=>$tmp->idhorario));
					if(!empty($ghorariodetalle[0])){
	            		$ghorariodetalle_=$ghorariodetalle[0];
						$this->oNegHorariogrupoaula->idhorario=$tmp->idhorario;
						$accion='_edit';
	            	}					
				}				

				if($idhorariopadre<0){
					if(!empty($idpadresarray[$idhorariopadre])){
						$idhorariopadre=$idpadresarray[$idhorariopadre];
					}else{
						$maxidpadre=$this->oNegHorariogrupoaula->maxidpadre($idgrupoauladetalle);
						$maxidpadre++;
						$idpadresarray[$idhorariopadre]=($maxidpadre);
						$idhorariopadre=$maxidpadre;
					}
				}
				$this->oNegHorariogrupoaula->idgrupoauladetalle=$idgrupoauladetalle;
				$this->oNegHorariogrupoaula->fecha_finicio=!empty($tmp->start)?$tmp->start:date('Y/m/d h:i:s');
				$this->oNegHorariogrupoaula->fecha_final=!empty($tmp->end)?$tmp->end:date('Y/m/d h:i:s');
				$this->oNegHorariogrupoaula->descripcion=!empty($tmp->title)?$tmp->title:'';
				$this->oNegHorariogrupoaula->color=$tmp->color=!empty($tmp->color)?$tmp->color:$color;
				$this->oNegHorariogrupoaula->idhorariopadre=$idhorariopadre;
				$this->oNegHorariogrupoaula->diasemana=!empty($tmp->diasemana)?$tmp->diasemana:0;
				if($accion=='_add')
					$res=$this->oNegHorariogrupoaula->agregar();
				else
					$res=$this->oNegHorariogrupoaula->editar();
				$datos['horarios'][$res]=$tmp;
			}            
            $datos["idgrupoaula"]=$idgrupoaula;
            $datos["idcurso"]=!empty($idcurso)?$idcurso:0;
            $datos["iddocente"]=!empty($iddocente)?$iddocente:0;
            $datos["idlocal"]=!empty($local)?$local:0;           
            $datos["nombreaula"]=$nombre;
            $datos["idgrupoauladetalle"]=$idgrupoauladetalle;            
			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('horario')).' '.JrTexto::_('saved successfully'),'datos'=>$datos)); 
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveAcad_grupoaula(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdgrupoaula'])) {
					$this->oNegAcad_grupoaula->idgrupoaula = $frm['pkIdgrupoaula'];
				}
				
				$this->oNegAcad_grupoaula->nombre=@$frm["txtNombre"];
					$this->oNegAcad_grupoaula->tipo=@$frm["txtTipo"];
					$this->oNegAcad_grupoaula->comentario=@$frm["txtComentario"];
					$this->oNegAcad_grupoaula->nvacantes=@$frm["txtNvacantes"];
					$this->oNegAcad_grupoaula->estado=@$frm["txtEstado"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAcad_grupoaula->agregar();
					}else{
									    $res=$this->oNegAcad_grupoaula->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAcad_grupoaula->idgrupoaula);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAcad_grupoaula(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_grupoaula->__set('idgrupoaula', $pk);
				$this->datos = $this->oNegAcad_grupoaula->dataAcad_grupoaula;
				$res=$this->oNegAcad_grupoaula->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_grupoaula->__set('idgrupoaula', $pk);
				$res=$this->oNegAcad_grupoaula->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAcad_grupoaula->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	    
}