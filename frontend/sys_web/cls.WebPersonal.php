<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-01-2018 
 * @copyright	Copyright (C) 31-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUbigeo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRoles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_apoderado', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_metas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_educacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_experiencialaboral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_referencia', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_metas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_record', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_horariogrupodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE, 'sys_negocio');
class WebPersonal extends JrWeb
{
	private $oNegPersonal;
	private $oNegGeneral;
	private $oNegUbigeo;
	private $oNegUgel;
	private $oNegRoles;
	private $oNegAcad_matricula;
	private $oNegAcad_grupoaula;
	private $oNegHorariogrupoaula;
	private $oNegAcad_grupoauladetalle;	
	private $oNegAcad_curso;
	private $oNegPersona_rol;
	private $oNegPerRecord;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersonal = new NegPersonal;
		$this->oNegPerApoderado = new NegPersona_apoderado;
		$this->oNegPerMetas = new NegPersona_metas;
		$this->oNegPerEducacion = new NegPersona_educacion;
		$this->oNegPerExplaboral = new NegPersona_experiencialaboral;
		$this->oNegPerReferencia = new NegPersona_referencia;
		$this->oNegPerMetas = new NegPersona_metas;
		$this->oNegPerRecord = new NegPersona_record;
		$this->oNegGeneral = new NegGeneral;
		$this->oNegUbigeo = new NegUbigeo;
		$this->oNegUgel = new NegUgel;
		$this->oNegRoles = new NegRoles;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegGrupoaula = new NegAcad_grupoaula;
		$this->oNegGrupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegHorariogrupoaula = new NegAcad_horariogrupodetalle;
		$this->oNegCurso = new NegAcad_curso;
		$this->oNegPersona_rol = new NegPersona_rol;
	}

	public function defecto(){
        $this->documento->script('events', '/libs/fullcalendar/');    
		return $this->filtros();
	}

	public function filtros()
	{
		try{
			global $aplicacion;				
            $this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			//$this->documento->script('croppie.min', '/libs/croppie/');
			//$this->documento->stylesheet('croppie', '/libs/croppie/');

			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->fkubigeo=$this->oNegUbigeo->buscar();
			$this->fkidugel=$this->oNegUgel->buscar();
			$this->idrol=!empty($_REQUEST["rol"])?$_REQUEST["rol"]:2;
			$this->idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			$this->fkrol=$this->oNegRoles->buscar();
			$this->fkcursos=$this->oNegCurso->buscar(array('orderby','nombre'));
			$vista = !empty($_REQUEST['verlis']) ? $_REQUEST['verlis'] : '';
			$this->documento->setTitulo(JrTexto::_('Personal'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->esquema = 'academico/personal-filtros'.$vista;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function listado(){
		try{
			global $aplicacion;
			$filtros=array();
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tipodoc"])&&@$_REQUEST["tipodoc"]!='')$filtros["tipodoc"]=$_REQUEST["tipodoc"];
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["ape_paterno"]!='')$filtros["ape_paterno"]=$_REQUEST["ape_paterno"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["ape_materno"]!='')$filtros["ape_materno"]=$_REQUEST["ape_materno"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fechanac"])&&@$_REQUEST["fechanac"]!='')$filtros["fechanac"]=$_REQUEST["fechanac"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["estado_civil"])&&@$_REQUEST["estado_civil"]!='')$filtros["estado_civil"]=$_REQUEST["estado_civil"];
			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"];
			if(isset($_REQUEST["urbanizacion"])&&@$_REQUEST["urbanizacion"]!='')$filtros["urbanizacion"]=$_REQUEST["urbanizacion"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["token"])&&@$_REQUEST["token"]!='')$filtros["token"]=$_REQUEST["token"];
			if(isset($_REQUEST["rol"])&&@$_REQUEST["rol"]!='') $filtros["rol"]=$_REQUEST["rol"];
			else{$filtros["rol"]=1;}
			$this->idrol=$filtros["rol"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			if(isset($_REQUEST["idioma"])&&@$_REQUEST["idioma"]!='')$filtros["idioma"]=$_REQUEST["idioma"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->datos=$this->oNegPersonal->buscar($filtros);
			$this->documento->setTitulo(JrTexto::_('Personal').' /', true);			
			$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:'1';
			$this->esquema = 'usuario/personal-vista'.$vista;
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';			
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function formulario(){
		try {
			global $aplicacion;			
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->esquema = 'usuario/formulario';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			$this->isdocente=false;
			$this->isalumno=false;
			if(!empty($_REQUEST['idpersona'])){
				$idpersona=$_REQUEST['idpersona'];
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
				if(!empty($this->datos)) $this->isdocente=$this->datos["rol"]==2?true:false;
				if(!empty($this->datos)) $this->isalumno=$this->datos["rol"]==3?true:false;
				if($this->isdocente==false){	
					$roli=$this->oNegPersona_rol->buscar(array('idpersonal'=>$idpersona,'idrol'=>2));
					$this->isdocente=!empty($roli[0])?true:false;
				}
				if($this->isalumno==false){	
					$roli=$this->oNegPersona_rol->buscar(array('idpersonal'=>$idpersona,'idrol'=>3));
					$this->isalumno=!empty($roli[0])?true:false;
				}
			}
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


    /******** menus primeros */
	public function informacion(){
		try {
			global $aplicacion;			
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');			
			$this->esquema = 'usuario/formulario-informacion';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			$this->isdocente=false;
			$this->isalumno=false;
			if(!empty($_REQUEST['idpersona'])){
				$idpersona=$_REQUEST['idpersona'];
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
				if(!empty($this->datos)) $this->isdocente=$this->datos["rol"]==2?true:false;
				if(!empty($this->datos)) $this->isalumno=$this->datos["rol"]==3?true:false;
				if($this->isdocente==false){	
					$roli=$this->oNegPersona_rol->buscar(array('idpersonal'=>$idpersona,'idrol'=>2));
					$this->isdocente=!empty($roli[0])?true:false;
				}
				if($this->isalumno==false){	
					$roli=$this->oNegPersona_rol->buscar(array('idpersonal'=>$idpersona,'idrol'=>3));
					$this->isalumno=!empty($roli[0])?true:false;
				}
			}
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_datospersonales(){
		try {
			global $aplicacion;	
			$this->esquema = 'usuario/formulario-datospersonales';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
			}
			$this->fktipodoc=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipodocidentidad','mostrar'=>1));
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_direccion(){
		try {
			global $aplicacion;	
			$this->esquema = 'usuario/formulario-direccion';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			$this->fkpais=$this->oNegUbigeo->buscar(array('pais'=>'all'));			
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
			}
			$this->fkpais=$this->oNegUbigeo->buscar(array('pais'=>'all'));			
			if(!empty($this->fkpais[0])){
				//$pais='PE';
				$pais='PE';
				$this->idpais=$pais;
				$idubigeo=str_pad($this->datos["ubigeo"],6,'0');
				$this->fkdepartamento=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>'all'));				
				if(!empty($this->fkdepartamento[0])){
					$depa=substr($idubigeo, 0,2);
					$this->iddepa=!empty($depa)&&@$depa!='00'?$depa:@$this->fkdepartamento[0]["departamento"];
					$this->fkprovincia=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>$this->iddepa,'provincia'=>'all'));
					if(!empty($this->fkprovincia[0])){
						$pro=substr($idubigeo, 2,2);
						$this->idpro=!empty($pro)&&@$pro!='00'?$pro:@$this->fkprovincia[0]["provincia"];
						$this->fkdistrito=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>$this->iddepa,'provincia'=>$this->idpro,'distrito'=>'all'));
						$this->iddis=substr($idubigeo, -2);
					}
				}
			}
			//$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			//$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_apoderado(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-apoderados';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->apoderados=$this->oNegPerApoderado->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			$this->fkparentesco=$this->oNegGeneral->buscar(array('tipo_tabla'=>'parentesco','mostrar'=>1));
			$this->fktipodoc=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipodocidentidad','mostrar'=>1));
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_educacion(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-educacion';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->educacion=$this->oNegPerEducacion->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}			
			$this->fktipoestudio=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipoestudio','mostrar'=>1));
			$this->fkareaestudio=$this->oNegGeneral->buscar(array('tipo_tabla'=>'areaestudio','mostrar'=>1));
			$this->fksituacionestudio=$this->oNegGeneral->buscar(array('tipo_tabla'=>'situacionestudio','mostrar'=>1));
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_record(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-record';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';			
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->records=$this->oNegPerRecord->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}			
			$this->fktiporecord=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tiporecord','mostrar'=>1));			
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_experiencialaboral(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-experiencialaboral';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->experiencias=$this->oNegPerExplaboral->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}	
			$this->fkarea=$this->oNegGeneral->buscar(array('tipo_tabla'=>'areaempresa','mostrar'=>1));			
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_metas(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-metas';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->metas=$this->oNegPerMetas->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function frm_referencias(){
		try {
			global $aplicacion;
			$this->esquema = 'usuario/formulario-referencias';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->referencias=$this->oNegPerReferencia->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
				$this->accion= !empty($this->datos)?'edit':'add';
			}			
			
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	
    public function getgrupoauladetalle($idgrupoauladetalle){
		try {
			global $aplicacion;	
			if($idgrupoauladetalle){
				$grupoauladetalle=$this->oNegGrupoauladetalle->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle));
				if(!empty($grupoauladetalle[0])){
					return $grupoauladetalle[0];
				}else return false;
			}else return false;
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function grupoestudio(){
		try {
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->esquema = 'usuario/grupoestudio';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$idpersona=@$_REQUEST['idpersona'];
				$this->oNegPersonal->idpersona = $idpersona;
				$this->persona = $this->oNegPersonal->dataPersonal;
				$this->gruposdocente=$this->oNegGrupoauladetalle->buscar(array('iddocente'=>$idpersona));
				$this->comoalumno=$this->oNegMatricula->buscar(array('idalumno'=>$idpersona));				
			}
			$this->documento->setTitulo(JrTexto::_('Groups').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function cursos(){
		try {
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->esquema = 'usuario/cursos';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';
			if(!empty($_REQUEST['idpersona'])){
				$idpersona=@$_REQUEST['idpersona'];
				$this->oNegPersonal->idpersona = $idpersona;
				$this->persona = $this->oNegPersonal->dataPersonal;
				$this->gruposdocente=$this->oNegGrupoauladetalle->buscar(array('iddocente'=>$idpersona));
				$this->comoalumno=$this->oNegMatricula->buscar(array('idalumno'=>$idpersona));				
			}
			$this->documento->setTitulo(JrTexto::_('Courses').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function notas(){
		try {
			global $aplicacion;	
			$this->esquema = 'usuario/notas';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';	
			if(!empty($_REQUEST['idpersona'])){
				$this->oNegPersonal->idpersona = @$_REQUEST['idpersona'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
			}
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function horarios(){
		try {
			global $aplicacion;	
			$this->documento->script('moment.min', '/libs/fullcalendar/');
            $this->documento->script('datetimepicker', '/libs/datepicker/');
            $this->documento->script('fullcalendar', '/libs/fullcalendar/');
            $this->documento->script('locale-all', '/libs/fullcalendar/');
            $this->documento->script('calendar', '/libs/fullcalendar/');
            $this->documento->script('events', '/libs/fullcalendar/');
            $this->documento->stylesheet('fullcalendar', '/libs/fullcalendar/');
            $this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
			$this->esquema = 'usuario/horarios';
			if(!empty($_REQUEST['idpersona'])){
				$idpersona=@$_REQUEST['idpersona'];
				$this->oNegPersonal->idpersona = $idpersona;
				$this->persona = $this->oNegPersonal->dataPersonal;
				$this->gruposdocente=$this->oNegGrupoauladetalle->buscar(array('iddocente'=>$idpersona));
				$this->horarios=array();
				if(!empty($this->gruposdocente))
					foreach ($this->gruposdocente as $rw){
        			$tmphorarios=$this->oNegHorariogrupoaula->buscar(array('idgrupoauladetalle'=>$rw["idgrupoauladetalle"]));
        			if(!empty($tmphorarios))
        			foreach ($tmphorarios as $hr){
        				$this->horarios[$hr["idhorario"]]='{id:'.$hr["idhorario"].
        									',title:\''.$rw["strcurso"].
        									'\',start:\''.$hr["fecha_finicio"].
        									'\',end:\''.$hr["fecha_final"].
        									'\',backgroundColor:\''.$hr["color"].
        									'\',color:\''.$hr["color"].'\'},';
        			}
        		}
				$this->comoalumno=$this->oNegMatricula->buscar(array('idalumno'=>$idpersona));
				if(!empty($this->comoalumno))
					foreach ($this->comoalumno as $rw){
        			$tmphorarios=$this->oNegHorariogrupoaula->buscar(array('idgrupoauladetalle'=>$rw["idgrupoauladetalle"]));
        			if(!empty($tmphorarios))
        			foreach ($tmphorarios as $hr){
        				$this->horarios[$hr["idhorario"]]='{id:'.$hr["idhorario"].
        									',title:\''.$rw["strcurso"].
        									'\',start:\''.$hr["fecha_finicio"].
        									'\',end:\''.$hr["fecha_final"].
        									'\',backgroundColor:\''.$hr["color"].
        									'\',color:\''.$hr["color"].'\'},';
        			}
        		}
			}			
			$this->documento->setTitulo(JrTexto::_('Schedule').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function historial(){
		try {
			global $aplicacion;	
			/*$this->esquema = 'usuario/formulario-informacion';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';	
			if(!empty($_REQUEST['dni'])){
				$this->oNegPersonal->dni = @$_REQUEST['dni'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
			}			*/
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function asistencias(){
		try {
			global $aplicacion;	
			/*$this->esquema = 'usuario/formulario-informacion';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';	
			if(!empty($_REQUEST['dni'])){
				$this->oNegPersonal->dni = @$_REQUEST['dni'];
				$this->datos = $this->oNegPersonal->dataPersonal;
				$this->accion= !empty($this->datos)?'edit':'add';
			}			*/
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function perfil(){
		try {
			global $aplicacion;			
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');			
			$this->esquema = 'usuario/personal-perfil';
			$this->accion=!empty($_REQUEST["accion"])?$_REQUEST["accion"]:'add';	
			if(!empty($_REQUEST['idpersona'])){
				$idpersona = @$_REQUEST['idpersona'];								
			}else{
				$usuarioAct = NegSesion::getUsuario();				
				$idpersona = $usuarioAct["idpersona"];				
			}

			$this->oNegPersonal->idpersona = $idpersona;
			$this->datos = $this->oNegPersonal->dataPersonal;
			$this->apoderados=$this->oNegPerApoderado->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
			$this->educacion=$this->oNegPerEducacion->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
			$this->experiencias=$this->oNegPerExplaboral->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
			$this->metas=$this->oNegPerMetas->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
			$this->referencias=$this->oNegPerReferencia->buscar(array('idpersona'=>@$_REQUEST['idpersona']));
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');		
			//$this->esquema = 'personal-frm';
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	} 

	public function json_datos(){
		$this->documento->plantilla = 'verblanco';
		if(!empty($_REQUEST['idpersona'])){
			$idpersona = @$_REQUEST['idpersona'];								
		}else{
			$usuarioAct = NegSesion::getUsuario();				
			$idpersona = $usuarioAct["idpersona"];				
		}
		$this->oNegPersonal->idpersona = $idpersona;
		$this->datos = $this->oNegPersonal->dataPersonal;
		echo json_encode(array('code'=>200,'datos'=>$this->datos));
		exit(0);
	}

	public function verficha(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Student').' /'.JrTexto::_('Ficha'), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			if(isset($_REQUEST["id"])&&@$_REQUEST["id"]!=''){
				$this->oNegPersonal->idpersona=$_REQUEST["id"];
				$this->datos=$this->oNegPersonal->dataPersonal;				
				if(empty($this->datos)){
					throw new Exception(JrTexto::_('Restricted access').'!!');	
					return;
				}
			}else{
				throw new Exception(JrTexto::_('Restricted access').'!!');	
				return;
			}
			$this->esquema = 'docente/ficha';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function cambiarclave(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Student').' /'.ucfirst(JrTexto::_('Change Password')), true);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->user='personal';
			$this->esquema = 'usuario/cambiarclave';
			if((isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')||(isset($_REQUEST["id"])&&@$_REQUEST["id"]!='')){


				$this->oNegPersonal->idpersona=!empty($_REQUEST["idpersona"])?$_REQUEST["idpersona"]:$_REQUEST["id"];
				$this->datos=$this->oNegPersonal->dataPersonal;
				if(empty($this->datos)){
					throw new Exception(JrTexto::_('Restricted access').'!!');	
					return;
				}
			}else{
				throw new Exception(JrTexto::_('Restricted access').'!!');	
				return;
			}
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}	
	}

	public function guardarDatos(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			@extract($_POST);
			$accion='add';			
			if(!empty($idpersona)){
				$datos=$this->oNegPersonal->buscar(array('idpersona'=>$idpersona));
				if(!empty($datos)){
					$accion='edit';
					$this->oNegPersonal->set('idpersona',$idpersona);
				}
			}

			$this->oNegPersonal->set('regusuario',$usuarioAct["idpersona"]);
			foreach ($_POST as $key => $value){
				if($key!='idpersona')
					$this->oNegPersonal->set($key,$value);
			}
			if(!empty($_FILES['fotouser'])){
				$file=$_FILES["fotouser"];
				$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
				$newfoto=$idpersona.'-'.date("Ymdhis").'.'.$ext;
				$dir_media=RUTA_BASE . 'static' . SD . 'media' . SD . 'usuarios' ;
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newfoto)) 
		  		{
		  			$this->oNegPersonal->set('foto',$newfoto);
		  		}					
			}
			//}
			if($accion=='add'){
				$res=$this->oNegPersonal->agregar($idpersona);
        	 	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Personal')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
			}else{
				$res=$this->oNegPersonal->editar();
        		echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Personal')).' '.JrTexto::_('update successfully'),'newid'=>$idpersona)); 
			}   	
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
            exit(0);
        }
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Personal', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tipodoc"])&&@$_REQUEST["tipodoc"]!='')$filtros["tipodoc"]=$_REQUEST["tipodoc"];
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["ape_paterno"]!='')$filtros["ape_paterno"]=$_REQUEST["ape_paterno"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["ape_materno"]!='')$filtros["ape_materno"]=$_REQUEST["ape_materno"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fechanac"])&&@$_REQUEST["fechanac"]!='')$filtros["fechanac"]=$_REQUEST["fechanac"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["estado_civil"])&&@$_REQUEST["estado_civil"]!='')$filtros["estado_civil"]=$_REQUEST["estado_civil"];
			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"];
			if(isset($_REQUEST["urbanizacion"])&&@$_REQUEST["urbanizacion"]!='')$filtros["urbanizacion"]=$_REQUEST["urbanizacion"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["token"])&&@$_REQUEST["token"]!='')$filtros["token"]=$_REQUEST["token"];
			if(isset($_REQUEST["rol"])&&@$_REQUEST["rol"]!='')$filtros["rol"]=$_REQUEST["rol"];
			if(isset($_REQUEST["foto"])&&@$_REQUEST["foto"]!='')$filtros["foto"]=$_REQUEST["foto"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			if(isset($_REQUEST["idioma"])&&@$_REQUEST["idioma"]!='')$filtros["idioma"]=$_REQUEST["idioma"];
						
			$this->datos=$this->oNegPersonal->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarPersonal(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdpersona)) {
				$this->oNegPersonal->idpersona = $frm['pkIdpersona'];
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	           	
			$this->oNegPersonal->tipodoc=@$txtTipodoc;
			$this->oNegPersonal->dni=@$txtDni;
			$this->oNegPersonal->ape_paterno=@$txtApe_paterno;
			$this->oNegPersonal->ape_materno=@$txtApe_materno;
			$this->oNegPersonal->nombre=@$txtNombre;
			$this->oNegPersonal->fechanac=@$txtFechanac;
			$this->oNegPersonal->sexo=@$txtSexo;
			$this->oNegPersonal->estado_civil=@$txtEstado_civil;
			$this->oNegPersonal->ubigeo=@$txtUbigeo;
			$this->oNegPersonal->urbanizacion=@$txtUrbanizacion;
			$this->oNegPersonal->direccion=@$txtDireccion;
			$this->oNegPersonal->telefono=@$txtTelefono;
			$this->oNegPersonal->celular=@$txtCelular;
			$this->oNegPersonal->email=@$txtEmail;
			$this->oNegPersonal->idugel=@$txtIdugel;
			$this->oNegPersonal->regusuario=@$txtRegusuario;
			$this->oNegPersonal->regfecha=@$txtRegfecha;
			$this->oNegPersonal->usuario=@$txtUsuario;
			$this->oNegPersonal->clave=@$txtClave;
			$this->oNegPersonal->token=@$txtToken;
			$this->oNegPersonal->rol=@$txtRol;
			$this->oNegPersonal->foto=@$txtFoto;
			$this->oNegPersonal->estado=@$txtEstado;
			$this->oNegPersonal->situacion=@$txtSituacion;
			$this->oNegPersonal->idioma=@$txtIdioma;
					
            if($accion=='_add') {
            	$res=$this->oNegPersonal->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Personal')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPersonal->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Personal')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarclave(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);
            }
      
			global $aplicacion;
            @extract($_POST);
            if(!empty(@$idpersona)) {
				$this->oNegPersonal->idpersona = $idpersona;
				$this->oNegPersonal->email=@$email;
				$this->oNegPersonal->usuario=@$usuario;
				$this->oNegPersonal->clave=md5($clave);
				$this->oNegPersonal->token=md5($clave);
				$res=$this->oNegPersonal->editar();
				
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Password')).' '.JrTexto::_('update successfully'),'newid'=>$res));
			}
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSavePersonal(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdpersona'])) {
					$this->oNegPersonal->idpersona = $frm['pkIdpersona'];
				}
				
				$this->oNegPersonal->tipodoc=@$frm["txtTipodoc"];
					$this->oNegPersonal->dni=@$frm["txtDni"];
					$this->oNegPersonal->ape_paterno=@$frm["txtApe_paterno"];
					$this->oNegPersonal->ape_materno=@$frm["txtApe_materno"];
					$this->oNegPersonal->nombre=@$frm["txtNombre"];
					$this->oNegPersonal->fechanac=@$frm["txtFechanac"];
					$this->oNegPersonal->sexo=@$frm["txtSexo"];
					$this->oNegPersonal->estado_civil=@$frm["txtEstado_civil"];
					$this->oNegPersonal->ubigeo=@$frm["txtUbigeo"];
					$this->oNegPersonal->urbanizacion=@$frm["txtUrbanizacion"];
					$this->oNegPersonal->direccion=@$frm["txtDireccion"];
					$this->oNegPersonal->telefono=@$frm["txtTelefono"];
					$this->oNegPersonal->celular=@$frm["txtCelular"];
					$this->oNegPersonal->email=@$frm["txtEmail"];
					$this->oNegPersonal->idugel=@$frm["txtIdugel"];
					$this->oNegPersonal->regusuario=@$frm["txtRegusuario"];
					$this->oNegPersonal->regfecha=@$frm["txtRegfecha"];
					$this->oNegPersonal->usuario=@$frm["txtUsuario"];
					$this->oNegPersonal->clave=@$frm["txtClave"];
					$this->oNegPersonal->token=@$frm["txtToken"];
					$this->oNegPersonal->rol=@$frm["txtRol"];
					$this->oNegPersonal->foto=@$frm["txtFoto"];
					$this->oNegPersonal->estado=@$frm["txtEstado"];
					$this->oNegPersonal->situacion=@$frm["txtSituacion"];
					$this->oNegPersonal->idioma=@$frm["txtIdioma"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegPersonal->agregar();
					}else{
									    $res=$this->oNegPersonal->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegPersonal->idpersona);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDPersonal(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersonal->__set('idpersona', $pk);
				$this->datos = $this->oNegPersonal->dataPersonal;
				$res=$this->oNegPersonal->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersonal->__set('idpersona', $pk);
				$res=$this->oNegPersonal->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegPersonal->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}