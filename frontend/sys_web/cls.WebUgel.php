<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-01-2017 
 * @copyright	Copyright (C) 10-01-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
class WebUgel extends JrWeb
{
	private $oNegUgel;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegUgel = new NegUgel;
	}

	public function defecto(){
		return $this->listado();
		
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Ugel', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegUgel->buscar();

			
			/*$this->fkiddepartamento=$this->oNegUgel->listareo();
			$this->fkidprovincia=$this->oNegUgel->listareo();*/
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Ugel'), true);
			$this->esquema = 'ugel-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Ugel', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Ugel').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Ugel', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			

			


			$this->frmaccion='Editar';
			$this->oNegUgel->idugel = @$_GET['id'];
			$this->datos = $this->oNegUgel->dataUgel;

			$_iddep=$this->datos['iddepartamento'];			
			$this->provincias=$this->oNegUgel->ubigeo(array('idpadre'=>$_iddep));

			//var_dump($this->datos);

			$this->pk=@$_GET['id'];
			//$this->documento->setTitulo(JrTexto::_('Ugel').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegUgel->idugel = @$_GET['id'];
			$this->datos = $this->oNegUgel->dataUgel;
			
			$_iddep=$this->datos['iddepartamento'];			
			$this->provincias=$this->oNegUgel->ubigeo(array('idpadre'=>$_iddep));

			//$this->documento->setTitulo(JrTexto::_('Ugel').' /'.JrTexto::_('see'), true);
			$this->pk=@$_GET['id'];
			$this->esquema = 'ugel-see';
			//$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
							
			$this->esquema = 'ugel-frm';
			
			$this->departamentos=$this->oNegUgel->ubigeo(array('iddep'=>'OK'));
			
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveUgel(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdugel'])) {
					$this->oNegUgel->idugel = $frm['pkIdugel'];
				}
				
				$this->oNegUgel->__set('descripcion',@$frm["txtDescripcion"]);
					$this->oNegUgel->__set('abrev',@$frm["txtAbrev"]);
					$this->oNegUgel->__set('iddepartamento',@$frm["txtIddepartamento"]);
					$this->oNegUgel->__set('idprovincia',@$frm["txtIdprovincia"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegUgel->agregar();
					}else{
									    $res=$this->oNegUgel->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegUgel->idugel);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDUgel(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegUgel->__set('idugel', $pk);
				$this->datos = $this->oNegUgel->dataUgel;
				$res=$this->oNegUgel->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegUgel->__set('idugel', $pk);
				$res=$this->oNegUgel->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xGetxPadre(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];
				$datos=$this->oNegUgel->ubigeo($filtro);				
				$oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	     
}