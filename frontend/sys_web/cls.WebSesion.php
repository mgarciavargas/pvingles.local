<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
class WebSesion extends JrWeb
{
	private $oNegSesion;
	protected $oNegConfig;
	public $oNegHistorialSesion;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegSesion = new NegSesion;
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->oNegHistorialSesion = new NegHistorial_sesion;
	}
	public function defecto()
	{
		return $this->login();
	}
	public function login()
	{
		try {
			global $aplicacion;	
			if(true === NegSesion::existeSesion()) {
				return $aplicacion->redir();
			}			
			if(!empty($_REQUEST['t'])) {
				if(true === $this->oNegSesion->ingresarxToken($_REQUEST['t'])) {
					return $aplicacion->redir(JrAplicacion::getJrUrl(array('perfil', 'cambiar-clave')), false);
				}
			}	
			$this->idioma=NegSesion::get('idioma','m3c_gen__');
			@extract($_POST, EXTR_OVERWRITE);
			
			if(!empty($usuario) && !empty($clave)){
				if(true === $this->oNegSesion->ingresar(@$usuario, @$clave)){
					$usuarioAct = NegSesion::getUsuario();
					$roles=$usuarioAct["roles"];
					$this->cambiarIdiomaProyecto();
					$this->iniciarHistorialSesion('P');
					if(empty($roles)){
						$aplicacion->redir('sesion/noroles');
					}elseif(count($roles)>1){
						$idrol=$usuarioAct["idrol"];
						$hayrols=false;
						if(!empty($roles))
							foreach ($roles as $k => $v){								
								if($idrol==$v["idrol"]){
									$hayrols=true;
									$aplicacion->redir('sesion/cambiar_ambito/?idrol='.$idrol."&rol=".$v["rol"]);
									exit();
								}								
							}
						if($hayrols==false)$aplicacion->redir('sesion/cambiarrol');
					}
					
					@session_start();					
					if(!empty($_SESSION["urlredirok"])){
						$urltmp=$_SESSION["urlredirok"];
						$_SESSION["urlredirok"]='';
						unset($_SESSION["urlredirok"]);
						$urltmp=str_replace(URL_BASE."/","",$urltmp);
						$url=explode('/',$urltmp);
						if(!empty($url[1])){
							$url2=explode('?',$url[1]);
							$url[1]=str_replace('/','',$url2[0]);
						}else $url[1]='';
						$urltmp2=@$url[0]."/".@$url[1];
						$noredirec=array('sesion/salir','');						
						if(in_array($urltmp2,$noredirec)){
							return $aplicacion->redir();
						}
						return $aplicacion->redir($urltmp);
					}else{
						return $aplicacion->redir();
					}
				}				
				$this->msjErrorLogin = true;
			}			
			return $this->form(@$usuario);
		} catch(Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			return $this->form(@$usuario);
		}
	}
	public function noroles(){
		try {
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Change empty'), true);
			$this->documento->plantilla = 'excepcion';
			$this->msj=JrTexto::_('Rol empty').'!!';
			$this->esquema = 'error/general';
			if(true === NegSesion::existeSesion()) {
				$this->terminarHistorialSesion('P');
				$this->oNegSesion->salir();
			}
			return parent::getEsquema();
		} catch(Exception $e) {			
			//var_dump($e);
			$aplicacion->redir();
		}		
	}
	public function cambiarrol(){
		try {
			global $aplicacion;			
			$this->documento->setTitulo(JrTexto::_('Change Rol'), true);
			$plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'verblanco';

			$this->documento->plantilla = $plantilla;//!empty($plantilla) ? $plantilla : 'excepcion';			
			$this->esquema = 'academico/roles-cambiar';
			$usuarioAct = NegSesion::getUsuario();
			$this->roles=$usuarioAct["roles"];
			return parent::getEsquema();
		} catch(Exception $e) {			
			//var_dump($e);
			$aplicacion->redir();
		}
	}
	protected function form($usuario = null)
	{
		try {
			global $aplicacion;			
			if(true == NegSesion::existeSesion()) {
				$aplicacion->redir();
			}			
			$this->usuario = $usuario;			
			$this->documento->plantilla = 'login';
			$this->esquema = 'login';
			return parent::getEsquema();
		} catch(Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			$aplicacion->redir();
		}
	}	
	public function salir()
	{
		global $aplicacion;
		try {			
			if(true === NegSesion::existeSesion()) {
				$this->terminarHistorialSesion('P');
				$this->oNegSesion->salir();
			}			
			$aplicacion->redir();
		} catch(Exception $e){
			if(true === NegSesion::existeSesion()) {
				$this->oNegSesion->salir();
			}
			$aplicacion->redir();
		}
	}	
	public function cambiar_ambito()
	{
		try{
			global $aplicacion;			
			$usuarioAct = NegSesion::getUsuario();	
			//exit($usuarioAct);
			if(!empty($_GET['rol'])&&!empty($_GET['idrol'])) {
				$oNegSesion = new NegSesion;
				$cambio=$oNegSesion->cambiar_rol($_GET['idrol'],$_GET['rol'],$usuarioAct["idpersona"]);
				$usuarioAct = NegSesion::getUsuario();				
			}
			$aplicacion->redir();
		} catch(Exception $e){			
			$aplicacion->redir();
		}
	}
	/*private function cambiarIdiomaProyecto()
	{
		// IDIOMA SEGUN PROYECTO 
		$usuarioAct = NegSesion::getUsuario();
		$proyecto = $this->oNegProyecto->buscar(array("idproyecto"=>$usuarioAct["idproyecto"]));
		if(empty($proyecto)){ return false; }
		$this->idioma = $proyecto[0]["idioma"];
		$documento = &JrInstancia::getDocumento();
        $documento->setIdioma($this->idioma);
        NegSesion::set('idioma', $this->idioma, 'idioma__');
	}*/
	protected function iniciarHistorialSesion($lugar)
	{
		$usuarioAct = NegSesion::getUsuario();
		$this->oNegHistorialSesion->tipousuario = ($usuarioAct['rol']=='Alumno')?'A':'P';
		$this->oNegHistorialSesion->idusuario = $usuarioAct['dni'];
		$this->oNegHistorialSesion->lugar = $lugar;
		$this->oNegHistorialSesion->fechaentrada = date('Y-m-d H:i:s');
		$this->oNegHistorialSesion->fechasalida = null;
		$idHistSesion = $this->oNegHistorialSesion->agregar();
		$sesion = JrSession::getInstancia();
		$sesion->set('idHistorialSesion', $idHistSesion, '__admin_m3c');
	}
	protected function terminarHistorialSesion($lugar)
	{
		$usuarioAct = NegSesion::getUsuario();
		$this->oNegHistorialSesion->idhistorialsesion = $usuarioAct['idHistorialSesion'];
		$this->oNegHistorialSesion->fechasalida = date('Y-m-d H:i:s');
		$resp = $this->oNegHistorialSesion->editar();
	}
	// ========================== Funciones xajax ========================== //
	public function xSolicitarCambioClave(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0]['usuario'])) { return;}
				JrCargador::clase('sys_negocio::NegUsuario', RUTA_BASE, 'sys_negocio');
				$oNegUsuario = new NegUsuario;			
				$this->admin = $oNegUsuario->procesarSolicitudCambioClave($args[0]['usuario']);
				if(!empty($this->admin)) {
					try {
						$isonline=NegTools::isonline();
						if(!empty($isonline)){						
							global $configSitio;
							JrCargador::clase('jrAdwen::JrCorreo');
							$oCorreo = new JrCorreo;
							$oCorreo->setRemitente('desarrollo@sistecapps.com', JrTexto::_('System management'));
							$oCorreo->setAsunto(JrTexto::_('Change password'));						
							$this->esquema = 'syscorreo-recuperar-clave';
							$oCorreo->setMensaje(parent::getEsquema());
							$oCorreo->addDestinarioPhpmailer($this->admin['email'], $this->admin['nombre']);						
							$oCorreo->sendPhpmailer();
							$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention'))
								, $this->pasarHtml(JrTexto::_('If the email is correct you will receive a message with a link to change your password'))
								, 'success');
						}else{
							$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention'))
								, $this->pasarHtml(JrTexto::_('This option is only active in online mode security'))
								, 'info');
						}
					} catch(Exception $e) {}
				}				
				
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'error');
			} 
		}
	}

	public function xCambiarRol(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try{
				global $aplicacion;
				$usu = NegSesion::getUsuario();
				if($usu["rol"]==$usu["rol_original"]){
					$x=NegSesion::set('rol','Alumno');
				}else{
					$y=NegSesion::set('rol',$usu["rol_original"]);
				}
				$oRespAjax->call('redir', $this->documento->getUrlBase());
			}catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'error');
			} 
		}
	}
}