<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Procesos batch a ejecutar  para hacer correcciones
 */
defined('RUTA_BASE') or die();
class WebBatch extends JrWeb
{

	public function __construct()
	{
		parent::__construct();		
	}

	public function defecto(){
		echo 'no hay proceso a ejecutar por defecto';
		exit(0);
	}

	public function rgenidactdet(){ //vuelve a generar id de actividad detalle 
		try{
			global $aplicacion;
			JrCargador::clase('sys_negocio::NegActividad_detalle', RUTA_BASE, 'sys_negocio');
			$oNegActiv = new NegActividad_detalle;
			$oNegActiv->setLimite(0,100000);
			$datos=$oNegActiv->buscar(array('ordenarpor'=>' iddetalle ASC, idactividad ASC'));
			$i=0;
			foreach ($datos as $dt){
				$i++;				
				$oNegActiv->setCampo($dt["iddetalle"],'iddetalle',$i);
				echo $dt["iddetalle"]." a ".$i."<br>";
			}
		}catch(Exception $e) {
			exit($e->getMessage());
		}
	}

	public function updatepeach(){ //corregir actividades 
		try{
			global $aplicacion;
			JrCargador::clase('sys_negocio::NegActividad_detalle', RUTA_BASE, 'sys_negocio');
			$oNegActiv = new NegActividad_detalle;
			$oNegActiv->setLimite(1400,100);
			$datos=$oNegActiv->buscar(array('ordenarpor'=>' iddetalle ASC, idactividad ASC'));
			$i=0;
			foreach ($datos as $dt){
				$textoedit=$dt["texto_edit"];
				$str=preg_replace('/<audio preload=\"auto\" src=\"__xRUTABASEx__\/(.*)static\/media\/record\/audio\//', '<audio preload="auto" src="__xRUTABASEx__/static/media/record/audio/', $textoedit);
				echo $str;
				$imod=$oNegActiv->setCampo($dt["iddetalle"],'texto_edit',$str);
				echo $imod." - ".$dt["iddetalle"]."<br>";
			}
		}catch(Exception $e) {
			exit($e->getMessage());
		}
	}
}