
<?php
	require_once("class/class_usuario.php");
	//if (session_status() == PHP_SESSION_NONE) {
	    @session_start();
	//}
	$obj = new Usuario();
?>

<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">
	<link rel="stylesheet" href="css/main.css">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	

    <link rel="stylesheet" href="css/login.css" />
    <link rel="stylesheet" href="plugins/magic/magic.css" />

</head>
    
<body style = 'background: #e9e9e9;'>

   <!-- PAGE CONTENT --> 
    <div class="container">

    	<div class = 'row'>
    		<div class = 'col-lg-4'></div>
    		<div class = 'col-lg-4'>
    			<h4 class = 'text-center'>
    				Logueo
    			</h4>
    		</div>
    		<div class = 'col-lg-4'></div>
    	</div>
   		<hr>
    	<form action = '#' method = 'post' class = 'form-horizontal'>
    		
	    	<div class = 'form-group'>
	    		<div class = 'col-lg-4'></div>
	    		<div class = 'col-lg-4'>
	    			<div class="input-group input-group-md">
						<input type="text" name = 'usuario' class="form-control" placeholder="Usuario">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>    
					</div>
	    		</div>
	    		<div class = 'col-lg-4'></div>
	    	</div>

	    	<div class = 'form-group'>
	    		<div class = 'col-lg-4'></div>
	    		<div class = 'col-lg-4'>
	    			<div class="input-group input-group-md">
						<input type="password" name = 'password' class="form-control" placeholder="Password">
						<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>    
					</div>
	    		</div>
	    		<div class = 'col-lg-4'></div>
	    	</div>

	 		<div class = 'form-group'>
	 			<div class = 'col-lg-4'></div>
	 			<div class = 'col-lg-4 text-center'>
	 				<input type = 'submit' class = 'btn btn-primary' value = 'Ingresar'>
	 			</div>
	 			<div class = 'col-lg-4'></div>
	 		</div>
			
    	</form>

	<?php
	if($_POST)
	{
		$res=$obj->logueo();

		if($res[0]["rol"]==1)
		{	
			$ses_us = $res[0]["dni"];
			$ses_cargo = $res[0]["rol"];
				
				/*if($res[0]["rol"]==1 && $_POST["tipo_cargo"]=="Administrador")
				{
					$ses_cargo1 = "Administrador";
				}
				else if($res[0]["rol"]==2 && $_POST["tipo_cargo"]=="Docente")
				{
					$ses_cargo1 = "Docente"; 
				}
				else if($res[0]["rol"]==3 && $_POST["tipo_cargo"]=="Supervisor")
				{
					$ses_cargo1 = "Supervisor";
				}*/

			$_SESSION["s_id_usu"]=$ses_us;
			$_SESSION["cargo"]=$ses_cargo1;
			$_SESSION["nombre"]=$res[0]["nombre"];

				echo '<meta http-equiv="refresh" content="0;url=lista_encuesta.php">';
			
		}
		/*else if($_POST["tipo_cargo"]=="Alumno")
		{
			$res1=$obj->logueo_usuario();
			if($res1)
			{
				session_start();
				$ses_us = $res1[0]["dni"];
				$_SESSION["id_usuario"]=$ses_us;
				$_SESSION["cargo"] = "Alumno";
				$_SESSION["nombre"]=$res1[0]["nombre"];

				echo '<meta http-equiv="refresh" content="0;url=lista_encuesta.php">';
			}
		}*/
		else
		{
			echo "<div class = 'row'>
					<div class = 'col-lg-12 text-center'>
						<br>
						<div class = 'alert alert-danger alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
								No está registrado

						</div>
					</div>
				</div>";
		}
	}
	?>


</body>

</html>

