<!doctype html>

<head>
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

	<!-- PAGE LEVEL TABLA STYLES -->
    <link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <script src ="plugins/dataTables/Spanish.js"></script>
    <!-- END PAGE LEVEL  STYLES -->

    <script src="js/pnotify.custom.min.js"></script>
  	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">
</head>

<style>
	input[type="radio"]{
		width: 2em;
		height: 2em;
	}
</style>

<body>

<div class = 'container-fluid'>

	<div class = 'row'>
		<div class = 'col-lg-12'>
			<?php
		        require_once("menu.php");
		    ?>
		</div>
	</div>

	<?php
	@session_start();
	require_once("class/class_ajustes.php");
	require_once("class/class_departamento.php");
	require_once("class/class_ugel.php");
	require_once("class/class_reporte.php");

	$obj = new Ajuste();
	$obj2=new Departamento();
	$obj3=new Ugel();
	$obj4=new Reporte();

if(!empty($_GET["id_rubrica"]))
{
	$rubrica = $obj->listar_idencuesta($_GET["id_rubrica"]);
	$cant_usuarios = $obj4->cantidad_usuarios_por_rubrica($_GET["id_rubrica"]);
	var_dump($cant_usuarios);
	$departamento = $obj2->listar_departamento();
	$ugel = $obj3->listar_ugel();
?>
	
	<div class = 'row' style="margin-top: 50px;">

		<div class = 'col-lg-1'></div>

		<div class = 'col-lg-10'>
			<div class = 'panel panel-primary'>

				<div class = 'panel-heading'>
					Reportes
				</div>

				<div class = 'panel-body'> 
					<input type = 'hidden' id = 'id_rubrica' value = '<?php echo $_GET["id_rubrica"];?>'>
					<div class = 'row'>
						<div class = 'col-lg-12 text-center'>
							<h4>Rúbrica: "<?php echo $rubrica[0]["titulo"];?>"</h4>
						</div>
					</div>
					<br>
					<div class = 'form-horizontal'>
						
						<div class = 'col-lg-3'>
							<div class = 'caja_select'>
								<select class = 'conestilo'>
									<option value = '0'>Departamento</option>
									<?php
										foreach($departamento as $dep)
										{
									?>
											<option value = '<?php echo $dep["iddepartamento"];?>'><?php echo $dep["nombre"];?></option>
											<?php
										}
									?>
								</select>
							</div>
						</div>
						
						<div class = 'col-lg-3'>
							<select class = 'form-control ugel'>
								<option value = '0'>Ugel</option>
							<?php
								foreach($ugel as $ug)
								{
									?>
									<option value = '<?php echo $ug["idugel"];?>'><?php echo $ug["descripcion"];?></option>
									<?php
								}
							?>
							</select>
						</div>
						
					</div>
					
					<div class = 'row'>
						<div class = 'col-lg-4'></div>
						<div class = 'col-lg-4 text-center'>
							<button class = 'btn btn-primary mostrar_reporte'>
								Mostrar reporte
							</button>
						</div>
						<div class = 'col-lg-4'></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class = 'col-lg-1'></div>
</div>
</div>

<script>

	$(".departamento").val(23);
	$(".selec_colegio").click(function(){
		var id_ugel = $(".ugel").val();
		if($(this).is(":checked")){
			$(".listado_ie").html("<img src = 'images/loader.gif' style = 'text-align:center;'>");
			$.ajax({
    			url: "grabar_ajax.php",
				type: "POST",
				dataType: "json",
				data: {
					"id_ugel": id_ugel,
					valor: "mostrar_ie"
				},
    		}).done(function(res){
	    		if(res.codigo=="OK"){
	    			$(".listado_ie").html(res.tabla);
	    		}
	    	}).fail(function(error){
	            console.log("no-muestra");
	        })
		}
	})

	$(".mostrar_reporte").click(function(){
		var id_rubrica = $("#id_rubrica").val();
		$.ajax({
    			url: "grabar_ajax.php",
				type: "POST",
				dataType: "json",
				data: {
					"id_rubrica": id_rubrica,
					valor: "mostrar_reporte_dimensiones"
				},
    		}).done(function(res){
	    		if(res.codigo=="OK"){
	    			$(".area_reporte").html(res.tabla);
	    		}
	    	}).fail(function(error){
	            console.log("no-muestra");
	        })
	})

	$(document).ready(function(){
		//$(".mostrar_reporte").trigger("click");
	});
</script>

<?php
}
else{
	?>
		<div class = 'row' style="margin-top: 60px;">
			<div class = 'col-lg-1'></div>
			<div class = 'col-lg-10'>
				<div class = 'panel panel-primary'>
					<div class = 'panel-heading'>
						AVISO
					</div>
					<div class = 'panel-body'>
						No hay rúbrica creada
					</div>
				</div>
				
			</div>
			<div class = 'col-lg-1'></div>
		</div>

	<?php

}

?>
</body>
</html>
