<?php
	session_start();

	require_once("class/class_mantenimiento.php");

	$obj = new Mantenimiento();

	if($_GET["id_rubrica"])
	{
		$get_rubrica = $_GET["id_rubrica"];
	}

	$res = $obj->listar_id_rubrica($get_rubrica);
?>

<!doctype html>

<head>	
	<meta charset="utf-8">
	<title>RÚBRICAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<script src="js/jquery-3.1.1.min.js"></script>

	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

	<!-- PAGE LEVEL TABLA STYLES -->
    <link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <script src ="plugins/dataTables/Spanish.js"></script>
    <!-- END PAGE LEVEL  STYLES -->

    <script src="js/pnotify.custom.min.js"></script>
  	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">

  	<!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="plugins/validationengine/css/validationEngine.jquery.css" />
	<!-- END PAGE LEVEL  STYLES -->

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">

	<script src = 'js/jquery.cokidoo-textarea.js'></script>
</head>

<body>

	<div class = 'container-fluid'>
		<div class = 'row'>
		    <div class = 'col-lg-12'>
			    <?php
			        require_once("menu2.php");
			    ?>
		    </div>
		</div>

		<div class = 'row' style="margin-top: 60px;">
			<div class = 'col-lg-4'>
				<br><button class = 'btn btn-danger nueva_dimension' data-toggle='modal' data-target='#myModal2'>NUEVA DIMENSIÓN</button>
			</div>
			<div class = 'col-lg-8'>
				<h3>Dimensiones de Rúbrica "<?php echo strtoupper($res[0]["nombre"]);?>"</h3>
			</div>
		</div>

		<?php //echo "Bienvenido: ".$_SESSION["usuario"]; ?>
		<div class = 'row'>
			
			<div class = 'col-lg-4'></div>
			<!--<div class = 'col-lg-4 text-center form-horizontal'>
				<select class = 'form-control selec_rubrica'>-->
					<?php
						/*$rubrica = $obj->listar_rubrica();
						foreach($rubrica as $valor)
						{
							echo "<option value = '".$valor["id_rubrica"]."'"; if(isset($get_rubrica) and $id_rubrica==$valor["id_rubrica"]){ echo "selected";} echo ">Año ".$valor["año"]."</option>";
						}*/
					?>
				<!--</select>
			</div>-->
			<div class = 'col-lg-4'></div>
		</div>
		<br>
		<div class = "row">
			<div class = 'col-lg-12'>
				<div class = 'table-responsive'>
					<table class = 'table table-striped table-condensed table-hover display' id='example1'>
						<thead>
							<tr style = 'background-color:#428bca; color: #fff;'>
								<th>N°</th>
								<th>Nombre Dimensión</th>
								<th>Ver Estándares</th>
								<th>Acción</th>
							</tr>
						</thead>
						<tbody class = 'llenado_dimensiones'>
							<?php
								
								$dimension = $obj->listar_dimensiones($get_rubrica);
								$cont=0;
								if(!empty($dimension))
								{
									foreach($dimension as $valor)
									{
										$cont++;
										echo "<tr id_dimension = '".$valor["id_dimension"]."'>
												<td>".$cont."</td>
												<td><div class = 'ver cambio_texto' valor_estatico='".$valor["nombre"]."' tecla_esc='no'>".$valor["nombre"]."</div></td>
												<td><button class = 'btn btn-success btn-circle mostrar_estandares' data-toggle='modal' data-target='#myModal'><i class = 'icon-search'></i></button></td>
												<td><button class = 'btn btn-danger btn-circle elim_dimension' data-toggle='modal' data-target='#notification_eliminar'><i class = 'icon-trash'></i></button></td>
											 </tr>";
									}
								}
								
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop='static' data-keyboard='false'>
	        <div class="modal-dialog modal-lg">
	            <div class="modal-content">
	               	<div id = 'ver_estandares'>
	               	</div>
		        </div>
	    	</div>
		</div>

		<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	        <div class="modal-dialog ">
	            <div class="modal-content">
	               	<div id = 'nueva_dimension'>
	               	</div>
		        </div>
	    	</div>
		</div>


	    <div class="modal" id="notificationModal_1" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	            	<div class="modal-dialog modal-lg">
	                	<div class="modal-content">
	                		<div id = 'mostrar_preguntas'>
	                		</div>
	                    </div>
	                </div>
	    </div>

	     <div class="modal" id="notification_eliminar" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	            	<div class="modal-dialog">
	                	<div class="modal-content">
	                		<div id = 'eliminar_dimension'>
	                		</div>
	                		<!--<div class = 'modal-body'>
	                			¿Está seguro que desea eliminar la dimensión seleccionada?
	                		</div>
	                		<div class = 'modal-footer'>
	                			<button class = 'btn btn-danger eliminar_dimension'>SI</button>
	                			<button class = 'btn btn-default' data-dismiss="modal" aria-hidden="true">NO</button>
	                		</div>-->
	                    </div>
	                </div>
	    </div>
	        

	</div><!--FIN CONTAINER-->

<script>

$("textarea").autoResize();

	$(".selec_rubrica").change(function(){
		var id_rubrica = $(this).val();

		$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_rubrica: id_rubrica,
				valor: "mostrar_dimensiones"
			}
	    })

	    .done(function(res){
	    	$(".llenado_dimensiones").html(res.tabla);
	    })

	    .fail(function(error){
            console.log("error-no-muestra-tabla");
		})
	})

	$(".container-fluid").on("click",".mostrar_estandares",function(){
		var id_dimension = $(this).parents("tr").attr("id_dimension");
		$.ajax({
                url: "ver_estandares.php",
                type: "POST",
                data: { id_dimension: id_dimension},
                success: function(result){
                     $("#ver_estandares").html(result);
                    }
                });
	});

	$(".container-fluid").on("click",".nueva_dimension",function(){
		var id_rubrica = $(".selec_rubrica").val();
		$.ajax({
                url: "nueva_dimension.php",
                type: "POST",
                data: { id_rubrica: id_rubrica},
                success: function(result){
                     $("#nueva_dimension").html(result);
                    }
                });
	});

	$(".container-fluid").on("click",".elim_dimension",function(){
		var id_dimension = $(this).parents("tr").attr("id_dimension");

		$("#eliminar_dimension").html("<div class = 'modal-body'>"+
	                					"¿Está seguro que desea eliminar la dimensión seleccionada?"+
	                				"</div>"+
	                				"<div class = 'modal-footer'>"+
	                					"<button class = 'btn btn-danger eliminar_dimension' id_dimension='"+id_dimension+"'>SI</button>"+
	                					"<button class = 'btn btn-default' data-dismiss='modal' aria-hidden='true'>NO</button>"+
	                				"</div>");
                  
	});

	$(".container-fluid").on("click",".eliminar_dimension",function(){
		var id_dimension = $(this).attr("id_dimension");

		$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_dimension: id_dimension,
				valor: "eliminar_dimension"
			}
	    })

	    .done(function(res){
			if(res.codigo=="OK")
			{
			    new PNotify({
					title: 'Mensaje',
					text: res.mensaje,
					delay: 1500

				});
					setTimeout (function(){
		                location.href='mantenimiento.php';
		            }, 1200);	
			}
		})

		.fail(function(error){
		 	console.log("error-no-elimina-dimension");
		})

	});
	
	$(".container-fluid").on("click",".cambio_texto",function(){
		var valor_estatico = $(this).attr("valor_estatico");
		$(this).html("<input type = 'text' style='width:100%;' class = 'form-control cambio' value = '"+valor_estatico+"' >");
		$(".cambio").focus();
		$(this).parents("#example1").find(".cambio_texto").removeClass("cambio_texto");
	});




	var con_cambio = function(obj,valor_nuevo,estilo){
    	$(obj).parents("table").find(".ver").addClass(estilo);
    	$(obj).parents(".ver").attr("valor_estatico",valor_nuevo);
		$(obj).parents(".ver").html(valor_nuevo);

    }

    var sin_cambio = function(obj,valor_estatico,estilo){
    	$(".cambio").parents("table").find(".ver").addClass(estilo);
		$(".cambio").parents(".ver").html(valor_estatico);
    }

	$(".container-fluid").on("blur",".cambio",function(e){
		
		var valor_estatico = $(this).parents(".ver").attr("valor_estatico");
		var valor_nuevo = $(this).val();
		var tecla_esc = $(this).parent(".ver").attr("tecla_esc");

		var id_dimension = $(this).parents("tr").attr("id_dimension");
		var obj = $(this);

		if(valor_nuevo.length>0 && valor_estatico != valor_nuevo && tecla_esc == "no")
		{

			$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				nombre: valor_nuevo,
				id_dimension: id_dimension,
				valor: "editar_dimension"
				}
		    })

		    .done(function(res){
				if(res.codigo=="OK")
				{
					con_cambio(obj,valor_nuevo,"cambio_texto");
					
					new PNotify({
						title: 'Mensaje',
						text: 'Dimensión modificada',
						delay: 1200

					});
				}
			})

		    .fail(function(error){
			 	console.log("error-no-edita-dimension");
			})
		}
		else if(valor_nuevo.length==0)
		{
			$(".cambio").replaceWith("<div class='form-group has-feedback' style = 'margin-bottom: 0px'><input type='text' class='form-control cambio' style = 'width:100%;' placeholder = 'Ingrese un nombre'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red'></i></div>");
			$(".cambio").addClass("borde_rojo");
		}
		else if(valor_estatico == valor_nuevo)
		{
			con_cambio(obj,valor_nuevo,"cambio_texto");
		}
		$(this).parent(".ver").attr("tecla_esc","no");	
	});

	$(".cambio").keypress(function(){
		$(this).removeClass("borde_rojo");
	});

	$("table").on("keypress",".cambio",function(e){ 		 
    	if(e.which == 13) {
    		$(this).parent(".ver").attr("tecla_esc","no");	
    		var padre = $(this).parents("tr");
    		var hermano = $(padre).find(".cambio");
		    $(hermano).trigger("blur"); 
		}
    });

    $("table").on("keyup",".cambio",function(e){
    	if(e.which == 27) {	
    		$(this).parents(".ver").attr("tecla_esc","si");
    		var valor_estatico = $(this).parents(".ver").attr("valor_estatico");
			var valor_nuevo = $(this).val();
			var obj = $(this);
    		sin_cambio(obj,valor_estatico,"cambio_texto");
		}
    });

    

	/****************************************************************/

		$(".container-fluid").on("blur",".cambio1",function(){

		var valor_estatico = $(this).parents(".ver").attr("valor_estatico");
		var valor_nuevo = $(this).val();
		var tecla_esc = $(this).parent(".ver").attr("tecla_esc");
		var id_estandar = $(this).parents("tr").attr("id_estandar");
		var obj = $(this);

		if(valor_nuevo.length>0 && valor_estatico != valor_nuevo && tecla_esc == "no")
		{
			$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				nombre: valor_nuevo,
				id_estandar: id_estandar,
				valor: "editar_estandar"
				}
		    })

		    .done(function(res){
				if(res.codigo=="OK")
				{
					con_cambio(obj,valor_nuevo,"cambio_texto1");
					
					new PNotify({
						title: 'Mensaje',
						text: 'Estándar modificado',
						delay: 1200

					});
				}
			})

		    .fail(function(error){
			 	console.log("error-no-edita-estandar");
			})
		}
		else if(valor_nuevo.length==0)
		{
			$(".cambio1").replaceWith("<div class='form-group has-feedback' style = 'margin-bottom: 0px'><textarea class='form-control cambio1' style = 'width:100%;' placeholder = 'Ingrese un nombre'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red'></i></textarea></div>");
			$(".cambio1").addClass("borde_rojo");
		}
		else if(valor_estatico == valor_nuevo)
		{
			con_cambio(obj,valor_nuevo,"cambio_texto1");
		}
		$(this).parent(".ver").attr("tecla_esc","no");		
	});

	$(".cambio1").keypress(function(){
		$(this).removeClass("borde_rojo");
	});

	$("table").on("keypress",".cambio1",function(e){ 		 
    	if(e.which == 13) {
    		$(this).parent(".ver").attr("tecla_esc","no");	
    		var padre = $(this).parents("tr");
    		var hermano = $(padre).find(".cambio");
		    $(hermano).trigger("blur"); 
		}
    });

    $("table").on("keyup",".cambio1",function(e){
    	if(e.which == 27) {	
    		$(this).parents(".ver").attr("tecla_esc","si");
    		var valor_estatico = $(this).parents(".ver").attr("valor_estatico");
			var valor_nuevo = $(this).val();
			var obj = $(this);
    		sin_cambio(obj,valor_estatico,"cambio_texto1");
		}
    });


</script>

<script src="plugins/validationengine/js/jquery.validationEngine.js"></script>
<script src="plugins/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
<script src="plugins/validationengine/js/languages/jquery.validationEngine-es.js"></script>

</body>

</html>