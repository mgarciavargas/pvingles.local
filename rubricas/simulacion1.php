<?php
	require_once("class/class_ajustes.php");
	require_once("class/class_evaluacion.php");
	require_once("class/class_mantenimiento.php");
	$obj1 = new Ajuste();
	$obj2 = new Evaluacion();
	$obj3 = new Mantenimiento();
	$ajuste = $obj1->listar_idencuesta($_GET["id_rubrica"]);
?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

	<!-- PAGE LEVEL TABLA STYLES -->
	<link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
	<script src ="plugins/dataTables/Spanish.js"></script>
	<!-- END PAGE LEVEL  STYLES -->

	<!-- PAGE LEVEL STYLES -->
	<link rel="stylesheet" href="plugins/validationengine/css/validationEngine.jquery.css" />
	<!-- END PAGE LEVEL  STYLES -->

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">

	<script src="js/pnotify.custom.min.js"></script>
	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">
</head>

<style>
	::-webkit-input-placeholder {
		font-style: italic;
	}
	:-moz-placeholder {
		font-style: italic;  
	}
	::-moz-placeholder {
		font-style: italic;  
	}
	:-ms-input-placeholder {  
		font-style: italic; 
	}

	table tr td
	{
		padding:5px;
	}

	input[type="radio"]{
		width: 2em;
		height: 2em;
	}

	.form-control[readonly] { 
		background-color: #fff;
	}
</style>

<body>
<br>
	<div class = 'container-fluid'>
	<div class = 'row'>
			<div class = 'col-lg-12'>
				<?php require_once("menu.php"); ?>
			</div>
		</div>

	<div class = 'row' style="margin-top: 60px;">
		<div class = 'col-lg-1'></div>

		<div class = 'col-lg-10'>
		<div class = 'panel panel-primary' id='panel_prim'>

			<div class = 'panel-heading'>
				<h4>Rúbrica: <?php echo $ajuste[0]["titulo"];?></h4>
			</div>
			<div class = 'panel-body'>
				<input type = 'hidden' class = 'tipo_encuesta' value = '<?php echo $ajuste[0]["tipo_encuestado"];?>'>
				<button class = 'ventana_modal' data-toggle="modal" data-target="#ventana_modal" style='display:none'>Ver datos</button>
				<input type = 'hidden' class = 'nombre_persona'>
				<input type = 'hidden' class = 'nombre_empresa'>
				<input type = 'hidden' id = 'id_rubrica' value = '<?php echo $_GET["id_rubrica"];?>'>

				<?php
					
					$encuesta=$obj2->listar_evaluacion($_GET["id_rubrica"]);
					//var_dump($encuesta);
					$cont1=0;
					$cant_estandar=0;
					$cont_preg = 0;

					if(!empty($encuesta))
					{
						$dimensiones = array();

						for($i=0;$i<sizeof($encuesta);$i++)
						{
							$cont1++;
							$tipo_pregunta = $encuesta[$i]["tipo_pregunta"];
							$x=json_decode($encuesta[$i]["otros_datos"],true);

							$cant_niveles = $x["niveles"];

							if(empty($dimensiones[$cant_niveles]))
								$dimensiones[$cant_niveles]=array($encuesta[$i]);
							else
							array_push($dimensiones[$cant_niveles],$encuesta[$i]);
							
							$cant_niv = $cant_niveles+1;

						}

						$cont_1=0;
						foreach($dimensiones as $dim)
						{
						?>
						<div class = 'table-responsive'>
							<table class='table table-striped' id='tabla_dimensiones' cellpadding="4">
							<?php
							foreach($dim as $dim1)
							{
								$cont_1++;
								if($dim1["tipo_pregunta"]=="PE")
								{
									$y=json_decode($dim1["otros_datos"],true);
									$cant_niv1=$y["niveles"];
									$cant_nive=$cant_niv1+2;

									$a=1;
						
									if($a==1 && !isset($b))
									{
										?>
										<tr class = 'cabecera'>
											<td colspan = '<?php echo $cant_nive;?>' class = 'text-center' style = 'background:#2196f3;color:#fff;border-right:2px solid #fff;'>
												<h4>DIMENSIONES</h4>
											</td>
											<?php
											
											?>
										</tr>

										<?php
									}
										$b=2;
										?>
										<tr style = 'background:#fb8c00;color:#fff;'>
											<td>
												<dt>DIMENSIÓN <?php echo $cont_1.".- ".$dim1["nombre"];?></dt>
											</td>
											<td align="right">
												
											</td>
											<?php
											for($i=0;$i<$cant_niv1;$i++)
											{
												$valor=$i+1;
												echo "<td align='center'><h4>".$valor."</h4></td>";
											}
											?>
										</tr>
									<?php
									$estandar = $obj3->listar_estandares($dim1["id_dimension"]);

									if(!empty($estandar))
									{
										foreach($estandar as $est)
										{
											$cant_estandar++;
											$cont_est=64+$cant_estandar;
											$letra = strtoupper(chr($cont_est));
											?>
											<tr>
												<td colspan = '<?php echo $cant_nive;?>' style = 'background:#e8e8e7;'>
													<dt><?php echo $letra.".- ".$est["nombre"];?></dt>
												</td>
											</tr>

											<?php
											$pregunta = $obj3->listar_preguntas($est["id_estandar"]);

											if(!empty($pregunta))
											{
												foreach($pregunta as $preg)
												{
													$cont_preg++;
													
													?>
													<tr class = 'fila_pregunta' tipo_pregunta = '<?php echo $dim1["tipo_pregunta"];?>' id_pregunta = '<?php echo $preg["id_pregunta"];?>' id_dimension='<?php echo $dim1["id_dimension"];?>'>
														<td style="background: #fcfce0;" colspan = '2'>
															<?php echo $cont_preg.".- ".$preg["nombre"];?>
														</td>
														<?php

														for($a=0;$a<$cant_niv1;$a++)
														{
															$j=$a+1;
															echo "<td style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center'>
																	<input type = 'radio' name = 'pregunta_".$cont_preg."' value = '".$j."'>
																</td>";
														}
														?>
													</tr>
													<?php
												}

											}
										}
									} //fin if empty estandar
								} //fin tipo pregunta pe

								else if($dim1["tipo_pregunta"]=="SN")
								{
									$cant_niv1=2;
									$cant_nive=4;
									$c=1;
							
									if($c==1 && !isset($d))
									{
										?>
										<tr class = 'cabecera'>
											<td colspan = '<?php echo $cant_nive;?>' class = 'text-center' style = 'background:#2196f3;color:#fff;border-right:2px solid #fff;'>
												<h4>SI-NO</h4>
											</td>
										</tr>

										<?php
									}
									$d=2;
							?>
							
										<tr style = 'background:#fb8c00;color:#fff;'>
											<td><dt>DIMENSIÓN <?php echo $cont_1.".- ".$dim1["nombre"];?></dt>
											</td>
											<td style="text-align:right;">
												
											</td>
											<td><h4>SI</h4></td>
											<td><h4>NO</h4></td>
										</tr>
										<?php
											$estandar = $obj3->listar_estandares($dim1["id_dimension"]);

											if(!empty($estandar))
											{
												foreach($estandar as $est)
												{
													$cant_estandar++;
													$cont_est=64+$cant_estandar;
													$letra = strtoupper(chr($cont_est));
													?>
													<tr>
														<td colspan = '<?php echo $cant_nive;?>' style = 'background:#e8e8e7;'>
															<dt><?php echo $letra.".- ".$est["nombre"];?></dt>
														</td>
													</tr>

													<?php
													$pregunta = $obj3->listar_preguntas($est["id_estandar"]);

													if(!empty($pregunta))
													{
														foreach($pregunta as $preg)
														{
															$cont_preg++;
															
															?>
															<tr class = 'fila_pregunta' tipo_pregunta = '<?php echo $dim1["tipo_pregunta"];?>' id_pregunta = '<?php echo $preg["id_pregunta"];?>' id_dimension='<?php echo $dim1["id_dimension"];?>'>
																<td style="background: #fcfce0;width:95%;" colspan="2">
																	<?php echo $cont_preg.".- ".$preg["nombre"];?>
																</td>
																<?php
																$var1=2;
																for($a=0;$a<$cant_niv1;$a++)
																{
																	$var1--;
																	echo "<td style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' >
																			<input type = 'radio' name = 'pregunta_".$cont_preg."' value='".$var1."'>
																		</td>";
																}
																?>
															</tr>
															<?php
														}
													}
												}
											} //fin empty estandar
								} //fin tipo pregunta SN

							} //fin foreach dimensiones as dim
						}
						echo "</table></div>";
					}
				?>
				<br>
				<?php

				
				$cont_3=$cont_preg;

				for($i=0;$i<sizeof($encuesta);$i++)
				{
					$cont_2++;
					
					$tipo_pregunta=$encuesta[$i]["tipo_pregunta"];

					if($tipo_pregunta=="A" || $tipo_pregunta=="C")
					{
						$cont_3++; 
						?>
						<div class = 'row item'>
							<div class = 'col-lg-12'>
								<div style = 'padding: 10px;background:#fb8c00;color:#fff;'>
    								<?php echo "DIMENSIÓN ".$cont_2.": ".$encuesta[$i]["nombre"];?>
    								<?php echo "<input type = 'hidden' name = 'pregunta_".$cont_3."' class = 'alt_marcado'>"; ?>
    								
    							</div>

    						<?php
	    						$estandar = $obj3->listar_estandares($encuesta[$i]["id_dimension"]);
    						?>
    							<div style = 'background:#e8e8e7;padding:5px;'>
    								<?php echo "ESTÁNDAR: ".$estandar[0]["nombre"];?>
    							</div>

							<?php 
								$pregunta=$obj3->listar_preguntas($estandar[0]["id_estandar"]);

    						?>
								<div style = 'background:#fcfce0;padding:5px;' class = 'fila_pregunta' tipo_pregunta = '<?php echo $tipo_pregunta;?>' id_pregunta = '<?php echo $pregunta[0]["id_pregunta"];?>' id_dimension='<?php echo $encuesta[$i]["id_dimension"];?>'>
									<dt><?php echo $pregunta[0]["nombre"];?></dt>
									<?php echo "<input type = 'hidden' name = 'puntaje_".$cont_3."' class = 'alt_puntaje'>"; ?>
								</div>
	    						<br>
    						</div>
					

							<div class = 'col-lg-12 fila_dimension'>
								<div class = 'well'>

							<?php
							if($tipo_pregunta=="A")
							{
								$x=json_decode($pregunta[0]["otros_datos"],true);

								echo "<ul style = 'list-style-type: none;'>
							";
								foreach($x["alternativas"] as $key=>$value)
								{
									if($key==$x["correcta"]){ $atributo = "true"; } 
									else {$atributo = "false";}

									echo "<li key = '".$key."' correcta = '".$atributo."' class = 'marcacion' style = 'cursor:pointer;'>
											<labelstyle = 'font-weight:normal;'>".$key.") </label> 
											<span>".$value."</span>
										</li>";
								}
								echo "</ul>";
							}
							if($tipo_pregunta=="C")
							{
								echo "<textarea class = 'form-control comp_pregunta' rows='5' name = 'pregunta_".$cont_3."'></textarea>";
							}
							
							?>
								</div>
							</div>
						</div>
						<?php
						
					}
				
				}

				?>
					<div class = 'row'>
						<div class = 'col-lg-12 text-right'>
							<a class = 'btn btn-primary' href = 'reporte1.php?id_rubrica=<?php echo $_GET["id_rubrica"];?>'>Enviar</a>
						</div>
					</div>
				</div>
			</div>


		</div>

	</div>
	<div class = 'col-lg-1'></div>
	</div>

	<div class = 'row aviso_enviado' style = 'display:none;'>
		<div class = 'col-lg-2'></div>
		<div class = 'col-lg-8'>
			<div class = 'panel panel-primary'>
				<div class = 'panel-header'>
				</div>
				<div class = 'panel-body text-center'>
					<p class = 'text-primary'>SUS RESPUESTAS HAN SIDO ENVIADAS</p>
					<h4>GRACIAS</h4>
				</div>
			</div>
		</div>
		<div class = 'col-lg-2'></div>
	</div>

	 <div class="modal" id="ventana_modal" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                       <div id = 'area_modal'>
                                       </div>
                                      
                                    </div>
                                </div>
                            </div>
<script>

	$(".ventana_modal").click(function(){
		$.ajax({
	        url: "form_datos.php",
	        type: "POST",
	        success: function(result){
	            $("#area_modal").html(result);
	        }
	    });
	});

	$(document).ready(function(){
		if($(".tipo_encuesta").val()=="P")
		{
			$(".ventana_modal").trigger("click");
			$("#panel_prim").css("display","none");
		}

	});

	$("body").on("click",".continuar",function(){
		var nombre_persona = $(".escriba_titulo").attr("nombre");
		var nombre_empresa = $(".escriba_descripcion").attr("nombre");

		$(".nombre_persona").val(nombre_persona);
		$(".nombre_empresa").val(nombre_empresa);

		$("#panel_prim").css("display","block");
	});

	var i=0;
	$(".fila_pregunta").each(function(){
		i++;
	});
	$(".cant_preguntas").val(i);

	$(".fila_dimension").on("click",".marcacion",function(){
		var valor_marcado = $(this).attr("key");

		$(this).parents(".well").find(".marcacion").removeClass("resaltado_amarillo");
		$(this).addClass("resaltado_amarillo"); 
		$(this).parents(".item").find(".alt_marcado").val(valor_marcado).attr("value",valor_marcado);

		if($(this).attr("correcta")=="true"){
			$(this).parents(".item").find(".alt_puntaje").val(1);
		}
		else
		{
			$(this).parents(".item").find(".alt_puntaje").val(0);
		}
	});


	$(".enviar").click(function(){
		var id_rubrica = $("#id_rubrica").val();
		var nombre = $(".nombre_persona").val();
		var institucion = $(".nombre_empresa").val();
		var a=0;
		var arreglo = {};
		new PNotify({
					title: 'Mensaje',
					text: 'Enviando respuestas',
					delay: 5000
				});

		$(".fila_pregunta").each(function(){
			a++;
			var id_dimension = $(this).attr("id_dimension");
			var id_pregunta = $(this).attr("id_pregunta");
			var tipo_pregunta = $(this).attr("tipo_pregunta");
			var puntaje = 0;
			var respuesta = "";
			//var elem = {}
			if(tipo_pregunta=="PE" || tipo_pregunta=="SN"){
				respuesta = $(this).find("input[type=radio][name=pregunta_"+a+"]:checked").val(); 
				puntaje = $(this).find("input[type=radio][name=pregunta_"+a+"]:checked").val();
			}
			else if(tipo_pregunta=="A"){
				respuesta = $(this).parents(".row").find(".alt_marcado").val();
				puntaje = $(this).find("input.alt_puntaje").val();
			}
			if(tipo_pregunta=="C"){
				respuesta=$(this).parents(".row").find(".comp_pregunta").val();
			}

			arreglo[id_pregunta]={
				"respuesta":respuesta,
				"puntaje_obtenido":puntaje,
				"id_dimension":id_dimension
			};
			
		});
		//console.log(arreglo);

		$.ajax({
			url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_rubrica: id_rubrica,
				nombre: nombre,
				institucion: institucion,
				arreglo: JSON.stringify(arreglo),
				valor: "grabar_examen"
			}
		}).done(function(res){
			if(res.codigo=="OK")
	    	{
				
				setTimeout (function(){
		            $("#panel_prim").css("display","none");
		            $(".aviso_enviado").css("display","block")
		        }, 1500);
		    }
		}).fail(function(error){
	        console.log("no-envia-respuestas");
	        console.log(error.responseText);
	    });
	})
	

</script>
</body>

</html>