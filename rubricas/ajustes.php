<?php @session_start(); 

require_once("class/class_ajustes.php"); 

$acc = "nuevo";
$ses_enc="";

$obj = new Ajuste();
$tipo_letra = $obj->listar_tipo_letra();

if(!empty($_GET["id_rubrica"]))
{
	$ses_enc = $_GET["id_rubrica"];
	$acc = "editado";
	$encuesta = $obj->listar_idencuesta($ses_enc);
}

?>

<!doctype html>

<head>
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

	<!-- PAGE LEVEL TABLA STYLES -->
    <link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <script src ="plugins/dataTables/Spanish.js"></script>
    <!-- END PAGE LEVEL  STYLES -->

    <script src="js/pnotify.custom.min.js"></script>
  	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">
</head>

<style>
	input[type="radio"]{
		width: 2em;
		height: 2em;
		margin-top: -5px;
	}
</style>

<body>

<div class = 'container-fluid'>

<?php
	if($_SESSION["s_id_usu"]){
?>
	<div class = 'row'>
		<div class = 'col-xs-12'>
			<?php
		        require_once("menu.php");
		    ?>
		</div>
	</div>

	<div class = 'row' style="margin-top: 60px;">

	<input type = 'hidden' class = 'accion' value = '<?php echo $acc;?>'>
	<div class = 'col-xs-1'></div>

	<div class = 'col-xs-10'>
		<div class = 'panel panel-primary'>

			<div class = 'panel-heading'>
				Configuración
			</div>

			<div class = 'panel-body'> 
				<div class = 'form-horizontal'> 
					<div class = 'form-group'> 
						<div class = 'col-xs-6'>
						<input type = "hidden" id="id_rubrica" value = "<?php echo $ses_enc;?>">
							<label for = 'titulo'>Título</label>
							<input type = 'text' class = 'form-control gris' id = 'titulo' name = 'titulo' placeholder="Agregar título" value = '<?php if($acc=="editado"){ echo $encuesta[0]["titulo"]; }?>'>
							<label for = 'titulo'>Descripción</label>
							<textarea name = 'descripcion' id='descripcion' rows = '6' class = 'form-control gris' placeholder="Agregar descripción" ><?php if($acc=="editado"){ echo $encuesta[0]["descripcion"]; } ?></textarea>
						</div>

						<div class = 'col-xs-6'>
							<div class = 'well' style="overflow: hidden;background-color: #fff; border: 1px solid #ddd">
								<div class = 'col-xs-6'>
									<label for='titulo'>
										Estado:
									</label>
									<div class = 'caja_select'>
									<?php 
										$estado = $encuesta[0]["activo"];

									?>
										<select name = 'estado' id = 'estado' class = 'conestilo'>
											<option value = '1' <?php echo ($acc=="editado" && $estado==1)?"selected":"";?> >Activo</option>
											<option value = '0' <?php echo ($acc=="editado" && $estado==0)?"selected":"";?> >Inactivo</option>
										</select>
									</div>
								</div>
								<?php
								if($acc=="editado" && !empty($encuesta[0]["foto"])){
									$ruta = $encuesta[0]["foto"];

								}
								else{
									$ruta = "images/nofoto.jpg";
								}
								?>
								<div class = 'col-xs-6 contenedor_imagen' style="position: relative;">
									<label for = 'titulo' class = 'text-left'>Foto:</label>
									<img src = '<?php echo $ruta;?>'  class = 'img-portada img-responsive img-thumbnails istooltip' width="170px" data-tipo="image" data-url=".img-portada" alt title data-original-title="Haga click aquí para agregar imagen">
									<input type = 'file' class = 'sub_imagen' src = 'images/nofoto.jpg' accept="image/*" style="position: absolute;left: 0;top:0;height: 100%;opacity: 0; cursor: pointer;">
								</div>
							</div>
						</div>
					</div>

					<div class = 'form-group'>
						<div class = 'col-xs-6'>
							<div class = 'well' style="overflow: hidden;background-color: #fff; border: 1px solid #ddd">
								
								<legend>Tipo de Encuestados</legend>
								<?php 

									$tipo = $encuesta[0]["tipo_encuestado"];
								?>

								<div class = 'col-xs-3'>
									<div class = 'radio'>
										
											<input type='radio' name='tipo_encuestado' value='A' class = 'tipo_encuestado' <?php  echo ($acc=="editado" && $tipo=="A")?"checked":""; ?> >
											<span style = 'margin-left: 15px'>An&oacute;nima</span>
											<br><br>
											<input type='radio' name='tipo_encuestado' value='P' class = 'tipo_encuestado' <?php echo ($acc=="editado" && $tipo=="P")?"checked":"";?>>
											<span style = 'margin-left: 15px'>P&uacute;blica</span>
										
									</div>
								</div>

								<?php
								$css="display:none;"; 
								if($acc=="editado")
								{
									$css="display:block;"; 
									$opcion_publico = $encuesta[0]["opcion_publico"];
									$tipo_registro = $encuesta[0]["registros"];
								}
								?>
								
								<div class = 'col-xs-9 tipo_publico' style="<?php echo $css;?>">
									<label class = 'control-label col-xs-5' style='text-align: left;'>
										Dirigido a:
									</label>
									<div class = 'col-xs-7'>
										<select class = 'form-control tipo_empresa'>
											<option value = 0>Seleccione</option>
											<option value = 'E' <?php if($acc=="editado" && $opcion_publico=="E"){ echo "selected";}?> >Empresas</option>
											<option value = 'I' <?php if($acc=="editado" && $opcion_publico=="I"){ echo "selected";}?>>Instituciones</option>
										</select>
									</div>
								</div>

								<div class = 'col-xs-9 cantidad_registros' style="<?php echo $css;?>">
									<label class = 'control-label col-xs-12' style='text-align: left;'>
										Cantidad de registros por temporada:
									</label>
									<div class = 'col-xs-12'>
										<label class = 'radio-inline'>
											<input type = 'radio' name = 'registro' class = 'registro' value = '1'
												<?php if($acc=="editado" && $tipo_registro=="1"){ echo "checked";}?>
											><span style = 'padding-left:15px;'>Uno</span>
										</label>
										<label class = 'radio-inline'>
											<input type = 'radio' name = 'registro' class = 'registro' value = '2'
												<?php if($acc=="editado" && $tipo_registro=="2"){ echo "checked";}?>
											><span style = 'padding-left:15px;'>Varios</span>
										</label>
									</div>
								</div>


							</div>
						</div>

						<div class = 'col-xs-6'>
							<div class = 'well' style="overflow: hidden;background-color: #fff; border: 1px solid #ddd">
								<legend>Autor</legend>
								<input type = 'text' class = 'form-control gris' id = 'autor' name = 'autor' placeholder="Agregar autor" id = 'autor' value = '<?php if($acc=="editado"){ echo $encuesta[0]["autor"]; }?>'>
							</div>
						</div>
					</div>
						

					<div class = 'form-group'>
						<div class = 'col-xs-12'>

						<div class = 'well' style="overflow: hidden;background-color: #fff; border: 1px solid #ddd">
							<fieldset>
							<legend>Formato de Rúbrica</legend>

							<div class = 'form-group'>

								<div class = 'col-xs-12'>
							
									<div class = 'row'>
										
										<div class = 'col-xs-4'>
											<label>Tipo de Letra</label>
											<div class = 'caja_select'>
												<select name = 'fuente' id = 'fuente' class = 'conestilo'>
													<?php
													for($i=0;$i<sizeof($tipo_letra);$i++)
													{
														$tipletra = $encuesta[0]["tipo_letra"];
													?>															
														<option value = <?php echo $tipo_letra[$i]["descripcion"];?> 
															<?php if($acc=="editado" && $tipletra == $tipo_letra[$i]["descripcion"]) { echo "selected";}?> >
															<?php echo $tipo_letra[$i]["descripcion"];?>
														</option>
													<?php
													}
													?>
												</select>
											</div>
										</div>

										<div class = 'col-xs-4'>
											<label>Tamaño de letra</label>
											<?php
												$tam_letra = $encuesta[0]["tamanio_letra"]; 
											?>

											<div class = 'caja_select'>
												<select name = 'fuentesize' id = 'fuentesize' class = 'conestilo'>
													<?php for($i=10;$i<30;$i++) { ?>
															<option value = <?php echo $i;?> 
																<?php
																	if($acc=="editado" && $tam_letra==$i){ echo "selected";} elseif($i==14) { echo "selected"; }
																?> >
															<?php echo $i;?>px
															</option>
													<?php } ?>
												</select>
											</div>
										</div>

										<div class = 'col-xs-4'>
											<label>Tipo de Rúbrica</label>
											<?php
												$tipo_rubrica = $encuesta[0]["tipo_rubrica"]; 
											?>
											<div class = 'caja_select'>
												<select name = 'tipo_rubrica' id = 'tipo_rubrica' class = 'conestilo'>
												
												<?php 
													$array_rubrica = array("Sólo preguntas","Estándares y preguntas","Dimensiones, estándares y preguntas");

													$a=0;
													for($i=0;$i<3;$i++){
														$a++;
												?>
													<option value = '<?php echo $a;?>' <?php if($acc=="editado" && $tipo_rubrica==$a){echo "selected";}?> >
														<?php echo $array_rubrica[$i];?>
													</option>
												<?php
													}
												?>
												</select>
											</div>
										</div>
									</div>

									<div class = 'row'>
										<div class = 'col-xs-4'>
										</div>
										<div class = 'col-xs-4 text-center'>
											<br>
											<?php
											if($acc=="editado")
											{
											?>
												<button class = 'btn btn-danger delete_encuesta'>
													<i class = 'icon-trash'></i>&nbsp;Eliminar
												</button>
											<?php
											}
											?>
											<button class = 'btn btn-primary save_encuesta'>
												<i class = 'icon-save'></i>&nbsp;Guardar y <br>agregar preguntas
											</button>
										</div>
										<div class = 'col-xs-4'>
										</div>
									</div>
								</div>
							</div>
							</fieldset>
						</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<?php
}
else
{
	echo "<div class = 'text-center'><br>No está logueado....<a href = 'index.php'>Regresar</a></div>";
}
?>

</div>	



<script>

	function subir(file){
		var from_data = new FormData();
		from_data.append("file",file[0].files[0]);
		from_data.append("valor","subir_imagen");
		$.ajax({
    		url: "subir_archivo.php",
			type: "POST",
			data: from_data,
            contentType: false,
      		processData: false,
      		dataType :'json',
      		cache: false,
      		beforeSend:function(){
      			//file.parent("div").append("<img src = 'images/loader.gif.' class = 'img-responsive img_arch'>");
      			$(".img-portada").attr("src",'images/loader.gif');
      		}
		
	    }).done(function(respuesta){
	    	if(respuesta.code=="ok")
	    	{
	    		nombre_archivo=respuesta.nombre_archivo;
	    		//file.parent("div").append("<img src = 'imagenes/"+nombre_archivo+"' class = 'img-responsive'>")
	    		//$(".img_arch").remove();
	    		$(".img-portada").attr("src",'imagenes/'+nombre_archivo);
	    	}
	    }).fail(function(b){console.log(b);});

	}

	$(".delete_encuesta").click(function(){
		var id_rubrica = $("#id_rubrica").val();
		$.ajax({
    			url: "grabar_ajax.php",
				type: "POST",
				dataType: "json",
				data: {
					id_rubrica: id_rubrica,
					valor: "eliminar_rubrica"
				},
    		}).done(function(res){
	    		if(res.codigo=="OK"){
	    			location.href='lista_encuesta.php';
	    		}
	    	}).fail(function(error){
	            console.log("no-elimina");
	        })
	})

	$(".sub_imagen").change(function(){
		
		var file = $(this);
		
		subir(file)
		
	});

	$("#cant_niveles").change(function(){
		var cant_niveles = $("#cant_niveles").val();
		if(cant_niveles>0){
			$.ajax({
    			url: "grabar_ajax.php",
				type: "POST",
				dataType: "json",
				data: {
					cant_niveles: cant_niveles,
					valor: "ver_puntuacion1"
				},
    		}).done(function(res){
	    		if(res.codigo=="OK"){
	    			location.href='evaluacion.php';
	    		}
	    	}).fail(function(error){
	            console.log("no-muestra");
	        })
		}
	});

	$(".save_encuesta").click(function(e){
		e.preventDefault();

		var dataAjax = {
			id_rubrica: $("#id_rubrica").val(),
			titulo: $("#titulo").val(),
			descripcion: $("#descripcion").val(),
			foto: $('.img-portada').attr('src'),
			tipo_empresa: $(".tipo_empresa").val(),
			fuente: $("#fuente").val(),
			fuente_size: $("#fuentesize").val(),
			estado: $("#estado").val(),
			tipo_encuestado: $("input.tipo_encuestado:checked").val(),
			registro: $("input.registro:checked").val(),
			autor: $("#autor").val(),
			tipo_rubrica: $("#tipo_rubrica").val(),
			valor: null,
		};

		var arreglo = {};
		if($(".accion").val()=="nuevo"){ dataAjax['valor'] = "grabar_encuesta";}
		else { dataAjax['valor'] = "actualizar_encuesta"; }
		
		$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: dataAjax,
	    }).done(function(res){
	    	if(res.codigo=="OK")
	    	{
	    		$("#id_encuesta").val("editado");
		    	new PNotify({
					title: 'Mensaje',
					text: 'Datos almacenados'
				});
				setTimeout (function(){
	                location.href='evaluacion.php?id_rubrica='+res.id_ultimo;
	            }, 1500);
	    	}
	    }).fail(function(error){
	        console.log("no-graba-encuesta");
	        console.log(error.responseText);
	    });

	});

	$(".tipo_encuestado").click(function(){ 
		var tipo_encuestado = $("input.tipo_encuestado:checked").val();
		
		if(tipo_encuestado=="P"){
			$(".tipo_publico").css("display","block");
			$(".cantidad_registros").css("display","block");
			$(".tipo_empresa").val(0);
		}
		else
		{
			$(".tipo_publico").css("display","none");
		}
	})
</script>

</body>

</html>