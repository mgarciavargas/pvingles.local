<div class = 'modal-header'>
	NUEVA RÚBRICA
</div>

<div class = 'modal-body'>
	<form class = "form-horizontal" id="block-validate">
		<div class = 'form-group'>
			<label class = 'control-label col-lg-3'>
				Año
			</label>
			<div class = 'col-lg-9'>
				<input type = 'text' class = 'form-control' id = 'anio' name = 'anio'>
			</div>
		</div>
		<div class = 'form-group'>
			<label class = 'control-label col-lg-3'>
				Nombre
			</label>
			<div class = 'col-lg-9'>
				<input type = 'text' id = 'nombre' name = 'nombre' class = 'form-control'>
			</div>
		</div>
		<div class = 'row text-center' id='aviso'>
		</div>

		<hr>
		<div class = "text-right">
			<button type="button" class="btn btn-default btn-close" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn1 submit1">Guardar</button>
		</div>
	</form>
</div>


<script>

	$(function () {

        $('#block-validate').validate({
		rules: {
			nombre:
			{
			    required: true
			},
			anio:
			{
			    required: true
			} 
		},
			errorClass: 'help-block',
			errorElement: 'span',
			highlight: function (element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-success').addClass('has-error');
		},
		    unhighlight: function (element, errorClass, validClass) {
		        $(element).parents('.form-group').removeClass('has-error');
		    },
		    submitHandler: function(){
		     
		    var nombre = $("#nombre").val();
		    var anio = $("#anio").val();
		        	
	    	
			$.ajax({
				url: "grabar_ajax.php",
				type: "POST",
				dataType: "json",
				data: {
					nombre: nombre,
					anio: anio,
					valor: "crear_rubrica"
				}
			})

			.done(function(a){
				if(a.codigo=="OK")
				{
					$("#aviso").css("display","block");
					$("#aviso").addClass("mensaje1").text(a.mensaje);	
					setTimeout (function(){
						$('.btn-close').trigger("click");
                       location.href='mantenimiento_1.php';
                    }, 1100);
				}
			})

			.fail(function(error){
	                console.log("error-no-crea-rubrica");
			})
				
			
        }

    });
});
</script>
</script>