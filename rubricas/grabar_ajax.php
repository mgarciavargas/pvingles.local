<?php
	require_once("class/class_mantenimiento.php");
	require_once("class/class_ajustes.php");
	require_once("class/class_evaluacion.php");
	require_once("class/class_reporte.php");
	require_once("class/class_respuestas.php");
	require_once("class/class_ie.php");
	require_once("class/class_provincia.php");
	require_once("class/class_distrito.php");
	require_once("class/class_ambiente.php");
	require_once("class/class_programacion.php");
	require_once("class/class_ugel.php");
 
	$mantenimiento = new Mantenimiento();
	$ajuste = new Ajuste();
	$evaluacion = new Evaluacion();
	$respuesta = new Respuestas();
	$reporte = new Reporte();
	$ie = new Ie();
	$prov = new Provincia();
	$dist = new Distrito();
	$ambiente = new Ambiente();
	$programacion = new Programacion();
	$ugel = new Ugel();

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="crear_rubrica")
	{
		try{
			$mantenimiento->crear_rubrica();
			echo json_encode(array('codigo'=>'OK', 'mensaje'=>'Rúbrica creada'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="editar_rubrica")
	{
		try{
			$mantenimiento->editar_rubrica($_POST["id_rubrica"]);
			echo json_encode(array('codigo'=>'OK', 'mensaje'=>'Rúbrica modificada'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}



	if(!empty($_POST["valor"]) and ($_POST["valor"])=="eliminar_rubrica")
	{
		try{
			$mantenimiento->eliminar_rubrica($_POST["id_rubrica"]);
			echo json_encode(array('codigo'=>'OK', 'mensaje'=>'Rúbrica eliminada'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="subir_imagen")
	{echo json_encode(array("code"=>"ok","mensaje"=>"Esta imagen se ha subido"));
		/*$fecha_actual = Date("Ymdhis");
		try{
			//if(move_uploaded_file($_FILES["file"]["tmp_name"],"imagenes/".$fecha_actual."_".$_FILES["file"]["name"])){
				
				exit(0);
			//}

		}
		catch(Exception $e){
			echo json_encode(array("code"=>"error","mensaje"=>$e->getMessage()]);
		}*/
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="mostrar_dimensiones"))
	{
		try{
			$res = $mantenimiento->listar_dimensiones($_POST["id_rubrica"]);
			if(!empty($res)){	
				$cont=0;
				foreach($res as $valor)
				{
					$cont++;
					$tabla .= 	"<tr id_dimension = '".$valor["id_dimension"]."'>
									<td>".$cont."</td>
									<td><div class = 'cambio_texto'>".$valor["nombre"]."</div></td>
									<td><button class = 'btn btn-success btn-circle mostrar_estandares' data-toggle='modal' data-target='#myModal'><i class = 'icon-search'></i></button></td>
									<td><button class = 'btn btn-danger btn-circle'><i class = 'icon-trash'></i></button></td>
								</tr>";
				}
			}
			else
			{
				$tabla = "";
			}
			echo json_encode(array('codigo'=>'OK','tabla'=>$tabla));

		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="listar_dimensiones_rubrica"))
	{
		try{
			$res=$mantenimiento->listar_dimensiones($_POST["id_rubrica"]);
			if(!empty($res))
			{
				foreach($res as $valor)
				{
					$select .= "<option value = '".$valor["id_dimension"]."'>".$valor["nombre"]."</option>";
				}
			}
			else
			{
				$select = "";
			}
			
			echo json_encode(array('codigo'=>'OK','select'=>$select));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="listar_estandares_dimension"))
	{
		try{
			$res=$mantenimiento->listar_estandares($_POST["id_dimension"]);
			if(!empty($res))
			{
				foreach($res as $valor)
				{
					$select .= "<option value = '".$valor["id_estandar"]."'>".$valor["nombre"]."</option>";
				}
			}
			else
			{
				$select = "";
			}
			
			echo json_encode(array('codigo'=>'OK','select'=>$select));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="listar_preguntas_estandar"))
	{
		try{
			$res=$mantenimiento->listar_preguntas($_POST["id_estandar"]);
			if(!empty($res))
			{
				$cont=0;
				foreach($res as $valor)
				{
					$cont++;
					$tabla.= "<tr>
								<td>".$cont."</td>
								<td align = 'left'>".$valor["nombre"]."</td>
								<td align = 'left'>".$valor["tipo_pregunta"]."</td>
								<td><button class = 'btn btn-danger btn-circle'>
										<i class = 'icon-trash'></i>
									</button></td>
							</tr>";
				}
			}
			else
			{
				$tabla = "";
			}
			
			echo json_encode(array('codigo'=>'OK','tabla'=>$tabla));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="comparar_nombre_dimension"))
	{
		try{
			$resultado = $mantenimiento->comparar_nombre_dimension($_POST["nombre"]);

			if($resultado[0]["cantidad"]>0)
			{
				echo json_encode(array('codigo'=>'no_registrar', 'mensaje'=>'Ya existe una dimensión con el mismo nombre'));
			}
			else
			{
				echo json_encode(array('codigo'=>'registrar'));
			}
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="crear_dimension")
	{
		try{
			$mantenimiento->crear_dimension($_POST["nombre"]);
			echo json_encode(array('codigo'=>'OK', 'mensaje'=>'Dimensión registrada'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="editar_dimension")
	{
		try{
			$evaluacion->editar_dimension();
			echo json_encode(array('codigo'=>'OK'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="crear_estandar")
	{
		try{
			$id_ultimo = $mantenimiento->crear_estandar($_POST["nombre"]);
			echo json_encode(array('codigo'=>'OK','id_ultimo'=>$id_ultimo));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="editar_estandar")
	{
		try{
			$evaluacion->editar_estandar();
			echo json_encode(array('codigo'=>'OK'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="eliminar_estandar")
	{
		try{
			$mantenimiento->eliminar_estandar($_POST["id_estandar"]);
			echo json_encode(array('codigo'=>'OK'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="eliminar_fisica_estandar")
	{
		try{
			$filtros=array(
				'id_estandar'=>$_POST["id_estandar"],
			);
			$mantenimiento->eliminar_fisica_estandar($filtros);
			/* se recomienda eliminar los respuestas de esta dimension*/
			echo json_encode(array('codigo'=>'OK'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="crear_pregunta")
	{
		try{
			$id_ultimo = $mantenimiento->crear_pregunta($_POST["nombre"]);
			echo json_encode(array('codigo'=>'OK','id_ultimo'=>$id_ultimo));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="editar_pregunta")
	{
		try{
			$evaluacion->editar_pregunta($_POST["id_pregunta"]);
			echo json_encode(array('codigo'=>'OK'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="eliminar_pregunta")
	{
		try{
			$mantenimiento->eliminar_pregunta($_POST["id_pregunta"]);
			echo json_encode(array('codigo'=>'OK'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="eliminar_fisica_pregunta")
	{
		try{
			$filtros=array(
				'id_pregunta'=>$_POST["id_pregunta"], 
			);
			$mantenimiento->eliminar_fisica_pregunta($filtros);
			echo json_encode(array('codigo'=>'OK'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

/********************AJUSTES*********************************/

if(!empty($_POST["valor"]) and ($_POST["valor"])=="ver_puntuacion1")
	{
		try{
			$cant_niveles = $_POST["cant_niveles"];
			$divisor = 100/$cant_niveles;
			$cuadro = "
			<table class = 'table table-striped table-bordered table-hover'>
					<thead>
						<tr style = 'background-color: #428bca;color:#fff;'>
							<th>Calificación</th>
							<th>Rango Inicial</th>
							<th>Rango Final</th>
						
						</tr>
					</thead>
					<tbody>";

			$cont = 0;
			$puntuacion_final = 0;
			for($i=0;$i<$cant_niveles;$i++)
			{
				$cont++;

				
					$valor_inicial = $puntuacion_final+1;

					$puntuacion_final = round($divisor*$cont);

				$cuadro.= "

				<tr valor_inicial = '".$valor_inicial."'  valor_final = '".$puntuacion_final."' class = 'fila'>
					<td><input type = 'text' class = 'representacion form-control gris'></td>
					<td>".$valor_inicial."%</td>
					<td>".$puntuacion_final."%</td>
					
				</tr>

				";
			}

			$cuadro .= "</tbody></table>";	

			echo json_encode(array('codigo'=>'OK','cuadro'=>$cuadro));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}


	if(!empty($_POST["valor"]) and ($_POST["valor"]=="grabar_encuesta"))
	{
		try{
			$id_ultimo=$ajuste->insertar_encuesta();
			echo json_encode(array('codigo'=>'OK','id_ultimo'=>$id_ultimo));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="actualizar_encuesta"))
	{
		try{
			$id_ultimo=$ajuste->actualizar_encuesta($_POST["id_rubrica"]);
			echo json_encode(array('codigo'=>'OK','id_ultimo'=>$id_ultimo));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}
/************************EVALUACIÓN************************************/

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="mostrar_dimensiones_anio"))
	{
		try{
			$res = $mantenimiento->listar_dimensiones($_POST["id_rubrica"]);
			if(!empty($res)){	
				$cont=0;

				$tabla.= "<table class='table table-striped' id = 'tabla_dim'>
				<thead>
					<tr>
						<td style = 'background:#fb8c00;color:#fff;'>N°</td>
						<td style = 'background:#2196f3;color:#fff;'>Dimensión</td>
					</tr>
				</thead><tbody>";
				foreach($res as $valor)
				{
					$cont++;
					$tabla .= 	"<tr id_dimension = '".$valor["id_dimension"]."'>
									<td width = '8%' style = 'background:#f5f9e6;font-weight:bold;color:#6a6b67;'>".$cont."</td>
									<td class = 'nombre_dimension' width = '92%' style = 'background:#e8e8e7;'>
										<span class = 'trasladar_dimension' style = 'color:#337ab7;font-weight:bold;cursor:pointer;'>".$valor["nombre"]."
										</span>

									</td>									
								</tr>";
				}
				$tabla.="</tbody></table>";
			}
			else
			{
				$tabla = "";
			}
			echo json_encode(array('codigo'=>'OK','tabla'=>$tabla));

		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="listar_dimension_tabla"))
	{
		try{
			$res=$mantenimiento->listar_estandares($_POST["id_dimension"]);
			$id_dimension = $_POST["id_dimension"];
			$cant_niveles = $_POST["cant_niveles"];
			$cant_preguntas = $_POST["cant_preguntas"];
			$cant_estandar = $_POST["cant_estandar"];
			$cant_dimension = $_POST["cant_dimension"];
			$cant_cabecera = $_POST["cant_cabecera"];

			$colspan = $cant_niveles+2;

			if(!empty($res))
			{
				if($cant_cabecera==0)
				{
					$tabla.="<tr class = 'cabecera'>
								<td width = '90%' colspan = '2' rowspan = '2' class = 'text-center' style = 'background:#2196f3;color:#fff;border-right:2px solid #fff;'><h4>DIMENSIONES</h4></td>
								<td width = '10%' colspan = '".$cant_niveles."' style = 'background:#2196f3;color:#fff;' class = 'fila_nivel text-center'><dt>NIVELES</dt></td>
							</tr>
							<tr class = 'por_nivel'>";

					for($i=1;$i<=$cant_niveles;$i++)
					{
						$tabla.= "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>".$i."</h5></td>";
					}
				}
				$cont=$cant_preguntas;
				$cant_dimension++;
				$let = 64+$cant_estandar;
				$tabla.= "
					</tr>
					<tr id_dimension='".$id_dimension."' style = 'padding: 10px;' class = 'fila_dimension'>
						<td colspan = '$colspan' class = 'td_dimension' style = 'background:#fb8c00;color:#fff;'>
							<div class = 'col-lg-9'>
								<dt style = 'text-align:left;display:inline;'>DIMENSIÓN ".$cant_dimension.":".strtoupper($_POST["nombre"])."</dt>
							</div>
							<div class = 'col-lg-3'>
								
							</div>
						</td>
						
					</tr>
				";

				foreach($res as $valor)
				{
					$let++;
					$letra = strtoupper(chr($let));

					$res1 = $mantenimiento->listar_preguntas($valor["id_estandar"]);

					$tabla .= "
								<tr id_estandar = '".$valor["id_estandar"]."' class = 'fila_estandar d_".$valor["id_dimension"]."'>
									<td class = 'td_estandar' colspan = '$colspan' style = 'background:#e8e8e7;'><dt>".$letra.") ".$valor["nombre"]."</dt></td></tr>
								";
					if(!empty($res1))
					{
						foreach($res1 as $pregunta)
						{
							$cont++;
							$tabla.= "<tr id_pregunta = '".$pregunta["id_pregunta"]."' class = 'fila_pregunta e_".$valor["id_estandar"]."'>
										<td style = 'background: #e0e8ef;'>".$cont.".-</td>
										<td style = 'background: #fcfce0;'>".$pregunta["nombre"]."</td>
										";
										for($i=1;$i<=$cant_niveles;$i++)
										{
											$tabla.= "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
										}
							$tabla.="
									</tr>";
						}
					}
					
				}
				//$tabla.="</tbody>";
			}

			echo json_encode(array('codigo'=>'OK','tabla'=>$tabla));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="grabar_alternativa"))
	{
		try{
			$id_dim = $evaluacion->grabar_dimension();
			$id_est = $evaluacion->grabar_estandar($id_dim);
			$id_preg = $evaluacion->grabar_pregunta($id_est);
			//$evaluacion->guardar_alternativa();
			echo json_encode(array('codigo'=>'OK'));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="grabar_autocompletar"))
	{
		try{
			$id_dim = $evaluacion->grabar_dimension();
			$id_est = $evaluacion->grabar_estandar($id_dim);
			$id_preg = $evaluacion->grabar_pregunta($id_est);
			//$evaluacion->guardar_autocompletar();
			echo json_encode(array('codigo'=>'OK'));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="grabar_dimension"))
	{
		try{
			$resp = $evaluacion->grabar_dimension();
			echo json_encode(array('codigo'=>'OK','data'=>$resp));
		}
		catch(Exception $e){
			echo json_encode(array('codigo'=>'Error','msg'=>$e->getMessage()));
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="grabar_dimension_bd")){
		try{
			$response=array();
			$response = $evaluacion->duplicar_dimension($_POST['id_dimension'], $_POST['id_rubrica']);
			$response['estandar']=array();
			$response['estandar']=$evaluacion->duplicar_estandar($_POST['id_dimension'], $response['id_dimension']);
			for ($i=0; $i < count($response['estandar']); $i++) { 
				$id_est_nueva = $response['estandar'][$i]['id_estandar'];
				$id_est_duplicar = $response['estandar'][$i]['duplicado_id_estandar'];
				$response['estandar'][$i]['pregunta']=array();
				$response['estandar'][$i]['pregunta']=$evaluacion->duplicar_pregunta($id_est_duplicar, $id_est_nueva);
			}
			echo json_encode(array('code'=>'OK','data'=>$response));
		}
		catch(Exception $e){
			echo json_encode(array('code'=>'Error','msg'=>$e->getMessage()));
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="actualizar_niveles_dimension"))
	{
		try{
			$arrDims=json_decode($_POST['arrIdDimensiones'], true);
			//var_dump($arrDims);
			foreach ($arrDims as $idDim) {
				$resp = $evaluacion->actualizar_niveles_dimension($idDim);
			}
			echo json_encode(array('code'=>'OK','data'=>$resp));
		}
		catch(Exception $e){
			echo json_encode(array('code'=>'Error','msg'=>$e->getMessage()));
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="eliminar_dimension"))
	{
		try{
			$resp = $evaluacion->eliminar_dimension($_POST['id_dimension']);
			echo json_encode(array('codigo'=>'OK','data'=>$resp));
		}
		catch(Exception $e){
			echo json_encode(array('codigo'=>'Error','msg'=>$e->getMessage()));
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="eliminar_fisica_dimension"))
	{
		try{
			$filtros=array(
				'id_dimension'=>$_POST['id_dimension']
			);
			$resp = $mantenimiento->eliminar_fisica_dimension($filtros);
			/* se recomienda eliminar los estandares y respuestas de esta dimension*/
			echo json_encode(array('codigo'=>'OK','data'=>$resp));
		}
		catch(Exception $e){
			echo json_encode(array('codigo'=>'Error','msg'=>$e->getMessage()));
		}
	}


	if(!empty($_POST["valor"]) and ($_POST["valor"]=="grabar_estandar"))
	{
		try{
			$resp = $evaluacion->grabar_estandar();
			echo json_encode(array('code'=>'OK','data'=>$resp));
		}
		catch(Exception $e){
			echo json_encode(array('code'=>'Error','msg'=>$e->getMessage()));
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="grabar_pregunta"))
	{
		try{
			$resp = $evaluacion->grabar_pregunta();
			echo json_encode(array('code'=>'OK','data'=>$resp));
		}
		catch(Exception $e){
			echo json_encode(array('code'=>'Error','msg'=>$e->getMessage()));
		}
	}



/****************RESPUESTAS********************************/

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="insertar_encuesta"))
	{
		try{
			$respuesta->insertar_encuesta();
			echo json_encode(array('codigo'=>'OK'));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

/**********************************************************/

/****************RESPUESTAS********************************/

if(!empty($_POST["valor"]) and ($_POST["valor"]=="grabar_puntuacion1"))
	{
		try{
			$ajuste->grabar_puntuacion1();
			echo json_encode(array('codigo'=>'OK'));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

if(!empty($_POST["valor"]) and ($_POST["valor"])=="grabar_examen")
	{
		try{
			$respuesta->grabar_examen();
			echo json_encode(array('codigo'=>'OK', 'mensaje'=>'Examen grabado'));
			//echo json_encode($resultado);
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

if(!empty($_POST["valor"]) and ($_POST["valor"])=="mostrar_resultado")
	{
	try{
		$reporte1=$reporte->listar_dimensiones_por_rubrica($_POST["id_rubrica"]);
		$cont=0;
		$tabla = "";

		foreach($reporte1 as $dim)
		{
			$dim1=$mantenimiento->listar_id_dimension($dim["id_dimension"]);

			if($dim1[0]["tipo_pregunta"]=="PE" || $dim1[0]["tipo_pregunta"]=="SN" || $dim1[0]["tipo_pregunta"]=="A")
			{
				$cont++;
			
			$cant_preg = $reporte->contar_preguntas_por_dimension($dim["id_dimension"]);

			$suma_total_por_dimension = $reporte->sumar_puntaje_obt_por_usuario($dim["id_dimension"],$_POST["id_rubrica"],$_POST["id_respuesta"]);
			$puntaje_maximo=0;
			$puntaje_promedio=0;
			$porcentaje_promedio=0;
			//$nombre_escala="";
			
			if($dim1[0]["tipo_pregunta"]=="PE")
			{
				$x=json_decode($dim1[0]["otros_datos"],true);
				$niv = $x["niveles"];
				$puntaje_maximo = (int)$cant_preg*(int)$niv;
			}

			if($dim1[0]["tipo_pregunta"]=="SN")
			{
				$puntaje_maximo = (int)$cant_preg;
			}

			if($dim1[0]["tipo_pregunta"]=="A")
			{
				$puntaje_maximo = 1;
			}
				
				
				$puntaje_promedio = (int)$suma_total_por_dimension[0]["suma"];
				$porcentaje_obtenido = round((int)$puntaje_promedio*100/(int)$puntaje_maximo);

				$cadena_puntuacion = json_decode($dim1[0]["escalas_evaluacion"],true);
				//var_dump($cadena_puntuacion);
				foreach($cadena_puntuacion as $key=>$value)
				{
					//if(value.rango_inicial<=porcentaje && porcentaje<=value.rango_final)
					if($value["rango_inicial"]<=$porcentaje_obtenido && $porcentaje_obtenido <= $value["rango_final"])
					{
						$nombre_escala = $key;
					}
				}
			
			
			
				$tabla.= "<div class = 'row'>
				
				<div class = 'col-lg-12'>
					<h4 style='font-size:110%;padding: 5px;color:#fff; background-color:#fb8c00'>
						DIMENSIÓN ".$cont.": ".$dim1[0]["nombre"]."
					</h4>

					<div class = 'table-responsive'>
						<table class = 'table table-striped table-condensed table-hover display'>
							<thead>
								<tr style = 'background-color:#428bca; color: #fff;'>
									<th>Puntaje Promedio Obtenido</th>
									<th>Porcentaje Promedio Obtenido</th>
									<th>Puntaje Máximo</th>
									<th>Escala</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>".$puntaje_promedio."</td>
									<td>".$porcentaje_obtenido."%</td>
									<td>".$puntaje_maximo."</td>
									<td>".$nombre_escala."</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>";	
		
			
			}
		}

			echo json_encode(array('codigo'=>'OK','tabla'=>$tabla));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/*****************EVALUACION MODE 2*********************************/

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="guardar_dimension")
	{
		try{
			$id_ultimo=$evaluacion->save_dimension();
			echo json_encode(array('codigo'=>'OK', 'id_ultimo'=>$id_ultimo));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"])=="actualizar_niveles")
	{
		try{
			$evaluacion->actualizar_niveles_dimension1();
			echo json_encode(array('codigo'=>'OK'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="guardar_estandar"))
	{
		try{
			$id_ultimo=$evaluacion->guardar_estandar();
			echo json_encode(array('codigo'=>'OK', 'id_ultimo'=>$id_ultimo));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="guardar_pregunta"))
	{
		try{
			$id_ultimo=$evaluacion->guardar_pregunta();
			echo json_encode(array('codigo'=>'OK', 'id_ultimo'=>$id_ultimo));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="elim_pregunta"))
	{
		try{
			$evaluacion->eliminar_pregunta();
			echo json_encode(array('codigo'=>'OK'));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="elim_estandar"))
	{
		try{
			$evaluacion->elim_estandar();
			echo json_encode(array('codigo'=>'OK'));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="elim_dimension"))
	{
		try{
			$evaluacion->elim_dimension();
			echo json_encode(array('codigo'=>'OK'));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="actualizar_plantilla"))
	{
		try{
			$evaluacion->act_plantilla();
			echo json_encode(array('codigo'=>'OK'));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="grabar_alternativa1"))
	{
		try{
			$evaluacion->act_pregunta_alternativas();
			echo json_encode(array('codigo'=>'OK'));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="mostrar_ie"))
	{
		try{
			$lista=$ie->listar_ie();

			$tabla = '';
			$cabecera = '';
			
			$tabla .= "<div style = 'overflow:scroll;height:300px;'><div class='table-responsive'>
			<table class = 'table table-striped table-condensed table-hover display' id='example3'>

				<thead>
					<tr style = 'background-color:#286090; color: #fff;'>
						<th>N°</th>
						<th>Nombre</th>";

				if(isset($_POST["edit"]) && ($_POST["edit"]=="sin_th")){
					$cabecera.='';
				}
				else if(!isset($_POST["edit"])){
					$cabecera.='<th>Prov.</th>
						<th>Dist.</th>
						<th>Direcc.</th>
						';
				}		
						
			$tabla.= $cabecera."<th>Elegir</th></tr>
				</thead>

				<tbody>";
					
						$cont = 0;

						for($i=0;$i<sizeof($lista);$i++)
						{
							$contenido = '';
							$id_registro = $lista[$i]["idlocal"];
                            $cont++;
					
							$tabla .='<tr style = "padding:0px;" id_registro = "<?php echo $id_registro;?>">
									<td>'.$cont.'</td>
									<td>'.$lista[$i]["nombre"].'</td>';
					
					if(!isset($_POST["edit"])){

						$provincia = $prov->listar_id_provincia($lista[$i]["idprovincia"]);
						$contenido.= '<td>'.$provincia[0]["nombre"].'</td>';
									
						if(empty($lista[$i]["iddistrito"]))
						{
							$distrito1 = "";
							$direccion = "";
						}
						else
						{ 
							$distrito = $dist->listar_id_distrito($lista[$i]["idprovincia"],$lista[$i]["iddistrito"]);
							
						}
						$distrito1 = $distrito[0]["nombre"];
						$direccion = $lista[$i]["direccion"];
							$contenido.=	'<td>'.$distrito1.'</td>
										<td>'.$direccion.'</td>
										';
					}				
								
							$tabla.=	$contenido.'
										<td>
											<input type = "radio" name = "selec_ie" class = "selec_ie" value = "'.$lista[$i]["idlocal"].",".$lista[$i]["nombre"].'">
										</td>
							</tr>';
						}
				
			$tabla .=	'</tbody>
			</table>
		</div></div>';

			echo json_encode(array('codigo'=>'OK','tabla'=>$tabla));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="mostrar_reporte_dimensiones"))
	{
		try{
			$tabla = '';
			$res=$reporte->listar_dimensiones_por_rubrica1($_POST["id_rubrica"]);
			$cont=0;

			foreach($res as $dim)
			{
				$dim1=$mantenimiento->listar_id_dimension($dim["id_dimension"]);

				if($dim1[0]["tipo_pregunta"]=="PE" || $dim1[0]["tipo_pregunta"]=="SN" || $dim1[0]["tipo_pregunta"]=="A")
				{
					$cont++;
				
				$cant_preg = $reporte->contar_preguntas_por_dimension($dim["id_dimension"]);
				$suma_total_por_dimension = $reporte->listar_preguntas_por_dimension_rubrica($dim["id_dimension"],$_POST["id_rubrica"]);
				$puntaje_maximo=0;
				$puntaje_promedio=0;
				$porcentaje_promedio=0;
				//$nombre_escala="";
				$cant_usuarios = $reporte->cantidad_usuarios_por_rubrica($_POST["id_rubrica"]);
				if($cant_usuarios>0){

					if($dim1[0]["tipo_pregunta"]=="PE")
					{
						$x=json_decode($dim1[0]["otros_datos"],true);
						$niv = $x["niveles"];
						$puntaje_maximo = (int)$cant_preg*(int)$niv;
					}

					if($dim1[0]["tipo_pregunta"]=="SN")
					{
						$puntaje_maximo = (int)$cant_preg;
					}

					if($dim1[0]["tipo_pregunta"]=="A")
					{
						$puntaje_maximo = 2;
					}
				}
				
					
					
					$puntaje_promedio = (int)$suma_total_por_dimension[0]["suma"]/(int)$cant_usuarios;
					$porcentaje_obtenido = round((int)$puntaje_promedio*100/(int)$puntaje_maximo);

					$cadena_puntuacion = json_decode($dim1[0]["escalas_evaluacion"],true);
					//var_dump($cadena_puntuacion);
					foreach($cadena_puntuacion as $key=>$value)
					{
						//if(value.rango_inicial<=porcentaje && porcentaje<=value.rango_final)
						if($value["rango_inicial"]<=$porcentaje_obtenido && $porcentaje_obtenido <= $value["rango_final"])
						{
							$nombre_escala = $key;
						}
					}
				
			$tabla.=	"<div class = 'row'>
					<div class = 'col-lg-2'></div>
					<div class = 'col-lg-8'>";
			$tabla.= '<h4 style="font-size:110%;padding: 5px;color:#fff; background-color:#fb8c00">
							DIMENSIÓN '.$cont.': "'.$dim1[0]["nombre"].'"
						</h4>';

			$tabla.=	"<div class = 'table-responsive'>
							<table class = 'table table-striped table-condensed table-hover display'>
								<thead>
									<tr style = 'background-color:#428bca; color: #fff;'>
										<th>Puntaje Promedio Obtenido</th>
										<th>Porcentaje Promedio Obtenido</th>
										<th>Puntaje Máximo</th>
										<th>Escala</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>".$puntaje_promedio."</td>
										<td>".$porcentaje_obtenido." %</td>
										<td>".$puntaje_maximo."</td>
										<td>".$nombre_escala."</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class = 'col-lg-2'></div>
				</div>";
				}
				
			}
					
					
			echo json_encode(array('codigo'=>'OK','tabla'=>$tabla));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="mostrar_ambiente"))
	{
		try{
			$res=$ambiente->listar_ambiente();
			echo json_encode(array('codigo'=>'OK','arr'=>$res));
		}

		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="mostrar_programacion"))
	{
		try{
			$res=$programacion->listar_programacion();
			echo json_encode(array('codigo'=>'OK','arr'=>$res));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	if(!empty($_POST["valor"]) and ($_POST["valor"]=="enviar_correo"))
	{
		$titulo = $_POST["titulo"];
		$link = $_POST["link"];
		$mensaje = $_POST["descripcion"]."<br><a href = 'http://".$link."'>Link de Rúbrica</a>";
		//$mensaje = "<html><body><a href = 'www.google.com'>www.google.com</a></body></html>";
		
		$cadena = $_POST["cadena"];

		$cad = explode(";",$cadena);



		try{
			
			// Para enviar un correo HTML, debe establecerse la cabecera Content-type
			$cabeceras  = 'MIME-Version: 1.0'."\r\n";
			$cabeceras .= 'Content-type: text/html; charset=utf-8'."\r\n";

			// Cabeceras adicionales
			$cabeceras .= 'To: <'.$cadena.'>'."\r\n";
			$cabeceras .= 'From: ESAN GLOBAL <esan_global@hotmail.com>'."\r\n";
			//$cabeceras .= 'Cc: birthdayarchive@example.com'."\r\n";
			//$cabeceras .= 'Bcc: birthdaycheck@example.com'."\r\n";

			// Enviarlo

			for($i=0;$i<sizeof($cad);$i++){

			mail($cad[$i], $titulo, $mensaje, $cabeceras);
		}
		
			echo json_encode(array('codigo'=>'OK'));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}


	if(!empty($_POST["valor"]) and ($_POST["valor"]=="listar_ugel")){
		try{
			$resultado=$ugel->listar_ugel();
			echo json_encode($resultado);
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

	}

?>	



