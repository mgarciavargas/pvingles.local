<?php
	require_once("conexion.php");

class C_Reporte2{
		
		private $generado = array();
		private $estandar = array();
		private $pregunta = array();
		private $evaluacion = array();
		private $departamento = array();
		private $registros = array();


		public function listar_evaluacion($id_encuesta)
	{
		$this->evaluacion = "";
		$sql = "SELECT * from rub_dimension where id_rubrica = ".$id_encuesta. " and activo = 1"; 
		$res = mysqli_query(Conectar::conex(),$sql);
		while($reg = mysqli_fetch_assoc($res))
		{
			$this->evaluacion[] = $reg;
		}		
		return $this->evaluacion;
	}

	public function listar_estandares($id_dimension)
		{
			$this->estandar="";
			$sql = "SELECT *from rub_estandar where id_dimension = ".$id_dimension." and activo = 1";  
			$res = mysqli_query(Conectar::conex(),$sql);
			while($reg = mysqli_fetch_assoc($res))
			{
				$this->estandar[] = $reg;
			}		
			return $this->estandar;
		}


		public function listar_preguntas($id_estandar)
		{
			$this->pregunta="";
			$sql = "SELECT *from rub_pregunta where id_estandar = ".$id_estandar; 
			
			$res = mysqli_query(Conectar::conex(),$sql);
			while($reg = mysqli_fetch_assoc($res))
			{
				$this->pregunta[] = $reg;
			}		
			return $this->pregunta;
		}


		public function listar_departamento()
		{
			$sql = "SELECT *FROM ubigeo where provincia='00' and distrito='00'";
			$res = mysqli_query(Conectar::conex(),$sql);
			while($reg = mysqli_fetch_assoc($res))
			{
				$this->departamento[] = $reg;
			}		
			return $this->departamento;
		}

		public function listar_ugel($filtros=null)
		{	
			
			try {

				$elementos = array();
				$sql = "SELECT * FROM ugel";
				$cond = array();

				if(!empty($_POST["iddep"])){
					$cond[] = "iddepartamento = ".$_POST["iddep"];
				}

				if(!empty($cond)) {
					$sql .= " WHERE ".implode(' AND ', $cond);
				}
				
				

				$res = mysqli_query(Conectar::conex(),$sql);
				while($reg = mysqli_fetch_assoc($res)) {
					$elementos[] = $reg;
				}
				return $elementos;

			} catch (Exception $e) {
				throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
			}

			
		}

		public function listar_ie($filtros=null){

			try {



				//$this->registros = null;
				$elementos = array();
				$sql = "SELECT * FROM local";
				$cond = array();

				if(!empty($_POST["idugel"])){
					$cond[] = "idugel = ".$_POST["idugel"];
				}

				if(!empty($cond)) {
					$sql .= " WHERE ".implode(' AND ', $cond);
				}
				
				

				$res = mysqli_query(Conectar::conex(),$sql);
				while($reg = mysqli_fetch_assoc($res)) {
					$elementos[] = $reg;
				}
				return $elementos;
			} catch (Exception $e) {
				throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
			}

		}

/*/////////////////////////////////////////////////*/
		public function autogenerado($consulta){

			$this->generado = null;
			$sql = $consulta;
			$res = mysqli_query(Conectar::conex(),$sql);
			
			while($reg = mysqli_fetch_assoc($res)){
				$this->generado[] = $reg;
				//$generado[] = $reg;
			}		
			//var_dump($sql);
			return $this->generado;
			//return $generado;
			//return $sql;

		}

/////////////////////////////////////////////////////////////////////////
		public function listar_ano($filtros=null){

			try {

				//$this->registros = null;
				$elementos = array();
				$sql = "SELECT YEAR(fecha_registro) as idano FROM rub_respuestas";
				$cond = array();

				if(!empty($_POST["idcolegio"])){
					$cond[] = "idlocal = ".$_POST["idcolegio"];
				}
				

				if(!empty($cond)) {
					$sql .= " WHERE ".implode(' AND ', $cond);
				}
				
					$sql .= " group by YEAR(fecha_registro) ";
				

				$res = mysqli_query(Conectar::conex(),$sql);
				while($reg = mysqli_fetch_assoc($res)) {
					$elementos[] = $reg;
				}
				return $elementos;
			} catch (Exception $e) {
				throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
			}

		}


/////////////////////////////////////////////////////////////////////////
		public function listar_nomrubricas($filtros=null){

			try {

				//$this->registros = null;
				$elementos = array();
				if ($_POST["docente"]=='ok'){

					$sql = "SELECT re.id_rubrica, ru.titulo
					FROM rub_respuestas re inner join rub_rubrica ru on ru.id_rubrica=re.id_rubrica
					where re.idlocal =".$_POST["idcolegio"]."
					and YEAR(re.fecha_registro) ='".$_POST["idano"]."'
					and ru.id_rubrica=38
					group by re.id_rubrica, ru.titulo
					order by ru.titulo asc";

				}else{

					$sql = "SELECT re.id_rubrica, ru.titulo
					FROM rub_respuestas re inner join rub_rubrica ru on ru.id_rubrica=re.id_rubrica
					where re.idlocal =".$_POST["idcolegio"]."
					and YEAR(re.fecha_registro) ='".$_POST["idano"]."'
					group by re.id_rubrica, ru.titulo
					order by ru.titulo asc";

				}
				
				
				$cond = array();
				

				$res = mysqli_query(Conectar::conex(),$sql);
				while($reg = mysqli_fetch_assoc($res)) {
					$elementos[] = $reg;
				}
				return $elementos;
				//return $sql;
			} catch (Exception $e) {
				throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
			}

		}


/////////////////////////////////////////////////////////////////////////
		public function listar_rubricas($filtros=null){

			try {

				//$this->registros = null;


				$cap_id=$this->autogenerado("select registros from rub_rubrica where id_rubrica=".$_POST['nomrubricas']);
				$registros = $cap_id[0]['registros'];

				$elementos = array();

				if ($registros==1){
				
					$sql = "SELECT re.id_respuestas, date(re.fecha_registro) as fecha_registro,ru.titulo
					FROM rub_respuestas re inner join rub_rubrica ru on ru.id_rubrica=re.id_rubrica
					where re.idlocal =".$_POST["idcolegio"]."
					and YEAR(re.fecha_registro) ='".$_POST["idano"]."'
					and ru.id_rubrica='".$_POST["nomrubricas"]."'
					order by re.fecha_registro desc";

				}else{

					$sql = "SELECT date(re.fecha_registro) as fecha_registro
					FROM rub_respuestas re inner join rub_rubrica ru on ru.id_rubrica=re.id_rubrica
					where re.idlocal =".$_POST["idcolegio"]."
					and YEAR(re.fecha_registro) ='".$_POST["idano"]."'
					and ru.id_rubrica='".$_POST["nomrubricas"]."'
					group by date(re.fecha_registro)";

				}
				
				$cond = array();
				

				$res = mysqli_query(Conectar::conex(),$sql);
				while($reg = mysqli_fetch_assoc($res)) {
					$elementos[] = $reg;
				}
				return $elementos;
				//return $sql;

			} catch (Exception $e) {
				throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
			}

		}

}
?>
