<?php
	require_once("class.php");
	
	class Mantenimiento
	{
		private $rubrica = array();
		private $rubrica1 = array();
		private $dimension = array();
		private $estandar = array();
		private $estandar1 = array();
		private $pregunta = array();
		private $pregunta1 = array();
		private $dimension1 = array();
		private $dimension2 = array();

		public function crear_rubrica()
		{
			$sql = "INSERT INTO rub_rubrica(nombre,anio,activo) VALUES ('".$_POST["nombre"]."','".$_POST["anio"]."',1)"; 
			$res = mysql_query($sql,Conectar::conex());
		}

		public function editar_rubrica($id_rubrica)
		{
			$sql = "UPDATE rub_rubrica set nombre = '".$_POST["nombre"]."' where id_rubrica = ".$id_rubrica;
			$res = mysql_query($sql,Conectar::conex());
		}

		public function eliminar_rubrica($id_rubrica)
		{
			$sql = "UPDATE rub_rubrica set activo = 0 where id_rubrica = ".$id_rubrica; 
			$res = mysql_query($sql,Conectar::conex());
		}

		public function listar_rubrica()
		{
			$sql = "SELECT *FROM rub_rubrica where activo = 1"; 
			$res = mysql_query($sql,Conectar::conex());

			while($reg = mysql_fetch_assoc($res))
			{
				$this->rubrica[] = $reg;
			}		
			return $this->rubrica;
		}

		public function listar_id_rubrica($id_rubrica)
		{
			$sql = "SELECT *from rub_rubrica where id_rubrica = ".$id_rubrica;
			$res = mysql_query($sql,Conectar::conex());

			while($reg = mysql_fetch_assoc($res))
			{
				$this->rubrica1[] = $reg;
			}		
			return $this->rubrica1;
		}

		public function listar_id_dimension($id_dimension)
		{
			$this->dimension2="";
			$sql = "SELECT *from rub_dimension where id_dimension = ".$id_dimension; 
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->dimension2[] = $reg;
			}		
			return $this->dimension2;
		}

		public function listar_dimensiones($id_rubrica=null)
		{
			$this->dimension="";
			$sql = "SELECT * FROM rub_dimension where activo = 1 AND tipo='N' AND (tipo_pregunta='PE' OR tipo_pregunta='SN')";
			if($id_rubrica!=null){
				$sql .= "AND id_rubrica = ".$id_rubrica;
			}
			$res = mysql_query($sql,Conectar::conex());

			while($reg = mysql_fetch_assoc($res))
			{
				$this->dimension[] = $reg;
			}		
			return $this->dimension;
		}

		public function eliminar_dimension($id_dimension)
		{
			$sql = "UPDATE rub_dimension SET activo = 0 where id_dimension = ".$id_dimension;
			$res = mysql_query($sql,Conectar::conex());
		}

		public function eliminar_fisica_dimension($filtros=array())
		{
			if(empty($filtros)) return false;
			$where = array();
			foreach ($filtros as $key => $value) {
				$where[]=" ".$key." = '".$value."'";
			}
			$sql = "DELETE FROM rub_dimension where ".implode(' AND ', $where);
			$res = mysql_query($sql,Conectar::conex());
		}

		public function listar_id_estandar($id_estandar)
		{
			$this->estandar1="";
			$sql = "SELECT *from rub_estandar where id_estandar = ".$id_estandar; 
			$res = mysql_query($sql,Conectar::conex());

			while($reg = mysql_fetch_assoc($res))
			{
				$this->estandar1[] = $reg;
			}		
			return $this->estandar1;
		}

		public function listar_estandares($id_dimension)
		{
			$this->estandar="";
			$sql = "SELECT *from rub_estandar where id_dimension = ".$id_dimension." and activo = 1";  
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->estandar[] = $reg;
			}		
			return $this->estandar;
		}

		public function crear_estandar($nombre)
		{
			$sql = "INSERT INTO rub_estandar(nombre,id_dimension,activo) VALUES ('".$_POST["nombre"]."','".$_POST["id_dimension"]."',1)"; //echo "sql: $sql";
			$res = mysql_query($sql,Conectar::conex());

			$id_ultimo = mysql_insert_id();
			return $id_ultimo;
		}

		public function editar_estandar($nombre)
		{
			$sql = "UPDATE rub_estandar set nombre = '".$nombre."' where id_estandar = ".$_POST["id_estandar"];
			$res = mysql_query($sql,Conectar::conex());
		}

		public function eliminar_estandar($id_estandar)
		{
			$sql = "UPDATE rub_estandar set activo = 0 where id_estandar = ".$id_estandar;
			$res = mysql_query($sql,Conectar::conex());
		}

		public function eliminar_fisica_estandar($filtros=array())
		{
			if(empty($filtros)) return false;
			$where = array();
			foreach ($filtros as $key => $value) {
				$where[]=" ".$key." = '".$value."'";
			}

			$sql = "DELETE FROM rub_estandar where ".implode(' AND ', $where);
			$res = mysql_query($sql,Conectar::conex());
		}

		public function listar_preguntas($id_estandar)
		{
			$this->pregunta="";
			$sql = "SELECT *from rub_pregunta where id_estandar = ".$id_estandar; 
			
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->pregunta[] = $reg;
			}		
			return $this->pregunta;
		}

		public function listar_id_pregunta($id_pregunta)
		{
			$this->pregunta1="";
			$sql = "SELECT *from rub_pregunta where id_pregunta = ".$id_pregunta; 
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->pregunta1[] = $reg;
			}		
			return $this->pregunta1;
		}

		public function comparar_nombre_dimension($nombre)
		{
			
			$sql = "SELECT count(*) as cantidad from rub_dimension where nombre = '".$nombre."' and id_rubrica = '".$_POST["id_rubrica"]."' and activo = 1";
			//echo $sql;
			$res = mysql_query($sql,Conectar::conex());

			while($reg = mysql_fetch_assoc($res))
			{
				$this->dimension1[] = $reg;
			}		
			return $this->dimension1;
		}

		public function crear_dimension($nombre)
		{
			$sql = "INSERT INTO rub_dimension(nombre,id_rubrica,activo) VALUES ('".$nombre."','".$_POST["id_rubrica"]."',1)";
			$res = mysql_query($sql,Conectar::conex());
		}

		public function editar_dimension($nombre)
		{
			$sql = "UPDATE rub_dimension set nombre = '".$nombre."'
					where id_dimension = '".$_POST["id_dimension"]."'"; 
			$res = mysql_query($sql,Conectar::conex());
		}

		public function crear_pregunta($nombre)
		{
			$sql = "INSERT INTO rub_pregunta(nombre,id_estandar,tipo_pregunta,activo) VALUES ('".$nombre."','".$_POST["id_estandar"]."','".$_POST["tipo_pregunta"]."',1)";
			$res = mysql_query($sql,Conectar::conex());

			$id_ultimo = mysql_insert_id();
			return $id_ultimo;
		}

		public function eliminar_pregunta($id_pregunta)
		{
			$sql = "UPDATE rub_pregunta set activo = 0 where id_pregunta = ".$id_pregunta;
			$res = mysql_query($sql,Conectar::conex());
		}

		public function eliminar_fisica_pregunta($filtros=array())
		{
			if(empty($filtros)) return false;
			$where = array();
			foreach ($filtros as $key => $value) {
				$where[]=" ".$key." = '".$value."'";
			}

			$sql = "DELETE FROM rub_pregunta WHERE ".implode(' AND ', $where);
			$res = mysql_query($sql,Conectar::conex());
		}

		


	}
?>