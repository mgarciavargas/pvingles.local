<?php
	require_once("class.php");

	class Reporte
	{
		private $reporte=array();
		private $reporte1=array();
		private $dimension = array();
		private $cantidad = array();
		private $cantidad1 = array();
		private $cantidad2 = array();
		private $personas = array();

		public function listar_resultado($id_encuesta)
		{
			$this->reporte="";
			$sql = "SELECT *from enc_respuestas where id_encuesta = ".$id_encuesta;
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->reporte[] = $reg;
			}		
			return $this->reporte;
		}

		public function listar_resultado_por_usuario($id_encuesta,$id_usuario)
		{
			$this->reporte1="";
			$sql = "SELECT *from enc_respuestas where id_encuesta = ".$id_encuesta." and id_usuario = ".$id_usuario;  //echo $sql;
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->reporte1[] = $reg;
			}		
			return $this->reporte1;
		}

		public function listar_dimensiones_por_rubrica($id_rubrica)
		{
			$this->dimension="";
			$sql = "SELECT *from detalle_respuestas dr
					INNER JOIN respuestas r ON dr.id_respuestas = r.id_respuestas
					where r.id_rubrica = $id_rubrica GROUP BY dr.id_dimension";
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->dimension[] = $reg;
			}		
			return $this->dimension;
		}

		public function listar_dimensiones_por_rubrica1($id_rubrica)
		{
			$this->dimension="";
			$sql = "SELECT *from dimension where id_rubrica = ".$id_rubrica." and activo = 1"; 
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->dimension[] = $reg;
			}		
			return $this->dimension;
		}

		public function contar_preguntas_por_dimension($id_dimension)
		{
			$this->cantidad="";
			$sql = "SELECT * from detalle_respuestas
					where id_dimension = ".$id_dimension." 
					GROUP BY id_pregunta";
					//echo $sql;
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->cantidad[] = $reg;
			}		
			return count($this->cantidad);
		}

		public function listar_preguntas_por_dimension_rubrica($id_dimension,$id_rubrica)
		{
			$this->cantidad1="";
			$sql = "SELECT SUM(dr.puntaje_obtenido) as suma from detalle_respuestas dr
					INNER JOIN respuestas r ON dr.id_respuestas = r.id_respuestas
					where r.id_rubrica = $id_rubrica and id_dimension = $id_dimension"; 
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->cantidad1[] = $reg;
			}		
			return $this->cantidad1;
		}

		public function cantidad_usuarios_por_rubrica($id_rubrica)
		{
			$this->cantidad2=array();
			$sql = "SELECT *from respuestas where id_rubrica = ".$id_rubrica." and activo = 1";

			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->cantidad2[] = $reg;
			}		
			return count($this->cantidad2);
		}

		public function listar_personas_por_rubrica($id_rubrica)
		{
			$this->personas="";
			$sql = "SELECT *from respuestas where id_rubrica = ".$id_rubrica." and activo = 1";
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->personas[] = $reg;
			}		
			return $this->personas;
		}

		public function sumar_puntaje_obt_por_usuario($id_dimension,$id_rubrica,$id_respuesta)
		{

			$this->cantidad1="";
			$sql = "SELECT SUM(dr.puntaje_obtenido) as suma 
					from detalle_respuestas dr
					INNER JOIN respuestas r ON dr.id_respuestas = r.id_respuestas
					where r.id_rubrica = $id_rubrica and dr.id_dimension = $id_dimension
					and dr.id_respuestas = ".$id_respuesta; 
			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res))
			{
				$this->cantidad1[] = $reg;
			}		
			return $this->cantidad1;
		}
	}
?>