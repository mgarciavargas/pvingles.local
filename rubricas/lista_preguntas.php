<?php
	require_once("class/class_mantenimiento.php");
	$id_estandar = $_POST["id_estandar"];
	$obj = new Mantenimiento();
	$estandar = $obj->listar_id_estandar($id_estandar);
	$preguntas = $obj->listar_preguntas($id_estandar);
	$tipo_preguntas = array('Ponderación Estándar','SI/NO','Alternativas','Autocompletar');	
?>
<div class = 'row'>
	<input type = 'hidden' id='id_estandar' value = '<?php echo $estandar[0]["id_estandar"];?>'>
	<div class = 'col-lg-12'>
		<div class = 'table-responsive'>
			<table class = 'table table-striped table-condensed table-hover display' id='tabla_preguntas'>
				<thead>
					<tr style = 'background-color:#5bc0de; color: #fff;'>
						<th width='5%'>N°</th>
						<th width='85%'>Pregunta</th>
					</tr>
				</thead>
				<tbody class = 'lista_preguntas'>
				<?php
				if(!empty($preguntas)) {
					$cont=0;
					foreach($preguntas as $valor) {
						$cont++;
				?>
					<tr id_pregunta="<?php echo $valor["id_pregunta"]; ?>">
						<td><?php echo $cont; ?></td>
						<td>
							<div class = 'ver' valor_estatico ="<?php echo $valor["nombre"]; ?>" tecla_esc='no'><?php echo $valor["nombre"]; ?></div>
						</td>
					</tr>
				<?php
					}
				} else { ?>
					<tr><td colspan="2">No se encontraron preguntas.</td></tr>
				<?php }?>
				</tbody>
			</table>
		</div>

	</div>
</div>

<script src = 'js/jquery.cokidoo-textarea.js'></script>
<script>
/*
$(".modal-footer").on("click",".agregar_pregunta",function(){
	var total_filas = parseInt($("#tabla_preguntas tbody tr").length) + 1;
	var arr_tipo_pregunta = ["Ponderación Estándar","SI/NO","Alternativas","Autocompletar"];
	var select='';
	for(var i=0;i<arr_tipo_pregunta.length;i++)
		{
			select += "<option value = '"+arr_tipo_pregunta[i]+"'>"+arr_tipo_pregunta[i]+"</option>";							
		}
	$("#tabla_preguntas").append("<tr class = 'pregunta_add'>"+
							"<td>"+total_filas+"</td>"+
							"<td><textarea class = 'form-control ultimo_valor'></textarea></td>"+
							"<td><select class = 'form-control tipo_pregunta'>"+
								"<option value ='0'>Seleccione</option>"+
							select+
							"</select></td>"+
							"<td>"+
								"<button class = 'btn btn-default btn-circle cancelar_pregunta'><i class = 'icon-remove'></i></button>"+
							"</td>"+
						  "</tr>");
	$(".ultimo_valor").focus();
	$(this).replaceWith("<button class = 'btn btn-primary guardar_pregunta'>Guardar</button>");
});

$("#tabla_preguntas").on("click",".cancelar_pregunta",function(){
	$(".pregunta_add").remove();
	$(".agregar_pregunta").removeAttr("disabled");
})

$(".modal-body").on("change",".tipo_pregunta",function(){
	$(this).removeClass("borde_rojo");
});

$(".modal-footer").on("click",".guardar_pregunta",function(){

	var nombre = $(".pregunta_add").find(".ultimo_valor").val();
	var id_estandar = $("#id_estandar").val();
	var tipo_pregunta = $(".pregunta_add").find(".tipo_pregunta").val();
	var total_filas = parseInt($("#tabla_preguntas tbody tr").length);
	

	if(tipo_pregunta == 0)
	{
		$(".pregunta_add").find(".tipo_pregunta").addClass("borde_rojo");
		var continuar="no";
	}
	else
	{
		var continuar = "ok";
	}

	if(nombre.length==0)
	{
		$(".ultimo_valor").replaceWith("<div class='form-group has-feedback' style = 'margin-bottom: 0px'><textarea class='form-control ultimo_valor' style = 'width:100%;' placeholder = 'Ingrese un nombre'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red'></i></textarea></div>");
		$(".ultimo_valor").addClass("borde_rojo");
	}

	else if(nombre.length>0 && continuar=="ok")
	{
		$.ajax({
		url: "grabar_ajax.php",
		type: "POST",
		dataType: "json",
		data: {
			nombre: nombre,
			id_estandar: id_estandar,
			tipo_pregunta: tipo_pregunta,
			valor: "crear_pregunta"
			}
	    })

	    .done(function(res){
	    	if(res.codigo=="OK")
	    	{
	    		var arr_tipo_pregunta = ["Ponderación Estándar","SI/NO","Alternativas","Autocompletar"];
				var select='';
				for(var i=0;i<arr_tipo_pregunta.length;i++)
				{
					if(tipo_pregunta==arr_tipo_pregunta[i]){ var selected = "selected";} else { var selected = "";}
					select += "<option value = '"+arr_tipo_pregunta[i]+"' "+selected+">"+arr_tipo_pregunta[i]+"</option>";	
				}

	    		$(".pregunta_add").html("<td>"+total_filas+"</td>"+
										"<td><div class = 'ver cambio_texto2' tecla_esc = 'no' valor_estatico='"+nombre+"'>"+nombre+"</div>"+
										"</td>"+
										"<td><select class = 'form-control tipo_pregunta'>"+
												"<option value ='0'>Seleccione</option>"+
													select+
											"</select>"+
										"</td>"+
										"<td>"+
											"<button class = 'btn btn-danger btn-circle eliminar_pregunta'><i class = 'icon-trash'></i></button>"+
										"</td>");
				$(".pregunta_add").attr("id_pregunta",res.id_ultimo).removeClass("pregunta_add");
				$(".guardar_pregunta").replaceWith("<button class = 'btn btn-success agregar_pregunta'>Agregar</button>");
	    	}
	    })

	    .fail(function(error){
		 	console.log("error-no-crea-pregunta");
		})
	}
})

$(".modal-body").on("click",".eliminar_pregunta",function(){
	var id_pregunta = $(this).parents("tr").attr("id_pregunta");
	var $this = $(this);

	$.ajax({
		url: "grabar_ajax.php",
		type: "POST",
		dataType: "json",
		data: {
			id_pregunta: id_pregunta,
			valor: "eliminar_pregunta"
			}
	    })

	.done(function(res){
		if(res.codigo=="OK")
		{
			$this.parents("tr").remove();

			var i = 1;
			$("#tabla_preguntas").find("tbody tr").each(function(){
				$(this).find("td:first").text(i);
				i++;	
			})

			new PNotify({
					title: 'Mensaje',
					text: 'Pregunta eliminada',
					delay: 1200

				});
		}
	})

	.fail(function(error){
        console.log("error-no-elimina-pregunta");
    })
});*/
</script>
