<?php
	require_once("class/class_mantenimiento.php");
	$obj = new Mantenimiento();
	$rubrica = $obj->listar_rubrica();

?>

<!doctype html>

<head>
	<script src="js/jquery-3.1.1.min.js"></script>
	<link rel="stylesheet" href="css/main.css">
</head>

<style>
	.box.primary header {
    color: #fff;
    background-repeat: repeat-x;
    border-bottom: 1px solid #1c82cc;
    background-image: linear-gradient(to bottom, #1c82cc 0%, #1c82cc 100%)
</style>

<body>

<div class = 'row'>
	<div class = 'col-lg-12' style="padding:0">
		<div class = 'box primary' style="margin-top: 0;">
			<header style = 'background-color: #1c82cc;'>
				<h5>Elegir Dimensión</h5>
				<button class="btn btn-xs btn-danger close-box pull-right cerrar_panel"><i class="icon-remove"></i></button>
			</header>

			<body>
			<br>
				<div class = 'form-horizontal'>
					<div class = 'form-group'>
						<div class = 'col-lg-12 text-center'>
							<span>
								Seleccione dimensiones de Rúbrica
							</span>
						</div>
					</div>
					<div class = 'form-group'>
						<div class = 'col-lg-2'></div>
						<div class = 'col-lg-8'>
							<select class = 'form-control anio_rubrica'>
								<option value = '0'>Seleccione</option>
								<?php
								for($i=0;$i<sizeof($rubrica);$i++)
								{
									echo "<option value = '".$rubrica[$i]["id_rubrica"]."'>".$rubrica[$i]["nombre"]."</option>";
								}
								?>
							</select>
						</div>
						<div class = 'col-lg-2'></div>
					</div>
					<br>
					<div class = 'row'>
						<div class = 'col-lg-12'>
							<div id = 'lista_dimensiones'>
							</div>
						</div>
					</div>
				</div>

			</body>
		</div>
	</div>
</div>

<script>

	$(".cerrar_panel").click(function(){
		$(".aside").hide();
	});

	$(".anio_rubrica").change(function(){
		var id_rubrica = $(".anio_rubrica").val();
		$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_rubrica: id_rubrica,
				valor: "mostrar_dimensiones_anio"
				}
	    	})

		.done(function(res){
	    	if(res.codigo=="OK")
	    	{
	    		$("#lista_dimensiones").html(res.tabla);
	    	}
	    })

	    .fail(function(error){
	        console.log("no-muestra-dimensiones");
	    })
	})

	$(".form-horizontal").on("click",".trasladar_dimension",function(){
		var id_dimension = $(this).parents("tr").attr("id_dimension");
		var nombre = $(this).parents("tr").find(".nombre_dimension").text();
		var cant_niveles = $(".cant_niveles").val();
		var cant_preguntas = parseInt($("#tabla_dimensiones tr.fila_pregunta").length);
		var cant_estandar = parseInt($("#tabla_dimensiones tr.fila_estandar").length);
		var cant_dimension = parseInt($("#tabla_dimensiones tr.fila_dimension").length);
		var cant_cabecera = parseInt($("#tabla_dimensiones tr.cabecera").length);

		$(this).parents("tr").remove();

		$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_dimension: id_dimension,
				nombre: nombre,
				cant_niveles: cant_niveles,
				cant_preguntas: cant_preguntas,
				cant_estandar: cant_estandar,
				cant_dimension: cant_dimension,
				cant_cabecera: cant_cabecera,
				valor: "listar_dimension_tabla"
				}
	    	})

		.done(function(res){
			$("#tabla_dimensiones").append(res.tabla);
			
			var i = 1;
    			$("#tabla_dim").find("tbody tr").each(function(){
    				$(this).find("td:first").text(i);
    				i++;	
    			})
		
		})

		.fail(function(error){
	         console.log("no-muestra-dimensiones");
	    })
	});

</script>

</body>

</html>