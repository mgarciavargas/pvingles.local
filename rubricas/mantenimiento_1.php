<?php
	//if (session_status() == PHP_SESSION_NONE) {
      @session_start();
    //}

	require_once("class/class_mantenimiento.php");

	$obj = new Mantenimiento();

	if($_GET["id_rubrica"])
	{
		$get_rubrica = $_GET["id_rubrica"];
	}

	$res=$obj->listar_rubrica();
?>

<!doctype html>

<head>	
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<script src="js/jquery-3.1.1.min.js"></script>

	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

	<!-- PAGE LEVEL TABLA STYLES -->
    <link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <script src ="plugins/dataTables/Spanish.js"></script>
    <!-- END PAGE LEVEL  STYLES -->

    <script src="js/pnotify.custom.min.js"></script>
  	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">

  	<!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="plugins/validationengine/css/validationEngine.jquery.css" />
	<!-- END PAGE LEVEL  STYLES -->

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">

	<script src = 'js/jquery.cokidoo-textarea.js'></script>
</head>

<body>

<div class = 'container-fluid'>
	<div class = 'row'>
	    <div class = 'col-lg-12'>
		    <?php
		        require_once("menu2.php");
		    ?>
	    </div>
	</div>
	<div class = 'row' style="margin-top: 60px;">
		<div class = 'col-lg-4'>
			<button class = 'btn btn-danger nueva_rubrica' data-toggle='modal' data-target='#myModal1'>NUEVA RÚBRICA</button>
		</div>
		<div class = 'col-lg-4'><h3>Rúbricas</h3></div>
		<div class = 'col-lg-4'></div>
	</div>
	<br>
	<div class = 'row'>
		<div class = 'col-lg-2'></div>
		<div class = 'col-lg-8'>
			<div class = 'table-responsive'>
				<table class = 'table table-striped table-condensed table-hover display' id='example1'>
					<thead>
						<tr style = 'background-color:#428bca; color: #fff;'>
							<th>N°</th>
							<th>Nombre</th>
							<th>Año</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody class = 'llenado_dimensiones'>
						<?php
							if(!empty($res))
							{
								$cont=0;
								foreach($res as $valor)
								{
									$cont++;
									echo "<tr id_rubrica=".$valor["id_rubrica"].">
										<td>".$cont."</td>
										<td><div class = 'ver cambio_texto' valor_estatico='".$valor["nombre"]."' tecla_esc='no'>".$valor["nombre"]."</div></td>
										<td>".$valor["anio"]."</td>
										<td>
											<a href = 'mantenimiento.php?id_rubrica=".$valor["id_rubrica"]."' class = 'btn btn-primary btn-circle'><i class = 'icon-search'></i></a>
											<button class = 'btn btn-danger btn-circle elim_rubrica' data-toggle='modal' data-target='#notification_eliminar'><i class = 'icon-trash'></i></button>
										</td>
									</tr>";
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<div class = 'col-lg-2'></div>
	</div>

	
</div>

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div id = 'nuevo_rubrica'>
            </div>
	    </div>
    </div>
</div>

 <div class="modal" id="notification_eliminar" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
    		<div id = 'eliminar_rubrica'>
    		</div>
    	
        </div>
    </div>
</div>

<script>

$(".nueva_rubrica").click(function(){
	$.ajax({
        url: "nuevo_rubrica.php",
        type: "POST",
        success: function(result){
            $("#nuevo_rubrica").html(result);
        }
    });
})

$(".container-fluid").on("click",".elim_rubrica",function(){
	var id_rubrica = $(this).parents("tr").attr("id_rubrica");

	$("#eliminar_rubrica").html("<div class = 'modal-body'>"+
                					"¿Está seguro que desea eliminar la rúbrica seleccionada?"+
                				"</div>"+
                				"<div class = 'modal-footer'>"+
                					"<button class = 'btn btn-danger eliminar_rubrica' id_rubrica='"+id_rubrica+"'>SI</button>"+
                					"<button class = 'btn btn-default' data-dismiss='modal' aria-hidden='true'>NO</button>"+
                				"</div>");
              
});

$("body").on("click",".eliminar_rubrica",function(){

	var id_rubrica = $(this).attr("id_rubrica");
	$.ajax({
		url: "grabar_ajax.php",
		type: "POST",
		dataType: "json",
		data: {
			id_rubrica: id_rubrica,
			valor: "eliminar_rubrica"
		}
    })

    .done(function(res){
		if(res.codigo=="OK")
		{
		    new PNotify({
				title: 'Mensaje',
				text: res.mensaje,
				delay: 1500

			});
				setTimeout (function(){
	                location.href='mantenimiento_1.php';
	            }, 1200);	
		}
	})

	.fail(function(error){
	 	console.log("error-no-elimina-rubrica");
	})

	


});

$(".container-fluid").on("click",".cambio_texto",function(){
		var valor_estatico = $(this).attr("valor_estatico");
		$(this).html("<input type = 'text' style='width:100%;' class = 'form-control cambio' value = '"+valor_estatico+"' >");
		$(".cambio").focus();
		$(this).parents("#example1").find(".cambio_texto").removeClass("cambio_texto");
	});




	var con_cambio = function(obj,valor_nuevo,estilo){
    	$(obj).parents("table").find(".ver").addClass(estilo);
    	$(obj).parents(".ver").attr("valor_estatico",valor_nuevo);
		$(obj).parents(".ver").html(valor_nuevo);

    }

    var sin_cambio = function(obj,valor_estatico,estilo){
    	$(".cambio").parents("table").find(".ver").addClass(estilo);
		$(".cambio").parents(".ver").html(valor_estatico);
    }

	$(".container-fluid").on("blur",".cambio",function(e){
		
		var valor_estatico = $(this).parents(".ver").attr("valor_estatico");
		var valor_nuevo = $(this).val();
		var tecla_esc = $(this).parent(".ver").attr("tecla_esc");

		var id_rubrica = $(this).parents("tr").attr("id_rubrica");
		var obj = $(this);

		if(valor_nuevo.length>0 && valor_estatico != valor_nuevo && tecla_esc == "no")
		{

			$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				nombre: valor_nuevo,
				id_rubrica: id_rubrica,
				valor: "editar_rubrica"
				}
		    })

		    .done(function(res){
				if(res.codigo=="OK")
				{
					con_cambio(obj,valor_nuevo,"cambio_texto");
					
					new PNotify({
						title: 'Mensaje',
						text: 'Rúbrica modificada',
						delay: 1200

					});
				}
			})

		    .fail(function(error){
			 	console.log("error-no-edita-rubrica");
			})
		}
		else if(valor_nuevo.length==0)
		{
			$(".cambio").replaceWith("<div class='form-group has-feedback' style = 'margin-bottom: 0px'><input type='text' class='form-control cambio' style = 'width:100%;' placeholder = 'Ingrese un nombre'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red'></i></div>");
			$(".cambio").addClass("borde_rojo");
		}
		else if(valor_estatico == valor_nuevo)
		{
			con_cambio(obj,valor_nuevo,"cambio_texto");
		}
		$(this).parent(".ver").attr("tecla_esc","no");	
	});

	$(".cambio").keypress(function(){
		$(this).removeClass("borde_rojo");
	});

	$("table").on("keypress",".cambio",function(e){ 		 
    	if(e.which == 13) {
    		$(this).parent(".ver").attr("tecla_esc","no");	
    		var padre = $(this).parents("tr");
    		var hermano = $(padre).find(".cambio");
		    $(hermano).trigger("blur"); 
		}
    });

    $("table").on("keyup",".cambio",function(e){
    	if(e.which == 27) {	
    		$(this).parents(".ver").attr("tecla_esc","si");
    		var valor_estatico = $(this).parents(".ver").attr("valor_estatico");
			var valor_nuevo = $(this).val();
			var obj = $(this);
    		sin_cambio(obj,valor_estatico,"cambio_texto");
		}
    });

</script>

<script src="plugins/validationengine/js/jquery.validationEngine.js"></script>
<script src="plugins/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
<script src="plugins/validationengine/js/languages/jquery.validationEngine-es.js"></script>

</body>

</html>