<?php


	if(!empty($_POST["valor"]) and ($_POST["valor"])=="subir_imagen")
	{	
		$fecha_actual = Date("Ymdhis");
		try{
			if(move_uploaded_file($_FILES["file"]["tmp_name"],"imagenes/".$fecha_actual."_".$_FILES["file"]["name"])){
				echo json_encode(array("code"=>"ok",
					"mensaje"=>"Esta imagen se ha subido",
					"nombre_archivo"=>$fecha_actual."_".$_FILES["file"]["name"]
					));
				exit(0);
			}

		}
		catch(Exception $e){
			echo json_encode(array("code"=>"error","mensaje"=>$e->getMessage()));
		}
	}
?>