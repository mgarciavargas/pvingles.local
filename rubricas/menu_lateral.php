<!doctype html>

<html>

<head>
	<script src="js/jquery-3.1.1.min.js"></script>
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

</head>

<style>
	.box.primary header {
    color: #fff;
    background-repeat: repeat-x;
    border-bottom: 1px solid #1c82cc;
    background-image: linear-gradient(to bottom, #1c82cc 0%, #1c82cc 100%)
</style>
<body>


<div class = 'row'>
	<div class = 'col-lg-12' style="padding:0;">
		<div class = 'box primary' style="margin-top: 0;">
			<header style = 'background-color: #1c82cc;'>
				<h5>Elegir plantilla</h5>
				<button class="btn btn-xs btn-danger close-box pull-right cerrar_panel"><i class="icon-remove"></i></button>
			</header>

			<body>

				<ul class = 'list-group'>
					<a href = '#' class = 'row list-group-item pond_estandar' data-toggle='modal' data-target='#myModal'>
						<div class = 'col-lg-3' style="padding:0">
							<img src = 'images/pond_estandar.png' class = 'img-responsive'>
						</div>
						<div class = 'col-lg-9'>
							<dt>Ponderación Estándar</dt>
						</div>
					</a>
					<a href = '#' class = 'row list-group-item alternativas' data-toggle='modal' data-target='#myModal2'>
						<div class = 'col-lg-3' style="padding:0">
							<img src = 'images/alternativas.png' class = 'img-responsive'>
						</div>
						<div class = 'col-lg-9'>
							<dt>Alternativas</dt>
						</div>
					</a>
					<a href = '#' class = 'row list-group-item si_no' data-toggle='modal' data-target='#myModal3'>
						<div class = 'col-lg-3' style="padding:0">
							<img src = 'images/si_no.png' class = 'img-responsive'>
						</div>
						<div class = 'col-lg-9'></dt>
							<dt>SI/NO
						</div>
					</a>
					<a href = '#' class = 'row list-group-item autocompletar' data-toggle='modal' data-target='#myModal4'>
						<div class = 'col-lg-3' style="padding:0">
							<img src = 'images/autocompletar.png' class = 'img-responsive'>
						</div>
						<div class = 'col-lg-9'>
							<dt>Autocompletar</dt>
						</div>
					</a>
				</ul>
			</body>
		</div>
	</div>

</div>

<script>

	$(".cerrar_panel").click(function(){
		$(".aside").hide();
	});

</script>

</body>

</html>

