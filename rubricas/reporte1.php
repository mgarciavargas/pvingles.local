<?php
	session_start();
	require_once("class/class_ajustes.php");
	require_once("class/class_reporte.php");
	require_once("class/class_mantenimiento.php");
	$obj1=new Reporte();
	$obj2=new Ajuste();
	$obj3=new Mantenimiento();
	$id_rubrica = "";
	if(!empty($_GET["id_rubrica"]))
	{
		$id_rubrica=$_GET["id_rubrica"];
	}

	
	//var_dump($res);

?>

<!doctype html>

<head>	
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<script src="js/jquery-3.1.1.min.js"></script>

	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

	<!-- PAGE LEVEL TABLA STYLES -->
    <link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <script src ="plugins/dataTables/Spanish.js"></script>
    <!-- END PAGE LEVEL  STYLES -->

    <script src="js/pnotify.custom.min.js"></script>
  	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">

  	<!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="plugins/validationengine/css/validationEngine.jquery.css" />
	<!-- END PAGE LEVEL  STYLES -->

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">

	<script src = 'js/jquery.cokidoo-textarea.js'></script>
</head>
<body>
	<div class = 'container-fluid'>
		<div class = 'row'>
		    <div class = 'col-lg-12'>
			    <?php
			        require_once("menu.php");
			    ?>
		    </div>
		</div>
		<div class = 'row' style="margin-top: 60px;">
			<div class = 'col-lg-1'></div>
			<div class = 'col-lg-10'>
				<div class = 'panel panel-primary'>
					<div class = 'panel-heading'>
						<h4>REPORTES</h4>
						<input type = 'hidden' class = 'id_rubrica' value = '<?php echo $id_rubrica;?>'>
					</div>
					<div class = 'panel-body'>
						<div class = 'row'>
						<?php
						$rubrica = $obj2->listar_idencuesta($id_rubrica);
						$cant_usuarios = $obj1->cantidad_usuarios_por_rubrica($id_rubrica);
						
						if($cant_usuarios>0) //?
						{
							
						?>
							<div class = 'col-lg-2'></div>
							<div class = 'col-lg-4 text-center'>
								<button class = 'btn btn-primary btn_nivel_general'>A NIVEL GENERAL</button>
							</div>
							<div class = 'col-lg-4 text-center'>
							<?php
								
							if($rubrica[0]["tipo_encuestado"]=="P")
							{
								?>
								<button class = 'btn btn-success btn_por_persona'>POR PERSONA</button>
								<?php
							}
							?>

								
							</div>
							<?php
							
							?>
							<div class = 'col-lg-2'></div>
						</div>

						<br>
						<div class = 'row nivel_general' style="display: none;">
							<?php 
							
							?>
							<div class = 'col-lg-12 text-center'>
								<h4>REPORTES POR DIMENSIONES A NIVEL GENERAL</h4>
								<h3>RÚBRICA: <?php echo $rubrica[0]["titulo"];?></h3>
							</div>

							<div class = 'col-lg-12 text-center'>
								<span class="label label-info">
									CANTIDAD DE PARTICIPANTES: <?php echo $cant_usuarios;?>
								</span>
							</div>
					
						
					<?php
						$reporte=$obj1->listar_dimensiones_por_rubrica($id_rubrica);
						
						$cont=0;
					

						foreach($reporte as $dim)
						{
							$dim1=$obj3->listar_id_dimension($dim["id_dimension"]);

							if($dim1[0]["tipo_pregunta"]=="PE" || $dim1[0]["tipo_pregunta"]=="SN" || $dim1[0]["tipo_pregunta"]=="A")
							{
								$cont++;
							
							$cant_preg = $obj1->contar_preguntas_por_dimension($dim["id_dimension"]);
							$suma_total_por_dimension = $obj1->listar_preguntas_por_dimension_rubrica($dim["id_dimension"],$id_rubrica);
							$puntaje_maximo=0;
							$puntaje_promedio=0;
							$porcentaje_promedio=0;
							//$nombre_escala="";
							
							if($dim1[0]["tipo_pregunta"]=="PE")
							{
								$x=json_decode($dim1[0]["otros_datos"],true);
								$niv = $x["niveles"];
								$puntaje_maximo = (int)$cant_preg*(int)$niv;
							}

							if($dim1[0]["tipo_pregunta"]=="SN")
							{
								$puntaje_maximo = (int)$cant_preg;
							}

							if($dim1[0]["tipo_pregunta"]=="A")
							{
								$puntaje_maximo = 1;
							}
								
								
								$puntaje_promedio = (int)$suma_total_por_dimension[0]["suma"]/(int)$cant_usuarios;
								$porcentaje_obtenido = round((int)$puntaje_promedio*100/(int)$puntaje_maximo);

								$cadena_puntuacion = json_decode($dim1[0]["escalas_evaluacion"],true);
								//var_dump($cadena_puntuacion);
								foreach($cadena_puntuacion as $key=>$value)
								{
									//if(value.rango_inicial<=porcentaje && porcentaje<=value.rango_final)
									if($value["rango_inicial"]<=$porcentaje_obtenido && $porcentaje_obtenido <= $value["rango_final"])
									{
										$nombre_escala = $key;
									}
								}
							?>
							<div class = 'row'>
								<div class = 'col-lg-2'></div>
								<div class = 'col-lg-8'>
									<h4 style="font-size:110%;padding: 5px;color:#fff; background-color:#fb8c00">
										<?php echo "DIMENSIÓN ".$cont.": ".$dim1[0]["nombre"];?>
									</h4>

									<div class = 'table-responsive'>
										<table class = 'table table-striped table-condensed table-hover display'>
											<thead>
												<tr style = 'background-color:#428bca; color: #fff;'>
													<th>Puntaje Promedio Obtenido</th>
													<th>Porcentaje Promedio Obtenido</th>
													<th>Puntaje Máximo</th>
													<th>Escala</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><?php echo $puntaje_promedio;?></td>
													<td><?php echo $porcentaje_obtenido." %";?></td>
													<td><?php echo $puntaje_maximo;?></td>
													<td><?php echo $nombre_escala;?></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class = 'col-lg-2'></div>
							</div>
							<?php
							}
							
						}
					
					
						
					?>	
					</div>

					<div class = 'row nivel_persona' style = 'display:none;'>
						<div class = 'col-lg-12 text-center'>
								<h4>REPORTES POR DIMENSIONES POR PERSONA</h4>
								<h3>RÚBRICA: <?php echo $rubrica[0]["titulo"];?></h3>
							</div>

						<div class = 'col-lg-4'>
						<br>
							<div class = 'table-responsive'>
								<table class = 'table table-striped table-condensed table-hover display'>
									<thead>
										<tr style = 'background-color:#428bca; color: #fff;'>
											<th>Nº</th>
											<th>Nombre</th>
											<th>Empresa</th>
											<th>Fecha</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$persona = $obj1->listar_personas_por_rubrica($id_rubrica);
										$cont_1=0;
										foreach($persona as $pers)
										{
											$cont_1++;
											?>
											<tr>
												<td><?php echo $cont_1;?></td>
												<td><?php echo "<a href = '#' class = 'nombre' id_respuestas = '".$pers["id_respuestas"]."'>".$pers["nombre"]."</a>";?></td>
												<td><?php echo $pers["institucion"];?></td>
												<td><?php echo substr($pers["fecha_registro"],0,10);?></td>
											</tr>
											<?php
										}
										?>
									</tbody>
								</table>
							</div>

						</div> <!--fin de div class col-lg-4-->
						<div class = 'col-lg-8 listado_persona'>

						</div><!-- fin de div class col-lg-8-->
					</div> <!--fin de row nivel persona-->
				<?php
					}
						else
						{
							echo "No hay datos registrados";
						}
				?>
					</div>
					
				</div>
			</div>
			<div class = 'col-lg-1'></div>
		</div>

	</div>

	<script>
	$(".btn_nivel_general").click(function(){
		$(".nivel_general").css("display","block");
		$(".nivel_persona").css("display","none");
	})

	$(".btn_por_persona").click(function(){
		$(".nivel_general").css("display","none");
		$(".nivel_persona").css("display","block");
	})

	$("body").on("click",".nombre",function(){

		$(".listado_persona").html("<img src = 'images/loader.gif'>");
		var id_respuesta = $(this).attr("id_respuestas");
		var id_rubrica = $(".id_rubrica").val();

		$.ajax({
			url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_respuesta: id_respuesta,
				id_rubrica: id_rubrica,
				valor: "mostrar_resultado"
			}
		}).done(function(res){
			if(res.codigo=="OK")
	    	{
				$(".listado_persona").html(res.tabla);
		    }
			}).fail(function(error){
	        console.log("no-muestra-tabla");
	        console.log(error.responseText);
	        })
	})
	</script>
</body>

</html>