<?php
	session_start();
	require_once("class/class_evaluacion.php");
	$obj = new Evaluacion();

?>

<!doctype html>

<head>	
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<script src="js/jquery-3.1.1.min.js"></script>

	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

	<!-- PAGE LEVEL TABLA STYLES -->
    <link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <script src ="plugins/dataTables/Spanish.js"></script>
    <!-- END PAGE LEVEL  STYLES -->

    <script src="js/pnotify.custom.min.js"></script>
  	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">

  	<!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="plugins/validationengine/css/validationEngine.jquery.css" />
	<!-- END PAGE LEVEL  STYLES -->

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">

	<script src = 'js/jquery.cokidoo-textarea.js'></script>
</head>

<body>
	<div class = 'container-fluid'>
		<div class = 'row'>
		    <div class = 'col-lg-12'>
			    <?php
			        require_once("menu2.php");
			    ?>
		    </div>
		</div>
		<div class = 'row' style="margin-top: 60px;">
			<div class = 'col-lg-1'></div>
			<div class = 'col-lg-10'>
				<div class = 'panel panel-info'>
					<div class = 'panel-heading'>
						<div class = 'row'>
							<div class = 'col-lg-4'></div>
							<div class = 'col-lg-4'>
								Tipo de Usuario: 
								 <select class = "form-control" name = 'tipo_cargo'>
				                	<option value = 'Supervisor'>Supervisor</option>
				                	<option value = 'Docente'>Docente</option>
				                	<option value = 'Alumno'>Alumno</option>
				                </select>
				            </div>
				            <div class = 'col-lg-4'></div>
				        </div>
					</div>
					<div class = 'panel-body'>
					<?php
						$encuesta=$obj->listar_evaluaciones();

						if(!empty($encuesta))
						{
							foreach($encuesta as $enc)
							{
								$id_encuesta = $enc["id_encuesta"];
							?>

								<div class = 'col-lg-2'>
									<a href = '<?php echo $pag;?>?id_encuesta=<?php echo $id_encuesta;?>&accion=ver' target='_blank'>
										<div class = 'exa-item'>
											<div class = 'titulo'><?php echo $enc["titulo"];?></div>
											<div class = 'portada'>
												<img class="img-responsive" width="100%" src="images/nofoto.jpg">	
											</div>
										</div>
									</a>
								</div>
							<?php
							}
						}
						else
						{
							echo "No tiene encuestas";
						}
					?>
					</div>
					<div class = 'panel-footer'>
					</div>
				</div>
			</div>
			<div class = 'col-lg-1'></div>
		</div>

	</div>
</body>

</html>