<?php
	require_once("class/class_mantenimiento.php");
	$obj1 = new Mantenimiento();
	
	$dimension = $obj1->listar_id_dimension($_POST["id_dimension"]);
	$accion = "nuevo";
	if(!empty($dimension[0]["escalas_evaluacion"]))
	{
		$accion="editado";
		$x = json_decode($dimension[0]["otros_datos"],true);
		//var_dump($x);
		$cant_niveles = $x["niveles"];
		
		$y = json_decode($dimension[0]["escalas_evaluacion"],true);
		//var_dump($dimension[0]["escalas_evaluacion"]);
		//
	}
?>

<div class = 'modal-header'>
	PUNTUACIÓN
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>

<div class = 'modal-body'>
<input type = 'hidden' class = 'dim_numero' value = '<?php echo $_POST["id_dimension"];?>'>
<div class = 'form-horizontal'>
	<div class = 'form-group'>
		<div class = 'col-xs-4'></div>
		<div class = 'col-xs-4 text-center'>
			<select class = 'form-control niveles'>
				<option value = '0'>Seleccione</option>

			<?php 
				$arr_niveles=array(2,3,4,5);
				for($i=0;$i<sizeof($arr_niveles);$i++)
				{
					echo "<option value = '".$arr_niveles[$i]."'"; if($accion=="editado" && $cant_niveles==$arr_niveles[$i]){ echo "selected";} echo ">".$arr_niveles[$i]."</option>";
				}
			?>
			</select>
		</div>
		<div class = 'col-xs-4'></div>
	</div>

	<div class = 'row'>

		<div class = 'col-lg-12 well muestra_niveles'>

			<div class = 'row'>
				<div class = 'col-xs-4'><dt>Calificación</dt></div>
				<div class = 'col-xs-4'><dt>Rango Inicial</dt></div>
				<div class = 'col-xs-4'><dt>Rango Final</dt></div>
			</div>

		

		<?php
		if($accion=="editado")
		{
			foreach($y as $key=>$value)
			{
				echo 
				"<div class = 'form-group fila' valor_inicial='".$value["rango_inicial"]."' valor_final = '".$value["rango_final"]."' class = 'fila'>
                    <div class = 'col-xs-4'><input type = 'text' value = '".$key."' class = 'form-control representacion'></div>
                    <div class = 'col-xs-4'><input type = 'text' class = 'form-control' disabled value = '".$value["rango_inicial"]."%'></div>
                   <div class = 'col-xs-4'><input type = 'text' class = 'form-control' disabled value = '".$value["rango_final"]."%'></div>
                </div>";
			}
		}
		?>
</div>
	</div>

</div>
</div>

<div class = 'modal-footer'>
	<div class = 'col-lg-12 text-right'>
		<button class = 'btn btn-primary guardar_puntuacion1' data-dismiss="modal">
			Guardar
		</button>
	</div>
</div>
