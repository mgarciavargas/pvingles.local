<?php
	//if (session_status() == PHP_SESSION_NONE) {
            @session_start();
    //    	}
	$ses_idencuesta = $_SESSION["id_rubrica"];
?>

<style>
	::-webkit-input-placeholder {
   font-style: italic;
	}
	:-moz-placeholder {
	   font-style: italic;  
	}
	::-moz-placeholder {
	   font-style: italic;  
	}
	:-ms-input-placeholder {  
	   font-style: italic; 
	}

	
	input[type="radio"]{
		width: 2em;
		height: 2em;
	}
</style>

<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

<div class = 'modal-header text-center'>
	<h4>SI/NO
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button></h4>
</div>

<div class = 'modal-body' style="max-height: 500px; overflow: auto;">
	<div class = 'form-horizontal'>
		<input type = 'hidden' id = 'id_encuesta' value = '<?php echo $_SESSION["id_rubrica"];?>'>
		<input type = 'hidden' class = 'cant_niveles' value = 2>
		<div class = 'form-group hidden' style="display: none !important;">
			<label class = 'control-label col-lg-4'>Niveles:</label>
			<div class = 'col-lg-4'>
				<select class = 'form-control cant_niveles'>
					<option value="2" selected>2</option>
				</select>
			</div>
		</div>
		<div class = 'row'>
			<div class = 'col-lg-12'>
				<div class = 'table-responsive'>
					<table class='table table-striped clonado' id='tabla_creacion_dimensiones' cellpadding="4">
						<tr class = 'cabecera fila_add_dimension'>
							<td width = '90%' colspan = '2' rowspan = '2' class = 'text-center' style = 'background:#2196f3;color:#fff;border-right:2px solid #fff;'><h4>DIMENSIONES</h4></td>
							<td width = '10%' colspan = '2' style = 'background:#2196f3;color:#fff;' class = 'fila_nivel text-center'><dt>OPCIÓN</dt></td>
						</tr>
						<tr class = 'por_nivel'>
							<?php
							$alt=array("SI","NO");
							for($i=0;$i<=1;$i++)
							{
								echo "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>".$alt[$i]."</h5></td>";
							}
							?>
						</tr>
						<tr>
							<td colspan = '4' class = 'col_add_dimension no_imprimir'>
								<div class = 'row bloque_add_dimension'>
									<div class = 'col-lg-12 text-center'>
										<button class = 'btn btn-default agregar_dimension'><i class = 'icon-plus'></i> Agregar Dimensión</button>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>	
			</div>
		</div>
	</div>
</div>
<div class = 'modal-footer'>
	<div class = 'row'>
		<div class = 'col-lg-4 text-left'>
			<button  class="btn btn-default" data-dismiss="modal">Cerrar sin guardar datos</button>
		</div>
		<div class = 'col-lg-4'></div>
		<div class = 'col-lg-4'>
			<button class = 'btn btn-primary guardar_si_no' data-dismiss="modal"><i class = 'icon-save'></i> Guardar Pregunta</button>
		</div>
	</div>
</div>

<script>

$(document).ready(function(){

		$(".agregar_dimension").click(function(){
		var colspan = $(".cant_niveles").val();
		var cant_fila_dimension = parseInt($("#tabla_creacion_dimensiones tr.fila_dimension").length)+1;

		$(this).closest("tr").before("<tr class = 'fila_dimension' style = 'background:#fb8c00;color:#fff;'>"+
										"<td colspan = '2'><dt style = 'display:inline;' class = 'let_dimension'>"+cant_fila_dimension+":</dt>"+
										
											"&nbsp;&nbsp;<div class = 'new_dimension' style = 'display:inline;'><input type = 'text' style = 'width:90%;display:inline;' class = 'form-control nueva_dimension' placeholder = 'Escriba aquí la dimensión'></div></td>"+
										"<td class = 'td_dimension text-right' colspan = '"+colspan+"'>"+
											"<button class = 'btn btn-circle btn-primary save_dimension'><i class = 'icon-save'></i></button>"+
											"<button class = 'btn btn-circle btn-default cancelar_add_dimension'><i class = 'icon-remove'></i></button>"+
										"</td>"+
									"</tr>");
		$(this).attr("disabled","disabled");
		$("#tabla_creacion_dimensiones").find(".agregar_estandar").attr("disabled","disabled");
		$("#tabla_creacion_dimensiones").find(".eliminar_dimension").attr("disabled","disabled");
		$("#tabla_creacion_dimensiones").find(".agregar_pregunta").attr("disabled","disabled");
		$("#tabla_creacion_dimensiones").find(".eliminar_estandar").attr("disabled","disabled");
	});

	$("#tabla_creacion_dimensiones").on("click",".save_dimension",function(){
		var nombre = $(".nueva_dimension").val();
		var let_dimension = $(this).parents("tr").find(".let_dimension").text();
		var cant_fila_dimension = parseInt($("#tabla_creacion_dimensiones tr.fila_dimension").length);

		if(nombre.length==0)
		{
			$(".nueva_dimension").replaceWith("<div style = 'margin-bottom: 0px;display:inline;position:relative;'><input type = 'text' class='form-control nueva_dimension' style = 'width:90%;display:inline;' placeholder = 'Ingrese una dimensión'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red' style = 'top:-10px;'></i></div>");
			$(".nueva_dimension").addClass("borde_rojo");
		}
		else
		{
			$(this).parents("tr").attr("clase_dimension",cant_fila_dimension);
			$(this).parents("tr").find(".nueva_dimension").parents(".new_dimension").replaceWith("<div class = 'reemplazo_dimension' style = 'display:inline;'>"+nombre+"</div>");
			$(this).siblings(".cancelar_add_dimension").remove();
			$(this).parents("tr").find("td").css("padding","5px");
			$(this).parents("tr").addClass("d_"+cant_fila_dimension);
			$(this).parents("tr").find(".let_dimension").text("DIMENSIÓN "+let_dimension);
			$(this).replaceWith("<button class = 'btn btn-primary btn-circle agregar_estandar no_imprimir' data-mytooltip='tooltip1' data-placement='bottom' title='Agregar Estándar'><i class = 'icon-plus'></i></button><button class = 'btn btn-danger btn-circle eliminar_dimension no_imprimir' data-mytooltip='tooltip1' data-placement='bottom' title='Eliminar dimensión'><i class = 'icon-trash'></i></button>");
			$(".agregar_dimension").removeAttr("disabled");

			$("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
			$("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
			$("#tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
			$("#tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");
		}
	});

	

	$("table").on("keypress",".nueva_dimension",function(e){ 		 
    	if(e.which == 13) {	
    		var padre = $(this).parents("tr");
    		var hermano = $(padre).find(".save_dimension");
		    $(hermano).trigger("click"); 
		}
    });

	$("table").on("click",".cancelar_add_dimension",function(){
		$(this).parents("tr").remove();
		$(".agregar_dimension").removeAttr("disabled");
	})

	$("table").on("click",".agregar_estandar",function(){
		var colspan = $(".cant_niveles").val();
		var dim = $(this).parents("tr").attr("clase_dimension");
		var cant_fila_dimension = parseInt($("#tabla_creacion_dimensiones tr.fila_dimension").length);
		var cant_fila_estandar = parseInt($("#tabla_creacion_dimensiones tr.fila_estandar").length);


		var letra = cant_fila_estandar+97;
		
		
		

		$(this).parents("table").find("tr.d_"+dim).last().after("<tr style = 'background:#e8e8e7;' class = 'fila_estandar d_"+dim+"' 															clase_dimension='"+dim+"'>"+
																	"<td colspan = '2'><dt class = 'let_estandar' style = 'display:inline;'>"+String.fromCharCode(letra).toUpperCase()+")</dt>"+
																	    "&nbsp;&nbsp;<div class = 'new_estandar' style = 'display:inline;'><input type = 'text' style = 'width:90%;display:inline;' class = 'form-control nuevo_estandar' placeholder = 'Escriba aquí el estándar'></div>"+
																	 "</td>"+
																	"<td  class = 'td_estandar text-right' colspan = '"+colspan+"'>"+
																		"<button class = 'btn btn-circle btn-primary save_estandar'><i class = 'icon-save'></i></button>"+
																		"<button class = 'btn btn-circle btn-default cancelar_add_estandar'><i class = 'icon-remove'></i></button>"+
																	"</td>"+
																"</tr>");
		var i = 97;
    		$("#tabla_creacion_dimensiones").find("tr.fila_estandar").each(function(){
    			$(this).find(".let_estandar").text(String.fromCharCode(i).toUpperCase()+")"); //incrementar
    			i++;	
    		})
		$("#tabla_creacion_dimensiones").find(".agregar_estandar").attr("disabled","disabled");
		$("#tabla_creacion_dimensiones").find(".eliminar_dimension").attr("disabled","disabled");
		$("#tabla_creacion_dimensiones").find(".agregar_pregunta").attr("disabled","disabled");
		$("#tabla_creacion_dimensiones").find(".eliminar_estandar").attr("disabled","disabled");
	})

	$("#tabla_creacion_dimensiones").on("click",".save_estandar",function(){
		var nombre = $(".nuevo_estandar").val();
		var let_estandar = $(this).parents("tr").find(".let_estandar").text();
		var cant_fila_estandar = parseInt($("#tabla_creacion_dimensiones tr.fila_estandar").length);
		var $this=$(this);
		if(nombre.length==0)
		{
			$(".nuevo_estandar").replaceWith("<div style = 'margin-bottom: 0px;display:inline;position:relative;'><input type = 'text' class='form-control nuevo_estandar' style = 'width:90%;display:inline;' placeholder = 'Ingrese un estándar'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red' style = 'top:-10px;'></i></div>");
			$(".nuevo_estandar").addClass("borde_rojo");
		}
		else
		{
			$(this).parents("tr").attr("clase_estandar",cant_fila_estandar);
			$(this).parents("tr").find(".nuevo_estandar").parents(".new_estandar").replaceWith("<div class = 'reemplazo_estandar' style = 'display:inline;'>"+nombre+"</div>");
			$(this).siblings(".cancelar_add_estandar").remove();
			$(this).parents("tr").find("td").css("padding","5px");
			$(this).parents("tr").addClass("e_"+cant_fila_estandar);
			$(this).parents("tr").find(".let_estandar").text(let_estandar);
			$this.replaceWith("<button class = 'btn btn-success btn-circle agregar_pregunta no_imprimir' data-mytooltip='tooltip1' data-placement='bottom' title='Agregar Pregunta'><i class = 'icon-plus'></i></button><button class = 'btn btn-warning btn-circle eliminar_estandar no_imprimir' data-mytooltip='tooltip1' data-placement='bottom' title='Eliminar estándar'><i class = 'icon-trash'></i></button>");
			$("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
			$("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
			$("#tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
			$("#tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");
			//$('*[data-mytooltip="tooltip1"]').tooltip();
		}
	});

	$("table").on("click",".cancelar_add_estandar",function(){
		$(this).parents("tr").remove();
		$("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
		$("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
		$("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
		$("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
	})

	$("table").on("click",".agregar_pregunta",function(){
		var dim = $(this).parents("tr").attr("clase_dimension");
		var est = $(this).parents("tr").attr("clase_estandar");
		var colspan = $(".cant_niveles").val();
		var cant_fila_pregunta = parseInt($("#tabla_creacion_dimensiones tr.fila_pregunta").length)+1;

		$(this).parents("table").find("tr.e_"+est).last().after("<tr style = 'background:#e8e8e7;' class = 'fila_pregunta d_"+dim+" e_"+															est+"' clase_dimension = '"+dim+"' clase_estandar='"+est+"'>"+
																	"<td width = '5%' style = 'background: #e0e8ef;'><div class = 'let_pregunta' style = 'display:inline;'>"+cant_fila_pregunta+".- </div>"+
																	"</td>"+
																	"<td style = 'background: #fcfce0;'><div class = 'new_pregunta' style = 'display:inline;'><input type = 'text' style = 'width:90%;display:inline;' class = 'form-control nueva_pregunta' placeholder = 'Escriba aquí la pregunta'></div>"+
																	"</td>"+
																	"<td colspan = '"+colspan+"' class = 'niveles'>"+
																		"<button class = 'btn btn-circle btn-primary save_pregunta'><i class = 'icon-save'></i></button>"+
																		"<button class = 'btn btn-circle btn-default cancelar_add_pregunta'><i class = 'icon-remove'></i></button>"+
																	"</td>"+
																"</tr>");

		var i = 1;
    		$("#tabla_creacion_dimensiones").find("tr.fila_pregunta").each(function(){
    			$(this).find(".let_pregunta").text(i+".- "); //incrementar
    			i++;	
    		})

		$(this).parents("table").find(".agregar_estandar").attr("disabled","disabled");
		$(this).parents("table").find(".eliminar_dimension").attr("disabled","disabled");
		$(this).parents("table").find(".agregar_pregunta").attr("disabled","disabled");
		$(this).parents("table").find(".eliminar_estandar").attr("disabled","disabled");

	})

	$("#tabla_creacion_dimensiones").on("click",".save_pregunta",function(){
		var nombre = $(".nueva_pregunta").val();
		var let_pregunta = $(this).parents("tr").find(".let_pregunta").text();
		var cant_fila_pregunta = parseInt($("#tabla_creacion_dimensiones tr.fila_pregunta").length);
		var $this=$(this);
		var cant_niveles = $(".cant_niveles").val();
		var columnas = '';
		if(nombre.length==0)
		{
			$(".nueva_pregunta").replaceWith("<div style = 'margin-bottom: 0px;display:inline;position:relative;'><input type = 'text' class='form-control nueva_pregunta' style = 'width:90%;display:inline;' placeholder = 'Ingrese una pregunta'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red' style = 'top:-10px;'></i></div>");
			$(".nueva_pregunta").addClass("borde_rojo");
		}
		else
		{
			$(this).parents("tr").attr("clase_pregunta",cant_fila_pregunta);
			$(this).parents("tr").find(".new_pregunta").replaceWith("<div class = 'reemplazo_pregunta' style = 'display:inline;'>"+nombre+"</div>");
			$(this).siblings(".cancelar_add_pregunta").remove();
			$(this).parents("tr").find("td").css("padding","5px");
			$(this).parents("tr").addClass("p_"+cant_fila_pregunta);

			for(var i=1;i<=cant_niveles;i++)
			{
				columnas += "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
			}
			var padre = $(this).parents("tr");
			$(this).parents("td").remove().append(columnas);
			$(padre).append(columnas);

			$("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
			$("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
			$("#tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
			$("#tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");
		
		}
	});

	$("table").on("click",".cancelar_add_pregunta",function(){
		$(this).parents("tr").remove();
		$("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
		$("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
		$("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
		$("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
		$("#tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
		$("#tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");
	})


});

</script>