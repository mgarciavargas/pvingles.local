<?php
	require_once("class/class_ajustes.php");
	require_once("class/class_departamento.php");
	require_once("class/class_ugel.php");
	require_once("class/class_personal.php");

	$obj1=new Ajuste();
	$obj2=new Departamento();
	$obj3=new Ugel();
	$obj4=new Personal();
	$rubrica = $obj1->listar_idencuesta($_POST["id_rubrica"]);
	$departamento = $obj2->listar_departamento();
	$ugel = $obj3->listar_ugel();
	$docente = $obj4->listar_docente();
?>
<!-- PAGE LEVEL TABLA STYLES -->
    <link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <script src ="plugins/dataTables/Spanish.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
<div class = 'modal-header'>
	FORMULARIO
</div>
<div class = 'modal-body'>
	<div class = 'container-fluid'>
	<br>
		<div class = 'row'>
			<div class = 'col-lg-12 text-center'>
				<h4 class = 'label label-success' style='font-size:130%;'>Ingrese sus datos para llenar la encuesta</h4>
			</div>
		</div>
		<br>
		<div class = 'row'>
			<div class = 'col-lg-12'>
				<div class = 'escriba_titulo responsable' nombre=''>Escriba aquí su nombre</div>
				<?php
				if($rubrica[0]["opcion_publico"]=="E"){
				?>
					<div class = 'escriba_descripcion' nombre='' style = 'margin-top:20px;'>Escriba aquí su empresa</div>
				<?php
				}
				?>
				
			</div>
		</div>
		<br><br>
		<div class = 'row'>
		<?php
			if($rubrica[0]["opcion_publico"]=="I")
			{
		?>
			<div class = 'form-horizontal'>
				<div class = 'form-group'>
					<label class = 'control-label col-lg-2'>Ciudad</label>
					<div class = 'col-lg-5'>
						<select class = 'form-control departamento' nombre_dep="TACNA" id="iddepartamento" name="iddepartamento" onchange="listar_ugel();">
						<?php
							foreach($departamento as $dep)
							{
								?>
								<option value = '<?php echo $dep["id_ubigeo"];?>'><?php echo $dep["ciudad"];?></option>
								<?php
							}
						?>
						</select>
					</div>
				</div>

				<div class = 'form-group'>
					<label class = 'control-label col-lg-2'>Ugel</label>
					<div class = 'col-lg-5'>
						<select class = 'form-control ugel' name="idugel" id="idugel">
						</select>
					</div>
				</div>

				<div class = 'row'>
					<div class = 'col-lg-12 listado_ie text-center'>

					</div>
				</div>

				<?php 
					if($_POST["id_rubrica"]==38){
					?>
					<div class = 'form-group'>
						<label class = 'control-label col-lg-2'>Docente</label>
						<div class = 'col-lg-5'>
							<select class = 'form-control docente'>
								<option value = '0'>Seleccione</option>
							<?php
								foreach($docente as $doc)
								{
									?>
									<option value = '<?php echo $doc["dni"];?>'><?php echo $doc["ape_paterno"]." ".$doc["ape_materno"]." ".$doc["nombre"];?></option>
									<?php
								}
							?>
							</select>
						</div>
					</div>
						<?php
					}

				?>
				
			</div>
		<?php
			}
		?>
		</div>
</div>
<br><br>
<div class = 'modal-footer'>
		<div class = 'row'>
			<div class = 'col-lg-12 text-right'>
				<button class = 'btn btn-primary continuar'>
					Continuar
				</button>
			</div>
		</div>
	</div>
</div>

<script src="plugins/dataTables/jquery.dataTables.js"></script>
<script src="plugins/dataTables/dataTables.bootstrap.js"></script>
<script>

	$(".departamento").val(23);


	$('#example3').dataTable({
        "language": dt_es
        });

	$(".ugel").change(function(){
		var id_ugel = $(".ugel").val();
		$(".listado_ie").html("<img src = 'images/loader.gif' style = 'text-align:center;'>");
		$.ajax({
    			url: "grabar_ajax.php",
				type: "POST",
				dataType: "json",
				data: {
					"id_ugel": id_ugel,
					valor: "mostrar_ie"
				},
    		}).done(function(res){
	    		if(res.codigo=="OK"){
	    			$(".listado_ie").html(res.tabla);
	    		}
	    	}).fail(function(error){
	            console.log("no-muestra");
	        })
	});

	var con_cambio = function(obj,input_type,nombre)
	{
		if(input_type=="titulo")
		{
			$(obj).replaceWith("<input type = 'text' class = 'form-control titulo_rubrica' value = '"+nombre+"' placeholder = 'Escriba aquí su nombre'>");
			$(".titulo_rubrica").focus();
		}
		else if(input_type=="descripcion")
		{
			$(obj).replaceWith("<textarea class = 'form-control descripcion_rubrica' style = 'margin-top:20px;' placeholder = 'Escriba aquí su empresa'>"+nombre+"</textarea>");
			$(".descripcion_rubrica").focus();

		}
		else
		{
			$(obj).replaceWith("<textarea class = 'form-control pregunta_rubrica' style = 'margin-top:20px;' placeholder = 'Escriba aquí la pregunta'>"+nombre+"</textarea>");
			$(".pregunta_rubrica").focus();
		}
	}

	var sin_cambio = function(obj,input,input_type)
	{
		if(input_type=="titulo"){ $(obj).replaceWith(input);}
		if(input_type=="descripcion"){ $(obj).replaceWith(input); }
		if(input_type=="pregunta"){ $(obj).replaceWith(input); }
		
	}

	$("body").on("click",".escriba_titulo",function(){
		var nombre = $(this).attr("nombre");
		if(nombre.length==0){nombre="";}
		con_cambio($(this),"titulo",nombre);
	});

	$("body").on("click",".escriba_descripcion",function(){
		var nombre = $(this).attr("nombre");
		if(nombre.length==0){nombre="";}
		con_cambio($(this),"descripcion",nombre);
	});

	$("body").on("click",".escriba_pregunta",function(){
		var nombre = $(this).attr("nombre");
		if(nombre.length==0){nombre="";}
		con_cambio($(this),"pregunta",nombre);
	});

	$("body").on("blur",".titulo_rubrica",function(){
		var nombre = $(this).val(); nombre1=nombre;
		if(nombre.length==0)
		{
			nombre = "Escriba aquí su nombre";
			nombre1 = "";
		}

		var input1 = "<div class = 'escriba_titulo responsable' nombre = '"+nombre1+"'>"+nombre+"</div>";
		sin_cambio($(this),input1,"titulo");
	});

	$("body").on("blur",".descripcion_rubrica",function(){
		var nombre_desc = $(this).val(); nombre1_desc=nombre_desc;
		if(nombre_desc.length==0)
		{
			nombre_desc = "Escriba aquí su empresa";
			nombre1_desc = "";
		}
		var input2 = "<div class = 'escriba_descripcion' nombre = '"+nombre1_desc+"' style = 'margin-top:20px;'>"+nombre_desc+"</div>";
		sin_cambio($(this),input2,"descripcion");
	});

	
function listar_ugel(){

		var iddep = $("#iddepartamento").val();
		
		//alert(iddep);
		

		var formData = new FormData();            
        formData.append("iddep",iddep);        
        formData.append("valor","listar_ugel");                    

        $('#idugel').html("");
		$('#idugel').append('<option value="">Seleccione Ugel</option>');

        $.ajax({
            url: "grabar_ajax.php",
            type: "POST",                
            dataType: "json",
            data:  formData,
            contentType: false,
            cache: false,
            processData:false                
        })
        .done(function(res){           
        	console.log(res);
            for (y=0; y<res.length; y++){                

                nom= res[y]["descripcion"];
                id= res[y]["idugel"];

                
                	$('#idugel').append('<option value="'+id+'">'+nom+'</option>');
                
                
                
            }

        }).fail(function() {

            console.log("error ugel");
               
        })
		
}

</script>
</body>

</html>