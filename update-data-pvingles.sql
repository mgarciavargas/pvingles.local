-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-01-2018 a las 15:27:41
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_curso`
--

DROP TABLE IF EXISTS `acad_curso`;
CREATE TABLE `acad_curso` (
  `idcurso` int(1) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(1) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idusuario` int(11) NOT NULL,
  `vinculosaprendizajes` text COLLATE utf8_spanish_ci,
  `materialesyrecursos` text COLLATE utf8_spanish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_curso`
--

INSERT INTO `acad_curso` (`idcurso`, `nombre`, `imagen`, `descripcion`, `estado`, `fecharegistro`, `idusuario`, `vinculosaprendizajes`, `materialesyrecursos`) VALUES
(1, 'A1', '/static/media/image/N1-U1-A1-20170630052326.jpg', 'Ingles inicial para estudiantes de primaria', 1, '2017-11-13 14:19:16', 43831104, '', NULL),
(2, 'A2', '/static/media/imagenes/cursos/nofoto.jpg', 'descripcion del curso a2', 1, '2017-11-13 14:26:00', 43831104, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_cursoaprendizaje`
--

DROP TABLE IF EXISTS `acad_cursoaprendizaje`;
CREATE TABLE `acad_cursoaprendizaje` (
  `idaprendizaje` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `idcursodetalle` int(11) NOT NULL,
  `idpadre` int(11) NOT NULL,
  `idrecurso` int(11) DEFAULT NULL,
  `descripcion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_cursodetalle`
--

DROP TABLE IF EXISTS `acad_cursodetalle`;
CREATE TABLE `acad_cursodetalle` (
  `idcursodetalle` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `orden` bigint(20) NOT NULL,
  `idrecurso` int(11) NOT NULL,
  `tiporecurso` varchar(2) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Examen, nivel , rubrica,etc',
  `idlogro` int(11) NOT NULL,
  `url` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `idpadre` smallint(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_cursodetalle`
--

INSERT INTO `acad_cursodetalle` (`idcursodetalle`, `idcurso`, `orden`, `idrecurso`, `tiporecurso`, `idlogro`, `url`, `idpadre`) VALUES
(58, 1, 1, 60, 'L', 0, '', 57),
(52, 1, 4, 3, 'E', 0, '', 2),
(54, 1, 3, 3, 'N', 0, '', 0),
(55, 1, 4, 4, 'N', 0, '', 0),
(56, 1, 5, 5, 'N', 0, '', 0),
(57, 1, 1, 59, 'U', 0, '', 56),
(51, 1, 14, 3, 'E', 3, '', 1),
(47, 1, 12, 17, 'U', 0, '', 1),
(48, 1, 1, 51, 'L', 0, '', 47),
(49, 1, 2, 52, 'L', 0, '', 47),
(50, 1, 3, 53, 'L', 0, '', 47),
(40, 1, 1, 45, 'L', 0, '', 39),
(41, 1, 2, 46, 'L', 0, '', 39),
(42, 1, 3, 47, 'L', 0, '', 39),
(43, 1, 11, 16, 'U', 0, '', 1),
(44, 1, 1, 48, 'L', 0, '', 43),
(45, 1, 2, 49, 'L', 0, '', 43),
(46, 1, 3, 50, 'L', 0, '', 43),
(36, 1, 1, 43, 'L', 0, '', 35),
(37, 1, 2, 42, 'L', 0, '', 35),
(38, 1, 3, 44, 'L', 0, '', 35),
(39, 1, 10, 15, 'U', 0, '', 1),
(32, 1, 1, 39, 'L', 0, '', 31),
(33, 1, 2, 40, 'L', 0, '', 31),
(34, 1, 3, 41, 'L', 0, '', 31),
(35, 1, 9, 14, 'U', 0, '', 1),
(28, 1, 1, 36, 'L', 0, '', 27),
(29, 1, 2, 37, 'L', 0, '', 27),
(30, 1, 3, 38, 'L', 0, '', 27),
(31, 1, 8, 13, 'U', 0, '', 1),
(24, 1, 1, 33, 'L', 0, '', 23),
(25, 1, 2, 34, 'L', 0, '', 23),
(26, 1, 3, 35, 'L', 0, '', 23),
(27, 1, 7, 12, 'U', 0, '', 1),
(20, 1, 1, 30, 'L', 0, '', 19),
(21, 1, 2, 31, 'L', 0, '', 19),
(22, 1, 3, 32, 'L', 0, '', 19),
(23, 1, 6, 11, 'U', 0, '', 1),
(16, 1, 1, 27, 'L', 0, '', 15),
(17, 1, 2, 58, 'L', 0, '', 15),
(18, 1, 3, 29, 'L', 0, '', 15),
(19, 1, 5, 10, 'U', 0, '', 1),
(13, 1, 2, 25, 'L', 0, '', 11),
(14, 1, 3, 26, 'L', 0, '', 11),
(15, 1, 4, 9, 'U', 2, '', 1),
(1, 1, 1, 1, 'N', 1, '', 0),
(2, 1, 1, 6, 'U', 0, '', 1),
(3, 1, 1, 18, 'L', 0, '', 2),
(4, 1, 2, 56, 'L', 0, '', 2),
(5, 1, 3, 55, 'L', 0, '', 2),
(53, 1, 2, 2, 'N', 0, '', 0),
(7, 1, 2, 7, 'U', 3, '', 1),
(8, 1, 1, 21, 'L', 0, '', 7),
(9, 1, 2, 22, 'L', 0, '', 7),
(10, 1, 3, 23, 'L', 0, '', 7),
(11, 1, 3, 8, 'U', 0, '', 1),
(12, 1, 1, 24, 'L', 0, '', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_cursosesion`
--

DROP TABLE IF EXISTS `acad_cursosesion`;
CREATE TABLE `acad_cursosesion` (
  `idsesion` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `duracion` time DEFAULT NULL,
  `informaciongeneral` longtext,
  `idcursodetalle` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_grupoaula`
--

DROP TABLE IF EXISTS `acad_grupoaula`;
CREATE TABLE `acad_grupoaula` (
  `idgrupoaula` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'virtual,presencial,mixto',
  `comentario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `nvacantes` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_grupoaula`
--

INSERT INTO `acad_grupoaula` (`idgrupoaula`, `nombre`, `tipo`, `comentario`, `nvacantes`, `estado`) VALUES
(6, 'InglesBasico', 'P', '', 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_grupoauladetalle`
--

DROP TABLE IF EXISTS `acad_grupoauladetalle`;
CREATE TABLE `acad_grupoauladetalle` (
  `idgrupoauladetalle` bigint(20) NOT NULL,
  `idgrupoaula` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `iddocente` int(11) NOT NULL,
  `idlocal` int(11) DEFAULT '0',
  `idambiente` int(11) DEFAULT '0' COMMENT 'aula física o laboratorio donde se dará la clase',
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_final` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_grupoauladetalle`
--

INSERT INTO `acad_grupoauladetalle` (`idgrupoauladetalle`, `idgrupoaula`, `idcurso`, `iddocente`, `idlocal`, `idambiente`, `nombre`, `fecha_inicio`, `fecha_final`) VALUES
(5, 6, 1, 43831104, 1, 1, '-', '2018-01-07 05:00:00', '2018-01-07 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_horariogrupodetalle`
--

DROP TABLE IF EXISTS `acad_horariogrupodetalle`;
CREATE TABLE `acad_horariogrupodetalle` (
  `idhorario` int(11) NOT NULL,
  `idgrupoauladetalle` int(11) NOT NULL,
  `fecha_finicio` timestamp NOT NULL,
  `fecha_final` timestamp NOT NULL,
  `descripcion` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `color` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idhorariopadre` tinyint(4) NOT NULL,
  `diasemana` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_horariogrupodetalle`
--

INSERT INTO `acad_horariogrupodetalle` (`idhorario`, `idgrupoauladetalle`, `fecha_finicio`, `fecha_final`, `descripcion`, `color`, `idhorariopadre`, `diasemana`) VALUES
(4, 5, '2018-01-28 11:00:00', '2018-01-28 13:00:00', 'A1', '#2b8ace', 1, 0),
(3, 5, '2018-01-21 11:00:00', '2018-01-21 13:00:00', 'A1', '#2b8ace', 1, 4),
(2, 5, '2018-01-14 11:00:00', '2018-01-14 13:00:00', 'A1', '#6ad722', 1, 2),
(1, 5, '2018-01-07 11:00:00', '2018-01-07 13:00:00', 'A1', '#2b8ace', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acad_matricula`
--

DROP TABLE IF EXISTS `acad_matricula`;
CREATE TABLE `acad_matricula` (
  `idmatricula` int(11) NOT NULL,
  `idgrupoauladetalle` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fecha_matricula` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `acad_matricula`
--

INSERT INTO `acad_matricula` (`idmatricula`, `idgrupoauladetalle`, `idalumno`, `fecha_registro`, `estado`, `idusuario`, `fecha_matricula`) VALUES
(3, 5, 454545455, '2018-01-13 05:00:00', 1, 43831104, '2018-01-13 05:00:00'),
(4, 5, 12345678, '2018-01-14 05:00:00', 1, 43831104, '2018-01-14 06:47:45'),
(5, 5, 87654321, '2018-01-14 05:00:00', 1, 43831104, '2018-01-14 06:47:45'),
(6, 5, 13578642, '2018-01-14 05:00:00', 1, 43831104, '2018-01-14 06:47:45'),
(7, 5, 86421357, '2018-01-14 05:00:00', 1, 43831104, '2018-01-14 06:47:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ambiente`
--

DROP TABLE IF EXISTS `ambiente`;
CREATE TABLE `ambiente` (
  `idambiente` int(11) NOT NULL,
  `idlocal` int(11) NOT NULL,
  `numero` varchar(3) COLLATE utf8_spanish_ci DEFAULT NULL,
  `capacidad` int(11) NOT NULL DEFAULT '0',
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `estado` tinyint(4) DEFAULT NULL,
  `turno` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ambiente`
--

INSERT INTO `ambiente` (`idambiente`, `idlocal`, `numero`, `capacidad`, `tipo`, `estado`, `turno`) VALUES
(1, 1, '101', 10, 'L', 1, 'T'),
(2, 1, '401', 30, 'L', 1, 'M'),
(3, 1, '404', 20, 'L', 0, 'N'),
(4, 1, '300', 100, 'A', 1, 'T');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `general`
--

DROP TABLE IF EXISTS `general`;
CREATE TABLE `general` (
  `idgeneral` int(11) NOT NULL,
  `codigo` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_tabla` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `general`
--

INSERT INTO `general` (`idgeneral`, `codigo`, `nombre`, `tipo_tabla`, `mostrar`) VALUES
(1, 'M', 'Masculino', 'sexo', 1),
(2, 'F', 'Femenino', 'sexo', 1),
(3, '1', 'DNI', 'tipodocidentidad', 1),
(4, '2', 'Passaporte', 'tipodocidentidad', 1),
(5, '1', 'Padre', 'parentesco', 1),
(6, '2', 'Madre', 'parentesco', 1),
(7, '3', 'Tio(a)', 'parentesco', 1),
(8, '3', 'Apoderado', 'parentesco', 1),
(9, 'C', 'Casado', 'estadocivil', 1),
(10, 'S', 'Soltero', 'estadocivil', 1),
(11, 'V', 'Viudo', 'estadocivil', 1),
(12, 'D', 'Divorciado', 'estadocivil', 1),
(13, 'B', 'Becado', 'condicionpersona', 1),
(14, 'O', 'Observado', 'condicionpersona', 1),
(16, '1', 'Primaria', 'tipoestudio', 1),
(17, '2', 'Segundaria', 'tipoestudio', 1),
(18, '3', 'Diplomado', 'tipoestudio', 1),
(19, '4', 'Universitario Bachiller', 'tipoestudio', 1),
(20, '5', 'Universitario-titulado', 'tipoestudio', 1),
(21, '6', 'Maestria', 'tipoestudio', 1),
(22, '7', 'Doctorado', 'tipoestudio', 1),
(23, '1', 'En curso', 'situacionestudio', 1),
(24, '2', 'Graduado', 'situacionestudio', 1),
(25, '3', 'Abandonado', 'situacionestudio', 1),
(26, '1', 'Ing. de Sistemas', 'areaestudio', 1),
(27, '2', 'Derecho', 'areaestudio', 1),
(28, '1', 'Logistica', 'areaempresa', 1),
(29, '2', 'Gerencia General', 'areaempresa', 1),
(30, 'V', 'Vitual', 'tipogrupo', 1),
(31, 'P', 'Presencial', 'tipogrupo', 1),
(32, 'M', 'Mixto', 'tipogrupo', 1),
(33, '1', 'Open', 'estadogrupo', 1),
(34, '2', 'Closed', 'estadogrupo', 1),
(35, '3', 'Anulado', 'estadogrupo', 1),
(36, 'L', 'Laboratorio', 'tipoambiente', 1),
(37, 'A', 'Aula', 'tipoambiente', 1),
(38, 'U', 'Auditorio', 'tipoambiente', 1),
(39, 'M', 'Mañana', 'turno', 1),
(40, 'T', 'Tarde', 'turno', 1),
(41, 'N', 'Noche', 'truno', 1);
INSERT INTO `abacoedu_devsmartlearn`.`general` (`idgeneral`, `codigo`, `nombre`, `tipo_tabla`, `mostrar`) VALUES ('42', '3', 'Informática', 'areaempresa', '1');
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local`
--

DROP TABLE IF EXISTS `local`;
CREATE TABLE `local` (
  `idlocal` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `direccion` text COLLATE utf8_spanish_ci,
  `id_ubigeo` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `vacantes` int(11) DEFAULT NULL,
  `idugel` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `local`
--

INSERT INTO `local` (`idlocal`, `nombre`, `direccion`, `id_ubigeo`, `tipo`, `vacantes`, `idugel`) VALUES
(1, 'Institucion educativa 1154', 'una direccion #123', '140202', 'C', 10, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_apoderado`
--

DROP TABLE IF EXISTS `persona_apoderado`;
CREATE TABLE `persona_apoderado` (
  `idapoderado` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ape_paterno` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ape_materno` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `sexo` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `tipodoc` int(11) NOT NULL,
  `ndoc` int(11) NOT NULL,
  `parentesco` int(11) NOT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `celular` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persona_apoderado`
--

INSERT INTO `persona_apoderado` (`idapoderado`, `idpersona`, `nombre`, `ape_paterno`, `ape_materno`, `correo`, `sexo`, `tipodoc`, `ndoc`, `parentesco`, `telefono`, `celular`, `mostrar`) VALUES
(1, 43831104, 'Abel', 'Chingo', 'Tello', 'abel_chingo@hotmail.com', 'M', 1, 43831104, 1, '97456', '456456', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_educacion`
--

DROP TABLE IF EXISTS `persona_educacion`;
CREATE TABLE `persona_educacion` (
  `ideducacion` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `institucion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `tipodeestudio` int(2) NOT NULL,
  `titulo` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `areaestudio` int(11) DEFAULT NULL,
  `situacion` int(1) NOT NULL,
  `fechade` date NOT NULL,
  `fechahasta` date DEFAULT NULL,
  `actualmente` int(1) DEFAULT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persona_educacion`
--

INSERT INTO `persona_educacion` (`ideducacion`, `idpersona`, `institucion`, `tipodeestudio`, `titulo`, `areaestudio`, `situacion`, `fechade`, `fechahasta`, `actualmente`, `mostrar`) VALUES
(5, 43831104, 'C.E.I Cristo Rey 16006', 1, '', 0, 3, '2018-01-04', '2018-01-04', 1, 1),
(2, 43831104, 'USS', 4, 'abogado', 1, 1, '2018-01-04', '2018-01-04', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_experiencialaboral`
--

DROP TABLE IF EXISTS `persona_experiencialaboral`;
CREATE TABLE `persona_experiencialaboral` (
  `idexperiencia` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `empresa` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `rubro` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `area` int(11) NOT NULL,
  `cargo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `funciones` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `fechade` date NOT NULL,
  `fechahasta` date NOT NULL,
  `actualmente` int(1) NOT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persona_experiencialaboral`
--

INSERT INTO `persona_experiencialaboral` (`idexperiencia`, `idpersona`, `empresa`, `rubro`, `area`, `cargo`, `funciones`, `fechade`, `fechahasta`, `actualmente`, `mostrar`) VALUES
(3, 43831104, 'Abaco', 'Educación', 2, 'gerente general', 'Planeamiento de estructuras y negocios ', '2018-01-04', '2018-01-05', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_metas`
--

DROP TABLE IF EXISTS `persona_metas`;
CREATE TABLE `persona_metas` (
  `idmeta` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `meta` text COLLATE utf8_spanish_ci NOT NULL,
  `objetivo` text COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persona_metas`
--

INSERT INTO `persona_metas` (`idmeta`, `idpersona`, `meta`, `objetivo`, `mostrar`) VALUES
(1, 43831104, 'Incrementar las ventas del sistema ', 'Vender 100 productos diarios ', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_referencia`
--

DROP TABLE IF EXISTS `persona_referencia`;
CREATE TABLE `persona_referencia` (
  `idreferencia` int(11) NOT NULL,
  `idpersona` int(8) NOT NULL,
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `cargo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `relacion` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `mostrar` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persona_referencia`
--

INSERT INTO `persona_referencia` (`idreferencia`, `idpersona`, `nombre`, `cargo`, `relacion`, `correo`, `telefono`, `mostrar`) VALUES
(1, 43831104, 'Abel Chingo Tello', 'Jefe de Area', 'Jefe Directo', 'abelchingo@gmail.com', '942171837', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_rol`
--

DROP TABLE IF EXISTS `persona_rol`;
CREATE TABLE `persona_rol` (
  `iddetalle` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona_rol`
--

INSERT INTO `persona_rol` (`iddetalle`, `idrol`, `idpersonal`) VALUES
(6, 1, 11111111),
(5, 2, 33333333),
(4, 3, 99999999),
(3, 3, 43831104),
(2, 2, 43831104),
(1, 1, 43831104);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `idrol` int(11) NOT NULL,
  `rol` varchar(35) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`idrol`, `rol`) VALUES
(1, 'Administrador'),
(2, 'Docente'),
(3, 'Alumno'),
(4, 'Supervisor');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acad_curso`
--
ALTER TABLE `acad_curso`
  ADD PRIMARY KEY (`idcurso`);

--
-- Indices de la tabla `acad_cursoaprendizaje`
--
ALTER TABLE `acad_cursoaprendizaje`
  ADD PRIMARY KEY (`idaprendizaje`);

--
-- Indices de la tabla `acad_cursodetalle`
--
ALTER TABLE `acad_cursodetalle`
  ADD PRIMARY KEY (`idcursodetalle`);

--
-- Indices de la tabla `acad_cursosesion`
--
ALTER TABLE `acad_cursosesion`
  ADD PRIMARY KEY (`idsesion`);

--
-- Indices de la tabla `acad_grupoaula`
--
ALTER TABLE `acad_grupoaula`
  ADD PRIMARY KEY (`idgrupoaula`);

--
-- Indices de la tabla `acad_grupoauladetalle`
--
ALTER TABLE `acad_grupoauladetalle`
  ADD PRIMARY KEY (`idgrupoauladetalle`);

--
-- Indices de la tabla `acad_horariogrupodetalle`
--
ALTER TABLE `acad_horariogrupodetalle`
  ADD PRIMARY KEY (`idhorario`);

--
-- Indices de la tabla `acad_matricula`
--
ALTER TABLE `acad_matricula`
  ADD PRIMARY KEY (`idmatricula`);

--
-- Indices de la tabla `ambiente`
--
ALTER TABLE `ambiente`
  ADD PRIMARY KEY (`idambiente`);

--
-- Indices de la tabla `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`idgeneral`);

--
-- Indices de la tabla `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`idlocal`);

--
-- Indices de la tabla `persona_educacion`
--
ALTER TABLE `persona_educacion`
  ADD PRIMARY KEY (`ideducacion`);

--
-- Indices de la tabla `persona_experiencialaboral`
--
ALTER TABLE `persona_experiencialaboral`
  ADD PRIMARY KEY (`idexperiencia`);

--
-- Indices de la tabla `persona_metas`
--
ALTER TABLE `persona_metas`
  ADD PRIMARY KEY (`idmeta`);

--
-- Indices de la tabla `persona_referencia`
--
ALTER TABLE `persona_referencia`
  ADD PRIMARY KEY (`idreferencia`);

--
-- Indices de la tabla `persona_rol`
--
ALTER TABLE `persona_rol`
  ADD PRIMARY KEY (`iddetalle`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idrol`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
