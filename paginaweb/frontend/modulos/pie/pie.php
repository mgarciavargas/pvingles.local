<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('jrAdwen::JrModulo', RUTA_LIBS, 'jrAdwen::');
JrCargador::clase('modulos::pie::Pie', RUTA_SITIO, 'modulos::pie');
$oMod = new Pie;
echo $oMod->mostrar(@$atributos['posicion']);