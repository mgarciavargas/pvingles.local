<?php
/**
 * @autor		: Abel Chingo Tello . ACHT
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class Pie extends JrModulo
{
	public function __construct()
	{
		parent::__construct();		
		$this->modulo = 'pie';
	}
	
	public function mostrar($html=null)
	{
		try {			
			if(empty($html)){
				$this->esquema = 'pie';
			}else{
				$this->plt=$html;
				$this->esquema = 'pie';
			}
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}