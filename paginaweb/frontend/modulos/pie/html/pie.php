<?php
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?>
<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic(); ?>/libs/blocklist/blocksit.min.js">
  
</script>
<style type="text/css">
.degagadopie{
  background: rgba(255,255,255,0.81);
background: -moz-linear-gradient(top, rgba(255,255,255,0.81) 1%, rgba(119,254,249,0.81) 51%, rgba(37,253,246,0.93) 81%, rgba(83,165,237,1) 100%);
background: -webkit-gradient(left top, left bottom, color-stop(1%, rgba(255,255,255,0.81)), color-stop(51%, rgba(119,254,249,0.81)), color-stop(81%, rgba(37,253,246,0.93)), color-stop(100%, rgba(83,165,237,1)));
background: -webkit-linear-gradient(top, rgba(255,255,255,0.81) 1%, rgba(119,254,249,0.81) 51%, rgba(37,253,246,0.93) 81%, rgba(83,165,237,1) 100%);
background: -o-linear-gradient(top, rgba(255,255,255,0.81) 1%, rgba(119,254,249,0.81) 51%, rgba(37,253,246,0.93) 81%, rgba(83,165,237,1) 100%);
background: -ms-linear-gradient(top, rgba(255,255,255,0.81) 1%, rgba(119,254,249,0.81) 51%, rgba(37,253,246,0.93) 81%, rgba(83,165,237,1) 100%);
background: linear-gradient(to bottom, rgba(255,255,255,0.81) 1%, rgba(119,254,249,0.81) 51%, rgba(37,253,246,0.93) 81%, rgba(83,165,237,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#53a5ed', GradientType=0 );
}
.titulopie{
  color: rgb(131, 131, 131);
  font-family: dinpro-bold;
  line-height: 24px;
  letter-spacing: -0.020em;
  font-weight: bold;
  color: #656060;
  font-size: 18px;
}

.grid{
  min-width:188px;
  min-height:100px;
  padding: 15px;
  background:transparent;
  margin:6px;
  font-size:12px;
  float:left;
}

.grid strong {
  border-bottom:1px solid #ccc;
  margin:10px 0;
  display:block;
  padding:0 0 5px;
  font-size:17px;
}
.grid .meta{
  text-align:right;
  color:#777;
  font-style:italic;
}
.grid .imgholder img{
  max-width:100%;
  background:#ccc;
  display:block;
}

</style>
<div class="degagadopie" style="min-height: calc(100vh);">
  <div class="col-12" style="padding: 1em auto">
    <div class="container" style="margin: 2.5em auto;">
    <div class="row" >
    <div class="col-md-6 col-sm-12 text-left">
      <div class="titulopie" >¿Necesitas ayuda? ¡ Escribenos !</div>
    </div>
    <div class="col-md-6 col-sm-12 text-right">
      <div class="titulopie" >
        <span class="col-4"><a href="tel:942171837"<i class="fa fa-mobile-phone"></i> (+51) 94171837</a></span>
        <span class="col-4"><a href="mailto:soporte@ingenioytalento.com"<i class="fa fa-envelope"></i> soporte@ingenioytalento.com</a></span>
        <span class="col-4"><a href="mailto:soporte@ingenioytalento.com"<i class="fa fa-help"></i> Ayuda</a></span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3">
       <div class="titulopie">Categorias</div>
       <ul class="showcategorias"></ul>
    </div>
    <div class="col-md-9" id="showbloklist">
      <div class="grid">        
        <strong class="titulopie">Sunset Lake</strong>
        <p>A peaceful sunset view...</p>        
      </div>
        
    </div>
   
  </div>
  <div class="row">
    <div class="col-md-6 col-sm-12 text-left">
      <div class="titulopie" >Métodos de pago</div>
      <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/metodosdepago.png">
    </div>
    <div class="col-md-6 col-sm-12 text-left">
      <div class="titulopie" >Siguenos en</div>
      <div>
        <span><i class="fa fa-facebook"></i></span>
        <span><i class="fa fa-twitter"></i></span>
        <span><i class="fa fa-instagram"></i></span>
        <span><i class="fa fa-google-plus"></i></span>
        <span><i class="fa fa-linkedin"></i></span>
        <span><i class="fa fa-youtube"></i></span>
      </div>
      <div class="text-left">Copyright © Ingenio&Talento 2018 - Todos los derechos reservados</div>
    </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 text-left" style="margin-top: 40px;">
      
    </div>    
  </div>
  </div>  
</div>
<script type="text/javascript">
  $(document).ready(function() {
  //vendor script
  $('#header')
  .css({ 'top':-50 })
  .delay(1000)
  .animate({'top': 0}, 800);
  
  $('#footer')
  .css({ 'bottom':-15 })
  .delay(1000)
  .animate({'bottom': 0}, 800);

  //blocksit define
  $(document).ready(function() {
    $('#showbloklist').BlocksIt({
      numOfCol: 4,
      offsetX: 6,
      offsetY: 6
    })
  });
  
  //window resize
  var currentWidth = 1100;
  $(window).resize(function() {
    var winWidth = $(window).width();
    var conWidth;
    if(winWidth < 660) {
      conWidth = 440;
      col = 2
    } else if(winWidth < 880) {
      conWidth = 660;
      col = 3
    } else if(winWidth < 1100) {
      conWidth = 880;
      col = 4;
    } else {
      conWidth = 1100;
      col = 5;
    }
    
    if(conWidth != currentWidth) {
      currentWidth = conWidth;
      $('#showbloklist').width(conWidth);
      $('#showbloklist').BlocksIt({
        numOfCol: col,
        offsetX: 8,
        offsetY: 8
      });
    }
  });
});

</script>