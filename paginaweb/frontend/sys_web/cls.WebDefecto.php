<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBolsa_publicaciones', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBolsa_postulante', RUTA_BASE, 'sys_negocio');
/*JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE, 'sys_negocio');*/
class WebDefecto extends JrWeb
{
	private $oNegEmpresas;
	private $oNegPublicaciones;
	private $oNegPostulante;
	/*protected $oNegGrupos;
    protected $oNegLocal;
    protected $oNegNiveles;
    protected $oNegGrupo_matricula;
    private $oNegAulavirtualinvitados;*/  
	public function __construct()
	{
		parent::__construct();
		$this->oNegEmpresas = new NegBolsa_empresas;
		$this->oNegPublicaciones = new NegBolsa_publicaciones;
		$this->oNegPostulante = new NegBolsa_postulante;
		/*$this->oNegAlumno=new NegAlumno;
		$this->oNegGrupos = new NegGrupos;
        $this->oNegLocal = new NegLocal;
        $this->oNegGrupo_matricula = new NegGrupo_matricula;
        $this->oNegAulavirtualinvitados = new NegAulavirtualinvitados;*/
	}

	public function defecto(){
		global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Web '),true);
			$this->esquema = 'paginaweb/inicio';
			$this->documento->plantilla ='inicio';
			return parent::getEsquema();		
	}	

	public function verlistado()
	{
		try{
			global $aplicacion;	
			$filtro=array();
			@extract($_REQUEST);
			if(!empty($texto))$filtro['buscar']=$texto;
			$filtro['mostrar']=1;
			@session_start();
			$this->login=false;
			if(!empty($_SESSION["loginempresa"])){
				$this->login=true;
				$this->empresa=$_SESSION["loginempresa"];				
			}
			$this->isLoginuser=NegSesion::existeSesion();

			$this->publicaciones=$this->oNegPublicaciones->buscar($filtro);	
			$this->esquema = 'bolsatrabajo/publicaciones';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'bolsatrabajo/general2';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function avisologueate()
	{
		try{
			global $aplicacion;	
			$filtro=array();
			@extract($_REQUEST);
			if(!empty($texto))$filtro['buscar']=$texto;
			$filtro['mostrar']=1;
			$this->publicaciones=$this->oNegPublicaciones->buscar($filtro);	
			$this->esquema = 'login-aviso';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'bolsatrabajo/general2';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function mispublicaciones()
	{
		try{
			global $aplicacion;
			$filtro=array();
			@extract($_REQUEST);
			if(!empty($texto))$filtro['buscar']=$texto;
			$filtro['mostrar']=1;
			@session_start();
			$this->login=false;
			if(!empty($_SESSION["loginempresa"])){
				$this->login=true;
				$this->empresa=$_SESSION["loginempresa"];
				$filtro["idempresa"]=$this->empresa["idempresa"];
			}
			$this->publicaciones=$this->oNegPublicaciones->buscar($filtro);
			$this->documento->setTitulo(JrTexto::_('Mis publicaciones'),true);
			$this->esquema = 'bolsatrabajo/publicaciones';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'bolsatrabajo/general2';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

/*
public function mispostulantes()
	{
		try{
			global $aplicacion;
			$filtro=array();
			@extract($_REQUEST);
			if(!empty($texto))$filtro['buscar']=$texto;
			$filtro['mostrar']=1;
			@session_start();
			$this->login=false;
			if(!empty($_SESSION["loginempresa"])){
				$this->login=true;
				$this->empresa=$_SESSION["loginempresa"];
				$filtro["idempresa"]=$this->empresa["idempresa"];
			}
			$this->publicaciones=$this->oNegPublicaciones->buscar($filtro);
			$this->documento->setTitulo(JrTexto::_('Mis publicaciones'),true);
			$this->esquema = 'bolsatrabajo/publicaciones';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'bolsatrabajo/general2';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/

	public function verpublicacion()
	{
		try{
			global $aplicacion;
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			$filtro=array();
			@extract($_REQUEST);			
			$filtro['mostrar']=1;			
			@session_start();
			$this->login=false;
			if(!empty($_SESSION["loginempresa"])){
				$this->login=true;
				$this->empresa=$_SESSION["loginempresa"];				
			}
			if(!empty($_REQUEST["idpublicacion"])){
				$idpublicacion=$_REQUEST["idpublicacion"];
				$publicaciones=$this->oNegPublicaciones->buscar(array('idpublicacion'=>$idpublicacion));
				if(!empty($publicaciones[0])){					
					$this->publicaciones=$publicaciones[0];
				}
				if($this->login)
				$this->postulantes=$this->oNegPostulante->buscar(array('idpublicacion'=>$idpublicacion));
			}			
			$this->documento->setTitulo(JrTexto::_('Mis publicaciones'),true);
			$this->esquema = 'bolsatrabajo/verpublicacion';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'bolsatrabajo/general2';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function publicar()
	{
		try {
			global $aplicacion;
			$this->documento->script('tinymce.min', '/libs/tinymce/');
			@session_start();
			$this->login=false;
			if(!empty($_SESSION["loginempresa"])){
				$this->login=true;
				$this->empresa=$_SESSION["loginempresa"];
				if(!empty($_REQUEST["idpublicacion"])){
					$idpublicacion=$_REQUEST["idpublicacion"];
					$publicaciones=$this->oNegPublicaciones->buscar(array('idpublicacion'=>$idpublicacion));
					if(!empty($publicaciones[0])){
						$idempresa=$publicaciones[0]["idempresa"];
						if($idempresa==$this->empresa["idempresa"])
						$this->publicaciones=$publicaciones[0];
					}
				}
			}
			$this->documento->setTitulo(JrTexto::_('Publish'),true);
			$this->esquema = 'bolsatrabajo/crear_publicacion';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'bolsatrabajo/general2';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
			
		}
	}

	public function crearempresa()
	{
		try {
			global $aplicacion;	
			@session_start();
			if(!empty($_SESSION["loginempresa"])){				
				$datos=$_SESSION["loginempresa"];
				$idempresa=$datos["idempresa"];
				$empresa=$this->oNegEmpresas->buscar(array('idempresa'=>$idempresa));
				if(!empty($empresa[0])) $this->datos=$empresa[0];
				$_SESSION["loginempresa"]=$this->datos;
			}			
			$this->documento->setTitulo(JrTexto::_('Create enterprise'),true);
			$this->esquema = 'bolsatrabajo/crear_empresa';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'bolsatrabajo/general2';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
			
		}
	} 

	public function postularme(){
		try{
			global $aplicacion;	
			$filtro=array();
			@extract($_REQUEST);
			if(!empty($texto))$filtro['buscar']=$texto;
			if(!empty($_REQUEST["idpublicacion"])){
				$idpublicacion=$_REQUEST["idpublicacion"];
				$publicaciones=$this->oNegPublicaciones->buscar(array('idpublicacion'=>$idpublicacion));
				if(!empty($publicaciones[0])){
					$this->publicaciones=$publicaciones[0];
				}
			}
			//$filtro['mostrar']=1;
			//$this->publicaciones=$this->oNegPublicaciones->buscar($filtro);	
			$this->esquema = 'postularme';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'bolsatrabajo/general2';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}