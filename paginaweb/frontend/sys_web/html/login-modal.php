<?php 
defined('RUTA_BASE') or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if($ismodal){ ?>
<span style="float: right; color: #777; font-size: 14px;" class="close cerrarmodal" data-dismiss="modal"><i class="fa fa-close"></i></span>
<?php } ?>
<form method="post" id="frmlogin-<?php echo $idgui;?>"  target="" style="padding: 1em 1em 1ex 1em">
    <div class="row"> 
        <div class="col-12 text-center">
            <h1><?php echo JrTexto::_('Iniciar Sesion');?></h1>
            <hr>
        </div>
        <div class="col-12 text-center" hidden="true" id="alert<?php echo $idgui; ?>">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong class="titulo"></strong> <span class="mensaje"></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        </div>
        <div class="col-12 form-group">
            <label style=""><?php echo JrTexto::_('Usuario or email');?></label>              
            <div class="input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span></div>
                <input type="text" name="usuario" required="required"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("User or email"))?>">                              
            </div>
        </div>
        <div class="col-12 form-group">
            <label style=""><?php echo JrTexto::_('Contraseña');?></label>              
            <div class="input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-key"></i></span></div>
                <input type="password" autocomplete="false" name="clave"  required="required" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("contraseña"))?>">                              
            </div>
        </div>
        <div class="col-12 form-group text-center"><br>
            <button type="submit" class="btn btn-primary"><i class="fa fa-user"></i><i class="fa fa-key"></i> <?php echo JrTexto::_('Iniciar Sesion')?></button>
            <button type="button" class="btn btn-warning btncancel<?php echo $idgui; ?>"><i class="fa fa-refresh"></i> <?php echo JrTexto::_('Cancelar')?></button><br>
            <a href="javascript:void(0)" class="ira<?php echo $idgui; ?>">Recuperar Contraseña</a>
        </div> 
        <div class="col-12 text-center">
            <small><?php echo date('Y')." ".JrTexto::_('Todos los derechos reservados'); ?> </small>
        </div>
    </div>
</form>
<form method="post" id="frmrecuperarusuario-<?php echo $idgui;?>" hidden="true" target="" style="padding: 1em 1em 1ex 1em" >
    <div class="row"> 
        <div class="col-12 text-center">
            <h1><?php echo JrTexto::_('Recuperar Contraseña');?></h1>
            <hr>
        </div>
        <div class="col-12 form-group">
            <label style=""><?php echo JrTexto::_('Correo de usuario');?></label>              
            <div class="input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span></div>
                <input type="email" name="email"  required="required" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Correo"))?>">                              
            </div>
        </div>       
        <div class="col-12 form-group text-center"><br>
            <button type="submit" class="btn btn-primary"><i class="fa fa-key"></i> <?php echo JrTexto::_('Recuperar contraseña')?></button>
            <button type="button" class="btn btn-warning btncancel<?php echo $idgui; ?>"><i class="fa fa-refresh"></i> <?php echo JrTexto::_('Cancelar')?></button><br>
            <a href="javascript:void(0)" class="ira<?php echo $idgui; ?>">Regresar a iniciar sesion</a>
        </div> 
        <div class="col-12 text-center">
            <small><?php echo date('Y')." ".JrTexto::_('Todos los derechos reservados'); ?> </small>
        </div>
    </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
    <?php if($ismodal){?>
        $('.btncancel<?php echo $idgui; ?>').click(function(ev){
            $(this).closest('.modal').modal('hide');
        });
    <?php } ?>
    $('a.ira<?php echo $idgui;?>').click(function(){
        var frm=$(this).closest('form');
        var frm2=frm.siblings('form')
        frm.animateCss('zoomOut',function(){
         frm.attr('hidden',true);        
         frm2.removeAttr('hidden').animateCss('zoomIn');
        })
    })
    $('#frmlogin-<?php echo $idgui;?>').bind({
        submit:function(ev){
            ev.preventDefault();
            var tmpfrm=document.getElementById('frmlogin-<?php echo $idgui;?>');  
            var formData = new FormData(tmpfrm);            
            var _sysajax={
                fromdata:formData,
                showmsjok:true,
                url:_sysUrlBase_+'/sesion/loginempresa',
                callback:function(rs){
                    var alert=$('#alert<?php echo $idgui; ?>');                    
                    alert.find('.titulo').html('');
                    alert.find('.mensaje').html(rs.msj);
                    alert.find('.alert').removeClass('alert-danger').addClass('alert-success');
                    alert.removeAttr('hidden');                    
                    setTimeout(function(){location.reload()},700);
                    return false;
                },callbackerror:function(rs){
                    var alert=$('#alert<?php echo $idgui; ?>');
                    alert.find('.titulo').html('Error');
                    alert.find('.mensaje').html(rs.msj);                    
                    alert.find('alert').removeClass('alert-success').addClass('alert-danger');
                    alert.removeAttr('hidden');
                }
            }
            sysajax(_sysajax);
            return false;
        }
    })
    $('#frmrecuperarusuario-<?php echo $idgui;?>').bind({
        submit:function(ev){
            ev.preventDefault();
                var tmpfrm=document.getElementById('frmrecuperarusuario-<?php echo $idgui;?>');  
                var formData = new FormData(tmpfrm);            
                var _sysajax={
                    fromdata:formData,
                    showmsjok:true,
                    url:_sysUrlBase_+'/sesion/recuperarclaveempresa',                 
                    callback:function(rs){
                       setTimeout(function(){location.reload()},500);
                    },
                    callbackerror:function(rs){
                        var alert2=$('#alert<?php echo $idgui; ?>');
                        alert.find('titulo').html('Error');
                        alert.find('mensaje').html(rs.msj);
                        alert.removeClass('alert-success').addClass('alert-danger');
                    }
                }
                sysajax(_sysajax);
                return false;
        }
    })
});
</script>