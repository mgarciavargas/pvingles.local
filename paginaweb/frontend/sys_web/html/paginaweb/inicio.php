<style type="text/css">
	.imagen2{
		background-image: url(<?php echo $this->documento->getUrlStatic(); ?>/media/web/imagen2.jpg);
    margin: 1em auto;
    min-height: 400px;
    position: relative;
	}
  .infoslider2{
    top:calc(50% - 50px);
  }
  .tituloh3{
    font-size: 25.888px;
    line-height: 32px;
    letter-spacing: -0.020em;
  }
  .txtcolornegro{ color:#555; }

  .info_22{
      color: #666666;
      width: 336px;
      font-size: 1.5rem;
      font-family: dinpro-regular; 
      padding: 1ex 3rem;   
  }
  .info2{
     padding:1em 1em 0ex 1em;
  }

  .img-circle{border-radius: 50%;}
  .imagen-fondo{ position: relative; }
  .imagen-fondo .texto-sobre{
    position: absolute;
    top:calc(50% - 30px);
    width: 100%;
  }

   .padding1ex{ padding: 10px 1ex; !important; }
    .card.vercurso{ width: 100%; cursor: pointer; }
    .card_img{display: block;  position: relative;}
    .card_img>img{ width: 100%; opacity: 1; -webkit-filter: sepia(.1) grayscale(.1) saturate(.8);  filter: sepia(.1) grayscale(.1) saturate(.8); display: block;
    height: 122px; overflow: hidden;  position: relative;}   
    .card:hover{outline: -webkit-focus-ring-color auto 5px;}
    .card_body{ padding: 1ex; position: relative; }
    .card .card_body > .card_acciones{ display: none; }
    .card:hover .card_body > .card_acciones {display: block;} 
    .card_acciones{  text-align: center;  position: absolute; width: 100%; bottom: 0px;  background: #9c9a9a5c;  margin: 0px -1ex;  padding: 0.5ex;  }
    .card_acciones.top{ bottom: auto; top: 0px;  }
    .card_title , .card_texto{ overflow: hidden; display: -webkit-box!important; text-overflow: ellipsis; white-space: normal; font-weight: 600; height: 36px; min-height: 42px;  font-size: 15px;  color: #29303b;  margin-bottom: 10px; -webkit-line-clamp: 2; line-clamp: 2; -webkit-box-orient: vertical; }    
    .card_texto{ font-size: 13px;   color: #686f7a}
    .slick-slider{margin-bottom: 0px;}
    .txtleftsobre{
          letter-spacing: -0.020em;
          font-family: dinpro-bold;
          font-size: 22.428px;
          line-height: 32px;
          padding-left: 2em;
          color: #fff;
          font-weight: bold;
    }
    .txtbottomsobre{     
      top: auto !important;      
      bottom: 60px;     
      color: #fff;
      font-weight: bold;
    }

</style>
<div class="col-12">
  <div class="container ">
    <div class="row">
      <div class="col-12 text-center info2 txtcolornegro" >
        <b>Desarrolla tu potencial</b>
      </div>
      <div class="col-12 text-center info_22" >
        Tú eliges lo que quieres alcanzar. Construye con nosotros tu camino profesional y obtén mejores oportunidades de trabajo. 
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12   " style="padding: 0.85em;" >
        <div class="imagen-fondo hvr-grow-shadow"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/desarrollatupersonal1.jpg" class="img-responsive img img-thumbnail">
        <div class="texto-sobre text-center txtbottomsobre">Cursos de Ofimatica</div></div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12  " style="padding: 0.85em;" >
        <div class="imagen-fondo hvr-grow-shadow"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/desarrollatupersonal2.jpg" class="img-responsive img img-thumbnail">
        <div class="texto-sobre text-center txtbottomsobre">cursos de Ingles</div></div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12  " style="padding: 0.85em;" >
        <div class="imagen-fondo hvr-grow-shadow"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/desarrollatupersonal3.jpg" class="img-responsive img img-thumbnail">
        <div class="texto-sobre text-center txtbottomsobre">Cursos de Diseño</div></div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12  " style="padding: 0.85em;" >
        <div class="imagen-fondo hvr-grow-shadow"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/desarrollatupersonal4.jpg" class="img-responsive img img-thumbnail">
        <div class="texto-sobre text-center txtbottomsobre">Cursos Tics</div></div>
      </div>
    </div>
  </div>
</div>
<div class="col-12">
  <div class="container ">
    <div class="row">
      <div class="col-12 text-center info2 txtcolornegro" >
        <b>Siempre hay algo más por aprender</b>
      </div>
      <div class="col-12 text-center info_22" >
        Contamos con +500 cursos en diferentes categorías. <br>
        Domina una nueva habilidad, técnica o software en demanda.
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-12 text-center" style="padding: 0.8em;" >
        <div class="imagen-fondo hvr-float-shadow"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/aprende1.png" class="img-responsive img img-thumbnail"><div class="texto-sobre text-left txtleftsobre">Curso 01</div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 text-center" style="padding: 0.8em;" >
        <div class="imagen-fondo hvr-float-shadow"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/aprende2.png" class="img-responsive img img-thumbnail"><div class="texto-sobre text-left txtleftsobre">Curso 02</div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 text-center" style="padding: 0.8em;" >
        <div class="imagen-fondo hvr-float-shadow"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/aprende3.png" class="img-responsive img img-thumbnail"><div class="texto-sobre text-left txtleftsobre">Curso 03</div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 text-center" style="padding: 0.8em;" >
        <div class="imagen-fondo hvr-float-shadow"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/aprende4.png" class="img-responsive img img-thumbnail"><div class="texto-sobre text-left txtleftsobre">Curso 04</div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 text-center" style="padding: 0.8em;" >
        <div class="imagen-fondo hvr-float-shadow"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/aprende5.png" class="img-responsive img img-thumbnail"><div class="texto-sobre text-left txtleftsobre">Curso 05</div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 text-center" style="padding: 0.8em;" >
        <div class="imagen-fondo hvr-float-shadow"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/aprende6.png" class="img-responsive img img-thumbnail"><div class="texto-sobre text-left txtleftsobre">Curso 06</div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-12">
  <div class="container ">
    <div class="row">
      <div class="col-12 text-center info2 txtcolornegro" >
        <b>Descubre cursos desarrollados por expertos en la industria</b>
      </div>     
    </div>
    <div class="row">
      <div class="col-12" id="cursoslistado" >
        
      </div>
    </div>
  </div>
</div>
<div class="col-12">
  <div class="container ">
    <div class="row">
      <div class="col-12 text-center info2 txtcolornegro" >
        <b>Vive la experiencia</b>
      </div>
      <div class="col-12 text-center info_22" >
        Una forma de aprender diferente que se adapta a tu estilo de vida.
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12 text-center hvr-float-shadow" >
        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/viveexperiencia1.jpg" class="img-responsive img img-thumbnail img-circle">
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 d-flex align-items-center" >        
        <p class="textoinfo col-12"><strong class="titulo"> Aprende a tu propio ritmo</strong><br>Aprender online es aprender donde quieras y como quieras. Te ofrecemos cursos sin horarios, sin límite de reproducciones o periodo de caducidad.</p>
      </div>
    </div>
    <div class="row">      
      <div class="d-none d-sm-flex col-md-6 col-sm-6 col-xs-12 align-items-center" >   
        <p class="textoinfo col-12"><strong> Comunidad de oportunidades</strong><br> Al final del curso, construirás un proyecto personal que te ayudará a ampliar tu portafolio y conseguir mejores oportunidades de trabajo.</p>     
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 text-center hvr-float-shadow" >
        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/viveexperiencia2.jpg" class="img-responsive img img-thumbnail img-circle">
      </div>
      <div class="d-flex d-sm-none col-md-6 col-sm-6 col-xs-12  align-items-center" >   
        <p class="textoinfo col-12"><strong> Comunidad de oportunidades</strong><br> Al final del curso, construirás un proyecto personal que te ayudará a ampliar tu portafolio y conseguir mejores oportunidades de trabajo.</p>     
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12 text-center hvr-float-shadow">
        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/viveexperiencia3.jpg" class="img-responsive img img-thumbnail img-circle">
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 d-flex align-items-center" >        
        <p class="textoinfo col-12"><strong class="titulo"> Certificado oficial</strong><br>Cada curso, certificación y carrera que termines, está avalado oficialmente por Crehana. Tu crecimiento profesional es nuestro compromiso.</p>
      </div>
    </div>
  </div>
</div>
<!--div class="col-12">
  <div class="container ">
    <div class="row">
      <div class="col-12 text-center info2 txtcolornegro" >
        <b>Construye proyectos reales</b>
      </div>
      <div class="col-12 text-center info_22" >
        Estamos convencidos de que la mejor forma de aprender es haciendo. Por eso, construye proyectos increíbles al terminar tus cursos y compártelos con la comunidad.
      </div>
    </div>
    <div class="row">
      <div class="col-12 slider" >
        
      </div>
    </div>
  </div>
</div-->
<!--div class="col-12">
  <div class="container ">
    <div class="row">
      <div class="col-12 text-center info2 txtcolornegro" >
        <b>Expertos en la industria que guiarán tu aprendizaje.</b>
      </div>
    </div>
    <div class="row">
      <div class="col-12">        
      </div>
    </div>
  </div>
</div-->

<div class="col-12">
  <div class="container ">
    <div class="row">
      <div class="col-12 text-center info2 txtcolornegro" >
        <b>Hemos potenciado el talento de más de 400,000 estudiantes de 25 países en el mundo. </b>
      </div>
    </div>
    <div class="row" style="padding:1em 0px 3em 0px">
      <div class="col-12" style="background-color: #fff">
        <div id="slick-itemssingle">
          <div class="slick-item" >
            <div class="row">
              <div class="col-6" >
                <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/testimonio1.jpg" class="img-responsive img img-thumbnail hvr-grow-shadow">
              </div>
              <div class="col-6 d-flex align-items-center" >  
                  <p class="textoinfo col-12">
                    Desde el primer día he notado un impacto en mi vida profesional, pues siento que me han brindado más conocimientos que mi propia Universidad.
                    <br><br>
                    <strong class="titulo"> Pedro Flores</strong>
                  </p>
              </div>
            </div>
          </div>
          <div class="slick-item" >
            <div class="row">
              <div class="col-6" >
                <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/testimonio2.jpg" class="img-responsive img img-thumbnail hvr-grow-shadow">
              </div>
              <div class="col-6 d-flex align-items-center" >        
                <p class="textoinfo col-12">
                 Antes de empezar los cursos, tenía muchas dudas sobre en que realmente me quería enfocarme y Ahora he aprendido cosas que la Universidad no enseñan muy bien y he iniciado el desarrollo de mi portafolio"<br><br>
                  <strong class="titulo"> Juan Cabanillas</strong>
                </p>
              </div>
            </div>
          </div>
          <div class="slick-item" >
            <div class="row">
              <div class="col-6" >
                <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/testimonio3.jpg" class="img-responsive img img-thumbnail hvr-grow-shadow">
              </div>
              <div class="col-6 d-flex align-items-center" >        
                <p class="textoinfo col-12">
                   La gran parte de los conocimientos que utilizo para el mundo laboral, los he adquirido con ustedes. Estoy muy satisfecho con mis cursos"<br><br>
                   <strong class="titulo"> Jhon Kenedy</strong></p>
              </div>
            </div>
          </div>
          <div class="slick-item" >
            <div class="row">
              <div class="col-6" >
                <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/web/testimonio4.jpg" class="img-responsive img img-thumbnail hvr-grow-shadow">
              </div>
              <div class="col-6 d-flex align-items-center" >        
                <p class="textoinfo col-12">
                  A los 6 meses de aprender, teniendo constancia con los cursos, me di cuenta que manejaba varios cursos nuevos Además, estoy inspirado a seguir aprendiendo en áreas novedosas y desconocidas para mí."<br><br>
                  <strong class="titulo"> Bertha Diaz</strong>
                </p>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="col-12" >
  <div class="container imagen2" >    
  		<div class="col-12 text-center infoslider infoslider2 img-tunhails">
        <div class="infocontent">           
          <span class="info3"><b>Únete a nuestra comunidad y vive una experiencia de aprendizaje diferente</b></span><br>           
          <span><button class="btn btnunete">Unete Gratis</button></span><br>
        </div>
      </div>    	
  </div>
 </div>

<script type="text/javascript">
  var categorias={};
  var rutaRaiz='<?php echo URL_RAIZ; ?>'
  $(document).ready(function(ev){
    __sysajax({
        formdata:{},
        showmsjok:false,
        url:rutaRaiz+'smartcourse/cursos/jsonbuscarxcategoria',
        callback:function(rs){
          let code=parseInt(rs.code);
           $ul=$('ul.showcategorias');
           $piecursos=$('#showbloklist').html('');
           $topcursos=$('#showcursos').html('');
           $cursoslistado=$('#cursoslistado').html('');
           if(code==200){
            dt=rs.data;
            let cursostop='';
            let cursoshtml='';
            $.each(dt,function(i,v){            
              let li='<li><a href="javascript:void(0)">'+v.nombre+'</a></li>';
              $ul.append(li);
              let liscur=v.hijos;             
              let cursos='<div class="grid"><strong class="titulopie">'+v.nombre+'</strong>';
              let jcon=0;
              let nhijos=Object.keys(v.hijos).length;
              console.log(nhijos);
              //cursos por categorias
              if(nhijos>4){
              cursoshtml+='<div class="panel mostrarcollapse activeshow" style="border: 1px solid #dad7d7; margin-bottom:1.5ex; "><div class="panel-heading bg-primary"><h3 class="panel-title">Los mejores cursos de: '+v.nombre+' <span class="badge badge-light">'+nhijos+'</span></h3></div><div class="panel-body"><div class="row"><div class="col-md-12"><div class="slick-items">';
              } else cursoshtml+='';
              $.each(liscur,function(j,h){ 
                if(nhijos>4){
                cursoshtml+='<div class="slick-item hvr-float-shadow" style="padding: 0.25ex;" ><div style="margin: 0.25ex 0.5ex; border: 1px solid #f00; box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);" ><div class="card vercurso " data-idcurso="'+h.idcurso+'"><img  src="'+h.imagen+'" class="img img-responsive" style="max-height: 230px;"></div> <div class="card_body"><div class="card_title text-center">';
                cursoshtml+=h.nombre+'</div><div class="card_texto">'+h.descripcion+'</div>';
                cursoshtml+='</div></div></div>';
                } else cursoshtml+='';
                cursostop+='<div class="col-md-4 col-sm-6 col-xs-12">'+h.nombre+'</div>';
                if(jcon<13){
                 cursos+='<p>'+h.nombre+'</p>';
                }else if(jcon==13){
                  cursos+='<p>Ver Mas</p>';
                }
                jcon++;
              });
              cursos+='</div>';
              if(nhijos>4){  cursoshtml+='</div></div></div></div></div>';
               } else cursoshtml+='';
              $piecursos.append(cursos); 
            })              
            $topcursos.append(cursostop);
            $cursoslistado.append(cursoshtml);
            var slikitems=$('.slick-items').slick(optslike());
          }
           $('#showbloklist').BlocksIt({ numOfCol: 4, offsetX: 6, offsetY: 6 });
        }
      })

      $('#cursoslistado').on('click','.panel.mostrarcollapse .panel-title',function(ev){
          ev.preventDefault();           
          let ac=$(this).closest('.panel.mostrarcollapse');
          ac.toggleClass('activeshow');
          if(!ac.hasClass('activeshow')) ac.children('.panel-body').fadeOut('fast');
          else ac.children('.panel-body').fadeIn('fast');
           ev.stopPropagation();
      });
      $('#slick-itemssingle').slick({dots: true, infinite: true,speed: 300});

     
    })
</script>