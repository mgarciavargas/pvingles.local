<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$datoslistadoheader='';
$datoslistadoheaderajax='';
$datoslistadoheaderajax2='';
$datosrefh='';
$datoslistado='';
$datoslistadoajax='';
$addjschk=false;
$jsfile=false;
$idguisys=uniqid();
?>
<?php
echo '<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
';
?>
<?php echo '<?php if(!$ismodal){?>';?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo '<?php echo $this->documento->getUrlBase();?>';?>"><i class="fa fa-home"></i>&nbsp;<?php echo '<?php echo JrTexto::_("Home"); ?>';?></a></li>
        <li><a href="<?php echo '<?php echo $this->documento->getUrlBase();?>';?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo '<?php echo JrTexto::_("Academic"); ?>';?></a></li>
        <li class="active">&nbsp;<?php echo '<?php echo JrTexto::_("'.$tb.'"); ?>';?></li>       
    </ol>
</div> </div>
<?php echo '<?php } ?>';?>

<div class="form-view" id="ventana_<?php echo '<?php echo $idgui; ?>';?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
            <?php if(!empty($campos))
            foreach ($campos as $campo){ ?>
              <?php
              $tipo=@$frm['tipo_'.$campo];                      
              if($tipo=='fk'){
              if(@$frm["tipofkcomo_".$campo]=='combobox'){?>

            <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
              <select id="fkcb<?php echo $campo ?>" name="fkcb<?php echo $campo ?>" class="form-control select-ctrl" >
                <option value=""><?php echo "<?php echo JrTexto::_('Seleccione'); ?>"; ?></option>
                  <?php echo '<?php 
                          if(!empty($this->fk'.$campo.'))
                            foreach ($this->fk'.$campo.' as $fk'.$campo.') { ?>';
                        echo '<option value="<?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]?>" <?php echo $fk'.$campo.'["'.$frm["tipofkid_".$campo].'"]==@$frm["'.$campo.'"]?"selected":""; ?> ><?php echo $fk'.$campo.'["'.@$frm["tipofkver_".$campo].'"] ?></option>';
                        echo '<?php } ?>'; ?>
                        
              </select>
            </div>
            <?php $datoslistadoheaderajax.=' d.'.$campo.'=$(\'#fkcb'.$campo.'\').val(),
            ';
  $datosrefh.='
  $(\'#fkcb'.$campo.'\').change(function(ev){
    refreshdatos'.$idguisys.'();
  });';
              } /////////////////////////////////////////////////////
              elseif($frm["tipofkcomo_".$campo]=='radiobutton'){?>
            <div class="col-xs-6 col-sm-4 col-md-3">
              <?php  echo '
                <?php 
                if(!empty($this->fk'.$campo.'))
                  foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                   ?>';
              ?>
                <input type="radio" id="rb<?php echo $campo ?>" name="rb<?php echo $campo ?>" >
              <?php echo ' <?php } ?>';?>
            </div>
            <?php $datoslistadoheaderajax.=' d.'.$campo.'=$(\'#fkrb'.$campo.'\').val(),
            ';
$datosrefh.='
  $(\'#fkrb'.$campo.'\').change(function(ev){
    refreshdatos'.$idguisys.'();
  });';
              }/////////////////////////////////////////////////
              elseif($frm["tipofkcomo_".$campo]=='checkbox'){?>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <?php echo '
                  <?php 
                  if(!empty($this->fk'.$campo.'))
                    foreach ($this->fk'.$campo.' as $fk'.$campo.') {
                     ?>';
                ?>
                  <input type="checkbox" id="ck<?php echo $campo ?>" name="ck<?php echo $campo ?>" class="form-control">
                <?php echo ' <?php } ?>';?>
            </div>                          
            <?php $datoslistadoheaderajax.=' d.'.$campo.'=$(\'#fkck'.$campo.'\').val(),
            ';
$datosrefh.='
  $(\'#fkck'.$campo.'\').change(function(ev){
    refreshdatos'.$idguisys.'();
  });';
              } ///////////////////////////////////////////////// terminca chekbox
              } /////////////////////////////////////////////////
              else{
                if($campo!=$campopk)
                  if($tipo=='date'||$tipo=='datetime'||$tipo=='time'){?>
                    <div class="col-xs-6 col-sm-4 col-md-3">
                      <div class="form-group">    
                          <div class='input-group date datetimepicker1 border1' >
                            <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo '<?php echo  ucfirst(JrTexto::_("'.$campo.'"))?>';?> </span>
                            <input type='text' class="form-control border0" name="date<?php echo $campo; ?>" id="date<?php echo $campo; ?>" />           
                          </div>
                      </div>
                    </div>
                <?php  $datoslistadoheaderajax.=' d.'.$campo.'=$(\'#date'.$campo.'\').val(),
            ';
$datosrefh.='
  $(\'#date'.$campo.'\').change(function(ev){
    refreshdatos'.$idguisys.'();
  });';
                  } /////////////////////////////////////////////////
                  elseif($tipo=='combobox'||$tipo=='radiobutton'){?>                          
                    <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
                      <select name="cb<?php echo $campo; ?>" id="cb<?php echo $campo; ?>" class="form-control select-ctrl">
                          <option value=""><?php echo '<?php echo ucfirst(JrTexto::_("All state"))';?>?></option>
                          <option value="1"><?php echo '<?php echo ucfirst(JrTexto::_("Active"))';?>?></option>
                          <option value="0"><?php echo '<?php echo ucfirst(JrTexto::_("Inactive"))';?>?></option>                              
                      </select>
                    </div>
                  <?php $datoslistadoheaderajax.=' d.'.$campo.'=$(\'#cb'.$campo.'\').val(),
            ';
$datosrefh.='
  $(\'#cb'.$campo.'\').change(function(ev){
    refreshdatos'.$idguisys.'();
  });';
                  }                        
              }
            } ?>              
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo '<?php echo  ucfirst(JrTexto::_("text to search"))?>';?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo '<?php echo  ucfirst(JrTexto::_("Search"))?>';?> <i class="fa fa-search"></i></span>  
                    <?php $datoslistadoheaderajax.=' //d.texto=$(\'#texto\').val(),
            ';
 $datosrefh.='
  $(\'.btnbuscar\').click(function(ev){
    refreshdatos'.$idguisys.'();
  });';?>
                </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo '<?php echo JrAplicacion::getJrUrl(array("'.$tb.'", "agregar"));?>';?>" data-titulo="<?php echo '<?php echo JrTexto::_("'.$tb.'").\' - \'.JrTexto::_("add"); ?>'; ?>"><i class="fa fa-plus"></i> <?php echo "<?php echo JrTexto::_('add')?>" ?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <?php if(!empty($campos))
                    foreach ($campos as $campo){
                      $verenlistado='_chk'.$campo;
                      $tipo=@$frm['tipo_'.$campo];
                      if($tipo=='file') $jsfile=true;
                      if($campo!=$campopk&&!empty($frm[$verenlistado])){
                        if($tipo=='fk'){
                      $datoslistadoheader.='<th><?php echo JrTexto::_("'.ucfirst(substr(str_replace('_', ' ', $frm["tipo2_".$campo]),4)).'")."(".JrTexto::_("'.ucfirst(str_replace('_', ' ', $frm["tipofkver_".$campo])).'").")"; ?></th>
                    ';
                      $datoslistado.='<td><?php echo !empty($reg["_'.$frm["tipofkver_".$campo].'"])?$reg["_'.$frm["tipofkver_".$campo].'"]:null; ?></td>
                    ';
                    $datoslistadoajax.='\'<?php echo JrTexto::_("'.$frm["tipofkver_".$campo].'") ;?>\': data[i].'.$frm["tipofkver_".$campo].',
                    ';
            $datoslistadoheaderajax2.='{\'data\': \'<?php echo JrTexto::_("'.ucfirst($frm["tipofkver_".$campo]).'") ;?>\'},
            ';
                  }else{
                      $datoslistadoheader.='<th><?php echo JrTexto::_("'.ucfirst(str_replace('_', ' ', $campo)).'") ;?></th>
                    ';
            $datoslistadoheaderajax2.='{\'data\': \'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\'},
            ';
                      if(($tipo=='textArea'||$tipo=='text')&&$frm["maximocaracteres".$campo]>100){
                      $datoslistado.='<td><?php echo substr($reg["'.$campo.'"],0,100)."..."; ?></td>
                    ';
                  $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': data[i].'.$campo.',
                ';
                       }elseif($tipo=='int'){
                      $datoslistado.='<td class="text-right"><?php echo $reg["'.$campo.'"] ;?></td>
                    ';
                  $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': data[i].'.$campo.',
                ';
                        }elseif($tipo=='decimal'){
                      $datoslistado.='<td class="text-right"><?php echo number_format($reg["'.$campo.'"],'.$frm["partedecimal".$campo].',"."," "); ?></td>
                    ';
                  $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': data[i].'.$campo.',
                ';
                        }elseif($tipo=='money'){
                      $datoslistado.='<td class="text-right"><span style="float:left">$.</span><?php echo number_format($reg["'.$campo.'"],'.$frm["partedecimal".$campo].',"."," "); ?></td>
                    ';
                  $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': data[i].'.$campo.',
                ';
                        }elseif($tipo=='file'){
                          $tipofile=$frm["tipo2_".$campo];
                          if($tipofile=='imagen'){
                      $datoslistado.='<td class="text-center"><img src="<?php echo $this->documento->getUrlBase().$reg["'.$campo.'"]; ?>" class="img-thumbnail" style="max-height:70px; max-width:50px;"></td>
                    ';
                  $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': \'<img src="\'+data[i].'.$campo.'+\' class="img-thumbnail" style="max-height:70px; max-width:50px;">\',
                ';
                          }elseif($tipofile=='video'){
                      $datoslistado.='<td class="text-center"><a href="<?php echo $reg["'.$campo.'"]; ?>" target="_blank"><?php echo JrTexto::_("ver video"); ?></a></td>
                    ';
                  $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': \'<a href="\'+data[i].'.$campo.'+\' target="_blank"> <?php echo JrTexto::_("ver video"); ?></a>\',
                ';
                          }elseif($tipofile=='documentos'){
                      $datoslistado.='<td class="text-center"><a href="<?php echo $reg["'.$campo.'"]; ?>" target="_blank"><?php echo JrTexto::_("ver documento"); ?></a></td>
                  ';
                $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': \'<a href="\'+data[i].'.$campo.'+\' target="_blank"> <?php echo JrTexto::_("ver documento"); ?></a>\',
                ';
                          }else{
                      $datoslistado.='<td class="text-center"><a href="<?php echo $reg["'.$campo.'"]; ?>" target="_blank"><?php echo JrTexto::_("ver"); ?></a></td>
                    '; 
                    $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': \'<a href="\'+data[i].'.$campo.'+\' target="_blank"> <?php echo JrTexto::_("ver"); ?></a>\',
                ';
                          }
                        }elseif($tipo=='combobox'||$tipo=='radiobutton'||$tipo=='checkbox'){
                          $addjschk=true;
                          if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){
                             if($frm["dattype".$campo]=="enum"){                              
                              $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1));
                              $datoslistado.='<td class="text-center">
                              <a style="cursor:pointer;" class="chklistado fa  <?php echo $reg["'.$campo.'"]=='.$valoresopt[0].'?"fa-check-circle":"fa-circle-o"; ?> " data-value="<?php echo $reg["'.$campo.'"]; ?>"
                        data-campo="'.$campo.'" data-txtid="<?php echo $reg["'.$campopk.'"]; ?>" data-valueno='.$valoresopt[0].' data-value2="<?php echo $reg["'.$campo.'"]=='.$valoresopt[0].'?'.$valoresopt[1].':'.$valoresopt[0].';?>" >
                        <?php echo $reg["'.$campo.'"]=='.$valoresopt[0].'?JrTexto::_('.$valoresopt[0].'):JrTexto::_('.$valoresopt[1].'); ?></a></td>
                    ';
                
                $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': \'<a style="cursor:pointer;" class="chklistado fa <?php echo $reg["'.$campo.'"]=='.$valoresopt[0].'?"fa-check-circle":"fa-circle-o"; ?> " href="\'+data[i].'.$campo.'+\' target="_blank"> <?php echo JrTexto::_("ver documento"); ?></a>\',
              ';
                             }else{
                              $datoslistado.='<td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="'.$campo.'"  data-id="<?php echo $reg["'.$campopk.'"]; ?>"> <i class="fa fa<?php echo !empty($reg["'.$campo.'"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["'.$campo.'"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    ';
                $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': \'<a href="javascript:;"  class="btn-chkoption" campo="'.$campo.'"  data-id="\'+data[i].'.$campopk.'+\'"> <i class="fa fa\'+(data[i].'.$campo.'==\'1\'?\'-check\':\'\')+\'-circle-o fa-lg"></i> \'+estados'.$idguisys.'[data[i].'.$campo.']+\'</a>\',
              ';
                             }
                          }else{
                        $datoslistado.='<td><?php echo $reg["'.$campo.'"] ;?></td>
                    ';
                  $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': data[i].'.$campo.',
              ';
                          }
                        }else{
                        $datoslistado.='<td><?php echo $reg["'.$campo.'"] ;?></td>
                    ';
                $datoslistadoajax.='\'<?php echo JrTexto::_("'.ucfirst($campo).'") ;?>\': data[i].'.$campo.',
              ';
                          }
                            }
                        }
                      }                      
                echo $datoslistadoheader;
              ?><th class="sorting_disabled"><span class="nobr"><?php echo "<?php echo JrTexto::_('Actions');?>"; ?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php //even pointer, odd pointer 
               /* echo '<?php $i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ $i++; ?>
                ';?><tr>
                  <td><?php echo '<?php echo $i;?>'; ?></td>
                  <?php echo $datoslistado;  ?><td><a class="btn btn-xs lis_ver " href="<?php echo '<?php echo JrAplicacion::getJrUrl(array(\''.$this->tabla.'\'))?>'; ?>ver/?id=<?php echo '<?php echo $reg["'.$campopk.'"]; ?>' ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update btnvermodal" data-modal='si' href="<?php echo '<?php echo JrAplicacion::getJrUrl(array("'.$this->tabla.'", "editar", "id=" . $reg["'.$campopk.'"]))?>';?>"  data-titulo="<?php echo '<?php echo JrTexto::_("'.ucfirst($this->tabla).'").\' - \'.JrTexto::_("edit"); ?>'; ?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo '<?php echo $reg["'.$campopk.'"]; ?>'?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php echo '<?php } ?>
          ';*/ ?>
          </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos<?php echo $idguisys ?>='';
function refreshdatos<?php echo $idguisys ?>(){
    tabledatos<?php echo $idguisys ?>.ajax.reload();
}
$(document).ready(function(){  
  var estados<?php echo $idguisys ?>={'1':'<?php echo '<?php echo JrTexto::_("Active") ?>'; ?>','0':'<?php echo '<?php echo JrTexto::_("Inactive") ?>';?>','C':'<?php echo '<?php echo JrTexto::_("Cancelled") ?>';?>'}
  var tituloedit<?php echo $idguisys ?>='<?php echo '<?php echo ucfirst(JrTexto::_("'.$this->tabla.'"))." - ".JrTexto::_("edit"); ?>';?>';
  var draw<?php echo $idguisys ?>=0;

  <?php echo  $datosrefh; ?>

  tabledatos<?php echo $idguisys ?>=$('#ventana_<?php echo '<?php echo $idgui; ?>';?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        <?php  echo $datoslistadoheaderajax2; ?>

        {'data': '<?php echo '<?php echo JrTexto::_("Actions") ;?>'?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/<?php echo $this->tabla; ?>/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
            <?php echo $datoslistadoheaderajax; ?>
            
            draw<?php echo $idguisys ?>=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw<?php echo $idguisys ?>;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              <?php echo $datoslistadoajax; ?>

              '<?php echo '<?php echo JrTexto::_("Actions") ;?>'?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/<?php echo $this->tabla; ?>/editar/?id='+data[i].<?php echo $campopk; ?>+'" data-titulo="'+tituloedit<?php echo $idguisys ?>+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].<?php echo $campopk; ?>+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo '<?php echo $this->documento->getIdioma()!=\'EN\'?(\',"language": { "url": "\'.$this->documento->getUrlStatic().\'/libs/datatable1.10/idiomas/\'.$this->documento->getIdioma().\'.json"}\'):\'\'?>';?>

    });

  $('#ventana_<?php echo '<?php echo $idgui; ?>';?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo "<?php echo JrTexto::_('Confirm action');?>";?>',
        content: '<?php echo "<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>";?>',
        confirmButton: '<?php echo "<?php echo JrTexto::_('Accept');?>";?>',
        cancelButton: '<?php echo "<?php echo JrTexto::_('Cancel');?>";?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', '<?php echo $this->tabla ?>', 'setCampo', id,campo,data);
          if(res) tabledatos<?php echo $idguisys ?>.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo '<?php echo $idgui; ?>';?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos<?php echo $idguisys ?>';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'<?php echo ucfirst($this->tabla); ?>';
    var claseid=ventana+'_<?php echo '<?php echo $idgui; ?>';?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo '<?php echo $idgui; ?>';?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo "<?php echo JrTexto::_('Confirm action');?>";?>',
      content: '<?php echo "<?php echo JrTexto::_('It is sure to delete this record ?'); ?>";?>',
      confirmButton: '<?php echo "<?php echo JrTexto::_('Accept');?>";?>',
      cancelButton: '<?php echo "<?php echo JrTexto::_('Cancel');?>";?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', '<?php echo $this->tabla?>', 'eliminar', id);
        if(res) tabledatos<?php echo $idguisys ?>.ajax.reload();
      }
    }); 
  });
});
<?php echo @$jsout; ?>
</script>