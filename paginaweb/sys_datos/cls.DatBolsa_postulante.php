<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatBolsa_postulante extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bolsa_postulante").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bolsa_postulante";			
			$cond = array();		
			if(isset($filtros["idpostular"])) {
					$cond[] = "idpostular = " . $this->oBD->escapar($filtros["idpostular"]);
			}
			if(isset($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["nombrecompleto"])) {
					$cond[] = "nombrecompleto = " . $this->oBD->escapar($filtros["nombrecompleto"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["correo"])) {
					$cond[] = "correo = " . $this->oBD->escapar($filtros["correo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			if(isset($filtros["idpublicacion"])) {
					$cond[] = "idpublicacion = " . $this->oBD->escapar($filtros["idpublicacion"]);
			}
			if(isset($filtros["idpostulante"])) {
					$cond[] = "idpostulante = " . $this->oBD->escapar($filtros["idpostulante"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bolsa_postulante").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bolsa_postulante";	
			$cond = array();			
			if(isset($filtros["idpostular"])) {
					$cond[] = "idpostular = " . $this->oBD->escapar($filtros["idpostular"]);
			}
			if(isset($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			if(isset($filtros["nombrecompleto"])) {
					$cond[] = "nombrecompleto = " . $this->oBD->escapar($filtros["nombrecompleto"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["correo"])) {
					$cond[] = "correo = " . $this->oBD->escapar($filtros["correo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}
			if(isset($filtros["idpublicacion"])) {
					$cond[] = "idpublicacion = " . $this->oBD->escapar($filtros["idpublicacion"]);
			}
			if(isset($filtros["idpostulante"])) {
					$cond[] = "idpostulante = " . $this->oBD->escapar($filtros["idpostulante"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bolsa_postulante").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($fecharegistro,$nombrecompleto,$telefono,$correo,$descripcion,$mostrar,$idpublicacion,$idpostulante)
	{
		try {
			
			$this->iniciarTransaccion('dat_bolsa_postulante_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpostular) FROM bolsa_postulante");
			++$id;
			
			$estados = array('idpostular' => $id
							
							,'fecharegistro'=>$fecharegistro
							,'nombrecompleto'=>$nombrecompleto
							,'telefono'=>$telefono
							,'correo'=>$correo
							,'descripcion'=>$descripcion
							,'mostrar'=>$mostrar
							,'idpublicacion'=>$idpublicacion
							,'idpostulante'=>$idpostulante							
							);
			
			$this->oBD->insert('bolsa_postulante', $estados);			
			$this->terminarTransaccion('dat_bolsa_postulante_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bolsa_postulante_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bolsa_postulante").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $fecharegistro,$nombrecompleto,$telefono,$correo,$descripcion,$mostrar,$idpublicacion,$idpostulante)
	{
		try {
			$this->iniciarTransaccion('dat_bolsa_postulante_update');
			$estados = array('fecharegistro'=>$fecharegistro
							,'nombrecompleto'=>$nombrecompleto
							,'telefono'=>$telefono
							,'correo'=>$correo
							,'descripcion'=>$descripcion
							,'mostrar'=>$mostrar
							,'idpublicacion'=>$idpublicacion
							,'idpostulante'=>$idpostulante								
							);
			
			$this->oBD->update('bolsa_postulante ', $estados, array('idpostular' => $id));
		    $this->terminarTransaccion('dat_bolsa_postulante_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bolsa_postulante").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bolsa_postulante  "
					. " WHERE idpostular = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bolsa_postulante").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bolsa_postulante', array('idpostular' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bolsa_postulante").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bolsa_postulante', array($propiedad => $valor), array('idpostular' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bolsa_postulante").": " . $e->getMessage());
		}
	}
   
		
}