<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatBolsa_empresas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bolsa_empresas").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bolsa_empresas";
			
			$cond = array();		
			
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["rason_social"])) {
					$cond[] = "rason_social = " . $this->oBD->escapar($filtros["rason_social"]);
			}
			if(isset($filtros["ruc"])) {
					$cond[] = "ruc = " . $this->oBD->escapar($filtros["ruc"]);
			}
			if(isset($filtros["logo"])) {
					$cond[] = "logo = " . $this->oBD->escapar($filtros["logo"]);
			}
			if(isset($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["representante"])) {
					$cond[] = "representante = " . $this->oBD->escapar($filtros["representante"]);
			}
			if(isset($filtros["usuario"])) {
					$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["clave"])) {
					$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["correo"])) {
					$cond[] = "correo = " . $this->oBD->escapar($filtros["correo"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bolsa_empresas").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bolsa_empresas";			
			
			$cond = array();		
					
			
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["rason_social"])) {
					$cond[] = "rason_social = " . $this->oBD->escapar($filtros["rason_social"]);
			}
			if(isset($filtros["ruc"])) {
					$cond[] = "ruc = " . $this->oBD->escapar($filtros["ruc"]);
			}
			if(isset($filtros["logo"])) {
					$cond[] = "logo = " . $this->oBD->escapar($filtros["logo"]);
			}
			if(isset($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["representante"])) {
					$cond[] = "representante = " . $this->oBD->escapar($filtros["representante"]);
			}
			if(isset($filtros["usuario"])) {
					$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["clave"])) {
					$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["correo"])) {
					$cond[] = "correo = " . $this->oBD->escapar($filtros["correo"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bolsa_empresas").": " . $e->getMessage());
		}
	}

	public function getxCredencial($usuario, $clave)
	{
		try {
			$sql = "SELECT * FROM bolsa_empresas "
					. " WHERE (usuario = " . $this->oBD->escapar($usuario)." OR correo=". $this->oBD->escapar($usuario).") "
					. " AND clave = '" . md5($clave) . "' AND estado=1";
			$res = $this->oBD->consultarSQL($sql);			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$rason_social,$ruc,$logo,$direccion,$telefono,$representante,$usuario,$clave,$correo,$estado)
	{
		try {
			
			$this->iniciarTransaccion('dat_bolsa_empresas_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idempresa) FROM bolsa_empresas");
			++$id;
			
			$estados = array('idempresa' => $id							
							,'nombre'=>$nombre
							,'rason_social'=>$rason_social
							,'ruc'=>$ruc
							,'logo'=>$logo
							,'direccion'=>$direccion
							,'telefono'=>$telefono
							,'representante'=>$representante
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'correo'=>$correo
							,'estado'=>$estado							
							);
			
			$this->oBD->insert('bolsa_empresas', $estados);			
			$this->terminarTransaccion('dat_bolsa_empresas_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bolsa_empresas_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bolsa_empresas").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$rason_social,$ruc,$logo,$direccion,$telefono,$representante,$usuario,$clave,$correo,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_bolsa_empresas_update');
			$estados = array('nombre'=>$nombre
							,'rason_social'=>$rason_social
							,'ruc'=>$ruc
							,'logo'=>$logo
							,'direccion'=>$direccion
							,'telefono'=>$telefono
							,'representante'=>$representante
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'correo'=>$correo
							,'estado'=>$estado								
							);
			
			$this->oBD->update('bolsa_empresas ', $estados, array('idempresa' => $id));
		    $this->terminarTransaccion('dat_bolsa_empresas_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bolsa_empresas").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bolsa_empresas  "
					. " WHERE idempresa = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bolsa_empresas").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bolsa_empresas', array('idempresa' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bolsa_empresas").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bolsa_empresas', array($propiedad => $valor), array('idempresa' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bolsa_empresas").": " . $e->getMessage());
		}
	}
   
		
}