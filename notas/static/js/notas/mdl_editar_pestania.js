var agregarIntervalosEscala = function($checkBox){
	var $frm = $checkBox.closest('#frmEditarPestania');
	var isChecked = $checkBox.is(':checked');
	if (isChecked) {
		$frm.find('.contenedor_escalas .val_nombre').parent().removeAttr('class').addClass('col-xs-5');
		$frm.find('.contenedor_escalas .val_desde').parent().removeClass('hidden');
		$frm.find('.contenedor_escalas .val_hasta').parent().removeClass('hidden');
		$frm.find('.minima_nota_pasar').removeClass('hidden');
	} else {
		$frm.find('.contenedor_escalas .val_nombre').parent().removeAttr('class').addClass('col-xs-11');
		$frm.find('.contenedor_escalas .val_desde').parent().addClass('hidden');
		$frm.find('.contenedor_escalas .val_hasta').parent().addClass('hidden');
		$frm.find('.minima_nota_pasar').addClass('hidden');
	}
};

var initEdit = () => { /* ECMAScript syntax */
	$('#frmEditarPestania select#tipo_info').trigger('change');
	var tipo_info = $('#frmEditarPestania select#tipo_info').val();
};

var activarBotonesAdd = () => {
	var value = $('#frmEditarPestania #opcColumna').val();
	if(value!='') {
		$('#frmEditarPestania .btn.add-col').removeAttr('disabled');
	} else {
		$('#frmEditarPestania .btn.add-col').attr('disabled', 'disabled');
	}
};

var ocultarSelectorCols = () => {
	var allOptions = $('#frmEditarPestania #opcColumna option').length - 1;
	var hiddenOptions = $('#frmEditarPestania #opcColumna option.hidden').length;
	if(allOptions===hiddenOptions) {
		$('#frmEditarPestania .selector_columnas').addClass('hidden');
	} else {
		$('#frmEditarPestania .selector_columnas').removeClass('hidden');
	}
};

var agregarAFormula = ($optSelected) => {
	var columna = {
		"idpestania" : $optSelected.val(),
		"nombre" : $optSelected.text(),
		"abreviatura" : $optSelected.attr('data-abrev'),
	}
	var $nota = $('#frmEditarPestania #nota_clonar').clone();
	$nota.removeClass('hidden');
	$nota.removeAttr('id');
	$nota.attr('data-idpestania', columna.idpestania);
	$nota.attr('data-abrev', columna.abreviatura);
	$nota.find('.nombre').text(columna.nombre);

	$('#frmEditarPestania .contenedor_notas').append($nota);

	$optSelected.addClass('hidden');
};

$(document).ready(function() {
	$('body').on('change', '#frmEditarPestania select#tipo_info', function(e) {
		var $frm = $(this).closest('#frmEditarPestania');
		$frm.find('#info_valor > .form-group ').addClass('hidden');
		var value = $(this).val();
		if (value=="") { return; }
		$frm.find('#info_valor > .form-group[data-opcion="'+value+'"]').removeClass('hidden');

		agregarIntervalosEscala($frm.find('#definir_intervalos'));
	}).on('click', '#frmEditarPestania #btn_agregar_escala', function(e) {
		e.preventDefault();
		var $frm = $(this).closest('#frmEditarPestania');
		var ultimo_val = $frm.find('.contenedor_escalas .escala').not($('#escala_clonar')).last().find('.val_hasta').val() ;
		var $new_escala = $frm.find('#escala_clonar').clone();
		$new_escala.removeAttr('id');
		$new_escala.find('.val_desde').val(ultimo_val);
		$frm.find('.contenedor_escalas').append($new_escala);
		$new_escala.removeClass('hidden');
		resizemodal($frm,-20);
	}).on('click', '#frmEditarPestania .btn_eliminar_escala', function(e) {
		e.preventDefault();
		$escala = $(this).closest('.escala');
		$escala.remove();
	}).on('change', '#frmEditarPestania #definir_intervalos', function(e) {
		e.preventDefault();
		agregarIntervalosEscala($(this));
	}).on('click', '#frmEditarPestania .btn-cancelar', function(e) {
		if($(this).closest('.modal').length) {
			$(this).closest('.modal').modal('hide');
		} else {
			$(this).closest('#frmEditarPestania').remove();
		}
		return false;
	}).on('change', '#frmEditarPestania #opcColumna', function(e) {
		activarBotonesAdd();
	}).on('click', '#frmEditarPestania .btn.add-col', function(e) {
		var $optSelected = $('#frmEditarPestania #opcColumna option:selected');
		agregarAFormula($optSelected);

		$('#frmEditarPestania #opcColumna').val('');
		activarBotonesAdd();
		ocultarSelectorCols();

		return false;
	}).on('click', '#frmEditarPestania .btn.add-all-cols', function(e) {
		$('#frmEditarPestania #opcColumna option').not('.hidden').each(function(i, option) {
			var $optSelected = $(option);
			if($optSelected.val()) {
				agregarAFormula($optSelected);
			}
		});

		$('#frmEditarPestania #opcColumna').val('');
		activarBotonesAdd();
		ocultarSelectorCols();

		return false;
	}).on('change', '#frmEditarPestania .columna_nota input.peso', function(e) {
		var value = $(this).val();
		if(value!='' && parseInt(value)<0) {
			$(this).val(0);
		}
	}).on('click', '#frmEditarPestania .descartar-nota', function(e) {
		var $nota = $(this).closest('.columna_nota');
		var idPestania = $nota.attr('data-idpestania');
		$('#frmEditarPestania #opcColumna option[value="'+idPestania+'"]').removeClass('hidden');
		$nota.remove();
		ocultarSelectorCols();
		return false;
	});
});