var ajustarAnchoCeldas = function() {
	var $divContent = $("#notas-main");
	var cantColums = $divContent.find("thead th").length;
	var anchoXcelda = 90; /* px. */ 
	$divContent.find('.contenedor-tabla').css('width', (cantColums*anchoXcelda)+"px");
};

var messageZone = function(estado, msje) {
	msje = msje || '';
	switch(estado) {
		case 'done': 
			$('#message-zone').removeClass('loading').removeClass('error');
			$('#message-zone').addClass('done');
			$('#message-zone span').text(MSJES_PHP.data_saved);
			break;
		case 'loading': 
			$('#message-zone').removeClass('done').removeClass('error');
			$('#message-zone').addClass('loading');
			$('#message-zone span').text(MSJES_PHP.loading);
			break;
		case 'error': 
			$('#message-zone').removeClass('loading').removeClass('done');
			$('#message-zone').addClass('error');
			$('#message-zone').attr("title", msje);
			$('#message-zone span').text(MSJES_PHP.error);
			break;
		default: break;
	};
};

var resizeTables = function() {
	var altoVentana = $(window).height() ,
		posicion = $('#registro').position() ,
		pos_arrriba = posicion.top,
		altoThead = $('#registro table').first().find('thead').outerHeight(),
		altoTbody = altoVentana - pos_arrriba - altoThead - 20;

	$('#registro table tbody').css('height', altoTbody+'px');
};

var jColumna = {};
var actualizarJsonCols = function(col){
	jColumna[ col.idpestania ] = {
		"idpestania" : col.idpestania,
		"nombre" : col.nombre,
		"tipo_info" : col.tipo_info,
		"info_valor" : null,
	};
	if(col.tipo_info=='N' || col.tipo_info=='R' || col.tipo_info=='E' || col.tipo_info=='P') {
		jColumna[col.idpestania].info_valor = JSON.parse(col.info_valor);
	}
};

var draggerTable = null;
var agregarColumna = function (columna) {
	var $tblNotas = $("#notas-main table");
	let clsTexto = (columna.tipo_info=='T')?'col_texto':'';
	let col_nombre = (columna.abreviatura!=null && columna.abreviatura.trim().length)?columna.abreviatura:columna.nombre.substr(0,4);
	$tblNotas.find('thead tr #btn_agregar_colum').closest('th').before(''+
		'<th class="head_col '+clsTexto+'" id="c_'+columna.idpestania+'" data-idcolumna="'+columna.idpestania+'" title="'+columna.nombre+'" style="background-color:'+columna.color+';">'+
			/*'<i class="fa fa-bars drag_column"></i>'+*/
			'<span class="editar-columna" title="'+MSJES_PHP.click_edit+' '+columna.nombre+'">'+col_nombre+'</span>'+
			'<a href="#" class="borrar-columna" title="'+MSJES_PHP.delete+' '+columna.nombre+'"><i class="fa fa-times color-red"></i></a>'+
		'</th>');
	$tblNotas.find('tbody tr').each(function(i, tr) {
		var $fila = $(tr);
		var idalumno = $fila.attr('data-idalumno');
		var data_idnota = '';
		var data_nota_num = '';
		var data_nota_txt = '';
		var nota_value = '';
		var data_observacion = '';
		var cls_aprobado = '';
		var nota = (typeof columna.notas_alum!='undefined')?columna.notas_alum[idalumno]:null;
		var info_valor = JSON.parse(columna.info_valor);
		if (typeof nota!='undefined' && nota!=null) {
			data_idnota = ' data-idnota="'+nota.idnota+'" ';
			data_nota_num = (nota.nota_num!==null)?' data-notanum="'+nota.nota_num+'" ':'';
			data_nota_txt = (nota.nota_txt!==null)?' data-notatxt="'+nota.nota_txt+'" ':'';
			data_observacion = (nota.observacion!==null)?' data-observacion="'+nota.observacion+'" ':'';
			if(!(columna.tipo_info=='T' || (columna.tipo_info=='E' && !info_valor.tiene_intervalos))) {
				cls_aprobado = (nota.aprobado=='1')?' aprobado':' desaprobado';
			}
			if(columna.tipo_info=='E' || columna.tipo_info=='T'){
				nota_value = nota.nota_txt;
			}  else {
				nota_value = nota.nota_num!=null?nota.nota_num:0.00;
			}
			if(columna.tipo_info=='P') {
				$('#notas-main table').addClass('tiene_ponderado');
			}
		}
		$fila.find('.comodin_celda').before(''+
			'<td class="nota '+clsTexto+cls_aprobado+'" id="a'+idalumno+'_c'+columna.idpestania+'" data-idcolumna="'+columna.idpestania+'"'+data_nota_txt + data_nota_num + data_idnota + data_observacion+' title="'+nota_value+'">'+
				'<button data-toggle="popover">'+nota_value+'</button>'+
			'</td>');
	});

	actualizarJsonCols(columna);
	ajustarAnchoCeldas();
};

var agregarHoja = function(hoja) {
	var strHoja = '';
	strHoja += '<li class="hvrR-backward tab_hoja" data-idhoja="'+hoja.idpestania+'">';
	strHoja += '<a class="vertical" href="#hoja_'+hoja.idpestania+'" data-toggle="tab" style="background: '+hoja.color+';">';
	strHoja += hoja.nombre;
	strHoja += '</a></li>';

	$('#btn_agregar_hoja').closest('li').before(strHoja);

	$('#hojas-pestanias li>a[href="#hoja_'+hoja.idpestania+'"]').tab('show');
};

var construirInfoValor = function(tipo_info, $form) {
	var valor = null;
	switch (tipo_info) {
		case 'N' :  
			valor = null; 
			var json = {
				"min_pass" : parseFloat($form.find('#info_valor div[data-opcion="N"] .val_minimo_pass').val()),
			};
			valor = JSON.stringify(json);
			break;
		case 'R' : 
			var rango = {
				"min" : parseFloat($form.find('#info_valor div[data-opcion="R"] .val_desde').val()),
				"max" : parseFloat($form.find('#info_valor div[data-opcion="R"] .val_hasta').val()),
				"min_pass" : parseFloat($form.find('#info_valor div[data-opcion="R"] .val_minimo_pass').val()),
			};
			if (isNaN(rango.max)) {
				console.log('Error: Rango máximo vacío.');
				$form.find('#info_valor div[data-opcion="R"] .val_hasta').parent().addClass('has-error');
				valor = false;
			} else if (rango.min >= rango.max) {
				console.log('Error: Rango mínimo es mayor o igual que rango máximo.');
				$form.find('#info_valor div[data-opcion="R"] .val_desde').parent().addClass('has-error');
				$form.find('#info_valor div[data-opcion="R"] .val_hasta').parent().addClass('has-error');
				valor = false;
			} else if (rango.min_pass <= rango.min || rango.min_pass >= rango.max) {
				console.log('Error: Valor "minimo para pasar" no esta dentro del rango.');
				$form.find('#info_valor div[data-opcion="R"] .val_minimo_pass').parent().addClass('has-error');
			} else {
				$form.find('#info_valor div[data-opcion="R"] .val_desde').parent().removeClass('has-error');
				$form.find('#info_valor div[data-opcion="R"] .val_hasta').parent().removeClass('has-error');
				$form.find('#info_valor div[data-opcion="R"] .val_minimo_pass').parent().removeClass('has-error');
				valor = JSON.stringify(rango);
			}
			break;
		case 'E' : 
			var hasIntervalos = $form.find('#info_valor div[data-opcion="E"] #definir_intervalos').is(':checked');
			var arrEscala = {
				"tiene_intervalos" : hasIntervalos,
				"escalas" : [],
				"min_pass" : null,
			};
			$form.find('#info_valor div[data-opcion="E"] .contenedor_escalas .escala').not('#escala_clonar').each(function(i, elem) {
				var $escala = $(elem);
				var new_escala = {
					"nombre" : $escala.find('.val_nombre').val(),
				};
				if(new_escala.nombre.trim()=="") {
					console.log('Error: Nombre vacío en: ', $escala);
					$escala.find('.val_nombre').parent().addClass('has-error');
					valor = false;
					return false;
				}
				if(hasIntervalos) {
					new_escala['min'] = parseFloat($escala.find('.val_desde').val());
					new_escala['max'] = parseFloat($escala.find('.val_hasta').val());
					if(new_escala.min >= new_escala.max) {
						console.log('Error: Rango mínimo es mayor o igual que rango máximo en: ', $escala);
						$escala.find('.val_desde').parent().addClass('has-error');
						$escala.find('.val_hasta').parent().addClass('has-error');
						valor = false;
						return false;
					}
					var minimo_pass = $form.find('#info_valor div[data-opcion="E"] .val_minimo_pass').val();
					arrEscala.min_pass = minimo_pass;
				}
				arrEscala.escalas.push(new_escala);
			});
			if(valor!==false){
				valor = JSON.stringify(arrEscala);
			}
			break;
		case 'T' : break;
		case 'A' : break;
		case 'P' : 
			var arrPonderado = {
				"formula" :  [],
				"min_pass" : $form.find('#info_valor div[data-opcion="P"] .val_minimo_pass').val(),
			};
			$form.find('#info_valor div[data-opcion="P"] .contenedor_notas .columna_nota').not('#nota_clonar').each(function(i, elem) {
				var $nota = $(elem);
				var ponderado = {
					"idpestania" : $nota.attr('data-idpestania'),
					"peso" : parseFloat($nota.find('input.peso').val()||0)
				};
				arrPonderado.formula.push(ponderado);
			});
			valor = JSON.stringify(arrPonderado);
			break;
		default  : valor = null; break;
	}
	return valor;
};

var getContenidoHoja = function(idHoja) {
	idHoja = idHoja || 0;
	if(idHoja===0) return false;
	var jAlert = null;
	restablecerTabla();
	$.ajax({
		url: _sysUrlBase_ + '/notas_pestania/buscarjson',
		type: 'POST',
		dataType: 'json',
		data: {
			"tipo_pestania" : 'C',
			"idarchivo" : $("#hIdArchivo").val(),
			"idpestania_padre" : idHoja,
			"con_notas" : true,
		},
		beforeSend: function(){
			messageZone('loading');
			jAlert = $.alert({
			    title: '',
			    content: '<div class="text-center"><div><i class="fa fa-cog fa-spin fa-4x"></i></div>' + MSJES_PHP.getting_ready + '</div>',
			    confirmButtonClass: 'hidden',
			    cancelButtonClass: 'hidden'
			}); 
		},
	}).done(function(resp) {
		if (resp.code=="ok") {
			if(resp.data.length){
				$.each(resp.data, function(i, column) {
					agregarColumna(column);
				});
			}
			messageZone('done');
		} else {
			/*mostrar_notificacion(MSJES_PHP.attention, resp.msje, "error");*/
			messageZone('error', resp.msj);
		}
	}).fail(fnAjaxFail).always(function() {
		jAlert.close();
	});
};

var guardarPestania = function($btn, dataSend) {
	dataSend = dataSend || {};
	if($.isEmptyObject(dataSend)) return false;
	$.ajax({
		url: _sysUrlBase_ + '/notas_pestania/xGuardar',
		type: 'POST',
		dataType: 'json',
		data: dataSend,
		beforeSend : function() {
			$btn.attr('disabled', 'disabled');
			$btn.find('i.fa').addClass('hidden');
			$btn.prepend('<i class="fa fa-circle-o-notch fa-spin fa-fw icon-cargando"></i>');
			messageZone('loading');
		},
	}).done(function(resp) {
		if(resp.code == "ok") {
			dataSend.idpestania = resp.data;
			if(dataSend.tipo_pestania=='C') {
				agregarColumna(dataSend);
				if(dataSend.idpestania!='' && dataSend.idpestania>0) {
					/* editar pestania */
					console.log("idpestania!=''  ==> obtener Contenido Hoja");
					getContenidoHoja(dataSend.idpestania_padre);
				}
			} else if(dataSend.tipo_pestania=='H'){
				agregarHoja(dataSend);
			}
			$('#frmEditarPestania').closest('.modal').modal('hide');
			messageZone('done');
		} else {
			/*mostrar_notificacion(MSJES_PHP.attention, resp.msj, "error");*/
			messageZone('error', resp.msj);
		}
	}).fail(function(err, msj, other) {
		console.log("error : " , err);
		messageZone('error', msj);
	}).always(function() {
		$btn.find('.icon-cargando').remove();
		$btn.find('i.fa').removeClass('hidden');
		$btn.removeAttr('disabled', 'disabled');
	});
};

var getPlacement = function($el) {
    var offset = $el.offset(),
        top = offset.top,
        left = offset.left,
        height = $(document).outerHeight(),
        width = $(document).outerWidth(),
        vert = 0.5 * height - top,
        vertPlacement = vert > 0 ? 'bottom' : 'top',
        horiz = 0.5 * width - left,
        horizPlacement = horiz > 0 ? 'right' : 'left',
        placement = Math.abs(horiz) > Math.abs(vert) ?  horizPlacement : vertPlacement;
    //console.log('getPlacement : ', placement);
    return placement
};

var popoverOpen = false;
var idCeldaPopover = null;
var destruirTodosPopover = function() {
	if( popoverOpen ) {
		$('#notas-main button[data-toggle="popover"]').popover('destroy');
		popoverOpen = false;
	}
};

var setPopoverTitulo  = function($celda) {
	var idColumna = $celda.attr('data-idcolumna');
	var nombreCol = jColumna[idColumna].nombre;
	var strHtml = '<b>'+nombreCol+'</b>';
	strHtml += '<button class="close" title="'+MSJES_PHP.close+'">&times;</button>';
	return strHtml;
};

var setPopoverContenido  = function($celda) {
	var idColumna = $celda.attr('data-idcolumna');
	var idAlumno = $celda.closest('tr[data-idalumno]').attr('data-idalumno');
	var nombreAlumno = $('#edita-alumnos table tbody tr[data-idalumno="'+idAlumno+'"]').find('td.nombre').text();
	var tipo_info = jColumna[idColumna].tipo_info;
	var info_valor = jColumna[idColumna].info_valor;
	var strHtml = '<div class="col-xs-12">'+nombreAlumno+'</div>';
	var celda_value = $celda.text().trim() || '';
	strHtml += '<div class="col-xs-12">'; 
	switch (tipo_info) {
		case "N" : 
			strHtml +='<div class="col-xs-12 col-sm-9 padding-0"><input type="number" class="form-control elegir_nota" min="0" placeholder="00" autofocus value="'+celda_value+'"></div>';
			break;
		case "R" : 
			strHtml += '<div class="col-xs-12 col-sm-9 padding-0">';
			strHtml += '<select class="form-control elegir_nota" autofocus>';
			var minimo = parseFloat(info_valor.min);
			var maximo = parseFloat(info_valor.max);
			
			for(let x=minimo ; x<=maximo; x++) {
				let num = (x<10?'0':'')+x;
				let num_float = parseFloat(num);
				let selected = (num_float==parseFloat(celda_value))?'selected':'';
				strHtml += '<option value="'+num+'" '+selected+'>'+num+'</option>';
			}
			strHtml += '</select></div>';
			break;
		case "E" : 
			var html_input = '';
			if (info_valor.tiene_intervalos){
				strHtml += '<div class="col-xs-12 padding-0">';
				strHtml += '<label><input type="checkbox" class="checkbox-ctrl escribir_numero"> '+MSJES_PHP.choose_from_number+'</label>';
				strHtml += '</div>';

				var min = info_valor.escalas[0].min;
				var max = info_valor.escalas[info_valor.escalas.length-1].max;
				html_input = '<input type="number" class="form-control valor_numerico_escala hidden" min="'+min+'" max="'+max+'" placeholder="00">';
			}
			strHtml += '<div class="col-xs-12 col-sm-9 padding-0">';
			strHtml += html_input;
			strHtml += '<select class="form-control elegir_nota opc_escalas" autofocus>';
			$.each(info_valor.escalas, function(i, escala) {
				let selected = (celda_value==escala.nombre)?'selected':'';
				strHtml += '<option value="'+escala.nombre+'" '+selected+'>'+escala.nombre+'</option>';
			});
			strHtml += '</select></div>';
			break;
		case "T" :
			strHtml +='<div class="col-xs-12 padding-0"><textarea class="form-control elegir_nota" placeholder="'+MSJES_PHP.write_something+'..." autofocus>'+celda_value+'</textarea></div>';
			break;
		case "P" :
			var observacion = $celda.attr('data-observacion');
			strHtml += '<div class="col-xs-12 padding-0 bolder"><p>'+observacion+'</p><p>= '+celda_value+'</p></div>';
			break;
		default : return false; break;
	}
	strHtml += '<div class="col-xs-12 col-sm-3 padding-0 text-right pull-right">';
	strHtml += '<button class="btn btn-primary btn-block btn_guardar_nota">Ok</button>';
	strHtml += '</div>';
	strHtml += '<div class="col-xs-12 col-sm-9 padding-0 text-left">';
	strHtml += '<label class="hidden"> <input type="checkbox" class="checkbox-ctrl"> '+MSJES_PHP.jump_next_student+'</label>';
	strHtml += '</div>';
	strHtml += '</div>';
	return strHtml;
};

var guardarNota = function($celda, dataSend) {
	dataSend = dataSend || {};
	if($.isEmptyObject(dataSend)) return false;
	$.ajax({
		url: _sysUrlBase_+'/notas/guardarNotas',
		type: 'POST',
		dataType: 'json',
		data: dataSend,
		beforeSend : function(){
			messageZone('loading');
		}
	}).done(function(resp) {
		if(resp.code=='ok') {
			$celda.attr('data-idnota',resp.newid);
			actualizarPromPonderados(dataSend.idhoja, dataSend.idalumno);
			messageZone('done');
		} else {
			// cargando message => algo salio mal
			messageZone('error', resp.msj);
		}
	}).fail(function(err, msj, other) {
		console.log('error', err);
		messageZone('error', msj);
	}).always(function() { });
};

var eliminarColumna = function (idPestania) {
	var idHoja = $('#navHojas .tab_hoja.active').attr('data-idhoja');
	$.ajax({
		url: _sysUrlBase_+'/notas_pestania/jxEliminar/',
		type: 'POST',
		dataType: 'json',
		data: {"idpestania": idPestania},
		beforeSend : function(){
			messageZone('loading');
		}
	}).done(function(resp) {
		if(resp.code=="ok") {
			getContenidoHoja(idHoja);
			messageZone('done');
		} else {
		messageZone('error', resp.msj);
		}
	}).fail(function(err, msj, other) {
		console.log('error', err);
		messageZone('error', msj);
	}).always(function() {
	});
};

var restablecerTabla = function () {
	$('.contenedor-tabla table thead th.head_col').remove();
	$('.contenedor-tabla table tbody td.nota').remove();
	jColumna = {};
	ajustarAnchoCeldas();
};

var actualizarPromPonderados = function(idHoja, idAlumno) {
	if( !$('#notas-main table').hasClass('tiene_ponderado') ) return false;
	$.ajax({
		url: _sysUrlBase_+'/notas_pestania/jxActualizarPonderados',
		type: 'POST',
		dataType: 'json',
		data: {"idhoja": idHoja},
		beforeSend : function() {
			messageZone('loading');
			$.each(jColumna, function(idpestania, columna) {
				if(columna.tipo_info == 'P') {
					$('#notas-main table tbody tr[data-idalumno="'+idAlumno+'"] td[data-idcolumna="'+columna.idpestania+'"] button').html('<i class="fa fa-circle-o-notch fa-spin fa-2x"></i>');
				}
			});
		},
	}).done(function(resp) {
		if(resp.code=='ok') {
			if(resp.data.length) {
				$.each(resp.data, function(i, col) {
					actualizarJsonCols(col);
					var notaAlum = col.notas_alum[idAlumno];
					var $celda = $('#notas-main table tbody tr[data-idalumno="'+idAlumno+'"] td[data-idcolumna="'+col.idpestania+'"]');
					$celda.attr({
						"data-notanum": notaAlum.nota_num,
						"data-observacion": notaAlum.observacion,
					});
					$celda.find('button').html(notaAlum.nota_num);
				});
			}
			messageZone('done');
		} else {
			console.log(resp.msj);
			messageZone('error', resp.msj);
		}
	}).fail(function(err, msj, other) {
		console.log("!Error : ", err);
		messageZone('error', msj);
	}).always(function() {});
};

$(document).ready(function() {
	cargarMensajesPHP($("#mensajes_idioma"));
	resizeTables();

	$('.tabla-contenidos tbody').on('scroll', function (e) {
		$('.tabla-contenidos tbody').not($(this)).scrollTop($(this).scrollTop());
	}).on( 'mousewheel DOMMouseScroll', function (e) { 
		var e0 = e.originalEvent;
		var delta = e0.wheelDelta || -e0.detail;

		this.scrollTop += ( delta < 0 ? 1 : -1 ) * 52; /* 52 = altura de celda*/
		destruirTodosPopover();
		e.preventDefault();  
	});

	$(window).resize(function(){
		resizeTables();
    });
    $(document).mousemove(function(){
    	if( !$('header').is(':visible') ){ resizeTables(); }
    });

	$('.agregar-alums').click(function(e) {
		e.preventDefault();
		var $modal = sysmodal({
			"tam" : "lg",
			"url" : _sysUrlBase_+'/notas_alumno/mdl_importar_alumnos',
			"borrar" : true,
			"titulo" : MSJES_PHP.add_students,
			"ventanaid": "agregar_alumnos",
			"cerrarconesc" : false,
			"showfooter" : false,
		});
	});

	$('.agregar_pestania').click(function(e) {
		e.preventDefault();
		destruirTodosPopover();
		var tipo_pestania = $(this).attr('data-tipopestania');
		var cantPestanias = (tipo_pestania=='H')?$('#navHojas li.tab_hoja').length:$('#notas-main table>thead th.head_col').length;
		var pestania_padre = (tipo_pestania=='C')?$('#navHojas li.tab_hoja.active').attr('data-idhoja'):0;
		var orden = cantPestanias+1;

		var $modal = sysmodal({
			"tam" : "md",
			"url" :  _sysUrlBase_+'/notas_pestania/mdl_agregar_pestania/?tipo_pestania='+tipo_pestania+'&orden='+orden+'&idpestania_padre='+pestania_padre,
			"borrar" : true,
			"titulo" : (tipo_pestania=='C')?MSJES_PHP.edit_columns:MSJES_PHP.edit_sheet,
			"ventanaid": "editar_pestania",
			"cerrarconesc" : false,
			"showfooter" : false,
		});
	});

	$('.btn-configuracion').click(function(e) {
		var idArchivo = $('#hIdArchivo').val();
		var $modal = sysmodal({
			"tam" : "md",
			"url" :  _sysUrlBase_+'/notas_pestania/mdl_configuracion/?idarchivo='+idArchivo,
			//"html" : '#frm_agregar_pestania',
			"borrar" : true,
			"titulo" : MSJES_PHP.settings,
			"ventanaid": "mdl_configuracion",
			"cerrarconesc" : false,
			"showfooter" : false,
		});

		return false;
	});

	$('body').on('click', '#frmEditarPestania .btn.guardar_pestania', function(e) {
		e.preventDefault();
		var $this = $(this);
		var $modal = $(this).closest('.modal');
		var $form = $modal.find('#frmEditarPestania');
		var dataSend = {
			'idpestania' : $form.find('#idpestania').val(),
			'nombre' : $form.find('#nombre').val(),
			'abreviatura' : $form.find('#abreviatura').val(),
			'tipo_pestania' : $form.find('#tipo_pestania').val(),
			'orden' : $form.find('#orden').val(),
			'color' : $form.find('#color').val(),
			'idpestania_padre' : $form.find('#idpestania_padre').val(),
			'idarchivo' : $('#hIdArchivo').val(),
			'tipo_info' : null,
			'info_valor' : null,
		};

		if(dataSend.tipo_pestania=='C') {
			dataSend.tipo_info = $form.find('#tipo_info').val();
			dataSend.info_valor = construirInfoValor(dataSend.tipo_info ,$form);
			if(dataSend.info_valor===false){ return false; }
		}

		guardarPestania($this, dataSend);
	});

	$('#navHojas').on('shown.bs.tab', 'li>a[data-toggle="tab"]', function(e) {
		e.preventDefault();
		var idHoja = $(this).closest('li').attr('data-idhoja'); 
		var color = $(this).css('background-color');
		$('#editar-notas #registro').css('border-color', color);
		getContenidoHoja(idHoja);
	});

	$('#notas-main').on('click', 'table tbody td>button', function(e) {
		e.preventDefault();
		var $btn = $(this);
		var $celda = $(this).closest('td');
		var idCelda = $btn.closest('td').attr('id');
		if( idCeldaPopover === idCelda ){ idCelda = null; }
		if(popoverOpen) {
			destruirTodosPopover();
			idCeldaPopover = null;
		}
		if( idCeldaPopover != idCelda ) {
			$btn.popover({
			    placement: getPlacement($btn),
			    trigger: 'click',
			    html: true,
			    container : 'body',
			    title: setPopoverTitulo($celda),
			    content: setPopoverContenido($celda),
			    template : '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content row"></div></div>',
			});
			$btn.popover('show');
			popoverOpen = true;
			idCeldaPopover = idCelda;
		}
	}).on('shown.bs.popover', 'table tbody td>button', function () {
		var idPopover = $(this).attr('aria-describedby');
		var $popover = $('#'+idPopover);
		var $focusElem = $popover.find('*[autofocus]');
		if(!$focusElem.is(':focus')){ $focusElem.trigger('focus'); }
	}).on('click', 'table thead th>.editar-columna', function(e) {
		var idPestania = $(this).closest('th').attr('data-idcolumna');
		var $modal = sysmodal({
			"tam" : "md",
			"url" : _sysUrlBase_+'/notas_pestania/mdl_editar_pestania/?idpestania='+idPestania+'&fn=initEdit',
			"borrar" : true,
			"titulo" : MSJES_PHP.edit_columns,
			"ventanaid": "editar_columna",
			"cerrarconesc" : false,
			"showfooter" : false,
		});
		return false;
	}).on('click', 'table thead th>.borrar-columna', function(e) {
		var idPestania = $(this).closest('th').attr('data-idcolumna');
		$.confirm({
			title: $(this).attr('title'),
			content: MSJES_PHP.are_you_sure_delete_column_and_scores,
			confirm: function(){
				eliminarColumna(idPestania);
			},
		});
		return false;
	})

	$('body').on('click', '.popover button.close', function(e) {
		e.preventDefault();
		var idPopover = $(this).closest('.popover').attr('id');
		$('#notas-main table tbody tr td>button[aria-describedby="'+idPopover+'"]').trigger('click');
	}).on('change', '.popover input.checkbox-ctrl.escribir_numero', function(e) {
		var isChecked = $(this).is(':checked');
		var $popover = $(this).closest('.popover');
		if (isChecked) {
			$popover.find('input.valor_numerico_escala').removeClass('hidden');
			$popover.find('input.valor_numerico_escala').trigger("focus");
			$popover.find('select.opc_escalas').addClass('hidden');
		} else {
			$popover.find('input.valor_numerico_escala').addClass('hidden');
			$popover.find('select.opc_escalas').removeClass('hidden');
			$popover.find('select.opc_escalas').trigger("focus");
		}
	}).on('keypress', '.popover *:focus', function(e) {
		if(e.which == 13) {
			e.preventDefault();
			var $popover = $(this).closest('.popover');
			$popover.find('.btn_guardar_nota').trigger('click');
	    }
	}).on('keyup', '.popover input[type="number"]', function(e) {
		var $popover = $(this).closest('.popover');
		var idPopover = $popover.attr('id');
		var idColumna = $('#notas-main table tbody tr td>button[aria-describedby="'+idPopover+'"]').closest('td').attr('data-idcolumna');
		var min = parseFloat($(this).attr('min'));
		var max = parseFloat($(this).attr('max'));
		var value = parseFloat($(this).val());
		if(!isNaN(min)) {
			if(value<min){ $(this).val(min); }
	    }
		if(!isNaN(max)) {
			if(value>max){ $(this).val(max); }
	    }
	    if(jColumna[idColumna].tipo_info=='E'){
	    	var txt_escala = '';
	    	$.each(jColumna[idColumna].info_valor.escalas, function(i, escala) {
				if ( value>=escala.min && value<escala.max) {
					txt_escala = escala.nombre;
					return false;
				}
			});
			$popover.find('select.elegir_nota').val(txt_escala);
	    }
	}).on('click', '.popover .btn_guardar_nota', function(e) {
		e.preventDefault();

		var $popover = $(this).closest('.popover');
		var idPopover = $(this).closest('.popover').attr('id');
		var $celda = $('#notas-main table tbody td>button[aria-describedby="'+idPopover+'"]').closest('td');
		var idColumna = $celda.attr('data-idcolumna');
		var tipo_info = jColumna[idColumna].tipo_info;
		console.log(jColumna[idColumna]);
		var minimo_pass = parseFloat(jColumna[idColumna].info_valor.min_pass) || 0;
		var nota_value = $popover.find('.elegir_nota').val();
		$celda.find('button').text(nota_value);
		$popover.find('.close').trigger('click');
		if(tipo_info=='P') return false;
		var dataSend = {
			'idnota': $celda.attr('data-idnota') || null,
			'idhoja': $('#navHojas li.tab_hoja.active').attr('data-idhoja'),
			'idcolumna': $celda.attr('data-idcolumna'),
			'idalumno': $celda.closest('tr').attr('data-idalumno'),
			'idarchivo': $('#hIdArchivo').val(),
			'nota_num': null,
			'nota_txt': null,
			'aprobado': 0,
		};

		if(tipo_info=='E'){
			dataSend.nota_txt = nota_value;
			$celda.attr('data-notatxt',nota_value);
			if( $popover.find('input[type="number"]').length ){
				var nota_num = parseFloat($popover.find('input[type="number"]').val());
				dataSend.nota_num = nota_num;
				$celda.attr('data-notanum',nota_num);
				if(nota_num>minimo_pass) {
					dataSend.aprobado = 1; 
					$celda.addClass('aprobado');
					$celda.removeClass('desaprobado');
				} else {
					$celda.addClass('desaprobado');
					$celda.removeClass('aprobado');
				}
			}
		} else if(tipo_info=='T'){
			dataSend.nota_txt = nota_value;
			$celda.attr('data-notatxt',nota_value);
		} else {
			dataSend.nota_num = nota_value;
			$celda.attr('data-notanum',nota_value);
			if(nota_value>minimo_pass) {
				dataSend.aprobado = 1; 
				$celda.addClass('aprobado');
				$celda.removeClass('desaprobado');
			} else {
				$celda.addClass('desaprobado');
				$celda.removeClass('aprobado');
			}
		}
		guardarNota($celda, dataSend);
	});

	//anchoNotasMain();
	$('#navHojas li').first().find('a[data-toggle="tab"]').tab('show');
});