var resizeTables = function() {
	var altoVentana = $(window).height() ,
		posicion = $('#registro').position() ,
		pos_arrriba = posicion.top,
		altoThead = $('#registro table').first().find('thead').outerHeight(),
		altoTbody = altoVentana - pos_arrriba - altoThead - 20;

	$('#registro table tbody').css('height', altoTbody+'px');
};

var getContenidoHoja = function(idHoja) {
	idHoja = idHoja || 0;
	if(idHoja===0) return false;
	var jAlert = null;
	restablecerTabla();
	$.ajax({
		url: _sysUrlBase_ + '/notas_pestania/buscarjson',
		type: 'POST',
		dataType: 'json',
		data: {
			"tipo_pestania" : 'C',
			"idarchivo" : $("#hIdArchivo").val(),
			"idpestania_padre" : idHoja,
			"con_notas" : true,
		},
		beforeSend: function(){
			messageZone('loading');
			jAlert = $.alert({
			    title: '',
			    content: '<div class="text-center"><div><i class="fa fa-cog fa-spin fa-4x"></i></div>' + MSJES_PHP.getting_ready + '</div>',
			    confirmButtonClass: 'hidden',
			    cancelButtonClass: 'hidden'
			}); 
		},
	}).done(function(resp) {
		if (resp.code=="ok") {
			if(resp.data.length){
				$.each(resp.data, function(i, column) {
					agregarColumna(column);
				});
			}
			messageZone('done');
		} else {
			/*mostrar_notificacion(MSJES_PHP.attention, resp.msje, "error");*/
			messageZone('error', resp.msj);
		}
	}).fail(fnAjaxFail).always(function() {
		jAlert.close();
	});
};

var messageZone = function(estado, msje) {
	msje = msje || '';
	switch(estado) {
		case 'done': 
			$('#message-zone').removeClass('loading').removeClass('error');
			$('#message-zone').addClass('done');
			$('#message-zone span').text(MSJES_PHP.data_saved);
			break;
		case 'loading': 
			$('#message-zone').removeClass('done').removeClass('error');
			$('#message-zone').addClass('loading');
			$('#message-zone span').text(MSJES_PHP.loading);
			break;
		case 'error': 
			$('#message-zone').removeClass('loading').removeClass('done');
			$('#message-zone').addClass('error');
			$('#message-zone').attr("title", msje);
			$('#message-zone span').text(MSJES_PHP.error);
			break;
		default: break;
	};
};

var restablecerTabla = function () {
	$('.contenedor-tabla table thead th.head_col').remove();
	$('.contenedor-tabla table tbody td.nota').remove();
	jColumna = {};
	ajustarAnchoCeldas();
};

var agregarColumna = function (columna) {
	var $tblNotas = $("#notas-main table");
	let clsTexto = (columna.tipo_info=='T')?'col_texto':'';
	let col_nombre = (columna.abreviatura!=null && columna.abreviatura.trim().length)?columna.abreviatura:columna.nombre.substr(0,4);
	$tblNotas.find('thead tr').append(''+
		'<th class="head_col '+clsTexto+'" id="c_'+columna.idpestania+'" data-idcolumna="'+columna.idpestania+'" title="'+columna.nombre+'" style="background-color:'+columna.color+';">'+
			'<span class="">'+col_nombre+'</span>'+
		'</th>');
	$tblNotas.find('tbody tr').each(function(i, tr) {
		var $fila = $(tr);
		var idalumno = $fila.attr('data-idalumno');
		var data_idnota = '';
		var data_nota_num = '';
		var data_nota_txt = '';
		var nota_value = '';
		var data_observacion = '';
		var cls_aprobado = '';
		var nota = (typeof columna.notas_alum!='undefined')?columna.notas_alum[idalumno]:null;
		var info_valor = JSON.parse(columna.info_valor);
		if (typeof nota!='undefined' && nota!=null) {
			data_idnota = ' data-idnota="'+nota.idnota+'" ';
			data_nota_num = (nota.nota_num!==null)?' data-notanum="'+nota.nota_num+'" ':'';
			data_nota_txt = (nota.nota_txt!==null)?' data-notatxt="'+nota.nota_txt+'" ':'';
			data_observacion = (nota.observacion!==null)?' data-observacion="'+nota.observacion+'" ':'';
			if(!(columna.tipo_info=='T' || (columna.tipo_info=='E' && !info_valor.tiene_intervalos))) {
				cls_aprobado = (nota.aprobado=='1')?' aprobado':' desaprobado';
			}
			if(columna.tipo_info=='E' || columna.tipo_info=='T'){
				nota_value = nota.nota_txt;
			}  else {
				nota_value = nota.nota_num!=null?nota.nota_num:0.00;
			}
			if(columna.tipo_info=='P') {
				$('#notas-main table').addClass('tiene_ponderado');
			}
		}
		$fila.append(''+
			'<td class="nota '+clsTexto+cls_aprobado+ '" id="a'+idalumno+'_c'+columna.idpestania+'" data-idcolumna="'+columna.idpestania+'"'+data_nota_txt + data_nota_num + data_idnota + data_observacion+' title="'+nota_value+'">'+
				'<button data-toggle="popover">'+nota_value+'</button>'+
			'</td>');
	});
	actualizarJsonCols(columna);
	ajustarAnchoCeldas();
};

var jColumna = {};
var actualizarJsonCols = function(col){
	jColumna[ col.idpestania ] = {
		"idpestania" : col.idpestania,
		"nombre" : col.nombre,
		"tipo_info" : col.tipo_info,
		"info_valor" : null,
	};
	if(col.tipo_info=='R' || col.tipo_info=='E' || col.tipo_info=='P') {
		jColumna[col.idpestania].info_valor = JSON.parse(col.info_valor);
	}
};

var ajustarAnchoCeldas = function() {
	var $divContent = $("#notas-main");
	var cantColums = $divContent.find("thead th").length;
	var anchoXcelda = 90; /* px. */ 
	$divContent.find('.contenedor-tabla').css('width', (cantColums*anchoXcelda)+"px");
};

$(document).ready(function() {
	cargarMensajesPHP($("#mensajes_idioma"));
	resizeTables();

	$('.tabla-contenidos tbody').on('scroll', function (e) {
		$('.tabla-contenidos tbody').not($(this)).scrollTop($(this).scrollTop());
	}).on( 'mousewheel DOMMouseScroll', function (e) { 
		var e0 = e.originalEvent;
		var delta = e0.wheelDelta || -e0.detail;

		this.scrollTop += ( delta < 0 ? 1 : -1 ) * 52; /* 52 = altura de celda*/
		e.preventDefault();  
	});

	$(window).resize(function(){
		resizeTables();
    });
    $(document).mousemove(function(){
    	if( !$('header').is(':visible') ){ resizeTables(); }
    });

	$('#navHojas').on('shown.bs.tab', 'li>a[data-toggle="tab"]', function(e) {
		e.preventDefault();
		var idHoja = $(this).closest('li').attr('data-idhoja'); 
		var color = $(this).css('background-color');
		$('.registro-notas #registro').css('border-color', color);
		getContenidoHoja(idHoja);
	});

	$('#btn-options .btn-buscar-alumno').click(function(e) {
		var idArchivo = $('#hIdArchivo').val();
		var $modal = sysmodal({
			"tam" : "lg",
			"url" : _sysUrlBase_+'/notas_alumno/mdl_buscar_alumno_notas/?idarchivo='+idArchivo,
			"borrar" : true,
			"titulo" : MSJES_PHP.search_students,
			"ventanaid": "buscar_alumno_notas",
			"cerrarconesc" : false,
			"showfooter" : false,
		});
		return false;
	});

	$('#btn-options .btn-altas-notas').click(function(e) {
		var idArchivo = $('#hIdArchivo').val();
		var $modal = sysmodal({
			"tam" : "lg",
			"url" : _sysUrlBase_+'/notas/mdl_altas_bajas_notas/?idarchivo='+idArchivo+'&orden=DESC&accion=altas_notas',
			"borrar" : true,
			"titulo" : MSJES_PHP.top_scores,
			"ventanaid": "altas_notas",
			"cerrarconesc" : false,
			"showfooter" : false,
		});
		return false;
	});

	$('#btn-options .btn-bajas-notas').click(function(e) {
		var idArchivo = $('#hIdArchivo').val();
		var $modal = sysmodal({
			"tam" : "lg",
			"url" : _sysUrlBase_+'/notas/mdl_altas_bajas_notas/?idarchivo='+idArchivo+'&orden=ASC&accion=bajas_notas',
			"borrar" : true,
			"titulo" : MSJES_PHP.bottom_scores,
			"ventanaid": "altas_notas",
			"cerrarconesc" : false,
			"showfooter" : false,
		});
		return false;
	});

	$('#btn-options .btn-desaprobados').click(function(e) {
		var idArchivo = $('#hIdArchivo').val();
		var $modal = sysmodal({
			"tam" : "lg",
			"url" : _sysUrlBase_+'/notas/mdl_altas_bajas_notas/?idarchivo='+idArchivo+'&accion=desaprobados',
			"borrar" : true,
			"titulo" : MSJES_PHP.disapproved_students,
			"ventanaid": "desaprobados",
			"cerrarconesc" : false,
			"showfooter" : false,
		});
		return false;
	});

	$('#btn-options .btn-aprobados').click(function(e) {
		var idArchivo = $('#hIdArchivo').val();
		var $modal = sysmodal({
			"tam" : "lg",
			"url" : _sysUrlBase_+'/notas/mdl_altas_bajas_notas/?idarchivo='+idArchivo+'&accion=aprobados',
			"borrar" : true,
			"titulo" : MSJES_PHP.approved_students,
			"ventanaid": "aprobados",
			"cerrarconesc" : false,
			"showfooter" : false,
		});
		return false;
	});

	//anchoNotasMain();
	$('#navHojas li').first().find('a[data-toggle="tab"]').tab('show');
});