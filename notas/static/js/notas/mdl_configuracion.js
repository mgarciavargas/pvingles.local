var guardarOrden  = function(from, to, el, mode) {
	console.log('from : ' , from, ' to : ' , to, ' el : ', el , ' mode : ', mode);
	var $table = $(el);
	var arrOrden = [];
	$table.find('tbody tr').each(function(index, row) {
		var nodo = {
			"idpestania": $(row).data('idpestania'),
			"orden": index+1
		};
		arrOrden.push(nodo);
	});

	$.ajax({
		url: _sysUrlBase_+'/notas_pestania/jxGuardarArray',
		type: 'POST',
		dataType: 'json',
		data: {"arrPestanias": arrOrden},
	}).done(function(resp) {
		if(resp.code=='ok') {
			console.log(resp.msj);
		} else {
			//algo salio mal
			console.log(resp.msj);
		}
	}).fail(function(err) {
		console.log("error : ", err);
	}).always(function() {
	});
};

var crearEspacioyTabla = function ($fila) {
	var idPestania = $fila.data('idpestania');
	var now = Date.now();
	var idTabla = 'tblColumnas_'+now;
	var strEspacio = '<tr class="collapsed-row" data-frompestania="'+idPestania+'">';
	strEspacio += '<td><div style="position:relative; padding-left:20px;">';
	strEspacio += '<h3>'+MSJES_PHP.columns+'</h3>';
	strEspacio += '<table class="table table-striped" id="'+idTabla+'">';
	strEspacio += '<tbody></tbody>';
	strEspacio += '<tfoot> <tr><td class="text-center">';
	strEspacio += '<button class="btn btn-success config-agregar-columna" data-tipopestania="C"><i class="fa fa-plus-circle"></i> '+MSJES_PHP.add_column+'</button>';
	strEspacio += '</td></tr> </tfoot>';
	strEspacio += '</table>';
	strEspacio += '</div></td>';
	strEspacio += '</tr>';

	$fila.after(strEspacio);

	return idTabla;
};

var draggerTable_Cols = null;
var cargarPestanias = function(dataSend, idTabla) {
	var $tabla = $('#'+idTabla);
	$.ajax({
		url: _sysUrlBase_+'/notas_pestania/buscarjson',
		type: 'GET',
		dataType: 'json',
		data: dataSend,
	}) .done(function(resp) {
		if(resp.code=="ok") {
			$tabla.find('tbody').html('');
			var tr_class = btn_edit_class = btn_delete_class = drag_handler_class = btn_view_columns = '';
			$.each(resp.data, function(i, col) {
				tr_class = col.tipo_pestania=='H'?'tr-hoja':'tr-columna';
				btn_edit_class = col.tipo_pestania=='H'?'btn-editar-hoja':'btn-editar-columna';
				btn_delete_class = col.tipo_pestania=='H'?'btn-eliminar-hoja':'btn-eliminar-columna';
				drag_handler_class = col.tipo_pestania=='H'?'drag_row':'drag_row_cols';
				btn_view_columns = col.tipo_pestania=='H'?'<button class="btn btn-xs btn-default btn-ver-hoja" title="'+MSJES_PHP.view_column+'"><i class="fa fa-chevron-down"></i></button>':'';

				let nuevaFila = '';
				nuevaFila += '<tr class="'+tr_class+'" data-idpestania="'+col.idpestania+'"><td>';
				nuevaFila += '<span><i class="fa fa-bars '+drag_handler_class+'"></i> &nbsp;</span>';
				nuevaFila += '<span class="nombre">'+col.nombre+'</span>';
				nuevaFila += '<div class="botones-accion pull-right">';
				nuevaFila += btn_view_columns;
				nuevaFila += '<button class="btn btn-xs btn-default color-blue '+btn_edit_class+'" title="'+MSJES_PHP.edit+'"><i class="fa fa-pencil "></i></button>';
				nuevaFila += '<button class="btn btn-xs btn-default color-red '+btn_delete_class+'" title="'+MSJES_PHP.delete+'"><i class="fa fa-trash "></i></button>';
				nuevaFila += '</div>';
				nuevaFila += '</td></tr>';

				$tabla.find('tbody').append(nuevaFila);
			});

			if(drag_handler_class!='') {
				draggerTable_Cols = tableDragger(document.querySelector("#"+idTabla),{
					mode: 'row',
					dragHandler: '.'+drag_handler_class,
					onlyBody: true,
					animation: 0
				});

				draggerTable_Cols.on('drop',function(from, to, el, mode){ 
					guardarOrden(from, to, el, mode); 
				});
			}

			resizemodal($('#'+idTabla).closest('.modal'),-20); /* funciones.js */
		} else {
			console.log('Error: ', resp.msj);
		}
	}) .fail(function(err) {
		console.log("error", err);
	}) .always(function() {});
};

var eliminarPestania = function (idPestania) {
	$.ajax({
		url: _sysUrlBase_+'/notas_pestania/jxEliminar/',
		type: 'POST',
		dataType: 'json',
		data: {"idpestania": idPestania},
	}).done(function(resp) {
		if(resp.code=="ok") {
			console.log('idpestania')
		} else {
			// cargando message => algo salio mal
		}
	}).fail(function(err) {
		console.log('error', err);
	}).always(function() {
	});
};

var cargarHojasTable = function() {
	var $table = $('#tblHojas');

	$table.find('tbody').html('');
	$.each(arrHojas, function(i, h) {
		let fila = '';
		fila += '<tr href="#" data-idpestania="'+h.idpestania+'" data-index="'+i+'"><td>';
		fila += '<span><i class="fa fa-bars drag_row"></i> &nbsp;</span>';
		fila += '<span>'+h.nombre+'</span>';
		fila += '<div class="botones-accion pull-right">';
		fila += '<button class="btn btn-xs btn-default btn-ver-hoja" title="'+MSJES_PHP.view_column+'"><i class="fa fa-chevron-down"></i></button>';
		fila += '<button class="btn btn-xs btn-default color-blue btn-editar-hoja" title="'+MSJES_PHP.edit+'"><i class="fa fa-pencil "></i></button>';
		fila += '<button class="btn btn-xs btn-default color-red btn-eliminar-hoja" title="'+MSJES_PHP.delete+'"><i class="fa fa-trash "></i></button>';
		fila += '</div>';
		fila += '</td></tr>';

		$table.find('tbody').append(fila);
	});
};

$(document).ready(function() {
	$('body').on('click', '#tblHojas .btn-ver-hoja', function(e){
		var $tr = $(this).closest('tr');

		if(!$tr.hasClass('collapsed')) {
			$('#tblHojas tr').removeClass('collapsed');
			$('#tblHojas tbody tr.collapsed-row').remove();
			draggerTable_Cols = null;

			$tr.addClass('collapsed');
			
			var idHoja = $(this).closest('tr').data('idpestania');
			var idTabla = crearEspacioyTabla($tr);
			cargarPestanias({"idpestania_padre": idHoja}, idTabla);
		} else {
			$('#tblHojas tr').removeClass('collapsed');
			$('#tblHojas tbody tr.collapsed-row').remove();
			draggerTable_Cols = null;
		}

		return false;
	}).on('click', '#tblHojas .btn-editar-hoja', function(e){
		var idPestania = $(this).closest('tr').data('idpestania');
		var $modal = sysmodal({
			"tam" : "md",
			"url" : _sysUrlBase_+'/notas_pestania/mdl_editar_pestania/?idpestania='+idPestania+'&fn=initEdit',
			"borrar" : true,
			"titulo" : MSJES_PHP.edit_sheet,
			"ventanaid": "config_editar_hoja",
			"cerrarconesc" : false,
			"showfooter" : false,
		});
		return false;
	}).on('click', '#tblHojas .btn-editar-columna', function(e){
		var idPestania = $(this).closest('tr').data('idpestania');
		var $modal = sysmodal({
			"tam" : "md",
			"url" : _sysUrlBase_+'/notas_pestania/mdl_editar_pestania/?idpestania='+idPestania+'&fn=initEdit',
			"borrar" : true,
			"titulo" : MSJES_PHP.edit_columns,
			"ventanaid": "config_editar_columna",
			"cerrarconesc" : false,
			"showfooter" : false,
		});
		return false;
	}).on('click', '#tblHojas .btn-eliminar-hoja', function(e) {
		var $btn = $(this);
		var idPestania = $btn.closest('tr').data('idpestania');
		$.confirm({
			title: $btn.attr('title'),
			content: MSJES_PHP.are_you_sure_delete_sheet_and_columns_scores,
			confirm: function(){
				eliminarPestania(idPestania);
				$btn.closest('tr').remove();
			},
		});
		return false;
	}).on('click', '#tblHojas .btn-eliminar-columna', function(e) {
		var $btn = $(this);
		var idPestania = $btn.closest('tr').data('idpestania');
		$.confirm({
			title: $btn.attr('title'),
			content: MSJES_PHP.are_you_sure_delete_column_and_scores,
			confirm: function(){
				eliminarPestania(idPestania);
				$btn.closest('tr').remove();
			},
		});
		return false;
	}).on('click', '.modal.config_editar_hoja .btn.guardar_pestania,.modal.config_editar_columna .btn.guardar_pestania', function(e) {
		let nombre = $(this).closest('.modal').find('#nombre').val();
		let idpestania = $(this).closest('.modal').find('#idpestania').val();
		$('#tblHojas tr[data-idpestania="'+idpestania+'"] span.nombre').text(nombre);
		return false;
	}).on('click', '#tblHojas button.config-agregar-hoja, #tblHojas button.config-agregar-columna', function(e) {
		let tipo_pestania = $(this).attr('data-tipopestania');
		let cantPestanias = (tipo_pestania=='H')?$('#tblHojas tbody>tr.tr-hoja').length:$('#tblHojas .collapsed-row table tbody>tr.tr-columna').length;
		let pestania_padre = (tipo_pestania=='C')?$('#tblHojas tbody>tr.tr-hoja.collapsed').attr('data-idpestania'):0;
		let orden = cantPestanias+1;

		var $modal = sysmodal({
			"tam" : "md",
			"url" :  _sysUrlBase_+'/notas_pestania/mdl_agregar_pestania/?tipo_pestania='+tipo_pestania+'&orden='+orden+'&idpestania_padre='+pestania_padre,
			//"html" : '#frm_agregar_pestania',
			"borrar" : true,
			"titulo" : (tipo_pestania=='C')?MSJES_PHP.edit_columns:MSJES_PHP.edit_sheet,
			"ventanaid": "config_editar_pestania",
			"cerrarconesc" : false,
			"showfooter" : false,
		});

		return false;
	}).on('click', '.config_editar_pestania .guardar_pestania', function(e) {
		var idArchivo = $('#hIdArchivo').val();
		var tipo_pestania = $(this).closest('.modal').find('#tipo_pestania').val();
		if(tipo_pestania=='H') {
			cargarPestanias({"tipo_pestania": 'H', "idarchivo": idArchivo} , 'tblHojas' );
		} else {
			var idTabla = $('#tblHojas tr.collapsed-row table').attr('id');
			var idHoja = $(this).closest('.modal').find('#idpestania_padre').val();
			cargarPestanias({"idpestania_padre": idHoja}, idTabla);
		}
		return false;
	}).on('hide.bs.modal', '.mdl_configuracion', function (e) {
		console.log('mdl_configuracion hidden');
		e.preventDefault();
		e.stopPropagation();
		if(!$('.jconfirm-box-container').length) {
			$.confirm({
				title: MSJES_PHP.settings,
				content: '<div class="col-xs-2 padding-0"><i class="fa fa-refresh fa-3x"></i></div><div class="col-xs-10 padding-0">'+MSJES_PHP.page_refresh_to_see+'</div>',
				autoClose: 'confirm|10000',
				//cancelButtonClass: 'hidden',
				confirm: function(){
					window.location.reload(false);
				}
			});
		} 
	});
});