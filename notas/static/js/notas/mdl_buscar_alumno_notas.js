'use strict';

var crearHojaNotas = function(hoja) {
	var str = '';
	str += '<div class="panel" style="border-color:'+hoja.color+'">';
	str += '<div class="panel-heading" style="background-color:'+hoja.color+'">';
	str += '<span>'+MSJES_PHP.sheet+': '+hoja.nombre+'</span>';
	str += '</div>';
	str += '<div class="panel-body">';
	str += '<div class="col-sm-offset-2 col-sm-8 table-wrapper">';
	str += '<table class="table table-striped">';
	str += '<tbody>';
	$.each(hoja.columnas, function(j, col) {
		str += '<tr>';
		str += '<td class="key-col" style="background-color:'+col.color+'"><span>'+col.nombre+''+(col.abreviatura!=null?' ('+col.abreviatura+')':'')+'</span></td>';
		
		var value_col = '';
		if(col.tipo_info!='E' && col.tipo_info!='T') {
			value_col = (col.nota!=null && col.nota.nota_num!=null)?col.nota.nota_num:'0.00';
		}
		str += '<td class="value-col">'+((col.tipo_info=='E'||col.tipo_info=='T')&&col.nota!=null&&col.nota.nota_txt!=null?col.nota.nota_txt:'')+' '+value_col+'</td>';

		var clsAprobado = '',
			aprobado_col = '-';
		if(col.nota!=null && col.nota.aprobado!=null) {
			clsAprobado = col.nota.aprobado=='1'?'aprobado':'desaprobado';
			aprobado_col = col.nota.aprobado=='1'?MSJES_PHP.pass:MSJES_PHP.fail;
		}
		str += '<td class="aprobado-col '+clsAprobado+'">'+aprobado_col+'</td>';
		str += '</tr>';
	});
	str += '</tbody>';
	str += '</table>';
	str += '</div>'; // .table-wrapper
	str += '</div>'; // .panel-body
	str += '</div>'; // .panel
	return str;
};

$(document).ready(function() {
	$('body')
	.on('change', '#mdl_buscar_alumno_notas #opcAlumno', function(e) {
		var idAlumno = $(this).val();
		var nombreAlumno = $(this).find('option:selected').text();
		$.ajax({
			url: _sysUrlBase_+'/notas/jxGetNotasxAlumno',
			type: 'GET',
			dataType: 'json',
			data: {
				"idarchivo": $('#hIdArchivo').val(),
				"idalumno" : idAlumno
			},
			beforeSend : function() {
				$('#resultado-buscar-alumno').html('<div class="text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><p>'+MSJES_PHP.loading+'</p></div>');
			}
		}).done(function(resp) {
			if(resp.code=='ok') {
				$('#resultado-buscar-alumno').html('<h2 class="nombre-alumno text-center">'+nombreAlumno+'</h2>');
				$.each(resp.data, function(i, hoja) {
					let strPanelNotas = crearHojaNotas(hoja);
					$('#resultado-buscar-alumno').append(strPanelNotas);
				});
			} else {

			}
		}).fail(function(err) {
			console.log("error: " , err);
		}).always(function() {});

	});
});