var fnCallback_mdl = function () {
	/* executed when "Importar alumnos" modal is loaded */
	$('.modal.agregar_alumnos #opcAgregarDesde option[value="A"]').addClass('hidden');
};

$(document).ready(function() {
	cargarMensajesPHP($("#mensajes_idioma"));

	$(".btn.agregar-archivo").click(function(e) {
		e.preventDefault();
		var $modal = sysmodal({
			"tam" : "lg",
			"url" : _sysUrlBase_+'/notas_alumno/mdl_importar_alumnos/?acc=listar',
			"borrar" : true,
			"titulo" : MSJES_PHP.add_students,
			"ventanaid": "agregar_alumnos",
			"cerrarconesc" : false,
			"showfooter" : false,
		});
    });

    $("#listar-archivos").on("click", ".elem-archivo .btn.eliminar" , function(e) {
    	e.preventDefault();
    	var idArchivo = $(this).attr('data-idarchivo');
    	var $archivo = $(this).closest('.elem-archivo');
    	$.confirm({
			title: MSJES_PHP.delete,
			content: MSJES_PHP.are_you_sure_delete,
			confirmButton: MSJES_PHP.accept,
			cancelButton: MSJES_PHP.cancel,
			confirmButtonClass: 'btn-danger',
			cancelButtonClass: 'btn-default',
			confirm: function(){
				$.ajax({
		    		url: _sysUrlBase_ + '/notas_archivo/xEliminar_cascada',
		    		type: 'POST',
		    		dataType: 'json',
		    		data: {"idarchivo": idArchivo},
		    	}).done(function(resp) {
		    		if ( resp.code=="ok" ) {
		    			$archivo.remove();
		    			$("#div_vacio").removeClass('hidden');
		    		} else {
		    			mostrar_notificacion(MSJES_PHP.error, resp.msj, 'error');
		    		}
		    	}).fail(fnAjaxFail).always(function() {});
			},
		});
    });

});