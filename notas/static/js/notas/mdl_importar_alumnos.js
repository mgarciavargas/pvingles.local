var frmNuevoArchivo = function(data) {
	var nombre_arch = data.curso.nombre || "";
	var imgCurso = data.curso.imagen || _sysUrlStatic_+'/img/sistema/defecto.png';
	$.confirm({
		title: MSJES_PHP.give_a_name,
		content: '' +
		'<form class="form-horizontal" id="frmAgregarArchivo">'+
		'<div class="col-xs-6 col-xs-offset-3 image-selector">'+
		'<input type="file" name="fileImagen" id="fileImagen" accept="image/*">'+
		'<img src="'+imgCurso+'" alt="cover_file" class="img-responsive center-block thumbnail imagen_cover">'+
		'</div>'+
		'<div class="col-xs-12 form-group">'+
		'<input type="text" name="txtNombre" id="txtNombre" class="form-control" placeholder="'+MSJES_PHP.name+'" value="'+nombre_arch+'">'+
		'</div>'+
		'</form>',
		confirmButton: MSJES_PHP.accept,
		cancelButton: MSJES_PHP.cancel,
		confirmButtonClass: 'btn-success',
		cancelButtonClass: 'btn-default',
		confirm: function(){
			var nombreArchivo = $('#frmAgregarArchivo #txtNombre').val().trim();
			if (nombreArchivo=="") { $('#frmAgregarArchivo #txtNombre').focus(); return false; }
			$.ajax({
				url: _sysUrlBase_+'/notas_archivo/xGuardarConAlumnos',
				type: 'POST',
				dataType: 'json',
				data: { 
					'arrAlumnos' : data.arrAlumnos,
					'archivo_nombre' : nombreArchivo,
					'archivo_imagen' : $('#frmAgregarArchivo img.imagen_cover').attr('src'),
					/*'archivo_identificador' : data.curso.idcurso,*/
					'archivo_identificador' : data.curso.idgrupoauladetalle,
				},
			}).done(function(resp) {
				if(resp.code="ok"){
					redir(_sysUrlBase_+'/notas/editar/?idarchivo='+resp.newid);
				}else{
					mostrar_notificacion(MSJES_PHP.error, resp.msj, 'error');
				}
			}).fail(fnAjaxFail);
		},
	});
};

var agregarAlumnos = function(arrAlumnos, idArchivo) {
	console.log(arrAlumnos);
	console.log('idArchivo =', idArchivo);
	$.ajax({
		url: _sysUrlBase_+'/notas_alumno/xAgregar',
		type: 'POST',
		dataType: 'json',
		data: {"arrAlumnos": arrAlumnos, "idArchivo": idArchivo},
	}).done(function(resp) {
		if (resp.code=='ok') {
			location.reload();
		} else {
			mostrar_notificacion(MSJES_PHP.error, resp.msje, 'error');
		}
	}).fail(fnAjaxFail).always(function() {
		console.log("complete");
	});
	
};

var X = XLSX;
//var XW = {msg: 'xlsx',worker: './xlsxworker.js'};

var global_wb;
var process_wb = (function() {
	/*var OUT = document.getElementById('out');*/
	var to_json = function to_json(workbook) {
		var result = {};
		workbook.SheetNames.forEach(function(sheetName) {
			var roa = X.utils.sheet_to_json(workbook.Sheets[sheetName], {header:1});
			if(roa.length) result[sheetName] = roa;
		});
		return JSON.stringify(result, 2, 2);
	};

	return function process_wb(wb) {
		global_wb = wb;
		var output = to_json(wb);
		if(_isJson(output)){
			var jOutput=JSON.parse(output);
			$contenedor = $('.modal #XLS .edita-alumnos');
			$contenedor.find('.nav-tabs').html('');
			$contenedor.find('.tab-content').html('');

			$.each(jOutput, function(key_hoja, arrFilas) {
				idHoja = key_hoja.replace(/ /gi, "_");
				var comboCelda = '<div class="form-group" title="'+MSJES_PHP.cannot_be_repeated+'"><select class="form-control campo_celda">'+
					'<option value="">- '+MSJES_PHP.ignore+' -</option>'+
					'<option value="apellidos">'+MSJES_PHP.last_name+'</option>'+
					'<option value="nombres">'+MSJES_PHP.name+'</option>'+
					'<option value="identificador">'+MSJES_PHP.identification+'</option>'+
				'</select></div>';
				var new_tab  = '<li><a href="#hoja_'+idHoja+'" data-toggle="tab">'+key_hoja+'</a></li>';
				var new_tabPane  = '<div class="tab-pane fade" id="hoja_'+idHoja+'">';
				new_tabPane += '<table id="tblAlumnos_'+idHoja+'" class="table table-striped table-hover"> ';
				new_tabPane += ' <thead></thead> ';
				new_tabPane += ' <tbody></tbody> ';
				new_tabPane += '</table></div>';
				$contenedor.find('.nav-tabs').append(new_tab);
				$contenedor.find('.tab-content').append(new_tabPane);

				var $table = $('.modal #XLS #tblAlumnos_'+idHoja);
				$.each(arrFilas, function(i, arrCeldas) {
					if (i == 0) {
						var checkBox = '<th class="input-chk"><input type="checkbox" class="checkbox-ctrl check-all"></th>';
						var arrHtml = arrCeldas.map(function(celda){ return '<th class="text-center">'+comboCelda+'<span>'+celda+'</span></th>'; });
						var tagFind = 'thead';
					} else {
						var checkBox = '<td class="input-chk"><input type="checkbox" class="checkbox-ctrl"></td>';
						var arrHtml = arrCeldas.map(function(celda){ return '<td>'+celda+'</td>'; });
						var tagFind = 'tbody';
					}
					var str_html = arrHtml.join('');
					$table.find(tagFind).append('<tr>'+checkBox+str_html+'</tr>');
				});
				$table.find('.checkbox-ctrl.check-all').trigger('click');
			});
			$('.modal #XLS .edita-alumnos .nav-tabs a').first().trigger('click');
			$('.modal #XLS .edita-alumnos').removeClass('hidden');
		}
	};
})();

var do_file = (function() {
	var rABS = typeof FileReader !== "undefined" && (FileReader.prototype||{}).readAsBinaryString;
	var use_worker = typeof Worker !== 'undefined';
	return function do_file(files) {
	    rABS = true;//domrabs.checked;
	    use_worker = true; //domwork.checked;
	    var f = files[0];
	    var reader = new FileReader();
	    reader.onload = function(e) {
	    	if(typeof console !== 'undefined') {/*console.log("onload", new Date(), rABS, use_worker);*/}
	    	var data = e.target.result;
	    	if(!rABS) data = new Uint8Array(data);

	    	else process_wb(X.read(data, {type: rABS ? 'binary' : 'array'}));
	    };
	    if(rABS) reader.readAsBinaryString(f);
	    else reader.readAsArrayBuffer(f);
	};
})();

var subirmedia=function(tipo, $file){
	if($file[0].files[0]===undefined) { return false; }
	var formData = new FormData();
	formData.append("tipo", tipo);
	formData.append("filearchivo", $file[0].files[0]);
	var $img = $file.closest('.image-selector').find('img');
	$.ajax({
		url: _sysUrlBase_+'/notas_archivo/subirarchivo',
		type: "POST",
		data:  formData,
		contentType: false,
		processData: false,
		dataType :'json',
		cache: false,
		beforeSend: function(XMLHttpRequest){
	        $img.attr('src', _sysUrlStatic_+'/img/sistema/loading.gif');
		}
	}).done(function(resp) {
		if(resp.code="ok"){
			$img.attr('src', resp.ruta);
		}else{
			mostrar_notificacion(MSJES_PHP.error, resp.msj, 'error');
		}
	}).fail(fnAjaxFail);
};

var initModal = function(){
	var $modal = $('#mdl_importar_alumnos').closest('.modal');
	$modal.addClass('modal_importar_alumnos');
	if (typeof fnCallback_mdl!=="undefined" && $.isFunction(fnCallback_mdl)) { 
		fnCallback_mdl(); 
	}
};

$(document).ready(function() {
	initModal();

	$('body').on('change', '.modal #mdl_importar_alumnos #opcAgregarDesde', function(e) {
		var value = $(this).val();
		var $modal = $(this).closest('.modal');
		$modal.find('.content > *').addClass('hidden');
		$modal.find('.content #'+value).removeClass('hidden');
		$modal.find('input[name=fileArchivo]').attr('id', 'fileArchivoXLS');

		var xlf = document.getElementById('fileArchivoXLS');
		if(!xlf.addEventListener) return;
		function handleFile(e) { do_file(e.target.files); }
		xlf.addEventListener('change', handleFile, false);

		resizemodal($modal,-20);
	}).on('click', '.modal #mdl_importar_alumnos .btn.cargar-cursos', function(e) {
		e.preventDefault();
		var $modal = $(this).closest('.modal');
		$(this).closest('.botones').addClass('hidden');
		$modal.find('.edita-cursos').removeClass('hidden');
		$.ajax({
			url: _sysUrlBase_ + '/../service/cursos_docente',
			type: 'GET',
			dataType: 'json',
			data: { iddocente: $("#hIdDocente").val(), },
			beforeSend: function() {
				$modal.find('.btns-cursos').html('<div class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i> '+MSJES_PHP.loading+'...</div>');
			},
		}).done(function(resp) {
			if(resp.code==200){
				$modal.find('.btns-cursos').html('');
				var html_cursos = "";
				var cursos = resp.data;
				if(cursos.length>0) {
					$.each(cursos, function(i, c) {
						html_cursos += '<div class="col-xs-12 col-sm-6 col-md-4">';
						html_cursos += '<button class="btn center-block cargar_alumnos" title="'+c.nombre+'" data-idcurso="'+c.idcurso+'" data-idgrupoauladetalle="'+c.idgrupoauladetalle+'">';
						html_cursos += '<img src="'+c.imagen+'" alt="fondo" class="img-responsive center-block">';
						html_cursos += '<span class="bolder">'+c.nombre+'</span>';
						html_cursos += '</button> </div>';
					});
					$modal.find('.btns-cursos').append(html_cursos);
				} else {
					$modal.find('.btns-cursos').html('<div class="col-xs-12 text-center"><i class="fa fa-minus-circle fa-4x"></i></div> <div class="col-xs-12 text-center bolder">'+MSJES_PHP.no_courses_found+'</div>');
				}
			} else {
				mostrar_notificacion(MSJES_PHP.error, resp.message, "error");
			}
		}).fail(fnAjaxFail);
	}).on('click', '.modal #mdl_importar_alumnos .edita-cursos .btn.atras', function(e) {
		e.preventDefault();
		var $modal = $(this).closest('.modal');
		$modal.find('.botones').removeClass('hidden');
		$modal.find('.edita-cursos').addClass('hidden');
	}).on('click', '.modal #mdl_importar_alumnos .edita-cursos .btn.cargar_alumnos', function(e) {
		e.preventDefault();
		var $modal = $(this).closest('.modal');
		var idCurso = $(this).attr('data-idcurso');
		var idGrupoAulaDet = $(this).attr('data-idgrupoauladetalle');
		var imgCurso = $(this).find('img.img-responsive').attr('src');
		var $tblAlumnos = $modal.find('.edita-alumnos #tblAlumnos');
		$modal.find('.edita-alumnos').removeClass('hidden');
		$modal.find('.edita-cursos').addClass('hidden');

		$.ajax({
			url: _sysUrlBase_ + '/../service/alumnos_curso',
			type: 'GET',
			dataType: 'json',
			data: {
				iddocente: $("#hIdDocente").val(), 
				idcurso: idCurso,
			},
		}).done(function(resp) {
			if (resp.code==200) {
				var alumnos = resp.data;
				var html_alums = "";
				$.each(alumnos, function(i, a) {
					html_alums += '<tr data-idalumno="'+a.idalumno+'">';
					html_alums += '<td class="input-chk"><input type="checkbox" class="checkbox-ctrl" id="chk_'+a.idalumno+'" name="chk_'+a.idalumno+'"></td>';
					html_alums += '<td class="apellidos">'+ a.stralumno.split(",")[0] +"</td>";
					html_alums += '<td class="nombres">'+ a.stralumno.split(",")[1] +"</td>";
					html_alums += '<td class="curso">'+ a.strcurso +"</td>";
					html_alums += '<td class="local">'+ a.strlocal +"</td>";
					html_alums += '</tr>';
				});
				$tblAlumnos.attr({
					'data-idcurso': idCurso, 
					'data-idgrupoauladetalle': idGrupoAulaDet,
					'data-imgcurso': imgCurso,
				});
				$tblAlumnos.find('tbody').html(html_alums);
				$tblAlumnos.find('input.checkbox-ctrl').prop('checked', true);
			} else {
				mostrar_notificacion(MSJES_PHP.error, resp.message, "error");
			}
		}).fail(fnAjaxFail);
	}).on('click', '.modal #mdl_importar_alumnos .edita-alumnos .btn.atras', function(e) {
		e.preventDefault();
		var $modal = $(this).closest('.modal');
		$modal.find('.edita-cursos').removeClass('hidden');
		$modal.find('.edita-alumnos').addClass('hidden');
	}).on('change', '.modal #mdl_importar_alumnos .checkbox-ctrl.check-all', function(e) {
		e.preventDefault();
		var isChecked = $(this).is(':checked');
        var $table = $(this).closest('table');
        var $item = $table.find('input.checkbox-ctrl').prop('checked', isChecked);
	}).on('click', '.modal #mdl_importar_alumnos .guardar_alumnos', function(e) {
		e.preventDefault();
		var $modal = $(this).closest('.modal');
		var opcAgregarDesde = $modal.find('#opcAgregarDesde').val();
		if(!opcAgregarDesde){ $.alert(MSJES_PHP.you_must_select_option, MSJES_PHP.error); return false; }

		var arrAlumnos = [];
		var jCurso = {};
		switch(opcAgregarDesde){
			case 'XLS' :
				if ($modal.find('#XLS table .has-error').length) { 
					$.alert(MSJES_PHP.correct_fields_red+'.<br>'+MSJES_PHP.cannot_be_repeated+'.', MSJES_PHP.error);
					var idTabPane = $modal.find('#XLS table .has-error').first().closest('.tab-pane').attr('id');
					$modal.find('#XLS .nav-tabs li a[href="#'+idTabPane+'"]').trigger('click');
					return false;
				} 

				$modal.find('#XLS .edita-alumnos .tab-pane table').each(function(i, tabla) {
					var $table = $(tabla);
					var arrHeader = [];
					$table.find('thead tr th select.campo_celda').each(function(j, comboBox) {
						/* Obteniendo orden del encabezado */
						var value = $(comboBox).val();
						arrHeader.push(value);
					});

					if(arrHeader.length){
						$table.find('tbody tr').each(function(k, fila) {
							var $fila = $(fila);
							if ($fila.find("input.checkbox-ctrl").is(':checked')) {
								var new_alum = {};
								$fila.find('td').not('.input-chk').each(function(index, celda) {
									/* recorrer c/celda de la fila para asociarlo con su columna encabezado */
									if(arrHeader[index]!=""){ 
										new_alum[ arrHeader[index] ] = $(celda).text(); 
										new_alum["origen"] = 'XLS';
									}
								});
								if ( !$.isEmptyObject(new_alum) ) { arrAlumnos.push(new_alum); }
							}
						});
					}
				});
				if(!arrAlumnos.length){ $.alert(MSJES_PHP.there_are_no_students+'.<br>'+MSJES_PHP.or_ignoring_columns, MSJES_PHP.error); return false;}
				break;
			case 'SL' : 
				var posible_nombre_archivo = "";
				var id_curso = $modal.find("#SL #tblAlumnos").attr('data-idcurso');
				var id_grupAulaDet = $modal.find("#SL #tblAlumnos").attr('data-idgrupoauladetalle');
				var img_curso = $modal.find("#SL #tblAlumnos").attr('data-imgcurso');
				$modal.find("#SL #tblAlumnos tbody tr").each(function(i, tr) {
					var $fila = $(tr);
					if ($fila.find("input.checkbox-ctrl").is(':checked')) {
						var new_alum = {
							"identificador" : $fila.attr('data-idalumno'),
							"apellidos" : $fila.find('.apellidos').text(),
							"nombres" : $fila.find('.nombres').text(),
							"origen" : "SMARTLEARN",
						}
						arrAlumnos.push(new_alum);
					}
					posible_nombre_archivo = $fila.find('.curso').text()+' - '+$fila.find('.local').text();
				});
				if(!arrAlumnos.length){ $.alert(MSJES_PHP.there_are_no_students, MSJES_PHP.error); return false;}
				jCurso = {
					'idcurso' : id_curso,
					'idgrupoauladetalle' : id_grupAulaDet,
					'nombre' : posible_nombre_archivo,
					'imagen' : img_curso,
				};
				break;
			case 'A' :
				//... 
				break;
		}

		if ( $('#hIdArchivo').length && $('#hIdArchivo').val()!="" ) {
			agregarAlumnos(arrAlumnos, $('#hIdArchivo').val());
		} else {
			frmNuevoArchivo({
				'arrAlumnos':arrAlumnos, 
				'curso': jCurso, 
			});
		}
	}).on('change', '.modal #mdl_importar_alumnos #XLS select.campo_celda', function(e) {
		var $thead = $(this).closest('thead');
		$thead.find('select.campo_celda').each(function(index, select) {
			var $this = $(select);
			var valor = $this.val();
			if(valor=="") { $this.closest('.form-group').removeClass('has-error'); }

			/* $this se comparará con los otros <select> y evalua si ya hay otro con su valor */
			$thead.find('select.campo_celda').not($this).each(function(i, otro_select) {
				var otro_valor = $(otro_select).val();
				if ( valor!="" && otro_valor!="" ) {
					if ( valor==otro_valor ){
						$this.closest('.form-group').addClass('has-error');
						$(otro_select).closest('.form-group').addClass('has-error');
						return false;
					} else {
						$this.closest('.form-group').removeClass('has-error');
					}
				}
			});
		});
	}).on('change', '#frmAgregarArchivo input#fileImagen', function(e) {
		subirmedia('image' , $(this));
	});
});