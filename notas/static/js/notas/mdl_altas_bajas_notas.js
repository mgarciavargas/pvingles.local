'use strict';
var colorPuesto = {
	"1" : "#ffbf00",
	"2" : "#cacbce",
	"3" : "#bb714c",
};
var crearTablaPuestos = function(puestos) {
	var strTabla = '';
	strTabla += '<table class="table table-striped">';
	strTabla += '<tbody>';
	$.each(puestos, function(index, p) {
		strTabla += '<tr>';
		if($('#hOrden').val()=='DESC'){
			strTabla += '<td style="color: '+colorPuesto[index+1]+'; font-size:1.5em"><strong>'+(index+1)+'</strong> <i class="fa fa-trophy"></i></td>';
		}
		strTabla += '<td>'+p.alumno_apellidos+' '+p.alumno_nombres+'</td>';
		strTabla += '<td>'+(p.tipo_info=='E'?p.nota_txt+' - ':'')+p.nota_num+'</td>';
		strTabla += '</tr>';
	});
	strTabla += '</tbody>';
	strTabla += '</table>';
	$('#resultado-altas-bajas-notas #resultado').html(strTabla);
};

var altas_bajas_notas = function(idPestania, nombrePestania){
	$.ajax({
		url: _sysUrlBase_+'/notas/jxGetTopNotas',
		type: 'GET',
		dataType: 'json',
		data: {
			"idpestania" : idPestania,
			"orden" : $('#hOrden').val(),
		},
		beforeSend : function() {
			$('#resultado-altas-bajas-notas').removeClass('hidden');
			$('#resultado-altas-bajas-notas #resultado').html('<div class="text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><p>'+MSJES_PHP.loading+'</p></div>');
		}
	}).done(function(resp) {
		if(resp.code=='ok') {
			let titulo = $('#hOrden').val()=='DESC'?MSJES_PHP.top_scores:MSJES_PHP.bottom_scores;
			$('#resultado-altas-bajas-notas h2').text(titulo);
			crearTablaPuestos(resp.data);
		} else {

		}
	}).fail(function(err) {
		console.log("error: " , err);
	}).always(function() {});
};

var crearTablaAprobadosYDesaprobados = function(notas) {
	var strTabla = '';
	var min_pass = '';
	if(notas.length) {
		var info_valor = JSON.parse(notas[0].info_valor);
		if(notas[0].tipo_info!='T' || (notas[0].tipo_info=='E' && info_valor.min_pass!=null) ){
			min_pass = info_valor.min_pass;
		}
	}
	strTabla += '<table class="table table-striped">';
	strTabla += '<caption class="text-center"><strong>'+MSJES_PHP.minimun_pass+':</strong> '+min_pass+'</caption>';
	strTabla += '<tbody>';
	$.each(notas, function(index, n) {
		strTabla += '<tr>';
		strTabla += '<td>'+n.alumno_apellidos+' '+n.alumno_nombres+'</td>';
		strTabla += '<td>'+(n.tipo_info=='E'?n.nota_txt+' - ':'')+n.nota_num+'</td>';
		strTabla += '</tr>';
	});
	strTabla += '</tbody>';
	strTabla += '</table>';
	$('#resultado-altas-bajas-notas #resultado').html(strTabla);
};

var aprobados_desaprobados = function(idPestania, isAprobado) {
	$.ajax({
		url: _sysUrlBase_+'/notas/jxGetAprobadosDesaprobados',
		type: 'GET',
		dataType: 'json',
		data: {
			"idpestania": idPestania,
			"aprobado": isAprobado,
		},
		beforeSend : function() {
			$('#resultado-altas-bajas-notas').removeClass('hidden');
			$('#resultado-altas-bajas-notas #resultado').html('<div class="text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><p>'+MSJES_PHP.loading+'</p></div>');
		}
	}).done(function(resp) {
		if(resp.code == 'ok') {
			let titulo = $('#hAccion').val()=='aprobados'?MSJES_PHP.approved_students:MSJES_PHP.disapproved_students;
			crearTablaAprobadosYDesaprobados(resp.data);
			$('#resultado-altas-bajas-notas h2').text(titulo);
		} else {

		}
	}).fail(function(err) {
		console.log("error : ", err);
	}).always(function() {});
};

$(document).ready(function() {
	$('body')
	.on('change', '#mdl_altas_bajas_notas #opcPestania', function(e) {
		var accion = $('#hAccion').val();
		var idPestania = $(this).val();
		var nombrePestania = $(this).find('option:selected').text();
		if(accion == 'altas_notas' || accion == 'bajas_notas') {
			altas_bajas_notas(idPestania, nombrePestania);
		} else if(accion == 'aprobados' || accion == 'desaprobados') {
			var isAprobado = $('#hAccion').val()=='aprobados'?'1':'0';
			aprobados_desaprobados(idPestania, isAprobado);
		}
	});
});