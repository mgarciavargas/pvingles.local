<?php
/**
 * @autor       Abel Chingo Tello, ACHT
 * @fecha       08/09/2016
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR. 'ini_app.php');
require_once(RUTA_LIBS . 'xajax/xajax_core/xajaxAIO.inc.php');

global $aplicacion;
$aplicacion->iniciar();
$xajax = new xajax();
$xajax->setRequestURI(URL_BASE.'/ServerxAjax.php');
$xajax->setCharEncoding('UTF-8');
$xajax->configure('decodeUTF8Input', true);
//$xajax->setFlag("debug", true);
$xajax->configure('defaultMode', 'synchronous');
$xajax->registerFunction(array('_', 'WebAdminxAjax', 'call'), RUTA_INC . 'cls.WebAdminxAjax.php');
$xajax->processRequest();