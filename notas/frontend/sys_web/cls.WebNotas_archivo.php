<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		30-01-2018 
 * @copyright	Copyright (C) 30-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas_archivo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_pestania', RUTA_BASE, 'sys_negocio');
class WebNotas_archivo extends JrWeb
{
	private $oNegN_archivo;
	private $oNegN_alumno;
	private $oNegN_pestania;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegN_archivo = new NegNotas_archivo;
		$this->oNegN_alumno = new NegNotas_alumno;
		$this->oNegN_pestania = new NegNotas_pestania;
	}

	public function defecto(){
		/*return $this->listado();*/
	}

/*
	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_archivo', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idarchivo"])&&@$_REQUEST["idarchivo"]!='')$filtros["idarchivo"]=$_REQUEST["idarchivo"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["imagen"])&&@$_REQUEST["imagen"]!='')$filtros["imagen"]=$_REQUEST["imagen"];
			if(isset($_REQUEST["identificador"])&&@$_REQUEST["identificador"]!='')$filtros["identificador"]=$_REQUEST["identificador"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["fechareg"])&&@$_REQUEST["fechareg"]!='')$filtros["fechareg"]=$_REQUEST["fechareg"];
			
			$this->datos=$this->oNegN_archivo->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Notas_archivo'), true);
			$this->esquema = 'notas_archivo-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_archivo', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Notas_archivo').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_archivo', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegN_archivo->idarchivo = @$_GET['id'];
			$this->datos = $this->oNegN_archivo->dataNotas_archivo;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Notas_archivo').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'notas_archivo-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/

// ========================== Funciones ajax ========================== //

	public function buscarjson() {
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_archivo', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idarchivo"])&&@$_REQUEST["idarchivo"]!='')$filtros["idarchivo"]=$_REQUEST["idarchivo"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["imagen"])&&@$_REQUEST["imagen"]!='')$filtros["imagen"]=$_REQUEST["imagen"];
			if(isset($_REQUEST["identificador"])&&@$_REQUEST["identificador"]!='')$filtros["identificador"]=$_REQUEST["identificador"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["fechareg"])&&@$_REQUEST["fechareg"]!='')$filtros["fechareg"]=$_REQUEST["fechareg"];
						
			$this->datos=$this->oNegN_archivo->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarNotas_archivo() {
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if(empty($_POST)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit(0);            
			}
			@extract($_POST);
			$accion='_add';            
			if(!empty(@$pkIdarchivo)) {
				$this->oNegN_archivo->idarchivo = $frm['pkIdarchivo'];
				$accion='_edit';
			}
			$usuarioAct = NegSesion::getUsuario();

			$this->oNegN_archivo->nombre=@$txtNombre;
			$this->oNegN_archivo->imagen=@$txtImagen;
			$this->oNegN_archivo->identificador=@$txtIdentificador;
			$this->oNegN_archivo->iddocente=@$txtIddocente;
			$this->oNegN_archivo->fechareg=@$txtFechareg;

			if($accion=='_add') {
				$arrIdAlums=$this->oNegN_archivo->agregar();
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_archivo')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
			}else{
				$res=$this->oNegN_archivo->editar();
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_archivo')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
			}
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function xGuardarConAlumnos() {
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if(empty($_POST)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit(0);            
			}
			@extract($_POST);
			$accion='_add';
			if(!empty(@$idarchivo)) {
				$this->oNegN_archivo->idarchivo = $idarchivo;
				$accion='_edit';
			}
			$usuarioAct = NegSesion::getUsuario();

			$this->oNegN_archivo->nombre=@$archivo_nombre;
			$this->oNegN_archivo->imagen=@str_replace($this->documento->getUrlBase(), '__xRUTABASEx__', @$archivo_imagen) ;
			$this->oNegN_archivo->identificador=@$archivo_identificador;
			$this->oNegN_archivo->iddocente=@$usuarioAct["dni"];
			$this->oNegN_archivo->fechareg=@date('Y-m-d H:i:s');

			if($accion=='_add') {
				$res=$this->oNegN_archivo->agregar();
				$this->agregarAlumnos($arrAlumnos, $res);
				$this->crearPrimeraHoja($res);
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_archivo')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
			} else {
				$res=$this->oNegN_archivo->editar();
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_archivo')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
			}
			
            exit(0);
		} catch (Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}

	public function subirarchivo() {
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;	
			if(empty($_POST["tipo"])||empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit();
			}
			$folder = 'img';
			$file=$_FILES["filearchivo"];

			$ext=@strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
			$intipo='';
			$tipo=@$_POST["tipo"];
			if($tipo=='image'){
				$intipo=array('jpg','jpeg','png','bmp', 'gif');
				$carpeta_tipo = 'notas_archivos';
			}

			if(!in_array($ext, $intipo)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
				exit(0);
			}
			$newname=date('YmdHis').'_'.$file['name'];
			$directorio = RUTA_BASE . 'static' . SD . $folder . SD . $carpeta_tipo ;
			@mkdir($directorio, '0777', true);
			if(move_uploaded_file($file["tmp_name"], $directorio.SD.$newname)){
				echo json_encode(array(
					'code'=>'ok', 
					'msj'=>JrTexto::_('File uploaded successfully'),
					'ruta'=>$this->documento->getUrlStatic().'/'.$folder.'/'.$carpeta_tipo.'/'.$newname,
					'nombre'=>$file["name"],
					'tipo'=>$tipo,
				));
				exit(0);
			}
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error tryin to upload the file').'. '.$e->getMessage()));
			exit(0);
		}
	}

	public function xEliminar_cascada() {
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			if(empty($_POST)){
				throw new Exception( JrTexto::_('data incomplete') );         
			}
			@extract($_POST);
			$this->oNegN_archivo->idarchivo = @$idarchivo;
			$resp = $this->oNegN_archivo->eliminar_cascada();

			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Delete')).' '.JrTexto::_('successfully'),'data'=>$resp));
            exit(0);
		} catch (Exception $e) {
			echo json_encode(array( 'code'=>'Error','msj'=>$e->getMessage() ));
			exit(0);
		}
	}

	
// ========================== Funciones xajax ========================== //
	public function xSaveNotas_archivo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdarchivo'])) {
					$this->oNegN_archivo->idarchivo = $frm['pkIdarchivo'];
				}
				
				$this->oNegN_archivo->nombre=@$frm["txtNombre"];
				$this->oNegN_archivo->imagen=@$frm["txtImagen"];
				$this->oNegN_archivo->identificador=@$frm["txtIdentificador"];
				$this->oNegN_archivo->iddocente=@$frm["txtIddocente"];
				$this->oNegN_archivo->fechareg=@$frm["txtFechareg"];

				if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegN_archivo->agregar();
				}else{
					$res=$this->oNegN_archivo->editar();
				}
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegN_archivo->idarchivo);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}

			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDNotas_archivo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegN_archivo->__set('idarchivo', $pk);
				$this->datos = $this->oNegN_archivo->dataNotas_archivo;
				$res=$this->oNegN_archivo->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegN_archivo->__set('idarchivo', $pk);
				$res=$this->oNegN_archivo->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegN_archivo->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	    


// ========================== Funciones Privadas ========================== //
	private function agregarAlumnos($arrAlumnos = array(), $idArch=null)
	{
		try {
			if (empty($arrAlumnos)) { throw new Exception(JrTexto::_("There are no students to import")); }
			$arrAlumnosIDs = array();
			foreach ($arrAlumnos as $i => $a) {
				$this->oNegN_alumno->nombres= @$a["nombres"];
				$this->oNegN_alumno->apellidos= @$a["apellidos"];
				$this->oNegN_alumno->identificador= @$a["identificador"];
				$this->oNegN_alumno->idarchivo= $idArch;
				$this->oNegN_alumno->origen= $origen;
				$this->oNegN_alumno->fechareg= @date('Y-m-d H:i:s');

				$new_id_Alum = $this->oNegN_alumno->agregar();
			 	$arrAlumnosIDs[] = $new_id_Alum;
			}
			return $arrAlumnosIDs;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	private function crearPrimeraHoja($idArchivo = null)
	{
		try {
			if (empty($idArchivo)) { throw new Exception(JrTexto::_("There are no students to import")); }
			$this->oNegN_pestania->nombre = ucfirst(JrTexto::_("Sheet")).' 1';
			$this->oNegN_pestania->tipo_pestania = 'H';
			$this->oNegN_pestania->orden = 1;
			$this->oNegN_pestania->idarchivo = $idArchivo;
			$new_idHoja = $this->oNegN_pestania->agregar();

			return $new_idHoja;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}