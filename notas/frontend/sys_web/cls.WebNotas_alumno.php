<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		30-01-2018 
 * @copyright	Copyright (C) 30-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_archivo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas', RUTA_BASE, 'sys_negocio');
class WebNotas_alumno extends JrWeb
{
	private $oNegNotas_alumno;
	private $oNegNotas_archivo;
	private $oNegNotas;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegNotas_alumno = new NegNotas_alumno;
		$this->oNegNotas_archivo = new NegNotas_archivo;
		$this->oNegNotas = new NegNotas;
		
		$this->usuarioAct = NegSesion::getUsuario();
	}

	public function defecto(){
		return $this->listar();
	}

	/*public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_alumno', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["nombres"])&&@$_REQUEST["nombres"]!='')$filtros["nombres"]=$_REQUEST["nombres"];
			if(isset($_REQUEST["apellidos"])&&@$_REQUEST["apellidos"]!='')$filtros["apellidos"]=$_REQUEST["apellidos"];
			if(isset($_REQUEST["identificador"])&&@$_REQUEST["identificador"]!='')$filtros["identificador"]=$_REQUEST["identificador"];
			if(isset($_REQUEST["idarchivo"])&&@$_REQUEST["idarchivo"]!='')$filtros["idarchivo"]=$_REQUEST["idarchivo"];
			if(isset($_REQUEST["fechareg"])&&@$_REQUEST["fechareg"]!='')$filtros["fechareg"]=$_REQUEST["fechareg"];
			
			$this->datos=$this->oNegNotas_alumno->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Notas_alumno'), true);
			$this->esquema = 'notas_alumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_alumno', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Notas_alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_alumno', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegNotas_alumno->idnota = @$_GET['id'];
			$this->datos = $this->oNegNotas_alumno->dataNotas_alumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Notas_alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}*/

	public function listar()
	{
		try {
			global $aplicacion;
			$identificador = @$_REQUEST["identificador"];
			$idDocente = @$_REQUEST["iddocente"];
			if(empty($identificador) || empty($idDocente)) { throw new Exception("Identificador, IdDocente".' '.JrTexto::_("not found"));
			}
			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Scores')) , 'link'=> '/../calificaciones' ],
                [ 'texto'=> ucfirst(JrTexto::_('List'))  ],
            ];

            $this->archivos = $this->oNegNotas_archivo->buscar(array("identificador"=>$identificador, "iddocente"=>$idDocente));

            if(count($this->archivos)==1) {
            	header('Location:'.$this->documento->getUrlBase().'/notas_alumno/ver/?idarchivo='.$this->archivos[0]["idarchivo"]);
            	exit(0);
            }

			$this->documento->setTitulo(ucfirst (JrTexto::_('List').' - '.JrTexto::_('Scores')), true);
			$this->documento->plantilla = 'notas/general';
			$this->esquema = 'notas_alumno/listar';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver()
	{
		try {
			global $aplicacion;
            $idArchivo = @$_REQUEST["idarchivo"];
            $archivo = $this->oNegNotas_alumno->buscar(array("idarchivo"=>$idArchivo, "identificador"=>$this->usuarioAct["dni"], "origen"=>"SMARTLEARN"));
            if(empty($archivo)) {
				throw new Exception(ucfirst(JrTexto::_('You are not in this Score File').'.'));
			}
			$this->archivo = @$this->oNegNotas_archivo->buscar(array("idarchivo"=>$idArchivo))[0];

			$alumno = $this->oNegNotas_alumno->buscar(array("identificador"=>$this->usuarioAct["dni"], "idarchivo"=>$idArchivo));

            $this->notas = $this->oNegNotas->getNotasXAlumno($idArchivo, @$alumno[0]["idalumno"]);
            
			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Scores')) , 'link'=> '/../calificaciones' ],
                [ 'texto'=> ucfirst(JrTexto::_('View'))  ],
            ];
			$this->documento->setTitulo(ucfirst (JrTexto::_('View').' - '.JrTexto::_('Scores')), true);
			$this->documento->plantilla = 'notas/general';
			$this->esquema = 'notas_alumno/ver';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function mdl_importar_alumnos()
	{
		try {
			global $aplicacion;

			$this->esquema = 'notas_alumno/mdl_importar_alumnos';
			$this->documento->plantilla = 'modal';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function mdl_buscar_alumno_notas()
	{
		try {
			global $aplicacion;

			$idArchivo = @$_REQUEST["idarchivo"];
			$this->alumnos = $this->oNegNotas_alumno->buscar(array('idarchivo'=>$idArchivo));
			$this->esquema = 'notas_alumno/mdl_buscar_alumno_notas';
			$this->documento->plantilla = 'modal';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //
	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_alumno', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["nombres"])&&@$_REQUEST["nombres"]!='')$filtros["nombres"]=$_REQUEST["nombres"];
			if(isset($_REQUEST["apellidos"])&&@$_REQUEST["apellidos"]!='')$filtros["apellidos"]=$_REQUEST["apellidos"];
			if(isset($_REQUEST["identificador"])&&@$_REQUEST["identificador"]!='')$filtros["identificador"]=$_REQUEST["identificador"];
			if(isset($_REQUEST["idarchivo"])&&@$_REQUEST["idarchivo"]!='')$filtros["idarchivo"]=$_REQUEST["idarchivo"];
			if(isset($_REQUEST["fechareg"])&&@$_REQUEST["fechareg"]!='')$filtros["fechareg"]=$_REQUEST["fechareg"];
						
			$this->datos=$this->oNegNotas_alumno->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function xAgregar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            if (empty($arrAlumnos) && empty($idArchivo)) { throw new Exception(JrTexto::_("There are no students to import")); }

			$arrAlumnosIDs = array();
			foreach ($arrAlumnos as $i => $a) {
	            $accion='_add';
	            $busqueda = $this->oNegNotas_alumno->buscarRepetido(array("nombres"=>@$a["nombres"],"apellidos"=>@$a["apellidos"],"or_identificador"=>@$a["identificador"], "idarchivo"=>@$idArchivo));
	            if(!empty($busqueda)) { 
	            	$accion='_edit'; 
	            	$this->oNegNotas_alumno->idalumno = $busqueda[0]['idalumno'];
	            }

				$this->oNegNotas_alumno->nombres= trim(@$a["nombres"]);
				$this->oNegNotas_alumno->apellidos= trim(@$a["apellidos"]);
				$this->oNegNotas_alumno->identificador= trim(@$a["identificador"]);
				$this->oNegNotas_alumno->idarchivo= @$idArchivo;
				$this->oNegNotas_alumno->origen= @$a["origen"];
				$this->oNegNotas_alumno->fechareg= @date('Y-m-d H:i:s');

	            if($accion=='_add') {
	            	$res=$this->oNegNotas_alumno->agregar();
	            }else{
	            	$res=$this->oNegNotas_alumno->editar();
	            }
			 	$arrAlumnosIDs[] = $res;
			}
	        echo json_encode(array('code'=>'ok','msje'=>ucfirst(JrTexto::_('updated successfully')),'newid'=>$arrAlumnosIDs));
					
            exit(0); 
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msje'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveNotas_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdAlumno'])) {
					$this->oNegNotas_alumno->idalumno = $frm['pkIdAlumno'];
				}
				
				$this->oNegNotas_alumno->nombres=@$frm["txtNombres"];
				$this->oNegNotas_alumno->apellidos=@$frm["txtApellidos"];
				$this->oNegNotas_alumno->identificador=@$frm["txtIdentificador"];
				$this->oNegNotas_alumno->idarchivo=@$frm["txtIdarchivo"];
				$this->oNegNotas_alumno->fechareg=@$frm["txtFechareg"];

				if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegNotas_alumno->agregar();
				}else{
					$res=$this->oNegNotas_alumno->editar();
				}
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegNotas_alumno->idnota);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDNotas_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_alumno->__set('idnota', $pk);
				$this->datos = $this->oNegNotas_alumno->dataNotas_alumno;
				$res=$this->oNegNotas_alumno->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_alumno->__set('idnota', $pk);
				$res=$this->oNegNotas_alumno->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegNotas_alumno->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	


	// ========================== Funciones publicas ========================== //  
	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'notas_alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}   
}