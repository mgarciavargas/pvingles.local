<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		30-01-2018 
 * @copyright	Copyright (C) 30-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_archivo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_pestania', RUTA_BASE, 'sys_negocio');
class WebNotas extends JrWeb
{
	private $oNegNotas;
	private $oNegN_Archivo;
	private $oNegN_Alumno;
	private $oNegN_Pestania;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegNotas = new NegNotas;
		$this->oNegN_Archivo = new NegNotas_archivo;
		$this->oNegN_Alumno = new NegNotas_alumno;
		$this->oNegN_Pestania = new NegNotas_pestania;
				
		$this->usuarioAct = NegSesion::getUsuario();
	}

	public function defecto(){
		return $this->listar();
	}

	public function listar()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Scores', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            //$this->documento->script('xlsx', '/libs/importexcel/');
            //$this->documento->script('jszip', '/libs/importexcel/');
            $this->documento->script('xlsx.full.min', '/libs/importexcel/');
            $this->documento->script('listar', '/js/notas/');
			$this->documento->script('mdl_importar_alumnos', '/js/notas/');

			$this->archivos = $this->oNegN_Archivo->buscar(array("iddocente"=>$this->usuarioAct["dni"]));
			
			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Scores')) /*, 'link'=> $this->urlBtnRetroceder*/ ],
            ];

			$this->documento->plantilla = 'notas/general';
			$this->documento->setTitulo(JrTexto::_('List').' - '.JrTexto::_('Scores'), true);
			$this->esquema = 'notas/listar';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try{
			global $aplicacion;
            $idArchivo = $_REQUEST["idarchivo"];
			$archivo = $this->oNegN_Archivo->buscar( array("idarchivo"=>$idArchivo, "iddocente"=>$this->usuarioAct['dni']) );
			if(empty($archivo)) {
				throw new Exception(ucfirst(JrTexto::_('File').' '.JrTexto::_('does not exist').'.'));
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            
            $this->documento->script('xlsx.full.min', '/libs/importexcel/');
            $this->documento->script('editar', '/js/notas/');
			$this->documento->script('mdl_importar_alumnos', '/js/notas/');
			$this->documento->script('mdl_editar_pestania', '/js/notas/');
			$this->documento->script('mdl_configuracion', '/js/notas/');
			
			$this->documento->script('table-dragger.min', '/libs/table-dragger/');

			$this->archivo = $archivo[0];
			$this->alumnos = $this->oNegN_Alumno->buscar(array("idarchivo"=>$idArchivo));
			$this->hojas = $this->oNegN_Pestania->buscar(array("idarchivo"=>$idArchivo, "tipo_pestania"=>'H'));
			
			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Scores')) , 'link'=> '/' ],
                [ 'texto'=> ucfirst(JrTexto::_('Edit'))  ],
            ];

			$this->documento->plantilla = 'notas/general';
			$this->documento->setTitulo(ucfirst (JrTexto::_('Edit').' - '.JrTexto::_('Scores')), true);
			$this->esquema = 'notas/editar';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver()
	{
		try {
			global $aplicacion;
            $idArchivo = @$_REQUEST["idarchivo"];
            $archivo = $this->oNegN_Archivo->buscar( array("idarchivo"=>$idArchivo, "iddocente"=>$this->usuarioAct['dni']) );
			if(empty($archivo)) {
				throw new Exception(ucfirst(JrTexto::_('Score File').' '.JrTexto::_('not found').'.'));
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

            $this->documento->script('ver', '/js/notas/');
            $this->documento->script('mdl_buscar_alumno_notas', '/js/notas/');
            $this->documento->script('mdl_altas_bajas_notas', '/js/notas/');

			$this->archivo = $archivo[0];
			$this->alumnos = $this->oNegN_Alumno->buscar(array("idarchivo"=>$idArchivo));
			$this->hojas = $this->oNegN_Pestania->buscar(array("idarchivo"=>$idArchivo, "tipo_pestania"=>'H'));
			
			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Scores')) , 'link'=> '/../calificaciones' ],
                [ 'texto'=> ucfirst(JrTexto::_('View'))  ],
            ];

			$this->documento->setTitulo(ucfirst (JrTexto::_('View').' - '.JrTexto::_('Scores')), true);
			$this->documento->plantilla = 'notas/general';
			$this->esquema = 'notas/ver';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function mdl_altas_bajas_notas()
	{
		try {
			global $aplicacion;
			#$this->documento->script('mdl_altas_notas', '/js/notas/');
			$idArchivo = @$_REQUEST["idarchivo"];
			$this->orden = @$_REQUEST["orden"];
			$this->accion = @$_REQUEST["accion"];
			$this->pestanias = $this->oNegN_Pestania->buscar(array('idarchivo'=>$idArchivo, 'tipo_pestania'=>'H'));
			foreach ($this->pestanias as $i => $h) {
				$this->pestanias[$i]["columnas"] = $this->oNegN_Pestania->buscar(array('idarchivo'=>$idArchivo, 'tipo_pestania'=>'C', 'idpestania_padre'=>$h["idpestania"]));
			}
			#echo '<pre>'; print_r($this->pestanias); echo '</pre>'; 
			$this->esquema = 'notas/mdl_altas_bajas_notas';
			$this->documento->plantilla = 'modal';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

/*
	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idnota"])&&@$_REQUEST["idnota"]!='')$filtros["idnota"]=$_REQUEST["idnota"];
			if(isset($_REQUEST["tipo_nota"])&&@$_REQUEST["tipo_nota"]!='')$filtros["tipo_nota"]=$_REQUEST["tipo_nota"];
			if(isset($_REQUEST["idarchivo"])&&@$_REQUEST["idarchivo"]!='')$filtros["idarchivo"]=$_REQUEST["idarchivo"];
			if(isset($_REQUEST["idhoja"])&&@$_REQUEST["idhoja"]!='')$filtros["idhoja"]=$_REQUEST["idhoja"];
			if(isset($_REQUEST["idcolumna"])&&@$_REQUEST["idcolumna"]!='')$filtros["idcolumna"]=$_REQUEST["idcolumna"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"];
			if(isset($_REQUEST["observacion"])&&@$_REQUEST["observacion"]!='')$filtros["observacion"]=$_REQUEST["observacion"];
			if(isset($_REQUEST["fechareg"])&&@$_REQUEST["fechareg"]!='')$filtros["fechareg"]=$_REQUEST["fechareg"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			
			$this->datos=$this->oNegNotas->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Notas'), true);
			$this->esquema = 'notas-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegNotas->idnota = @$_GET['id'];
			$this->datos = $this->oNegNotas->dataNotas;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Notas').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Notas').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'notas-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/
	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idnota"])&&@$_REQUEST["idnota"]!='')$filtros["idnota"]=$_REQUEST["idnota"];
			if(isset($_REQUEST["tipo_nota"])&&@$_REQUEST["tipo_nota"]!='')$filtros["tipo_nota"]=$_REQUEST["tipo_nota"];
			if(isset($_REQUEST["idarchivo"])&&@$_REQUEST["idarchivo"]!='')$filtros["idarchivo"]=$_REQUEST["idarchivo"];
			if(isset($_REQUEST["idhoja"])&&@$_REQUEST["idhoja"]!='')$filtros["idhoja"]=$_REQUEST["idhoja"];
			if(isset($_REQUEST["idcolumna"])&&@$_REQUEST["idcolumna"]!='')$filtros["idcolumna"]=$_REQUEST["idcolumna"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"];
			if(isset($_REQUEST["observacion"])&&@$_REQUEST["observacion"]!='')$filtros["observacion"]=$_REQUEST["observacion"];
			if(isset($_REQUEST["fechareg"])&&@$_REQUEST["fechareg"]!='')$filtros["fechareg"]=$_REQUEST["fechareg"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			$this->datos=$this->oNegNotas->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarNotas(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idnota)) {
				$this->oNegNotas->idnota = @$idnota;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
			$this->oNegNotas->idarchivo=@$idarchivo;
			$this->oNegNotas->idhoja=@$idhoja;
			$this->oNegNotas->idcolumna=@$idcolumna;
			$this->oNegNotas->idalumno=@$idalumno;
			$this->oNegNotas->nota_num=@$nota_num;
			$this->oNegNotas->nota_txt=@$nota_txt;
			$this->oNegNotas->aprobado=@$aprobado;
			$this->oNegNotas->observacion=@$observacion;
			$this->oNegNotas->fechareg=@$fechareg;

            if($accion=='_add') {
            	$res=$this->oNegNotas->agregar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas')).' 	'.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegNotas->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function jxGetNotasxAlumno()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$datos = array();
			$idAlumno = @$_REQUEST["idalumno"];
			$idArchivo = @$_REQUEST["idarchivo"];

			$datos = $this->oNegNotas->getNotasXAlumno($idArchivo, $idAlumno);

			echo json_encode(array('code'=>'ok', 'data'=>$datos));
		 	exit(0);
		} catch (Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}

	public function jxGetTopNotas()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$datos = array();
			$idPestania =  @$_REQUEST["idpestania"];
			$orden =  @$_REQUEST["orden"]; //ASC | DESC
			$this->oNegNotas->setLimite(0,3);
			$datos = $this->oNegNotas->buscar(array(
				'idcolumna'=> $idPestania,
				'order_by'	=> array(
					'nota_num'=>$orden,
				)
			));
			#echo '<pre>'; print_r($datos); echo '</pre>';
			echo json_encode(array('code'=>'ok', 'data'=>$datos));
		 	exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}

	public function jxGetAprobadosDesaprobados()
	{
		try {
			global $aplicacion;
			$datos = array();
			$idPestania =  @$_REQUEST["idpestania"];
			$aprobado =  @$_REQUEST["aprobado"];

			$this->oNegNotas->setLimite(0,99999);
			$datos= $this->oNegNotas->buscar(array('idcolumna'=>$idPestania, 'aprobado'=>$aprobado));
			
			echo json_encode(array('code'=>'ok', 'data'=>$datos));
		 	exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}
	// ========================== Funciones xajax ========================== //
/*
	public function xSaveNotas(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdnota'])) {
					$this->oNegNotas->idnota = $frm['pkIdnota'];
				}
				
				$this->oNegNotas->tipo_nota=@$frm["txtTipo_nota"];
				$this->oNegNotas->idarchivo=@$frm["txtIdarchivo"];
				$this->oNegNotas->idhoja=@$frm["txtIdhoja"];
				$this->oNegNotas->idcolumna=@$frm["txtIdcolumna"];
				$this->oNegNotas->idalumno=@$frm["txtIdalumno"];
				$this->oNegNotas->nota=@$frm["txtNota"];
				$this->oNegNotas->observacion=@$frm["txtObservacion"];
				$this->oNegNotas->fechareg=@$frm["txtFechareg"];
				$this->oNegNotas->estado=@$frm["txtEstado"];

				if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegNotas->agregar();
				}else{
					$res=$this->oNegNotas->editar();
				}
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegNotas->idnota);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDNotas(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas->__set('idnota', $pk);
				$this->datos = $this->oNegNotas->dataNotas;
				$res=$this->oNegNotas->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas->__set('idnota', $pk);
				$res=$this->oNegNotas->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegNotas->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
*/
}