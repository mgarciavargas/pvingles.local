<form class="form-horizontal editar-columna" id="frmEditarPestania" autocomplete="off">
	<input type="hidden" id="idpestania" name="idpestania" value="<?php echo @$this->datos["idpestania"]; ?>">
	<input type="hidden" id="tipo_pestania" name="tipo_pestania" value="<?php echo @$this->datos["tipo_pestania"]; ?>">
	<input type="hidden" id="orden" name="orden" value="<?php echo @$this->datos["orden"]; ?>">
	<input type="hidden" id="idpestania_padre" name="idpestania_padre" value="<?php echo @$this->datos["idpestania_padre"]; ?>">
	<div class="form-group">
		<label for="nombre" class="col-xs-12 col-sm-3 control-label"><?php echo JrTexto::_("Name"); ?></label>
		<div class="col-xs-12 col-sm-8">
			<input type="text" class="form-control" id="nombre" name="nombre" maxlength="100" value="<?php echo @$this->datos["nombre"]; ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="abreviatura" class="col-xs-12 col-sm-3 control-label"><?php echo JrTexto::_("Abbreviation"); ?></label>
		<div class="col-xs-12 col-sm-3">
			<input type="text" class="form-control" id="abreviatura" name="abreviatura" maxlength="5" value="<?php echo @$this->datos["abreviatura"]; ?>">
		</div>
	</div>
	<div class="form-group <?php echo (@$this->datos["tipo_pestania"]=='H' || !empty($this->datos["idpestania"]))?'hidden':''; ?>">
		<label for="tipo_info" class="col-xs-12 col-sm-3 control-label"><?php echo ucfirst(JrTexto::_("Type of score")); ?></label>
		<div class="col-xs-12 col-sm-8">
			<select type="text" class="form-control" id="tipo_info" name="tipo_info">
				<option value="">- <?php echo ucfirst(JrTexto::_("Select")); ?> -</option>
				<option value="N" <?php echo (@$this->datos["tipo_info"]=="N")?'selected':''; ?>><?php echo ucfirst(JrTexto::_("Numeric")); ?></option>
				<option value="R" <?php echo (@$this->datos["tipo_info"]=="R")?'selected':''; ?>><?php echo ucfirst(JrTexto::_("Selector between a numeric range")); ?></option>
				<option value="E" <?php echo (@$this->datos["tipo_info"]=="E")?'selected':''; ?>><?php echo ucfirst(JrTexto::_("Scale selector")); ?></option>
				<option value="T" <?php echo (@$this->datos["tipo_info"]=="T")?'selected':''; ?>><?php echo ucfirst(JrTexto::_("Text")); ?></option>
				<option value="P" <?php echo (@$this->datos["tipo_info"]=="P")?'selected':''; ?>><?php echo ucfirst(JrTexto::_("Weighted average")); ?></option>
				<option class="hidden" value="A"><?php echo ucfirst(JrTexto::_("Attendance")); ?></option>
			</select>
		</div>
	</div>
	<div class="col-xs-offset-3 col-xs-9 <?php echo (@$this->datos["tipo_pestania"]=='H' || !empty($this->datos["idpestania"]))?'hidden':''; ?>" id="info_valor">
		<div class="form-group thumbnail hidden" data-opcion="N">
			<label class="col-xs-11 col-sm-6"><?php echo ucfirst(JrTexto::_("Minimum score to pass")); ?>:</label>
			<div class="col-xs-11 col-sm-5 padding-0">
				<input type="number" class="form-control val_minimo_pass" min="1" placeholder="<?php echo ucfirst(JrTexto::_("Minimum to pass")); ?>" value="<?php echo !empty($this->info_valor_R)?$this->info_valor_R['max']:''; ?>">
			</div>
		</div>

		<div class="form-group thumbnail hidden" data-opcion="R">
			<label class="col-xs-11 col-sm-2"><?php echo ucfirst(JrTexto::_("Range")); ?></label>
			<div class="col-xs-11 col-sm-10">
				<div class="col-xs-6"> 
					<input type="number" class="form-control val_desde" min="0" placeholder="<?php echo ucfirst(JrTexto::_("Minimum")); ?>" value="<?php echo !empty($this->info_valor_R)?$this->info_valor_R['min']:'0'; ?>"> 
				</div>
				<div class="col-xs-6">
					<input type="number" class="form-control val_hasta" min="1" placeholder="<?php echo ucfirst(JrTexto::_("Maximum")); ?>" value="<?php echo !empty($this->info_valor_R)?$this->info_valor_R['max']:''; ?>">
				</div>
			</div>

			<label class="col-xs-11 col-sm-6"><?php echo ucfirst(JrTexto::_("Minimum score to pass")); ?>:</label>
			<div class="col-xs-11 col-sm-5 padding-0">
				<input type="number" class="form-control val_minimo_pass" min="1" placeholder="<?php echo ucfirst(JrTexto::_("Minimum to pass")); ?>" value="<?php echo !empty($this->info_valor_R)?$this->info_valor_R['max']:''; ?>">
			</div>
		</div>

		<div class="form-group thumbnail hidden" data-opcion="E">
			<label class="col-xs-6"><?php echo ucfirst(JrTexto::_("Scale")); ?></label>
			<label class="col-xs-6 control-label " style=" padding-top:  0; ">
				<input type="checkbox" class="checkbox-ctrl" id="definir_intervalos" <?php echo (!empty($this->info_valor_E) && @$this->info_valor_E["tiene_intervalos"]===true)?'checked':'' ?>> <?php echo ucfirst(JrTexto::_("define intervals")); ?>
			</label>
			<div class="col-xs-12 padding-0 contenedor_escalas">
				<div class="col-xs-12 padding-0 escala hidden" id="escala_clonar">
					<div class="col-xs-5"> 
						<input type="text" class="form-control val_nombre"  name="nombre" placeholder="<?php echo ucfirst(JrTexto::_("Name")); ?>"> 
					</div>
					<div class="col-xs-3"> 
						<input type="number" class="form-control val_desde" placeholder="<?php echo ucfirst(JrTexto::_("Minimum")); ?>" min="0" value="0"> 
					</div>
					<div class="col-xs-3">
						<input type="number" class="form-control val_hasta" placeholder="<?php echo ucfirst(JrTexto::_("Maximum")); ?>" min="1" value="">
					</div>
					<div class="col-xs-1">
						<button class="btn btn-xs btn-default btn_eliminar_escala" title="<?php echo ucfirst(JrTexto::_("Delete").' '.JrTexto::_("scale")); ?>"><i class="color-red fa fa-trash"></i></button>
					</div>
				</div>
				<?php if(empty($this->info_valor_E)){ ?>
				<div class="col-xs-12 padding-0 escala">
					<div class="col-xs-5"> 
						<input type="text" class="form-control val_nombre"  name="nombre" placeholder="<?php echo ucfirst(JrTexto::_("Name")); ?>"> 
					</div>
					<div class="col-xs-3"> 
						<input type="number" class="form-control val_desde" placeholder="<?php echo ucfirst(JrTexto::_("Minimum")); ?>" min="0" value="0"> 
					</div>
					<div class="col-xs-3">
						<input type="number" class="form-control val_hasta" placeholder="<?php echo ucfirst(JrTexto::_("Maximum")); ?>" min="1" value="">
					</div>
					<div class="col-xs-1">
						<button class="btn btn-xs btn-default btn_eliminar_escala" title="<?php echo ucfirst(JrTexto::_("Delete").' '.JrTexto::_("scale")); ?>"><i class="color-red fa fa-trash"></i></button>
					</div>
				</div>
				<?php } else { 
					foreach ($this->info_valor_E["escalas"] as $escala) { ?>
				<div class="col-xs-12 padding-0 escala">
					<div class="col-xs-5"> 
						<input type="text" class="form-control val_nombre"  name="nombre" placeholder="<?php echo ucfirst(JrTexto::_("Name")); ?>" value="<?php echo @$escala["nombre"]; ?>"> 
					</div>
					<div class="col-xs-3"> 
						<input type="number" class="form-control val_desde" placeholder="<?php echo ucfirst(JrTexto::_("Minimum")); ?>" min="0" value="<?php echo @$escala["min"]; ?>"> 
					</div>
					<div class="col-xs-3">
						<input type="number" class="form-control val_hasta" placeholder="<?php echo ucfirst(JrTexto::_("Maximum")); ?>" min="1" value="<?php echo @$escala["max"]; ?>">
					</div>
					<div class="col-xs-1">
						<button class="btn btn-xs btn-default btn_eliminar_escala" title="<?php echo ucfirst(JrTexto::_("Delete").' '.JrTexto::_("scale")); ?>"><i class="color-red fa fa-trash"></i></button>
					</div>
				</div>	
				<?php } }  ?>

			</div>
			<div class="col-xs-12 text-center">
				<br>
				<a href="#" id="btn_agregar_escala" title="<?php echo ucfirst(JrTexto::_("Add").' '.JrTexto::_("scale")); ?>"><i class="fa fa-plus-circle fa-2x"></i></a>
			</div>
			<div class="col-xs-12 padding-0 minima_nota_pasar hidden"><hr>
				<label class="col-xs-11 col-sm-6"><?php echo ucfirst(JrTexto::_("Minimum score to pass")); ?>:</label>
				<div class="col-xs-11 col-sm-5 padding-0">
					<input type="number" class="form-control val_minimo_pass" min="1" placeholder="<?php echo ucfirst(JrTexto::_("Minimum to pass")); ?>" value="<?php echo !empty($this->info_valor_R)?$this->info_valor_R['max']:''; ?>">
				</div>
			</div>
		</div>

		<div class="form-group thumbnail hidden" data-opcion="P">
			<label class="col-xs-11 col-sm-11"><?php echo ucfirst(JrTexto::_("Weighted average")); ?></label>
			<div class="col-xs-12 padding-0 selector_columnas">
				<div class="col-xs-12 col-sm-7">
					<select name="opcColumna" id="opcColumna" class="form-control">
						<option value="">- Seleccionar -</option>
						<?php if(!empty($this->columnas)) {
							foreach ($this->columnas as $c) { ?>
						<option data-abrev="<?php echo $c["abreviatura"]; ?>" value="<?php echo $c["idpestania"]; ?>"><?php echo $c["nombre"]; ?></option>
						<?php } } ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-2">
					<button class="btn btn-primary btn-block add-col" title="<?php echo JrTexto::_("Add this score to formula"); ?>" disabled="disabled"><i class="fa fa fa-hand-o-down"></i></button>
				</div>
				<div class="col-xs-12 col-sm-3">
					<button class="btn btn-primary btn-block add-all-cols" title="<?php echo JrTexto::_("Add all scores to the formula"); ?>"><i class="fa fa fa-hand-o-down"></i><i class="fa fa fa-hand-o-down"></i></button>
				</div>
			</div>
			<div class="col-xs-12 padding-0 contenedor_notas"><br>
				<div class="col-xs-12 padding-0 columna_nota hidden" id="nota_clonar">
					<div class="col-xs-6 nombre"></div>
					<div class="col-xs-4">
						<input type="number" class="form-control peso" placeholder="<?php echo ucfirst(JrTexto::_("Weight")); ?>" min="0" value="0">
					</div>
					<div class="col-xs-2">
						<a href="#" class="btn-lg descartar-nota"><i class="fa fa-hand-o-up color-red"></i></a>
					</div>
				</div>
				<?php if(!empty($this->info_valor_P)){ 
					foreach ($this->info_valor_P as $val) { ?>
				<div class="col-xs-12 padding-0 columna_nota" data-idpestania="<?php echo $val["idpestania"]; ?>" data-abrev="">
					<div class="col-xs-6 nombre"></div>
					<div class="col-xs-4">
						<input type="number" class="form-control peso" placeholder="<?php echo ucfirst(JrTexto::_("Weight")); ?>" min="0" value="<?php echo @$val["peso"]?$val["peso"]:0; ?>">
					</div>
					<div class="col-xs-2">
						<a href="#" class="btn-lg descartar-nota"><i class="fa fa-hand-o-up color-red"></i></a>
					</div>
				</div>
				<?php } } ?>
			</div>

			<div class="col-xs-12 padding-0"><hr>
				<label class="col-xs-11 col-sm-6"><?php echo ucfirst(JrTexto::_("Minimum score to pass")); ?>:</label>
				<div class="col-xs-11 col-sm-5 padding-0">
					<input type="number" class="form-control val_minimo_pass" min="1" placeholder="<?php echo ucfirst(JrTexto::_("Minimum to pass")); ?>" value="<?php echo !empty($this->info_valor_R)?$this->info_valor_R['max']:''; ?>">
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="form-group">
		<label for="color" class="col-xs-12 col-sm-3 control-label"><?php echo JrTexto::_("Color"); ?></label>
		<div class="col-xs-12 col-sm-3">
			<input type="color" class="form-control" id="color" name="color" value="<?php echo !empty($this->datos["color"])?@$this->datos["color"]:'#ffffff'; ?>">
		</div>
	</div>
	<div class="col-xs-12 text-center">
		<hr style="margin-top: 0;">
		<button class="btn btn-default pull-left btn-cancelar"><?php echo ucfirst(JrTexto::_("Cancel")); ?></button>
		<button class="btn btn-success pull-right guardar_pestania"><i class="fa fa-save"></i> <?php echo ucfirst(JrTexto::_("Save")); ?></button>
	</div>
</form>

<script type="text/javascript">
var funcionInicial = <?php echo !empty(@$this->fnCallback)?$this->fnCallback:'null' ?>;
$(document).ready(function() {
	if(funcionInicial!==null) funcionInicial();
});
</script>