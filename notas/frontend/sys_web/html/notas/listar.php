<div class="row" id="breadcrumb"> <div class="col-xs-12 padding-0">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>/../"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<div class="row" id="listar-archivos">
	<input type="hidden" id="hIdDocente" name="hIdDocente" value="<?php echo $this->usuarioAct["dni"]; ?>">
	<div class="col-xs-12">
		<button class="btn btn-success agregar-archivo"><i class="fa fa-plus-square"></i> <span><?php echo ucfirst(JrTexto::_("Start a new record of scores")) ?></span></button>
	</div>
	<?php if (!empty($this->archivos)) {
	foreach ($this->archivos as $i=>$a){ ?>
	<div class="col-xs-3">
		<div class="thumbnail elem-archivo">
			<img src="<?php echo @str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $a["imagen"]); ?>" class="img-responsive center-block" alt="cover">
			<div class="caption">
				<h3><?php echo $a["nombre"]; ?></h3>
				<p class="text-center">
					<a href="#" class="btn btn-default eliminar" data-idarchivo="<?php echo $a["idarchivo"]; ?>" title="<?php echo ucfirst(JrTexto::_("Delete")); ?>"><i class="fa fa-trash color-red"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_("Delete")); ?></span></a>
					 &nbsp; 
					<a href="<?php echo $this->documento->getUrlBase().'/notas/editar/?idarchivo='.$a["idarchivo"]; ?>" class="btn btn-primary" title="<?php echo ucfirst(JrTexto::_("Edit")); ?>"><i class="fa fa-pencil"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_("Edit")); ?></span></a> &nbsp; 
					<a href="<?php echo $this->documento->getUrlBase().'/notas/ver/?idarchivo='.$a["idarchivo"]; ?>" class="btn btn-info" title="<?php echo ucfirst(JrTexto::_("View")); ?>"><i class="fa fa-eye"></i> <span class="sr-only"><?php echo ucfirst(JrTexto::_("View")); ?></span></a> 
				</p>
			</div>
		</div>
	</div>
	<?php } } ?>
	<div class="col-xs-12 text-center <?php echo !empty($this->archivos)?"hidden":""; ?>" id="div_vacio"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4><?php echo ucfirst(JrTexto::_("You have not created record of scores")); ?>.</h4></div>
</div>

<section id="mensajes_idioma" class="hidden">
	<input type="hidden" id="add_students" value="<?php echo JrTexto::_("Add students"); ?>">
	<input type="hidden" id="are_you_sure_delete" value="<?php echo ucfirst(JrTexto::_("are you sure you want to delete this").' '.JrTexto::_("file")).'?'; ?>">
	<input type="hidden" id="delete" value="<?php echo ucfirst(JrTexto::_("Delete")); ?>">
</section>