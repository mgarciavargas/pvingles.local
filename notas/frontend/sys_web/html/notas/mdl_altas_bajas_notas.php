<div class="row" id="mdl_altas_bajas_notas">
	<input type="hidden" id="hOrden" value="<?php echo $this->orden; ?>">
	<input type="hidden" id="hAccion" value="<?php echo $this->accion; ?>">
	<div class="col-sm-offset-2 col-sm-8" id="buscador-pestania">
		<div class="input-group" style="margin: 0;">
			<span class="input-group-addon"><?php echo ucfirst(JrTexto::_("Column")); ?></span>
			<select type="text" class="form-control" id="opcPestania"> 
				<option value="">- <?php echo JrTexto::_("Select column"); ?> -</option>
				<?php foreach ($this->pestanias as $p) { ?>
				<optgroup  label="<?php echo $p["nombre"]; ?>">					
					<?php foreach ($p["columnas"] as $c) { 
						if($c["tipo_info"]!='T') ?>
					<option value="<?php echo $c["idpestania"]; ?>"><?php echo $c["nombre"]; ?></option>
					<?php } ?>
				</optgroup>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="col-xs-12 hidden" id="resultado-altas-bajas-notas"><hr>
		<h2 class="text-center"><?php echo JrTexto::_("Scores") ?></h2>
		<div id="resultado" class="col-sm-offset-2 col-sm-8"></div>
	</div>
</div>