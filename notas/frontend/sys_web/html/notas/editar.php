<div class="row" id="breadcrumb">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>/../"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div>

<div class="row registro-notas" id="editar-notas">
	<input type="hidden" id="hIdDocente" name="hIdDocente" value="<?php echo $this->usuarioAct["dni"]; ?>">
	<input type="hidden" id="hIdArchivo" name="hIdArchivo" value="<?php echo $this->archivo["idarchivo"]; ?>">
	<h1 id="titulo_registro" class="col-xs-12 col-sm-8">
		<?php echo @$this->archivo["nombre"] ?>
	</h1>
	<div id="message-zone" class="col-xs-12 col-sm-2 done">
		<span><?php echo ucfirst(JrTexto::_("Data Saved")); ?></span>
	</div>
	<div id="btn-options" class="col-xs-12 col-sm-1 text-right">
		<a href="#" class="btn btn-xs btn-primary btn-configuracion"><i class="fa fa-cog"></i></a>
	</div>
	<div class="col-xs-11" id="registro">
		<div class="col-xs-6 col-sm-4 tabla-contenidos" id="edita-alumnos">
			<div class="contenedor-tabla">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>N°</th>
							<th>Nombres</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($this->alumnos as $i=>$a) { ?>
						<tr data-idalumno="<?php echo $a['idalumno']; ?>"> 
							<td class="nro"><?php echo ($i+1); ?></td> 
							<td class="nombre"><?php echo $a["apellidos"].' '.$a["nombres"]; ?></td> 
						</tr>
						<?php } ?>
						<tr class="tr_comodin"> 
							<td colspan="2">
								<div class="text-center">
									<a href="#" class="btn-accion agregar-alums" title="<?php echo JrTexto::_("Add Students") ?>"><i class="fa fa-plus-circle fa-2x"></i></a>
								</div>
							</td> 
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-xs-6 col-sm-8 tabla-contenidos" id="notas-main">
			<div class="contenedor-tabla">
				<table class="table table-striped table-text-center table-hover">
					<thead>
						<tr>
							<th><a href="#" class="agregar_pestania" data-tipopestania="C" id="btn_agregar_colum" title="<?php echo JrTexto::_("Edit Columns") ?>"><i class="fa fa-plus-circle fa-2x"></i></a></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($this->alumnos as $i=>$a) { ?>
						<tr data-idalumno="<?php echo $a['idalumno']; ?>">  <td class="comodin_celda" id="comodin_<?php echo $a['idalumno']; ?>"></td> </tr>
						<?php } ?>
						<tr class="tr_comodin"><td></td></tr>
					</tbody>
				</table>
			</div> 
		</div>
	</div>
	<div class="col-xs-1 padding-0 tabs-right" id="hojas-pestanias">
        <ul id="navHojas" class="nav nav-tabs pull-right">
        	<?php if (!empty($this->hojas)) {
        	foreach ($this->hojas as $x => $h) { ?>
            <li class="hvrR-backward tab_hoja" data-idhoja="<?php echo $h["idpestania"]; ?>"><a class="vertical" href="#hoja_<?php echo $h["idpestania"]; ?>" data-toggle="tab" style="background: <?php echo $h["color"]; ?>;"><?php echo $h["nombre"]; ?></a></li>
        	<?php } } ?>
            <li class=""><a class="agregar_pestania" data-tipopestania="H" id="btn_agregar_hoja" href="#"><i class="fa fa-plus-circle fa-2x"></i></a></li>
        </ul>
    </div>
</div>

<section id="mensajes_idioma" class="hidden">
	<input type="hidden" id="columns" value="<?php echo ucfirst(JrTexto::_("columns")); ?>">
	<input type="hidden" id="add_students" value="<?php echo ucfirst(JrTexto::_("Add students")); ?>">
	<input type="hidden" id="edit_columns" value="<?php echo ucfirst(JrTexto::_("Edit columns")); ?>">
	<input type="hidden" id="edit_sheet" value="<?php echo ucfirst(JrTexto::_("Edit sheets")); ?>">
	<input type="hidden" id="save" value="<?php echo ucfirst(JrTexto::_("Save")); ?>">
	<input type="hidden" id="data_saved" value="<?php echo ucfirst(JrTexto::_("Data Saved")); ?>.">
	<input type="hidden" id="loading" value="<?php echo ucfirst(JrTexto::_("Loading")); ?>">
	<input type="hidden" id="error" value="<?php echo ucfirst(JrTexto::_("Error")); ?>">
	<input type="hidden" id="getting_ready" value="<?php echo ucfirst(JrTexto::_("Getting ready")); ?>">
	<input type="hidden" id="no_courses_found" value="<?php echo ucfirst(JrTexto::_("No courses were found")); ?>">
	<input type="hidden" id="attention" value="<?php echo ucfirst(JrTexto::_("Attention")); ?>">
	<input type="hidden" id="jump_next_student" value="<?php echo ucfirst(JrTexto::_("Jump to next student")); ?>">
	<input type="hidden" id="close" value="<?php echo ucfirst(JrTexto::_("Close")); ?>">
	<input type="hidden" id="choose_from_number" value="<?php echo ucfirst(JrTexto::_("Choose from numerical value")); ?>">
	<input type="hidden" id="write_something" value="<?php echo ucfirst(JrTexto::_("Write something")); ?>">
	<input type="hidden" id="click_edit" value="<?php echo ucfirst(JrTexto::_("Click to edit")); ?>">
	<input type="hidden" id="edit" value="<?php echo ucfirst(JrTexto::_("Edit")); ?>">
	<input type="hidden" id="delete" value="<?php echo ucfirst(JrTexto::_("Delete")); ?>">
	<input type="hidden" id="are_you_sure_delete_column_and_scores" value="<?php echo ucfirst(JrTexto::_("Are you sure you want to delete this column and all its scores?")); ?>">
	<input type="hidden" id="are_you_sure_delete_sheet_and_columns_scores" value="<?php echo ucfirst(JrTexto::_("Are you sure you want to delete this sheet and all its columns and scores?")); ?>">
	<input type="hidden" id="settings" value="<?php echo ucfirst(JrTexto::_("settings")); ?>">
	<input type="hidden" id="add_sheet" value="<?php echo ucfirst(JrTexto::_("Add sheet")); ?>">
	<input type="hidden" id="add_column" value="<?php echo ucfirst(JrTexto::_("Add column")); ?>">
	<input type="hidden" id="view_column" value="<?php echo ucfirst(JrTexto::_("View").' '.JrTexto::_("columns")); ?>">
	<input type="hidden" id="page_refresh_to_see" value="<?php echo ucfirst(JrTexto::_("The page will be refreshed to see the changes")); ?>.">
</section>