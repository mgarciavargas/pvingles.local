<style type="text/css">
	tr.collapsed i.fa.fa-chevron-down{
		transform: rotate(180deg);
	}
</style>

<h3><?php echo ucfirst(JrTexto::_("Sheets")); ?></h3>
<table class="table table-striped" id="tblHojas">
	<tbody>
		<?php foreach ($this->hojas as $i=>$h) { ?>
		<tr class="tr-hoja" data-idpestania="<?php echo $h["idpestania"]; ?>"><td>
			<span><i class="fa fa-bars drag_row"></i> &nbsp;</span>
			<span class="nombre"><?php echo $h["nombre"]; ?></span>
			<div class="botones-accion pull-right">
				<button class="btn btn-xs btn-default btn-ver-hoja" title="<?php echo ucfirst(JrTexto::_("View").' '.JrTexto::_("columns")); ?>"><i class="fa fa-chevron-down"></i></button>
				<button class="btn btn-xs btn-default color-blue btn-editar-hoja" title="<?php echo ucfirst(JrTexto::_("Edit")); ?>"><i class="fa fa-pencil "></i></button>
				<button class="btn btn-xs btn-default color-red btn-eliminar-hoja" title="<?php echo ucfirst(JrTexto::_("Delete")); ?>"><i class="fa fa-trash "></i></button>
			</div>
		</td></tr>
	<?php } ?>
	</tbody>
	<tfoot>
		<tr><td class="text-center">
			<button class="btn btn-success config-agregar-hoja" data-tipopestania="H"><i class="fa fa-plus-circle"></i> <?php echo ucfirst(JrTexto::_("Add sheet")); ?></button>
		</td></tr>
	</tfoot>
</table>


<!--div>

	<ul class="nav nav-tabs" role="tablist">
		<?php foreach ($this->hojas as $i=>$h) { ?>
		<li class="<?php echo $i==0?'active':''; ?>" data-idhoja="<?php echo $h["idpestania"]; ?>"><a href="#h_<?php echo $h["idpestania"]; ?>" role="tab" data-toggle="tab"><?php echo $h["nombre"]; ?></a></li>
		<?php } ?>
	</ul>

	<div class="tab-content">
		<?php foreach ($this->hojas as $i=>$h) { ?>
		<div class="tab-pane fade <?php echo $i==0?'in active':''; ?>" id="h_<?php echo $h["idpestania"]; ?>">
			<div class="col-xs-12 col-sm-8"><br>
				<form class="form-horizontal editar-hoja" id="frmEditaHoja<?php echo $h["idpestania"]; ?>" autocomplete="off">
					<input type="hidden" id="idpestania" name="idpestania" value="<?php echo @$h["idpestania"]; ?>">
					<input type="hidden" id="tipo_pestania" name="tipo_pestania" value="<?php echo @$h["tipo_pestania"]; ?>">
					<input type="hidden" id="orden" name="orden" value="<?php echo @$h["orden"]; ?>">
					<input type="hidden" id="idpestania_padre" name="idpestania_padre" value="<?php echo @$h["idpestania_padre"]; ?>">
					<div class="form-group">
						<label for="nombre" class="col-xs-12 col-sm-4 control-label"><?php echo JrTexto::_("Name"); ?></label>
						<div class="col-xs-12 col-sm-7">
							<input type="text" class="form-control" id="nombre" name="nombre" maxlength="100" value="<?php echo @$h["nombre"]; ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="abreviatura" class="col-xs-12 col-sm-4 control-label"><?php echo JrTexto::_("Abbreviation"); ?></label>
						<div class="col-xs-12 col-sm-3">
							<input type="text" class="form-control" id="abreviatura" name="abreviatura" maxlength="5" value="<?php echo @$h["abreviatura"]; ?>">
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<label for="color" class="col-xs-12 col-sm-4 control-label"><?php echo JrTexto::_("Color"); ?></label>
						<div class="col-xs-12 col-sm-3">
							<input type="color" class="form-control" id="color" name="color" value="<?php echo !empty($h["color"])?@$h["color"]:'#ffffff'; ?>">
						</div>
					</div>
					<div class="col-xs-12 text-center">
						<hr style="margin-top: 0;">
						<button class="btn btn-default pull-left btn-cancelar"><?php echo ucfirst(JrTexto::_("Cancel")); ?></button>
						<button class="btn btn-success pull-right guardar_hoja"><i class="fa fa-save"></i> <?php echo ucfirst(JrTexto::_("Save")); ?></button>
					</div>
				</form>
			</div>
			<div class="col-xs-12 col-sm-4"><br>
				<p class="bolder"><?php echo ucfirst(JrTexto::_("Columns")); ?></p>
				<div class="list-group">
					<?php foreach ($h["columnas"] as $j=>$c) { ?>
					<a href="#" class="list-group-item" data-idcolumna="<?php echo $c["nombre"]; ?>"><?php echo $c["nombre"]; ?></a>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>

</div-->

<script type="text/javascript">
var draggerTable_Hojas = null;

$(document).ready(function() {
	draggerTable_Hojas = tableDragger(document.querySelector("#tblHojas"),{
		mode: 'row',
		dragHandler: '.drag_row',
		onlyBody: true,
		animation: 0
	});

	draggerTable_Hojas.on('drop',function(from, to, el, mode){ 
		guardarOrden(from, to, el, mode); 
	});

});
</script>