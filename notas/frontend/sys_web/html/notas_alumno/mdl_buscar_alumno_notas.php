<div class="row" id="mdl_buscar_alumno_notas">
	<div class="col-sm-offset-2 col-xs-8" id="buscador-alumno">
		<div class="input-group" style="margin: 0;">
			<span class="input-group-addon"><?php echo ucfirst(JrTexto::_("Student")); ?></span>
			<select type="text" class="form-control" id="opcAlumno"> 
				<option value="">- <?php echo JrTexto::_("Select student"); ?> -</option>
				<?php foreach ($this->alumnos as $a) { ?>
				<option value="<?php echo $a["idalumno"]; ?>"><?php echo $a["apellidos"].' '.$a["nombres"]; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="col-xs-12"><hr>
		<div id="resultado-buscar-alumno">
			<h2 class="nombre-alumno"></h2>
			<div class="paneles-notas"></div>
		</div>
	</div>
</div>