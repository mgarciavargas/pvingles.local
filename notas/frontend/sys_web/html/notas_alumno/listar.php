<div class="row" id="breadcrumb">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>/../"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div>

<div class="row registro-notas" id="listar-notas-alumno">
    <?php if (!empty($this->archivos)) {
    foreach ($this->archivos as $i=>$a){ ?>
    <div class="col-xs-3">
        <div class="thumbnail elem-archivo">
            <img src="<?php echo @str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $a["imagen"]); ?>" class="img-responsive center-block" alt="cover">
            <div class="caption">
                <h3><?php echo $a["nombre"]; ?></h3>
                <p class="text-center">
                    <a href="<?php echo $this->documento->getUrlBase().'/notas_alumno/ver/?idarchivo='.$a["idarchivo"]; ?>" class="btn btn-info" title="<?php echo ucfirst(JrTexto::_("View")); ?>"><i class="fa fa-eye"></i> <span class=""><?php echo ucfirst(JrTexto::_("View")); ?></span></a> 
                </p>
            </div>
        </div>
    </div>
    <?php } } else { ?>
    <div class="text-center bolder" style="font-size: 1.5em;">
        <i class="fa fa-minus-circle fa-4x"></i>
        <p><?php echo JrTexto::_("There is no score file"); ?></p>
    </div>
    <?php } ?>
</div>