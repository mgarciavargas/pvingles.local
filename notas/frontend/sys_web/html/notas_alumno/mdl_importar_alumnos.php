<div class="row" id="mdl_importar_alumnos">
	<div class="col-xs-12 form-group select-ctrl-wrapper select-azul">
		<select name="opcAgregarDesde" id="opcAgregarDesde" class="form-control select-ctrl">
			<option value="">- <?php echo ucfirst(JrTexto::_("Select")) ?> -</option>
			<option value="SL"><?php echo JrTexto::_("Import from SmartLearn") ?></option>
			<option value="XLS"><?php echo JrTexto::_("Import from XLS file") ?></option>
			<option class="<?php echo @$_GET["acc"]=="listar"?'hidden':'' ?>" value="A"><?php echo JrTexto::_("Add 1 student") ?></option>
		</select>
	</div>
	<div class="col-xs-12 content">
		<div class="hidden" id="XLS">
			<div class="col-xs-12">
				<button class="btn btn-success btn-lg center-block subir-archivo">
					<i class="fa fa-file-excel-o"></i> 
					<?php echo ucfirst(JrTexto::_("Select file")); ?>
					<input type="file" id="" name="fileArchivo" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
				</button>
			</div>

			<div class="col-xs-12 edita-alumnos hidden">
				<ul class="nav nav-tabs" role="tablist"></ul>

				<div class="tab-content"></div>
			</div>
		</div>

		<div class="hidden" id="SL">
			<div class="col-xs-12 botones">
				<button class="btn btn-primary btn-lg center-block cargar-cursos">
					<i class="fa fa-cloud-download"></i>  <?php echo JrTexto::_("See my Courses") ?>
				</button>
			</div>
			<div class="col-xs-12 edita-cursos hidden">
				<div class="col-xs-12 col-sm-3">
					<button class="btn btn-block btn-default atras"><i class="fa fa-chevron-left"></i> <?php echo JrTexto::_("Back"); ?></button>
				</div>
				<div class="col-xs-12 col-sm-9"><h3 class="col-xs-8 text-center"><?php echo JrTexto::_("Select course"); ?></h3></div>

				<div class="col-xs-12 btns-cursos">
					<!--div class="col-xs-6 col-sm-3">
						<button class="btn center-block cargar-cursos">
							<img src="http://192.168.11.57/pvingles.local/static/media/imagenes/levels/a1.png" alt="fondo" class="img-responsive center-block">  
							<span class="bolder"><?php echo JrTexto::_("---"); ?></span>
						</button>
					</div-->
				</div>
			</div>
			<div class="col-xs-12 edita-alumnos hidden">
				<div class="col-xs-12 col-sm-3">
					<button class="btn btn-block btn-default atras"><i class="fa fa-chevron-left"></i> <?php echo JrTexto::_("Back to courses"); ?></button>
				</div>
				<div class="col-xs-12">
					<table class="table table-striped table-hover" id="tblAlumnos">
						<thead>
							<tr>
								<th class="input-chk"><input type="checkbox" class="checkbox-ctrl check-all"></th>
								<th><?php echo JrTexto::_("Last name"); ?></th>
								<th><?php echo JrTexto::_("Name"); ?></th>
								<th><?php echo JrTexto::_("Course"); ?></th>
								<th><?php echo JrTexto::_("Institution"); ?></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="hidden" id="A">-</div>
	</div>
	<div class="col-xs-12 modal-footer" style="background: #fff; bottom: -60px; position: fixed; border-bottom-left-radius: 6px; border-bottom-right-radius: 6px;">     
		<button type="button" class="btn btn-default cerrarmodal pull-left" data-dismiss="modal"><?php echo JrTexto::_('Close') ?></button>
		<button class="btn btn-success guardar_alumnos"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save') ?></button>
	</div>

	<section id="mensajes_idioma" class="hidden">
		<input type="hidden" id="accept" value="<?php echo ucfirst(JrTexto::_("Accept")); ?>">
		<input type="hidden" id="cancel" value="<?php echo ucfirst(JrTexto::_("Cancel")); ?>">
		<input type="hidden" id="add" value="<?php echo ucfirst(JrTexto::_("Add")); ?>">
		<input type="hidden" id="name" value="<?php echo ucfirst(JrTexto::_("Name")); ?>">
		<input type="hidden" id="save" value="<?php echo JrTexto::_("Save"); ?>">
		<input type="hidden" id="loading" value="<?php echo JrTexto::_("Loading"); ?>">
		<input type="hidden" id="no_courses_found" value="<?php echo JrTexto::_("No courses were found"); ?>">
		<input type="hidden" id="attention" value="<?php echo ucfirst(JrTexto::_("Attention")); ?>">
		<input type="hidden" id="error" value="<?php echo ucfirst(JrTexto::_("Error")); ?>">
		<input type="hidden" id="you_must_select_option" value="<?php echo ucfirst(JrTexto::_("You must select an option")); ?>">
		<input type="hidden" id="give_a_name" value="<?php echo ucfirst(JrTexto::_("Give your file a name")); ?>">
		<input type="hidden" id="there_are_no_students" value="<?php echo ucfirst(JrTexto::_("There are no students to import")); ?>">
		<input type="hidden" id="last_name" value="<?php echo ucfirst(JrTexto::_("Last name")); ?>">
		<input type="hidden" id="name" value="<?php echo ucfirst(JrTexto::_("Name")); ?>">
		<input type="hidden" id="identification" value="<?php echo ucfirst(JrTexto::_("Identification")); ?>">
		<input type="hidden" id="ignore" value="<?php echo ucfirst(JrTexto::_("Ignore")); ?>">
		<input type="hidden" id="cannot_be_repeated" value="<?php echo ucfirst(JrTexto::_("The fields can not be repeated")); ?>">
		<input type="hidden" id="correct_fields_red" value="<?php echo ucfirst(JrTexto::_("You must correct the fields in red")); ?>">
		<input type="hidden" id="or_ignoring_columns" value="<?php echo ucfirst(JrTexto::_("or").' '.JrTexto::_("you are ignoring all the columns")); ?>">
	</section>
</div>


<script type="text/javascript">
$(document).ready(function() {
	cargarMensajesPHP($("#mdl_importar_alumnos #mensajes_idioma"));
});
</script>
