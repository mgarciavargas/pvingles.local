<div class="row" id="breadcrumb">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>/../"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div>


<div class="row" id="ver-notas-alumno">

    <div class="col-xs-12 padding-0">
        <div class="col-xs-12 col-sm-2"><img src="<?php echo str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $this->archivo["imagen"]); ?>" class="img-responsive"></div>
    	<div class="col-xs-12 col-sm-10">
            <h3 class="bolder"><?php echo $this->archivo["nombre"]; ?></h3>
        </div>
    </div>

    <div class="col-xs-12 padding-0" id="resultados-notas"><hr>
        <?php if(!empty($this->notas)){ 
        foreach ($this->notas as $n) { ?>
        <div class="panel" style="border-color: <?php echo $n["color"] ?>;">
            <div class="panel-heading bolder" style="background-color: <?php echo $n["color"] ?>;">
                <span><?php echo $n["nombre"].(!empty($n["abreviatura"])?' ('.$n["abreviatura"].')':''); ?></span>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-offset-3 col-sm-6">
                    <?php if(!empty($n["columnas"])){ ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($n["columnas"] as $col) { ?>
                            <tr style="background-color: <?php echo $col["color"] ?>;">
                                <td><span><?php echo $col["nombre"].(!empty($col["abreviatura"])?' ('.$col["abreviatura"].')':''); ?></span></td>
                                <td><span><?php 
                                $nota = '';
                                if( ($col["tipo_info"]=='E' || $col["tipo_info"]=='T') && !empty($col["nota"]) && !empty($col["nota"]["nota_txt"]) ) {
                                    $nota .=  $col["nota"]["nota_txt"];
                                }
                                if($col["tipo_info"]!='E' && $col["tipo_info"]!='T') {
                                    $nota .= ( !empty($col["nota"]) && !empty($col["nota"]["nota_num"]) )?$col["nota"]["nota_num"]:'0.00';
                                }

                                echo $nota; ?></span></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } else { ?>
                    <div class="text-center">
                        <i class="fa fa-minus-circle fa-4x"></i>
                        <p><?php echo JrTexto::_("There is no scores") ?></p>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } } ?>
    </div>
	
</div>