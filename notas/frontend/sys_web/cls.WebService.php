<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas_archivo', RUTA_BASE, 'sys_negocio');
#JrCargador::clase('sys_negocio::Neg...', RUTA_BASE, 'sys_negocio');
class WebService extends JrWeb
{
	private $oNegNotas_archivo;
	#private $oNeg...;
	public function __construct()
	{
		parent::__construct();
		$this->oNegArchivo = new NegNotas_archivo;
		#$this->oNeg... = new Neg...;
	}

	public function defecto(){
		global $aplicacion;
		return $aplicacion->error("403 - You don't have permission to access this server.");
	}

	/** 
	* @title Service::get_todos_archivos --
	* Obtiene todos los notas_archivos del docente.
	* @param iddocente (obligatorio) : dni del docente a buscar.
	* @param identificador : idgrupoauladetalle del archivo.
	*/
	public function get_todos_archivos()
	{
		try{
			global $aplicacion;
			$this->documento->plantilla = 'blanco';
			if(!$this->estaAfiliado()){ throw new Exception(JrTexto::_("Not affiliated")); }
			if( $this->faltanParams(array("iddocente")) ){ exit(0); }

			$data = array();
			$filtros["iddocente"] = @$_GET['iddocente'];
			if(isset($_GET['identificador']) && $_GET['identificador']!='') $filtros["identificador"] = $_GET['identificador'];
			$search = $this->oNegArchivo->buscar($filtros);
			foreach ($search as $arch) {
				$arch["imagen"] = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $arch["imagen"]);
				$data[] = $arch;
			}
			$response = array(
				'code' => 200,
				'message' => 'Success',
				'data' => $data
			);
			echo json_encode($response);
	        return parent::getEsquema();
		}catch(Exception $e) {
			$response = array(
				'code'=>500,
				'message' => $e->getMessage(),
				'data'=>[]
			);
			echo json_encode($response);exit(0);
		}
	}


	/*================= FUNCIONES PRIVADAS =================*/
	private function estaAfiliado($params = null)
	{
		try {
			/* Identificar si tiene accessos a los servicios de smartLearn segun el/los parametro/s $_GET que corresponda/n */

			return true;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function faltanParams($arrParams = array())
	{
		try {
			$paramsMissing = array();
			foreach ($arrParams as $param) {
				if (!array_key_exists($param, $_GET)) {
					$paramsMissing[] = " '".$param."' ";
				}
			}

			if (!empty($paramsMissing)) {
				throw new Exception( JrTexto::_("Params are missing").': '.implode(' , ', $paramsMissing) );
			}

			return false;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}