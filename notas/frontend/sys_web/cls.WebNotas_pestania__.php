<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-01-2018 
 * @copyright	Copyright (C) 24-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas_pestania', RUTA_BASE, 'sys_negocio');
class WebNotas_pestania extends JrWeb
{
	private $oNegNotas_pestania;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegNotas_pestania = new NegNotas_pestania;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_pestania', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idpestania"])&&@$_REQUEST["idpestania"]!='')$filtros["idpestania"]=$_REQUEST["idpestania"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["abreviatura"])&&@$_REQUEST["abreviatura"]!='')$filtros["abreviatura"]=$_REQUEST["abreviatura"];
			if(isset($_REQUEST["tipo_pestania"])&&@$_REQUEST["tipo_pestania"]!='')$filtros["tipo_pestania"]=$_REQUEST["tipo_pestania"];
			if(isset($_REQUEST["tipo_peso"])&&@$_REQUEST["tipo_peso"]!='')$filtros["tipo_peso"]=$_REQUEST["tipo_peso"];
			if(isset($_REQUEST["peso"])&&@$_REQUEST["peso"]!='')$filtros["peso"]=$_REQUEST["peso"];
			if(isset($_REQUEST["maximo"])&&@$_REQUEST["maximo"]!='')$filtros["maximo"]=$_REQUEST["maximo"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["idpestania_padre"])&&@$_REQUEST["idpestania_padre"]!='')$filtros["idpestania_padre"]=$_REQUEST["idpestania_padre"];
			
			$this->datos=$this->oNegNotas_pestania->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Notas_pestania'), true);
			$this->esquema = 'notas_pestania-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_pestania', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Notas_pestania').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_pestania', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegNotas_pestania->idpestania = @$_GET['id'];
			$this->datos = $this->oNegNotas_pestania->dataNotas_pestania;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Notas_pestania').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'notas_pestania-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_pestania', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpestania"])&&@$_REQUEST["idpestania"]!='')$filtros["idpestania"]=$_REQUEST["idpestania"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["abreviatura"])&&@$_REQUEST["abreviatura"]!='')$filtros["abreviatura"]=$_REQUEST["abreviatura"];
			if(isset($_REQUEST["tipo_pestania"])&&@$_REQUEST["tipo_pestania"]!='')$filtros["tipo_pestania"]=$_REQUEST["tipo_pestania"];
			if(isset($_REQUEST["tipo_peso"])&&@$_REQUEST["tipo_peso"]!='')$filtros["tipo_peso"]=$_REQUEST["tipo_peso"];
			if(isset($_REQUEST["peso"])&&@$_REQUEST["peso"]!='')$filtros["peso"]=$_REQUEST["peso"];
			if(isset($_REQUEST["maximo"])&&@$_REQUEST["maximo"]!='')$filtros["maximo"]=$_REQUEST["maximo"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["idpestania_padre"])&&@$_REQUEST["idpestania_padre"]!='')$filtros["idpestania_padre"]=$_REQUEST["idpestania_padre"];
						
			$this->datos=$this->oNegNotas_pestania->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function xGuardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idpestania)) {
				$this->oNegNotas_pestania->idpestania = @$idpestania;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
			$this->oNegNotas_pestania->nombre=@$nombre;
			$this->oNegNotas_pestania->abreviatura=@$abreviatura;
			$this->oNegNotas_pestania->tipo_pestania=@$tipo_pestania;
			$this->oNegNotas_pestania->tipo_peso=@$tipo_peso;
			$this->oNegNotas_pestania->peso=@$peso;
			$this->oNegNotas_pestania->maximo=@$maximo;
			$this->oNegNotas_pestania->color=@$color;
			$this->oNegNotas_pestania->orden=@$orden;
			$this->oNegNotas_pestania->idpestania_padre=@$idpestania_padre;
					
            if($accion=='_add') {
            	$res=$this->oNegNotas_pestania->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_pestania')).' '.JrTexto::_('saved successfully'),'data'=>$res)); 
            }else{
            	$res=$this->oNegNotas_pestania->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_pestania')).' '.JrTexto::_('update successfully'),'data'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveNotas_pestania(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdpestania'])) {
					$this->oNegNotas_pestania->idpestania = $frm['pkIdpestania'];
				}
				
				$this->oNegNotas_pestania->nombre=@$frm["txtNombre"];
					$this->oNegNotas_pestania->abreviatura=@$frm["txtAbreviatura"];
					$this->oNegNotas_pestania->tipo_pestania=@$frm["txtTipo_pestania"];
					$this->oNegNotas_pestania->tipo_peso=@$frm["txtTipo_peso"];
					$this->oNegNotas_pestania->peso=@$frm["txtPeso"];
					$this->oNegNotas_pestania->maximo=@$frm["txtMaximo"];
					$this->oNegNotas_pestania->color=@$frm["txtColor"];
					$this->oNegNotas_pestania->orden=@$frm["txtOrden"];
					$this->oNegNotas_pestania->idpestania_padre=@$frm["txtIdpestania_padre"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegNotas_pestania->agregar();
					}else{
									    $res=$this->oNegNotas_pestania->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegNotas_pestania->idpestania);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDNotas_pestania(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_pestania->__set('idpestania', $pk);
				$this->datos = $this->oNegNotas_pestania->dataNotas_pestania;
				$res=$this->oNegNotas_pestania->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_pestania->__set('idpestania', $pk);
				$res=$this->oNegNotas_pestania->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegNotas_pestania->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}