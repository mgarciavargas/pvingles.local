<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
#JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
class WebDefecto extends JrWeb
{
	#private $oNegAulavirtualinvitados;
	public function __construct()
	{
		parent::__construct();
		#$this->oNegGrupo_matricula = new NegGrupo_matricula;
	}

	public function defecto(){
		global $aplicacion;		
		return $this->verlistado();		
	}

	public function verlistado()
	{
		try{
			global $aplicacion;			
	
			return $aplicacion->redir("notas");
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

}