<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		30-01-2018 
 * @copyright	Copyright (C) 30-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas_pestania', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas', RUTA_BASE, 'sys_negocio');
class WebNotas_pestania extends JrWeb
{
	private $oNegNotas_pestania;
	private $oNegNotas;
		
	public function __construct()
	{
		parent::__construct();	
		$this->oNegNotas = new NegNotas;	
		$this->oNegNotas_pestania = new NegNotas_pestania;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_pestania', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idpestania"])&&@$_REQUEST["idpestania"]!='')$filtros["idpestania"]=$_REQUEST["idpestania"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["abreviatura"])&&@$_REQUEST["abreviatura"]!='')$filtros["abreviatura"]=$_REQUEST["abreviatura"];
			if(isset($_REQUEST["tipo_pestania"])&&@$_REQUEST["tipo_pestania"]!='')$filtros["tipo_pestania"]=$_REQUEST["tipo_pestania"];
			if(isset($_REQUEST["tipo_info"])&&@$_REQUEST["tipo_info"]!='')$filtros["tipo_info"]=$_REQUEST["tipo_info"];
			if(isset($_REQUEST["info_valor"])&&@$_REQUEST["info_valor"]!='')$filtros["info_valor"]=$_REQUEST["info_valor"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["idarchivo"])&&@$_REQUEST["idarchivo"]!='')$filtros["idarchivo"]=$_REQUEST["idarchivo"];
			if(isset($_REQUEST["idpestania_padre"])&&@$_REQUEST["idpestania_padre"]!='')$filtros["idpestania_padre"]=$_REQUEST["idpestania_padre"];
			
			$this->datos=$this->oNegNotas_pestania->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Notas_pestania'), true);
			$this->esquema = 'notas_pestania-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_pestania', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Notas_pestania').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_pestania', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegNotas_pestania->idpestania = @$_GET['id'];
			$this->datos = $this->oNegNotas_pestania->dataNotas_pestania;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Notas_pestania').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function mdl_agregar_pestania()
	{
		try {
			global $aplicacion;

			$this->datos = array();
			$this->datos["tipo_pestania"] = @$_REQUEST["tipo_pestania"];
			$this->datos["orden"] = @$_REQUEST["orden"];
			$this->datos["idpestania_padre"] = @$_REQUEST["idpestania_padre"];

			$searchCols = $this->oNegNotas_pestania->buscar(array("tipo_pestania"=>'C', "idpestania_padre"=>$this->datos["idpestania_padre"], "tipo_info"=>array("'R'","'N'","'E'","'P'")  ));
			$this->columnas = array();
			if(!empty($searchCols)) {
				foreach ($searchCols as $i => $c) {
					if($c["tipo_info"]=='E') {
						$info_valor = json_decode($c["info_valor"], true);
						if($info_valor["tiene_intervalos"]) {
							$this->columnas[] = $c;
						}
					} else {
						$this->columnas[] = $c;
					}
				}
			}

			$this->documento->plantilla = 'blanco';
			$this->esquema = 'notas/mdl_editar_pestania';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function mdl_editar_pestania()
	{
		try {
			global $aplicacion;
			if(empty($_REQUEST["idpestania"])) {
				throw new Exception(JrTexto::_("Tab not found"));
			}
			$this->fnCallback = @$_REQUEST["fn"];
			$this->oNegNotas_pestania->idpestania = @$_REQUEST["idpestania"];
			$this->datos = $this->oNegNotas_pestania->dataNotas_pestania;
			$this->columnas = $this->oNegNotas_pestania->buscar(array("tipo_pestania"=>'C', "idpestania_padre"=>$this->datos["idpestania_padre"]));


			$this->info_valor_R = $this->info_valor_E = null;
			if( $this->datos["tipo_info"]=='R' ) {
				$this->info_valor_R = json_decode($this->datos["info_valor"], true);
			} else if ( $this->datos["tipo_info"]=='E' ) {
				$this->info_valor_E = json_decode($this->datos["info_valor"], true);
			}else if ( $this->datos["tipo_info"]=='P' ) {
				$this->info_valor_P = json_decode($this->datos["info_valor"], true);
			}

			$this->documento->plantilla = 'blanco';
			$this->esquema = 'notas/mdl_editar_pestania';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function mdl_configuracion()
	{
		try {
			global $aplicacion;
			$idArchivo = $_REQUEST["idarchivo"];
			$this->hojas = $this->oNegNotas_pestania->buscar(array("idarchivo"=>$idArchivo, "tipo_pestania"=>'H'));
			if(!empty($this->hojas)) {
				foreach ($this->hojas as $i=>$h) {
					$this->hojas[$i]["columnas"] = $this->oNegNotas_pestania->buscar(array("idarchivo"=>$idArchivo, "tipo_pestania"=>'C', "idpestania_padre"=>$h["idpestania"]));
				}
			}

			#echo '<pre>'; print_r($this->hojas); echo '</pre>';
			$this->documento->plantilla = 'blanco';
			$this->esquema = 'notas/mdl_configuracion';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'notas_pestania-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_pestania', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpestania"])&&@$_REQUEST["idpestania"]!='')$filtros["idpestania"]=$_REQUEST["idpestania"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["abreviatura"])&&@$_REQUEST["abreviatura"]!='')$filtros["abreviatura"]=$_REQUEST["abreviatura"];
			if(isset($_REQUEST["tipo_pestania"])&&@$_REQUEST["tipo_pestania"]!='')$filtros["tipo_pestania"]=$_REQUEST["tipo_pestania"];
			if(isset($_REQUEST["tipo_info"])&&@$_REQUEST["tipo_info"]!='')$filtros["tipo_info"]=$_REQUEST["tipo_info"];
			if(isset($_REQUEST["info_valor"])&&@$_REQUEST["info_valor"]!='')$filtros["info_valor"]=$_REQUEST["info_valor"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
			if(isset($_REQUEST["idarchivo"])&&@$_REQUEST["idarchivo"]!='')$filtros["idarchivo"]=$_REQUEST["idarchivo"];
			if(isset($_REQUEST["idpestania_padre"])&&@$_REQUEST["idpestania_padre"]!='')$filtros["idpestania_padre"]=$_REQUEST["idpestania_padre"];
			
			$this->datos=$this->oNegNotas_pestania->buscar($filtros);

			if(isset($_REQUEST["con_notas"])&&@$_REQUEST["con_notas"]==true) {
				$this->getNotasxColumna($this->datos);
			}

			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function xGuardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idpestania)) {
				$this->oNegNotas_pestania->idpestania = @$idpestania;
				$accion='_edit';
			}
			$usuarioAct = NegSesion::getUsuario();

			$this->oNegNotas_pestania->nombre=@$nombre;
			$this->oNegNotas_pestania->abreviatura=@$abreviatura;
			$this->oNegNotas_pestania->tipo_pestania=@$tipo_pestania;
			$this->oNegNotas_pestania->tipo_info=@$tipo_info;
			$this->oNegNotas_pestania->info_valor=@$info_valor;
			$this->oNegNotas_pestania->color=@$color;
			$this->oNegNotas_pestania->orden=@$orden;
			$this->oNegNotas_pestania->idarchivo=@$idarchivo;
			$this->oNegNotas_pestania->idpestania_padre=@$idpestania_padre;

			if($accion=='_add') {
				$res=$this->oNegNotas_pestania->agregar();
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_pestania')).' '.JrTexto::_('saved successfully'),'data'=>$res)); 
			}else{
				$res=$this->oNegNotas_pestania->editar();
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_pestania')).' '.JrTexto::_('update successfully'),'data'=>$res)); 
			}
			
			if(@$tipo_pestania=='C' && @$tipo_info=='P') {
				$idNotas = $this->oNegNotas->guardarPromediosPonderados($res);
			}

            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function jxGuardarArray(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST['arrPestanias'])){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data "arrPestanias" incomplete')));
                exit(0);            
            }
            @extract($_POST);
            if(!empty($arrPestanias)) {
            	foreach ($arrPestanias as $p) {
            		$accion='_add';
		            if(!empty(@$p["idpestania"])) {
						$this->oNegNotas_pestania->idpestania = @$p["idpestania"];
						$accion='_edit';
					}
            		
					if(!empty(@$p["nombre"])){
						$this->oNegNotas_pestania->nombre = $p["nombre"];
					}
					if(!empty(@$p["abreviatura"])){
						$this->oNegNotas_pestania->abreviatura = $p["abreviatura"];
					}
					if(!empty(@$p["tipo_pestania"])){
						$this->oNegNotas_pestania->tipo_pestania = $p["tipo_pestania"];
					}
					if(!empty(@$p["tipo_info"])){
						$this->oNegNotas_pestania->tipo_info = $p["tipo_info"];
					}
					if(!empty(@$p["info_valor"])){
						$this->oNegNotas_pestania->info_valor = $p["info_valor"];
					}
					if(!empty(@$p["color"])){
						$this->oNegNotas_pestania->color = $p["color"];
					}
					if(!empty(@$p["orden"])){
						$this->oNegNotas_pestania->orden = $p["orden"];
					}
					if(!empty(@$p["idarchivo"])){
						$this->oNegNotas_pestania->idarchivo = $p["idarchivo"];
					}
					if(!empty(@$p["idpestania_padre"])){
						$this->oNegNotas_pestania->idpestania_padre = $p["idpestania_padre"];
					}

					if($accion=='_add') {
						$resp[] = $this->oNegNotas_pestania->agregar();
						$msj = ucfirst(JrTexto::_('Notas_pestania')).' '.JrTexto::_('saved successfully'); 
					}else{
						$resp[] = $this->oNegNotas_pestania->editar();
						$msj  = ucfirst(JrTexto::_('Notas_pestania')).' '.JrTexto::_('update successfully');
					}
            	}
            }

			echo json_encode(array('code'=>'ok', 'msj'=> $msj, 'data'=>$resp));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function jxEliminar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;	
			if(empty($_POST["idpestania"])){
				throw new Exception(JrTexto::_('ID is missing'));
            }
            $this->oNegNotas_pestania->idpestania = @$_POST["idpestania"];

            $condicion = array();
            if($this->oNegNotas_pestania->tipo_pestania=='H') {
            	$condicion = array('idhoja'=>$this->oNegNotas_pestania->idpestania);
            } else if($this->oNegNotas_pestania->tipo_pestania=='C') {
            	$condicion = array('idcolumna'=>$this->oNegNotas_pestania->idpestania, 'idhoja'=>$this->oNegNotas_pestania->idpestania_padre);
            }

            $del = $this->oNegNotas->eliminarXPestania($condicion);
			$res = $this->oNegNotas_pestania->eliminar();

			if(!empty($res)) {
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_pestania')).' '.JrTexto::_('deleted successfully'),'data'=>$res)); 
			} else {
				throw new Exception(JrTexto::_('Delete failed').'!');
			}
            exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}
	
	public function jxActualizarPonderados()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;	
			if(empty($_POST["idhoja"])){
				throw new Exception(JrTexto::_('ID sheet is missing'));
            }
            @extract($_POST);
            $ids_notas = array();
            $colsPonderado = $this->oNegNotas_pestania->buscar(array("idpestania_padre"=>$idhoja, "tipo_info"=>'P'));
            if(!empty($colsPonderado)) {
            	foreach ($colsPonderado as $col) {
            		$ids_notas[] = $this->oNegNotas->guardarPromediosPonderados($col["idpestania"]);
            	}
            }
           	
           	$this->getNotasxColumna($colsPonderado);

           	echo json_encode(array('code'=>'ok','data'=>$colsPonderado));
            exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}
	// ========================== Funciones xajax ========================== //
	public function xSaveNotas_pestania(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdpestania'])) {
					$this->oNegNotas_pestania->idpestania = $frm['pkIdpestania'];
				}
				
				$this->oNegNotas_pestania->nombre=@$frm["txtNombre"];
				$this->oNegNotas_pestania->abreviatura=@$frm["txtAbreviatura"];
				$this->oNegNotas_pestania->tipo_pestania=@$frm["txtTipo_pestania"];
				$this->oNegNotas_pestania->tipo_info=@$frm["txtTipo_info"];
				$this->oNegNotas_pestania->info_valor=@$frm["txtInfo_valor"];
				$this->oNegNotas_pestania->color=@$frm["txtColor"];
				$this->oNegNotas_pestania->orden=@$frm["txtOrden"];
				$this->oNegNotas_pestania->idarchivo=@$frm["txtIdarchivo"];
				$this->oNegNotas_pestania->idpestania_padre=@$frm["txtIdpestania_padre"];
				
				if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegNotas_pestania->agregar();
				}else{
					$res=$this->oNegNotas_pestania->editar();
				}
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegNotas_pestania->idpestania);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDNotas_pestania(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_pestania->__set('idpestania', $pk);
				$this->datos = $this->oNegNotas_pestania->dataNotas_pestania;
				$res=$this->oNegNotas_pestania->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_pestania->__set('idpestania', $pk);
				$res=$this->oNegNotas_pestania->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegNotas_pestania->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	// ========================== Funciones privadas ========================== //
	private function getNotasxColumna(&$arrColumnas)
	{
		try {
			foreach ($arrColumnas as $i=>$c) {
				$notas = $this->oNegNotas->buscar(array("idhoja"=>$c['idpestania_padre'], "idcolumna"=>$c['idpestania'], "idarchivo"=>$c['idarchivo'] ));
				foreach ($notas as $k => $n) {
					$arrColumnas[$i]['notas_alum'][ $n['idalumno'] ] = array(
						"idnota" => $n['idnota'],
						"nota_num" => $n['nota_num'],
						"nota_txt" => $n['nota_txt'],
						"aprobado" => $n['aprobado'],
						"observacion" => $n['observacion'],
						"fechareg" => $n['fechareg']
					);
				}
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}