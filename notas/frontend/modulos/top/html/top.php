<style type="text/css">
    .cambiarrolmenu a ,.cambiarrolmenu a:hover{
         font-size: 11px !important;
        color: #c13838 !important;
        background: transparent !important;
        text-decoration: none !important;
    }
</style>
<header>
    <div class="container-fluid">
        <div class="navbar-header navbar-static-top"> 
            <button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button"> 
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
            </button> 
            <a href="<?php echo $this->documento->getUrlSitio() ?>" class="navbar-brand" id="logo">
                <img src="<?php echo $this->documento->getUrlStatic() ?>/img/sistema/logo.png" class="hvr-wobble-skew img-responsive hidden" alt="logo">
            </a>                        
        </div>       
        <nav class="collapse navbar-collapse" id="bs-navbar"> 
            <ul class="nav navbar-nav navbar-right menutop1"> 
                <li class="dropdown" id="account"> 
                    <a href="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user-circle"></i>
                    </a>

                    <div class="user-menu dropdown-menu">
                        <div class="account-info">
                            <a  class="user-photo">
                                <img src="<?php echo $this->documento->getUrlBase() ?>/../static/media/imagenes/user_avatar.jpg" alt="user" class="img-responsive">
                            </a>
                            <div class="data-user">
                                <div class="user-name"><?php echo $this->usuarioAct['nombre_full'] ?></div>
                                <div class="user-email"><?php echo $this->usuarioAct['email'] ?></div>
                                <?php 
                                $roles=$this->usuarioAct["roles"]; 
                                $idrol_=$this->usuarioAct["idrol"];
                                $verrol=$this->usuarioAct["rol"];
                                if(count($roles)>1){                                    
                                    $verrol.=' <small class="cambiarrolmenu"><a href="'.$this->documento->getUrlBase().'/../sesion/cambiarrol" class="changerol">('.JrTexto::_('Change Rol').')</a></small>';
                                }else{ $verrol=$this->usuarioAct["rol"];}
                                ?>
                                <div class="user-email">
                                 <?php echo $verrol; ?></div>
                                <a class="btn btn-blue capitalize btn-profile" href="<?php echo $this->documento->getUrlBase() ?>/../personal/perfil">
                                    <?php echo JrTexto::_('my profile'); ?>
                                </a>
                            </div>
                        </div>
                        <div class="action-buttons">
                            <div>
                                <a href="<?php echo $this->documento->getUrlBase() ?>/../personal/cambiarclave/?id=<?php echo $this->usuarioAct["dni"];?>" class="btn btn-default capitalize"><?php echo JrTexto::_('change password'); ?></a>
                            </div>
                            <div>
                                <a href="<?php echo $this->documento->getUrlBase() ?>/../sesion/salir" class="btn btn-default capitalize"><?php echo JrTexto::_('log out'); ?></a>
                            </div>
                        </div>
                    </div> 
                </li>
                <li> 
                    <a  class="hvr-buzz-out" href="#" id="getting-started">
                        <i class="fa fa-th-large"></i>
                    </a> 
                </li> 
            </ul>
        </nav> 
    </div>
</header>