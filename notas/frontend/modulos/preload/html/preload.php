<link href="<?php echo $this->documento->getUrlTema()?>/css/preload.css" rel="stylesheet">
<div id="loader-wrapper">
    <div id="loader">
    	<img src="<?php echo $this->documento->getUrlStatic()?>/tema/css/images/cargando.gif" class="img-responsive">
    </div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<script type="text/javascript">
$(document).ready(function() {    
    setTimeout(function(){
        $('body').addClass('loaded');
        $('h1').css('color','#222222');
    }, 1000);    
});
</script>