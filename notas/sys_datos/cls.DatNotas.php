<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		30-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatNotas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Notas").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM notas";
			
			$cond = array();		
			
			if(isset($filtros["idnota"])) {
				$cond[] = "idnota = " . $this->oBD->escapar($filtros["idnota"]);
			}
			if(isset($filtros["idarchivo"])) {
				$cond[] = "idarchivo = " . $this->oBD->escapar($filtros["idarchivo"]);
			}
			if(isset($filtros["idhoja"])) {
				$cond[] = "idhoja = " . $this->oBD->escapar($filtros["idhoja"]);
			}
			if(isset($filtros["idcolumna"])) {
				$cond[] = "idcolumna = " . $this->oBD->escapar($filtros["idcolumna"]);
			}
			if(isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["nota_num"])) {
				$cond[] = "nota_num = " . $this->oBD->escapar($filtros["nota_num"]);
			}
			if(isset($filtros["nota_txt"])) {
				$cond[] = "nota_txt = " . $this->oBD->escapar($filtros["nota_txt"]);
			}
			if(isset($filtros["aprobado"])) {
				$cond[] = "aprobado = " . $this->oBD->escapar($filtros["aprobado"]);
			}
			if(isset($filtros["observacion"])) {
				$cond[] = "observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}
			if(isset($filtros["fechareg"])) {
				$cond[] = "fechareg = " . $this->oBD->escapar($filtros["fechareg"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Notas").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT 
					N.*, 
					C.nombre AS columna_nombre, 
					C.abreviatura AS columna_abreviatura, 
					C.color AS columna_color,
					C.orden AS columna_orden,
					C.tipo_info, 
					C.info_valor,
					H.nombre AS hoja_nombre,
					H.color AS hoja_color,
					H.orden AS hoja_orden,
					A.nombres AS alumno_nombres,
					A.apellidos AS alumno_apellidos,
					A.identificador AS alumno_identificador
				FROM notas N
				JOIN notas_pestania C ON N.idcolumna = C.idpestania
				JOIN notas_pestania H ON N.idhoja = H.idpestania
				JOIN notas_alumno A ON N.idalumno = A.idalumno";			
					
			/*  * * * WHERE * * *  */
			$cond = array();
			
			if(isset($filtros["idnota"])) {
				$cond[] = "N.idnota = " . $this->oBD->escapar($filtros["idnota"]);
			}
			if(isset($filtros["idarchivo"])) {
				$cond[] = "N.idarchivo = " . $this->oBD->escapar($filtros["idarchivo"]);
			}
			if(isset($filtros["idhoja"])) {
				$cond[] = "N.idhoja = " . $this->oBD->escapar($filtros["idhoja"]);
			}
			if(isset($filtros["idcolumna"])) {
				$cond[] = "N.idcolumna = " . $this->oBD->escapar($filtros["idcolumna"]);
			}
			if(isset($filtros["idalumno"])) {
				$cond[] = "N.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["nota_num"])) {
				$cond[] = "N.nota_num = " . $this->oBD->escapar($filtros["nota_num"]);
			}
			if(isset($filtros["nota_txt"])) {
				$cond[] = "N.nota_txt = " . $this->oBD->escapar($filtros["nota_txt"]);
			}
			if(isset($filtros["aprobado"])) {
				$cond[] = "N.aprobado = " . $this->oBD->escapar($filtros["aprobado"]);
			}
			if(isset($filtros["observacion"])) {
				$cond[] = "N.observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}
			if(isset($filtros["fechareg"])) {
				$cond[] = "N.fechareg = " . $this->oBD->escapar($filtros["fechareg"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			/*  * * * ORDER BY * * *  */
			$orderBy = array();

			if(isset($filtros["order_by"])) {
				foreach ($filtros["order_by"] as $field => $asc_desc) {
					if($field=="idnota") {
						$orderBy[] = "N.idnota ".$asc_desc;
					}
					if($field=="idarchivo") {
						$orderBy[] = "N.idarchivo ".$asc_desc;
					}
					if($field=="idhoja") {
						$orderBy[] = "N.idhoja ".$asc_desc;
					}
					if($field=="idcolumna") {
						$orderBy[] = "N.idcolumna ".$asc_desc;
					}
					if($field=="idalumno") {
						$orderBy[] = "N.idalumno ".$asc_desc;
					}
					if($field=="nota_num") {
						$orderBy[] = "N.nota_num ".$asc_desc;
					}
					if($field=="nota_txt") {
						$orderBy[] = "N.nota_txt ".$asc_desc;
					}
					if($field=="aprobado") {
						$orderBy[] = "N.aprobado ".$asc_desc;
					}
					if($field=="observacion") {
						$orderBy[] = "N.observacion ".$asc_desc;
					}
					if($field=="fechareg") {
						$orderBy[] = "N.fechareg ".$asc_desc;
					}
					if($field=="hoja_orden") {
						$orderBy[] = "hoja_orden ".$asc_desc;
					}
					if($field=="columna_orden") {
						$orderBy[] = "columna_orden ".$asc_desc;
					}
				}
			}
			if(!empty($orderBy)) {
				$sql .= " ORDER BY " . implode(' , ', $orderBy);
			}

			#echo $sql; exit(0);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Notas").": " . $e->getMessage());
		}
	}
	
	public function insertar($idarchivo,$idhoja,$idcolumna,$idalumno,$nota_num,$nota_txt,$aprobado,$observacion,$fechareg)
	{
		try {
			
			$this->iniciarTransaccion('dat_notas_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idnota) FROM notas");
			++$id;
			
			$estados = array('idnota' => $id
							,'idarchivo'=>$idarchivo
							,'idhoja'=>$idhoja
							,'idcolumna'=>$idcolumna
							,'idalumno'=>$idalumno
							#,'fechareg'=>$fechareg						
							);
			
			if(!empty($observacion)) {
				$estados["observacion"] =  $observacion;
			}
			if(!empty($nota_num)) {
				$estados["nota_num"] =  $nota_num;
			}
			if(!empty($nota_txt)) {
				$estados["nota_txt"] =  $nota_txt;
			}
			if(isset($aprobado)) {
				$estados["aprobado"] =  $aprobado;
			}

			$this->oBD->insert('notas', $estados);			
			$this->terminarTransaccion('dat_notas_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_notas_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Notas").": " . $e->getMessage());
		}
	}
	public function actualizar($id,$idarchivo,$idhoja,$idcolumna,$idalumno,$nota_num,$nota_txt,$aprobado,$observacion,$fechareg)
	{
		try {
			$this->iniciarTransaccion('dat_notas_update');
			$estados = array('idarchivo'=>$idarchivo
							,'idhoja'=>$idhoja
							,'idcolumna'=>$idcolumna
							,'idalumno'=>$idalumno
							#,'fechareg'=>$fechareg							
							);
			
			if(!empty($observacion)) {
				$estados["observacion"] =  $observacion;
			}
			if(!empty($nota_num)) {
				$estados["nota_num"] =  $nota_num;
			}
			if(!empty($nota_txt)) {
				$estados["nota_txt"] =  $nota_txt;
			}
			if(isset($aprobado)) {
				$estados["aprobado"] =  $aprobado;
			}

			$this->oBD->update('notas ', $estados, array('idnota' => $id));
		    $this->terminarTransaccion('dat_notas_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notas").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM notas  "
					. " WHERE idnota = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Notas").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('notas', array('idnota' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Notas").": " . $e->getMessage());
		}
	}

	public function eliminarXPestania($filtros=null)
	{
		try {
			$sql = "DELETE FROM notas";
			$cond = [];

			if(!empty($filtros["idhoja"])) {
				$cond[] = "idhoja = " . $this->oBD->escapar($filtros["idhoja"]);
			}
			if(!empty($filtros["idcolumna"])) {
				$cond[] = "idcolumna = " . $this->oBD->escapar($filtros["idcolumna"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			#echo($sql); exit(0);
			return $this->oBD->ejecutarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Notas").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('notas', array($propiedad => $valor), array('idnota' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notas").": " . $e->getMessage());
		}
	}
   
		
}