<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		30-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatNotas_pestania extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Notas_pestania").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM notas_pestania";
			
			$cond = array();		
			
			if(isset($filtros["idpestania"])) {
				$cond[] = "idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["abreviatura"])) {
				$cond[] = "abreviatura = " . $this->oBD->escapar($filtros["abreviatura"]);
			}
			if(isset($filtros["tipo_pestania"])) {
				$cond[] = "tipo_pestania = " . $this->oBD->escapar($filtros["tipo_pestania"]);
			}
			if(isset($filtros["tipo_info"])) {
				$cond[] = "tipo_info = " . $this->oBD->escapar($filtros["tipo_info"]);
			}
			if(isset($filtros["info_valor"])) {
				$cond[] = "info_valor = " . $this->oBD->escapar($filtros["info_valor"]);
			}
			if(isset($filtros["color"])) {
				$cond[] = "color = " . $this->oBD->escapar($filtros["color"]);
			}
			if(isset($filtros["orden"])) {
				$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(isset($filtros["idarchivo"])) {
				$cond[] = "idarchivo = " . $this->oBD->escapar($filtros["idarchivo"]);
			}
			if(isset($filtros["idpestania_padre"])) {
				$cond[] = "idpestania_padre = " . $this->oBD->escapar($filtros["idpestania_padre"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Notas_pestania").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM notas_pestania";
			
			$cond = array();
			
			if(isset($filtros["idpestania"])) {
				$cond[] = "idpestania = " . $this->oBD->escapar($filtros["idpestania"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["abreviatura"])) {
				$cond[] = "abreviatura = " . $this->oBD->escapar($filtros["abreviatura"]);
			}
			if(isset($filtros["tipo_pestania"])) {
				if(is_array($filtros["tipo_pestania"])) {
					$cond[] = "tipo_pestania IN (". implode(' , ', $filtros["tipo_pestania"]) .") ";
				} else {
					$cond[] = "tipo_pestania = " . $this->oBD->escapar($filtros["tipo_pestania"]);
				}
			}
			if(isset($filtros["tipo_info"])) {
				if(is_array($filtros["tipo_info"])) {
					$cond[] = "tipo_info IN (". implode(' , ', $filtros["tipo_info"]) .") ";
				} else {
					$cond[] = "tipo_info = " . $this->oBD->escapar($filtros["tipo_info"]);
				}
			}
			if(isset($filtros["info_valor"])) {
				$cond[] = "info_valor = " . $this->oBD->escapar($filtros["info_valor"]);
			}
			if(isset($filtros["color"])) {
				$cond[] = "color = " . $this->oBD->escapar($filtros["color"]);
			}
			if(isset($filtros["orden"])) {
				$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(isset($filtros["idarchivo"])) {
				$cond[] = "idarchivo = " . $this->oBD->escapar($filtros["idarchivo"]);
			}
			if(isset($filtros["idpestania_padre"])) {
				$cond[] = "idpestania_padre = " . $this->oBD->escapar($filtros["idpestania_padre"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY orden ASC";
			#echo $sql; exit(0);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Notas_pestania").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$abreviatura,$tipo_pestania,$tipo_info,$info_valor,$color,$orden,$idarchivo,$idpestania_padre) 
	{
		try {
			
			$this->iniciarTransaccion('dat_notas_pestania_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpestania) FROM notas_pestania");
			++$id;
			
			$estados = array('idpestania' => $id
							,'nombre'=>$nombre
							,'tipo_pestania'=>$tipo_pestania
							,'orden'=>$orden
							,'idarchivo'=>$idarchivo
							);

			if(!empty($abreviatura)) {
				$estados["abreviatura"] =  $abreviatura;
			}
			if(!empty($tipo_info)) {
				$estados["tipo_info"] =  $tipo_info;
			}
			if(!empty($info_valor)) {
				$estados["info_valor"] =  $info_valor;
			}
			if(!empty($color)) {
				$estados["color"] =  $color;
			}
			if(!empty($idpestania_padre)) {
				$estados["idpestania_padre"] =  $idpestania_padre;
			}
			
			$this->oBD->insert('notas_pestania', $estados);			
			$this->terminarTransaccion('dat_notas_pestania_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_notas_pestania_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Notas_pestania").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$abreviatura,$tipo_pestania,$tipo_info,$info_valor,$color,$orden,$idarchivo,$idpestania_padre)
	{
		try {
			$this->iniciarTransaccion('dat_notas_pestania_update');
			$estados = array('nombre'=>$nombre
							,'tipo_pestania'=>$tipo_pestania
							,'orden'=>$orden
							,'idarchivo'=>$idarchivo
							);
			
			if(!empty($abreviatura)) {
				$estados["abreviatura"] =  $abreviatura;
			}
			if(!empty($tipo_info)) {
				$estados["tipo_info"] =  $tipo_info;
			}
			if(!empty($info_valor)) {
				$estados["info_valor"] =  $info_valor;
			}
			if(!empty($color)) {
				$estados["color"] =  $color;
			}
			if(!empty($idpestania_padre)) {
				$estados["idpestania_padre"] =  $idpestania_padre;
			}
			$this->oBD->update('notas_pestania ', $estados, array('idpestania' => $id));
		    $this->terminarTransaccion('dat_notas_pestania_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notas_pestania").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM notas_pestania  "
					. " WHERE idpestania = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Notas_pestania").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('notas_pestania', array('idpestania' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Notas_pestania").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('notas_pestania', array($propiedad => $valor), array('idpestania' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Notas_pestania").": " . $e->getMessage());
		}
	}
   
		
}