<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

JrCargador::clase('sys_datos::BD::BDMySQLI', RUTA_BASE, 'sys_datos::BD');
class DatBase
{
	protected $oBD;
	protected $num_regs;
	protected $usuario;
	public function conectar()
	{
		try {
			//$this->oBD = BDMySQLI::getInstancia('localhost','pvingles','root','');	
			$this->oBD = BDMySQLI::getInstancia('127.0.0.1','pvingles.local','root','');			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function iniciarTransaccion($id_transaccion = '')
	{
		$this->oBD->iniciarTransaccion($id_transaccion);
	}

	public function terminarTransaccion($id_transaccion = '')
	{
		$this->oBD->terminarTransaccion($id_transaccion);
	}

	public function cancelarTransaccion($id_transaccion = '')
	{
		$this->oBD->truncarTransaccion($id_transaccion);
	}

	public function setLimite($desde, $desplazamiento)
	{
		$this->oBD->setLimite($desde, $desplazamiento);
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($nombre);

		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}
}
