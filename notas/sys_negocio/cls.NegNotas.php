<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		30-01-2018
 * @copyright	Copyright (C) 30-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNotas', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatNotas_pestania', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatNotas_alumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegNotas 
{
	protected $idnota;
	protected $idarchivo;
	protected $idhoja;
	protected $idcolumna;
	protected $idalumno;
	protected $nota_num;
	protected $nota_txt;
	protected $aprobado;
	protected $observacion;
	protected $fechareg;
	
	protected $dataNotas;
	protected $oDatNotas;
	protected $oDatNotas_pestania;
	protected $oDatNotas_alumno;

	public function __construct()
	{
		$this->oDatNotas = new DatNotas;
		$this->oDatNotas_pestania = new DatNotas_pestania;
		$this->oDatNotas_alumno = new DatNotas_alumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatNotas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatNotas->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatNotas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatNotas->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatNotas->get($this->idnota);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatNotas->iniciarTransaccion('neg_i_Notas');
			$this->idnota = $this->oDatNotas->insertar($this->idarchivo,$this->idhoja,$this->idcolumna,$this->idalumno,$this->nota_num,$this->nota_txt,$this->aprobado,$this->observacion,$this->fechareg);
			$this->oDatNotas->terminarTransaccion('neg_i_Notas');	
			return $this->idnota;
		} catch(Exception $e) {	
		    $this->oDatNotas->cancelarTransaccion('neg_i_Notas');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatNotas->actualizar($this->idnota,$this->idarchivo,$this->idhoja,$this->idcolumna,$this->idalumno,$this->nota_num,$this->nota_txt,$this->aprobado,$this->observacion,$this->fechareg);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function guardarPromediosPonderados($idColumna = 0)
	{
		try {
			if($idColumna==0){ throw new Exception(JrTexto::_("IdPestania is missing for average scores")); }
			$new_IDs = array();

			$columnas = $this->oDatNotas_pestania->buscar(array("idpestania"=>$idColumna));

			if(!empty($columnas)) {
				$columna = $columnas[0];
				$info_valor = json_decode($columna["info_valor"], true);
				$formula = $info_valor["formula"];
				$minimo_pass = $info_valor["min_pass"];
				$alumnos = $this->oDatNotas_alumno->buscar(array("idarchivo"=>$columna["idarchivo"]));
				if(!empty($alumnos)) {
					foreach ($alumnos as $a) {
						$promPonderado = $this->calcularPonderado($a, $formula, $minimo_pass);
						$buscarNota = $this->oDatNotas->buscar(array("idhoja"=>$columna["idpestania_padre"] ,"idcolumna"=>$columna["idpestania"] ,"idalumno"=>$a["idalumno"] ,"idarchivo"=>$columna["idarchivo"] ));
						$acc='_add';
						if(!empty($buscarNota)) {
							$this->idnota = @$buscarNota[0]["idnota"];
							$acc='_edit';
						}
						$this->idarchivo = $columna["idarchivo"];
						$this->idhoja = $columna["idpestania_padre"];
						$this->idcolumna = $columna["idpestania"];
						$this->idalumno = $a["idalumno"];
						$this->nota_num = $promPonderado["calculado"];
						$this->nota_txt = null;
						$this->aprobado = $promPonderado["aprobado"];
						$this->observacion = $promPonderado["texto_formula"];
						$this->fechareg = date('Y-m-d H:i:s');

						if($acc=='_add') {
							$new_IDs[] = $this->agregar();
						} else {
							$new_IDs[] = $this->editar();
						}
					}
				}
			}
	
			return $new_IDs;
		} catch(Exception $e) {	
		    $this->oDatNotas->cancelarTransaccion('neg_i_Notas');		
			throw new Exception($e->getMessage());
		}
	}

	public function calcularPonderado($alumno = array(), $formula = array(), $minimo_pass = 0.0) {
		try {
			$notaPonderada = $pesoTotal = $notaAcumulada = 0.0;
			$strFormula = '';
			$strNotas = array();
			foreach ($formula as $f) {
				$peso = (float) $f['peso'];
				$buscarNota = $this->oDatNotas->buscar(array("idalumno"=>$alumno['idalumno'], "idcolumna"=>$f['idpestania'] ));
				if(!empty($buscarNota)) {
					$nota = $buscarNota[0]["nota_num"];
					$nota = !empty($nota)?( (float)$nota ):0.0;
				} else {
					$nota = 0.0;
				}
				$strNotas[] = $peso.'*'.$nota;
				$notaAcumulada += $peso*$nota;
				$pesoTotal += $peso;
			}
			$notaPonderada = $notaAcumulada/$pesoTotal;
			$strFormula = "(". implode(' + ', $strNotas) .")/".$pesoTotal;

			return array(
				"calculado" => $notaPonderada,
				"texto_formula" => $strFormula,
				"aprobado" => ($notaPonderada > $minimo_pass) ? '1' : '0',
			);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getNotasXAlumno($idArchivo = null, $idAlumno = null)
	{
		try {
			if(empty($idArchivo) || empty($idAlumno)) {
				throw new Exception(JrTexto::_("Params are missing"));
			}
			$response = array();

			$hojas = $this->oDatNotas_pestania->buscar(array('idarchivo'=>$idArchivo, 'tipo_pestania'=>'H'));
			if(!empty($hojas)) {
				foreach ($hojas as $h) {
					$columnas = $this->oDatNotas_pestania->buscar(array('idarchivo'=>$idArchivo, 'tipo_pestania'=>'C', 'idpestania_padre'=>$h["idpestania"]));
					$h["columnas"] = array();
					if(!empty($columnas)) {
						foreach ($columnas as $j => $c) {
							$notas = $this->oDatNotas->buscar(array(
								'idalumno'	=> $idAlumno, 
								'idarchivo'	=> $idArchivo, 
								'idhoja'	=> $h["idpestania"], 
								'idcolumna'	=> $c["idpestania"],
								'order_by'	=> array(
									'idarchivo'		=> 'ASC',
									'hoja_orden'	=> 'ASC',
									'columna_orden'	=> 'ASC',
								)
							));
							$c["nota"] = @$notas[0];
							$h["columnas"][] = $c;
						}
					}
					$response[] = $h;
				}
			}

			return $response;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Notas', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotas->eliminar($this->idnota);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function eliminarXPestania($condicion = array())
	{
		try {
			/*if(!NegSesion::tiene_acceso('Notas', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotas->eliminarXPestania($condicion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdnota($pk){
		try {
			$this->dataNotas = $this->oDatNotas->get($pk);
			if(empty($this->dataNotas)) {
				throw new Exception(JrTexto::_("Notas").' '.JrTexto::_("not registered"));
			}
			$this->idnota = $this->dataNotas["idnota"];
			$this->idarchivo = $this->dataNotas["idarchivo"];
			$this->idhoja = $this->dataNotas["idhoja"];
			$this->idcolumna = $this->dataNotas["idcolumna"];
			$this->idalumno = $this->dataNotas["idalumno"];
			$this->nota_num = $this->dataNotas["nota_num"];
			$this->nota_txt = $this->dataNotas["nota_txt"];
			$this->observacion = $this->dataNotas["observacion"];
			$this->fechareg = $this->dataNotas["fechareg"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('notas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataNotas = $this->oDatNotas->get($pk);
			if(empty($this->dataNotas)) {
				throw new Exception(JrTexto::_("Notas").' '.JrTexto::_("not registered"));
			}

			return $this->oDatNotas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
		
}